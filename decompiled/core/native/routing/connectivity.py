try:
    from _pathing import connectivity_handle as Handle, connectivity_handle_list as HandleList
except ImportError:

    class Handle:
        __qualname__ = 'Handle'

        def __init__(self, location):
            self.location = location

    class HandleList:
        __qualname__ = 'HandleList'

        def __init__(self):
            pass

import pickle
from sims4.common import get_available_packs
import sims4.log
import sims4.resources
logger = sims4.log.Logger('ACCBCC', default_owner='manus')
AC_CACHE_FILENAME = 'ac_pickle_cache'
AC_CACHE_PY_UNOPT_FILENAME = 'ac_pickle_cache_py_unopt'
AC_FILENAME_EXTENSION = '.ach'
AC_CACHE_VERSION = b'version#0004'
_wrong_ac_cache_version = False
TEST_LOCAL_CACHE = False

def read_ac_cache_from_resource():
    global _wrong_ac_cache_version
    if _wrong_ac_cache_version:
        return {}
    ac_cache_combined = {}
    available_packs = get_available_packs()
    logger.info('Available packs: {}', available_packs)
    if TEST_LOCAL_CACHE:
        file_name = None
        file_name = 'C:\\tmp\\ac_bc_cache\\ac_pickle_cache'
        for pack in available_packs:
            pack_name = str(pack).replace('Pack.', '')
            pack_file = file_name + '_' + pack_name + AC_FILENAME_EXTENSION
            logger.always('Loading AC cache file {}.'.format(pack_file))
            with open(pack_file, 'rb') as ac_cache_file:
                resource_version = ac_cache_file.read(len(AC_CACHE_VERSION))
                ret = pickle.load(ac_cache_file)
                logger.always('Loaded AC cache with {} entries.', len(ret))
                ac_cache_combined.update(ret)
        return ac_cache_combined
    key_name = None
    key_name = AC_CACHE_FILENAME
    for pack in available_packs:
        pack_name = str(pack).replace('Pack.', '')
        pack_key = key_name + '_' + pack_name
        key = sims4.resources.Key.hash64(pack_key, sims4.resources.Types.AC_CACHE)
        loader = sims4.resources.ResourceLoader(key)
        ac_cache_file = loader.load()
        logger.info('Loading AC cache {} (key: {}) as file {}.', pack_key, key, ac_cache_file)
        if not ac_cache_file:
            logger.debug('Failed to load animation constraint cache file from the resource loader (key = {})', pack_key)
        resource_version = ac_cache_file.read(len(AC_CACHE_VERSION))
        if resource_version != AC_CACHE_VERSION:
            _wrong_ac_cache_version = True
            logger.warn('The Animation Constraint cache in the resource manager is from a different version. Current version is {}, resource manager version is {}.\nStartup will be slower until the versions are aligned.', AC_CACHE_VERSION, resource_version)
            return {}
        try:
            ac_cache_combined.update(pickle.load(ac_cache_file))
        except pickle.UnpicklingError as exc:
            logger.exception('Unpickling the Animation Constraint cache failed. Startup will be slower as a consequence.', exc=exc)
            return {}
    return ac_cache_combined

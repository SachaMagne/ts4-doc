from aspirations.aspiration_types import AspriationType
from sims4.tuning.instance_manager import InstanceManager

class AspirationInstanceManager(InstanceManager):
    __qualname__ = 'AspirationInstanceManager'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._nonemotional_whimsets = []
        self._emotional_whimsets = {}

    def all_whim_sets_gen(self):
        yield self._nonemotional_whimsets
        yield self._emotional_whimsets.values()

    def get_emotional_whimset(self, mood):
        return self._emotional_whimsets.get(mood)

    def on_start(self):
        super().on_start()
        for aspiration in self.types.values():
            while aspiration.aspiration_type() == AspriationType.WHIM_SET:
                if aspiration.whimset_emotion is not None:
                    self._emotional_whimsets[aspiration.whimset_emotion] = aspiration
                else:
                    self._nonemotional_whimsets.append(aspiration)

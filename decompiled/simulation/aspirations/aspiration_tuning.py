from aspirations.aspiration_types import AspriationType
from event_testing import objective_tuning
from event_testing.milestone import Milestone
from event_testing.resolver import DoubleSimResolver
from event_testing.results import TestResult
from interactions import ParticipantType
from interactions.utils.notification import NotificationElement
from sims import genealogy_tracker
from sims4.tuning.instances import HashedTunedInstanceMetaclass, lock_instance_tunables
from sims4.tuning.tunable import TunableEnumEntry, HasTunableSingletonFactory, AutoFactoryInit, TunableSet, TunableReference, OptionalTunable
from sims4.tuning.tunable_base import GroupNames, GroupNames, SourceQueries
from sims4.utils import classproperty
from singletons import DEFAULT
from situations.situation_goal import TunableWeightedSituationGoalReference
from ui.ui_dialog import UiDialogResponse
from ui.ui_dialog_notification import UiDialogNotification
import enum
import event_testing
import services
import sims4.localization
import sims4.log
import sims4.tuning.tunable
import ui.screen_slam
logger = sims4.log.Logger('AspirationTuning')

class AspirationBasic(Milestone, metaclass=HashedTunedInstanceMetaclass, manager=services.get_instance_manager(sims4.resources.Types.ASPIRATION)):
    __qualname__ = 'AspirationBasic'
    INSTANCE_SUBCLASSES_ONLY = True
    INSTANCE_TUNABLES = {'complete_only_in_sequence': sims4.tuning.tunable.Tunable(description='\n            Aspirations that will only start progress if all previous track aspirations are complete.', tunable_type=bool, default=False), 'disabled': sims4.tuning.tunable.Tunable(description='\n            Checking this box will remove this Aspiration from the event system and the UI, but preserve the tuning.', tunable_type=bool, default=False, export_modes=sims4.tuning.tunable_base.ExportModes.All), 'screen_slam': OptionalTunable(description='\n            Which screen slam to show when this aspiration is complete.\n            Localization Tokens: Sim - {0.SimFirstName}, Milestone Name - \n            {1.String}, Aspiration Track Name - {2.String}\n            ', tunable=ui.screen_slam.TunableScreenSlamSnippet())}

    @classmethod
    def handle_event(cls, sim_info, event, resolver):
        if sim_info is not None:
            sim_info.aspiration_tracker.handle_event(cls, event, resolver)

    @classmethod
    def aspiration_type(cls):
        return AspriationType.BASIC

    @classmethod
    def register_callbacks(cls):
        tests = [objective.objective_test for objective in cls.objectives]
        services.get_event_manager().register_tests(cls, tests)

class Aspiration(AspirationBasic):
    __qualname__ = 'Aspiration'
    INSTANCE_TUNABLES = {'display_name': sims4.localization.TunableLocalizedString(description='\n            Display name for this aspiration\n            ', allow_none=True, export_modes=sims4.tuning.tunable_base.ExportModes.All), 'descriptive_text': sims4.localization.TunableLocalizedString(description='\n            Description for this aspiration\n            ', allow_none=True, export_modes=sims4.tuning.tunable_base.ExportModes.All), 'is_child_aspiration': sims4.tuning.tunable.Tunable(description='\n            Child aspirations are only possible to complete as a child.\n            ', tunable_type=bool, default=False, export_modes=sims4.tuning.tunable_base.ExportModes.All), 'reward': sims4.tuning.tunable.TunableReference(description='\n            Which rewards are given when this aspiration is completed.\n            ', manager=services.get_instance_manager(sims4.resources.Types.REWARD), allow_none=True)}

    @classmethod
    def aspiration_type(cls):
        return AspriationType.FULL_ASPIRATION

    @classmethod
    def _verify_tuning_callback(cls):
        for objective in cls.objectives:
            pass
        logger.debug('Loading asset: {0}', cls)

class AspirationSimInfoPanel(AspirationBasic):
    __qualname__ = 'AspirationSimInfoPanel'
    INSTANCE_TUNABLES = {'display_name': sims4.localization.TunableLocalizedString(description='\n            Display name for this aspiration.\n            ', allow_none=True, export_modes=sims4.tuning.tunable_base.ExportModes.All), 'descriptive_text': sims4.localization.TunableLocalizedString(description='\n            Description for this aspiration.\n            ', allow_none=True, export_modes=sims4.tuning.tunable_base.ExportModes.All), 'category': sims4.tuning.tunable.TunableReference(description='\n            The category this aspiration track goes into when displayed in the\n            UI.\n            ', manager=services.get_instance_manager(sims4.resources.Types.ASPIRATION_CATEGORY), export_modes=sims4.tuning.tunable_base.ExportModes.All)}

    @classmethod
    def aspiration_type(cls):
        return AspriationType.SIM_INFO_PANEL

    @classmethod
    def _verify_tuning_callback(cls):
        for objective in cls.objectives:
            pass

lock_instance_tunables(AspirationSimInfoPanel, complete_only_in_sequence=False)

class AspirationNotification(AspirationBasic):
    __qualname__ = 'AspirationNotification'
    INSTANCE_TUNABLES = {'objectives': sims4.tuning.tunable.TunableList(description='\n            A Set of objectives for completing an aspiration.', tunable=sims4.tuning.tunable.TunableReference(description='\n                One objective for an aspiration', manager=services.get_instance_manager(sims4.resources.Types.OBJECTIVE))), 'disabled': sims4.tuning.tunable.Tunable(description='\n            Checking this box will remove this Aspiration from the event system and the UI, but preserve the tuning.', tunable_type=bool, default=False), 'notification': UiDialogNotification.TunableFactory(description='\n            This text will display in a notification pop up when completed.\n            ')}

    @classmethod
    def aspiration_type(cls):
        return AspriationType.NOTIFICATION

lock_instance_tunables(AspirationNotification, complete_only_in_sequence=False)

class AspirationCareer(AspirationBasic):
    __qualname__ = 'AspirationCareer'

    def reward(self, *args, **kwargs):
        pass

    @classmethod
    def aspiration_type(cls):
        return AspriationType.CAREER

    @classmethod
    def _verify_tuning_callback(cls):
        for objective in cls.objectives:
            pass

lock_instance_tunables(AspirationCareer, complete_only_in_sequence=True)

class AspirationFamilialTrigger(AspirationBasic):
    __qualname__ = 'AspirationFamilialTrigger'
    INSTANCE_TUNABLES = {'objectives': sims4.tuning.tunable.TunableList(description='\n            A Set of objectives for completing an aspiration.', tunable=sims4.tuning.tunable.TunableReference(description='\n                One objective for an aspiration', manager=services.get_instance_manager(sims4.resources.Types.OBJECTIVE))), 'target_family_relationships': TunableSet(description='\n            These relations will get an event message upon Aspiration completion that they can test for.', tunable=TunableEnumEntry(genealogy_tracker.FamilyRelationshipIndex, genealogy_tracker.FamilyRelationshipIndex.FATHER)), 'disabled': sims4.tuning.tunable.Tunable(description='\n            Checking this box will remove this Aspiration from the event system and the UI, but preserve the tuning.', tunable_type=bool, default=False)}

    @classmethod
    def aspiration_type(cls):
        return AspriationType.FAMILIAL

    @classmethod
    def _verify_tuning_callback(cls):
        for objective in cls.objectives:
            pass

lock_instance_tunables(AspirationFamilialTrigger, complete_only_in_sequence=False)

class AspirationCategory(metaclass=HashedTunedInstanceMetaclass, manager=services.get_instance_manager(sims4.resources.Types.ASPIRATION_CATEGORY)):
    __qualname__ = 'AspirationCategory'
    INSTANCE_TUNABLES = {'display_text': sims4.localization.TunableLocalizedString(description='Text used to show the Aspiration Category name in the UI', export_modes=sims4.tuning.tunable_base.ExportModes.All), 'ui_sort_order': sims4.tuning.tunable.Tunable(description='\n            Order in which this category is sorted against other categories in the UI.\n            If two categories share the same sort order, undefined behavior will insue.\n            ', tunable_type=int, default=0, export_modes=sims4.tuning.tunable_base.ExportModes.All), 'icon': sims4.tuning.tunable.TunableResourceKey(None, resource_types=sims4.resources.CompoundTypes.IMAGE, description='\n            The icon to be displayed in the panel view.\n            ', allow_none=True, export_modes=sims4.tuning.tunable_base.ExportModes.All, tuning_group=GroupNames.UI), 'is_sim_info_panel': sims4.tuning.tunable.Tunable(description='\n            Checking this box will mark this category for the Sim Info Panel, not the Aspirations panel.', tunable_type=bool, default=False, export_modes=sims4.tuning.tunable_base.ExportModes.All)}

class AspirationTrackLevels(enum.Int):
    __qualname__ = 'AspirationTrackLevels'
    LEVEL_1 = 1
    LEVEL_2 = 2
    LEVEL_3 = 3
    LEVEL_4 = 4
    LEVEL_5 = 5
    LEVEL_6 = 6

TRACK_LEVEL_MAX = 6

class AspirationTrack(metaclass=HashedTunedInstanceMetaclass, manager=services.get_instance_manager(sims4.resources.Types.ASPIRATION_TRACK)):
    __qualname__ = 'AspirationTrack'
    INSTANCE_TUNABLES = {'display_text': sims4.localization.TunableLocalizedString(description='\n            Text used to show the Aspiration Track name in the UI', export_modes=sims4.tuning.tunable_base.ExportModes.All), 'description_text': sims4.localization.TunableLocalizedString(description='\n            Text used to show the Aspiration Track description in the UI', export_modes=sims4.tuning.tunable_base.ExportModes.All), 'icon': sims4.tuning.tunable.TunableResourceKey(None, resource_types=sims4.resources.CompoundTypes.IMAGE, description='\n            The icon to be displayed in the panel view.\n            ', export_modes=sims4.tuning.tunable_base.ExportModes.All, tuning_group=GroupNames.UI), 'icon_high_res': sims4.tuning.tunable.TunableResourceKey(None, resource_types=sims4.resources.CompoundTypes.IMAGE, description='\n            The icon to be displayed in aspiration track selection.\n            ', allow_none=True, export_modes=sims4.tuning.tunable_base.ExportModes.All, tuning_group=GroupNames.UI), 'category': sims4.tuning.tunable.TunableReference(description='\n            The category this aspiration track goes into when displayed in the UI.', manager=services.get_instance_manager(sims4.resources.Types.ASPIRATION_CATEGORY), export_modes=sims4.tuning.tunable_base.ExportModes.All), 'primary_trait': sims4.tuning.tunable.TunableReference(description='\n            This is the primary Aspiration reward trait that is applied upon selection from CAS.', manager=services.get_instance_manager(sims4.resources.Types.TRAIT), export_modes=sims4.tuning.tunable_base.ExportModes.All, allow_none=True), 'aspirations': sims4.tuning.tunable.TunableMapping(description='\n            A Set of objectives for completing an aspiration.', key_type=TunableEnumEntry(AspirationTrackLevels, AspirationTrackLevels.LEVEL_1), value_type=sims4.tuning.tunable.TunableReference(description='\n                One aspiration in the track, associated for a level', manager=services.get_instance_manager(sims4.resources.Types.ASPIRATION), class_restrictions='Aspiration', reload_dependent=True), export_modes=sims4.tuning.tunable_base.ExportModes.All), 'reward': sims4.tuning.tunable.TunableReference(description='\n            Which rewards are given when this aspiration track is completed.', manager=services.get_instance_manager(sims4.resources.Types.REWARD), export_modes=sims4.tuning.tunable_base.ExportModes.All), 'notification': UiDialogNotification.TunableFactory(description='\n            This text will display in a notification pop up when completed.\n            ', locked_args={'text_tokens': DEFAULT, 'icon': None, 'primary_icon_response': UiDialogResponse(text=None, ui_request=UiDialogResponse.UiDialogUiRequest.SHOW_ASPIRATION_SELECTOR), 'secondary_icon': None}), 'mood_asm_param': sims4.tuning.tunable.Tunable(description="\n            The asm parameter for Sim's mood for use with CAS ASM state machine, driven by selection\n            of this AspirationTrack, i.e. when a player selects the a romantic aspiration track, the Flirty\n            ASM is given to the state machine to play. The name tuned here must match the animation\n            state name parameter expected in Swing.", tunable_type=str, default=None, source_query=SourceQueries.SwingEnumNamePattern.format('mood'), export_modes=sims4.tuning.tunable_base.ExportModes.All)}
    _sorted_aspirations = None

    @classmethod
    def get_aspirations(cls):
        return cls._sorted_aspirations

    @classmethod
    def get_next_aspriation(cls, current_aspiration):
        next_aspiration_level = None
        current_aspiration_guid = current_aspiration.guid64
        for (level, track_aspiration) in cls.aspirations.items():
            while track_aspiration.guid64 == current_aspiration_guid:
                next_aspiration_level = int(level) + 1
                break
        if next_aspiration_level in cls.aspirations:
            return cls.aspirations[next_aspiration_level]

    @classproperty
    def is_child_aspiration_track(cls):
        return cls._sorted_aspirations[0][1].is_child_aspiration

    @classmethod
    def _tuning_loaded_callback(cls):
        cls._sorted_aspirations = tuple(sorted(cls.aspirations.items()))

    @classmethod
    def _verify_tuning_callback(cls):
        logger.debug('Loading asset: {}', cls, owner='ddriscoll')
        if cls.category == None:
            logger.error('{} Aspiration Track has no category set.', cls, owner='ddriscoll')
        if len(cls.aspirations) == 0:
            logger.error('{} Aspiration Track has no aspirations mapped to levels.', cls, owner='ddriscoll')
        else:
            aspiration_list = cls.aspirations.values()
            aspiration_set = set(aspiration_list)
            if len(aspiration_set) != len(aspiration_list):
                logger.error('{} Aspiration Track has repeating aspiration values in the aspiration map.', cls, owner='ddriscoll')

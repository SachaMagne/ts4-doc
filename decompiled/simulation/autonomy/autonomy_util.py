import sims4.reload
with sims4.reload.protected(globals()):
    sim_id_to_sub_autonomy_ping = None
    info_start_time = None

class SubAutonomyPingData:
    __qualname__ = 'SubAutonomyPingData'

    def __init__(self):
        self.num_mixers_cached = []
        self.cache_hits = 0
        self.cache_use_fails = 0
        self.mixers_cleared = 0

def get_info_start_time():
    return info_start_time

def record_autonomy_ping_data(start_time):
    global info_start_time, sim_id_to_sub_autonomy_ping
    if info_start_time:
        return
    info_start_time = start_time
    sim_id_to_sub_autonomy_ping = dict()

def stop_sub_autonomy_ping_data():
    global info_start_time, sim_id_to_sub_autonomy_ping
    if not info_start_time:
        return
    info_start_time = None
    sim_id_to_sub_autonomy_ping.clear()
    sim_id_to_sub_autonomy_ping = None

def print_sub_autonomy_ping_data(current_time, output):
    if not sim_id_to_sub_autonomy_ping:
        output('No recordings have been made.')
        return
    output('Total Time Recording (SimTime): {}'.format(current_time - info_start_time))
    total_request = 0
    total_cached = 0
    total_hits = 0
    total_failures = 0
    total_unused = 0
    num_caches_under_max = 0
    for ping_data in sim_id_to_sub_autonomy_ping.values():
        total_request += len(ping_data.num_mixers_cached)
        total_failures += ping_data.cache_use_fails
        total_hits += ping_data.cache_hits
        total_unused += ping_data.mixers_cleared
        for (num_cached_mixers, max_to_cache) in ping_data.num_mixers_cached:
            total_cached += num_cached_mixers
            while num_cached_mixers < max_to_cache:
                num_caches_under_max += 1
    output('Total Requests: {}'.format(total_request))
    output('Total Mixers Cached: {}'.format(total_cached))
    output('Total Cached Mixers Not Used: {} ({:.2f}%)'.format(total_unused, total_unused/total_cached))
    output('Total Cache Hits: {} ({:.2f}%)'.format(total_hits, total_hits/total_cached))
    output('Total Cache Misses: {} ({:.2f}%)'.format(total_failures, total_failures/total_cached))
    output('Average Cached per request: {}'.format(total_cached/total_request))
    output('Caches Under Max Request: {}'.format(num_caches_under_max))

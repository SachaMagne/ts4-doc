from bucks.bucks_enums import BucksType
from server_commands.argument_helpers import TunableInstanceParam
from sims4.commands import CommandType
import services
import sims4
logger = sims4.log.Logger('Bucks', default_owner='tastle')

@sims4.commands.Command('bucks.request_perks_list', command_type=CommandType.Live)
def request_perks_list(bucks_type, _connection=None):
    active_household = services.active_household()
    active_household.bucks_tracker.send_perks_list_for_bucks_type(bucks_type)

@sims4.commands.Command('bucks.unlock_perk', command_type=CommandType.Live)
def unlock_perk_by_name_or_id(bucks_perk, unlock_for_free:bool=False, _connection=None):
    active_household = services.active_household()
    if unlock_for_free:
        active_household.bucks_tracker.unlock_perk(bucks_perk)
    else:
        active_household.bucks_tracker.pay_for_and_unlock_perk(bucks_perk)

@sims4.commands.Command('bucks.update_bucks_by_amount')
def update_bucks_by_amount(bucks_type, amount:int=0, _connection=None):
    active_household = services.active_household()
    result = active_household.bucks_tracker.try_modify_bucks(bucks_type, amount)
    if not result:
        sims4.commands.output('Failed to give {} bucks of type {}'.format(amount, bucks_type), _connection)
    else:
        sims4.commands.output('Granted {} bucks of type {}'.format(amount, bucks_type), _connection)

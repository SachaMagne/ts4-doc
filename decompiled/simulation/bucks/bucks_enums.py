from sims4.tuning.dynamic_enum import DynamicEnumLocked

class BucksType(DynamicEnumLocked, partitioned=True):
    __qualname__ = 'BucksType'
    INVALID = 0

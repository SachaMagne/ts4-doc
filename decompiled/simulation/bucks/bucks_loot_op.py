from bucks.bucks_enums import BucksType
from interactions.utils.loot_basic_op import BaseLootOperation
from sims4.tuning.tunable import TunableEnumEntry, TunableRange
import sims4
logger = sims4.log.Logger('Bucks', default_owner='tastle')

class BucksLoot(BaseLootOperation):
    __qualname__ = 'BucksLoot'
    FACTORY_TUNABLES = {'bucks_type': TunableEnumEntry(description='\n            The type of Bucks to grant.\n            ', tunable_type=BucksType, default=BucksType.INVALID), 'amount': TunableRange(description='\n            The amount of Bucks to award. Must be a positive value.\n            ', tunable_type=int, default=10, minimum=1)}

    def __init__(self, bucks_type, amount, **kwargs):
        super().__init__(**kwargs)
        self._bucks_type = bucks_type
        self._amount = amount

    def _apply_to_subject_and_target(self, subject, target, resolver):
        household = subject.household
        if household is None:
            logger.error('Attempting to apply a BucksLoot op to the subject {} but they have no household.', subject)
            return
        result = household.bucks_tracker.try_modify_bucks(self._bucks_type, self._amount)
        if not result:
            logger.error("Failed to modify the household {}'s bucks of type {} by amount {}.", household, self._bucks_type, self._amount)

from bucks.bucks_enums import BucksType
from interactions.utils.tunable_icon import TunableIconFactory
from rewards.reward_tuning import TunableSpecificReward
from sims4.localization import TunableLocalizedStringFactory
from sims4.tuning.instances import TunedInstanceMetaclass
from sims4.tuning.tunable import TunableReference, TunableEnumEntry, Tunable, TunableList, TunableTuple, OptionalTunable, TunableRange
from sims4.tuning.tunable_base import GroupNames
import services
import sims4
logger = sims4.log.Logger('Bucks', default_owner='tastle')

class BucksPerkTunables:
    __qualname__ = 'BucksPerkTunables'
    PERK_UNLOCKED_TOOLTIP = TunableLocalizedStringFactory(description='\n        A tooltip that will be shown on Perks unlocked manually by the user.\n        \n        Expected Arguments: None.\n        ')
    LINKED_PERK_UNLOCKED_TOOLTIP = TunableLocalizedStringFactory(description="\n        A tooltip that will be shown on Perks unlocked because another Perk's\n        linked_perks list referenced them. Lets the user know why this Perk is\n        no longer available for purchase even though they never explicitely\n        bought it.\n        \n        Expected Arguments: {0.String} - The display name of the Perk that\n        unlocked this one.\n        ")

class BucksPerk(metaclass=TunedInstanceMetaclass, manager=services.get_instance_manager(sims4.resources.Types.BUCKS_PERK)):
    __qualname__ = 'BucksPerk'
    INSTANCE_TUNABLES = {'associated_bucks_type': TunableEnumEntry(description='\n            The type of Bucks required to unlock this Perk.\n            ', tunable_type=BucksType, default=BucksType.INVALID), 'unlock_cost': Tunable(description='\n            How many Bucks of the specified type this Perk costs to unlock.\n            ', tunable_type=int, default=100), 'rewards': TunableList(description='\n            A list of rewards to grant the household when this Perk is\n            unlocked.\n            ', tunable=TunableSpecificReward()), 'linked_perks': TunableList(description='\n            A list of Perks that will be unlocked along with this one if not\n            already unlocked.\n            ', tunable=TunableReference(manager=services.get_instance_manager(sims4.resources.Types.BUCKS_PERK))), 'temporary_perk_information': OptionalTunable(TunableTuple(description='\n            Tunables associated with temporary Perks.\n            ', duration=TunableRange(description='\n                The tunable number of Sim hours this Perk should last for, if\n                temporary.\n                ', tunable_type=int, default=1, minimum=1), unlocked_tooltip=OptionalTunable(TunableLocalizedStringFactory(description='\n                A tooltip that will be shown on this Perk when unlocked so the\n                user knows when they will be able to buy it again. No expected\n                arguments.\n                ')))), 'display_name': TunableLocalizedStringFactory(description="\n            This Perk's display name. No expected arguments.\n            ", tuning_group=GroupNames.UI), 'perk_description': TunableLocalizedStringFactory(description='\n            The description for this Perk. No expected arguments.\n            ', tuning_group=GroupNames.UI), 'icon': TunableIconFactory(tuning_group=GroupNames.UI)}

    @classmethod
    def _tuning_loaded_callback(cls):
        if cls.temporary_perk_information is not None and cls.linked_perks:
            logger.error("A Bucks Perk has been created that's both temporary and has linked Perks. This is not supported. {}", cls)

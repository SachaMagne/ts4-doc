from _collections import defaultdict
from contextlib import contextmanager
from protocolbuffers import Dialog_pb2, Consts_pb2
from bucks.bucks_enums import BucksType
from bucks.bucks_perk import BucksPerkTunables
from clock import interval_in_sim_hours
from distributor import shared_messages
from distributor.ops import SetBuckFunds
from distributor.rollback import ProtocolBufferRollback
from distributor.shared_messages import IconInfoData, create_icon_info_msg
from distributor.system import Distributor
from sims4.callback_utils import CallableList
import alarms
import clock
import services
import sims4
logger = sims4.log.Logger('Bucks', default_owner='tastle')

class BucksTracker:
    __qualname__ = 'BucksTracker'

    def __init__(self, household):
        self._household = household
        self._unlocked_perks = {}
        self._bucks = {}
        self._bucks_modified_callbacks = defaultdict(CallableList)
        self._perk_unlocked_callbacks = defaultdict(CallableList)
        self._active_perk_timers = {}
        self._inactive_perk_timers = {}
        for bucks_type in BucksType:
            self._unlocked_perks[bucks_type] = {}
            self._active_perk_timers[bucks_type] = {}
            self._inactive_perk_timers[bucks_type] = {}

    def add_bucks_modified_callback(self, bucks_type, callback):
        self._bucks_modified_callbacks[bucks_type].register(callback)

    def remove_bucks_modified_callback(self, bucks_type, callback):
        self._bucks_modified_callbacks[bucks_type].unregister(callback)

    def add_perk_unlocked_callback(self, bucks_type, callback):
        self._perk_unlocked_callbacks[bucks_type].register(callback)

    def remove_perk_unlocked_callback(self, bucks_type, callback):
        self._perk_unlocked_callbacks[bucks_type].unregister(callback)

    def is_perk_unlocked(self, perk):
        return perk in self._unlocked_perks[perk.associated_bucks_type]

    def unlock_perk(self, perk, unlocked_by=None):
        if perk.rewards:
            dummy_sim = next(self._household.sim_info_gen(), None)
            if dummy_sim is None:
                logger.error('Trying to unlock a Perk for household {}, but there are no Sims.', self._household)
                return
            for reward in perk.rewards:
                reward().open_reward(dummy_sim, True)
        if not (perk.temporary_perk_information is not None and self._set_up_temporary_perk_timer(perk)):
            return
        self._perk_unlocked_callbacks[perk.associated_bucks_type](perk)
        self._unlocked_perks[perk.associated_bucks_type][perk] = unlocked_by
        for linked_perk in perk.linked_perks:
            while not self.is_perk_unlocked(linked_perk):
                self.unlock_perk(linked_perk, unlocked_by=perk)

    def pay_for_and_unlock_perk(self, perk):
        if self.is_perk_unlocked(perk):
            logger.error('Attempting to unlock a Perk {} for household {} that has already been unlocked.', perk, self._household)
            return
        if not self.try_modify_bucks(perk.associated_bucks_type, -perk.unlock_cost):
            logger.error('Attempting to unlock a Perk {} for household {} that they cannot afford.', perk, self._household)
            return
        self.unlock_perk(perk)

    def lock_perk(self, perk):
        if not self.is_perk_unlocked(perk):
            logger.error('Attempting to lock a Perk {} for household {} that has not been unlocked.', perk, self._household)
            return
        if perk.temporary_perk_information is not None:
            self.deactivate_temporary_perk_timer(perk)
        del self._unlocked_perks[perk.associated_bucks_type][perk]

    def activate_stored_temporary_perk_timers_of_type(self, bucks_type):
        if bucks_type not in self._inactive_perk_timers:
            return
        for (perk, remaining_ticks) in list(self._inactive_perk_timers[bucks_type].items()):
            self._set_up_temporary_perk_timer(perk, remaining_ticks=remaining_ticks)
            del self._inactive_perk_timers[bucks_type][perk]

    def deactivate_all_temporary_perk_timers_of_type(self, bucks_type):
        if bucks_type not in self._active_perk_timers:
            return
        for perk in list(self._active_perk_timers[bucks_type]):
            self.deactivate_temporary_perk_timer(perk)

    def deactivate_temporary_perk_timer(self, perk):
        perk_timer_handle = self._active_perk_timers[perk.associated_bucks_type][perk]
        if perk_timer_handle is not None:
            current_time = services.time_service().sim_now
            remaining_ticks = (perk_timer_handle.finishing_time - current_time).in_ticks()
            if remaining_ticks > 0:
                self._inactive_perk_timers[perk.associated_bucks_type][perk] = remaining_ticks
            perk_timer_handle.cancel()
        del self._active_perk_timers[perk.associated_bucks_type][perk]

    def _set_up_temporary_perk_timer(self, perk, remaining_ticks=None):
        if perk.temporary_perk_information is None:
            logger.error('Attempting to setup and alarm for a Perk that is not temporary. {}', perk)
            return False
        if perk in self._active_perk_timers[perk.associated_bucks_type]:
            logger.error('Attempting to add a timer for a temporary Perk that arleady has a timer set up. {}', perk)
            return False
        if remaining_ticks is None:
            time_until_perk_lock = interval_in_sim_hours(perk.temporary_perk_information.duration)
        else:
            time_until_perk_lock = clock.TimeSpan(remaining_ticks)
        perk_timer_handle = alarms.add_alarm(self, time_until_perk_lock, lambda _: self.lock_perk(perk))
        self._active_perk_timers[perk.associated_bucks_type][perk] = perk_timer_handle
        return True

    def all_perks_of_type_gen(self, bucks_type):
        perks_instance_manager = services.get_instance_manager(sims4.resources.Types.BUCKS_PERK)
        for perk in perks_instance_manager.types.values():
            while perk.associated_bucks_type is bucks_type:
                yield perk

    def get_disabled_tooltip_for_perk(self, perk):
        if perk.temporary_perk_information is not None:
            if perk.temporary_perk_information.unlocked_tooltip is not None:
                return perk.temporary_perk_information.unlocked_tooltip()
            return
        unlocked_by = self._unlocked_perks[perk.associated_bucks_type][perk]
        if unlocked_by is not None:
            return BucksPerkTunables.LINKED_PERK_UNLOCKED_TOOLTIP(unlocked_by.display_name())
        return BucksPerkTunables.PERK_UNLOCKED_TOOLTIP()

    def send_perks_list_for_bucks_type(self, bucks_type):
        bucks_msg = Dialog_pb2.GameplayPerkList()
        bucks_msg.bucks_type = bucks_type
        for perk in self.all_perks_of_type_gen(bucks_type):
            with ProtocolBufferRollback(bucks_msg.perk_list) as perk_list:
                perk_list.id = perk.guid64
                perk_list.display_name = perk.display_name()
                perk_list.description = perk.perk_description()
                perk_list.icon = create_icon_info_msg(IconInfoData(icon_resource=perk.icon.key))
                perk_list.cost = perk.unlock_cost
                perk_list.affordable = self._bucks[bucks_type] >= perk.unlock_cost
                unlocked = self.is_perk_unlocked(perk)
                perk_list.purchased = unlocked
                while unlocked:
                    disabled_tooltip = self.get_disabled_tooltip_for_perk(perk)
                    while disabled_tooltip is not None:
                        perk_list.disabled_tooltip = disabled_tooltip
        op = shared_messages.create_message_op(bucks_msg, Consts_pb2.MSG_GAMEPLAY_PERK_LIST)
        Distributor.instance().add_op_with_no_owner(op)

    def on_all_households_and_sim_infos_loaded(self):
        if not self._household.id == services.active_household_id():
            return
        for bucks_type in self._bucks.keys():
            self.try_modify_bucks(bucks_type, 0)

    def _distribute_bucks(self, bucks_type):
        op = SetBuckFunds(bucks_type, self._bucks[bucks_type])
        Distributor.instance().add_op_with_no_owner(op)

    def try_modify_bucks(self, bucks_type, amount, distribute=True):
        if bucks_type in self._bucks:
            new_amount = self._bucks[bucks_type] + amount
        else:
            new_amount = amount
        if new_amount < 0:
            return False
        self._bucks[bucks_type] = new_amount
        self._bucks_modified_callbacks[bucks_type]()
        if distribute:
            self._distribute_bucks(bucks_type)
        return True

    def load_data(self, household_proto):
        bucks_perk_manager = services.get_instance_manager(sims4.resources.Types.BUCKS_PERK)
        for bucks_data in household_proto.gameplay_data.bucks_data:
            self.try_modify_bucks(bucks_data.bucks_type, bucks_data.amount, distribute=False)
            for perk_data in bucks_data.unlocked_perks:
                perk_ref = bucks_perk_manager.get(perk_data.perk)
                if perk_ref is None:
                    logger.info('Trying to load unavailable BUCKS_PERK resource: {}', perk_data.perk)
                unlocked_by = bucks_perk_manager.get(perk_data.unlock_reason)
                self._unlocked_perks[perk_ref.associated_bucks_type][perk_ref] = unlocked_by
                while perk_data.time_left:
                    self._set_up_temporary_perk_timer(perk_ref, perk_data.time_left)

    def save_data(self, household_msg):
        for bucks_type in BucksType:
            with self._deactivate_perk_timers(bucks_type), ProtocolBufferRollback(household_msg.gameplay_data.bucks_data) as bucks_data:
                bucks_data.bucks_type = bucks_type
                bucks_data.amount = self._bucks.get(bucks_type, 0)
                for (perk, unlocked_by) in self._unlocked_perks[bucks_type].items():
                    with ProtocolBufferRollback(bucks_data.unlocked_perks) as unlocked_perks:
                        unlocked_perks.perk = perk.guid64
                        if unlocked_by is not None:
                            unlocked_perks.unlock_reason = unlocked_by.guid64
                        while perk in self._inactive_perk_timers[bucks_type]:
                            unlocked_perks.time_left = self._inactive_perk_timers[bucks_type][perk]

    @contextmanager
    def _deactivate_perk_timers(self, bucks_type):
        if self._active_perk_timers[bucks_type]:
            if self._inactive_perk_timers[bucks_type]:
                logger.error('Household {} has both active and inactive temporary Perk timers. This is not expected and will cause save/load issues.', self._household)
            self.deactivate_all_temporary_perk_timers_of_type(bucks_type)
            had_active_timers = True
        else:
            had_active_timers = False
        try:
            yield None
        finally:
            if had_active_timers:
                self.activate_stored_temporary_perk_timers_of_type(bucks_type)

from cas.cas import set_caspart, get_caspart_bodytype, randomize_part_color, randomize_skintone_from_tags
from sims.outfits.outfit_enums import BodyType
from sims4.repr_utils import standard_repr
from sims4.tuning.dynamic_enum import DynamicEnum
from sims4.tuning.tunable import HasTunableSingletonFactory, AutoFactoryInit, TunableCasPart, TunableEnumEntry, TunableList, TunableVariant, Tunable
import enum
import sims4
import tag
logger = sims4.log.Logger('Appearance')

class AppearanceModifierType(enum.Int):
    __qualname__ = 'AppearanceModifierType'
    SET_CAS_PART = 0
    RANDOMIZE_BODY_TYPE_COLOR = 1
    RANDOMIZE_SKINTONE_FROM_TAGS = 2

class AppearanceModifierPriority(DynamicEnum):
    __qualname__ = 'AppearanceModifierPriority'
    INVALID = 0

class AppearanceModifier(HasTunableSingletonFactory, AutoFactoryInit):
    __qualname__ = 'AppearanceModifier'

    class BaseAppearanceModification(HasTunableSingletonFactory, AutoFactoryInit):
        __qualname__ = 'AppearanceModifier.BaseAppearanceModification'
        FACTORY_TUNABLES = {'_is_combinable_with_same_type': Tunable(description='\n                True if this modifier type is able to be combined with another\n                of its type. If True, and two modifiers conflict, then the tuned\n                priority will be used to resolve the conflict. If False, only\n                a single modifier of this type with the highest priority will be shown.\n                ', tunable_type=bool, default=True)}

        def modify_sim_info(self, source_sim_info, modified_sim_info, random_seed):
            raise NotImplementedError('Attempting to use the BaseAppearanceModification base class, use sub-classes instead.')

        @property
        def is_permanent_modification(self):
            return False

        @property
        def modifier_type(self):
            raise NotImplementedError('Attempting to use the BaseAppearanceModification base class, use sub-classes instead.')

        @property
        def is_combinable_with_same_type(self):
            return self._is_combinable_with_same_type

        @property
        def combinable_sorting_key(self):
            raise NotImplementedError('Attempting to use the BaseAppearanceModification base class, use sub-classes instead.')

    class SetCASPart(BaseAppearanceModification):
        __qualname__ = 'AppearanceModifier.SetCASPart'
        FACTORY_TUNABLES = {'cas_part': TunableCasPart(description='\n                The CAS part that will be modified.\n                '), 'should_toggle': Tunable(description="\n                Whether or not to toggle this part. e.g. if it exists, remove\n                it, if it doesn't exist, add it. If set to false, the part will\n                be added if it doesn't exist, but not removed if it does exist.\n                ", tunable_type=bool, default=False), 'replace_with_random': Tunable(description='\n                Whether or not to replace the tuned cas part with a random\n                variant.\n                ', tunable_type=bool, default=False), 'update_genetics': Tunable(description='\n                Whether or not to update the genetics of the sim with this\n                modification to make it a permanent modification. NOTE: DO NOT\n                tune permanent with temporary modifications on the same\n                appearance modifier.\n                ', tunable_type=bool, default=False)}

        def modify_sim_info(self, source_sim_info, modified_sim_info, random_seed):
            set_caspart(source_sim_info._base, modified_sim_info._base, self.cas_part, self.should_toggle, self.replace_with_random, self.update_genetics, random_seed)

        @property
        def is_permanent_modification(self):
            return self.update_genetics

        @property
        def modifier_type(self):
            return AppearanceModifierType.SET_CAS_PART

        @property
        def combinable_sorting_key(self):
            return get_caspart_bodytype(self.cas_part)

        def __repr__(self):
            return standard_repr(self, cas_part=self.cas_part, should_toggle=self.should_toggle, replace_with_random=self.replace_with_random, update_genetics=self.update_genetics)

    class RandomizeBodyTypeColor(BaseAppearanceModification):
        __qualname__ = 'AppearanceModifier.RandomizeBodyTypeColor'
        FACTORY_TUNABLES = {'body_type': TunableEnumEntry(description='\n                The body type that will have its color randomized.\n                ', tunable_type=BodyType, default=BodyType.NONE)}

        def modify_sim_info(self, source_sim_info, modified_sim_info, random_seed):
            randomize_part_color(source_sim_info._base, modified_sim_info._base, self.body_type, random_seed)
            return False

        @property
        def modifier_type(self):
            return AppearanceModifierType.RANDOMIZE_BODY_TYPE_COLOR

        @property
        def combinable_sorting_key(self):
            return self.body_type

        def __repr__(self):
            return standard_repr(self, body_type=self.body_type)

    class RandomizeSkintoneFromTags(BaseAppearanceModification):
        __qualname__ = 'AppearanceModifier.RandomizeSkintoneFromTags'
        FACTORY_TUNABLES = {'tag_list': TunableList(TunableEnumEntry(description='\n                    A specific tag.\n                    ', tunable_type=tag.Tag, default=tag.Tag.INVALID)), 'locked_args': {'_is_combinable_with_same_type': False}}

        def modify_sim_info(self, source_sim_info, modified_sim_info, random_seed):
            randomize_skintone_from_tags(source_sim_info._base, modified_sim_info._base, list(self.tag_list), random_seed)
            return False

        @property
        def modifier_type(self):
            return AppearanceModifierType.RANDOMIZE_SKINTONE_FROM_TAGS

        def __repr__(self):
            return standard_repr(self, tag_list=self.tag_list)

    @staticmethod
    def _verify_tunable_callback(instance_class, tunable_name, source, value, **kwargs):
        is_permanent_modification = None
        for tuned_modifier in value.appearance_modifiers:
            if is_permanent_modification is None:
                is_permanent_modification = tuned_modifier.is_permanent_modification
            else:
                while is_permanent_modification != tuned_modifier.is_permanent_modification:
                    logger.error('An appearance modifier is attempting to combine a permanent\n                                    modifier with a temporary one. This is not supported.', owner='jwilkinson')
                    return

    FACTORY_TUNABLES = {'priority': TunableEnumEntry(description='\n            The priority of the appearance request. Higher priority will\n            take precedence over lower priority. Equal priority will favor\n            recent requests.\n            ', tunable_type=AppearanceModifierPriority, default=AppearanceModifierPriority.INVALID), 'appearance_modifiers': TunableList(description='\n            The specific appearance modifiers to use for this buff.\n            ', tunable=TunableVariant(set_cas_part=SetCASPart.TunableFactory(), randomize_body_type_color=RandomizeBodyTypeColor.TunableFactory(), randomize_skintone_between_tags=RandomizeSkintoneFromTags.TunableFactory(), default='set_cas_part')), 'apply_to_all_outfits': Tunable(description='\n            If checked, the appearance modifiers will be applied to all outfits,\n            otherwise they will only be applied to the current outfit.\n            ', tunable_type=bool, default=True), 'verify_tunable_callback': _verify_tunable_callback}

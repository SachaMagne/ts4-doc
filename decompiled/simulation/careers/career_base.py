from collections import namedtuple
from contextlib import contextmanager
from weakref import WeakKeyDictionary
import itertools
import math
import operator
import random
import weakref
from protocolbuffers import Consts_pb2, DistributorOps_pb2, SimObjectAttributes_pb2
from protocolbuffers.Dialog_pb2 import UiCareerNotificationArgs
from protocolbuffers.DistributorOps_pb2 import Operation
from audio.primitive import play_tunable_audio
from careers import career_ops
from careers.career_event_manager import CareerEventManager
from careers.career_mixins import CareerKnowledgeMixin
from careers.career_ops import CareerTimeOffReason
from careers.career_scheduler import get_career_schedule_for_level
from careers.coworker import CoworkerMixin
from careers.retirement import Retirement
from clock import interval_in_sim_minutes
from date_and_time import create_time_span, DateAndTime, TimeSpan, DATE_AND_TIME_ZERO, MINUTES_PER_HOUR, date_and_time_from_week_time
from distributor.ops import GenericProtocolBufferOp, EndOfWorkday
from distributor.rollback import ProtocolBufferRollback
from distributor.system import Distributor
from event_testing import test_events
from event_testing.resolver import SingleSimResolver, DoubleSimResolver
from event_testing.results import EnqueueResult
from interactions.aop import AffordanceObjectPair
from interactions.context import QueueInsertStrategy
from objects import ALL_HIDDEN_REASONS, HiddenReasonFlag
from sims.sickness_tuning import SicknessTuning
from sims.sim_outfits import OutfitChangeReason
from sims.sim_spawner_service import SimSpawnRequest, SimSpawnPointStrategy, SimSpawnReason
from sims4.callback_utils import CallableList
from sims4.localization import LocalizationHelperTuning
from sims4.math import Threshold, clamp, EPSILON
from sims4.protocol_buffer_utils import has_field
from sims4.utils import flexmethod
from singletons import DEFAULT
from ui.ui_dialog import UiDialogResponse
from world.spawn_point import SpawnPointOption, SpawnPoint
import alarms
import clock
import date_and_time
import distributor.system
import enum
import interactions.context
import services
import sims
import sims4.log
import sims4.math
import telemetry_helper
TELEMETRY_GROUP_CAREERS = 'CARE'
TELEMETRY_HOOK_CAREER_START = 'CAST'
TELEMETRY_HOOK_CAREER_END = 'CAEN'
TELEMETRY_HOOK_CAREER_PROMOTION = 'CAUP'
TELEMETRY_HOOK_CAREER_DEMOTION = 'CADW'
TELEMETRY_HOOK_CAREER_DAILY_END = 'CADA'
TELEMETRY_HOOK_CAREER_OVERMAX = 'CAOM'
TELEMETRY_HOOK_CAREER_LEAVE_EARLY = 'CALE'
TELEMETRY_HOOK_CAREER_EVENT_END = 'CEND'
TELEMETRY_CAREER_ID = 'caid'
TELEMETRY_CAREER_LEVEL = 'leve'
TELEMETRY_CAREER_DAILY_PERFORMANCE = 'poin'
TELEMETRY_TRACK_ID = 'trid'
TELEMETRY_TRACK_LEVEL = 'trlv'
TELEMETRY_TRACK_OVERMAX = 'trom'
TELEMETRY_CAREER_EVENT_MEDAL = 'ceme'
TELEMETRY_CAREER_EVENT_GOALS = 'cego'
career_telemetry_writer = sims4.telemetry.TelemetryWriter(TELEMETRY_GROUP_CAREERS)
logger = sims4.log.Logger('Careers', default_owner='tingyul')
NO_TIME_DIFFERENCE = date_and_time.TimeSpan.ZERO
PERCENT_FLOAT_CONVERSION = 0.01
PTO_RESPONSE_ID = 0
with sims4.reload.protected(globals()):
    _career_event_overrides = WeakKeyDictionary()

def set_career_event_override(sim_info, career_event):
    _career_event_overrides[sim_info] = career_event

class Evaluation(enum.Int, export=False):
    __qualname__ = 'Evaluation'
    ON_TARGET = Ellipsis
    PROMOTED = Ellipsis
    DEMOTED = Ellipsis
    FIRED = Ellipsis

class EvaluationResult:
    __qualname__ = 'EvaluationResult'

    def __init__(self, evaluation, dialog_factory, *args, **kwargs):
        self.evaluation = evaluation
        self.dialog_factory = dialog_factory
        self.dialog_args = args
        self.dialog_kwargs = kwargs

    def display_dialog(self, career):
        if self.dialog_factory is not None:
            career.send_career_message(self.dialog_factory, *self.dialog_args, **self.dialog_kwargs)

class CareerBase(CoworkerMixin, CareerKnowledgeMixin, sims.sim_spawner_service.ISimSpawnerServiceCustomer):
    __qualname__ = 'CareerBase'
    TONE_STAT_MOD = 1

    def __init__(self, sim_info, init_track=False):
        self._sim_info = sim_info
        self.on_promoted = CallableList()
        self.on_demoted = CallableList()
        self.on_career_removed = CallableList()
        self._level = 0
        self._user_level = 1
        self._overmax_level = 0
        self._join_time = None
        self._career_location = self.career_location(self)
        self._current_work_start = None
        self._current_work_end = None
        self._current_work_duration = None
        self._at_work = False
        self._requested_day_off_reason = career_ops.CareerTimeOffReason.NO_TIME_OFF
        self._taking_day_off_reason = career_ops.CareerTimeOffReason.NO_TIME_OFF
        self._current_track = None
        if init_track:
            self._current_track = self.start_track
        self._auto_work = False
        self._pending_promotion = False
        self._work_scheduler = None
        self._end_work_handle = None
        self._late_for_work_handle = None
        self._interaction = None
        self._statistic_down_listeners = {}
        self._statistic_up_listeners = {}
        self._career_event_manager = None
        self._career_session_extended = False
        self._has_attended_first_day = False
        self._career_event_cooldown_map = {}
        self._should_restore_career_state = True

    def __repr__(self):
        return '{} on {}'.format(type(self).__name__, self._get_sim())

    def _get_sim(self):
        return self._sim_info.get_sim_instance(allow_hidden_flags=ALL_HIDDEN_REASONS)

    @property
    def sim_info(self):
        return self._sim_info

    @property
    def career_event_manager(self):
        return self._career_event_manager

    @property
    def current_level_tuning(self):
        return self._current_track.career_levels[self._level]

    @property
    def next_level_tuning(self):
        next_level = self._level + 1
        if next_level < len(self._current_track.career_levels):
            return self._current_track.career_levels[next_level]

    @property
    def previous_level_tuning(self):
        previous_level = self._level - 1
        if previous_level >= 0:
            return self._current_track.career_levels[previous_level]

    @property
    def current_track_tuning(self):
        return self._current_track

    @property
    def level(self):
        return self._level

    @property
    def user_level(self):
        return self._user_level

    @property
    def overmax_level(self):
        return self._overmax_level

    @property
    def has_attended_first_day(self):
        return self._has_attended_first_day

    @property
    def days_worked_statistic(self):
        return self._sim_info.get_statistic(self._days_worked_statistic_type)

    @property
    def active_days_worked_statistic(self):
        return self._sim_info.get_statistic(self._active_days_worked_statistic_type)

    @property
    def join_time(self):
        return self._join_time

    @property
    def work_performance(self):
        performance_stat = self._sim_info.statistic_tracker.get_statistic(self.current_level_tuning.performance_stat)
        if performance_stat is not None:
            return performance_stat.get_value()
        return 0

    @property
    def work_performance_stat(self):
        performance_stat = self._sim_info.statistic_tracker.get_statistic(self.current_level_tuning.performance_stat)
        if performance_stat is not None:
            return performance_stat
        logger.error('Career: Missing work performance stat. Sim:{} Career: {}', self._sim_info, self, owner='tingyul')

    @property
    def pto_commodity_instance(self):
        return self._sim_info.get_statistic(self.pto_commodity, add=True)

    @property
    def pto(self):
        pto_commodity_instance = self.pto_commodity_instance
        if pto_commodity_instance is not None:
            return int(pto_commodity_instance.get_value())
        return 0

    @property
    def requested_day_off_reason(self):
        return self._requested_day_off_reason

    @property
    def requested_day_off(self):
        return self._requested_day_off_reason is not career_ops.CareerTimeOffReason.NO_TIME_OFF

    @property
    def taking_day_off_reason(self):
        return self._taking_day_off_reason

    @property
    def taking_day_off(self):
        return self._taking_day_off_reason is not career_ops.CareerTimeOffReason.NO_TIME_OFF

    @property
    def currently_at_work(self):
        return self._at_work

    @property
    def is_at_active_event(self):
        return self._career_event_manager is not None

    @property
    def is_late(self):
        if not self.is_work_time or self.currently_at_work:
            return False
        late_time = self.get_late_time()
        if late_time is None:
            return False
        now = services.time_service().sim_now
        if now < late_time:
            return False
        return True

    @property
    def is_work_time(self):
        if self._current_work_start is None:
            return False
        current_time = services.time_service().sim_now
        if current_time.time_between_week_times(self._current_work_start, self._current_work_end):
            return True
        return False

    def is_time_during_shift(self, time=DEFAULT):
        if self._work_scheduler is None:
            return True
        time = services.time_service().sim_now if time is DEFAULT else time
        for (start_time, end_time) in self._work_scheduler.get_schedule_entries():
            while time.time_between_week_times(start_time, end_time):
                return True
        return False

    @property
    def start_time(self):
        return self._current_work_start

    def get_late_time(self):
        if not self.is_work_time:
            return
        time_before_late = self._current_work_duration*(1 - self.current_level_tuning.performance_metrics.full_work_day_percent*PERCENT_FLOAT_CONVERSION)
        late_time = self._current_work_start + time_before_late
        return late_time

    @flexmethod
    def is_valid_career(cls, inst, sim_info=DEFAULT, from_join=False):
        if not (inst is not None and inst._career_location.is_valid_career_location()):
            return False
        inst_or_cls = inst if inst is not None else cls
        sim_info = inst_or_cls.sim_info if sim_info is DEFAULT else sim_info
        if from_join or any(career.guid64 != inst_or_cls.guid64 and career.career_category == inst_or_cls.career_category for career in sim_info.career_tracker):
            return False
        resolver = SingleSimResolver(sim_info)
        return inst_or_cls.career_availablity_tests.run_tests(resolver)

    def _populate_work_state_time_off_msg(self, msg, reason):
        reason_text = self.career_messages.career_time_off_messages[reason]
        if reason_text is not None:
            panel_string = reason_text.text
            tooltip_string = reason_text.tooltip
            pto = self.pto
            if panel_string is not None:
                msg.panel_string = panel_string(self._sim_info, pto)
            if tooltip_string is not None:
                msg.tooltip_string = tooltip_string(self._sim_info, pto)
        msg.work_state = DistributorOps_pb2.SetAtWorkInfo.WORKDAY_TIME_OFF

    def create_work_state_msg(self):
        msg = DistributorOps_pb2.SetAtWorkInfo()
        msg.career_uid = self.guid64
        if self.is_work_time:
            if self.taking_day_off:
                self._populate_work_state_time_off_msg(msg, self.taking_day_off_reason)
            elif self.currently_at_work:
                if self._career_session_extended:
                    msg.end_time_override = self._current_work_end.absolute_ticks()
                msg.work_state = DistributorOps_pb2.SetAtWorkInfo.WORKDAY_ATTENDING
            elif self.is_late:
                msg.work_state = DistributorOps_pb2.SetAtWorkInfo.WORKDAY_LATE
            else:
                msg.work_state = DistributorOps_pb2.SetAtWorkInfo.WORKDAY_AVAILABLE
        elif self.requested_day_off:
            self._populate_work_state_time_off_msg(msg, self.requested_day_off_reason)
        elif self._sim_info.is_in_travel_group():
            if self.pto > 0:
                self._populate_work_state_time_off_msg(msg, career_ops.CareerTimeOffReason.PTO)
            else:
                self._populate_work_state_time_off_msg(msg, career_ops.CareerTimeOffReason.MISSING_WORK)
        else:
            msg.work_state = DistributorOps_pb2.SetAtWorkInfo.WORKDAY_OVER
        return msg

    @property
    def auto_work(self):
        return self._auto_work

    def request_day_off(self, reason):
        if self.is_work_time and not self.is_late:
            self._taking_day_off_reason = reason
            self.resend_at_work_info()
            return
        self._requested_day_off_reason = reason
        self.resend_career_data()
        self.resend_at_work_info()

    def _add_pto(self, days):
        initial_pto_value = self.pto
        self.pto_commodity_instance.add_value(days)
        final_pto_value = self.pto
        delta = final_pto_value - initial_pto_value
        return delta

    def resend_career_data(self):
        self._sim_info.career_tracker.resend_career_data()

    def resend_at_work_info(self):
        self._sim_info.career_tracker.resend_at_work_infos()
        client = services.client_manager().get_client_by_household_id(self._sim_info._household_id)
        if client is not None:
            client.selectable_sims.notify_dirty()

    def get_career_entry_data(self, career_history=None, user_level_override=None):
        resolver = SingleSimResolver(self._sim_info)
        if user_level_override is not None:
            (new_level, new_user_level, current_track) = self.get_career_entry_level_from_user_level(user_level_override)
        else:
            (new_level, new_user_level, current_track) = self.get_career_entry_level(career_history=career_history, resolver=resolver)
        return (new_level, new_user_level, current_track)

    def get_career_entry_level_from_user_level(self, desired_user_level):
        track = self.start_track
        track_start_level = 1
        while True:
            track_length = len(track.career_levels)
            level = desired_user_level - track_start_level
            if level < track_length:
                user_level = track_start_level + level
                return (level, user_level, track)
            if not track.branches:
                level = track_length - 1
                user_level = track_start_level + level
                return (level, user_level, track)
            track_start_level += track_length
            track = random.choice(track.branches)
            track_length = len(track.career_levels)

    def get_career_entry_level(self, career_history=None, resolver=None):
        if career_history is None or self.guid64 not in career_history:
            level = int(self.start_level_modifiers.get_max_modifier(resolver))
            max_level = len(self.start_track.career_levels)
            level = sims4.math.clamp(0, level, max_level - 1)
            return (level, level + 1, None)
        history = career_history[self.guid64]
        new_level = history.level
        new_user_level = history.user_level
        time_of_leave = history.time_of_leave
        current_track = history.career_track
        new_level -= self.levels_lost_on_leave
        new_user_level -= self.levels_lost_on_leave
        current_time = services.time_service().sim_now
        time_gone_from_career = current_time - time_of_leave
        days_gone_from_career = time_gone_from_career.in_days()
        if self.days_to_level_loss > 0:
            levels_to_lose = int(days_gone_from_career/self.days_to_level_loss)
            new_level -= levels_to_lose
            new_user_level -= levels_to_lose
        if new_level < 0:
            new_level = 0
        if new_user_level < 1:
            new_user_level = 1
        return (new_level, new_user_level, current_track)

    def get_next_wakeup_time(self) -> date_and_time.DateAndTime:
        return self.current_level_tuning.wakeup_time

    def get_next_work_time(self, offset_time=None, check_if_can_go_now=False, consider_skipped_shifts=True):
        if self._work_scheduler is None:
            return (None, None, None)
        now = services.time_service().sim_now
        if offset_time:
            now += offset_time
        (best_time, work_data_list) = self._work_scheduler.time_until_next_scheduled_event(now, schedule_immediate=check_if_can_go_now)
        work_data = work_data_list[0]
        start_time = now + best_time
        if self.requested_day_off:
            valid_start_time = start_time + TimeSpan.ONE
        else:
            valid_start_time = self.get_valid_first_work_day_time()
        if consider_skipped_shifts and start_time < valid_start_time:
            (best_time, work_data_list) = self._work_scheduler.time_until_next_scheduled_event(valid_start_time, schedule_immediate=False)
            best_time += valid_start_time - now
            work_data = work_data_list[0]
            start_time = now + best_time
        end_time = now.time_of_next_week_time(work_data.end_time)
        return (best_time, start_time, end_time)

    def get_valid_first_work_day_time(self):
        if self._join_time is None:
            return DATE_AND_TIME_ZERO
        return self._join_time + clock.interval_in_sim_minutes(self.initial_delay)

    def should_skip_next_shift(self, check_if_can_go_now=False):
        if self.requested_day_off:
            return True
        (_, start, _) = self.get_next_work_time(check_if_can_go_now=check_if_can_go_now, consider_skipped_shifts=False)
        if start is None:
            return False
        if start > self.get_valid_first_work_day_time():
            return False
        return True

    def get_is_school(self):
        return isinstance(self, self._sim_info.SCHOOL_CAREER) or isinstance(self, self._sim_info.HIGH_SCHOOL_CAREER)

    def get_career_location(self):
        return self._career_location

    def get_company_name(self):
        return self._career_location.get_company_name()

    def _give_rewards_for_skipped_levels(self):
        level_of_last_reward = self._sim_info.career_tracker.get_highest_level_reached(self.guid64)
        track = self.current_track_tuning
        level = self.level
        user_level = self.user_level
        while user_level > level_of_last_reward:
            reward = track.career_levels[level].promotion_reward
            if reward is not None:
                reward.give_reward(self._sim_info)
            user_level -= 1
            level -= 1
            while level < 0:
                if track.parent_track is None:
                    break
                track = track.parent_track
                level = len(track.career_levels) - 1
                continue

    def join_career(self, career_history=None, user_level_override=None, give_skipped_rewards=True):
        (new_level, new_user_level, current_track) = self.get_career_entry_data(career_history=career_history, user_level_override=user_level_override)
        self._level = new_level
        self._user_level = new_user_level
        if current_track is None:
            self._current_track = self.start_track
        else:
            self._current_track = current_track
        self._join_time = services.time_service().sim_now
        self._reset_career_objectives(self._current_track, new_level)
        self.career_start()
        loot = self.current_level_tuning.loot_on_join
        if loot is not None:
            resolver = SingleSimResolver(self._sim_info)
            loot.apply_to_resolver(resolver)
        if give_skipped_rewards:
            self._give_rewards_for_skipped_levels()
        self._send_telemetry(TELEMETRY_HOOK_CAREER_START)
        if not self.get_is_school():
            (_, first_work_time, _) = self.get_next_work_time()
            self.send_career_message(self.career_messages.join_career_notification, first_work_time)
        self.add_coworker_relationship_bit()
        self._add_career_knowledge()

    def career_start(self, is_load=False, is_level_change=False):
        if self.career_messages.career_early_warning_time is not None:
            early_warning_time_span = date_and_time.create_time_span(hours=self.career_messages.career_early_warning_time)
        else:
            early_warning_time_span = None
        schedule_immediate = False if self.initial_delay else True
        self._work_scheduler = self.current_level_tuning.schedule(self, start_callback=self._start_work_callback, schedule_immediate=schedule_immediate, early_warning_callback=self._early_warning_callback, early_warning_time_span=early_warning_time_span)
        self._add_performance_statistics()
        if is_load:
            self.restore_career_session()
            self.restore_tones()
        else:
            if self.current_level_tuning.work_outfit.outfit_tags and (self.current_level_tuning.work_outfit.generate_on_level_change or not is_level_change):
                self._sim_info.generate_career_outfit(tag_list=list(self.current_level_tuning.work_outfit.outfit_tags))
            sim = self._get_sim()
            if sim is not None:
                sim.update_sleep_schedule()
            services.get_event_manager().process_event(test_events.TestEvent.CareerEvent, sim_info=self._sim_info, career=self)
        self._add_statistic_metric_listeners()

    def career_stop(self):
        if self._work_scheduler is not None:
            self._work_scheduler.destroy()
            self._work_scheduler = None
        if self._end_work_handle is not None:
            alarms.cancel_alarm(self._end_work_handle)
            self._end_work_handle = None
        if self._late_for_work_handle is not None:
            alarms.cancel_alarm(self._late_for_work_handle)
            self._late_for_work_handle = None
        self._remove_performance_statistics()
        self._remove_statistic_metric_listeners()

    def _add_performance_statistics(self):
        tuning = self.current_level_tuning
        sim_info = self._sim_info
        tracker = sim_info.get_tracker(tuning.performance_stat)
        tracker.add_statistic(tuning.performance_stat)
        for metric in tuning.performance_metrics.statistic_metrics:
            tracker = sim_info.get_tracker(metric.statistic)
            tracker.add_statistic(metric.statistic)

    def _remove_performance_statistics(self):
        tuning = self.current_level_tuning
        sim_info = self._sim_info
        sim_info.remove_statistic(tuning.performance_stat)
        for metric in tuning.performance_metrics.statistic_metrics:
            sim_info.remove_statistic(metric.statistic)

    def _remove_pto_commodity(self):
        sim_info = self._sim_info
        sim_info.remove_statistic(self.pto_commodity)

    def _reset_performance_statistics(self):
        tuning = self.current_level_tuning
        sim_info = self._sim_info
        sim_info.remove_statistic(self.WORK_SESSION_PERFORMANCE_CHANGE)
        for metric in tuning.performance_metrics.statistic_metrics:
            while metric.reset_at_end_of_work:
                tracker = sim_info.get_tracker(metric.statistic)
                tracker.set_value(metric.statistic, metric.statistic.initial_value)

    def _on_statistic_metric_changed(self, stat_type):
        self.resend_career_data()
        self._refresh_statistic_metric_listeners()

    def _add_statistic_metric_listeners(self):
        metrics = self.current_level_tuning.performance_metrics
        for metric in metrics.statistic_metrics:
            tracker = self._sim_info.get_tracker(metric.statistic)
            value = tracker.get_value(metric.statistic)
            (lower_threshold, upper_threshold) = self._get_statistic_progress_thresholds(metric.statistic, value)
            if lower_threshold:
                threshold = Threshold(lower_threshold.threshold, operator.lt)
                handle = tracker.create_and_add_listener(metric.statistic, threshold, self._on_statistic_metric_changed)
                self._statistic_down_listeners[metric.statistic] = handle
            while upper_threshold:
                threshold = Threshold(upper_threshold.threshold, operator.ge)
                handle = tracker.create_and_add_listener(metric.statistic, threshold, self._on_statistic_metric_changed)
                self._statistic_up_listeners[metric.statistic] = handle

    def _remove_statistic_metric_listeners(self):
        for (stat_type, handle) in itertools.chain(self._statistic_down_listeners.items(), self._statistic_up_listeners.items()):
            tracker = self._sim_info.get_tracker(stat_type)
            tracker.remove_listener(handle)
        self._statistic_down_listeners = {}
        self._statistic_up_listeners = {}

    def _refresh_statistic_metric_listeners(self):
        self._remove_statistic_metric_listeners()
        self._add_statistic_metric_listeners()

    def _get_performance_tooltip(self):
        loc_strings = []
        metrics = self.current_level_tuning.performance_metrics
        if metrics.performance_tooltip is not None:
            loc_strings.append(metrics.performance_tooltip)
        for metric in metrics.statistic_metrics:
            text = metric.tooltip_text
            while text is not None:
                if text.general_description:
                    lower_threshold = None
                    stat = self._sim_info.get_statistic(metric.statistic)
                    if stat is not None:
                        (lower_threshold, _) = self._get_statistic_progress_thresholds(stat.stat_type, stat.get_value())
                    if lower_threshold:
                        description = text.general_description(lower_threshold.text)
                    else:
                        description = text.general_description()
                    loc_strings.append(description)
        if loc_strings:
            return LocalizationHelperTuning.get_new_line_separated_strings(*loc_strings)

    def _get_statistic_progress_thresholds(self, stat_type, value):
        metrics = self.current_level_tuning.performance_metrics
        for metric in metrics.statistic_metrics:
            while metric.statistic is stat_type:
                text = metric.tooltip_text
                if text is not None:
                    lower_threshold = None
                    upper_threshold = None
                    for threshold in text.thresholded_descriptions:
                        if value >= threshold.threshold and (lower_threshold is None or threshold.threshold > lower_threshold.threshold):
                            lower_threshold = threshold
                        while value < threshold.threshold and (upper_threshold is None or threshold.threshold < upper_threshold.threshold):
                            upper_threshold = threshold
                    return (lower_threshold, upper_threshold)
                break
        return (None, None)

    def apply_performance_change(self, time_elapsed, tone_multiplier):
        metrics = self.current_level_tuning.performance_metrics
        gain = 0
        loss = 0

        def add_metric(value):
            nonlocal gain, loss
            if value >= 0:
                gain += value
            else:
                loss -= value

        add_metric(metrics.base_performance)
        for commodity_metric in metrics.commodity_metrics:
            tracker = self._sim_info.get_tracker(commodity_metric.commodity)
            curr_value = tracker.get_user_value(commodity_metric.commodity)
            while curr_value is not None and commodity_metric.threshold.compare(curr_value):
                add_metric(commodity_metric.performance_mod)
        for mood_metric in metrics.mood_metrics:
            while self._sim_info.get_mood() is mood_metric.mood:
                add_metric(mood_metric.performance_mod)
                break
        for metric in metrics.statistic_metrics:
            while metric.performance_curve is not None:
                stat = self._sim_info.get_statistic(metric.statistic, add=False)
                if stat is not None:
                    stat_value = stat.get_value()
                    performance_mod = metric.performance_curve.get(stat_value)
                    add_metric(performance_mod)
        if metrics.tested_metrics:
            resolver = SingleSimResolver(self._sim_info)
            for metric in metrics.tested_metrics:
                while metric.tests.run_tests(resolver):
                    add_metric(metric.performance_mod)
        completed_objectives = 0
        promotion_milestone = self.current_level_tuning.aspiration
        if promotion_milestone is not None:
            for objective in promotion_milestone.objectives:
                while self._sim_info.aspiration_tracker.objective_completed(objective):
                    completed_objectives += 1
        objective_mod = completed_objectives*metrics.performance_per_completed_goal
        add_metric(objective_mod)
        total = gain*tone_multiplier - loss
        delta = total*time_elapsed.in_ticks()/self._current_work_duration.in_ticks()
        self.work_performance_stat.add_value(delta)
        session_stat = self._sim_info.statistic_tracker.get_statistic(self.WORK_SESSION_PERFORMANCE_CHANGE)
        session_stat.add_value(delta)

    def get_busy_time_periods(self):
        busy_times = []
        if self._work_scheduler is not None:
            busy_times.extend(self._work_scheduler.get_schedule_times())
            for time_period in self.current_level_tuning.additional_unavailable_times:
                start_time = time_period.start_time()
                end_time = start_time + clock.interval_in_sim_hours(time_period.period_duration)
                busy_times.append((start_time.absolute_ticks(), end_time.absolute_ticks()))
        return busy_times

    def _should_automatically_use_pto(self):
        return self._sim_info.is_in_travel_group() and self.pto > 0

    def _early_warning_callback(self):
        if self.should_skip_next_shift() or self._should_automatically_use_pto():
            return
        if SicknessTuning.is_child_sim_sick(self._sim_info):
            resolver = SingleSimResolver(self.sim_info)
            for sick_loot in SicknessTuning.LOOT_ACTIONS_ON_CHILD_CAREER_AUTO_SICK:
                sick_loot.apply_to_resolver(resolver)
            return
        if self._sim_info.is_selectable and self.career_messages.career_early_warning_notification is not None:
            self.send_career_message(self.career_messages.career_early_warning_notification)

    def _career_missing_work_response(self, dialog):
        if not dialog.accepted:
            return
        sim = self._sim_info.get_sim_instance()
        if sim is None:
            return
        context = interactions.context.InteractionContext(sim, interactions.context.InteractionContext.SOURCE_SCRIPT_WITH_USER_INTENT, interactions.priority.Priority.High, insert_strategy=interactions.context.QueueInsertStrategy.NEXT, bucket=interactions.context.InteractionBucketType.DEFAULT)
        sim.push_super_affordance(self.career_messages.career_missing_work.affordance, sim, context)

    def _late_for_work_callback(self, _):
        self.resend_at_work_info()
        if self.taking_day_off:
            return
        sim = self._sim_info.get_sim_instance()
        if sim is not None:
            affordance = self.get_work_affordance()
            if affordance is None or sim.queue.has_duplicate_super_affordance(affordance, sim, sim):
                return
        self.send_career_message(self.career_messages.career_missing_work.dialog, on_response=self._career_missing_work_response)

    def on_sim_creation_callback(self, sim, request):
        self.push_go_to_work_affordance()

    def on_sim_creation_denied_callback(self, request):
        self.attend_work()

    def send_uninstantiated_sim_home_for_work(self):
        if self._sim_info.is_at_home:
            return
        home_zone_id = self._sim_info.household.home_zone_id
        if home_zone_id == services.current_zone_id():
            sim_spawn_request = SimSpawnRequest(self._sim_info, SimSpawnReason.ACTIVE_HOUSEHOLD, SimSpawnPointStrategy(spawner_tags=(SpawnPoint.ARRIVAL_SPAWN_POINT_TAG,), spawn_point_option=None, spawn_action=None), secondary_priority=0, customer=self, customer_data=None, spin_up_action=None)
            services.sim_spawner_service().submit_request(sim_spawn_request)
        else:
            self._sim_info.inject_into_inactive_zone(home_zone_id)
            self.attend_work()

    def _start_work_callback(self, scheduler, alarm_data, extra_data):
        if self._at_work:
            return
        current_time = services.time_service().sim_now
        logger.debug('My Work Time just triggered!, Current Time: {}', current_time)
        if self.should_skip_next_shift(check_if_can_go_now=True):
            if self.requested_day_off:
                self._taking_day_off_reason = self._requested_day_off_reason
                self._requested_day_off_reason = career_ops.CareerTimeOffReason.NO_TIME_OFF
            else:
                self._requested_day_off_reason = career_ops.CareerTimeOffReason.NO_TIME_OFF
                self.resend_career_data()
                return
        elif self._sim_info.is_in_travel_group():
            if self.pto > 0:
                self._taking_day_off_reason = career_ops.CareerTimeOffReason.PTO
                self.pto_commodity_instance.add_value(-1)
            else:
                self._taking_day_off_reason = career_ops.CareerTimeOffReason.MISSING_WORK
            self.resend_career_data()
        end_time = date_and_time_from_week_time(current_time.week(), alarm_data.end_time)
        if current_time > end_time:
            logger.error("\n                The career {} for {} is about to start a shift with an end time\n                of {}, but it's already {}! Please provide a save and GSI dump\n                and file a DT.\n                ", self, self._sim_info, end_time, current_time, owner='epanero')
            return
        self.start_new_career_session(current_time, end_time)
        sim = self._get_sim()
        if self._sim_info.is_npc:
            if sim is not None:
                for situation in services.get_zone_situation_manager().get_situations_sim_is_in(sim):
                    situation_job = situation.get_current_job_for_sim(sim)
                    while situation_job is not None and situation_job.participating_npcs_should_ignore_work:
                        return
            self._career_location.on_npc_start_work()
            return
        if not self.taking_day_off and not self._sim_info.is_in_travel_group():
            if sim is not None:
                for situation in services.get_zone_situation_manager().get_situations_sim_is_in(sim):
                    job = situation.get_current_job_for_sim(sim)
                    while job is not None and job.confirm_leave_situation_for_work:
                        responses = None
                        if self.pto > 0:
                            pto_response = UiDialogResponse(dialog_response_id=PTO_RESPONSE_ID, text=self.career_messages.situation_leave_confirmation.take_pto_button_text)
                            responses = (pto_response,)

                        def on_response(dialog):
                            if dialog.accepted:
                                if self._try_offer_career_event():
                                    return
                                self.push_go_to_work_affordance()
                            elif dialog.response == PTO_RESPONSE_ID:
                                self.request_day_off(CareerTimeOffReason.PTO)
                                self.pto_commodity_instance.add_value(-1)
                                self.resend_career_data()

                        self.send_career_message(self.career_messages.situation_leave_confirmation.dialog, on_response=on_response, additional_responses=responses)
                        return
            if self._try_offer_career_event():
                return
            if sim is not None:
                self.push_go_to_work_affordance()
            elif self._sim_info.household.home_zone_id != self._sim_info.zone_id:
                self.send_uninstantiated_sim_home_for_work()
            else:
                self.attend_work()

    def _try_offer_career_event(self):
        if services.get_persistence_service().is_save_locked():
            return False
        if self._sim_info.is_npc or not self._sim_info.is_instanced():
            return False
        if self._sim_info in _career_event_overrides:
            career_event = _career_event_overrides.pop(self._sim_info)
        else:
            self._prune_stale_career_event_cooldowns()
            resolver = SingleSimResolver(self._sim_info)
            available_events = tuple(event for event in self.career_events if not self.is_career_event_on_cooldown(event) and event.tests.run_tests(resolver))
            if not available_events:
                return False
            household = self._sim_info.household
            for sim_info in household.sim_info_gen():
                while any(career.is_at_active_event for career in sim_info.career_tracker.careers.values()):
                    return False
            career_event = random.choice(available_events)
        services.get_career_service().add_pending_career_event_offer(self, career_event, on_accepted=self._on_career_event_accepted, on_canceled=self._on_career_event_declined)
        return True

    def _on_career_event_accepted(self, career_event):
        self._start_career_event(career_event)
        self._add_career_event_cooldown(career_event)
        self.attend_work(start_tones=False)

    def _on_career_event_declined(self, career_event):
        self.push_go_to_work_affordance()

    def _start_career_event(self, career_event):
        self.active_days_worked_statistic.add_value(1)
        self._career_event_manager = CareerEventManager(self)
        self._career_event_manager.start()
        self._career_event_manager.request_career_event(career_event)
        self._career_event_manager.start_top_career_event()
        self.resend_at_work_info()

    def _end_career_event(self):
        if self._career_event_manager is None:
            logger.error('Trying to end a career event when career {} does not have a career event manager', self)
            return
        payout = self._career_event_manager.get_career_event_payout_info()
        if payout is None:
            logger.error('Failed to get career event payout info for {}', self)
            self.leave_work()
            return
        work_duration = self._current_work_duration.in_hours()
        now = services.time_service().sim_now
        span_worked = now - self._current_work_start
        hours_worked = min(span_worked.in_hours(), work_duration)
        performance = self.current_level_tuning.performance_metrics.base_performance*payout.performance_multiplier
        if performance > 0:
            performance *= hours_worked/work_duration
        self.work_performance_stat.add_value(performance)
        (money_earned, pto_earned) = self._collect_rewards(hours_worked, payout.money_multiplier)
        result = self.evaluate_career_performance(money_earned, pto_earned)
        if result is not None:
            result.display_dialog(self)
        text = payout.text_factory(self._sim_info)
        op = EndOfWorkday(career_uid=self.guid64, score_text=text, level_icon=self._current_track.icon, money_earned=money_earned, paid_time_off_earned=pto_earned)
        if result is not None and result.evaluation == Evaluation.PROMOTED and result.dialog_factory is not None:
            dialog = result.dialog_factory(self._sim_info, resolver=SingleSimResolver(self._sim_info))
            dialog_msg = dialog.build_msg(additional_tokens=self.get_career_text_tokens() + result.dialog_args)
            op.add_promotion_info(self, dialog_msg.text)
        main_zone_id = self._career_event_manager.get_main_zone_id()
        if services.current_zone().ui_dialog_service.auto_respond:
            CareerEventManager.post_career_event_travel(self._sim_info, zone_id_override=main_zone_id)
        else:
            services.get_career_service().set_main_career_event_zone_id_and_lock_save(main_zone_id)
            distributor.system.Distributor.instance().add_op(self._sim_info, op)
        self._send_end_of_career_event_telemetry(payout.medal, payout.num_goals_completed)
        services.get_event_manager().process_event(test_events.TestEvent.WorkdayComplete, sim_info=self._sim_info, career=self, time_worked=span_worked.in_ticks(), money_made=money_earned)
        self.end_career_event_without_payout()

    def end_career_event_without_payout(self):
        self._career_event_manager.stop()
        self._career_event_manager = None
        self.end_career_session()

    def create_career_event_situations_during_zone_spin_up(self):
        if self._career_event_manager is not None:
            self._career_event_manager.create_career_event_situations_during_zone_spin_up()

    def _add_career_event_cooldown(self, career_event):
        if career_event.cooldown > 0:
            current_day = self.days_worked_statistic.get_value()
            self._career_event_cooldown_map[career_event.guid64] = int(current_day + career_event.cooldown)

    def is_career_event_on_cooldown(self, career_event):
        return career_event.guid64 in self._career_event_cooldown_map

    def _prune_stale_career_event_cooldowns(self):
        current_day = self.days_worked_statistic.get_value()
        self._career_event_cooldown_map = {event_id: day for (event_id, day) in self._career_event_cooldown_map.items() if current_day < day}

    def _end_work_callback(self, _):
        if self._interaction is not None and self._interaction() is not None:
            return
        if self.currently_at_work:
            self.leave_work()
        else:
            if self.taking_day_off_reason == CareerTimeOffReason.MISSING_WORK or self.taking_day_off_reason == CareerTimeOffReason.NO_TIME_OFF:
                time_at_work = 0
            else:
                time_at_work = self._current_work_duration.in_hours()
            if not self._sim_info.is_npc:
                self.handle_career_loot(time_at_work)
            self.end_career_session()

    def _create_work_session_alarms(self):
        self._create_end_of_work_day_alarm()
        if not self.currently_at_work:
            now = services.time_service().sim_now
            late_time = self.get_late_time()
            if now < late_time:
                self._late_for_work_handle = alarms.add_alarm(self, late_time - now + TimeSpan.ONE, self._late_for_work_callback)

    def _create_end_of_work_day_alarm(self):
        if self._end_work_handle is not None:
            self._end_work_handle.cancel()
        self._end_work_handle = alarms.add_alarm(self, self.time_until_end_of_work() + TimeSpan.ONE, self._end_work_callback)

    def start_new_career_session(self, start_time, end_time):
        self._current_work_start = start_time
        self._current_work_end = end_time
        self._current_work_duration = self._current_work_end - self._current_work_start
        self._at_work = False
        self._career_session_extended = False
        self._create_work_session_alarms()
        self.resend_at_work_info()
        self._sim_info.add_statistic(self.WORK_SESSION_PERFORMANCE_CHANGE, self.WORK_SESSION_PERFORMANCE_CHANGE.initial_value)

    def restore_career_session(self):
        if self.is_work_time:
            self._create_work_session_alarms()
        if self._career_event_manager is not None:
            self._career_event_manager.start()

    def end_career_session(self):
        if self._end_work_handle is not None:
            alarms.cancel_alarm(self._end_work_handle)
            self._end_work_handle = None
        if self._late_for_work_handle is not None:
            alarms.cancel_alarm(self._late_for_work_handle)
            self._late_for_work_handle = None
        self._interaction = None
        if not self.taking_day_off:
            self._reset_performance_statistics()
        self._current_work_start = None
        self._current_work_end = None
        self._current_work_duration = None
        self._at_work = False
        self._career_session_extended = False
        self._taking_day_off_reason = career_ops.CareerTimeOffReason.NO_TIME_OFF
        self.resend_at_work_info()

    def extend_career_session(self):
        if self._career_session_extended:
            logger.error('Trying to extend work hours twice for career {}', self)
            return
        if self._current_work_end is None:
            logger.error('Trying to extend work hours when not during work hours for career {}', self)
            return
        if not self.is_at_active_event:
            logger.error('Extending career session only supported for active events.')
            return
        self._career_session_extended = True
        self._current_work_end = self._current_work_end + interval_in_sim_minutes(minutes=self.current_level_tuning.stay_late_extension)
        if self._career_event_manager is not None:
            self._career_event_manager.on_career_session_extended()
        self._create_end_of_work_day_alarm()
        self.resend_at_work_info()

    def get_work_affordance(self):
        if self._sim_info.can_go_to_work():
            return self.career_affordance
        if self._sim_info.should_send_home_to_go_to_work():
            return self.go_home_to_work_affordance

    def push_go_to_work_affordance(self):
        sim = self._get_sim()
        if sim is None:
            return EnqueueResult.NONE
        affordance = self.get_work_affordance()
        if affordance is None:
            return EnqueueResult.NONE
        if not sim.queue.can_queue_visible_interaction():
            return EnqueueResult.NONE
        if sim.queue.has_duplicate_super_affordance(affordance, sim, sim):
            return EnqueueResult.NONE
        context = interactions.context.InteractionContext(sim, interactions.context.InteractionContext.SOURCE_SCRIPT_WITH_USER_INTENT, interactions.priority.Priority.High, insert_strategy=QueueInsertStrategy.LAST)
        result = sim.push_super_affordance(affordance, sim, context, career_uid=self.guid64)
        return result

    def leave_work_early(self):
        self._send_telemetry(TELEMETRY_HOOK_CAREER_LEAVE_EARLY)
        interaction = self._interaction() if self._interaction is not None else None
        if interaction is not None:
            interaction.cancel_user(cancel_reason_msg='User canceled work interaction through UI panel.')
        else:
            self.leave_work(left_early=True)

    def attend_work(self, interaction=None, start_tones=True):
        if not self._has_attended_first_day:
            self._has_attended_first_day = True
        if interaction is not None:
            self._interaction = weakref.ref(interaction)
        if self._at_work:
            return
        self.days_worked_statistic.add_value(1)
        self._at_work = True
        self._should_restore_career_state = True
        if self._late_for_work_handle is not None:
            alarms.cancel_alarm(self._late_for_work_handle)
            self._late_for_work_handle = None
        self.send_career_message(self.career_messages.career_daily_start_notification)
        self.resend_at_work_info()
        if start_tones:
            self.start_tones()
        services.get_event_manager().process_event(test_events.TestEvent.WorkdayStart, sim_info=self._sim_info, career=self)

    def leave_work(self, left_early=False):
        if self._career_event_manager is not None:
            self._end_career_event()
            return
        hours_worked = self.end_tones_and_get_hours_worked()
        if not self._sim_info.is_npc:
            self.handle_career_loot(hours_worked)
        self.end_career_session()
        if not self._sim_info.is_npc:
            self._sim_info.away_action_tracker.reset_to_default_away_action()
        if not left_early:
            self._try_invite_over()

    def time_until_end_of_work(self):
        current_time = services.time_service().sim_now
        time_to_work_end = current_time.time_to_week_time(self._current_work_end)
        return time_to_work_end

    def _try_invite_over(self):
        if self.invite_over is None:
            return
        if self.sim_info.is_npc or not self.sim_info.is_at_home:
            return
        if not self.sim_info.is_instanced(allow_hidden_flags=HiddenReasonFlag.RABBIT_HOLE):
            return
        resolver = SingleSimResolver(self.sim_info)
        if random.random() > self.invite_over.chance.get_chance(resolver):
            return
        services.sim_filter_service().submit_filter(self.invite_over.sim_filter, self._on_try_invite_over_sim_filter_response, requesting_sim_info=self.sim_info)

    def _on_try_invite_over_sim_filter_response(self, filter_results, callback_event_data):
        if not filter_results:
            return
        result = random.choice(filter_results)

        def response(dialog):
            if dialog.accepted:
                services.get_current_venue().summon_npcs((result.sim_info,), self.invite_over.purpose, self.sim_info)

        resolver = DoubleSimResolver(self.sim_info, result.sim_info)
        dialog = self.invite_over.confirmation_dialog(self.sim_info, resolver=resolver)
        dialog.show_dialog(additional_tokens=(result.sim_info,), on_response=response)

    def _send_telemetry(self, hook_tag, level=None):
        with telemetry_helper.begin_hook(career_telemetry_writer, hook_tag, sim_info=self._sim_info) as hook:
            self._populate_telemetry_hook_with_career_data(hook, level=level)

    def _send_end_of_career_event_telemetry(self, medal, num_goals_completed):
        with telemetry_helper.begin_hook(career_telemetry_writer, TELEMETRY_HOOK_CAREER_EVENT_END, sim_info=self._sim_info) as hook:
            self._populate_telemetry_hook_with_career_data(hook)
            hook.write_enum(TELEMETRY_CAREER_EVENT_MEDAL, medal)
            hook.write_int(TELEMETRY_CAREER_EVENT_GOALS, num_goals_completed)

    def _populate_telemetry_hook_with_career_data(self, hook, level=None):
        level = self._level if level is None else level
        hook.write_int(TELEMETRY_CAREER_ID, self.guid64)
        hook.write_int(TELEMETRY_CAREER_LEVEL, self._user_level)
        hook.write_guid(TELEMETRY_TRACK_ID, self._current_track.guid64)
        hook.write_int(TELEMETRY_TRACK_LEVEL, level)
        hook.write_int(TELEMETRY_TRACK_OVERMAX, self._overmax_level)

    def promote(self):
        result = self._promote()
        if result is not None:
            result.display_dialog(self)

    def demote(self):
        result = self._demote()
        if result is not None:
            result.display_dialog(self)

    def quit_career(self, post_quit_msg=True):
        self._send_telemetry(TELEMETRY_HOOK_CAREER_END)
        loot = self.current_level_tuning.loot_on_quit
        if loot is not None:
            resolver = SingleSimResolver(self._sim_info)
            loot.apply_to_resolver(resolver)
        self._sim_info.career_tracker.career_leave(self)
        if post_quit_msg:
            self.send_career_message(self.career_messages.quit_career_notification)
        self._remove_pto_commodity()
        self.resend_career_data()
        self.resend_at_work_info()
        self._remove_career_knowledge()
        self.remove_coworker_relationship_bit()

    def can_change_level(self, demote=False):
        delta = 1 if not demote else -1
        new_level = self._level + delta
        num_career_levels = len(self._current_track.career_levels)
        if new_level < 0:
            return False
        if new_level >= num_career_levels and not self.current_track_tuning.branches and self.current_track_tuning.overmax is None:
            return False
        return True

    def _change_level_within_track(self, delta):
        self.career_stop()
        self._overmax_level = 0
        self._sim_info.career_tracker.update_history(self)
        self._reset_career_objectives(self._current_track, self.level)
        self.career_start(is_level_change=True)
        self.resend_career_data()
        self.resend_at_work_info()

    def _handle_promotion(self, previous_salary, previous_highest_level):
        if self.user_level > previous_highest_level and self.current_level_tuning.promotion_reward is not None:
            reward_payout = self.current_level_tuning.promotion_reward.give_reward(self._sim_info)
            reward_text = LocalizationHelperTuning.get_bulleted_list(None, *(reward.get_display_text() for reward in reward_payout))
        else:
            reward_text = None
        (_, next_work_time, _) = self.get_next_work_time()
        salary = self.get_hourly_pay()
        salary_increase = salary - previous_salary
        level_text = self.current_level_tuning.promotion_notification_text(self._sim_info)
        is_not_school = not self.get_is_school()
        promotion_sting = self.current_level_tuning.promotion_audio_sting
        if promotion_sting is not None:
            play_tunable_audio(promotion_sting)
        if self.current_level_tuning.screen_slam is not None:
            self.current_level_tuning.screen_slam.send_screen_slam_message(self._sim_info, self._sim_info, self.current_level_tuning.title(self._sim_info), self.user_level, self.current_track_tuning.career_name(self._sim_info))
        if self.promotion_buff is not None:
            self._sim_info.add_buff_from_op(self.promotion_buff.buff_type, buff_reason=self.promotion_buff.buff_reason)
        if self.has_outfit():
            self._sim_info.resend_current_outfit()
        else:
            new_outfit = self._sim_info._outfits.get_outfit_for_clothing_change(None, OutfitChangeReason.DefaultOutfit, resolver=SingleSimResolver(self._sim_info))
            self._sim_info.set_current_outfit(new_outfit)
        self._send_telemetry(TELEMETRY_HOOK_CAREER_PROMOTION)
        self.on_promoted(self._sim_info)
        if reward_text is None:
            return EvaluationResult(Evaluation.PROMOTED, self.career_messages.promote_career_rewardless_notification, next_work_time, salary, salary_increase, level_text, display_career_info=is_not_school)
        return EvaluationResult(Evaluation.PROMOTED, self.career_messages.promote_career_notification, next_work_time, salary, salary_increase, level_text, reward_text, display_career_info=is_not_school)

    def _promote(self):
        if self._level + 1 < len(self._current_track.career_levels):
            return self._promote_within_track()
        if self.current_track_tuning.branches:
            return self._promote_to_new_branch()
        if self.current_track_tuning.overmax is not None:
            return self._increase_overmax_level()

    def _demote(self):
        current_level_tuning = self.current_level_tuning
        current_performance = self.work_performance
        if self.can_be_fired and (current_performance <= current_level_tuning.demotion_performance_level or self._level == 0):
            return self._fire()
        if self._level > 0:
            return self._demote_within_track()

    def _promote_to_new_branch(self):
        sim_info = self._sim_info
        if not sim_info.is_selectable or not sim_info.valid_for_distribution:
            random_track = random.choice(self.current_track_tuning.branches)
            logger.error("Trying to branch an npc's career: {}. Randomly choosing a branch: {}", self, random_track)
            return self.set_new_career_track(random_track)
        if services.current_zone().ui_dialog_service.auto_respond:
            return self.set_new_career_track(self.current_track_tuning.branches[0])
        self._pending_promotion = True
        msg = self.get_select_career_track_pb(sim_info, self, self.current_track_tuning.branches)
        Distributor.instance().add_op(sim_info, GenericProtocolBufferOp(Operation.SELECT_CAREER_UI, msg))
        return EvaluationResult(Evaluation.PROMOTED, None)

    def on_branch_selection(self, career_track):
        result = self.set_new_career_track(career_track)
        if result is not None:
            result.display_dialog(self)

    def _promote_within_track(self):
        previous_salary = self.get_hourly_pay()
        previous_highest_level = self._sim_info.career_tracker.get_highest_level_reached(self.guid64)
        self._change_level_within_track(1)
        return self._handle_promotion(previous_salary, previous_highest_level)

    def _demote_within_track(self):
        self._change_level_within_track(-1)
        if self.demotion_buff is not None:
            self._sim_info.add_buff_from_op(self.demotion_buff.buff_type, buff_reason=self.demotion_buff.buff_reason)
        self.on_demoted(self._sim_info)
        self._send_telemetry(TELEMETRY_HOOK_CAREER_DEMOTION, level=self.level)
        return EvaluationResult(Evaluation.DEMOTED, self.career_messages.demote_career_notification)

    def _fire(self):
        self._sim_info.career_tracker.remove_career(self.guid64, post_quit_msg=False)
        if self.fired_buff is not None:
            self._sim_info.add_buff_from_op(self.fired_buff.buff_type, buff_reason=self.fired_buff.buff_reason)
        self._send_telemetry(TELEMETRY_HOOK_CAREER_DEMOTION, level=-1)
        return EvaluationResult(Evaluation.FIRED, self.career_messages.fire_career_notification)

    def _apply_on_target(self, money_earned, pto_earned):
        self.resend_career_data()
        if self.career_messages.career_performance_warning.threshold.compare(self.work_performance):
            self.send_career_message(self.career_messages.career_performance_warning.dialog, on_response=self._career_performance_warning_response)
        if pto_earned > 0 and self.career_messages.pto_gained_text is not None:
            pto_notification = self.career_messages.pto_gained_text(self._sim_info)
        else:
            pto_notification = LocalizationHelperTuning.get_raw_text('')
        if self.currently_at_work:
            return EvaluationResult(Evaluation.ON_TARGET, self.career_messages.career_daily_end_notification, money_earned, pto_notification)
        if self.taking_day_off:
            notification = self.career_messages.career_time_off_messages[self._taking_day_off_reason].day_end_notification
            if notification is not None:
                return EvaluationResult(Evaluation.ON_TARGET, notification, money_earned, pto_notification)
        return EvaluationResult(Evaluation.ON_TARGET, None)

    def _increase_overmax_level(self):
        previous_salary = self.get_hourly_pay()
        self._sim_info.career_tracker.update_history(self)
        self.resend_career_data()
        self._send_telemetry(TELEMETRY_HOOK_CAREER_OVERMAX)
        self._remove_performance_statistics()
        self._remove_statistic_metric_listeners()
        self._add_performance_statistics()
        self._add_statistic_metric_listeners()
        salary = self.get_hourly_pay()
        salary_increase = salary - previous_salary
        overmax = self.current_track_tuning.overmax
        if overmax is not None and overmax.reward is not None:
            reward_payout = overmax.reward.give_reward(self._sim_info)
            reward_text = LocalizationHelperTuning.get_bulleted_list(None, *(reward.get_display_text() for reward in reward_payout))
        else:
            reward_text = None
        overmax_notification = self.career_messages.overmax_notification if reward_text is not None else self.career_messages.overmax_rewardless_notification
        if self.promotion_buff is not None:
            self._sim_info.add_buff_from_op(self.promotion_buff.buff_type, buff_reason=self.promotion_buff.buff_reason)
        return EvaluationResult(Evaluation.PROMOTED, overmax_notification, self._overmax_level + 1, salary, salary_increase, reward_text)

    def set_new_career_track(self, career_track):
        self._pending_promotion = False
        previous_salary = self.get_hourly_pay()
        previous_highest_level = self._sim_info.career_tracker.get_highest_level_reached(self.guid64)
        self.career_stop()
        self._current_track = career_track
        self._level = 0
        self._reset_career_objectives(career_track, 0)
        self._sim_info.career_tracker.update_history(self)
        self.career_start()
        self.resend_career_data()
        self.resend_at_work_info()
        return self._handle_promotion(previous_salary, previous_highest_level)

    def _reset_career_objectives(self, track, level):
        career_aspiration = track.career_levels[level].get_aspiration()
        if career_aspiration is not None:
            self._sim_info.aspiration_tracker.reset_milestone(career_aspiration)
            career_aspiration.register_callbacks()
            self._sim_info.aspiration_tracker.process_test_events_for_aspiration(career_aspiration)

    def _get_promote_performance_level(self):
        performance_level = self.current_level_tuning.promote_performance_level
        overmax = self.current_track_tuning.overmax
        if overmax is not None:
            max_level = self.current_level_tuning.performance_stat.max_value_tuning
            performance_level = min(max_level, performance_level + self._overmax_level*overmax.performance_threshold_increase)
        return performance_level

    def evaluate_career_performance(self, money_earned, pto_earned):
        current_level_tuning = self.current_level_tuning
        current_performance = self.work_performance
        resolver = SingleSimResolver(self._sim_info)
        if (self.can_change_level(demote=True) or self.can_be_fired) and current_performance <= current_level_tuning.demotion_performance_level:
            if random.random() < self.demotion_chance_modifiers.get_multiplier(resolver):
                return self._demote()
        if self.can_change_level(demote=False):
            promotion_aspiration = current_level_tuning.aspiration
            if promotion_aspiration is None or self._sim_info.aspiration_tracker.milestone_completed(promotion_aspiration):
                performance_threshold = self._get_promote_performance_level()
                resolver = SingleSimResolver(self._sim_info)
                if random.random() < self.early_promotion_chance.get_multiplier(resolver):
                    multiplier = self.early_promotion_modifiers.get_multiplier(resolver)
                    current_performance += performance_threshold*multiplier
                if current_performance >= performance_threshold:
                    return self._promote()
        return self._apply_on_target(money_earned, pto_earned)

    def handle_career_loot(self, hours_worked):
        (money_earned, pto_earned) = self._collect_rewards(hours_worked)
        result = self.evaluate_career_performance(money_earned, pto_earned)
        if result is not None:
            result.display_dialog(self)
        self._send_telemetry(TELEMETRY_HOOK_CAREER_DAILY_END)
        if not self.taking_day_off:
            span_worked = create_time_span(hours=hours_worked)
            services.get_event_manager().process_event(test_events.TestEvent.WorkdayComplete, sim_info=self._sim_info, career=self, time_worked=span_worked.in_ticks(), money_made=money_earned)

    def _career_performance_warning_response(self, dialog):
        if not dialog.accepted:
            return
        sim = self._sim_info.get_sim_instance()
        if sim is None:
            return
        context = interactions.context.InteractionContext(sim, interactions.context.InteractionContext.SOURCE_SCRIPT_WITH_USER_INTENT, interactions.priority.Priority.High, insert_strategy=interactions.context.QueueInsertStrategy.NEXT, bucket=interactions.context.InteractionBucketType.DEFAULT)
        sim.push_super_affordance(self.career_messages.career_performance_warning.affordance, sim, context)

    @flexmethod
    def get_hourly_pay(cls, inst, sim_info=DEFAULT, career_track=DEFAULT, career_level=DEFAULT, overmax_level=DEFAULT):
        inst_or_cls = inst if inst is not None else cls
        sim_info = sim_info if sim_info is not DEFAULT else inst.sim_info
        career_track = career_track if career_track is not DEFAULT else inst.current_track_tuning
        career_level = career_level if career_level is not DEFAULT else inst.level
        overmax_level = overmax_level if overmax_level is not DEFAULT else inst.overmax_level
        logger.assert_raise(career_level >= 0, 'get_hourly_pay: Current Level is negative: {}, Level: {}', type(inst_or_cls).__name__, career_level)
        logger.assert_raise(career_level < len(career_track.career_levels), 'get_hourly_pay: Current Level is bigger then the number of careers: {}, Level: {}', type(inst_or_cls).__name__, career_level)
        level_tuning = career_track.career_levels[career_level]
        hourly_pay = level_tuning.simoleons_per_hour
        if career_track.overmax is not None:
            hourly_pay += career_track.overmax.salary_increase*overmax_level
        for trait_bonus in level_tuning.simolean_trait_bonus:
            while sim_info.trait_tracker.has_trait(trait_bonus.trait):
                hourly_pay += hourly_pay*(trait_bonus.bonus*0.01)
        hourly_pay = int(hourly_pay)
        return hourly_pay

    @flexmethod
    def get_daily_pay(cls, inst, career_track=DEFAULT, career_level=DEFAULT, **kwargs):
        inst_or_cls = inst if inst is not None else cls
        career_track = career_track if career_track is not DEFAULT else inst.current_track_tuning
        career_level = career_level if career_level is not DEFAULT else inst.level
        career_level_tuning = career_track.career_levels[career_level]
        join_time = inst.join_time if inst is not None else None
        work_schedule = get_career_schedule_for_level(career_level_tuning, join_time=join_time)
        if work_schedule is None:
            hours_per_day = Retirement.DAILY_HOURS_WORKED_FALLBACK
        else:
            ticks_per_week = sum(end_ticks - start_ticks for (start_ticks, end_ticks) in work_schedule.get_schedule_times())
            hours_per_day = date_and_time.ticks_to_time_unit(ticks_per_week, date_and_time.TimeUnit.HOURS, True)/7
        return int(hours_per_day*inst_or_cls.get_hourly_pay(career_track=career_track, career_level=career_level, **kwargs))

    def _collect_rewards(self, time_at_work, pay_multiplier=1):
        current_level_tuning = self.current_level_tuning
        performance_metrics = current_level_tuning.performance_metrics
        work_duration = self._current_work_duration.in_hours()
        percent_at_work = time_at_work/work_duration
        work_time_multiplier = 1
        if percent_at_work*100 < performance_metrics.full_work_day_percent:
            self.work_performance_stat.add_value(-performance_metrics.missed_work_penalty)
            work_time_multiplier = percent_at_work/(performance_metrics.full_work_day_percent/100)
        work_money = math.ceil(self.get_hourly_pay()*work_duration*work_time_multiplier*pay_multiplier)
        self._sim_info.household.funds.add(work_money, Consts_pb2.TELEMETRY_MONEY_CAREER, self._get_sim())
        if self.taking_day_off_reason != career_ops.CareerTimeOffReason.PTO:
            pto_delta = self._add_pto(self.current_level_tuning.pto_per_day*work_time_multiplier)
        else:
            pto_delta = 0
        return (work_money, pto_delta)

    def get_career_text_tokens(self):
        job = self.current_level_tuning.title(self._sim_info)
        career = self._current_track.career_name(self._sim_info)
        company = self.get_company_name()
        return (job, career, company)

    def send_career_message(self, dialog_factory, *additional_tokens, icon_override=None, on_response=None, display_career_info=False, additional_responses=None, **kwargs):
        if self._sim_info.is_npc:
            return
        dialog = dialog_factory(self._sim_info, resolver=SingleSimResolver(self._sim_info))
        if dialog is not None:
            if display_career_info:
                career_args = UiCareerNotificationArgs()
                career_args.career_uid = self.guid64
                career_args.career_level = self.level
                career_args.career_track = self._current_track.guid64
                career_args.user_career_level = self.user_level
                career_args.sim_id = self._sim_info.id
                career_args.paid_time_off_available = self.pto
                self._work_scheduler.populate_scheduler_msg(career_args.work_schedule)
            else:
                career_args = None
            if additional_responses:
                dialog.set_responses(additional_responses)
            icon_override = (self._current_track.icon, None) if icon_override is None else icon_override
            dialog.show_dialog(additional_tokens=self.get_career_text_tokens() + additional_tokens, icon_override=icon_override, secondary_icon_override=(None, self._sim_info), on_response=on_response, career_args=career_args, **kwargs)

    def populate_set_career_op(self, career_op):
        career_op.career_uid = self.guid64
        career_op.career_level = self.level
        career_op.pay = self.get_hourly_pay()
        career_op.pay_raise_count = self.overmax_level
        career_op.performance = int(self.work_performance)
        career_op.company = self.get_company_name()
        career_op.auto_work = self.auto_work
        career_op.career_track = self._current_track.guid64
        career_op.user_career_level = self.user_level
        career_op.performance_complete = self.work_performance >= self._get_promote_performance_level()
        career_op.skip_next_shift = self.should_skip_next_shift()
        career_op.paid_time_off_available = self.pto
        if self._work_scheduler is not None:
            self._work_scheduler.populate_scheduler_msg(career_op.work_schedule)
        tooltip = self._get_performance_tooltip()
        if tooltip:
            career_op.performance_tooltip = tooltip
        career_op.is_retired = False

    def sim_skewer_rabbit_hole_affordances_gen(self, context, **kwargs):
        for tone in self.get_available_tones_gen():
            yield AffordanceObjectPair(self.CAREER_TONE_INTERACTION, None, self.CAREER_TONE_INTERACTION, None, away_action=tone, away_action_sim_info=self._sim_info, **kwargs)
        affordance = self.current_level_tuning.tones.leave_work_early
        if affordance is not None:
            for aop in affordance.potential_interactions(self._get_sim(), context, sim_info=self._sim_info, **kwargs):
                yield aop

    def get_available_tones_gen(self):
        tones = self.current_level_tuning.tones
        yield tones.default_action
        yield tones.optional_actions

    def start_tones(self):
        if self._sim_info.is_npc:
            return
        tones = self.current_level_tuning.tones
        tracker = self._sim_info.away_action_tracker
        tracker.add_on_away_action_started_callback(self._on_tone_started)
        tracker.add_on_away_action_ended_callback(self._on_tone_ended)
        tracker.create_and_apply_away_action(tones.default_action)

    def restore_tones(self):
        if self._sim_info.is_npc:
            return
        if self.is_at_active_event:
            return
        if self.currently_at_work:
            tracker = self._sim_info.away_action_tracker
            tracker.add_on_away_action_started_callback(self._on_tone_started)
            tracker.add_on_away_action_ended_callback(self._on_tone_ended)
            if tracker.current_away_action is None:
                tones = self.current_level_tuning.tones
                tracker.create_and_apply_away_action(tones.default_action)

    def end_tones_and_get_hours_worked(self):
        if self._sim_info.is_npc:
            return
        tracker = self._sim_info.away_action_tracker
        tracker.stop()
        tracker.remove_on_away_action_started_callback(self._on_tone_started)
        tracker.remove_on_away_action_ended_callback(self._on_tone_ended)
        dominant_tone = None
        dominant_value = 0
        for tone in self.get_available_tones_gen():
            stat = self._sim_info.get_statistic(tone.runtime_commodity, add=False)
            while stat is not None:
                value = stat.get_value()
                if dominant_tone is None or value > dominant_value:
                    dominant_tone = tone
                    dominant_value = value
        if dominant_tone is not None:
            tone = dominant_tone(tracker)
            tone.apply_dominant_tone_loot()
        hours_worked = self.get_hours_worked()
        self._remove_tone_commodities()
        return hours_worked

    def get_hours_worked(self):
        minutes = 0
        for tone in self.get_available_tones_gen():
            stat = self._sim_info.get_statistic(tone.runtime_commodity, add=False)
            while stat is not None:
                value = stat.get_value()
                minutes += value
        hours = minutes/MINUTES_PER_HOUR
        return hours

    def _on_tone_started(self, tone):
        if self._is_valid_tone(tone):
            stat = self._sim_info.get_statistic(tone.runtime_commodity)
            stat.add_statistic_modifier(CareerBase.TONE_STAT_MOD)

    def _on_tone_ended(self, tone):
        if self._is_valid_tone(tone):
            stat = self._sim_info.get_statistic(tone.runtime_commodity)
            stat.remove_statistic_modifier(CareerBase.TONE_STAT_MOD)

    def _remove_tone_commodities(self):
        for tone in self.get_available_tones_gen():
            self._sim_info.remove_statistic(tone.runtime_commodity)

    def _is_valid_tone(self, tone):
        for other_tone in self.get_available_tones_gen():
            while tone.guid64 == other_tone.guid64:
                return True
        return False

    def has_outfit(self):
        return bool(self.current_level_tuning.work_outfit.outfit_tags)

    def _add_career_knowledge(self):
        for sim_info in self.get_coworker_sim_infos_gen():
            sim_info.relationship_tracker.add_knows_career(self._sim_info.id)
            self._sim_info.relationship_tracker.add_knows_career(sim_info.id)
        for sim_info in self._sim_info.household:
            sim_info.relationship_tracker.send_relationship_info(self._sim_info.id)

    def _remove_career_knowledge(self):
        tracker = self._sim_info.relationship_tracker
        for target in tracker.get_target_sim_infos():
            if target is None:
                logger.error('\n                    SimInfo {} has a relationship with a None target. The target\n                    has probably been pruned and the data is out of sync. Please\n                    provide a save and GSI dump and file a DT for this.\n                    ', self._sim_info, owner='epanero')
            if target.household_id == self._sim_info.household_id:
                target.relationship_tracker.send_relationship_info(self._sim_info.id)
            target.relationship_tracker.remove_knows_career(self._sim_info.id)

    def setup_career_event(self):
        if self.career_event_manager is not None:
            if self.sim_info.is_npc:
                self.end_career_event_without_payout()
                home_zone_id = self._sim_info.household.home_zone_id
                if services.current_zone_id() != home_zone_id:
                    self._sim_info.inject_into_inactive_zone(home_zone_id)
                else:
                    strat = SimSpawnPointStrategy(spawner_tags=(SpawnPoint.ARRIVAL_SPAWN_POINT_TAG,), spawn_point_option=None, spawn_action=None)
                    request = SimSpawnRequest(self._sim_info, SimSpawnReason.LOT_OWNER, strat)
                    services.sim_spawner_service().submit_request(request)
                    self.career_event_manager.request_career_event_zone_director()
            else:
                self.career_event_manager.request_career_event_zone_director()

    def startup_career(self):
        pass

    def on_loading_screen_animation_finished(self):
        if self._pending_promotion:
            self.promote()

    @property
    def should_restore_career_state(self):
        return self._should_restore_career_state

    def get_persistable_sim_career_proto(self):
        proto = SimObjectAttributes_pb2.PersistableSimCareer()
        proto.career_uid = self.guid64
        proto.track_uid = self.current_track_tuning.guid64
        proto.track_level = self.level
        proto.user_display_level = self.user_level
        proto.overmax_level = self.overmax_level
        proto.attended_work = self.currently_at_work
        proto.requested_day_off_reason = self.requested_day_off_reason
        proto.taking_day_off_reason = self.taking_day_off_reason
        proto.pending_promotion = self._pending_promotion
        self._career_location.save_career_location(proto)
        proto.has_attended_first_day = self._has_attended_first_day
        should_restore_state = self._should_restore_career_state
        if self.is_work_time:
            sim = self._get_sim()
            should_restore_state = False
        else:
            should_restore_state = True
        proto.should_restore_career_state = should_restore_state
        if self._current_work_start is not None:
            proto.current_work_start = self._current_work_start.absolute_ticks()
            proto.current_work_end = self._current_work_end.absolute_ticks()
            proto.current_work_duration = self._current_work_duration.in_ticks()
            proto.career_session_extended = self._career_session_extended
        if self._join_time is not None:
            proto.join_time = self._join_time.absolute_ticks()
        if self._career_event_manager is not None and hasattr(proto, 'career_event_manager_data'):
            proto.career_event_manager_data = self._career_event_manager.get_career_event_manager_data_proto()
        for (career_event_id, day) in self._career_event_cooldown_map.items():
            with ProtocolBufferRollback(proto.career_event_cooldowns) as cooldown:
                cooldown.career_event_id = career_event_id
                cooldown.day = day
        return proto

    def load_from_persistable_sim_career_proto(self, proto, skip_load=False):
        self._current_track = services.get_instance_manager(sims4.resources.Types.CAREER_TRACK).get(proto.track_uid)
        self._level = proto.track_level
        self._user_level = proto.user_display_level
        self._overmax_level = proto.overmax_level
        self._career_location.load_career_location(proto)
        if skip_load:
            self._join_time = services.time_service().sim_now
        else:
            self._join_time = DateAndTime(proto.join_time)
            self._at_work = proto.attended_work
            self._has_attended_first_day = proto.has_attended_first_day
            if proto.HasField('should_restore_career_state'):
                self._should_restore_career_state = proto.should_restore_career_state
            if proto.HasField('called_in_sick'):
                if proto.called_in_sick:
                    self._requested_day_off_reason = career_ops.CareerTimeOffReason.FAKE_SICK
            elif proto.HasField('requested_day_off_reason'):
                self._requested_day_off_reason = career_ops.CareerTimeOffReason(proto.requested_day_off_reason)
            self._taking_day_off_reason = career_ops.CareerTimeOffReason(proto.taking_day_off_reason)
            self._pending_promotion = proto.pending_promotion
            if self._pending_promotion and self._level + 1 >= len(self._current_track.career_levels) and not self._current_track.branches:
                self._pending_promotion = False
            if proto.HasField('current_work_start'):
                self._current_work_start = DateAndTime(proto.current_work_start)
                self._current_work_end = DateAndTime(proto.current_work_end)
                self._current_work_duration = TimeSpan(proto.current_work_duration)
                self._career_session_extended = proto.career_session_extended
            if has_field(proto, 'career_event_manager_data'):
                self._career_event_manager = CareerEventManager(self)
                self._career_event_manager.load_career_event_manager_data_proto(proto.career_event_manager_data)
            for cooldown in proto.career_event_cooldowns:
                self._career_event_cooldown_map[cooldown.career_event_id] = cooldown.day
        self.career_start(is_load=True)

    def get_career_seniority(self):
        duration = (services.time_service().sim_now - self._join_time).in_weeks()
        return clamp(EPSILON, math.tanh(duration), 1)

    def get_custom_gsi_data(self):
        return {}

import enum

class CareerCategory(enum.Int):
    __qualname__ = 'CareerCategory'
    Invalid = 0
    Work = 1
    School = 2
    TeenPartTime = 3

from distributor.ops import BuildBuyLockUnlock
from role.role_state import RoleState
from sims4.localization import TunableLocalizedString
from sims4.tuning.instances import lock_instance_tunables
from sims4.tuning.tunable import TunableTuple, OptionalTunable
from situations.bouncer.bouncer_types import BouncerExclusivityCategory
from situations.situation_complex import SituationComplexCommon, SituationState, SituationStateData
from situations.situation_job import SituationJob
from situations.situation_time_jump import SITUATION_TIME_JUMP_DISALLOW
from situations.situation_types import SituationCreationUIOption
import distributor.system
CAREER_SESSION_EXTENDED = 'career_session_extended'

class CareerEventSituation(SituationComplexCommon):
    __qualname__ = 'CareerEventSituation'
    INSTANCE_TUNABLES = {'user_job': TunableTuple(description='\n            The job and role which the career Sim is placed into.\n            ', situation_job=SituationJob.TunableReference(description='\n                A reference to a SituationJob that can be performed at this Situation.\n                '), role_state=RoleState.TunableReference(description='\n                A role state the sim assigned to the job will perform.\n                ')), 'build_buy_lock_reason': OptionalTunable(description='\n            If enabled then build buy will be disabled during this situation.\n            ', tunable=TunableLocalizedString(description='\n                The reason build buy is locked. For this case, it is because \n                build buy is not allowed during active career. Used on the disabled \n                buildbuy button in the HUD.\n                '))}
    REMOVE_INSTANCE_TUNABLES = ('category', 'entitlement', '_resident_job', '_icon', 'job_display_ordering', 'recommended_job_object_notification', 'recommended_job_object_text', '_cost', 'force_invite_only', 'duration', 'max_participants', '_initiating_sim_tests', 'targeted_situation', '_NPC_host_filter', '_NPC_hosted_player_tests', 'NPC_hosted_situation_start_message', 'NPC_hosted_situation_use_player_sim_as_filter_requester', 'NPC_hosted_situation_player_job', 'venue_types', 'venue_invitation_message', 'venue_situation_player_job', '_relationship_between_job_members', '_jobs_to_put_in_party', '_implies_greeted_status', '_survives_active_household_change')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._sim = None
        self._career_session_extended = False
        reader = self._seed.custom_init_params_reader
        if reader is not None:
            self._career_session_extended = reader.read_bool(CAREER_SESSION_EXTENDED, False)

    @classmethod
    def _states(cls):
        return (SituationStateData(1, CareerEventSituationState),)

    @classmethod
    def _get_tuned_job_and_default_role_state_tuples(cls):
        return [(cls.user_job.situation_job, cls.user_job.role_state)]

    @classmethod
    def default_job(cls):
        return cls.user_job.situation_job

    def _send_build_buy_lock(self):
        if self.build_buy_lock_reason is not None:
            op = BuildBuyLockUnlock(True, self.build_buy_lock_reason)
            distributor.system.Distributor.instance().add_op_with_no_owner(op)

    def start_situation(self):
        super().start_situation()
        self._change_state(CareerEventSituationState())
        self._send_build_buy_lock()

    def load_situation(self):
        result = super().load_situation()
        if result:
            self._send_build_buy_lock()
        return result

    def on_remove(self):
        super().on_remove()
        if self.build_buy_lock_reason is not None:
            op = BuildBuyLockUnlock(False)
            distributor.system.Distributor.instance().add_op_with_no_owner(op)

    def build_situation_start_message(self):
        msg = super().build_situation_start_message()
        msg.is_active_career = True
        msg.has_stayed_late = self._career_session_extended
        return msg

    def build_situation_duration_change_op(self):
        msg = super().build_situation_duration_change_op()
        msg.has_stayed_late = self._career_session_extended
        return msg

    def _on_set_sim_job(self, sim, job_type):
        super()._on_set_sim_job(sim, job_type)
        self._sim = sim

    def _save_custom_situation(self, writer):
        super()._save_custom_situation(writer)
        writer.write_bool(CAREER_SESSION_EXTENDED, self._career_session_extended)

    def get_situation_goal_actor(self):
        return self._sim.sim_info

    def on_career_session_extended(self, new_duration):
        self._career_session_extended = True
        self.change_duration(new_duration)

lock_instance_tunables(CareerEventSituation, exclusivity=BouncerExclusivityCategory.NEUTRAL, creation_ui_option=SituationCreationUIOption.NOT_AVAILABLE, time_jump=SITUATION_TIME_JUMP_DISALLOW)

class CareerEventSituationState(SituationState):
    __qualname__ = 'CareerEventSituationState'

import protocolbuffers
from careers.career_enums import CareerCategory
from careers.career_history import CareerHistory
from careers.career_tuning import Career
from careers.retirement import Retirement
from date_and_time import DATE_AND_TIME_ZERO
from distributor.rollback import ProtocolBufferRollback
from singletons import DEFAULT
import distributor
import services
import sims4.resources

class CareerTracker:
    __qualname__ = 'CareerTracker'

    def __init__(self, sim_info):
        self._sim_info = sim_info
        self._careers = {}
        self._career_history = {}
        self._retirement = None

    def __iter__(self):
        return iter(self._careers.values())

    @property
    def careers(self):
        return self._careers

    def resend_career_data(self):
        if services.current_zone().is_zone_shutting_down:
            return
        if not self._sim_info.valid_for_distribution:
            return
        op = distributor.ops.SetCareers(self._careers)
        distributor.system.Distributor.instance().add_op(self._sim_info, op)

    def _at_work_infos(self):
        at_work_infos = []
        for career in self._careers.values():
            at_work_infos.append(career.create_work_state_msg())
        return at_work_infos

    def resend_at_work_infos(self):
        if self._sim_info.is_npc:
            return
        op = distributor.ops.SetAtWorkInfos(self._at_work_infos())
        distributor.system.Distributor.instance().add_op(self._sim_info, op)

    @property
    def has_career(self):
        return bool(self._careers)

    def has_career_outfit(self):
        return any(career.has_outfit() for career in self._careers.values())

    def _on_confirmation_dialog_response(self, dialog, new_career):
        if dialog.accepted:
            if new_career.can_quit:
                self.quit_quittable_careers()
            self.add_career(new_career)

    def add_career(self, new_career, show_confirmation_dialog=False, user_level_override=None, give_skipped_rewards=True):
        if show_confirmation_dialog:
            (level, _, track) = new_career.get_career_entry_data(career_history=self._career_history, user_level_override=user_level_override)
            career_track_tuning = track if track is not None else new_career.start_track
            career_level_tuning = career_track_tuning.career_levels[level]
            if self._retirement is not None:
                self._retirement.send_dialog(Career.UNRETIRE_DIALOG, career_level_tuning.title(self._sim_info), icon_override=DEFAULT, on_response=lambda dialog: self._on_confirmation_dialog_response(dialog, new_career))
                return
            if new_career.can_quit:
                quittable_careers = self.get_quittable_careers()
                if quittable_careers:
                    career = next(iter(quittable_careers.values()))
                    career.send_career_message(Career.SWITCH_JOBS_DIALOG, career_level_tuning.title(self._sim_info), icon_override=DEFAULT, on_response=lambda dialog: self._on_confirmation_dialog_response(dialog, new_career))
                    return
        self.end_retirement()
        self._careers[new_career.guid64] = new_career
        new_career.join_career(career_history=self._career_history, user_level_override=user_level_override, give_skipped_rewards=give_skipped_rewards)
        self.resend_career_data()

    def remove_career(self, career_uid, post_quit_msg=True):
        if career_uid in self._careers:
            career = self._careers[career_uid]
            career.career_stop()
            career.quit_career(post_quit_msg=post_quit_msg)
            career.on_career_removed(self._sim_info)

    def remove_invalid_careers(self):
        for (career_uid, career) in list(self._careers.items()):
            while not career.is_valid_career():
                self.remove_career(career_uid, post_quit_msg=False)

    def get_career_by_uid(self, career_uid):
        if career_uid in self._careers:
            return self._careers[career_uid]

    def get_career_by_category(self, career_category):
        for career in self:
            while career.career_category == career_category:
                return career

    def has_work_career(self):
        return any(career.career_category != CareerCategory.School for career in self)

    def has_quittable_career(self):
        if self.get_quittable_careers():
            return True
        return False

    def get_quittable_careers(self):
        quittable_careers = dict((uid, career) for (uid, career) in self._careers.items() if career.can_quit)
        return quittable_careers

    def quit_quittable_careers(self):
        for career_uid in self.get_quittable_careers():
            self.remove_career(career_uid)

    def get_at_work_career(self):
        for career in self._careers.values():
            while career.currently_at_work:
                return career

    @property
    def currently_at_work(self):
        for career in self._careers.values():
            while career.currently_at_work:
                return True
        return False

    @property
    def currently_during_work_hours(self):
        for career in self._careers.values():
            while career.is_work_time:
                return True
        return False

    @property
    def career_currently_within_hours(self):
        for career in self._careers.values():
            while career.is_work_time:
                return career

    def get_currently_at_work_career(self):
        for career in self._careers.values():
            while career.currently_at_work:
                return career

    def career_leave(self, career):
        self.update_history(career, from_leave=True)
        del self._careers[career.guid64]

    @property
    def career_history(self):
        return self._career_history

    def update_history(self, career, from_leave=False):
        highest_level = self.get_highest_level_reached(career.guid64)
        if career.user_level > highest_level:
            highest_level = career.user_level
        time_of_leave = services.time_service().sim_now if from_leave else DATE_AND_TIME_ZERO
        self._career_history[career.guid64] = CareerHistory(career_track=career.current_track_tuning, level=career.level, user_level=career.user_level, overmax_level=career.overmax_level, highest_level=highest_level, time_of_leave=time_of_leave, daily_pay=career.get_daily_pay(), days_worked=career.days_worked_statistic.get_value(), active_days_worked=career.active_days_worked_statistic.get_value())

    def get_highest_level_reached(self, career_uid):
        entry = self._career_history.get(career_uid)
        if entry is not None:
            return entry.highest_level
        return 0

    @property
    def retirement(self):
        return self._retirement

    def retire_career(self, career_uid):
        for uid in list(self._careers):
            self.remove_career(uid, post_quit_msg=False)
        self._retirement = Retirement(self._sim_info, career_uid)
        self._retirement.start(send_retirement_notification=True)

    def end_retirement(self):
        if self._retirement is not None:
            self._retirement.stop()
            self._retirement = None

    @property
    def retired_career_uid(self):
        if self._retirement is not None:
            return self._retirement.career_uid
        return 0

    def start_retirement(self):
        if self._retirement is not None:
            self._retirement.start()

    def on_sim_added_to_skewer(self):
        self.resend_career_data()
        self.resend_at_work_infos()

    def on_loading_screen_animation_finished(self):
        for career in self._careers.values():
            career.on_loading_screen_animation_finished()

    def on_sim_startup(self):
        for career in self._careers.values():
            career.startup_career()

    def on_death(self):
        for (uid, career) in list(self._careers.items()):
            if career.is_at_active_event:
                career.end_career_event_without_payout()
            self.remove_career(uid, post_quit_msg=False)
        self.end_retirement()

    def clean_up(self):
        for career in self._careers.values():
            career.career_stop()
        self._careers.clear()
        self.end_retirement()

    def on_situation_request(self, situation):
        career = self.get_at_work_career()
        if self._sim_info.is_npc:
            career_location = career.get_career_location()
            if services.current_zone_id() == career_location.get_zone_id():
                return
        if not (career is not None and career.is_at_active_event):
            career.leave_work_early()

    def save(self):
        save_data = protocolbuffers.SimObjectAttributes_pb2.PersistableSimCareers()
        for career in self._careers.values():
            with ProtocolBufferRollback(save_data.careers) as career_proto:
                career_proto.MergeFrom(career.get_persistable_sim_career_proto())
        for (career_uid, career_history) in self._career_history.items():
            with ProtocolBufferRollback(save_data.career_history) as career_history_proto:
                career_history_proto.career_uid = career_uid
                career_history.save_career_history(career_history_proto)
        if self._retirement is not None:
            save_data.retirement_career_uid = self._retirement.career_uid
        return save_data

    def load(self, save_data, skip_load=False):
        self._careers.clear()
        for career_save_data in save_data.careers:
            career_uid = career_save_data.career_uid
            career_type = services.get_instance_manager(sims4.resources.Types.CAREER).get(career_uid)
            while career_type is not None:
                career = career_type(self._sim_info)
                career.load_from_persistable_sim_career_proto(career_save_data, skip_load=skip_load)
                self._careers[career_uid] = career
        self._career_history.clear()
        for history_entry in save_data.career_history:
            if skip_load and history_entry.career_uid not in self._careers:
                pass
            career_history = CareerHistory.load_career_history(self._sim_info, history_entry)
            while career_history is not None:
                self._career_history[history_entry.career_uid] = career_history
        self._retirement = None
        retired_career_uid = save_data.retirement_career_uid
        if retired_career_uid in self._career_history:
            self._retirement = Retirement(self._sim_info, retired_career_uid)

    def activate_career_aspirations(self):
        for career in self._careers.values():
            career_aspiration = career._current_track.career_levels[career._level].get_aspiration()
            while career_aspiration is not None:
                career_aspiration.register_callbacks()
                self._sim_info.aspiration_tracker.validate_and_return_completed_status(career_aspiration)
                self._sim_info.aspiration_tracker.process_test_events_for_aspiration(career_aspiration)

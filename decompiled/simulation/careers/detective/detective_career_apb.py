from careers.career_event_zone_director import CareerEventZoneDirectorProxy
from careers.detective.detective_career import DetectiveCareer
from situations.bouncer.bouncer_types import BouncerRequestPriority, RequestSpawningOption
from situations.situation import Situation
from situations.situation_guest_list import SituationGuestList, SituationGuestInfo
from situations.situation_job import SituationJob
import services
import sims4.telemetry
import telemetry_helper
TELEMETRY_GROUP_DETECTIVE = 'DETE'
TELEMETRY_HOOK_APB_CALL = 'APBC'
TELEMETRY_CLUES_FOUND = 'clue'
detective_apb_telemetry_writer = sims4.telemetry.TelemetryWriter(TELEMETRY_GROUP_DETECTIVE)

class ZoneDirectorApb(CareerEventZoneDirectorProxy):
    __qualname__ = 'ZoneDirectorApb'
    INSTANCE_TUNABLES = {'detective_career': DetectiveCareer.TunableReference(description='\n            The career that we want to use to spawn the criminal.\n            ', tuning_group='APB'), 'apb_situation': Situation.TunableReference(description='\n            The situation controlling the APB. This will manage the criminal Sim\n            as well as all the decoys.\n            ', tuning_group='APB'), 'apb_neutral_situation': Situation.TunableReference(description='\n            The situation controlling all Sims in the zone, including Sims in\n            the APB situation.\n            ', tuning_group='APB'), 'apb_situation_job_detective': SituationJob.TunableReference(description='\n            The job that the detective is put into for the duration of the APB.\n            ', tuning_group='APB'), 'apb_situation_job_decoy': SituationJob.TunableReference(description='\n            The job that the decoys are put into for the duration of the APB.\n            ', tuning_group='APB'), 'apb_situation_job_criminal': SituationJob.TunableReference(description='\n            The job that the criminal is put into for the duration of the APB.\n            ', tuning_group='APB')}

    def create_situations_during_zone_spin_up(self):
        sim_info = self._career_event.sim_info
        career = sim_info.career_tracker.careers.get(self.detective_career.guid64)
        if career is not None:
            situation_manager = services.get_zone_situation_manager()
            situation_manager.create_situation(self.apb_neutral_situation, user_facing=False)
            guest_list = SituationGuestList(invite_only=True, filter_requesting_sim_id=sim_info.sim_id)
            guest_list.add_guest_info(SituationGuestInfo(career.active_criminal_sim_id, self.apb_situation_job_criminal, RequestSpawningOption.DONT_CARE, BouncerRequestPriority.VIP))
            guest_list.add_guest_info(SituationGuestInfo(sim_info.sim_id, self.apb_situation_job_detective, RequestSpawningOption.DONT_CARE, BouncerRequestPriority.VIP))
            for apb_decoy in career.get_decoys_for_apb_gen():
                guest_list.add_guest_info(SituationGuestInfo(apb_decoy.sim_id, self.apb_situation_job_decoy, RequestSpawningOption.DONT_CARE, BouncerRequestPriority.VIP))
            situation_manager.create_situation(self.apb_situation, guest_list=guest_list, spawn_sims_during_zone_spin_up=True, user_facing=False)
            with telemetry_helper.begin_hook(detective_apb_telemetry_writer, TELEMETRY_HOOK_APB_CALL, sim_info=sim_info) as hook:
                hook.write_int(TELEMETRY_CLUES_FOUND, len(career.get_discovered_clues()))
        return super().create_situations_during_zone_spin_up()

from filters.tunable import FilterTermVariant
from sims4.tuning.instances import HashedTunedInstanceMetaclass
from sims4.tuning.tunable import TunableReference, HasTunableReference
from ui.ui_dialog_notification import TunableUiDialogNotificationSnippet
import services
import sims4.resources

class Clue(HasTunableReference, metaclass=HashedTunedInstanceMetaclass, manager=services.get_instance_manager(sims4.resources.Types.DETECTIVE_CLUE)):
    __qualname__ = 'Clue'
    INSTANCE_TUNABLES = {'filter_term': FilterTermVariant(description='\n            The filter that will be used to spawn Sims (including the criminal)\n            that match this clue.\n            '), 'notebook_entry': TunableReference(description="\n            The entry that will be added to the player's notebook when they\n            discover this clue.\n            ", manager=services.get_instance_manager(sims4.resources.Types.NOTEBOOK_ENTRY)), 'notification': TunableUiDialogNotificationSnippet(description='\n            The notification that will be displayed to the player when this clue\n            is discovered.\n            ')}

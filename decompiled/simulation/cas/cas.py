try:
    import _cas
except:

    class _cas:
        __qualname__ = '_cas'
        SimInfo = None

        @staticmethod
        def age_up_sim(*_, **__):
            pass

        @staticmethod
        def get_buffs_from_part_ids(*_, **__):
            return []

        @staticmethod
        def get_tags_from_outfit(*_, **__):
            return set()

        @staticmethod
        def generate_offspring(*_, **__):
            pass

        @staticmethod
        def generate_household(*_, **__):
            pass

        @staticmethod
        def generate_merged_outfit(*_, **__):
            pass

        @staticmethod
        def generate_occult_siminfo(*_, **__):
            pass

        @staticmethod
        def is_duplicate_merged_outfit(*_, **__):
            pass

        @staticmethod
        def is_online_entitled(*_, **__):
            pass

        @staticmethod
        def apply_siminfo_override(*_, **__):
            pass

        @staticmethod
        def randomize_part_color(*_, **__):
            pass

        @staticmethod
        def randomize_skintone_from_tags(*_, **__):
            pass

        @staticmethod
        def set_caspart(*_, **__):
            pass

        @staticmethod
        def get_caspart_bodytype(*_, **__):
            pass

BaseSimInfo = _cas.SimInfo
age_up_sim = _cas.age_up_sim
get_buff_from_part_ids = _cas.get_buffs_from_part_ids
get_tags_from_outfit = _cas.get_tags_from_outfit
generate_offspring = _cas.generate_offspring
generate_household = _cas.generate_household
generate_merged_outfit = _cas.generate_merged_outfit
generate_occult_siminfo = _cas.generate_occult_siminfo
is_duplicate_merged_outfit = _cas.is_duplicate_merged_outfit
is_online_entitled = _cas.is_online_entitled
apply_siminfo_override = _cas.apply_siminfo_override
randomize_part_color = _cas.randomize_part_color
randomize_skintone_from_tags = _cas.randomize_skintone_from_tags
set_caspart = _cas.set_caspart
get_caspart_bodytype = _cas.get_caspart_bodytype
import collections
from crafting.recipe import Recipe
from event_testing.test_variants import SkillRangeTest
from event_testing.tests import TunableTestSet
from sims4.localization import TunableLocalizedStringFactory
from sims4.tuning.instances import TunedInstanceMetaclass, HashedTunedInstanceMetaclass
from sims4.tuning.tunable import TunableResourceKey, TunableRealSecond, TunableList, TunableReference, Tunable, OptionalTunable, HasTunableReference
import services
import sims4.log
import sims4.resources
logger = sims4.log.Logger('Music')

class VocalTrack(HasTunableReference, metaclass=HashedTunedInstanceMetaclass, manager=services.get_instance_manager(sims4.resources.Types.RECIPE)):
    __qualname__ = 'VocalTrack'
    INSTANCE_TUNABLES = {'vocal_clip': TunableResourceKey(description='\n            The propx file of the vox to play.\n            ', default=None, resource_types=(sims4.resources.Types.PROPX,)), 'tests': TunableTestSet(description='\n            Tests to verify if this song is available for the Sim to play.\n            ')}

class MusicTrack(metaclass=HashedTunedInstanceMetaclass, manager=services.get_instance_manager(sims4.resources.Types.RECIPE)):
    __qualname__ = 'MusicTrack'
    INSTANCE_TUNABLES = {'music_clip': TunableResourceKey(description='\n            The propx file of the music clip to play.\n            ', needs_tuning=False, default=None, resource_types=(sims4.resources.Types.PROPX,)), 'length': TunableRealSecond(description="\n            The length of the clip in real seconds.  This should be a part of\n            the propx's file name.\n            ", needs_tuning=False, default=30, minimum=0), 'buffer': TunableRealSecond(description="\n            A buffer added to the track length.  This is used to prevent the\n            audio from stopping before it's finished.\n            ", needs_tuning=False, default=0), 'check_for_unlock': Tunable(description="\n            Whether or not to check the Sim's Unlock Component to determine if\n            they can play the song.  Currently, only clips that are meant to be\n            unlocked by the Write Song interaction should have this set to true.\n            ", needs_tuning=False, tunable_type=bool, default=False), 'music_track_name': OptionalTunable(description="\n            If the clip is of a song, this is its name. The name is shown in the\n            Pie Menu when picking specific songs to play.\n            \n            If the clip isn't a song, like clips used for the Practice or Write\n            Song interactions, this does not need to be tuned.\n            ", tunable=TunableLocalizedStringFactory(description="\n                The track's name.\n                "), enabled_by_default=True), 'tests': TunableTestSet(description='\n            Tests to verify if this song is available for the Sim to play.\n            '), 'moods': TunableList(description="\n            A list of moods that will be used to determine which song a Sim will\n            play autonomously.  If a Sim doesn't know any songs that their\n            current mood, they'll play anything.\n            ", tunable=TunableReference(manager=services.mood_manager()), needs_tuning=True), 'vocal_tracks': TunableList(description='\n            If this music track has vocals, add them here.  The first track that\n            passes its test will be used.  If no tracks pass their test, none\n            will be used.\n            ', tunable=VocalTrack.TunableReference())}

class MusicStyle(metaclass=TunedInstanceMetaclass, manager=services.get_instance_manager(sims4.resources.Types.RECIPE)):
    __qualname__ = 'MusicStyle'
    INSTANCE_TUNABLES = {'music_tracks': TunableList(TunableReference(description='\n            A particular music track to use as part of this\n            style.\n            ', manager=services.get_instance_manager(sims4.resources.Types.RECIPE), class_restrictions=(MusicTrack,))), 'pie_menu_category': TunableReference(description='\n            The pie menu category for this music style.\n            This can be used to break styles up into genres.\n            ', manager=services.get_instance_manager(sims4.resources.Types.PIE_MENU_CATEGORY), allow_none=True)}
    tracks_by_skill = collections.defaultdict(lambda : collections.defaultdict(set))
    styles_for_track = collections.defaultdict(set)

    @classmethod
    def _tuning_loaded_callback(cls):
        services.get_instance_manager(sims4.resources.Types.RECIPE).add_on_load_complete(cls._set_up_dictionaries)

    @classmethod
    def _set_up_dictionaries(cls, _):
        for track in cls.music_tracks:
            cls.styles_for_track[track].add(cls)
            for test_group in track.tests:
                has_skill_test = False
                for test in test_group:
                    if not isinstance(test, SkillRangeTest):
                        pass
                    has_skill_test = True
                    for level in range(test.skill_range_min, test.skill_range_max + 1):
                        cls.tracks_by_skill[test.skill][level].add(track)
                while not has_skill_test:
                    logger.error('{} has no tuned skill test in one of its test groups. This makes it hard to optimize music track choosing. Please tune at least one skill test in every test group.', cls, owner='bhill')

class MusicRecipe(Recipe):
    __qualname__ = 'MusicRecipe'
    INSTANCE_TUNABLES = {'music_track_unlock': TunableReference(description='\n            The music track that will be unlocked when the crafting process is\n            complete.\n            ', manager=services.get_instance_manager(sims4.resources.Types.RECIPE), class_restrictions=(MusicTrack,)), 'music_style': TunableReference(description='\n            Which music style the Sim will pull tracks from while writing\n            the song.\n            ', manager=services.get_instance_manager(sims4.resources.Types.RECIPE), class_restrictions=(MusicStyle,))}

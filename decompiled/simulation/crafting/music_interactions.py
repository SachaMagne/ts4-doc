import random
from audio.primitive import PlaySound
from crafting.crafting_interactions import CraftingPhaseSuperInteractionMixin, CraftingPhaseStagingSuperInteraction
from crafting.music import MusicStyle
from event_testing.resolver import SingleSimResolver
from interactions.aop import AffordanceObjectPair
from interactions.base.super_interaction import SuperInteraction
from interactions.interaction_finisher import FinishingType
from interactions.social.social_super_interaction import SocialSuperInteraction
from sims4.tuning.tunable import Tunable, TunableList, TunableReference
from sims4.utils import flexmethod
from singletons import DEFAULT
from ui.ui_dialog_generic import UiDialogTextInputOk
import alarms
import clock
import event_testing.results
import services
import sims4
logger = sims4.log.Logger('Interactions')

class PlayAudioSuperInteraction(SuperInteraction):
    __qualname__ = 'PlayAudioSuperInteraction'
    INSTANCE_SUBCLASSES_ONLY = True
    SCRIPT_EVENT_ID_START_AUDIO = 100
    SCRIPT_EVENT_ID_STOP_AUDIO = 101
    INSTANCE_TUNABLES = {'play_multiple_clips': Tunable(description='\n            If true, the Sim will continue playing until the interaction is\n            cancelled or exit conditions are met. \n            ', needs_tuning=False, tunable_type=bool, default=False), 'music_styles': TunableList(TunableReference(description='\n            Which music styles are available for this interaction.\n            ', manager=services.get_instance_manager(sims4.resources.Types.RECIPE), class_restrictions=(MusicStyle,), pack_safe=True)), 'use_buffer': Tunable(description="\n            If true, this interaction will add the buffer tuned on the music\n            track to the length of the track.  This is tunable because some\n            interactions, like Practice, use shorter audio clips that don't\n            require the buffer.\n            ", needs_tuning=False, tunable_type=bool, default=True)}

    def __init__(self, aop, context, track=None, pie_menu_category=None, unlockable_name=None, **kwargs):
        super().__init__(aop, context, **kwargs)
        self._track = track
        self.pie_menu_category = pie_menu_category
        self._unlockable_name = unlockable_name
        self._sound_alarm = None
        self._sound = None
        self._vocals = None
        self.add_exit_function(self._cancel_sound_alarm)

    def build_basic_content(self, sequence=(), **kwargs):
        self.store_event_handler(self._create_sound_alarm, handler_id=self.SCRIPT_EVENT_ID_START_AUDIO)
        self.store_event_handler(self._cancel_sound_alarm, handler_id=self.SCRIPT_EVENT_ID_STOP_AUDIO)
        return super().build_basic_content(sequence, **kwargs)

    def on_reset(self):
        self._cancel_sound_alarm()
        super().on_reset()

    def _create_sound_alarm(self, *args, **kwargs):
        track_length = self._get_track_length()
        if self._sound_alarm is None:
            self._sound_alarm = alarms.add_alarm(self, track_length, self._sound_alarm_callback)
        if self._sound is None:
            self._sound = PlaySound(self._instrument, self._track.music_clip.instance)
        vocal_track = self._get_vocal_track()
        if vocal_track is not None:
            self._vocals = PlaySound(self.sim, vocal_track.vocal_clip.instance, is_vox=True)
            self._vocals.start()
        self._sound.start()

    def _sound_alarm_callback(self, handle):
        if self.play_multiple_clips:
            self._cancel_sound_alarm()
            if hasattr(self, 'recipe'):
                styles = [self.recipe.music_style]
            else:
                styles = self.music_styles
            self._track = PlayAudioSuperInteraction._get_next_track(styles, self.sim, self.get_resolver())
            self._create_sound_alarm()
        else:
            self.cancel(FinishingType.NATURAL, cancel_reason_msg='Sound alarm triggered and the song finished naturally.')

    def _cancel_sound_alarm(self, *args, **kwargs):
        if self._sound_alarm is not None:
            alarms.cancel_alarm(self._sound_alarm)
            self._sound_alarm = None
        if self._sound is not None:
            self._sound.stop()
            self._sound = None
        if self._vocals is not None:
            self._vocals.stop()
            self._vocals = None

    def _get_track_length(self):
        real_seconds = self._track.length
        if self.use_buffer:
            real_seconds += self._track.buffer
        interval = clock.interval_in_real_seconds(real_seconds)
        return interval

    @property
    def _instrument(self):
        return self.target

    def _get_vocal_track(self):
        if self._track.vocal_tracks:
            resolver = self.get_resolver()
            for track in self._track.vocal_tracks:
                while track.tests.run_tests(resolver):
                    return track

    @staticmethod
    def _get_next_track(styles, sim, resolver):
        valid_tracks = []
        styles = set(styles)
        for (skill, level_to_tracks) in MusicStyle.tracks_by_skill.items():
            skill_stat = sim.get_statistic(skill)
            skill_level = 0 if skill_stat is None else skill_stat.get_user_value()
            for track in level_to_tracks[skill_level]:
                if not styles & MusicStyle.styles_for_track[track]:
                    pass
                valid_tracks.append(track)
        sim_mood = sim.get_mood()
        valid_mood_tracks = [track for track in valid_tracks if sim_mood in track.moods]
        if not valid_mood_tracks and not valid_tracks:
            return
        to_consider = valid_mood_tracks or valid_tracks
        random.shuffle(to_consider)
        for track in to_consider:
            if track.check_for_unlock and sim.sim_info.unlock_tracker.is_unlocked(track):
                return track
            while not track.check_for_unlock and track.tests.run_tests(resolver):
                return track

    @classmethod
    def _has_tracks(cls, sim, resolver):
        styles = set(cls.music_styles)
        for (skill, level_to_tracks) in MusicStyle.tracks_by_skill.items():
            skill_stat = sim.get_statistic(skill)
            if skill_stat is None:
                pass
            for track in level_to_tracks[skill_stat.get_user_value()]:
                if not styles & MusicStyle.styles_for_track[track]:
                    pass
                if track.check_for_unlock and sim.sim_info.unlock_tracker.is_unlocked(track):
                    return True
                while not track.check_for_unlock and track.tests.run_tests(resolver):
                    return True
        return False

class PlayAudioSuperInteractionTieredMenu(PlayAudioSuperInteraction):
    __qualname__ = 'PlayAudioSuperInteractionTieredMenu'

    @flexmethod
    def get_pie_menu_category(cls, inst, pie_menu_category=None, **interaction_parameters):
        if inst is not None:
            return inst.pie_menu_category
        return pie_menu_category

    @flexmethod
    def _get_name(cls, inst, target=DEFAULT, context=DEFAULT, track=None, unlockable_name=None, **kwargs):
        if track is not None and track.music_track_name is not None:
            return track.music_track_name(unlockable_name)
        inst_or_cls = inst if inst is not None else cls
        return super(SuperInteraction, inst_or_cls)._get_name(target=target, context=context, **kwargs)

    @classmethod
    def potential_interactions(cls, target, context, **kwargs):
        sim = context.sim
        resolver = SingleSimResolver(sim.sim_info)
        for style in cls.music_styles:
            for track in style.music_tracks:
                while track.tests.run_tests(resolver):
                    if not track.check_for_unlock:
                        yield AffordanceObjectPair(cls, target, cls, None, track=track, pie_menu_category=style.pie_menu_category, **kwargs)
                    else:
                        unlocks = sim.sim_info.unlock_tracker.get_unlocks(track)
                        if unlocks:
                            while True:
                                for unlock in unlocks:
                                    yield AffordanceObjectPair(cls, target, cls, None, track=unlock.tuning_class, pie_menu_category=style.pie_menu_category, unlockable_name=unlock.name, **kwargs)

class PlayAudioSuperInteractionNonTieredMenu(PlayAudioSuperInteraction):
    __qualname__ = 'PlayAudioSuperInteractionNonTieredMenu'

    def __init__(self, aop, context, **kwargs):
        super().__init__(aop, context, **kwargs)
        if 'phase' in kwargs:
            phase = kwargs['phase']
            styles = [phase.recipe.music_style]
        else:
            styles = self.music_styles
        self._track = PlayAudioSuperInteraction._get_next_track(styles, context.sim, self.get_resolver())

    @flexmethod
    def test(cls, inst, context=DEFAULT, **kwargs):
        if inst is not None:
            inst_or_cls = inst
            if inst._track is None:
                return event_testing.results.TestResult(False, 'No available songs to play.')
        else:
            inst_or_cls = cls
            context = cls.context if context is DEFAULT else context
            if not cls._has_tracks(context.sim, context.sim.get_resolver()):
                return event_testing.results.TestResult(False, 'No available songs to play.')
        return super(__class__, inst_or_cls).test(context=context, **kwargs)

class PlayAudioSocialSuperInteraction(PlayAudioSuperInteractionNonTieredMenu, SocialSuperInteraction):
    __qualname__ = 'PlayAudioSocialSuperInteraction'

    @property
    def _instrument(self):
        if self.carry_target is not None:
            return self.carry_target
        return self.target

class PlayAudioCraftingPhaseStagingSuperInteraction(PlayAudioSuperInteractionNonTieredMenu, CraftingPhaseStagingSuperInteraction):
    __qualname__ = 'PlayAudioCraftingPhaseStagingSuperInteraction'

    @flexmethod
    def test(cls, inst, context=DEFAULT, **kwargs):
        inst_or_cls = inst if inst is not None else cls
        return super(PlayAudioSuperInteraction, inst_or_cls).test(context=context, **kwargs)

TEXT_INPUT_SONG_NAME = 'song_name'

class UnluckMusicTrackSuperInteraction(CraftingPhaseSuperInteractionMixin, SuperInteraction):
    __qualname__ = 'UnluckMusicTrackSuperInteraction'
    INSTANCE_TUNABLES = {'dialog': UiDialogTextInputOk.TunableFactory(description='\n            Text entry dialog to name the song the Sim wrote.\n            ', text_inputs=(TEXT_INPUT_SONG_NAME,))}

    def _run_interaction_gen(self, timeline):

        def on_response(dialog):
            if not dialog.accepted:
                self.cancel(FinishingType.DIALOG, cancel_reason_msg='Name Song dialog timed out from client.')
                return
            name = dialog.text_input_responses.get(TEXT_INPUT_SONG_NAME)
            self.sim.sim_info.unlock_tracker.add_unlock(self.phase.recipe.music_track_unlock, name)

        dialog = self.dialog(self.sim, self.get_resolver())
        dialog.show_dialog(on_response=on_response)

        def _destroy_target():
            self.process.current_ico.destroy(source=self, cause='Destroying target of unlock music track SI')

        self.add_exit_function(_destroy_target)
        return True

class LicenseSongSuperInteraction(SuperInteraction):
    __qualname__ = 'LicenseSongSuperInteraction'
    INSTANCE_TUNABLES = {'music_styles': TunableList(TunableReference(description='\n            Which music styles are available for this interaction.  This\n            should be only the Written Music Style for the particular\n            instrument.\n            ', manager=services.get_instance_manager(sims4.resources.Types.RECIPE), class_restrictions=(MusicStyle,), reload_dependent=True))}

    @classmethod
    def _verify_tuning_callback(cls):
        for style in cls.music_styles:
            for track in style.music_tracks:
                while not track.check_for_unlock:
                    logger.error("MusicTrack {} does not have check_for_unlock set to False.  This is required for MusicTracks that can be 'Licensed'.", track.__name__)

    def __init__(self, aop, context, track=None, unlockable_name=None, **kwargs):
        super().__init__(aop, context, unlockable_name=unlockable_name, **kwargs)
        self._track = track
        self._unlockable_name = unlockable_name

    @flexmethod
    def _get_name(cls, inst, target=DEFAULT, context=DEFAULT, track=None, unlockable_name=None, **kwargs):
        if unlockable_name is not None and track.music_track_name is not None:
            return track.music_track_name(unlockable_name)
        inst_or_cls = inst if inst is not None else cls
        return super(SuperInteraction, inst_or_cls)._get_name(target=target, context=context, **kwargs)

    @classmethod
    def potential_interactions(cls, target, context, **kwargs):
        if context.sim is None:
            return
        for style in cls.music_styles:
            for track in style.music_tracks:
                unlocks = context.sim.sim_info.unlock_tracker.get_unlocks(track)
                while unlocks:
                    while True:
                        for unlock in unlocks:
                            yield AffordanceObjectPair(cls, target, cls, None, track=unlock.tuning_class, pie_menu_category=style.pie_menu_category, unlockable_name=unlock.name, **kwargs)

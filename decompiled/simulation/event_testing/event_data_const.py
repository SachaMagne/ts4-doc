import enum

class ObjectiveDataStorageType(enum.Int, export=False):
    __qualname__ = 'ObjectiveDataStorageType'
    CountData = Ellipsis
    IdData = Ellipsis

class DataType(enum.Int, export=False):
    __qualname__ = 'DataType'
    RelationshipData = 1
    SimoleanData = 2
    TimeData = 3
    TravelData = 5
    ObjectiveCount = 6
    CareerData = 7
    TagData = 8
    RelativeStartingData = 9

class RelationshipData(enum.Int, export=False):
    __qualname__ = 'RelationshipData'
    CurrentRelationships = 0
    TotalRelationships = 1

class SimoleonData(enum.Int):
    __qualname__ = 'SimoleonData'
    MoneyFromEvents = 0
    TotalMoneyEarned = 1

class TimeData(enum.Int, export=False):
    __qualname__ = 'TimeData'
    SimTime = 0
    ServerTime = 1

class TagData(enum.Int, export=False):
    __qualname__ = 'TagData'
    SimoleonsEarned = 0
    TimeElapsed = 1

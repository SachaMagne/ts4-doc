from event_testing import TargetIdTypes
from event_testing.event_data_const import ObjectiveDataStorageType
from event_testing.results import TestResultNumeric
from event_testing.test_events import TestEvent
from interactions import ParticipantType
from sims4.localization import TunableLocalizedStringFactory
from sims4.tuning.instances import HashedTunedInstanceMetaclass
from sims4.tuning.tunable import TunableVariant, HasTunableSingletonFactory, AutoFactoryInit
from sims4.utils import classproperty
import enum
import event_testing.results as results
import event_testing.test_variants
import event_testing.tests
import event_testing.tests_with_data as tests_with_data
import services
import sims4.log
import sims4.tuning.tunable
import tag
logger = sims4.log.Logger('ObjectiveTuning', default_owner='jjacobson')

class ParticipantTypeActorHousehold(enum.IntFlags):
    __qualname__ = 'ParticipantTypeActorHousehold'
    Actor = ParticipantType.Actor
    ActiveHousehold = ParticipantType.ActiveHousehold

class ParticipantTypeTargetAllRelationships(enum.IntFlags):
    __qualname__ = 'ParticipantTypeTargetAllRelationships'
    TargetSim = ParticipantType.TargetSim
    AllRelationships = ParticipantType.AllRelationships

class TunableObjectiveTestVariant(TunableVariant):
    __qualname__ = 'TunableObjectiveTestVariant'

    def __init__(self, description='A tunable test supported for use as an objective.', **kwargs):
        super().__init__(at_work=event_testing.test_variants.AtWorkTest.TunableFactory(locked_args={'subject': ParticipantType.Actor, 'tooltip': None}), buff_added=event_testing.test_variants.TunableBuffAddedTest(locked_args={'tooltip': None}), career_attendence=tests_with_data.TunableCareerAttendenceTest(locked_args={'tooltip': None}), career_test=event_testing.test_variants.TunableCareerTest.TunableFactory(locked_args={'subjects': ParticipantType.Actor, 'tooltip': None}), collected_item_test=event_testing.test_variants.CollectedItemTest.TunableFactory(locked_args={'tooltip': None}), collection_test=event_testing.test_variants.TunableCollectionThresholdTest(locked_args={'who': ParticipantType.Actor, 'tooltip': None}), crafted_item=event_testing.test_variants.TunableCraftedItemTest(locked_args={'tooltip': None}), familial_trigger_test=tests_with_data.TunableFamilyAspirationTriggerTest(locked_args={'tooltip': None}), generation_created=tests_with_data.GenerationTest.TunableFactory(locked_args={'tooltip': None}), has_buff=event_testing.test_variants.TunableBuffTest(locked_args={'subject': ParticipantType.Actor, 'tooltip': None}), household_size=event_testing.test_variants.HouseholdSizeTest.TunableFactory(locked_args={'participant': ParticipantType.Actor, 'tooltip': None}), mood_test=event_testing.test_variants.TunableMoodTest(locked_args={'who': ParticipantType.Actor, 'tooltip': None}), object_criteria=event_testing.test_variants.ObjectCriteriaTest.TunableFactory(locked_args={'tooltip': None}), object_purchase_test=event_testing.test_variants.TunableObjectPurchasedTest(locked_args={'tooltip': None}), offspring_created_test=tests_with_data.OffspringCreatedTest.TunableFactory(locked_args={'tooltip': None}), ran_interaction_test=tests_with_data.TunableParticipantRanInteractionTest(locked_args={'participant': ParticipantType.Actor, 'tooltip': None}), relationship=event_testing.test_variants.TunableRelationshipTest(participant_type_override=(ParticipantTypeTargetAllRelationships, ParticipantTypeTargetAllRelationships.AllRelationships), locked_args={'tooltip': None}), selected_aspiration_track_test=event_testing.test_variants.TunableSelectedAspirationTrackTest(locked_args={'who': ParticipantType.Actor, 'tooltip': None}), simoleons_earned=tests_with_data.TunableSimoleonsEarnedTest(locked_args={'tooltip': None}), simoleon_value=event_testing.test_variants.TunableSimoleonsTest(locked_args={'subject': ParticipantType.Actor, 'tooltip': None}), situation_running_test=event_testing.test_variants.TunableSituationRunningTest(locked_args={'tooltip': None}), skill_tag=event_testing.test_variants.TunableSkillTagThresholdTest(locked_args={'who': ParticipantType.Actor, 'tooltip': None}), statistic=event_testing.test_variants.StatThresholdTest.TunableFactory(locked_args={'who': ParticipantType.Actor, 'tooltip': None}), total_simoleons_earned=event_testing.test_variants.TunableTotalSimoleonsEarnedTest(locked_args={'tooltip': None}), total_interaction_time_elapsed_by_tag=tests_with_data.TunableTotalTimeElapsedByTagTest(locked_args={'tooltip': None}), total_relationship_bit=tests_with_data.TunableTotalRelationshipBitTest(locked_args={'tooltip': None}), total_simoleons_earned_by_tag=tests_with_data.TunableTotalSimoleonsEarnedByTagTest(locked_args={'tooltip': None}), total_time_played=event_testing.test_variants.TunableTotalTimePlayedTest(locked_args={'tooltip': None}), total_zones_traveled=tests_with_data.TunableTotalTravelTest(locked_args={'tooltip': None}), trait=event_testing.test_variants.TraitTest.TunableFactory(participant_type_override=(ParticipantTypeActorHousehold, ParticipantTypeActorHousehold.Actor), locked_args={'tooltip': None}), unlock_earned=event_testing.test_variants.TunableUnlockedTest(locked_args={'participant': ParticipantType.Actor, 'tooltip': None}), whim_completed_test=tests_with_data.WhimCompletedTest.TunableFactory(locked_args={'tooltip': None}), location_test=event_testing.test_variants.LocationTest.TunableFactory(location_tests={'is_outside': False, 'is_natural_ground': False, 'is_in_slot': False, 'is_on_active_lot': False}), description=description, **kwargs)

class _ObjectiveCompletionType(HasTunableSingletonFactory, AutoFactoryInit):
    __qualname__ = '_ObjectiveCompletionType'

    def get_number_required(self, test):
        raise NotImplementedError

    def get_if_money(self, test):
        return False

    @property
    def data_type(self):
        raise NotImplementedError

    def increment_data(self, data_object, objective, resolver, result, additional_result):
        raise NotImplementedError

    def check_objective_validity(self, objective):
        pass

    def check_if_should_test(self, resolver):
        return not resolver.on_zone_load

class SimInfoStatisticObjectiveTrack(_ObjectiveCompletionType):
    __qualname__ = 'SimInfoStatisticObjectiveTrack'

    def get_number_required(self, test):
        return sims4.math.MAX_INT32

    @property
    def data_type(self):
        return ObjectiveDataStorageType.CountData

    def increment_data(self, data_object, objective, resolver, result, additional_result):
        if result and additional_result:
            data_object.add_objective_value(objective, 1)
        count = data_object.get_objective_count(objective)
        return results.TestResultNumeric(False, 'Objective: not possible because sim info panel member.', current_value=count, goal_value=0, is_money=False)

class _IterationsObjectiveTrack(_ObjectiveCompletionType):
    __qualname__ = '_IterationsObjectiveTrack'
    FACTORY_TUNABLES = {'iterations_required_to_pass': sims4.tuning.tunable.Tunable(int, 1, description='The number of times that the objective test must pass in order for the objective to be considered complete.')}

    def get_number_required(self, test):
        return self.iterations_required_to_pass

    @property
    def data_type(self):
        return ObjectiveDataStorageType.CountData

    def increment_data(self, data_object, objective, resolver, result, additional_result):
        if result and additional_result:
            data_object.add_objective_value(objective, 1)
        count = data_object.get_objective_count(objective)
        if count < self.get_number_required(objective.objective_test):
            return results.TestResultNumeric(False, 'Objective: not possible because sim info panel member.', current_value=count, goal_value=self.iterations_required_to_pass, is_money=False)
        return results.TestResult.TRUE

class _UniqueTargetsObjectiveTrack(_ObjectiveCompletionType):
    __qualname__ = '_UniqueTargetsObjectiveTrack'
    FACTORY_TUNABLES = {'unique_targets_required_to_pass': sims4.tuning.tunable.Tunable(description='\n                The number of unique targets that need to attained to pass.\n                ', tunable_type=int, default=1), 'id_to_check': sims4.tuning.tunable.TunableEnumEntry(description="\n            Uniqueness can be by either instance id or definition id. For example, crafting 2 plates\n            of mac and cheese will have the same definition id but different instance id's.", tunable_type=TargetIdTypes, default=TargetIdTypes.DEFAULT)}

    def get_number_required(self, test):
        return self.unique_targets_required_to_pass

    @property
    def data_type(self):
        return ObjectiveDataStorageType.IdData

    def increment_data(self, data_object, objective, resolver, result, additional_result):
        if result and additional_result:
            target_id = resolver.get_target_id(objective.objective_test, self.id_to_check)
            if target_id is not None:
                data_object.add_objective_value(objective, target_id)
        count = data_object.get_objective_count(objective)
        if count < self.unique_targets_required_to_pass:
            return results.TestResultNumeric(False, 'Objective: not enough iterations.', current_value=count, goal_value=self.unique_targets_required_to_pass, is_money=False)
        return results.TestResult.TRUE

    def check_objective_validity(self, objective):
        if not objective.objective_test.UNIQUE_TARGET_TRACKING_AVAILABLE:
            logger.error('Objective {} tuned with test {} that has no unique target tracking available.', objective, objective.objective_test)

class _TagChecklistObjectiveTrack(_ObjectiveCompletionType):
    __qualname__ = '_TagChecklistObjectiveTrack'
    FACTORY_TUNABLES = {'unique_tags_required_to_pass': sims4.tuning.tunable.Tunable(description='\n                The number of unique tags that will count once for checking off the list.\n                ', tunable_type=int, default=1), 'tag_checklist': sims4.tuning.tunable.TunableList(description='\n            A list of single count tags that if present on the tested subject will count toward the total.', tunable=sims4.tuning.tunable.TunableEnumEntry(tag.Tag, tag.Tag.INVALID))}

    def get_number_required(self, test):
        return self.unique_tags_required_to_pass

    @property
    def data_type(self):
        return ObjectiveDataStorageType.IdData

    def increment_data(self, data_object, objective, resolver, result, additional_result):
        if result and additional_result:
            tags_to_test = resolver.get_tags(objective.objective_test)
            for tag_from_test in tags_to_test:
                for tag_from_objective in self.tag_checklist:
                    while tag_from_test is tag_from_objective:
                        data_object.add_objective_value(objective, tag_from_objective)
                        break
        count = data_object.get_objective_count(objective)
        if count < self.unique_tags_required_to_pass:
            return results.TestResultNumeric(False, 'Objective: not enough iterations.', current_value=count, goal_value=self.unique_tags_required_to_pass, is_money=False)
        return results.TestResult.TRUE

    def check_objective_validity(self, objective):
        if not objective.objective_test.TAG_CHECKLIST_TRACKING_AVAILABLE:
            logger.error('Objective {} tuned with test {} that has no tag checklist tracking avilable', objective, objective.objective_test)

class _UniqueLocationsObjectiveTrack(_ObjectiveCompletionType):
    __qualname__ = '_UniqueLocationsObjectiveTrack'
    FACTORY_TUNABLES = {'unique_locations_required_to_pass': sims4.tuning.tunable.Tunable(description='\n                The number of unique locations needed to pass.\n                ', tunable_type=int, default=1)}

    def get_number_required(self, test):
        return self.unique_locations_required_to_pass

    @property
    def data_type(self):
        return ObjectiveDataStorageType.IdData

    def increment_data(self, data_object, objective, resolver, result, additional_result):
        if result and additional_result:
            lot_id = services.get_zone(resolver.sim_info.zone_id).lot.lot_id
            if lot_id is not None:
                data_object.add_objective_value(objective, lot_id)
        count = data_object.get_objective_count(objective)
        if count < self.unique_locations_required_to_pass:
            return results.TestResultNumeric(False, 'Objective: not enough matching location iterations.', current_value=count, goal_value=self.unique_locations_required_to_pass, is_money=False)
        return results.TestResult.TRUE

class _IterationsSingleSituation(_ObjectiveCompletionType):
    __qualname__ = '_IterationsSingleSituation'
    FACTORY_TUNABLES = {'iterations_required_to_pass': sims4.tuning.tunable.Tunable(description='\n                The number of times that the objective test must pass in a\n                single situation for the objective to be considered complete.\n                ', tunable_type=int, default=1)}

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.current_situation_id = 0

    def get_number_required(self, test):
        return self.iterations_required_to_pass

    @property
    def data_type(self):
        return ObjectiveDataStorageType.CountData

    def increment_data(self, data_object, objective, resolver, result, additional_result):
        if result and additional_result:
            sim = resolver.sim_info.get_sim_instance()
            if sim is None:
                return results.TestResultNumeric(False, "Objective: Couldn't find sim instance.", current_value=data_object.get_objective_count(objective), goal_value=self.iterations_required_to_pass, is_money=False)
            user_facing_situation_id = 0
            for situation in services.get_zone_situation_manager().get_situations_sim_is_in(sim):
                while situation.is_user_facing:
                    user_facing_situation_id = situation.id
                    break
            if user_facing_situation_id == 0:
                return results.TestResultNumeric(False, 'Objective: Sim is not currently in a situation.', current_value=data_object.get_objective_count(objective), goal_value=self.iterations_required_to_pass, is_money=False)
            if user_facing_situation_id != self.current_situation_id:
                self.current_situation_id = user_facing_situation_id
                data_object.reset_objective_count(objective)
            data_object.add_objective_value(objective, 1)
        count = data_object.get_objective_count(objective)
        if count < self.iterations_required_to_pass:
            return results.TestResultNumeric(False, 'Objective: not enough iterations.', current_value=count, goal_value=self.iterations_required_to_pass, is_money=False)
        return results.TestResult.TRUE

class _UseTestResult(_ObjectiveCompletionType):
    __qualname__ = '_UseTestResult'
    FACTORY_TUNABLES = {'only_use_result_on_home_zone': sims4.tuning.tunable.Tunable(description="\n                If checked then no results will be calculated or replaced if\n                the test event is triggered on a lot other than the sim's home\n                zone.\n                ", tunable_type=bool, default=False)}

    def get_number_required(self, test):
        return test.goal_value()

    @property
    def data_type(self):
        return ObjectiveDataStorageType.CountData

    def get_if_money(self, test):
        return test.is_goal_value_money

    def check_if_should_test(self, resolver):
        if not self.only_use_result_on_home_zone:
            return True
        return services.active_household().home_zone_id == services.current_zone_id()

    def increment_data(self, data_object, objective, resolver, result, additional_result):
        if result:
            data_object.set_objective_value(objective, self.get_number_required(objective.objective_test))
            return results.TestResult.TRUE
        if not isinstance(result, TestResultNumeric):
            return result
        data_object.set_objective_value(objective, result.current_value)
        return result

    def check_objective_validity(self, objective):
        if objective.additional_tests:
            logger.error('Additional tests tuned on objective {}.  These tests will not be run with the Use Test Result Completion type.', objective)

class Objective(metaclass=HashedTunedInstanceMetaclass, manager=services.get_instance_manager(sims4.resources.Types.OBJECTIVE)):
    __qualname__ = 'Objective'
    INSTANCE_TUNABLES = {'display_text': TunableLocalizedStringFactory(description='Text used to show the progress of this objective in the UI', allow_none=True, export_modes=sims4.tuning.tunable_base.ExportModes.All), 'objective_test': TunableObjectiveTestVariant(description='Type of test used for this objective'), 'additional_tests': event_testing.tests.TunableTestSet(description='Additional tests that must be true when the Objective Test passes in order for the Objective consider having passed.'), 'satisfaction_points': sims4.tuning.tunable.Tunable(description='\n            The number of satisfaction points, if relevant, that completion of this objective awards.', tunable_type=int, default=0, export_modes=sims4.tuning.tunable_base.ExportModes.All), 'objective_completion_type': TunableVariant(iterations=_IterationsObjectiveTrack.TunableFactory(), sim_info_statistic=SimInfoStatisticObjectiveTrack.TunableFactory(), unique_targets=_UniqueTargetsObjectiveTrack.TunableFactory(), unique_locations=_UniqueLocationsObjectiveTrack.TunableFactory(), tag_checklist=_TagChecklistObjectiveTrack.TunableFactory(), iterations_single_situation=_IterationsSingleSituation.TunableFactory(), use_test_result=_UseTestResult.TunableFactory(), default='iterations', description='\n                           The type of check that will be used to determine if\n                           this objective passes or fails.\n                           \n                           iterations: This tests the total number of times\n                           that the tests have passed.  The objective is\n                           considered complete when the the number of times\n                           it has passed is equal to the tuned number of times\n                           it should pass.\n                           \n                           unique_targets: Works just like the \'iterations\'\n                           type of objective completion type but only counts\n                           unique ids of the targets of the test rather than\n                           counting all of them.  Not all tests are supported\n                           with this objective completion type.\n                           Currently supported tests:\n                           ran_interaction_test\n                           crafted_object\n                           \n                           unique_locations: Works just like unique targets, except it only\n                           increments if the objective was completed in a unique location (lot id)\n                           than any previous successful completion.\n                           \n                           sim_info_statistic: Counts the number of times that\n                           the tests have passed.  Objectives tuned with this\n                           objective completion type will never pass and\n                           instead just propagate the number of times the test\n                           has passed back to their caller.\n                           \n                           tag_checklist: Track an iteration count of one completion per tag \n                           tuned on the list. Ex. Paint 4 paintings of different genres, in this \n                           case you would tune a count of "4" and add all genre tags to the tag \n                           list. Each painting created would only count if it was not from a genre \n                           tag previously entered. In order to support this functionality, each \n                           painting object created would need to be tagged with it\'s genre upon \n                           creation, which can be tuned in Recipe.\n                           \n                           iterations_single_situation: This tests the total \n                           number of times that the tests have passed during a\n                           single situation.  If the situation ends, the count\n                           will reset when the tests pass the for first time \n                           during a new situation.  The objective is\n                           considered complete when the the number of times\n                           it has passed is equal to the tuned number of times\n                           it should pass.\n                           '), 'resettable': sims4.tuning.tunable.Tunable(description='\n            Setting this allows for this objective to reset back to zero for certain uses, such as for Whim Set activation.\n            ', tunable_type=bool, default=False), 'relative_to_unlock_moment': sims4.tuning.tunable.Tunable(description="\n            If true this objective will start counting from the moment of assignment or reset instead\n            of over the total lifetime of a Sim, most useful for Careers and Whimsets. Note:\n            this effect is only for 'Total' data tests (tests that used persisted save data)\n             ", tunable_type=bool, default=False), 'tooltip': TunableLocalizedStringFactory(description='\n            Tooltip that will display on the objective.\n            ', allow_none=True, export_modes=sims4.tuning.tunable_base.ExportModes.All)}

    @classmethod
    def goal_value(cls):
        return cls.objective_completion_type.get_number_required(cls.objective_test)

    @classproperty
    def is_goal_value_money(cls):
        return cls.objective_completion_type.get_if_money(cls.objective_test)

    @classproperty
    def data_type(cls):
        return cls.objective_completion_type.data_type

    @classmethod
    def _verify_tuning_callback(cls):
        cls.objective_test.validate_tuning_for_objective(cls)
        cls.objective_completion_type.check_objective_validity(cls)
        if cls.objective_test.USES_DATA_OBJECT:
            pass

    @classmethod
    def _get_current_iterations_test_result(cls, objective_data):
        return results.TestResultNumeric(False, 'Objective: not enough iterations.', current_value=objective_data.get_objective_count(cls.guid64), goal_value=cls.objective_completion_type.get_number_required(), is_money=False)

    @classmethod
    def run_test(cls, event, resolver, objective_data=None):
        if event not in cls.objective_test.test_events and event != TestEvent.UpdateObjectiveData:
            return results.TestResult(False, 'Objective test not present in event set.')
        if not cls.objective_completion_type.check_if_should_test(resolver):
            return results.TestResult(False, 'Objective completion type prevents testing this objective.')
        test_result = resolver(cls.objective_test, objective_data, cls.guid64)
        if test_result:
            additional_test_results = cls.additional_tests.run_tests(resolver)
        else:
            additional_test_results = results.TestResult.NONE
        return cls.objective_completion_type.increment_data(objective_data, cls, resolver, test_result, additional_test_results)

    @classmethod
    def reset_objective(cls, objective_data):
        objective_data.reset_objective_count(cls)
        cls.set_starting_point(objective_data)

    @classmethod
    def set_starting_point(cls, objective_data):
        if cls.relative_to_unlock_moment:
            cls.objective_test.save_relative_start_values(cls.guid64, objective_data)
            return True
        return False

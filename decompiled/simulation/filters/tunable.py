import math
import random
from careers.career_enums import CareerCategory
from cas.cas import get_tags_from_outfit
from relationships.relationship_track import RelationshipTrack
from sims.genealogy_tracker import FamilyRelationshipIndex
from sims.sim_info_types import Age, Gender
from sims4.tuning.instances import TunedInstanceMetaclass
from sims4.tuning.tunable import Tunable, TunableEnumEntry, TunableList, TunableVariant, HasTunableSingletonFactory, HasTunableReference, TunableSet, OptionalTunable, AutoFactoryInit, TunableReference, TunablePackSafeReference, TunableTuple, TunableRange
from sims4.utils import flexmethod, classproperty
from singletons import DEFAULT
from statistics.skill import Skill
from world import region
import filters.household_template
import services
import sims4.log
import sims4.resources
import tag
logger = sims4.log.Logger('SimFilter')

class FilterResult:
    __qualname__ = 'FilterResult'
    TRUE = None
    FALSE = None

    def __init__(self, *args, score=1, sim_info=None, conflicting_career_track_id=None):
        if args:
            (self._reason, self._format_args) = (args[0], args[1:])
        else:
            (self._reason, self._format_args) = (None, ())
        self.score = score
        self.sim_info = sim_info
        self.conflicting_career_track_id = conflicting_career_track_id

    @property
    def reason(self):
        if self._format_args and self._reason:
            self._reason = self._reason.format(self._format_args)
            self._format_args = ()
        return self._reason

    def __repr__(self):
        if self.reason:
            return '<FilterResult: sim_info {0} score{1} reason {2}>'.format(self.sim_info, self.score, self.reason)
        return '<FilterResult: sim_info {0} score{1}>'.format(self.sim_info, self.score)

    def __bool__(self):
        return self.score != 0

    def combine_with_other_filter_result(self, other):
        if self.sim_info is not None:
            if self.sim_info != other.sim_info:
                raise AssertionError('Attempting to combine filter results between 2 different sim infos: {} and {}'.format(self.sim_info, other.sim_info))
        else:
            self.sim_info = other.sim_info
        if self._reason is None:
            self._reason = other._reason
            self._format_args = other._format_args
        if self.conflicting_career_track_id is None:
            self.conflicting_career_track_id = other.conflicting_career_track_id

FilterResult.TRUE = FilterResult()
FilterResult.FALSE = FilterResult(score=0)

class BaseFilterTerm(HasTunableSingletonFactory, AutoFactoryInit):
    __qualname__ = 'BaseFilterTerm'

    @property
    def is_sim_info_conformable(self):
        raise NotImplementedError

    def calculate_score(self, **kwargs):
        raise NotImplementedError

    def conform_sim_creator_to_filter_term(self, **kwargs):
        return FilterResult.TRUE

    def conform_sim_info_to_filter_term(self, created_sim_info, **kwargs):
        return self.calculate_score(created_sim_info, **kwargs)

class InvertibleFilterTerm(BaseFilterTerm):
    __qualname__ = 'InvertibleFilterTerm'
    FACTORY_TUNABLES = {'invert_score': Tunable(description='\n            Invert the score so that the filter term will score is the opposite\n            of what the score would be.\n            \n            For example, if sim is busy, normally would return 1, but if checked\n            value would return 0 and would not be chosen by filter system.\n            ', tunable_type=bool, default=False)}

    def invert_score_if_necessary(self, score):
        if self.invert_score:
            return 1 - score
        return score

def calculate_score_from_value(value, min_value, max_value, ideal_value):
    if ideal_value == value:
        return 1
    min_value -= 1
    max_value += 1
    score = 0
    if value < ideal_value:
        score = (value - min_value)/(ideal_value - min_value)
    else:
        score = (max_value - value)/(max_value - ideal_value)
    return max(0, min(1, score))

class SkillFilterTerm(InvertibleFilterTerm):
    __qualname__ = 'SkillFilterTerm'

    @staticmethod
    def _verify_tunable_callback(instance_class, tunable_name, source, value):
        if value.skill is None:
            logger.error('TunableSkillFilterTerm {} has a filter term {} with invalid Skill.', source, tunable_name)
        if value.ideal_value < value.min_value or value.ideal_value > value.max_value:
            logger.error('TunableSkillFilterTerm {} has a filter term {} that is tuned with ideal_value {} outside of the minimum and maximum bounds [{}, {}].', source, tunable_name, value.ideal_value, value.min_value, value.max_value)

    FACTORY_TUNABLES = {'skill': Skill.TunableReference(description='\n            The skill the range applies to.\n            '), 'min_value': Tunable(description='\n            Minimum value of the skill that we are filtering against.\n            ', tunable_type=int, default=0), 'max_value': Tunable(description='\n            Maximum value of the skill that we are filtering against.\n            ', tunable_type=int, default=10), 'ideal_value': Tunable(description='\n            Ideal value of the skill that we are filtering against.\n            ', tunable_type=int, default=5), 'verify_tunable_callback': _verify_tunable_callback}

    @property
    def is_sim_info_conformable(self):
        return True

    def calculate_score(self, sim_info, **kwargs):
        tracker = sim_info.get_tracker(self.skill)
        value = tracker.get_user_value(self.skill)
        score = calculate_score_from_value(value, self.min_value, self.max_value, self.ideal_value)
        return FilterResult(score=self.invert_score_if_necessary(score), sim_info=sim_info)

    def conform_sim_info_to_filter_term(self, created_sim_info, **kwargs):
        if not self.skill.can_add(created_sim_info):
            return FilterResult('Unable to add Skill due to entitlement restriction {}.', self.skill, score=0, sim_info=created_sim_info)
        skill_value = created_sim_info.get_stat_value(self.skill)
        if skill_value is None:
            current_user_value = self.skill.convert_to_user_value(self.skill.initial_value)
        else:
            current_user_value = self.skill.convert_to_user_value(skill_value)
        if self.invert_score != self.min_value <= current_user_value <= self.max_value:
            return FilterResult.TRUE
        if self.invert_score:
            min_range = self.min_value - self.skill.min_value_tuning
            max_range = self.skill.max_value_tuning - self.max_value
            if min_range <= 0 and max_range > 0:
                skill_user_value = random.randint(self.max_value, self.skill.max_value_tuning)
            elif min_range > 0 and max_range <= 0:
                skill_user_value = random.randint(self.skill.min_value_tuning, self.min_value)
            elif min_range > 0 and max_range > 0:
                chosen_level = random.randint(0, min_range + max_range)
                if chosen_level > min_range:
                    skill_user_value = chosen_level - min_range + self.max_value
                else:
                    skill_user_value = chosen_level + self.skill.min_value_tuning
                    FilterResult('Failed to add proper skill level to sim.', sim_info=created_sim_info, score=0)
            else:
                FilterResult('Failed to add proper skill level to sim.', sim_info=created_sim_info, score=0)
        elif self.min_value == self.max_value:
            skill_user_value = self.ideal_value
        else:
            skill_user_value = round(random.triangular(self.min_value, self.max_value, self.ideal_value))
        skill_value = self.skill.convert_from_user_value(skill_user_value)
        created_sim_info.add_statistic(self.skill, skill_value)
        return FilterResult.TRUE

class CareerFilterTerm(InvertibleFilterTerm):
    __qualname__ = 'CareerFilterTerm'

    class _CareerTypeExplicit(HasTunableSingletonFactory, AutoFactoryInit):
        __qualname__ = 'CareerFilterTerm._CareerTypeExplicit'
        FACTORY_TUNABLES = {'career_type': TunableReference(description='\n                The career that this term applies to. Sims that are conformed to the\n                filter will enter the career at its entry level.\n                ', manager=services.get_instance_manager(sims4.resources.Types.CAREER))}

        def get_career_for_conformation(self, sim_info):
            return self.career_type

        def get_career_for_score(self, sim_info):
            career_tracker = sim_info.career_tracker
            return career_tracker.get_career_by_uid(self.career_type.guid64)

    class _CareerTypeFromCategory(HasTunableSingletonFactory, AutoFactoryInit):
        __qualname__ = 'CareerFilterTerm._CareerTypeFromCategory'
        FACTORY_TUNABLES = {'career_category': TunableEnumEntry(description='\n                The career category that this filter term applies to. Sims may\n                be conformed if we want to remove the career, i.e. the term is\n                inverted, but may not be conformed to join a career.\n                ', tunable_type=CareerCategory, default=CareerCategory.Work)}

        def get_career_for_conformation(self, sim_info):
            return self.get_career_for_score(sim_info)

        def get_career_for_score(self, sim_info):
            career_tracker = sim_info.career_tracker
            return career_tracker.get_career_by_category(self.career_category)

    class _CareerLocationFilterActiveLot(HasTunableSingletonFactory):
        __qualname__ = 'CareerFilterTerm._CareerLocationFilterActiveLot'

        def calculate_career_score(self, career):
            career_location = career.get_career_location()
            if career_location.get_zone_id() != services.current_zone_id():
                return 0
            return 1

        def conform_career(self, career):
            career_location = career.get_career_location()
            career_location.set_zone_id(services.current_zone_id())

    class _CareerScheduleFilterAvailableNow(HasTunableSingletonFactory):
        __qualname__ = 'CareerFilterTerm._CareerScheduleFilterAvailableNow'

        def calculate_career_score(self, career):
            if career.is_time_during_shift():
                return 1
            return 0

        def conform_career(self, career):
            pass

    FACTORY_TUNABLES = {'career': TunableVariant(description='\n            Define the career that this filter term applies to.\n            ', from_explicit_type=_CareerTypeExplicit.TunableFactory(), from_category=_CareerTypeFromCategory.TunableFactory(), default='from_explicit_type'), 'career_location': TunableVariant(description='\n            Define how important career location is for this filter. Leave None\n            if location is not important.\n            ', on_active_lot=_CareerLocationFilterActiveLot.TunableFactory(), default=None), 'career_schedule': TunableVariant(description='\n            Define how scheduling affects this filter. Leave None if schedule is\n            not important.\n            ', available_now=_CareerScheduleFilterAvailableNow.TunableFactory(), default=None), 'career_seniority': Tunable(description='\n            If enabled, Sims that have been in the career for longer will score\n            higher than Sims that have been in the career for less time.\n            ', tunable_type=bool, default=True), 'career_user_level': OptionalTunable(description='\n            If enabled we add an additional requirement of the user level of\n            the career to the requirements.  If we conform or generate a sim\n            with this filter and the career has branches a branch will be\n            chosen at random.\n            ', tunable=TunableRange(description='\n                The specific user level that will be looked at for this career.\n                ', tunable_type=int, default=1, minimum=1))}

    @property
    def is_sim_info_conformable(self):
        return True

    def calculate_score(self, sim_info, **kwargs):
        career = self.career.get_career_for_score(sim_info)
        if career is not None:
            score = 1
            reason = ''
            if self.career_location is not None:
                score *= self.career_location.calculate_career_score(career)
                if score == 0:
                    reason = 'Career Location returned score of 0'
            if self.career_schedule is not None:
                score *= self.career_schedule.calculate_career_score(career)
                if score == 0 and not reason:
                    reason = 'Career Schedule returned score of 0'
            if self.career_seniority:
                score *= career.get_career_seniority()
                if score == 0 and not reason:
                    reason = 'Career Seniority returned score of 0'
            if self.career_user_level != career.user_level:
                score = 0
                if not reason:
                    reason = 'Career user level does not match actual career level.'
        else:
            score = 0
            reason = 'Career is None'
        return FilterResult(reason, score=self.invert_score_if_necessary(score), sim_info=sim_info)

    def conform_sim_info_to_filter_term(self, created_sim_info, **kwargs):
        career = self.career.get_career_for_conformation(created_sim_info)
        if career is not None:
            career_tracker = created_sim_info.career_tracker
            if self.invert_score:
                career_tracker.remove_career(career.guid64)
            elif career.is_valid_career(created_sim_info):
                career = career(created_sim_info)
                if self.career_location is not None:
                    self.career_location.conform_career(career)
                if self.career_schedule is not None:
                    self.career_schedule.conform_career(career)
                career_tracker.add_career(career, user_level_override=self.career_user_level)
        return self.calculate_score(created_sim_info)

class TraitFilterTerm(InvertibleFilterTerm):
    __qualname__ = 'TraitFilterTerm'
    FACTORY_TUNABLES = {'trait': TunablePackSafeReference(description='\n            The trait to filter against.\n            ', manager=services.get_instance_manager(sims4.resources.Types.TRAIT))}

    @property
    def is_sim_info_conformable(self):
        return True

    def calculate_score(self, sim_info, **kwargs):
        score = 0
        if self.trait is not None and sim_info.trait_tracker.has_trait(self.trait):
            score = 1
        return FilterResult(score=self.invert_score_if_necessary(score), sim_info=sim_info)

    def conform_sim_creator_to_filter_term(self, *, sim_creator, **kwargs):
        if self.trait is None:
            if self.invert_score:
                return FilterResult.TRUE
            return FilterResult('Failed to conform sim creator to filter since trait is None in trait filter term.', score=0)
        else:
            if not self.invert_score:
                sim_creator.traits.add(self.trait)
            return FilterResult.TRUE

    def conform_sim_info_to_filter_term(self, created_sim_info, **kwargs):
        if self.trait is None and self.invert_score:
            return FilterResult.TRUE
        if self.invert_score != created_sim_info.trait_tracker.has_trait(self.trait):
            return FilterResult.TRUE
        if self.invert_score:
            if not created_sim_info.remove_trait(self.trait):
                return FilterResult('Failed conform sim to filter by removing trait {}', self.trait, sim_info=created_sim_info, score=0)
        elif not created_sim_info.add_trait(self.trait):
            return FilterResult('Failed conform sim to filter by adding trait {}', self.trait, sim_info=created_sim_info, score=0)
        return FilterResult.TRUE

class AgeFilterTerm(InvertibleFilterTerm):
    __qualname__ = 'AgeFilterTerm'

    @staticmethod
    def _verify_tunable_callback(instance_class, tunable_name, source, value):
        if value.ideal_value < value.min_value or value.ideal_value > value.max_value:
            logger.error('TunableAgeFilterTerm {} has a filter term {} that is tuned with ideal_value {} outside of the minimum and maximum bounds [{}, {}].'.format(source, tunable_name, value.ideal_value, value.min_value, value.max_value))

    FACTORY_TUNABLES = {'min_value': TunableEnumEntry(description='\n            The minimum age of the sim we are filtering for.\n            ', tunable_type=Age, default=Age.BABY), 'max_value': TunableEnumEntry(description='\n            The maximum age of the sim we are filtering for.\n            ', tunable_type=Age, default=Age.ELDER), 'ideal_value': TunableEnumEntry(description='\n            The ideal age of the sim we are filtering for.\n            ', tunable_type=Age, default=Age.ADULT), 'verify_tunable_callback': _verify_tunable_callback}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._min_value_int = int(math.log(int(self.min_value), 2))
        self._max_value_int = int(math.log(int(self.max_value), 2))
        self._ideal_value_int = int(math.log(int(self.ideal_value), 2))

    @property
    def is_sim_info_conformable(self):
        return False

    def calculate_score(self, sim_info, **kwargs):
        value = int(math.log(int(sim_info.age), 2))
        score = calculate_score_from_value(value, self._min_value_int, self._max_value_int, self._ideal_value_int)
        return FilterResult(score=self.invert_score_if_necessary(score), sim_info=sim_info)

    def conform_sim_creator_to_filter_term(self, *, sim_creator, **kwargs):
        if self.invert_score and sim_creator.age != self.ideal_value != self.min_value <= sim_creator.age <= self.max_value:
            return FilterResult.TRUE
        if self.invert_score:
            if self.min_value == sim_creator.age == self.max_value:
                return FilterResult('Cannot find valid age in order to conform sim to age filter term.', score=0)
            if self.min_value == sim_creator.age:
                sim_creator.age = self.max_value
            elif self.max_value == sim_creator.age:
                sim_creator.age = self.min_value
            else:
                sim_creator.age = random.choice([self.min_value, self.max_value])
        else:
            sim_creator.age = self.ideal_value
        return FilterResult.TRUE

class GenderFilterTerm(BaseFilterTerm):
    __qualname__ = 'GenderFilterTerm'
    FACTORY_TUNABLES = {'gender': TunableEnumEntry(description='\n            The required gender of the sim we are filtering for.\n            ', tunable_type=Gender, default=Gender.MALE)}

    @property
    def is_sim_info_conformable(self):
        return False

    def calculate_score(self, sim_info, **kwargs):
        if sim_info.gender is self.gender:
            return FilterResult(score=1, sim_info=sim_info)
        return FilterResult(score=0, sim_info=sim_info)

    def conform_sim_creator_to_filter_term(self, *, sim_creator, **kwargs):
        sim_creator.gender = self.gender
        return FilterResult.TRUE

class CasTagsFilterTerm(BaseFilterTerm):
    __qualname__ = 'CasTagsFilterTerm'
    FACTORY_TUNABLES = {'cas_tags': TunableSet(description='\n            The Create a Sim tags that will be filtered for.\n            ', tunable=TunableEnumEntry(tunable_type=tag.Tag, default=tag.Tag.INVALID))}

    @property
    def is_sim_info_conformable(self):
        return False

    def calculate_score(self, sim_info, **kwargs):
        try:
            tags = get_tags_from_outfit(sim_info._base, *sim_info.get_current_outfit())
        except Exception as exc:
            logger.error('Failed to calculate CAS Filter Tag Term for Sim {} with current outfit: {}\nException: {}', sim_info, sim_info.get_current_outfit(), exc, owner='bhill', trigger_breakpoint=True)
            return FilterResult(score=0, sim_info=sim_info)
        if self.cas_tags & set().union(*tags.values()):
            return FilterResult(score=1, sim_info=sim_info)
        return FilterResult(score=0, sim_info=sim_info)

    def conform_sim_creator_to_filter_term(self, *, sim_creator, **kwargs):
        sim_creator.tag_set.extend(self.cas_tags)
        return FilterResult.TRUE

class IsBusyFilterTerm(InvertibleFilterTerm):
    __qualname__ = 'IsBusyFilterTerm'
    OVERRIDE_SELECTABLE = 1
    OVERRIDE_NPC = 2
    FACTORY_TUNABLES = {'override': TunableVariant(description='\n            Determine a set of Sims that is never considered busy.\n            ', locked_args={'no_override': None, 'selectable_sims': OVERRIDE_SELECTABLE, 'npc_sims': OVERRIDE_NPC}, default='no_override')}

    @property
    def is_sim_info_conformable(self):
        return False

    def _is_sim_overriden(self, sim_info):
        if self.override == self.OVERRIDE_SELECTABLE:
            return sim_info.is_selectable
        if self.override == self.OVERRIDE_NPC:
            return not sim_info.is_selectable
        return False

    def calculate_score(self, sim_info, start_time_ticks=None, end_time_ticks=None, **kwargs):
        is_busy = 0
        for career in sim_info.career_tracker.careers.values():
            busy_times = career.get_busy_time_periods()
            if start_time_ticks is not None and end_time_ticks is not None:
                for (busy_start_time, busy_end_time) in busy_times:
                    while start_time_ticks <= busy_end_time and end_time_ticks >= busy_start_time:
                        is_busy = 1
                        break
            else:
                current_time = services.time_service().sim_now
                current_time_in_ticks = current_time.time_since_beginning_of_week().absolute_ticks()
                for (busy_start_time, busy_end_time) in busy_times:
                    while busy_start_time <= current_time_in_ticks <= busy_end_time:
                        is_busy = 1
                        break
        score = self.invert_score_if_necessary(is_busy)
        if is_busy and self._is_sim_overriden(sim_info):
            return FilterResult(sim_info=sim_info, conflicting_career_track_id=career.current_track_tuning.guid64)
        return FilterResult(score=score, sim_info=sim_info)

class IsNeighborFilterTerm(InvertibleFilterTerm):
    __qualname__ = 'IsNeighborFilterTerm'

    @property
    def is_sim_info_conformable(self):
        return False

    def _is_neighbor(self, sim_info, requesting_sim_info):
        if sim_info.household is None or requesting_sim_info is None or requesting_sim_info.household is None:
            return False
        home_zone_id = sim_info.household.home_zone_id
        target_home_zone_id = requesting_sim_info.household.home_zone_id
        if home_zone_id == target_home_zone_id:
            return False
        if home_zone_id == 0 or target_home_zone_id == 0:
            return False
        sim_home_zone_proto_buffer = services.get_persistence_service().get_zone_proto_buff(home_zone_id)
        target_sim_home_zone_proto_buffer = services.get_persistence_service().get_zone_proto_buff(target_home_zone_id)
        if sim_home_zone_proto_buffer is None or target_sim_home_zone_proto_buffer is None:
            return False
        if sim_home_zone_proto_buffer.world_id != target_sim_home_zone_proto_buffer.world_id:
            return False
        return True

    def calculate_score(self, sim_info, requesting_sim_info=None, **kwargs):
        is_neighbor = 0
        if self._is_neighbor(sim_info, requesting_sim_info):
            is_neighbor = 1
        score = self.invert_score_if_necessary(is_neighbor)
        return FilterResult(score=score, sim_info=sim_info)

    def conform_sim_creator_to_filter_term(self, *, sim_creator, **kwargs):
        if not self.invert_score:
            return FilterResult('Unable to create a neighbor sim', score=0)
        return FilterResult.TRUE

class LivesTogetherFilterTerm(InvertibleFilterTerm):
    __qualname__ = 'LivesTogetherFilterTerm'
    FACTORY_TUNABLES = {'consider_household': Tunable(description='\n            If enabled, this will succeed if the Sim is in the same household as\n            the requesting Sim.\n            ', tunable_type=bool, default=True), 'consider_travel_group': Tunable(description='\n            If enabled, this will succeed if the Sim is in the same household as\n            the requesting Sim.\n            ', tunable_type=bool, default=True), 'exclude_sims_in_both': Tunable(description="\n            If enabled, this will fail if the Sim is in both the same household\n            and travel group. If false, it doesn't care.\n            ", tunable_type=bool, default=False)}

    @property
    def is_sim_info_conformable(self):
        return False

    def calculate_score(self, sim_info, requesting_sim_info=None, **kwargs):
        if requesting_sim_info is None:
            return FilterResult(score=0, sim_info=sim_info)
        in_household = self.consider_household and sim_info.household_id == requesting_sim_info.household_id
        in_travel_group = self.consider_travel_group and sim_info.travel_group_id == requesting_sim_info.travel_group_id
        score = (in_household or in_travel_group) and not (self.exclude_sims_in_both and (in_travel_group and in_household))
        return FilterResult(score=self.invert_score_if_necessary(int(score)), sim_info=sim_info)

    def conform_sim_creator_to_filter_term(self, **kwargs):
        if not self.invert_score:
            return FilterResult('Unable to create a sim in the household or travel group.', score=0)

class InFamilyFilterTerm(InvertibleFilterTerm):
    __qualname__ = 'InFamilyFilterTerm'

    @property
    def is_sim_info_conformable(self):
        return False

    def calculate_score(self, sim_info, household_id=0, **kwargs):
        score = 1 if sim_info.household_id == household_id else 0
        return FilterResult(score=self.invert_score_if_necessary(score), sim_info=sim_info)

    def conform_sim_creator_to_filter_term(self, **kwargs):
        if not self.invert_score:
            return FilterResult('Unable to create a sim in a household.', score=0)
        return FilterResult.TRUE

class IsGhostFilterTerm(InvertibleFilterTerm):
    __qualname__ = 'IsGhostFilterTerm'
    FACTORY_TUNABLES = {'require_npc': Tunable(description=' \n            If enabled, the Sim needs to be an NPC. If not enabled, any Sim that\n            is a ghost will pass the filter.\n            ', tunable_type=bool, default=True), 'always_pass_active_household_sims': Tunable(description='\n            If enabled then we will always pass sims in the active household,\n            whether they are a ghost or not.\n            ', tunable_type=bool, default=False)}

    @property
    def is_sim_info_conformable(self):
        return False

    def calculate_score(self, sim_info, **kwargs):
        if self.always_pass_active_household_sims and sim_info.is_selectable:
            return FilterResult.TRUE
        if sim_info.is_ghost:
            score = 0 if self.require_npc and not sim_info.is_npc else 1
        else:
            score = 0
        return FilterResult(score=self.invert_score_if_necessary(score), sim_info=sim_info)

    def conform_sim_creator_to_filter_term(self, **kwargs):
        if not self.invert_score:
            return FilterResult('Unable to create a ghost sim.', score=0)
        return FilterResult.TRUE

class InCompatibleRegionFilterTerm(InvertibleFilterTerm):
    __qualname__ = 'InCompatibleRegionFilterTerm'

    @property
    def is_sim_info_conformable(self):
        return False

    def calculate_score(self, sim_info, requesting_sim_info=None, **kwargs):
        score = 0
        if requesting_sim_info is not None:
            region_instance = region.get_region_instance_from_zone_id(requesting_sim_info.zone_id)
            score = int(region_instance.is_sim_info_compatible(sim_info))
        else:
            current_region = services.current_region()
            score = int(current_region.is_sim_info_compatible(sim_info))
        return FilterResult(score=self.invert_score_if_necessary(score), sim_info=sim_info)

class _RelationshipFilterTerm(InvertibleFilterTerm):
    __qualname__ = '_RelationshipFilterTerm'

    class _RelationshipRequestingSim(HasTunableSingletonFactory):
        __qualname__ = '_RelationshipFilterTerm._RelationshipRequestingSim'

        def get_relationships(self, sim_info, requesting_sim_info):
            if sim_info is None or requesting_sim_info is None:
                logger.error("Attempting to get relationships between a sim and None. This can be caused by tuning a rel bit filter term on a filter attached to something that doesn't specify a sim.\nOne fix for this is to add a filter_requesting_sim_id to the SituationGuestList that is being used here.", owner='bhill')
                return ()
            return ((requesting_sim_info, sim_info),)

    class _RelationshipAllKnown(HasTunableSingletonFactory):
        __qualname__ = '_RelationshipFilterTerm._RelationshipAllKnown'

        def get_relationships(self, sim_info, _):
            valid_relationships = []
            for relationship in sim_info.relationship_tracker:
                target_sim_info = relationship.find_target_sim_info()
                while target_sim_info is not None:
                    valid_relationships.append((target_sim_info, sim_info))
            return valid_relationships

    class _RelationshipAllKnownPlayed(HasTunableSingletonFactory):
        __qualname__ = '_RelationshipFilterTerm._RelationshipAllKnownPlayed'

        def get_relationships(self, sim_info, _):
            valid_relationships = []
            for relationship in sim_info.relationship_tracker:
                target_sim_info = relationship.find_target_sim_info()
                while target_sim_info is not None:
                    if target_sim_info.household is None or not target_sim_info.household.is_persistent_npc:
                        valid_relationships.append((target_sim_info, sim_info))
            return valid_relationships

    FACTORY_TUNABLES = {'requesting_sim_override': Tunable(description='\n            If checked then the filter term will always return 1 if the\n            requesting sim info is the sim info we are looking at.\n            ', tunable_type=bool, default=False), 'relationship_selector': TunableVariant(description='\n            Define which relationships are to be considered.\n            ', use_requesting_sim=_RelationshipRequestingSim.TunableFactory(), use_all_sims=_RelationshipAllKnown.TunableFactory(), use_played_sims=_RelationshipAllKnownPlayed.TunableFactory(), default='use_requesting_sim')}

    @property
    def is_sim_info_conformable(self):
        return True

    def calculate_score(self, sim_info, requesting_sim_info=None, **kwargs):
        if self.requesting_sim_override and sim_info is requesting_sim_info:
            return FilterResult(score=self.invert_score_if_necessary(1), sim_info=sim_info)
        relationships = self.relationship_selector.get_relationships(sim_info, requesting_sim_info)
        if not relationships:
            return FilterResult(score=self.invert_score_if_necessary(0), sim_info=sim_info)
        score = 1
        for relationship in relationships:
            score *= self._calculate_relationship_score(*relationship)
            while not score:
                break
        return FilterResult(score=self.invert_score_if_necessary(score), sim_info=sim_info)

    def conform_sim_info_to_filter_term(self, created_sim_info, requesting_sim_info=None, **kwargs):
        for relationship in self.relationship_selector.get_relationships(created_sim_info, requesting_sim_info):
            self._conform_relationship(*relationship)
        return self.calculate_score(created_sim_info, requesting_sim_info=requesting_sim_info, **kwargs)

    def _calculate_relationship_score(self, sim_info, requesting_sim_info):
        raise NotImplementedError

    def _conform_relationship(self, sim_info, created_sim_info):
        raise NotImplementedError

class RelationshipBitFilterTerm(_RelationshipFilterTerm):
    __qualname__ = 'RelationshipBitFilterTerm'
    FACTORY_TUNABLES = {'white_list': TunableSet(description='\n            A set of relationship bits that requires the requesting sim to have\n            at least one matching relationship bit with the sims we are scoring.\n            ', tunable=TunableReference(description='\n                A relationship bit that we will use to check if the requesting\n                sim has it with the sim we are scoring.\n                ', manager=services.get_instance_manager(sims4.resources.Types.RELATIONSHIP_BIT))), 'black_list': TunableSet(description='\n            A set of relationship bits that requires the requesting sim to not\n            have any one matching relationship bits with the sims we are\n            scoring.\n            ', tunable=TunableReference(description='\n                A relationship bit that we will use to check if the requesting\n                sim has it with the sim we are scoring.\n                ', manager=services.get_instance_manager(sims4.resources.Types.RELATIONSHIP_BIT)))}

    def _calculate_relationship_score(self, sim_info, requesting_sim_info):
        relationship_bits = set(sim_info.relationship_tracker.get_all_bits(target_sim_id=requesting_sim_info.sim_id))
        if self.white_list and not relationship_bits & self.white_list:
            return 0
        if self.black_list and relationship_bits & self.black_list:
            return 0
        return 1

    def _conform_relationship(self, sim_info, created_sim_info):
        for rel_bit in self.white_list:
            sim_info.relationship_tracker.add_relationship_bit(created_sim_info.sim_id, rel_bit, force_add=True)
        for rel_bit in self.black_list:
            sim_info.relationship_tracker.remove_relationship_bit(created_sim_info.sim_id, rel_bit)

class RelationshipTrackFilterTerm(_RelationshipFilterTerm):
    __qualname__ = 'RelationshipTrackFilterTerm'
    FACTORY_TUNABLES = {'min_value': Tunable(description='\n            The minimum value of the relationship track that we are filtering\n            against.\n            ', tunable_type=int, default=-100), 'max_value': Tunable(description='\n            The maximum value of the relationship track that we are filtering\n            against.\n            ', tunable_type=int, default=100), 'ideal_value': Tunable(description='\n            Ideal value of the relationship track that we are filtering against.\n            ', tunable_type=int, default=0), 'relationship_track': RelationshipTrack.TunableReference(description='\n            The relationship track that we are filtering against.\n            ')}

    def _calculate_relationship_score(self, sim_info, requesting_sim_info):
        relationship_value = requesting_sim_info.relationship_tracker.get_relationship_score(sim_info.sim_id, self.relationship_track)
        return calculate_score_from_value(relationship_value, self.min_value, self.max_value, self.ideal_value)

    def _conform_relationship(self, sim_info, created_sim_info):
        if self.min_value == self.max_value:
            relationship_value = self.ideal_value
        else:
            relationship_value = round(random.triangular(self.min_value, self.max_value, self.ideal_value))
        created_sim_info.relationship_tracker.set_relationship_score(sim_info.sim_id, relationship_value, self.relationship_track)
        sim_info.relationship_tracker.set_relationship_score(created_sim_info.sim_id, relationship_value, self.relationship_track)

class GenealogyFilterTerm(InvertibleFilterTerm):
    __qualname__ = 'GenealogyFilterTerm'
    FACTORY_TUNABLES = {'family_relationship': TunableEnumEntry(description='\n            This is the family relationship between the requesting sim and the\n            target sim.\n            ', tunable_type=FamilyRelationshipIndex, default=FamilyRelationshipIndex.MOTHER), 'swap_direction': Tunable(description='\n            This is the direction between the requesting sim and the target sim\n            so that the genealogy is checked between the sim info and the\n            requesting sim info.\n            ', tunable_type=bool, default=False)}

    @property
    def is_sim_info_conformable(self):
        return False

    def calculate_score(self, sim_info, requesting_sim_info=None, **kwargs):
        if requesting_sim_info is None:
            return FilterResult(score=0, sim_info=sim_info)
        if self.swap_direction:
            score = 1 if sim_info.get_relation(self.family_relationship) == requesting_sim_info.id else 0
        else:
            score = 1 if requesting_sim_info.get_relation(self.family_relationship) == sim_info.id else 0
        return FilterResult(score=self.invert_score_if_necessary(score), sim_info=sim_info)

    def conform_sim_creator_to_filter_term(self, requesting_sim_info=None, **kwargs):
        if requesting_sim_info is None:
            return FilterResult('Unable to create sims with specific relationship-- requesting sim info is required', score=0)
        return FilterResult.TRUE

    def conform_sim_info_to_filter_term(self, created_sim_info, requesting_sim_info=None, **kwargs):
        if self.invert_score:
            return FilterResult.TRUE
        if self.swap_direction:
            created_sim_info.set_and_propagate_family_relation(self.family_relationship, requesting_sim_info)
            created_sim_info.set_default_relationships(reciprocal=True)
        else:
            requesting_sim_info.set_and_propagate_family_relation(self.family_relationship, created_sim_info)
            requesting_sim_info.set_default_relationships(reciprocal=True)
        return FilterResult.TRUE

class PregnancyFilterTerm(BaseFilterTerm):
    __qualname__ = 'PregnancyFilterTerm'
    FACTORY_TUNABLES = {'pregnancy_progress': OptionalTunable(description='\n            If enabled then we will check a specific \n            ', tunable=TunableTuple(description='\n                ', minimum_value=Tunable(description='\n                    The minimum commodity value that will pass.\n                    ', tunable_type=float, default=0.0), maximum_value=Tunable(description='\n                    The maximum commodity value that will pass.\n                    ', tunable_type=float, default=100.0), ideal_value=Tunable(description='\n                    The ideal commodity value for scoring.\n                    ', tunable_type=float, default=50.0))), 'pregnancy_partner_filter': OptionalTunable(description='\n            Specify how Sims are conformed to match this filter term.\n            ', tunable=TunableReference(description='\n                The filter that will be used to find a pregnancy partner for for\n                this Sim.\n                ', manager=services.get_instance_manager(sims4.resources.Types.SIM_FILTER)), enabled_name='Use_Spouse_or_Filter', disabled_name='Use_Spouse_Exclusively')}

    @property
    def is_sim_info_conformable(self):
        return True

    def calculate_score(self, sim_info, **kwargs):
        if not sim_info.is_pregnant:
            return FilterResult('Sim Info has no pregnancy commodity.', score=0.0, sim_info=sim_info)
        if self.pregnancy_progress is None:
            return FilterResult(sim_info=sim_info)
        from interactions.utils.pregnancy import PregnancyTracker
        pregnancy_stat = sim_info.get_statistic(PregnancyTracker.PREGNANCY_COMMODITY)
        pregnancy_value = pregnancy_stat.get_user_value()
        score = calculate_score_from_value(pregnancy_value, self.pregnancy_progress.minimum_value, self.pregnancy_progress.maximum_value, self.pregnancy_progress.ideal_value)
        return FilterResult(score=score, sim_info=sim_info)

    def conform_sim_info_to_filter_term(self, created_sim_info, **kwargs):
        pregnancy_partner_sim_info = created_sim_info.get_significant_other_sim_info()
        if pregnancy_partner_sim_info is None or pregnancy_partner_sim_info.gender == created_sim_info.gender:
            if self.pregnancy_partner_filter is None:
                return FilterResult('Sim has no spouse or same-sex spouse, and no fallback filter', score=0)
            filter_results = services.sim_filter_service().submit_matching_filter(sim_filter=self.pregnancy_partner_filter, allow_yielding=False)
            if not filter_results:
                return FilterResult('Cannot find sim to be pregnancy partner to make sim pregnant.', score=0)
            pregnancy_partner_sim_info = filter_results[0].sim_info
        created_sim_info.pregnancy_tracker.start_pregnancy(created_sim_info, pregnancy_partner_sim_info)
        if self.pregnancy_progress is not None:
            if self.pregnancy_progress.minimum_value == self.pregnancy_progress.maximum_value:
                pregnancy_value = self.pregnancy_progress.minimum_value
            else:
                pregnancy_value = random.triangular(self.pregnancy_progress.minimum_value, self.pregnancy_progress.maximum_value, self.pregnancy_progress.ideal_value)
            from interactions.utils.pregnancy import PregnancyTracker
            created_sim_info.set_stat_value(PregnancyTracker.PREGNANCY_COMMODITY, pregnancy_value)
        return FilterResult.TRUE

class AgeUpFilterTerm(InvertibleFilterTerm):
    __qualname__ = 'AgeUpFilterTerm'

    @property
    def is_sim_info_conformable(self):
        return False

    def calculate_score(self, sim_info, **kwargs):
        score = 1 if sim_info.can_age_up() else 0
        return FilterResult(score=self.invert_score_if_necessary(score), sim_info=sim_info)

    def conform_sim_creator_to_filter_term(self, **kwargs):
        if not self.invert_score:
            return FilterResult('Unable to create a sim that is ready to age up.', score=0)
        return FilterResult.TRUE

class HasHomeZoneFilterTerm(InvertibleFilterTerm):
    __qualname__ = 'HasHomeZoneFilterTerm'
    FACTORY_TUNABLES = {'include_vacation_home': Tunable(description='\n            If enabled and the Sim is on a vacation at a zone, this will return\n            True.\n            ', tunable_type=bool, default=False)}

    @property
    def is_sim_info_conformable(self):
        return False

    def calculate_score(self, sim_info, **kwargs):
        score = 1 if sim_info.household.home_zone_id != 0 else 0
        score = int(score or self.include_vacation_home and sim_info.is_in_travel_group())
        return FilterResult(score=self.invert_score_if_necessary(score), sim_info=sim_info)

    def conform_sim_creator_to_filter_term(self, **kwargs):
        if not self.invert_score:
            return FilterResult('Unable to create a sim that has a home lot.', score=0)
        return FilterResult.TRUE

class HasHouseholdEverBeenPlayedFilterTerm(InvertibleFilterTerm):
    __qualname__ = 'HasHouseholdEverBeenPlayedFilterTerm'

    @property
    def is_sim_info_conformable(self):
        return False

    def calculate_score(self, sim_info, **kwargs):
        score = 0 if sim_info.household.is_persistent_npc else 1
        return FilterResult(score=self.invert_score_if_necessary(score), sim_info=sim_info)

    def conform_sim_creator_to_filter_term(self, **kwargs):
        if not self.invert_score:
            return FilterResult('Unable to create a sim that has a household that has been played by the player.', score=0)
        return FilterResult.TRUE

class AgeProgressFilterTerm(BaseFilterTerm):
    __qualname__ = 'AgeProgressFilterTerm'

    @staticmethod
    def _verify_tunable_callback(instance_class, tunable_name, source, value):
        if value.ideal_value < value.min_value or value.ideal_value > value.max_value:
            logger.error('TunableAgeProgressFilterTerm {} has a filter term {} that is tuned with ideal_value {} outside of the minimum and maximum bounds [{}, {}].'.format(source, tunable_name, value.ideal_value, value.min_value, value.max_value))

    FACTORY_TUNABLES = {'min_value': Tunable(description='\n            Minimum value of age progress.\n            ', tunable_type=float, default=0), 'max_value': Tunable(description='\n            Maximum value of age progress.\n            ', tunable_type=float, default=10), 'ideal_value': Tunable(description='\n            Ideal value of age progress.\n            ', tunable_type=float, default=5), 'verify_tunable_callback': _verify_tunable_callback}

    @property
    def is_sim_info_conformable(self):
        return True

    def calculate_score(self, sim_info, **kwargs):
        value = sim_info.age_progress
        score = calculate_score_from_value(value, self.min_value, self.max_value, self.ideal_value)
        return FilterResult(score=score, sim_info=sim_info)

    def conform_sim_info_to_filter_term(self, created_sim_info, **kwargs):
        current_value = created_sim_info.age_progress
        if self.min_value <= current_value <= self.max_value:
            return FilterResult.TRUE
        if self.min_value == self.max_value:
            new_age_progress_value = self.ideal_value
        else:
            new_age_progress_value = round(random.triangular(self.min_value, self.max_value, self.ideal_value))
        created_sim_info.age_progress = new_age_progress_value
        return FilterResult.TRUE

class IsNotIncestuousFilterTerm(BaseFilterTerm):
    __qualname__ = 'IsNotIncestuousFilterTerm'

    @property
    def is_sim_info_conformable(self):
        return False

    def calculate_score(self, sim_info, requesting_sim_info=None, **kwargs):
        if requesting_sim_info is None:
            return FilterResult('Trying to check against incest but there is no requesting sim info.', score=0, sim_info=sim_info)
        if requesting_sim_info.incest_prevention_test(sim_info):
            return FilterResult(score=1, sim_info=sim_info)
        return FilterResult('Incest prevention filter failed.', score=0, sim_info=sim_info)

class FilterTermVariant(TunableVariant):
    __qualname__ = 'FilterTermVariant'

    def __init__(self, **kwargs):
        super().__init__(skill=SkillFilterTerm.TunableFactory(), trait=TraitFilterTerm.TunableFactory(), age=AgeFilterTerm.TunableFactory(), gender=GenderFilterTerm.TunableFactory(), cas_tags=CasTagsFilterTerm.TunableFactory(), is_busy=IsBusyFilterTerm.TunableFactory(), in_family=InFamilyFilterTerm.TunableFactory(), is_ghost=IsGhostFilterTerm.TunableFactory(), is_not_incestuous=IsNotIncestuousFilterTerm.TunableFactory(), relationship_bit=RelationshipBitFilterTerm.TunableFactory(), relationship_track=RelationshipTrackFilterTerm.TunableFactory(), can_age_up=AgeUpFilterTerm.TunableFactory(), has_home_zone=HasHomeZoneFilterTerm.TunableFactory(), has_household_ever_been_played=HasHouseholdEverBeenPlayedFilterTerm.TunableFactory(), age_progress=AgeProgressFilterTerm.TunableFactory(), is_neighbor=IsNeighborFilterTerm.TunableFactory(), in_compatible_region=InCompatibleRegionFilterTerm.TunableFactory(), lives_together=LivesTogetherFilterTerm.TunableFactory(), genealogy=GenealogyFilterTerm.TunableFactory(), career=CareerFilterTerm.TunableFactory(), pregnancy=PregnancyFilterTerm.TunableFactory(), default='skill', **kwargs)

class TunableSimFilter(HasTunableReference, metaclass=TunedInstanceMetaclass, manager=services.get_instance_manager(sims4.resources.Types.SIM_FILTER)):
    __qualname__ = 'TunableSimFilter'
    TOP_NUMBER_OF_SIMS_TO_LOOK_AT = Tunable(description='\n        When running a filter request and doing a weighted random, how many of\n        the top scorers will be used to get the results.\n        ', tunable_type=int, default=5)
    BLANK_FILTER = TunableReference(description='\n        A filter that is used when a filter of None is passed in.  This filter\n        should have no filter terms.\n        ', manager=services.get_instance_manager(sims4.resources.Types.SIM_FILTER))
    ANY_FILTER = TunableReference(description='\n        A filter used for creating debug sims in your neighborhood.\n        ', manager=services.get_instance_manager(sims4.resources.Types.SIM_FILTER))
    UNIMPORTANT_FILTER = TunableReference(description='\n        A filter used to find Sims that are considered unimportant. Unimportant\n        Sims can be used as a fallback before generating new SimInfos in the\n        case a filter request fails. This allows us to conserve SimInfos.\n        ', manager=services.get_instance_manager(sims4.resources.Types.SIM_FILTER))
    INSTANCE_TUNABLES = {'_filter_terms': TunableList(description='\n            A list of filter terms that will be used to query the townie pool\n            for sims.\n            ', tunable=FilterTermVariant()), '_template_chooser': TunableReference(description='\n            A reference to a template chooser.  In the case that the filter\n            fails to find any sims that match it, the template chooser will\n            select a template to use that will be used to create a sim.  After\n            that sim is created then the filter will fix up the sim further in\n            order to ensure that the template that the sim defines still meets\n            the criteria of the filter.\n            ', manager=services.get_instance_manager(sims4.resources.Types.TEMPLATE_CHOOSER), allow_none=True), 'use_weighted_random': Tunable(description='\n            If checked will do a weighted random of top results rather than\n            just choosing the best ones.\n            ', tunable_type=bool, default=False), 'repurpose_terms': TunableVariant(description='\n            If specified, then Sims, should any be available, are conformed to\n            the filter terms. Unimportant Sims are defined by the\n            UNIMPORTANT_FILTER global tunable or a specified filter.\n            \n            The purpose of this is to conserve SimInfos when it is not necessary\n            to generate brand new Sims. For example, an unimportant Sim might\n            become the bartender at a venue.\n            ', use_specific_sims=TunableReference(description='\n                Use a specific filter to determine unimportant Sims.\n                ', manager=services.get_instance_manager(sims4.resources.Types.SIM_FILTER)), locked_args={'use_unimportant_sims': DEFAULT, 'dont_repurpose': None}, default='dont_repurpose'), 'repurpose_game_breaker': Tunable(description='\n            If checked, then we can repurpose instanced sims for the results of\n            this filter.\n            \n            DO NOT TUNE THIS UNLESS YOU TALK TO A GPE.  IT WILL POTENTIALLY\n            CAUSE WEIRD SUBTLE BUGS.\n            ', tunable_type=bool, default=False), '_set_household_as_hidden': Tunable(description="\n            If checked, the household created for this template will be hidden.\n            Normally used with household_template_override. e.g. Death's\n            household.\n            ", tunable_type=bool, default=False), '_household_templates_override': OptionalTunable(description='\n            If enabled, when creating sim info use the household template\n            specified.\n            ', tunable=TunableList(tunable=filters.household_template.HouseholdTemplate.TunableReference()))}

    @classmethod
    def _verify_tuning_callback(cls):
        repurpose_filter = cls.UNIMPORTANT_FILTER if cls.repurpose_terms is DEFAULT else cls.repurpose_terms
        if repurpose_filter is not None:
            if cls is repurpose_filter:
                logger.error("{} specifies itself as a repurpose filter. That's not going to do anything useful.", cls)
            template_chooser = repurpose_filter._template_chooser
            if template_chooser is not None:
                logger.error("{} specifies {} as a repurpose filter, but that specifies {} as a template. Repurpose terms can't have templates.", cls, repurpose_filter, template_chooser)
            has_conformable_term = False
            for filter_term in cls.get_filter_terms():
                if filter_term.is_sim_info_conformable:
                    has_conformable_term = True
                else:
                    while has_conformable_term:
                        logger.error('{} specifies repurpose terms, but has non-conformable term {} after other conformable terms. All non-conformable terms must come first.', cls, filter_term)
                        break

    @flexmethod
    def get_filter_terms(cls, inst):
        inst_or_cls = inst if inst is not None else cls
        return inst_or_cls._filter_terms

    @classmethod
    def choose_template(cls):
        if cls._template_chooser is not None:
            return cls._template_chooser.choose_template()

    @classmethod
    def is_aggregate_filter(cls):
        return False

    @flexmethod
    def _conform_sim_info(cls, inst, sim_info, **kwargs):
        inst_or_cls = inst if inst is not None else cls
        for filter_term in inst_or_cls.get_filter_terms():
            result = filter_term.conform_sim_info_to_filter_term(created_sim_info=sim_info, **kwargs)
            while not result:
                return result
        return FilterResult.TRUE

    @flexmethod
    def create_sim_info(cls, inst, zone_id, blacklist_sim_ids=(), **kwargs):
        inst_or_cls = inst if inst is not None else cls
        if inst_or_cls.repurpose_terms is not None:
            repurpose_terms = inst_or_cls.UNIMPORTANT_FILTER if inst_or_cls.repurpose_terms is DEFAULT else inst_or_cls.repurpose_terms
            for unimportant_result in services.sim_filter_service().submit_filter(repurpose_terms, None, blacklist_sim_ids=blacklist_sim_ids, allow_yielding=False, **kwargs):
                result = inst_or_cls._conform_sim_info(unimportant_result.sim_info, **kwargs)
                while result:
                    return FilterResult('Unimportant SimInfo repurposed successfully', sim_info=unimportant_result.sim_info)
        template = cls.choose_template()
        if not template:
            return FilterResult('No template selected, template chooser might not be tuned properly.', score=0)
        sim_creator = template.sim_creator
        for filter_term in inst_or_cls.get_filter_terms():
            result = filter_term.conform_sim_creator_to_filter_term(sim_creator=sim_creator, **kwargs)
            while not result:
                return result
        (household_template_type, insertion_indexes_to_sim_creators) = cls.find_household_template_that_contains_sim_filter((sim_creator,))
        if household_template_type is None:
            return FilterResult('No template selected, there is no household template with minimum age and matching gender of sim_template.', score=0)
        (household, created_sim_infos) = household_template_type.get_sim_infos_from_household(zone_id, insertion_indexes_to_sim_creators, creation_source='filter: {}'.format(cls.__name__))
        household.hidden = cls._set_household_as_hidden
        created_sim_info = created_sim_infos.pop()
        template.add_template_data_to_sim(created_sim_info, sim_creator)
        result = inst_or_cls._conform_sim_info(created_sim_info, **kwargs)
        if not result:
            return result
        return FilterResult('SimInfo created successfully', sim_info=created_sim_info)

    @classmethod
    def find_household_template_that_contains_sim_filter(cls, sim_creations_to_match):
        valid_filter_template_type = []
        if cls._household_templates_override:
            templates_to_iterate_over = cls._household_templates_override
        else:
            templates_to_iterate_over = services.get_instance_manager(sims4.resources.Types.SIM_TEMPLATE).types.values()
        for filter_template_type in templates_to_iterate_over:
            if filter_template_type.template_type == filters.sim_template.SimTemplateType.SIM:
                pass
            index_to_sim_creation = {}
            for sim_creation in sim_creations_to_match:
                for (index, household_member_data) in enumerate(filter_template_type.get_household_members()):
                    if index in index_to_sim_creation:
                        pass
                    if household_member_data.sim_template._sim_creation_info.age_variant is not None and sim_creation.age != household_member_data.sim_template._sim_creation_info.age_variant.min_age:
                        pass
                    possible_gender = household_member_data.sim_template._sim_creation_info.gender
                    if possible_gender is not None and possible_gender != sim_creation.gender:
                        pass
                    index_to_sim_creation[index] = sim_creation
            while len(index_to_sim_creation) == len(sim_creations_to_match):
                valid_filter_template_type.append((filter_template_type, index_to_sim_creation))
        if valid_filter_template_type:
            return random.choice(valid_filter_template_type)
        return (None, None)

class TunableAggregateFilter(HasTunableReference, metaclass=TunedInstanceMetaclass, manager=services.get_instance_manager(sims4.resources.Types.SIM_FILTER)):
    __qualname__ = 'TunableAggregateFilter'
    INSTANCE_TUNABLES = {'leader_filter': TunableSimFilter.TunableReference(description='\n            Sim filter that is used as the leader of the group. All\n            relationships will use this sim as the reference point.\n            '), 'filters': TunableList(description='\n            List of filters for the sims to be included in the group.\n            ', tunable=TunableSimFilter.TunableReference())}

    @classmethod
    def is_aggregate_filter(cls):
        return True

    @classmethod
    def get_filter_count(cls):
        return len(cls.filters) + 1

class DynamicSimFilter(TunableSimFilter):
    __qualname__ = 'DynamicSimFilter'
    INSTANCE_TUNABLES = {'_additional_filter_terms': TunableList(description='\n            A list of filter terms that are going to be used in conjunction with\n            the terms provided by the user of this filter.\n            ', tunable=FilterTermVariant())}
    REMOVE_INSTANCE_TUNABLES = ('_filter_terms',)

    def __init__(self, filter_terms=()):
        self._filter_terms = filter_terms + self._additional_filter_terms

class BlankFilter(TunableSimFilter):
    __qualname__ = 'BlankFilter'
    REMOVE_INSTANCE_TUNABLES = ('_filter_terms',)

    @classproperty
    def _filter_terms(cls):
        return ()

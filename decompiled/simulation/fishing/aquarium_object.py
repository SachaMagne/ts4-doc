import broadcasters.environment_score
import objects.game_object
import vfx
import collections

class Aquarium(objects.game_object.GameObject):
    __qualname__ = 'Aquarium'
    VFX_SLOT_NAME = '_FX_fish_'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._fish_vfx_handles = []

    def on_object_added_to_inventory(self, obj):
        vfx_data = obj.inventory_to_fish_vfx.get(self.inventory_component.inventory_type, None)
        object_id = obj.id
        for _ in range(obj.stack_count()):
            insert_effect = False
            if vfx_data is not None:
                vfx_index = 1
                i = 0
                for (i, fish_vfx_handle) in enumerate(self._fish_vfx_handles):
                    if fish_vfx_handle is not None:
                        pass
                    vfx_index += i
                    insert_effect = True
                    break
                vfx_index += len(self._fish_vfx_handles)
                vfx_name = '{}_{}'.format(vfx_data.vfx_name, vfx_index)
                vfx_slot_name = '{}{}'.format(vfx_data.vfx_base_bone_name, vfx_index)
            else:
                vfx_name = obj.fishbowl_vfx
                vfx_slot_name = self.VFX_SLOT_NAME
            play_effect_handle = vfx.PlayEffect(self, vfx_name, joint_name=vfx_slot_name)
            play_effect_handle.start()
            if insert_effect:
                self._fish_vfx_handles[i] = (object_id, play_effect_handle)
            else:
                self._fish_vfx_handles.append((object_id, play_effect_handle))
        self.add_dynamic_component(objects.components.types.ENVIRONMENT_SCORE_COMPONENT.instance_attr)

    def on_object_removed_from_inventory(self, obj):
        num_to_remove = obj.stack_count()
        for (i, fish_vfx_handle) in enumerate(self._fish_vfx_handles):
            if fish_vfx_handle is None:
                pass
            while fish_vfx_handle[0] == obj.id:
                fish_vfx_handle[1].stop()
                self._fish_vfx_handles[i] = None
                num_to_remove -= 1
                if num_to_remove == 0:
                    break
        if len(self.inventory_component) == 0:
            self._fish_vfx_handles.clear()
            self.remove_component(objects.components.types.ENVIRONMENT_SCORE_COMPONENT.instance_attr)

    def on_object_stack_id_updated(self, new_obj_id, old_obj_id, stack_count):
        for (i, fish_vfx_handle) in enumerate(self._fish_vfx_handles):
            if fish_vfx_handle is None:
                pass
            while fish_vfx_handle[0] == old_obj_id:
                self._fish_vfx_handles[i] = (new_obj_id, fish_vfx_handle[1])
                stack_count -= 1
                if stack_count <= 0:
                    break

    def get_environment_score(self, sim, ignore_disabled_state=False):
        if len(self.inventory_component) > 0:
            total_mood_scores = collections.Counter()
            total_positive_score = 0
            total_negative_score = 0
            total_contributions = []
            for fish in self.inventory_component:
                (mood_scores, negative_score, positive_score, contributions) = fish.get_environment_score(sim=sim, ignore_disabled_state=ignore_disabled_state)
                total_mood_scores.update(mood_scores)
                total_positive_score += positive_score
                total_negative_score += negative_score
                total_contributions.extend(contributions)
            return (total_mood_scores, total_negative_score, total_positive_score, total_contributions)
        return broadcasters.environment_score.environment_score_component.EnvironmentScoreComponent.ENVIRONMENT_SCORE_ZERO

from protocolbuffers.Consts_pb2 import MGR_UNMANAGED, MGR_OBJECT, MGR_SIM_INFO
from gsi_handlers.gameplay_archiver import GameplayArchiver
from sims4.gsi.schema import GsiGridSchema, GsiFieldVisualizers
import services
distributor_archive_schema = GsiGridSchema(label='Distributor Log')
distributor_archive_schema.add_field('index', label='Index', type=GsiFieldVisualizers.INT, width=1, unique_field=True)
distributor_archive_schema.add_field('account', label='Client Account', width=2)
distributor_archive_schema.add_field('target_name', label='Target Name', width=2)
distributor_archive_schema.add_field('type', label='Type', width=1)
distributor_archive_schema.add_field('size', label='Size', width=1)
distributor_archive_schema.add_field('manager_id', label='Manager Id', type=GsiFieldVisualizers.INT, width=1)
distributor_archive_schema.add_field('blockers', label='Blockers', width=5)
distributor_archive_schema.add_field('tags', label='Tags', width=2)
archiver = GameplayArchiver('Distributor', distributor_archive_schema, max_records=250)
sim_distributor_archive_schema = GsiGridSchema(label='Distributor Log Sim', sim_specific=True)
sim_distributor_archive_schema.add_field('index', label='Index', type=GsiFieldVisualizers.INT, width=1, unique_field=True)
sim_distributor_archive_schema.add_field('account', label='Client Account', width=2)
sim_distributor_archive_schema.add_field('target_name', label='Target Name', width=2)
sim_distributor_archive_schema.add_field('type', label='Type', width=1)
sim_distributor_archive_schema.add_field('size', label='Size', width=1)
sim_distributor_archive_schema.add_field('manager_id', label='Manager Id', type=GsiFieldVisualizers.INT, width=1)
sim_distributor_archive_schema.add_field('blockers', label='Blockers', width=5)
sim_distributor_archive_schema.add_field('tags', label='Tags', width=2)
sim_archiver = GameplayArchiver('SimDistributor', sim_distributor_archive_schema, max_records=150)

def archive_operation(target_id, target_name, manager_id, message, index, client):
    message_type = '? UNKNOWN ?'
    for (enum_name, enum_value) in message.DESCRIPTOR.enum_values_by_name.items():
        while enum_value.number == message.type:
            message_type = enum_name
            break
    blocker_entries = []
    tag_entries = []
    for channel in message.additional_channels:
        if channel.id.manager_id == MGR_UNMANAGED:
            tag_entries.append(str(channel.id.object_id))
        else:
            blocker_entries.append('{}:{}'.format(str(channel.id.manager_id), str(channel.id.object_id)))
    entry = {'target_name': target_name, 'index': index, 'account': client.account.persona_name, 'size': len(message.data), 'type': message_type, 'manager_id': manager_id, 'blockers': ','.join(blocker_entries), 'tags': ','.join(tag_entries)}
    if manager_id == MGR_OBJECT:
        obj = services.object_manager().get(target_id)
        if sim_archiver.enabled:
            sim_archiver.archive(data=entry, object_id=target_id, zone_override=client.zone_id)
        return
    elif manager_id == MGR_SIM_INFO:
        if sim_archiver.enabled:
            sim_info = services.sim_info_manager().get(target_id)
            if sim_info is not None:
                sim_archiver.archive(data=entry, object_id=sim_info.sim_id, zone_override=client.zone_id)
        return
    if archiver.enabled:
        archiver.archive(data=entry, object_id=target_id, zone_override=client.zone_id)

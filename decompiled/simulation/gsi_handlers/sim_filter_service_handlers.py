from gsi_handlers.gameplay_archiver import GameplayArchiver
from sims4.gsi.schema import GsiGridSchema, GsiFieldVisualizers

class SimFilterServiceGSILoggingData:
    __qualname__ = 'SimFilterServiceGSILoggingData'

    def __init__(self, request_type, sim_filter_type, yielding):
        self.request_type = request_type
        self.sim_filter_type = sim_filter_type
        self.yielding = yielding
        self.rejected_sim_infos = []
        self.created_sim_infos = []

    def add_created_houshold(self, household):
        for sim_info in household:
            self.created_sim_infos.append({'sim_info': str(sim_info)})

    def add_rejected_sim_info(self, sim_info, reason, filter_term):
        self.rejected_sim_infos.append({'sim_info': str(sim_info), 'reason': reason, 'filter_term': str(filter_term)})

sim_filter_service_archive_schema = GsiGridSchema(label='Sim Filter Service Archive')
sim_filter_service_archive_schema.add_field('request_type', label='Request Type')
sim_filter_service_archive_schema.add_field('yielding', label='Yielding')
sim_filter_service_archive_schema.add_field('matching_results', label='Num Matching', type=GsiFieldVisualizers.INT)
sim_filter_service_archive_schema.add_field('created_sims', label='Num Created', type=GsiFieldVisualizers.INT)
sim_filter_service_archive_schema.add_field('filter_type', label='Filter Type', width=2.5)
with sim_filter_service_archive_schema.add_has_many('FilterResult', GsiGridSchema) as sub_schema:
    sub_schema.add_field('sim_info', label='Sim Info', width=1)
    sub_schema.add_field('score', label='Score', type=GsiFieldVisualizers.FLOAT, width=0.5)
with sim_filter_service_archive_schema.add_has_many('Created', GsiGridSchema) as sub_schema:
    sub_schema.add_field('sim_info', label='Sim Info', width=3)
with sim_filter_service_archive_schema.add_has_many('Rejected', GsiGridSchema) as sub_schema:
    sub_schema.add_field('sim_info', label='Sim Info', width=1)
    sub_schema.add_field('reason', label='Reason', width=1)
    sub_schema.add_field('filter_term', label='Filter Fail', width=2)
archiver = GameplayArchiver('sim_filter_service_archive', sim_filter_service_archive_schema)

def archive_filter_request(filter_results, gsi_logging_data):
    entry = {}
    entry['request_type'] = str(gsi_logging_data.request_type)
    entry['yielding'] = str(gsi_logging_data.yielding)
    entry['filter_type'] = str(gsi_logging_data.sim_filter_type)
    entry['matching_results'] = len(filter_results)
    entry['created_sims'] = len(gsi_logging_data.created_sim_infos)
    filter_results_list = []
    for filter_result in filter_results:
        filter_results_list.append({'sim_info': str(filter_result.sim_info), 'score': filter_result.score, 'reason': filter_result.reason})
    entry['FilterResult'] = filter_results_list
    entry['Created'] = list(gsi_logging_data.created_sim_infos)
    entry['Rejected'] = list(gsi_logging_data.rejected_sim_infos)
    archiver.archive(data=entry)

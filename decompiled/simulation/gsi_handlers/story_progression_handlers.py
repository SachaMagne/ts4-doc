from gsi_handlers.gameplay_archiver import GameplayArchiver
from sims4.gsi.schema import GsiGridSchema, GsiFieldVisualizers
story_progression_schema = GsiGridSchema(label='Story Progression')
story_progression_schema.add_field('action_type', label='Type', width=1)
story_progression_schema.add_field('action_message', label='Message', type=GsiFieldVisualizers.STRING, width=10)
story_progression_archiver = GameplayArchiver('story_progression', story_progression_schema)

def archive_story_progression(action, message, *args):
    entry = {'action_type': str(action), 'action_message': message.format(*args)}
    story_progression_archiver.archive(data=entry)

from clock import ClockSpeedMode
from distributor.system import Distributor
from event_testing.results import TestResult
from objects.terrain import TerrainSuperInteraction, get_zone_id_from_pick_location, get_venue_instance_from_pick_location
from sims4.tuning.tunable import TunableSet, TunableReference
import distributor.ops
import routing
import services
import sims4.log
logger = sims4.log.Logger('GalleryInteractions')

class MoveInFromGallerySuperInteraction(TerrainSuperInteraction):
    __qualname__ = 'MoveInFromGallerySuperInteraction'
    VALID_VENUE_TYPES = TunableSet(description='\n        A set of valid venue types that this interaction can be run on.\n        ', tunable=TunableReference(description='\n            A venue type that a sim can be moved into.\n            ', manager=services.get_instance_manager(sims4.resources.Types.VENUE)))

    @classmethod
    def _test(cls, target, context, **kwargs):
        (position, surface, result) = cls._get_target_position_surface_and_test_off_lot(target, context)
        if not result:
            return result
        to_zone_id = get_zone_id_from_pick_location(context.pick)
        if to_zone_id is None:
            return TestResult(False, 'Could not resolve lot id: {} into a valid zone id.', context.pick.lot_id)
        if to_zone_id == services.active_household().home_zone_id:
            return TestResult(False, "Cannot move sim into the active household's home zone.")
        venue_instance = get_venue_instance_from_pick_location(context.pick)
        if venue_instance not in MoveInFromGallerySuperInteraction.VALID_VENUE_TYPES:
            return TestResult(False, 'Cannot move sim into lot of venue type {}.', venue_instance)
        location = routing.Location(position, sims4.math.Quaternion.IDENTITY(), surface)
        routable = routing.test_connectivity_permissions_for_handle(routing.connectivity.Handle(location), context.sim.routing_context)
        if routable:
            return TestResult(False, 'Cannot Move a sim in from routable terrain !')
        return TestResult.TRUE

    def _run_interaction_gen(self, timeline):
        to_zone_id = get_zone_id_from_pick_location(self.context.pick)
        if to_zone_id is None:
            logger.error('Could not resolve lot id: {} into a valid zone id trying to move sim into lot.', self.context.pick.lot_id, owner='jjacobson')
            return
        if services.get_persistence_service().is_save_locked():
            return
        lot_id = self.context.pick.lot_id
        if lot_id is None:
            return
        persistence_service = services.get_persistence_service()
        household_id = persistence_service.get_household_id_from_lot_id(lot_id)
        op = distributor.ops.MoveHouseholdIntoLotFromGallery(to_zone_id, household_id)
        Distributor.instance().add_op(self.sim, op)
        services.game_clock_service().set_clock_speed(ClockSpeedMode.PAUSED)

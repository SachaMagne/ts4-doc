import random
from animation.posture_manifest_constants import STAND_AT_NONE_CONSTRAINT, SWIM_AT_NONE_CONSTRAINT
from interactions import ParticipantType
from interactions.base.super_interaction import SuperInteraction
from interactions.constraints import Circle, Constraint, Nowhere, create_constraint_set
from interactions.utils.routing import WalkStyle, with_walkstyle, PlanRoute, get_route_element_for_path, WalkStylePriority, FollowPath
from sims.sim_info_types import SimInfoSpawnerTags
from sims4.random import pop_weighted
from sims4.tuning.tunable import Tunable, TunableEnumEntry
from sims4.utils import flexmethod
import build_buy
import element_utils
import objects.pools
import routing
import services
import sims4.log
import sims4.math
logger = sims4.log.Logger('WaypointInteraction')

class WaypointInteraction(SuperInteraction):
    __qualname__ = 'WaypointInteraction'

    def __init__(self, aop, *args, **kwargs):
        super().__init__(aop, *args, **kwargs)
        waypoint_info = kwargs.get('waypoint_info')
        target = aop.target
        if waypoint_info is not None:
            (self._start_constraint, self._waypoint_constraints) = waypoint_info
        else:
            (self._start_constraint, self._waypoint_constraints) = self.get_waypoint_constraints(self.context, target)

    @classmethod
    def _is_linked_to(cls, super_affordance):
        return False

    def do_route_to_constraint(self, constraint, timeline, walkstyle_priority=WalkStyle.INVALID):
        goals = []
        handles = constraint.get_connectivity_handles(self.sim)
        for handle in handles:
            goals.extend(handle.get_goals())
        if not goals:
            return False
        route = routing.Route(self.sim.routing_location, goals, routing_context=self.sim.routing_context)
        plan_primitive = PlanRoute(route, self.sim)
        result = yield element_utils.run_child(timeline, plan_primitive)
        if not result:
            return False
        if not plan_primitive.path.nodes or not plan_primitive.path.nodes.plan_success:
            return False
        route = get_route_element_for_path(self.sim, plan_primitive.path)
        result = yield element_utils.run_child(timeline, with_walkstyle(self.sim, WalkStyle.JOG, walkstyle_priority, self.id, sequence=route))
        return result

    def cancel(self, *args, **kwargs):
        for sim_primitive in list(self.sim.primitives):
            while isinstance(sim_primitive, FollowPath):
                sim_primitive.detach()
        return super().cancel(*args, **kwargs)

    @classmethod
    def get_rallyable_aops_gen(cls, target, context, **kwargs):
        key = 'waypoint_info'
        if key not in kwargs:
            jog_waypoint_constraints = cls.get_waypoint_constraints(context, target)
            kwargs[key] = jog_waypoint_constraints
        yield super().get_rallyable_aops_gen(target, context, rally_constraint=jog_waypoint_constraints[0], **kwargs)

class GoJoggingInteraction(WaypointInteraction):
    __qualname__ = 'GoJoggingInteraction'
    NUM_JOG_POINTS = Tunable(description='\n        The number of waypoints to select, from spawn points in the zone, to\n        visit for a Jog prior to returning to the original location.\n        ', tunable_type=int, default=2)
    CONSTRAINT_RADIUS = Tunable(description='\n        The radius in meters of the various jogging waypoint constraints that\n        are generated.\n        ', tunable_type=float, default=6)
    WALKSTYLE_PRIORITY = TunableEnumEntry(description='\n        The walkstyle priority override of the jog interaction.\n        ', tunable_type=WalkStylePriority, default=WalkStyle.INVALID)

    @flexmethod
    def _constraint_gen(cls, inst, sim, target, participant_type=ParticipantType.Actor):
        yield STAND_AT_NONE_CONSTRAINT
        if inst is not None:
            yield inst._start_constraint

    def _run_interaction_gen(self, timeline):
        for constraint in self._waypoint_constraints:
            result = yield self.do_route_to_constraint(constraint, timeline, walkstyle_priority=GoJoggingInteraction.WALKSTYLE_PRIORITY)
            while not result:
                return False
        return True

    @classmethod
    def get_waypoint_constraints(cls, context, target):
        sim = context.sim
        if context.pick is not None:
            pick_position = context.pick.location
            pick_vector = pick_position - sim.position
            pick_vector /= pick_vector.magnitude()
        else:
            pick_vector = sim.forward
        zone = services.current_zone()
        active_lot = zone.lot
        sim_poly = sims4.geometry.CompoundPolygon(sims4.geometry.Polygon([sim.position]))
        lot_poly = sims4.geometry.CompoundPolygon(sims4.geometry.Polygon([corner for corner in reversed(active_lot.corners)]))
        intersection = lot_poly.intersect(sim_poly)
        sim_on_lot = len(intersection) >= 1
        if sim_on_lot:
            spawn_point = zone.get_spawn_point(lot_id=active_lot.lot_id, sim_spawner_tags=SimInfoSpawnerTags.SIM_SPAWNER_TAGS)
            origin_position = spawn_point.center
            routing_surface = routing.SurfaceIdentifier(zone.id, 0, routing.SurfaceType.SURFACETYPE_WORLD)
            except_lot_id = active_lot.lot_id
        else:
            origin_position = sim.position
            routing_surface = sim.routing_surface
            except_lot_id = None
        interaction_constraint = Circle(origin_position, cls.CONSTRAINT_RADIUS, routing_surface=routing_surface, los_reference_point=None)
        jog_waypoint_constraints = []
        zone = services.current_zone()
        active_lot = zone.lot
        constraint_set = zone.get_spawn_points_constraint(except_lot_id=except_lot_id)
        constraints_weighted = []
        min_score = sims4.math.MAX_FLOAT
        for constraint in constraint_set:
            spawn_point_vector = constraint.average_position - sim.position
            score = sims4.math.vector_dot_2d(pick_vector, spawn_point_vector)
            if score < min_score:
                min_score = score
            constraints_weighted.append((score, constraint))
        constraints_weighted = [(score - min_score, constraint) for (score, constraint) in constraints_weighted]
        constraints_weighted = sorted(constraints_weighted, key=lambda i: i[0])
        first_constraint = constraints_weighted[-1][1]
        del constraints_weighted[-1]
        first_constraint_circle = Circle(first_constraint.average_position, cls.CONSTRAINT_RADIUS, routing_surface=first_constraint.routing_surface)
        jog_waypoint_constraints.append(first_constraint_circle)
        last_waypoint_position = first_constraint.average_position
        for _ in range(cls.NUM_JOG_POINTS - 1):
            constraints_weighted_next = []
            for (_, constraint) in constraints_weighted:
                average_position = constraint.average_position
                distance_last = (average_position - last_waypoint_position).magnitude_2d()
                distance_home = (average_position - origin_position).magnitude_2d()
                constraints_weighted_next.append((distance_last + distance_home, constraint))
            next_constraint = pop_weighted(constraints_weighted_next)
            next_constraint_circle = Circle(next_constraint.average_position, cls.CONSTRAINT_RADIUS, routing_surface=next_constraint.routing_surface)
            jog_waypoint_constraints.append(next_constraint_circle)
            constraints_weighted = constraints_weighted_next
            last_waypoint_position = next_constraint.average_position
        jog_waypoint_constraints.append(interaction_constraint)
        return (interaction_constraint, jog_waypoint_constraints)

class SwimmingWaypointInteraction(WaypointInteraction):
    __qualname__ = 'SwimmingWaypointInteraction'
    NUM_SWIM_LOOPS = Tunable(description="\n        The number of times a Sim will do a single loop of swimming from one point\n        to another before they're done swimming\n        ", tunable_type=int, default=20)
    START_CONSTRAINT_RADIUS = Tunable(description='\n        The radius in meters of the starting constraint when you get into the pool.\n        Making this smaller will make the Sim start closer to where you clicked.\n        ', tunable_type=float, default=6)
    SWIM_CONSTRAINT_WIDTH = Tunable(description='\n        The width of the constraints created around the edge of the pool, that the\n        Sim will swim between.\n        ', tunable_type=float, default=1.5)

    @flexmethod
    def _constraint_gen(cls, inst, sim, target, participant_type=ParticipantType.Actor):
        yield SWIM_AT_NONE_CONSTRAINT
        if inst is not None:
            yield inst._start_constraint

    def _run_interaction_gen(self, timeline):
        num_waypoints = len(self._waypoint_constraints)
        for num_loops in range(SwimmingWaypointInteraction.NUM_SWIM_LOOPS):
            if num_loops % num_waypoints == 0:
                random.shuffle(self._waypoint_constraints)
            constraint = self._waypoint_constraints[num_loops % num_waypoints]
            result = yield self.do_route_to_constraint(constraint, timeline)
            while not result or self.is_finishing:
                return False
        return True

    @classmethod
    def get_waypoint_constraints(cls, context, target):
        sim = context.sim
        if target is None:
            position = sim.position
        else:
            position = target.position
        routing_surface = target.routing_location.routing_surface
        level = routing_surface.secondary_id
        if not build_buy.is_location_pool(sim.zone_id, position, level):
            logger.error('Trying to execute a SwimmingWaypointInteraction and the position is not inside the pool: {}, target: {}, position: {}', sim, target, position, owner='cgast')
            return (Nowhere('SwimmingWaypointInteraction, selected position is not inside any pool on the lot.'), [])
        pool_block_id = build_buy.get_block_id(sim.zone_id, position, level - 1)
        pool = objects.pools.pool.get_pool_by_block_id(pool_block_id)
        if pool is not None:
            pool_edge_constraints = pool.get_edge_constraint(constraint_width=cls.SWIM_CONSTRAINT_WIDTH, inward_dir=True, return_constraint_list=True)
            return (create_constraint_set(pool_edge_constraints), pool_edge_constraints)
        return (Nowhere('Failed to find pool at position: {}, level: {}', position, level - 1), [])

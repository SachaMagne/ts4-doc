import random
from event_testing.tests import TunableTestSet
from filters.tunable import TunableSimFilter
from interactions import ParticipantType, ParticipantTypeActorTargetSim, ParticipantTypeSingle, ParticipantTypeSingleSim
from interactions.interaction_finisher import FinishingType
from interactions.utils.interaction_elements import XevtTriggeredElement
from objects.components.state import TunableStateValueReference, StateComponent
from objects.helpers.create_object_helper import CreateObjectHelper
from objects.hovertip import TooltipFieldsComplete
from objects.slots import RuntimeSlot
from objects.system import create_object
from sims.genealogy_tracker import genealogy_caching, FamilyRelationshipIndex
from sims.sim_info_types import Gender
from sims.sim_spawner import SimSpawner, SimCreator
from sims4.random import weighted_random_item
from sims4.tuning.geometric import TunableVector3, TunableDistanceSquared
from sims4.tuning.tunable import TunableReference, TunableList, TunableEnumEntry, TunableVariant, TunableTuple, Tunable, OptionalTunable, TunableInterval, AutoFactoryInit, HasTunableSingletonFactory, TunableFactory, TunablePercent, TunableRange, TunableAngle
from singletons import EMPTY_SET
from tag import Tag
from tunable_multiplier import TunableMultiplier
from world.spawn_actions import TunableSpawnActionVariant
import build_buy
import id_generator
import interactions
import objects.components.types
import placement
import routing
import services
import sims.ghost
import sims4.math
logger = sims4.log.Logger('Creation')

class ObjectCreationElement(XevtTriggeredElement):
    __qualname__ = 'ObjectCreationElement'
    POSITION = 'position'
    INVENTORY = 'inventory'
    SLOT = 'slot'
    POSITION_INCREMENT = 0.5

    class ObjectDefinition(HasTunableSingletonFactory, AutoFactoryInit):
        __qualname__ = 'ObjectCreationElement.ObjectDefinition'
        FACTORY_TUNABLES = {'definition': TunableReference(description='\n                The definition of the object that is created.\n                ', manager=services.definition_manager(), allow_none=True)}

        def get_definition(self, interaction):
            return self.definition

        def setup_created_object(self, interaction, created_object):
            pass

        def get_source_object(self, interaction):
            pass

    class ObjectDefinitionTested(HasTunableSingletonFactory, AutoFactoryInit):
        __qualname__ = 'ObjectCreationElement.ObjectDefinitionTested'
        FACTORY_TUNABLES = {'fallback_definition': TunableReference(description='\n                Should no test pass, use this definition.\n                ', manager=services.definition_manager(), allow_none=True), 'definitions': TunableList(description='\n                A list of potential object definitions to use.\n                ', tunable=TunableTuple(weight=TunableMultiplier.TunableFactory(description='\n                        The weight of this definition relative to other\n                        definitions in this list.\n                        '), definition=TunableReference(description='\n                        The definition of the object to be created.\n                        ', manager=services.definition_manager(), pack_safe=True)))}

        def get_definition(self, interaction):
            resolver = interaction.get_resolver()
            definition = weighted_random_item([(pair.weight.get_multiplier(resolver), pair.definition) for pair in self.definitions])
            if definition is not None:
                return definition
            return self.fallback_definition

        def setup_created_object(self, interaction, created_object):
            pass

        def get_source_object(self, interaction):
            pass

    class RecipeDefinition(HasTunableSingletonFactory, AutoFactoryInit):
        __qualname__ = 'ObjectCreationElement.RecipeDefinition'
        FACTORY_TUNABLES = {'recipe': TunableReference(description='\n                The recipe to use to create the object.\n                ', manager=services.recipe_manager())}

        def get_definition(self, interaction):
            return self.recipe.final_product.definition

        def setup_created_object(self, interaction, created_object):
            from crafting.crafting_process import CraftingProcess
            crafting_process = CraftingProcess(crafter=interaction.sim, recipe=self.recipe)
            crafting_process.setup_crafted_object(created_object, is_final_product=True)

        def get_source_object(self, interaction):
            pass

    class CloneObject(HasTunableSingletonFactory, AutoFactoryInit):
        __qualname__ = 'ObjectCreationElement.CloneObject'

        class ParticipantObject(HasTunableSingletonFactory, AutoFactoryInit):
            __qualname__ = 'ObjectCreationElement.CloneObject.ParticipantObject'
            FACTORY_TUNABLES = {'participant': TunableEnumEntry(description='\n                    Used to clone a participant object.\n                    ', tunable_type=ParticipantType, default=ParticipantType.Object)}

            def get_object(self, interaction):
                return interaction.get_participant(self.participant)

        class SlottedObject(HasTunableSingletonFactory, AutoFactoryInit):
            __qualname__ = 'ObjectCreationElement.CloneObject.SlottedObject'
            FACTORY_TUNABLES = {'slotted_to_participant': TunableTuple(description='\n                    Used to clone an object slotted to a participant.\n                    ', parent_object_participant=TunableEnumEntry(description='\n                        The participant object which will contain the specified\n                        slot where the object to be cloned is slotted.\n                        ', tunable_type=ParticipantType, default=ParticipantType.Object), parent_slot_type=TunableReference(description='\n                        A particular slot type where the cloned object can be found.  The\n                        first slot of this type found on the source_object will be used.\n                        ', manager=services.get_instance_manager(sims4.resources.Types.SLOT_TYPE)))}

            def get_object(self, interaction):
                parent_object = interaction.get_participant(self.slotted_to_participant.parent_object_participant)
                if parent_object is not None:
                    for runtime_slot in parent_object.get_runtime_slots_gen(slot_types={self.slotted_to_participant.parent_slot_type}, bone_name_hash=None):
                        if runtime_slot.empty:
                            pass
                        else:
                            return runtime_slot.children[0]

        FACTORY_TUNABLES = {'source_object': TunableVariant(description='\n                Where the object to be cloned can be found.\n                ', is_participant=ParticipantObject.TunableFactory(), slotted_to_participant=SlottedObject.TunableFactory(), default='slotted_to_participant')}

        def get_definition(self, interaction):
            source_object = self.get_source_object(interaction)
            if source_object is not None:
                return source_object.definition

        def setup_created_object(self, interaction, created_object):
            pass

        def get_source_object(self, interaction):
            return self.source_object.get_object(interaction)

    FACTORY_TUNABLES = {'creation_data': TunableVariant(description='\n            Define what to create.\n            ', definition=ObjectDefinition.TunableFactory(), definition_tested=ObjectDefinitionTested.TunableFactory(), recipe=RecipeDefinition.TunableFactory(), clone_object=CloneObject.TunableFactory(), default='definition'), 'initial_states': TunableList(description='\n            A list of states to apply to the object as soon as it is created.\n            ', tunable=TunableTuple(description='\n                The state to apply and optional tests to decide if the state\n                should apply.\n                ', state=TunableStateValueReference(), tests=OptionalTunable(description='\n                    If enabled, the state will only get set on the created\n                    object if the tests pass. Note: These tests can not be\n                    performed on the newly created object.\n                    ', tunable=TunableTestSet()))), 'destroy_on_placement_failure': Tunable(description='\n            If checked, the created object will be destroyed on placement failure.\n            If unchecked, the created object will be placed into an appropriate\n            inventory on placement failure if possible.  If THAT fails, object\n            will be destroyed.\n            ', tunable_type=bool, default=False), 'cancel_on_destroy': Tunable(description='\n            If checked, the interaction will be canceled if object is destroyed\n            due to placement failure or if destroy on placement failure is\n            unchecked and the fallback fails.\n            ', tunable_type=bool, default=True), 'transient': Tunable(description='\n            If checked, the created object will be destroyed when the interaction ends.\n            ', tunable_type=bool, default=False), 'owner_sim': TunableEnumEntry(description='\n            The participant Sim whose household should own the object. Leave this\n            as Invalid to not assign ownership.\n            ', tunable_type=ParticipantTypeSingleSim, default=ParticipantType.Invalid), 'location': TunableVariant(description='\n            Where the object should be created.\n            ', default='position', position=TunableTuple(description='\n                An in-world position based off of the chosen Participant Type.\n                ', locked_args={'location': POSITION}, location_target=TunableEnumEntry(description='\n                    Who or what to create this object next to.\n                    ', tunable_type=ParticipantType, default=ParticipantType.Actor), offset_tuning=TunableTuple(default_offset=TunableVector3(description="\n                        The default Vector3 offset from the location target's\n                        position.\n                        ", default=sims4.math.Vector3.ZERO()), x_randomization_range=OptionalTunable(TunableInterval(description='\n                        A random number in this range will be applied to the\n                        default offset along the x axis.\n                        ', tunable_type=float, default_lower=0, default_upper=0)), z_randomization_range=OptionalTunable(TunableInterval(description='\n                        A random number in this range will be applied to the\n                        default offset along the z axis.\n                        ', tunable_type=float, default_lower=0, default_upper=0))), ignore_bb_footprints=Tunable(description='\n                    Ignores the build buy object footprints when trying to find\n                    a position for creating this object.  This will allow \n                    objects to appear on top of each other.\n                    e.g. Trash cans when tipped over want to place the trash \n                    right under them so it looks like the pile came out from \n                    the object while it was tipped.\n                    ', tunable_type=bool, default=True), allow_off_lot_placement=Tunable(description='\n                    If checked, objects will be allowed to be placed off-lot.\n                    If unchecked, we will always attempt to place created\n                    objects on the active lot.\n                    ', tunable_type=bool, default=False), in_same_room=Tunable(description='\n                    If checked, objects will be placed in the same block/room\n                    of the participant. If there is not enough space to put\n                    down the object in the same block, the creation will fail.\n                    ', tunable_type=bool, default=False), face_target=OptionalTunable(description='\n                    If enabled, the object will be facing the participant while\n                    placed down..\n                    ', tunable=TunableTuple(target_to_face=TunableEnumEntry(description='\n                            Who or what the created object should face.\n                            ', tunable_type=ParticipantType, default=ParticipantType.Actor), angle=TunableAngle(description='\n                            The angle that facing will trying to keep inside\n                            while test FGL. The larger the number is, the more\n                            offset the facing could be, but the chance will be\n                            higher to succeed in FGL.\n                            ', default=0.5*sims4.math.PI, minimum=0, maximum=sims4.math.PI)))), inventory=TunableTuple(description='\n                An inventory based off of the chosen Participant Type.\n                ', locked_args={'location': INVENTORY}, location_target=TunableEnumEntry(description='\n                    "The owner of the inventory the object will be created in."\n                    ', tunable_type=ParticipantType, default=ParticipantType.Actor)), slot=TunableTuple(description='\n                Slot the object into the specified slot on the tuned location_target.\n                ', locked_args={'location': SLOT}, location_target=TunableEnumEntry(description='\n                    The object which will contain the specified slot.\n                    ', tunable_type=ParticipantType, default=ParticipantType.Object), parent_slot=TunableVariant(description='\n                    The slot on location_target where the object should go. This\n                    may be either the exact name of a bone on the location_target or a\n                    slot type, in which case the first empty slot of the specified type\n                    in which the child object fits will be used.\n                    ', by_name=Tunable(description='\n                        The exact name of a slot on the location_target in which the target\n                        object should go.  \n                        ', tunable_type=str, default='_ctnm_'), by_reference=TunableReference(description='\n                        A particular slot type in which the target object should go.  The\n                        first empty slot of this type found on the location_target will be used.\n                        ', manager=services.get_instance_manager(sims4.resources.Types.SLOT_TYPE))))), 'reserve_object': OptionalTunable(description='\n            If this is enabled, the created object will be reserved for use by\n            the set Sim.\n            ', tunable=TunableEnumEntry(tunable_type=ParticipantTypeActorTargetSim, default=ParticipantTypeActorTargetSim.Actor))}

    def __init__(self, interaction, *args, sequence=(), **kwargs):
        super().__init__(interaction, sequence=sequence, *args, **kwargs)
        self._definition_cache = None
        self._placement_failed = False
        self._assigned_ownership = False
        if self.reserve_object is not None:
            reserved_sim = self.interaction.get_participant(self.reserve_object)
        else:
            reserved_sim = None
        self._object_helper = CreateObjectHelper(reserved_sim, self.definition, self.interaction, object_to_clone=self.creation_data.get_source_object(interaction), init=self._setup_created_object)

    @property
    def definition(self):
        if self._definition_cache is None:
            self._definition_cache = self.creation_data.get_definition(self.interaction)
        return self._definition_cache

    @property
    def placement_failed(self):
        return self._placement_failed

    def create_object(self):
        created_object = create_object(self.definition, init=self._setup_created_object, post_add=self._place_object)
        if self._placement_failed:
            created_object.destroy(source=self.interaction, cause='Failed to place object created by basic extra.')
            return
        return created_object

    def _build_outer_elements(self, sequence):
        return self._object_helper.create(sequence)

    def _do_behavior(self):
        self._place_object(self._object_helper.object)
        if self._placement_failed:
            if self.cancel_on_destroy:
                self.interaction.cancel(FinishingType.FAILED_TESTS, cancel_reason_msg='Cannot place object')
                return False
            return True
        if not self.transient:
            self._object_helper.claim()
        return True

    def _setup_created_object(self, created_object):
        self.interaction.object_create_helper = self._object_helper
        interaction_resolver = self.interaction.get_resolver()
        self.creation_data.setup_created_object(self.interaction, created_object)
        if self.owner_sim != ParticipantType.Invalid:
            owner_sim = interaction_resolver.get_participant(self.owner_sim)
            if owner_sim is not None and owner_sim.is_sim:
                created_object.set_household_owner_id(owner_sim.household.id)
                self._assigned_ownership = True
        for initial_state in self.initial_states:
            if created_object.state_component is None:
                created_object.add_component(StateComponent(created_object))
            while initial_state.tests is None or initial_state.tests.run_tests(interaction_resolver):
                if created_object.has_state(initial_state.state.state):
                    created_object.set_state(initial_state.state.state, initial_state.state)
        if created_object.has_component(objects.components.types.CRAFTING_COMPONENT):
            created_object.crafting_component.update_simoleon_tooltip()
            created_object.crafting_component.update_quality_tooltip()
        created_object.update_tooltip_field(TooltipFieldsComplete.simoleon_value, created_object.current_value)
        created_object.update_object_tooltip()

    def _get_ignored_object_ids(self):
        pass

    def _place_object_no_fallback(self, created_object):
        participant = self.interaction.get_participant(self.location.location_target)
        if self.location.location == self.POSITION:
            offset_tuning = self.location.offset_tuning
            default_offset = sims4.math.Vector3(offset_tuning.default_offset.x, offset_tuning.default_offset.y, offset_tuning.default_offset.z)
            x_range = offset_tuning.x_randomization_range
            z_range = offset_tuning.z_randomization_range
            start_orientation = sims4.random.random_orientation()
            if x_range is not None:
                x_axis = start_orientation.transform_vector(sims4.math.Vector3.X_AXIS())
                default_offset += x_axis*random.uniform(x_range.lower_bound, x_range.upper_bound)
            if z_range is not None:
                z_axis = start_orientation.transform_vector(sims4.math.Vector3.Z_AXIS())
                default_offset += z_axis*random.uniform(z_range.lower_bound, z_range.upper_bound)
            offset = sims4.math.Transform(default_offset, sims4.math.Quaternion.IDENTITY())
            start_position = sims4.math.Transform.concatenate(offset, participant.transform).translation
            routing_surface = participant.routing_surface
            active_lot = services.active_lot()
            location_in_pool = routing_surface.type == routing.SurfaceType.SURFACETYPE_POOL
            if location_in_pool:
                routing_surface = routing.SurfaceIdentifier(routing_surface.primary_id, routing_surface.secondary_id, routing.SurfaceType.SURFACETYPE_WORLD)
                search_flags = placement.FGLSearchFlag.CALCULATE_RESULT_TERRAIN_HEIGHTS | placement.FGLSearchFlag.DONE_ON_MAX_RESULTS
            else:
                search_flags = placement.FGLSearchFlagsDefault
            if self.location.in_same_room:
                search_flags |= placement.FGLSearchFlag.STAY_IN_CURRENT_BLOCK
            search_flags |= placement.FGLSearchFlag.ALLOW_GOALS_IN_SIM_POSITIONS | placement.FGLSearchFlag.ALLOW_GOALS_IN_SIM_INTENDED_POSITIONS
            restrictions = None
            face_target_restriction = self.location.face_target
            if face_target_restriction is not None:
                target_to_face = self.interaction.get_participant(face_target_restriction.target_to_face)
                if target_to_face is not None:
                    restriction = sims4.geometry.RelativeFacingRange(target_to_face.transform.translation, face_target_restriction.angle)
                    restrictions = (restriction,)
            if self.location.allow_off_lot_placement and not active_lot.is_position_on_lot(start_position):
                created_object.location = sims4.math.Location(sims4.math.Transform(start_position, start_orientation), routing_surface)
                starting_location = placement.create_starting_location(position=start_position, orientation=start_orientation, routing_surface=routing_surface)
                context = placement.create_fgl_context_for_object_off_lot(starting_location, created_object, search_flags=search_flags, ignored_object_ids=(created_object.id,), restrictions=restrictions)
            else:
                if location_in_pool:
                    search_flags |= placement.FGLSearchFlag.SHOULD_TEST_BUILDBUY
                else:
                    search_flags |= placement.FGLSearchFlag.SHOULD_TEST_BUILDBUY | placement.FGLSearchFlag.STAY_IN_CURRENT_BLOCK
                if not (self.location.ignore_bb_footprints or active_lot.is_position_on_lot(start_position)):
                    start_position = active_lot.get_default_position(position=start_position)
                starting_location = placement.create_starting_location(position=start_position, orientation=start_orientation, routing_surface=routing_surface)
                context = placement.create_fgl_context_for_object(starting_location, created_object, search_flags=search_flags, ignored_object_ids=self._get_ignored_object_ids(), position_increment=self.POSITION_INCREMENT, restrictions=restrictions)
            (translation, orientation) = placement.find_good_location(context)
            if translation is not None:
                created_object.move_to(routing_surface=routing_surface, translation=translation, orientation=orientation)
                return True
        elif self.location.location == self.SLOT:
            parent_slot = self.location.parent_slot
            if participant.slot_object(parent_slot=parent_slot, slotting_object=created_object):
                return True
        return False

    def _place_object(self, created_object):
        self._setup_created_object(created_object)
        if self._place_object_no_fallback(created_object):
            return True
        if not self.destroy_on_placement_failure:
            participant = self.interaction.get_participant(self.location.location_target)
            if participant.inventory_component is not None and created_object.inventoryitem_component is not None:
                if not self._assigned_ownership:
                    if participant.is_sim:
                        participant_household_id = participant.household.id
                    else:
                        participant_household_id = participant.get_household_owner_id()
                    created_object.set_household_owner_id(participant_household_id)
                    self._assigned_ownership = True
                participant.inventory_component.system_add_object(created_object, participant)
                return True
            sim = self.interaction.sim
            if sim is not None:
                if not sim.household.is_npc_household:
                    try:
                        created_object.set_household_owner_id(sim.household.id)
                        if build_buy.move_object_to_household_inventory(created_object):
                            return True
                        logger.error('Creation: Failed to place object {} in household inventory.', created_object, owner='rmccord')
                    except KeyError:
                        pass
        self._placement_failed = True
        return False

class SimCreationElement(XevtTriggeredElement):
    __qualname__ = 'SimCreationElement'

    class _ActiveHouseholdFactory(TunableFactory):
        __qualname__ = 'SimCreationElement._ActiveHouseholdFactory'

        @staticmethod
        def factory(_):
            return services.active_household()

        FACTORY_TYPE = factory

    class _ParticipantHouseholdFactory(TunableFactory):
        __qualname__ = 'SimCreationElement._ParticipantHouseholdFactory'

        @staticmethod
        def factory(interaction, participant):
            sim = interaction.get_participant(participant)
            if sim is None:
                logger.error('_ParticipantHouseholdFactory: {} does not have participant {}', interaction, participant, owner='jjacobson')
                return
            return sim.household

        FACTORY_TYPE = factory

        def __init__(self, *args, **kwargs):
            super().__init__(participant=TunableEnumEntry(description='\n                    The participant that will have their household used to put the\n                    sim into.\n                    ', tunable_type=ParticipantTypeActorTargetSim, default=ParticipantTypeActorTargetSim.Actor), **kwargs)

    class _NoHousheoldFactory(TunableFactory):
        __qualname__ = 'SimCreationElement._NoHousheoldFactory'

        @staticmethod
        def factory(_):
            pass

        FACTORY_TYPE = factory

    class _BaseSimInfoSource(HasTunableSingletonFactory, AutoFactoryInit):
        __qualname__ = 'SimCreationElement._BaseSimInfoSource'

        def get_sim_infos_and_positions(self, resolver):
            raise NotImplementedError('Attempting to use the _BaseSimInfoSource base class, use sub-classes instead.')

        def _try_add_sim_info_to_household(self, sim_info, resolver, household, skip_household_check=False):
            if household is not None and (skip_household_check or household is not sim_info.household):
                if not household.can_add_sim_info(sim_info):
                    logger.warn('create_sim_from_sim_info element on the interaction: {} could not add a new sim to the tuned household.', resolver.interaction)
                    return False
                household.add_sim_info(sim_info)
                sim_info.assign_to_household(household)
            return True

        def do_pre_spawn_behavior(self, sim_info, resolver, household):
            self._try_add_sim_info_to_household(sim_info, resolver, household)

        def do_post_spawn_behavior(self, sim_info, resolver, client_manager):
            client = client_manager.get_client_by_household_id(sim_info.household_id)
            if client is not None:
                client.add_selectable_sim_info(sim_info)

    class _TargetedObjectResurrection(_BaseSimInfoSource):
        __qualname__ = 'SimCreationElement._TargetedObjectResurrection'
        FACTORY_TUNABLES = {'sim_info_subject': TunableEnumEntry(description='\n                The subject from which the Sim Info used to create the new Sim\n                should be fetched.\n                ', tunable_type=ParticipantType, default=ParticipantType.Object)}

        def get_sim_infos_and_positions(self, resolver, household):
            use_fgl = True
            stored_sim_info_object = resolver.get_participant(self.sim_info_subject)
            if stored_sim_info_object is None:
                return ()
            sim_info = stored_sim_info_object.get_stored_sim_info()
            if sim_info is None:
                return ()
            return ((sim_info, stored_sim_info_object.position, None, use_fgl),)

        def do_pre_spawn_behavior(self, sim_info, resolver, household):
            super().do_pre_spawn_behavior(sim_info, resolver, household)
            sims.ghost.Ghost.remove_ghost_from_sim(sim_info)

    class _MassObjectResurrection(_BaseSimInfoSource):
        __qualname__ = 'SimCreationElement._MassObjectResurrection'
        FACTORY_TUNABLES = {'participant': TunableEnumEntry(description='\n                The participant of the interaction that will have sims resurrected\n                around their position.\n                ', tunable_type=ParticipantType, default=ParticipantType.Actor), 'radius': TunableDistanceSquared(description='\n                The distance around a participant that will resurrect all of the\n                dead sim objects.\n                ', default=1), 'tag': TunableEnumEntry(description='\n                Tag the delineates an object that we want to resurrect sims\n                from.\n                ', tunable_type=Tag, default=Tag.INVALID)}

        def get_sim_infos_and_positions(self, resolver, household):
            use_fgl = True
            sim_infos_and_positions = []
            participant = resolver.get_participant(self.participant)
            position = participant.position
            for obj in services.object_manager().get_objects_with_tag_gen(self.tag):
                obj_position = obj.position
                distance_from_pos = obj_position - position
                if distance_from_pos.magnitude_squared() > self.radius:
                    pass
                sim_info = obj.get_stored_sim_info()
                if sim_info is None:
                    pass
                sim_infos_and_positions.append((sim_info, obj_position, None, use_fgl))
            return tuple(sim_infos_and_positions)

        def do_pre_spawn_behavior(self, sim_info, resolver, household):
            super().do_pre_spawn_behavior(sim_info, resolver)
            sims.ghost.Ghost.remove_ghost_from_sim(sim_info)

    class _SlotSpawningSimInfoSource(_BaseSimInfoSource):
        __qualname__ = 'SimCreationElement._SlotSpawningSimInfoSource'

        class _SlotByName(HasTunableSingletonFactory, AutoFactoryInit):
            __qualname__ = 'SimCreationElement._SlotSpawningSimInfoSource._SlotByName'
            FACTORY_TUNABLES = {'slot_name': Tunable(description='\n                    The exact name of a slot on the parent object.\n                    ', tunable_type=str, default='_ctnm_')}

            def get_slot_type_and_hash(self):
                return (None, sims4.hash_util.hash32(self.slot_name))

        class _SlotByType(HasTunableSingletonFactory, AutoFactoryInit):
            __qualname__ = 'SimCreationElement._SlotSpawningSimInfoSource._SlotByType'
            FACTORY_TUNABLES = {'slot_type': TunableReference(description='\n                    A particular slot type in which the should spawn.\n                    ', manager=services.get_instance_manager(sims4.resources.Types.SLOT_TYPE))}

            def get_slot_type_and_hash(self):
                return (self.slot_type, None)

        FACTORY_TUNABLES = {'participant': TunableEnumEntry(description='\n                The participant that is a sim that will be cloned\n                Note: MUST be a sim. Use create object - clone object for non-sim objects.\n                ', tunable_type=ParticipantTypeSingle, default=ParticipantType.Actor), 'sim_spawn_slot': TunableVariant(description='\n                The slot on the parent object where the sim should spawn. This\n                may be either the exact name of a bone on the parent object or a\n                slot type, in which case the first empty slot of the specified type\n                will be used. If None is chosen, then the sim will spawn on the\n                landing strip.\n                ', by_name=_SlotByName.TunableFactory(), by_type=_SlotByType.TunableFactory())}

        def __init__(self, sim_spawn_slot=None, **kwargs):
            super().__init__(sim_spawn_slot=sim_spawn_slot, **kwargs)
            self._slot_type = None
            self._bone_name_hash = None
            if sim_spawn_slot is not None:
                (self._slot_type, self._bone_name_hash) = sim_spawn_slot.get_slot_type_and_hash()

        def _get_position_and_location(self, spawning_object):
            (position, location) = (None, None)
            if self._slot_type is not None:
                for runtime_slot in spawning_object.get_runtime_slots_gen(slot_types={self._slot_type}, bone_name_hash=self._bone_name_hash):
                    location = runtime_slot.location
            elif self._bone_name_hash is not None:
                runtime_slot = RuntimeSlot(spawning_object, self._bone_name_hash, EMPTY_SET)
                if runtime_slot is not None:
                    location = runtime_slot.location
            if location is not None:
                location = sims4.math.Location(location.world_transform, spawning_object.routing_surface, slot_hash=location.slot_hash)
                position = location.transform.translation
            return (position, location)

    class _CloneSimInfoSource(_SlotSpawningSimInfoSource):
        __qualname__ = 'SimCreationElement._CloneSimInfoSource'
        FACTORY_TUNABLES = {'relationship_bits_to_add': TunableList(description='\n                A list of relationship bits to add between the source sim\n                and the clone sim.\n                ', tunable=TunableReference(manager=services.get_instance_manager(sims4.resources.Types.RELATIONSHIP_BIT)))}

        def _ensure_parental_lineage_exists(self, source_sim_info, clone_sim_info):
            with genealogy_caching():
                if any(source_sim_info.genealogy.get_parent_sim_ids_gen()):
                    return
                mom_id = id_generator.generate_object_id()
                source_sim_info.genealogy.set_family_relation(FamilyRelationshipIndex.MOTHER, mom_id)
                clone_sim_info.genealogy.set_family_relation(FamilyRelationshipIndex.MOTHER, mom_id)
                sim_info_manager = services.sim_info_manager()
                for child_sim_id in source_sim_info.genealogy.get_children_sim_ids_gen():
                    child_sim_info = sim_info_manager.get(child_sim_id)
                    while child_sim_info is not None:
                        grandparent_relation = FamilyRelationshipIndex.MOTHERS_MOM if source_sim_info.is_female else FamilyRelationshipIndex.FATHERS_MOM
                        child_sim_info.genealogy.set_family_relation(grandparent_relation, mom_id)

        def _create_clone_sim_info(self, source_sim_info, resolver, household):
            sim_creator = SimCreator(gender=source_sim_info.gender, age=source_sim_info.age, first_name=SimSpawner.get_random_first_name(source_sim_info.account, source_sim_info.gender == Gender.FEMALE), last_name=source_sim_info._base.last_name)
            (sim_info_list, _) = SimSpawner.create_sim_infos((sim_creator,), household=household, account=source_sim_info.account, generate_deterministic_sim=True, creation_source='cloning', skip_adding_to_household=True)
            clone_sim_info = sim_info_list[0]
            source_sim_proto = source_sim_info.save_sim(for_cloning=True)
            clone_sim_id = clone_sim_info.sim_id
            clone_first_name = clone_sim_info._base.first_name
            clone_last_name = clone_sim_info._base.last_name
            clone_first_name_key = clone_sim_info._base.first_name_key
            clone_last_name_key = clone_sim_info._base.last_name_key
            clone_full_name_key = clone_sim_info._base.full_name_key
            clone_sim_info.load_sim_info(source_sim_proto, is_clone=True)
            clone_sim_info.sim_id = clone_sim_id
            clone_sim_info._base.first_name = clone_first_name
            clone_sim_info._base.last_name = clone_last_name
            clone_sim_info._base.first_name_key = clone_first_name_key
            clone_sim_info._base.last_name_key = clone_last_name_key
            clone_sim_info._base.full_name_key = clone_full_name_key
            if not self._try_add_sim_info_to_household(clone_sim_info, resolver, household, skip_household_check=True):
                return
            source_trait_tracker = source_sim_info.trait_tracker
            clone_trait_tracker = clone_sim_info.trait_tracker
            for trait in tuple(clone_trait_tracker.personality_traits):
                clone_sim_info.remove_trait(trait)
            for trait in tuple(source_trait_tracker.personality_traits):
                clone_sim_info.add_trait(trait)
            correct_aspiration_trait = clone_sim_info.primary_aspiration.primary_trait
            for trait in tuple(clone_trait_tracker.aspiration_traits):
                while trait is not correct_aspiration_trait:
                    clone_sim_info.remove_trait(trait)
            source_sim_info.relationship_tracker.create_relationship(clone_sim_info.sim_id)
            clone_sim_info.relationship_tracker.create_relationship(source_sim_info.sim_id)
            source_sim_info.relationship_tracker.add_relationship_score(clone_sim_info.sim_id, 1)
            clone_sim_info.relationship_tracker.add_relationship_score(source_sim_info.sim_id, 1)
            for rel_bit in self.relationship_bits_to_add:
                source_sim_info.relationship_tracker.add_relationship_bit(clone_sim_info.sim_id, rel_bit, force_add=True)
                clone_sim_info.relationship_tracker.add_relationship_bit(source_sim_info.sim_id, rel_bit, force_add=True)
            self._ensure_parental_lineage_exists(source_sim_info, clone_sim_info)
            services.sim_info_manager().set_default_genealogy()
            clone_sim_info.set_default_data()
            clone_sim_info.save_sim()
            household.save_data()
            clone_sim_info.resend_physical_attributes()
            clone_sim_info.relationship_tracker.clean_and_send_remaining_relationship_info()
            return clone_sim_info

        def do_pre_spawn_behavior(self, sim_info, resolver, household):
            pass

        def get_sim_infos_and_positions(self, resolver, household):
            use_fgl = False
            sim_info = resolver.get_participant(self.participant)
            if not sim_info.is_sim:
                logger.error('create_sim_from_sim_info element is tuned to use a participant that is not a sim.')
                return ()
            clone_sim_info = self._create_clone_sim_info(sim_info, resolver, household)
            if clone_sim_info is None:
                return ()
            (position, location) = (None, None)
            spawning_object = resolver.get_participant(interactions.ParticipantType.Object)
            if spawning_object is not None:
                (position, location) = self._get_position_and_location(spawning_object)
                use_fgl = position is None
            return ((clone_sim_info, position, location, use_fgl),)

        def do_post_spawn_behavior(self, sim_info, resolver, client_manager):
            super().do_post_spawn_behavior(sim_info, resolver, client_manager)
            sim_info.commodity_tracker.set_all_commodities_to_max(visible_only=True)

    class _SimFilterSimInfoSource(_SlotSpawningSimInfoSource):
        __qualname__ = 'SimCreationElement._SimFilterSimInfoSource'
        FACTORY_TUNABLES = {'filter': TunableSimFilter.TunableReference(description='\n                Sim filter that is used to create or find a Sim that matches\n                this filter request.\n                ')}

        def get_sim_infos_and_positions(self, resolver, household):
            use_fgl = True
            sim_info = resolver.get_participant(self.participant)
            if not sim_info.is_sim:
                logger.error('create_sim_from_sim_info element is tuned to use a participant that is not a sim.')
                return ()
            filter_result = services.sim_filter_service().submit_matching_filter(sim_filter=self.filter, requesting_sim_info=sim_info, allow_yielding=False)
            if not filter_result:
                return ()
            (position, location) = (None, None)
            spawning_object = resolver.get_participant(interactions.ParticipantType.Object)
            if spawning_object is not None:
                (position, location) = self._get_position_and_location(spawning_object)
                use_fgl = position is None
            return ((filter_result[0].sim_info, position, location, use_fgl),)

    FACTORY_TUNABLES = {'sim_info_source': TunableVariant(description='\n            The source of the sim_info and position data for the sims to be\n            created.\n            ', targeted=_TargetedObjectResurrection.TunableFactory(), mass_object=_MassObjectResurrection.TunableFactory(), clone_a_sim=_CloneSimInfoSource.TunableFactory(), sim_filter=_SimFilterSimInfoSource.TunableFactory(), default='targeted'), 'household_option': TunableVariant(description='\n            The household that the created sim should be put into.\n            ', active_household=_ActiveHouseholdFactory(), participant_household=_ParticipantHouseholdFactory(), no_household=_NoHousheoldFactory(), default='participant_household'), 'spawn_action': TunableSpawnActionVariant(description='\n            Define the methods to show the Sim after spawning on the lot. This\n            defaults to fading the Sim in, but can be a specific interaction or\n            an animation.\n            ')}

    def _do_behavior(self):
        resolver = self.interaction.get_resolver()
        household = self.household_option(self.interaction)
        client_manager = services.client_manager()
        for (sim_info, position, location, use_fgl) in self.sim_info_source.get_sim_infos_and_positions(resolver, household):
            self.sim_info_source.do_pre_spawn_behavior(sim_info, resolver, household)
            SimSpawner.spawn_sim(sim_info, position, spawn_action=self.spawn_action, sim_location=location, use_fgl=use_fgl)
            self.sim_info_source.do_post_spawn_behavior(sim_info, resolver, client_manager)
        return True

from protocolbuffers import SimObjectAttributes_pb2 as protocols
from objects import ALL_HIDDEN_REASONS
from sims4.tuning.dynamic_enum import DynamicEnumLocked
from sims4.tuning.tunable import TunableMapping, TunableEnumEntry, TunableReference
import services
import sims4.reload
with sims4.reload.protected(globals()):
    _is_death_enabled = True
logger = sims4.log.Logger('Death')
DEATH_INTERACTION_MARKER_ATTRIBUTE = 'death_interaction'

def toggle_death(enabled=None):
    global _is_death_enabled
    if enabled is None:
        _is_death_enabled = not _is_death_enabled
    else:
        _is_death_enabled = enabled

def is_death_enabled():
    return _is_death_enabled

def is_permanent_death(death_type):
    return death_type == DeathType.NEGLECT

def get_death_interaction(sim):
    return getattr(sim, DEATH_INTERACTION_MARKER_ATTRIBUTE, None)

class DeathType(DynamicEnumLocked):
    __qualname__ = 'DeathType'
    NONE = 0
    NEGLECT = 1
    NETHERWORLD = 2

class DeathTracker:
    __qualname__ = 'DeathTracker'
    DEATH_ZONE_ID = 0
    DEATH_TYPE_GHOST_TRAIT_MAP = TunableMapping(description='\n        The ghost trait to be applied to a Sim when they die with a given death\n        type.\n        ', key_type=TunableEnumEntry(description='\n            The death type to map to a ghost trait.\n            ', tunable_type=DeathType, default=DeathType.NONE), key_name='Death Type', value_type=TunableReference(description='\n            The ghost trait to apply to a Sim when they die from the specified\n            death type.\n            ', manager=services.trait_manager()), value_name='Ghost Trait')
    IS_DYING_BUFF = TunableReference(description='\n        A reference to the buff a Sim is given when they are dying.\n        ', manager=services.buff_manager())

    def __init__(self, sim_info):
        self._sim_info = sim_info
        self._death_type = None
        self._death_time = None

    @property
    def death_type(self):
        return self._death_type

    @property
    def death_time(self):
        return self._death_time

    @property
    def is_ghost(self):
        return self._sim_info.trait_tracker.has_any_trait(self.DEATH_TYPE_GHOST_TRAIT_MAP.values())

    def get_ghost_trait(self):
        return self.DEATH_TYPE_GHOST_TRAIT_MAP.get(self._death_type)

    def set_death_type(self, death_type):
        self._sim_info.inject_into_inactive_zone(self.DEATH_ZONE_ID, start_away_actions=False)
        self._sim_info.household.remove_sim_info(self._sim_info, destroy_if_empty_gameplay_household=True)
        if is_permanent_death(death_type):
            services.sim_info_manager().remove_permanently(self._sim_info)
        else:
            household = services.household_manager().create_household(self._sim_info.account)
            household.hidden = True
            household.add_sim_info(self._sim_info)
            self._sim_info.assign_to_household(household)
            ghost_trait = DeathTracker.DEATH_TYPE_GHOST_TRAIT_MAP.get(death_type)
            if ghost_trait is not None:
                self._sim_info.add_trait(ghost_trait)
        self._death_type = death_type
        self._death_time = services.time_service().sim_now.absolute_ticks()
        self._sim_info.reset_age_progress()
        self._sim_info.resend_death_type()

    def clear_death_type(self):
        self._death_type = None
        self._death_time = None
        self._sim_info.resend_death_type()

    @staticmethod
    def handle_adultless_household(household):
        if not any(sim_info.is_teen_or_older for sim_info in household):
            persistence_service = services.get_persistence_service()
            sim_info_manager = services.sim_info_manager()
            for sim_info in household:
                sim_to_destroy = sim_info.get_sim_instance(allow_hidden_flags=ALL_HIDDEN_REASONS)
                if sim_to_destroy is not None:
                    sim_to_destroy.destroy(source=sim_to_destroy, cause='Last adult sim dying, destroying dependent Sims.')
                persistence_service.del_sim_proto_buff(sim_info.sim_id)
                sim_info_manager.remove_permanently(sim_info)
            zone_manager = services.get_zone_manager()
            for retail_zone_id in household.retail_tracker.retail_managers:
                zone_manager.clear_lot_ownership(retail_zone_id)
            if household.home_zone_id:
                household.clear_household_lot_ownership()
            household.hidden = True

    def save(self):
        if self._death_type is not None:
            data = protocols.PersistableDeathTracker()
            data.death_type = self._death_type
            data.death_time = self._death_time
            return data

    def load(self, data):
        self._death_type = data.death_type
        self._death_time = data.death_time

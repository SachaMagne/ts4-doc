from protocolbuffers import Consts_pb2
from interactions import ParticipantType
from interactions.utils.destruction_liability import DeleteObjectLiability, DELETE_OBJECT_LIABILITY
from interactions.utils.interaction_elements import XevtTriggeredElement
from objects.client_object_mixin import ClientObjectMixin
from sims.funds import FundsSource
from sims4.tuning.tunable import TunableEnumEntry, OptionalTunable, TunableTuple, TunableRange, Tunable
from singletons import DEFAULT
from tunable_multiplier import TunableMultiplier
import algos

class ObjectDestructionElement(XevtTriggeredElement):
    __qualname__ = 'ObjectDestructionElement'
    FACTORY_TUNABLES = {'objects_to_destroy': TunableEnumEntry(description='\n            The objects to destroy.\n            ', tunable_type=ParticipantType, default=ParticipantType.Object), 'award_value': OptionalTunable(description="\n            If necessary, define how an amount corresponding to the objects'\n            value is distributed among Sims.\n            ", tunable=TunableTuple(recipients=TunableEnumEntry(description='\n                    Who to award funds to.  If more than one participant is\n                    specified, the value will be evenly divided among the\n                    recipients.\n                    ', tunable_type=ParticipantType, default=ParticipantType.Actor), multiplier=TunableRange(description='\n                    Value multiplier for the award.\n                    ', tunable_type=float, default=1.0), tested_multipliers=TunableMultiplier.TunableFactory(description='\n                    Each multiplier that passes its test set will be applied to\n                    each award payment.\n                    ')))}

    def __init__(self, interaction, **kwargs):
        super().__init__(interaction, **kwargs)
        if self.award_value is not None:
            self.award_value_to = self.award_value.recipients
            self.value_multiplier = self.award_value.multiplier
            self.tested_multipliers = self.award_value.tested_multipliers
        else:
            self.award_value_to = None
        self._destroyed_objects = []

    @classmethod
    def on_affordance_loaded_callback(cls, affordance, object_destruction_element):

        def get_simoleon_delta(interaction, target=DEFAULT, context=DEFAULT):
            award_value = object_destruction_element.award_value
            if award_value is None:
                return (0, FundsSource.HOUSEHOLD)
            target = interaction.target if target is DEFAULT else target
            sim = interaction.sim if context is DEFAULT else context.sim
            destructees = interaction.get_participants(object_destruction_element.objects_to_destroy, sim=sim, target=target)
            total_value = sum(obj.current_value for obj in destructees)
            skill_multiplier = 1 if context is DEFAULT else interaction.get_skill_multiplier(interaction.monetary_payout_multipliers, sim)
            if total_value > 0:
                total_value *= skill_multiplier
            tested_multiplier = award_value.tested_multipliers.get_multiplier(interaction.get_resolver(target=target, context=context))
            return (total_value*award_value.multiplier*tested_multiplier, FundsSource.HOUSEHOLD)

        affordance.register_simoleon_delta_callback(get_simoleon_delta)

    def _destroy_objects(self):
        interaction = self.interaction
        sim = self.interaction.sim
        for object_to_destroy in self._destroyed_objects:
            in_use = object_to_destroy.in_use_by(sim, owner=interaction)
            if object_to_destroy.is_part:
                obj = object_to_destroy.part_owner
            else:
                obj = object_to_destroy
            if self.interaction.is_saved_participant(obj) or obj.parts and any(self.interaction.is_saved_participant(obj_part) for obj_part in obj.parts):
                obj.remove_from_client(fade_duration=ClientObjectMixin.FADE_DURATION)
                delete_liability = DeleteObjectLiability([obj])
                self.interaction.add_liability(DELETE_OBJECT_LIABILITY, delete_liability)
                return
            if in_use:
                obj.transient = True
                obj.remove_from_client(fade_duration=ClientObjectMixin.FADE_DURATION)
            else:
                if obj is interaction.target:
                    interaction.set_target(None)
                obj.destroy(source=interaction, cause='Destroying object in basic extra.', fade_duration=ClientObjectMixin.FADE_DURATION)

    def _do_behavior(self):
        if self.award_value_to is not None:
            awardees = self.interaction.get_participants(self.award_value_to)
        else:
            awardees = ()
        destructees = self.interaction.get_participants(self.objects_to_destroy)
        if destructees:
            for object_to_destroy in destructees:
                if not object_to_destroy.can_be_destroyed():
                    pass
                if object_to_destroy.is_in_inventory():
                    inventory = object_to_destroy.get_inventory()
                    inventory.try_remove_object_by_id(object_to_destroy.id)
                else:
                    object_to_destroy.set_parent(None, transform=object_to_destroy.transform, routing_surface=object_to_destroy.routing_surface)
                    obj_footprint_comp = object_to_destroy.footprint_component
                    if obj_footprint_comp is not None:
                        obj_footprint_comp.disable_footprint()
                    object_to_destroy.remove_from_client(fade_duration=ClientObjectMixin.FADE_DURATION)
                self._destroyed_objects.append(object_to_destroy)
                while awardees:
                    multiplier = self.tested_multipliers.get_multiplier(self.interaction.get_resolver())
                    value = int(object_to_destroy.current_value*self.value_multiplier*multiplier)
                    awards = algos.distribute_total_over_parts(value, [1]*len(awardees))
                    object_to_destroy.current_value = 0
                    interaction_tags = set()
                    if self.interaction is not None:
                        interaction_tags |= self.interaction.get_category_tags()
                    object_tags = frozenset(interaction_tags | object_to_destroy.get_tags())
                    while True:
                        for (recipient, award) in zip(awardees, awards):
                            recipient.family_funds.add(award, Consts_pb2.TELEMETRY_OBJECT_SELL, recipient, tags=object_tags)
            if self.interaction.is_finishing:
                self._destroy_objects()
            else:
                self.interaction.add_exit_function(self._destroy_objects)

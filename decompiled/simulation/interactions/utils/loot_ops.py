import random
from protocolbuffers import Consts_pb2
from protocolbuffers.DistributorOps_pb2 import SetWhimBucks
from distributor.ops import BreakThroughMessage
from event_testing.tests import TunableTestSet
from interactions import ParticipantType
from interactions.utils import LootType
from interactions.utils.loot_basic_op import BaseLootOperation, BaseTargetedLootOperation
from objects.components.portal_locking_component import LockSimInfoData, LockAllWithSimIdExceptionData, LockType, LockAllWithSituationJobExceptionData
from objects.components.state import TunableStateValueReference
from objects.components.types import PORTAL_LOCKING_COMPONENT
from sims.unlock_tracker import TunableUnlockVariant
from sims4.tuning.tunable import Tunable, TunableRange, TunableReference, OptionalTunable, TunableRealSecond, TunableVariant, TunableEnumEntry, TunableList, TunableFactory, HasTunableSingletonFactory, AutoFactoryInit
from ui.notebook_tuning import NotebookSubCategories
from ui.ui_dialog import UiDialogOk
from ui.ui_dialog_notification import UiDialogNotification
import build_buy
import distributor.system
import event_testing
import objects
import services
import sims4.log
import sims4.resources
import telemetry_helper
logger = sims4.log.Logger('LootOperations')
FLOAT_TO_PERCENT = 0.01
TELEMETRY_GROUP_LOOT_OPS = 'LOOT'
TELEMETRY_HOOK_DETECTIVE_CLUE = 'DECL'
TELEMETRY_DETECTIVE_CLUE_FOUND = 'clue'
loot_op_telemetry_writer = sims4.telemetry.TelemetryWriter(TELEMETRY_GROUP_LOOT_OPS)

class BaseGameLootOperation(BaseLootOperation):
    __qualname__ = 'BaseGameLootOperation'

    @staticmethod
    def _verify_tunable_callback(instance_class, tunable_name, source, value):
        if value._subject == ParticipantType.Invalid:
            logger.error("The 'subject' in BaseGameLootOperation {} should not be ParticipantType.Invalid.", instance_class)

    FACTORY_TUNABLES = {'locked_args': {'advertise': False}, 'verify_tunable_callback': _verify_tunable_callback}

class LifeExtensionLootOp(BaseLootOperation):
    __qualname__ = 'LifeExtensionLootOp'
    FACTORY_TUNABLES = {'description': '\n            This loot will grant a life extension.\n            ', 'bonus_days': TunableRange(description="\n            Number of bonus days to be granted to the target's life.\n            ", tunable_type=int, default=1, minimum=0), 'reset_aging_progress_in_category': Tunable(description="\n            If checked, this loot op will also reset the target's aging\n            progress in their current age category.\n            ", tunable_type=bool, default=False)}

    def __init__(self, bonus_days, reset_aging_progress_in_category, **kwargs):
        super().__init__(**kwargs)
        self.bonus_days = bonus_days
        self.reset_aging_progress_in_category = reset_aging_progress_in_category

    @property
    def loot_type(self):
        return LootType.LIFE_EXTENSION

    def _apply_to_subject_and_target(self, subject, target, resolver):
        subject.add_bonus_days(self.bonus_days)
        if self.reset_aging_progress_in_category:
            subject.reset_age_progress()

class StateChangeLootOp(BaseLootOperation):
    __qualname__ = 'StateChangeLootOp'
    FACTORY_TUNABLES = {'description': '\n            This loot will change the state of the subject.\n            ', 'state_value': TunableStateValueReference()}

    @TunableFactory.factory_option
    def subject_participant_type_options(**kwargs):
        return BaseLootOperation.get_participant_tunable('subject', use_objects_with_tag=True, **kwargs)

    def __init__(self, state_value, **kwargs):
        super().__init__(**kwargs)
        self.state_value = state_value

    def _apply_to_subject_and_target(self, subject, target, resolver):
        subject_obj = self._get_object_from_recipient(subject)
        if subject_obj is not None:
            state_value = self.state_value
            subject_obj.set_state(state_value.state, state_value)

class DialogLootOp(BaseLootOperation):
    __qualname__ = 'DialogLootOp'
    FACTORY_TUNABLES = {'dialog': TunableVariant(description='\n            Type of dialog to show.\n            ', notification=UiDialogNotification.TunableFactory(description='\n                This text will display in a notification pop up when completed.\n                '), dialog_ok=UiDialogOk.TunableFactory(description='\n                Display a dialog with an okay button.\n                '), default='notification')}

    def __init__(self, dialog, **kwargs):
        super().__init__(**kwargs)
        self.dialog = dialog

    def _apply_to_subject_and_target(self, subject, target, resolver):
        if subject is not None and subject.is_selectable:
            dialog = self.dialog(subject, resolver)
            dialog.show_dialog(event_id=self.dialog.factory.DIALOG_MSG_TYPE)

class AddTraitLootOp(BaseLootOperation):
    __qualname__ = 'AddTraitLootOp'
    FACTORY_TUNABLES = {'description': '\n            This loot will add the specified trait.\n            ', 'trait': TunableReference(description='\n            The trait to be added.\n            ', manager=services.get_instance_manager(sims4.resources.Types.TRAIT))}

    def __init__(self, trait, **kwargs):
        super().__init__(**kwargs)
        self._trait = trait

    def _apply_to_subject_and_target(self, subject, target, resolver):
        subject.add_trait(self._trait)

class RemoveTraitLootOp(BaseLootOperation):
    __qualname__ = 'RemoveTraitLootOp'

    class _BaseRemoveTrait(HasTunableSingletonFactory, AutoFactoryInit):
        __qualname__ = 'RemoveTraitLootOp._BaseRemoveTrait'

        def get_trait_to_remove(self, subject, target, resolver):
            raise NotImplementedError('Attempting to use the _BaseRemoveTrait base class, use sub-classes instead.')

    class _RemoveSpecificTrait(_BaseRemoveTrait):
        __qualname__ = 'RemoveTraitLootOp._RemoveSpecificTrait'
        FACTORY_TUNABLES = {'specific_trait': TunableReference(description='\n                The trait to be removed.\n                ', manager=services.get_instance_manager(sims4.resources.Types.TRAIT))}

        def get_trait_to_remove(self, subject, target, resolver):
            return self.specific_trait

    class _RemoveRandomPersonalityTrait(_BaseRemoveTrait):
        __qualname__ = 'RemoveTraitLootOp._RemoveRandomPersonalityTrait'
        FACTORY_TUNABLES = {'traits_to_not_consider': TunableList(description='\n                Personality traits that should not be considered for removal. Leave\n                blank to consider all personality traits.\n                ', tunable=TunableReference(description='\n                    A personality trait that should not be removed.\n                    ', manager=services.get_instance_manager(sims4.resources.Types.TRAIT), pack_safe=True))}

        def get_trait_to_remove(self, subject, target, resolver):
            trait_to_remove = None
            personality_traits_to_consider = [trait for trait in subject.trait_tracker.personality_traits if trait not in self.traits_to_not_consider]
            if personality_traits_to_consider:
                trait_to_remove = random.choice(personality_traits_to_consider)
            else:
                logger.warn('RemoveRandomPersonalityTraitLootOp could not find a valid personality trait to remove.')
            return trait_to_remove

    FACTORY_TUNABLES = {'trait': TunableVariant(description='\n            Type of trait removal to perform.\n            ', specific_trait=_RemoveSpecificTrait.TunableFactory(), random_personality_trait=_RemoveRandomPersonalityTrait.TunableFactory(), default='specific_trait')}

    def __init__(self, trait, **kwargs):
        super().__init__(**kwargs)
        self._trait = trait

    def _apply_to_subject_and_target(self, subject, target, resolver):
        trait_to_remove = self._trait.get_trait_to_remove(subject, target, resolver)
        if trait_to_remove is not None:
            subject.remove_trait(trait_to_remove)

class HouseholdFundsInterestLootOp(BaseLootOperation):
    __qualname__ = 'HouseholdFundsInterestLootOp'
    FACTORY_TUNABLES = {'description': '\n            This loot will deliver interest income to the current Household for their current funds,\n            based on the percentage tuned against total held. \n        ', 'interest_rate': Tunable(description='\n            The percentage of interest to apply to current funds.\n            ', tunable_type=int, default=0), 'notification': OptionalTunable(description='\n            If enabled, this notification will display when this interest payment is made.\n            Token 0 is the Sim - i.e. {0.SimFirstName}\n            Token 1 is the interest payment amount - i.e. {1.Money}\n            ', tunable=UiDialogNotification.TunableFactory())}

    def __init__(self, interest_rate, notification, **kwargs):
        super().__init__(**kwargs)
        self._interest_rate = interest_rate
        self._notification = notification

    def _apply_to_subject_and_target(self, subject, target, resolver):
        pay_out = int(subject.household.funds.money*self._interest_rate*FLOAT_TO_PERCENT)
        subject.household.funds.add(pay_out, Consts_pb2.TELEMETRY_INTERACTION_REWARD, self._get_object_from_recipient(subject))
        if self._notification is not None:
            dialog = self._notification(subject, resolver)
            dialog.show_dialog(event_id=self._notification.factory.DIALOG_MSG_TYPE, additional_tokens=(pay_out,))

class FireLootOp(BaseLootOperation):
    __qualname__ = 'FireLootOp'

    def _apply_to_subject_and_target(self, subject, target, resolver):
        if subject is None:
            logger.error('Invalid subject specified for this loot operation. {}  Please fix in tuning.', self)
            return
        subject_obj = self._get_object_from_recipient(subject)
        if subject_obj is None:
            logger.error('No valid object for subject specified for this loot operation. {}  Please fix in tuning.', resolver)
            return
        fire_service = services.get_fire_service()
        fire_service.spawn_fire_at_object(subject_obj)

class UnlockLootOp(BaseLootOperation):
    __qualname__ = 'UnlockLootOp'
    FACTORY_TUNABLES = {'description': '\n            This loot will give Sim an unlock item like recipe etc. \n            ', 'unlock_item': TunableUnlockVariant(description='\n            The unlock item that will give to the Sim.\n            ')}

    def __init__(self, unlock_item, **kwargs):
        super().__init__(**kwargs)
        self._unlock_item = unlock_item

    def _apply_to_subject_and_target(self, subject, target, resolver):
        if subject is None:
            logger.error('Subject {} is None for the loot {}..', self.subject, self)
            return
        if not subject.is_sim:
            logger.error('Subject {} is not Sim for the loot {}.', self.subject, self)
            return
        subject.unlock_tracker.add_unlock(self._unlock_item, None)

class CollectibleShelveItem(BaseLootOperation):
    __qualname__ = 'CollectibleShelveItem'

    def __init__(self, *args, **kwargs):
        super().__init__(target_participant_type=ParticipantType.Object, *args, **kwargs)

    def _apply_to_subject_and_target(self, subject, target, resolver):
        target_slot = subject.get_collectable_slot()
        if target_slot:
            for runtime_slot in target.get_runtime_slots_gen(bone_name_hash=sims4.hash_util.hash32(target_slot)):
                while runtime_slot and runtime_slot.empty:
                    runtime_slot.add_child(subject)
                    return True
        return False

class FireDeactivateSprinklerLootOp(BaseLootOperation):
    __qualname__ = 'FireDeactivateSprinklerLootOp'

    def _apply_to_subject_and_target(self, subject, target, resolver):
        fire_service = services.get_fire_service()
        if fire_service is not None:
            fire_service.deactivate_sprinkler_system()

class FireCleanScorchLootOp(BaseLootOperation):
    __qualname__ = 'FireCleanScorchLootOp'

    def _apply_to_subject_and_target(self, subject, target, resolver):
        if subject is None:
            logger.error('Subject {} is None for the loot {}..', self.subject, self)
            return
        if not subject.is_sim:
            logger.error('Subject {} is not Sim for the loot {}.', self.subject, self)
            return
        fire_service = services.get_fire_service()
        if fire_service is None:
            logger.error('Fire Service in none when calling the lootop: {}.', self)
            return
        sim = self._get_object_from_recipient(subject)
        location = sims4.math.Vector3(*sim.location.transform.translation) + sim.forward
        level = sim.location.level
        scorch_locations = fire_service.find_cleanable_scorch_mark_locations_within_radius(location, level, fire_service.SCORCH_TERRAIN_CLEANUP_RADIUS)
        if scorch_locations:
            zone_id = services.current_zone_id()
            with build_buy.floor_feature_update_context(zone_id, build_buy.FloorFeatureType.BURNT):
                for scorch_location in scorch_locations:
                    build_buy.set_floor_feature(zone_id, build_buy.FloorFeatureType.BURNT, scorch_location, level, 0)

class ExtinguishNearbyFireLootOp(BaseLootOperation):
    __qualname__ = 'ExtinguishNearbyFireLootOp'

    def _apply_to_subject_and_target(self, subject, target, resolver):
        if subject is None:
            logger.error('Subject {} is None for the loot {}..', self.subject, self)
            return
        fire_service = services.get_fire_service()
        if fire_service is None:
            logger.error('Fire Service in none when calling the lootop: {}.', self)
            return
        subject = self._get_object_from_recipient(subject)
        fire_service.extinguish_nearby_fires(subject)
        return True

class AwardWhimBucksLootOp(BaseLootOperation):
    __qualname__ = 'AwardWhimBucksLootOp'
    FACTORY_TUNABLES = {'description': '\n            This loot will give the specified number of whim bucks to the sim. \n            ', 'whim_bucks': TunableRange(description='\n            The number of whim bucks to give.\n            ', tunable_type=int, default=1, minimum=1)}

    def __init__(self, whim_bucks, **kwargs):
        super().__init__(**kwargs)
        self._whim_bucks = whim_bucks

    def _apply_to_subject_and_target(self, subject, target, resolver):
        if subject is None:
            logger.error('Subject {} is None for the loot {}..', self.subject, self)
            return False
        subject.add_whim_bucks(self._whim_bucks, SetWhimBucks.COMMAND)

class RefreshWhimsLootOp(BaseLootOperation):
    __qualname__ = 'RefreshWhimsLootOp'

    def _apply_to_subject_and_target(self, subject, target, resolver):
        if subject is None:
            logger.error('Subject {} is None for the loot {}..', self.subject, self)
            return False
        subject.whim_tracker.refresh_whims()

class DiscoverClueLootOp(BaseTargetedLootOperation):
    __qualname__ = 'DiscoverClueLootOp'
    FACTORY_TUNABLES = {'career_reference': TunableReference(description='\n            A reference to the detective career that keeps track of what clues\n            to display to the player.\n            ', manager=services.get_instance_manager(sims4.resources.Types.CAREER), class_restrictions=('DetectiveCareer',)), 'fallback_actions': TunableReference(description='\n            List of loot actions that will occur if there are no more clues to\n            be discovered. This can be used to hook up a notification, for\n            example.\n            ', manager=services.get_instance_manager(sims4.resources.Types.ACTION), allow_none=True, class_restrictions=('LootActions', 'RandomWeightedLoot'))}

    def __init__(self, *args, career_reference, fallback_actions, **kwargs):
        super().__init__(*args, **kwargs)
        self.career_reference = career_reference
        self.fallback_actions = fallback_actions

    @property
    def loot_type(self):
        return LootType.DISCOVER_CLUE

    def _apply_to_subject_and_target(self, subject, target, resolver):
        career = subject.career_tracker.careers.get(self.career_reference.guid64)
        if career is None:
            logger.error('Failed to find career {} on Sim {}.', self.career_reference, subject, owner='bhill')
            return
        clue = career.pop_unused_clue()
        if clue is None:
            if self.fallback_actions:
                for action in self.fallback_actions:
                    action.apply_to_resolver(resolver)
            return
        if clue.notification is not None:
            dialog = clue.notification(subject, resolver=resolver)
            if dialog is not None:
                dialog.show_dialog()
        if clue.notebook_entry is not None:
            subject.notebook_tracker.unlock_entry(clue.notebook_entry())
        with telemetry_helper.begin_hook(loot_op_telemetry_writer, TELEMETRY_HOOK_DETECTIVE_CLUE, sim_info=subject) as hook:
            hook.write_guid(TELEMETRY_DETECTIVE_CLUE_FOUND, clue.guid64)

class NewCrimeLootOp(BaseTargetedLootOperation):
    __qualname__ = 'NewCrimeLootOp'
    FACTORY_TUNABLES = {'career_reference': TunableReference(description='\n            A reference to the detective career that keeps track of what crime\n            is currently being tracked.\n            ', manager=services.get_instance_manager(sims4.resources.Types.CAREER), class_restrictions=('DetectiveCareer',))}

    def __init__(self, *args, career_reference, **kwargs):
        super().__init__(*args, **kwargs)
        self.career_reference = career_reference

    @property
    def loot_type(self):
        return LootType.NEW_CRIME

    def _apply_to_subject_and_target(self, subject, target, resolver):
        career = subject.career_tracker.careers.get(self.career_reference.guid64)
        if career is None:
            logger.error('Failed to find career {} on Sim {}.', self.career_reference, subject, owner='bhill')
            return
        career.create_new_crime_data()

class BreakThroughLootOperation(BaseLootOperation):
    __qualname__ = 'BreakThroughLootOperation'
    FACTORY_TUNABLES = {'breakthrough_commodity': TunableReference(description='\n            The commodity that tracks the breakthrough progress.\n            ', manager=services.get_instance_manager(sims4.resources.Types.STATISTIC)), 'time': TunableRealSecond(description='\n            The amount of time, in real seconds, to show headline effect.\n            ', default=5)}

    def __init__(self, *args, breakthrough_commodity, time, **kwargs):
        super().__init__(*args, **kwargs)
        self.breakthrough_commodity = breakthrough_commodity
        self.time = time

    def _apply_to_subject_and_target(self, subject, target, resolver):
        if not subject.is_sim:
            logger.error('Subject {} is not a Sim for the loot {}.', self.subject, self)
            return
        progress = 0
        commodity = subject.get_statistic(self.breakthrough_commodity, add=False)
        if commodity is not None:
            progress = int(100*commodity.get_normalized_value())
        op = BreakThroughMessage(sim_id=subject.id, progress=progress, display_time=self.time)
        distributor.system.Distributor.instance().add_op(subject, op)

class DestroyObjectsFromInventoryLootOp(BaseLootOperation):
    __qualname__ = 'DestroyObjectsFromInventoryLootOp'
    ALL = 'ALL'
    FACTORY_TUNABLES = {'description': "\n            Destroy every object in the target's inventory that passes the\n            tuned tests.\n            ", 'object_tests': TunableTestSet(description='\n            A list of tests to apply to all objects in the target inventory.\n            Every object that passes these tests will be destroyed.\n            '), 'count': TunableVariant(description='\n            The number of each object type to destroy. (e.g. if count is 2,\n            then for every type of object that passes the tests, 2 of that\n            object type will be destroyed). If this number exceeds the size of\n            a stack, then the whole stack will be removed without errors.\n            ', number=TunableRange(tunable_type=int, default=1, minimum=0), locked_args={'all': ALL}, default='all')}

    def __init__(self, *args, object_tests, count, **kwargs):
        super().__init__(*args, **kwargs)
        self.object_tests = object_tests
        self.count = count

    def _apply_to_subject_and_target(self, subject, target, resolver):
        if subject.is_sim:
            subject = subject.get_sim_instance()
        inventory = getattr(subject, 'inventory_component', None)
        if inventory is None:
            logger.error('Subject {} does not have an inventory to check for objects to destroy.', subject)
            return
        objects_to_destroy = set()
        for obj in inventory:
            single_object_resolver = event_testing.resolver.SingleObjectResolver(obj)
            if not self.object_tests.run_tests(single_object_resolver):
                pass
            objects_to_destroy.add(obj)
        for obj in objects_to_destroy:
            if self.count == self.ALL:
                count = obj.stack_count()
            else:
                count = min(self.count, obj.stack_count())
            while not inventory.try_destroy_object(obj, count=count, source=inventory, cause='Destroying specified objects from inventory loot op.'):
                logger.error('Error trying to destroy object {}.', obj)
        objects_to_destroy.clear()

class RemoveNotebookEntry(BaseLootOperation):
    __qualname__ = 'RemoveNotebookEntry'
    FACTORY_TUNABLES = {'subcategory_id': TunableEnumEntry(description='\n            Subcategory type.\n            ', tunable_type=NotebookSubCategories, default=NotebookSubCategories.INVALID), 'removal_type': OptionalTunable(description='\n            Option to select if we want to remove by subcategory (like remove\n            all clues) or by a specific entry.\n            ', tunable=TunableList(description='\n                List of entries to be removed.\n                ', tunable=TunableReference(description="\n                    The entry that will be removed from the player's notebook.\n                    ", manager=services.get_instance_manager(sims4.resources.Types.NOTEBOOK_ENTRY), pack_safe=True)), disabled_name='all_entries', enabled_name='remove_by_reference')}

    def __init__(self, *args, subcategory_id, removal_type, **kwargs):
        super().__init__(*args, **kwargs)
        self.subcategory_id = subcategory_id
        self.removal_type = removal_type

    def _apply_to_subject_and_target(self, subject, target, resolver):
        if not subject.is_sim:
            logger.error('Subject {} is not a Sim for the loot {}.', self.subject, self)
            return
        if self.removal_type is None:
            subject.notebook_tracker.remove_entries_by_subcategory(self.subcategory_id)
        else:
            for entry in self.removal_type:
                subject.notebook_tracker.remove_entry_by_reference(self.subcategory_id, entry)

def _validate_lock_subject_and_target(loot, subject, target):
    if not subject.is_sim:
        logger.error('Subject {} is not a Sim for the loot {}.', subject, loot, owner='cjiang')
        return False
    if not target.has_component(PORTAL_LOCKING_COMPONENT):
        logger.error('Target {} is not a Portal for the loot {}.', target, loot, owner='cjiang')
        return False
    return True

class LockDoor(BaseTargetedLootOperation):
    __qualname__ = 'LockDoor'
    FACTORY_TUNABLES = {'lock_data': TunableVariant(lock_siminfo=LockSimInfoData.TunableFactory(), lock_all_with_simid_exception=LockAllWithSimIdExceptionData.TunableFactory(), lock_all_with_situation_job_exception=LockAllWithSituationJobExceptionData.TunableFactory(), default='lock_all_with_simid_exception'), 'replace_same_lock_type': Tunable(description='\n            If True, it will replace the same type of lock data in the locking\n            component, otherwise it will update the existing data.\n            ', tunable_type=bool, default=True)}

    def __init__(self, *args, lock_data, replace_same_lock_type, **kwargs):
        super().__init__(*args, **kwargs)
        self.lock_data = lock_data
        self.replace_same_lock_type = replace_same_lock_type

    def _apply_to_subject_and_target(self, subject, target, resolver):
        if not _validate_lock_subject_and_target(self, subject, target):
            return
        lock_data = self.lock_data()
        lock_data.setup_data_by_sim(subject)
        target.add_lock_data(lock_data, replace_same_lock_type=self.replace_same_lock_type)

class UnlockDoor(BaseTargetedLootOperation):
    __qualname__ = 'UnlockDoor'
    FACTORY_TUNABLES = {'unlock_type': OptionalTunable(description='\n            The type of the lock we want to remove, by default should be everything.\n            ', tunable=TunableEnumEntry(tunable_type=LockType, default=LockType.LOCK_ALL_WITH_SIMID_EXCEPTION), disabled_name='unlock_every_type')}

    def __init__(self, *args, unlock_type, **kwargs):
        super().__init__(*args, **kwargs)
        self.unlock_type = unlock_type

    def _apply_to_subject_and_target(self, subject, target, resolver):
        if not _validate_lock_subject_and_target(self, subject, target):
            return
        target.remove_player_locks(unlock_type=self.unlock_type)

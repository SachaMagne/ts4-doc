from protocolbuffers import Consts_pb2
from interactions import ParticipantType
from interactions.interaction_finisher import FinishingType
from interactions.liability import Liability
from interactions.utils.interaction_elements import XevtTriggeredElement
from retail.retail_funds import RetailFundsCategory
from retail.retail_utils import RetailUtils
from sims.funds import FundsSource, get_funds_for_source
from sims4.localization import TunableLocalizedStringFactory
from sims4.tuning.tunable import TunableVariant, Tunable, HasTunableFactory, HasTunableSingletonFactory, AutoFactoryInit, TunableEnumEntry, OptionalTunable
from singletons import DEFAULT
from tunable_multiplier import TunableMultiplier
import enum
import objects.components.types
import sims4.log
logger = sims4.log.Logger('Payment', default_owner='trevor')

def _get_tunable_payment_variant(*args, allow_retail=True, **kwargs):
    kwargs['amount'] = _PaymentAmount.TunableFactory()
    kwargs['bills'] = _PaymentBills.TunableFactory()
    kwargs['catalog_value'] = _PaymentCatalogValue.TunableFactory()
    kwargs['current_value'] = _PaymentCurrentValue.TunableFactory()
    kwargs['base_retail_value'] = _PaymentBaseRetailValue.TunableFactory()
    if allow_retail:
        kwargs['retail_amount'] = _PaymentRetailAmount.TunableFactory()
    return TunableVariant(default='amount', *args, **kwargs)

def _get_tunable_payment_source_variant(*args, **kwargs):
    kwargs['household'] = _PaymentSourceHousehold.TunableFactory()
    kwargs['retail'] = _PaymentSourceRetail.TunableFactory()
    return TunableVariant(default='household', *args, **kwargs)

class _PaymentSource(HasTunableSingletonFactory, AutoFactoryInit):
    __qualname__ = '_PaymentSource'

    def try_reserve_funds(self, sim, amount):
        raise NotImplementedError

class _PaymentSourceHousehold(_PaymentSource):
    __qualname__ = '_PaymentSourceHousehold'

    @property
    def funds_source(self):
        return FundsSource.HOUSEHOLD

    def try_reserve_funds(self, sim, amount):
        household_funds = get_funds_for_source(self.funds_source, sim=sim)
        return household_funds.try_remove(amount, Consts_pb2.TELEMETRY_INTERACTION_COST, sim)

class _PaymentSourceRetail(_PaymentSource):
    __qualname__ = '_PaymentSourceRetail'
    FACTORY_TUNABLES = {'funds_category': TunableEnumEntry(description='\n            If defined, this expense is categorized and can be displayed in the\n            Retail finance dialog.\n            ', tunable_type=RetailFundsCategory, default=RetailFundsCategory.NONE)}

    @property
    def funds_source(self):
        retail_funds = get_funds_for_source(FundsSource.RETAIL, sim=None)
        if retail_funds is None:
            return FundsSource.HOUSEHOLD
        return FundsSource.RETAIL

    def try_reserve_funds(self, sim, amount):
        retail_funds = get_funds_for_source(FundsSource.RETAIL, sim=sim)
        if retail_funds is None:
            return _PaymentSourceHousehold().try_reserve_funds(sim, amount)
        return retail_funds.try_remove(amount, Consts_pb2.TELEMETRY_INTERACTION_COST, sim, funds_category=self.funds_category)

class _Payment(HasTunableSingletonFactory, AutoFactoryInit):
    __qualname__ = '_Payment'

    def on_payment(self, amount, resolver):
        return True

class _PaymentAmount(_Payment):
    __qualname__ = '_PaymentAmount'
    FACTORY_TUNABLES = {'amount': Tunable(description='\n            The amount to pay.\n            ', tunable_type=int, default=0)}

    def get_amount(self, resolver):
        return self.amount

class _PaymentBills(_Payment):
    __qualname__ = '_PaymentBills'
    FACTORY_TUNABLES = {'participant': TunableEnumEntry(description='\n            The participant for whom we need to pay bills.\n            ', tunable_type=ParticipantType, default=ParticipantType.Actor)}

    def _get_bills_manager(self, resolver):
        participant = resolver.get_participant(self.participant)
        if participant is not None:
            household = participant.household
            if household is not None:
                return household.bills_manager

    def get_amount(self, resolver):
        bills_manager = self._get_bills_manager(resolver)
        if bills_manager is not None:
            return bills_manager.current_payment_owed

    def on_payment(self, amount, resolver):
        bills_manager = self._get_bills_manager(resolver)
        if bills_manager is not None:
            if bills_manager.current_payment_owed != amount:
                return False
            bills_manager.pay_bill()
            return True
        return False

class _PaymentCatalogValue(_Payment):
    __qualname__ = '_PaymentCatalogValue'
    FACTORY_TUNABLES = {'participant': TunableEnumEntry(description='\n            The participant for which we want to pay an amount equal to its\n            catalog value.\n            ', tunable_type=ParticipantType, default=ParticipantType.Object)}

    def get_amount(self, resolver):
        participant = resolver.get_participant(self.participant)
        if participant is not None:
            return participant.definition.price

class _PaymentCurrentValue(_Payment):
    __qualname__ = '_PaymentCurrentValue'
    FACTORY_TUNABLES = {'participant': TunableEnumEntry(description='\n            The participant for which we want to pay an amount equal to its\n            current value.\n            ', tunable_type=ParticipantType, default=ParticipantType.Object)}

    def get_amount(self, resolver):
        participant = resolver.get_participant(self.participant)
        if participant is not None:
            return participant.current_value

class _PaymentBaseRetailValue(_Payment):
    __qualname__ = '_PaymentBaseRetailValue'
    FACTORY_TUNABLES = {'participant': TunableEnumEntry(description='\n            The participant for which we want to pay an amount equal to its\n            retail value.\n            ', tunable_type=ParticipantType, default=ParticipantType.Object)}

    def get_amount(self, resolver):
        participant = resolver.get_participant(self.participant)
        if participant is not None:
            retail_component = participant.get_component(objects.components.types.RETAIL_COMPONENT)
            if retail_component is not None:
                return retail_component.get_retail_value()
            return participant.current_value

class PaymentRetailRevenueType(enum.Int):
    __qualname__ = 'PaymentRetailRevenueType'
    ITEM_SOLD = 0
    SEED_MONEY = 1

class _PaymentRetailAmount(_Payment):
    __qualname__ = '_PaymentRetailAmount'
    FACTORY_TUNABLES = {'retail_expense': _get_tunable_payment_variant(description='\n            The amount to pay, affected by retail multipliers. If this is 0,\n            then this operation costs nothing.\n            ', allow_retail=False), 'generate_revenue': OptionalTunable(description='\n            If this is enabled, then the retail provider will gain the spent\n            amount as revenue. If this is not enabled, then the expense is\n            incurred and no revenue is generated.\n            ', tunable=TunableEnumEntry(description='\n                The type of revenue generated by this interaction. If the type\n                is Item Sold, the items old count for the store will increment.\n                If the type is Seen Money, the money is added to the store\n                without the sold item count being touched.\n                ', tunable_type=PaymentRetailRevenueType, default=PaymentRetailRevenueType.ITEM_SOLD), enabled_by_default=True)}

    def get_amount(self, resolver):
        retail_manager = RetailUtils.get_retail_manager_for_zone()
        if retail_manager is not None:
            retail_expense = self.retail_expense.get_amount(resolver)
            if self.generate_revenue == PaymentRetailRevenueType.ITEM_SOLD:
                return retail_manager.get_value_with_markup(retail_expense)
            return retail_expense
        return self.retail_expense.get_amount(resolver)

    def on_payment(self, amount, resolver):
        if self.generate_revenue is None:
            return True
        retail_manager = RetailUtils.get_retail_manager_for_zone()
        if retail_manager is not None:
            retail_manager.modify_funds(amount, from_item_sold=self.generate_revenue == PaymentRetailRevenueType.ITEM_SOLD)
        return True

class PaymentLiability(Liability, HasTunableFactory):
    __qualname__ = 'PaymentLiability'
    LIABILITY_TOKEN = 'PaymentLiability'
    FACTORY_TUNABLES = {'cost': _get_tunable_payment_variant(description='\n            The amount of money it costs to run this interaction.\n            '), 'source': _get_tunable_payment_source_variant(description='\n            The source from which to remove the funds.\n            ')}

    def __init__(self, interaction, cost, source, init_on_add=False, **kwargs):
        self.interaction = interaction
        self.reserved_funds = None
        self.cost = cost
        self.source = source
        self._init_on_add = init_on_add

    @classmethod
    def on_affordance_loaded_callback(cls, affordance, liability_tuning):

        def get_simoleon_delta(interaction, target=DEFAULT, context=DEFAULT):
            return (-liability_tuning.cost.get_amount(interaction.get_resolver(target=target, context=context)), liability_tuning.source.funds_source)

        affordance.register_simoleon_delta_callback(get_simoleon_delta)

    def on_run(self):
        if not self._init_on_add:
            self._initialize()

    def on_add(self, *args, **kwargs):
        if self._init_on_add:
            self._initialize()

    def _initialize(self):
        if self.reserved_funds is not None:
            return
        amount_to_reserve = self.interaction.get_simoleon_cost()
        if amount_to_reserve <= 0:
            return
        sim = self.interaction.sim
        self.reserved_funds = self.source.try_reserve_funds(sim, amount_to_reserve)
        if self.reserved_funds is None:
            self.interaction.cancel(FinishingType.FAILED_TESTS, cancel_reason_msg='Source funds does not have enough money to perform this interaction. Source:{}'.format(self.source))

    def should_transfer(self, continuation):
        if not self.interaction.is_finishing:
            return True
        return self.interaction.is_finishing_naturally

    def transfer(self, interaction):
        self.interaction = interaction

    def process_payment(self):
        if self.reserved_funds is None:
            return
        if self.cost.on_payment(self.reserved_funds.amount, self.interaction.get_resolver()):
            self.reserved_funds.apply()
        else:
            self.reserved_funds.cancel()
        self.reserved_funds = None

    def release(self, *args, **kwargs):
        if self.reserved_funds is None:
            return
        if self.interaction.is_finishing_naturally:
            self.process_payment()
        else:
            self.reserved_funds.cancel()
        self.reserved_funds = None

class PaymentElement(XevtTriggeredElement):
    __qualname__ = 'PaymentElement'
    FACTORY_TUNABLES = {'cost': _get_tunable_payment_variant(description='\n            The type of payment an amount to pay.\n            '), 'source': _get_tunable_payment_source_variant(description='\n            The source of the funds.\n            '), 'display_only': Tunable(description="\n            A PaymentElement marked as display_only will affect an affordance's\n            display name (by appending the Simoleon cost in parentheses), but\n            will not deduct funds when run.\n            ", tunable_type=bool, default=False), 'cost_modifiers': TunableMultiplier.TunableFactory(description='\n            A tunable list of test sets and associated multipliers to apply to\n            the total cost of this payment.\n            ')}

    def __init__(self, interaction, **kwargs):
        super().__init__(interaction, **kwargs)
        if not self.display_only:
            self.liability = PaymentLiability(interaction, self.cost, self.source, init_on_add=True)
            interaction.add_liability(self.liability.LIABILITY_TOKEN, self.liability)

    @classmethod
    def on_affordance_loaded_callback(cls, affordance, payment_element):

        def get_simoleon_delta(interaction, target=DEFAULT, context=DEFAULT):
            interaction_resolver = interaction.get_resolver(target=target, context=context)
            payment_owed = payment_element.cost.get_amount(interaction_resolver) or 0
            if payment_owed:
                payment_owed *= -payment_element.cost_modifiers.get_multiplier(interaction_resolver)
            return (round(payment_owed), payment_element.source.funds_source)

        affordance.register_simoleon_delta_callback(get_simoleon_delta)

    def _do_behavior(self):
        if self.display_only:
            return
        self.liability.process_payment()

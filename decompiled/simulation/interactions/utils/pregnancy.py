import itertools
import random
from protocolbuffers import SimObjectAttributes_pb2 as protocols
from protocolbuffers.Localization_pb2 import LocalizedStringToken
from date_and_time import DateAndTime
from event_testing.resolver import SingleSimResolver, DoubleSimResolver
from event_testing.test_events import TestEvent
from filters.tunable import TunableSimFilter
from interactions import ParticipantTypeSingle
from interactions.utils.death import get_death_interaction
from interactions.utils.interaction_elements import XevtTriggeredElement
from relationships.relationship_bit import RelationshipBit
from relationships.relationship_tracker_tuning import DefaultGenealogyLink
from sims.sim_dialogs import SimPersonalityAssignmentDialog
from sims.sim_info_types import Gender, Age
from sims.sim_spawner import SimSpawner, SimCreator
from sims4.common import is_available_pack, Pack
from sims4.math import EPSILON, clamp, MAX_UINT32
from sims4.random import pop_weighted
from sims4.tuning.dynamic_enum import DynamicEnumLocked
from sims4.tuning.tunable import TunablePercent, TunableReference, TunableEnumEntry, TunableRange, TunableList, TunableTuple, Tunable, OptionalTunable, AutoFactoryInit, HasTunableSingletonFactory, TunableVariant, TunableMapping
from singletons import DEFAULT
from traits.traits import Trait, get_possible_traits
from tunable_multiplier import TunableMultiplier
from ui.ui_dialog import UiDialogOk
from ui.ui_dialog_generic import TEXT_INPUT_FIRST_NAME, TEXT_INPUT_LAST_NAME
import distributor.ops
import services
import sims4.log
import ui
logger = sims4.log.Logger('Pregnancy', default_owner='epanero')

class PregnancyClientMixin:
    __qualname__ = 'PregnancyClientMixin'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._pregnancy_progress = 0

    @distributor.fields.Field(op=distributor.ops.SetPregnancyProgress, default=None)
    def pregnancy_progress(self):
        return self._pregnancy_progress

    @pregnancy_progress.setter
    def pregnancy_progress(self, value):
        self._pregnancy_progress = clamp(0, value, 1) if value is not None else None

class PregnancyOrigin(DynamicEnumLocked):
    __qualname__ = 'PregnancyOrigin'
    DEFAULT = 0

class PregnancyOffspringData:
    __qualname__ = 'PregnancyOffspringData'

    def __init__(self, gender, genetics, first_name='', last_name='', traits=DEFAULT):
        self.gender = gender
        self.genetics = genetics
        self.first_name = first_name
        self.last_name = last_name
        self.traits = [] if traits is DEFAULT else traits

    @property
    def is_female(self):
        return self.gender == Gender.FEMALE

    def populate_localization_token(self, token):
        token.type = LocalizedStringToken.SIM
        token.first_name = self.first_name
        token.last_name = self.last_name
        token.is_female = self.is_female

class PregnancyTracker:
    __qualname__ = 'PregnancyTracker'
    PREGNANCY_COMMODITY = TunableReference(description='\n        The commodity to award if conception is successful.\n        ', manager=services.statistic_manager())
    PREGNANCY_TRAIT = TunableReference(description='\n        The trait that all pregnant Sims have during pregnancy.\n        ', manager=services.trait_manager())
    PREGNANCY_RATE = TunableRange(description='\n        The rate per Sim minute of pregnancy.\n        ', tunable_type=float, default=0.001, minimum=EPSILON)
    PREGNANCY_DIALOG = SimPersonalityAssignmentDialog.TunableFactory(description="\n        The dialog that is displayed when an offspring is created. It allows the\n        player to enter a first and last name for the Sim. An additional token\n        is passed in: the offspring's Sim data.\n        ", text_inputs=(TEXT_INPUT_FIRST_NAME, TEXT_INPUT_LAST_NAME))
    MULTIPLE_OFFSPRING_CHANCES = TunableList(description='\n        A list defining the probabilities of multiple births.\n        ', tunable=TunableTuple(size=Tunable(description='\n                The number of offspring born.\n                ', tunable_type=int, default=1), weight=Tunable(description='\n                The weight, relative to other outcomes.\n                ', tunable_type=float, default=1), npc_dialog=UiDialogOk.TunableFactory(description='\n                A dialog displayed when a NPC Sim gives birth to an offspring\n                that was conceived by a currently player-controlled Sim. The\n                dialog is specifically used when this number of offspring is\n                generated.\n                \n                Three tokens are passed in: the two parent Sims and the\n                offspring\n                ', locked_args={'text_tokens': None}), modifiers=TunableMultiplier.TunableFactory(description='\n                A tunable list of test sets and associated multipliers to apply\n                to the total chance of this number of potential offspring.\n                '), screen_slam_one_parent=OptionalTunable(description='\n                Screen slam to show when only one parent is available.\n                Localization Tokens: Sim A - {0.SimFirstName}\n                ', tunable=ui.screen_slam.TunableScreenSlamSnippet()), screen_slam_two_parents=OptionalTunable(description='\n                Screen slam to show when both parents are available.\n                Localization Tokens: Sim A - {0.SimFirstName}, Sim B -\n                {1.SimFirstName}\n                ', tunable=ui.screen_slam.TunableScreenSlamSnippet())))
    MONOZYGOTIC_OFFSPRING_CHANCE = TunablePercent(description='\n        The chance that each subsequent offspring of a multiple birth has the\n        same genetics as the first offspring.\n        ', default=50)
    GENDER_CHANCE_STAT = TunableReference(description='\n        A commodity that determines the chance that an offspring is female. The\n        minimum value guarantees the offspring is male, whereas the maximum\n        value guarantees it is female.\n        ', manager=services.statistic_manager())
    BIRTHPARENT_BIT = RelationshipBit.TunableReference(description='\n        The bit that is added on the relationship from the Sim to any of its\n        offspring.\n        ')
    PREGNANCY_ORIGIN_MODIFIERS = TunableMapping(description='\n        Define any modifiers that, given the origination of the pregnancy,\n        affect certain aspects of the generated offspring.\n        ', key_type=TunableEnumEntry(description='\n            The origin of the pregnancy.\n            ', tunable_type=PregnancyOrigin, default=PregnancyOrigin.DEFAULT, pack_safe=True), value_type=TunableTuple(description='\n            The aspects of the pregnancy modified specifically for the specified\n            origin.\n            ', default_relationships=TunableTuple(description='\n                Override default relationships for the parents.\n                ', father_override=OptionalTunable(description='\n                    If set, override default relationships for the father.\n                    ', tunable=TunableEnumEntry(description='\n                        The default relationships for the father.\n                        ', tunable_type=DefaultGenealogyLink, default=DefaultGenealogyLink.FamilyMember)), mother_override=OptionalTunable(description='\n                    If set, override default relationships for the mother.\n                    ', tunable=TunableEnumEntry(description='\n                        The default relationships for the mother.\n                        ', tunable_type=DefaultGenealogyLink, default=DefaultGenealogyLink.FamilyMember))), trait_entries=TunableList(description='\n                Sets of traits that might be randomly applied to each generated\n                offspring. Each group is individually randomized.\n                ', tunable=TunableTuple(description='\n                    A set of random traits. Specify a chance that a trait from\n                    the group is selected, and then specify a set of traits.\n                    Only one trait from this group may be selected. If the\n                    chance is less than 100%, no traits could be selected.\n                    ', chance=TunablePercent(description='\n                        The chance that a trait from this set is selected.\n                        ', default=100), traits=TunableList(description='\n                        The set of traits that might be applied to each\n                        generated offspring. Specify a weight for each trait\n                        compared to other traits in the same set.\n                        ', tunable=TunableTuple(description='\n                            A weighted trait that might be applied to the\n                            generated offspring. The weight is relative to other\n                            entries within the same set.\n                            ', weight=Tunable(description='\n                                The relative weight of this trait compared to\n                                other traits within the same set.\n                                ', tunable_type=float, default=1), trait=Trait.TunableReference(description='\n                                A trait that might be applied to the generated\n                                offspring.\n                                ', pack_safe=True)))))))

    def __init__(self, sim_info):
        self._sim_info = sim_info
        self._last_modified = None
        self._offspring_count_override = None
        self.clear_pregnancy()

    @property
    def account(self):
        return self._sim_info.account

    @property
    def is_pregnant(self):
        if self._seed:
            return True
        return False

    @property
    def offspring_count(self):
        return max(len(self._offspring_data), 1)

    @property
    def offspring_count_override(self):
        return self._offspring_count_override

    @offspring_count_override.setter
    def offspring_count_override(self, value):
        self._offspring_count_override = value

    def _get_parent(self, sim_id):
        sim_info_manager = services.sim_info_manager()
        if sim_id in sim_info_manager:
            return sim_info_manager.get(sim_id)

    def get_parents(self):
        if self._parent_ids:
            parent_a = self._get_parent(self._parent_ids[0])
            parent_b = self._get_parent(self._parent_ids[1]) or parent_a
            return (parent_a, parent_b)
        return (None, None)

    def get_partner(self):
        (owner, partner) = self.get_parents()
        if partner is not owner:
            return partner

    def start_pregnancy(self, parent_a, parent_b, pregnancy_origin=PregnancyOrigin.DEFAULT):
        if self.is_pregnant:
            return
        if not parent_a.incest_prevention_test(parent_b):
            return
        self._seed = random.randint(1, MAX_UINT32)
        self._parent_ids = (parent_a.id, parent_b.id)
        self._offspring_data = []
        self._origin = pregnancy_origin
        self.enable_pregnancy()

    def enable_pregnancy(self):
        if self.is_pregnant and not self._is_enabled:
            tracker = self._sim_info.get_tracker(self.PREGNANCY_COMMODITY)
            pregnancy_commodity = tracker.get_statistic(self.PREGNANCY_COMMODITY, add=True)
            pregnancy_commodity.add_statistic_modifier(self.PREGNANCY_RATE)
            self._sim_info.add_trait(self.PREGNANCY_TRAIT)
            self._last_modified = None
            self._is_enabled = True

    def update_pregnancy(self):
        if self.is_pregnant:
            if self._last_modified is not None:
                tracker = self._sim_info.get_tracker(self.PREGNANCY_COMMODITY)
                pregnancy_commodity = tracker.get_statistic(self.PREGNANCY_COMMODITY, add=True)
                if pregnancy_commodity.get_value() >= self.PREGNANCY_COMMODITY.max_value:
                    self._create_and_name_offspring()
                    self._show_npc_dialog()
                    self.clear_pregnancy()
                else:
                    delta_time = services.time_service().sim_now - self._last_modified
                    delta = self.PREGNANCY_RATE*delta_time.in_minutes()
                    pregnancy_commodity.add_value(delta)
            self._last_modified = services.time_service().sim_now

    def complete_pregnancy(self):
        services.get_event_manager().process_event(TestEvent.OffspringCreated, sim_info=self._sim_info, offspring_created=self.offspring_count)
        for tuning_data in self.MULTIPLE_OFFSPRING_CHANCES:
            while tuning_data.size == self.offspring_count:
                (parent_a, parent_b) = self.get_parents()
                if parent_a is parent_b:
                    screen_slam = tuning_data.screen_slam_one_parent
                else:
                    screen_slam = tuning_data.screen_slam_two_parents
                if screen_slam is not None:
                    screen_slam.send_screen_slam_message(self._sim_info, parent_a, parent_b)
                break

    def clear_pregnancy_visuals(self):
        if self._sim_info.pregnancy_progress:
            self._sim_info.pregnancy_progress = 0

    def clear_pregnancy(self):
        self._seed = 0
        self._parent_ids = []
        self._offspring_data = []
        self._origin = PregnancyOrigin.DEFAULT
        tracker = self._sim_info.get_tracker(self.PREGNANCY_COMMODITY)
        if tracker is not None:
            stat = tracker.get_statistic(self.PREGNANCY_COMMODITY, add=True)
            if stat is not None:
                stat.set_value(stat.min_value)
                stat.remove_statistic_modifier(self.PREGNANCY_RATE)
        tracker = self._sim_info.get_tracker(self.GENDER_CHANCE_STAT)
        if tracker is not None:
            stat = tracker.get_statistic(self.GENDER_CHANCE_STAT, add=True)
            if stat is not None:
                stat.set_value(stat.default_value)
        if self._sim_info.has_trait(self.PREGNANCY_TRAIT):
            self._sim_info.remove_trait(self.PREGNANCY_TRAIT)
        self.clear_pregnancy_visuals()
        self._is_enabled = False

    def _create_and_name_offspring(self, on_create=None):
        self.create_offspring_data()
        for offspring_data in self.get_offspring_data_gen():
            offspring_data.first_name = self._get_random_first_name(offspring_data)
            sim_info = self.create_sim_info(offspring_data)
            while on_create is not None:
                on_create(sim_info)

    def create_sim_info(self, offspring_data):
        (parent_a, parent_b) = self.get_parents()
        sim_creator = SimCreator(gender=offspring_data.gender, age=Age.BABY, first_name=offspring_data.first_name, last_name=offspring_data.last_name)
        household = self._sim_info.household
        zone_id = household.home_zone_id
        (sim_info_list, _) = SimSpawner.create_sim_infos((sim_creator,), household=household, account=self.account, zone_id=zone_id, generate_deterministic_sim=True, creation_source='pregnancy')
        sim_info = sim_info_list[0]
        sim_info.world_id = services.get_persistence_service().get_world_id_from_zone(zone_id)
        for trait in tuple(sim_info.trait_tracker.personality_traits):
            sim_info.remove_trait(trait)
        for trait in offspring_data.traits:
            sim_info.add_trait(trait)
        sim_info.apply_genetics(parent_a, parent_b, seed=offspring_data.genetics)
        sim_info.resend_physical_attributes()
        default_track_overrides = {}
        mother = parent_a if parent_a.gender == Gender.FEMALE else parent_b
        father = parent_a if parent_a.gender == Gender.MALE else parent_b
        if self._origin in self.PREGNANCY_ORIGIN_MODIFIERS:
            father_override = self.PREGNANCY_ORIGIN_MODIFIERS[self._origin].default_relationships.father_override
            if father_override is not None:
                default_track_overrides[father] = father_override
            mother_override = self.PREGNANCY_ORIGIN_MODIFIERS[self._origin].default_relationships.mother_override
            if mother_override is not None:
                default_track_overrides[mother] = mother_override
        self.initialize_sim_info(sim_info, parent_a, parent_b, default_track_overrides=default_track_overrides)
        self._sim_info.relationship_tracker.add_relationship_bit(sim_info.id, self.BIRTHPARENT_BIT)
        return sim_info

    @staticmethod
    def initialize_sim_info(sim_info, parent_a, parent_b, default_track_overrides=None):
        sim_info.add_parent_relations(parent_a, parent_b)
        if sim_info.household is not parent_a.household:
            parent_a.household.add_sim_info_to_household(sim_info)
        services.sim_info_manager().set_default_genealogy()
        sim_info.set_default_relationships(reciprocal=True, default_track_overrides=default_track_overrides)
        parent_generation = max(parent_a.generation, parent_b.generation if parent_b is not None else 0)
        sim_info.generation = parent_generation + 1 if not sim_info.household.is_npc_household else parent_generation
        services.get_event_manager().process_event(TestEvent.GenerationCreated, sim_info=sim_info)
        client = services.client_manager().get_client_by_household_id(sim_info.household_id)
        if client is not None:
            client.selectable_sims.add_selectable_sim_info(sim_info)

    def _select_traits_for_offspring(self, gender, random=random):
        traits = []
        personality_trait_slots = Trait.EQUIP_SLOT_NUMBER_MAP[Age.BABY]

        def _add_trait_if_possible(trait):
            nonlocal personality_trait_slots
            if any(t.is_conflicting(selected_trait) for t in traits):
                return
            if selected_trait.is_personality_trait:
                if not personality_trait_slots:
                    return
                personality_trait_slots -= 1
            traits.append(selected_trait)

        if self._origin in self.PREGNANCY_ORIGIN_MODIFIERS:
            trait_entries = self.PREGNANCY_ORIGIN_MODIFIERS[self._origin].trait_entries
            for trait_entry in trait_entries:
                if random.random() >= trait_entry.chance:
                    pass
                selected_trait = pop_weighted([(t.weight, t.trait) for t in trait_entry.traits if t.trait.is_valid_trait(Age.BABY, gender)], random=random)
                while selected_trait is not None:
                    _add_trait_if_possible(selected_trait)
        (parent_a, parent_b) = self.get_parents()
        if parent_a is not None and parent_b is not None:
            for inherited_trait_entries in parent_a.trait_tracker.get_inherited_traits(parent_b):
                selected_trait = pop_weighted(list(inherited_trait_entries), random=random)
                while selected_trait is not None:
                    _add_trait_if_possible(selected_trait)
        if not personality_trait_slots:
            return traits
        personality_traits = get_possible_traits(Age.BABY, gender)
        random.shuffle(personality_traits)
        while True:
            current_trait = personality_traits.pop()
            if not any(trait.is_conflicting(current_trait) for trait in traits):
                traits.append(current_trait)
                personality_trait_slots -= 1
            if not personality_traits or not personality_trait_slots:
                break
        return traits

    def create_offspring_data(self):
        r = random.Random()
        r.seed(self._seed)
        if self._offspring_count_override is not None:
            offspring_count = self._offspring_count_override
        else:
            offspring_count = pop_weighted([(p.weight*p.modifiers.get_multiplier(SingleSimResolver(self._sim_info)), p.size) for p in self.MULTIPLE_OFFSPRING_CHANCES], random=r)
        offspring_count = min(self._sim_info.household.free_slot_count + 1, offspring_count)
        self._offspring_data = []
        for offspring_index in range(offspring_count):
            if offspring_index and r.random() < self.MONOZYGOTIC_OFFSPRING_CHANCE:
                gender = self._offspring_data[offspring_index - 1].gender
                genetics = self._offspring_data[offspring_index - 1].genetics
            else:
                gender_chance_stat = self._sim_info.get_statistic(self.GENDER_CHANCE_STAT)
                if gender_chance_stat is None:
                    gender_chance = 0.5
                else:
                    gender_chance = (gender_chance_stat.get_value() - gender_chance_stat.min_value)/(gender_chance_stat.max_value - gender_chance_stat.min_value)
                gender = Gender.FEMALE if r.random() < gender_chance else Gender.MALE
                genetics = r.randint(1, MAX_UINT32)
            last_name = SimSpawner.get_family_name_for_gender(self._sim_info.account, self._sim_info.last_name, gender == Gender.FEMALE)
            traits = self._select_traits_for_offspring(gender, r)
            self._offspring_data.append(PregnancyOffspringData(gender, genetics, last_name=last_name, traits=traits))

    def get_offspring_data_gen(self):
        for offspring_data in self._offspring_data:
            yield offspring_data

    def _get_random_first_name(self, offspring_data):
        tries_left = 10

        def is_valid(first_name):
            nonlocal tries_left
            if not first_name:
                return False
            tries_left -= 1
            if tries_left and any(sim.first_name == first_name for sim in self._sim_info.household):
                return False
            if any(sim.first_name == first_name for sim in self._offspring_data):
                return False
            return True

        first_name = None
        while not is_valid(first_name):
            first_name = SimSpawner.get_random_first_name(self.account, offspring_data.is_female)
        return first_name

    def assign_random_first_names_to_offspring_data(self):
        for offspring_data in self.get_offspring_data_gen():
            offspring_data.first_name = self._get_random_first_name(offspring_data)

    def _show_npc_dialog(self):
        for tuning_data in self.MULTIPLE_OFFSPRING_CHANCES:
            while tuning_data.size == self.offspring_count:
                npc_dialog = tuning_data.npc_dialog
                if npc_dialog is not None:
                    for parent in self.get_parents():
                        if parent is None:
                            logger.error('Pregnancy for {} has a None parent for IDs {}. Please file a DT with a save attached.', self._sim_info, ','.join(str(parent_id) for parent_id in self._parent_ids))
                            return
                        parent_instance = parent.get_sim_instance()
                        while parent_instance is not None and parent_instance.client is not None:
                            additional_tokens = list(itertools.chain(self.get_parents(), self._offspring_data))
                            dialog = npc_dialog(parent_instance, DoubleSimResolver(additional_tokens[0], additional_tokens[1]))
                            dialog.show_dialog(additional_tokens=additional_tokens)
                return

    def save(self):
        data = protocols.PersistablePregnancyTracker()
        data.seed = self._seed
        data.origin = self._origin
        if self._last_modified is not None:
            self.last_modified = self._last_modified.absolute_ticks()
        data.parent_ids.extend(self._parent_ids)
        return data

    def load(self, data):
        self._seed = int(data.seed)
        try:
            self._origin = PregnancyOrigin(data.origin)
        except KeyError:
            self._origin = PregnancyOrigin.DEFAULT
        if data.HasField('last_modified'):
            self._last_modified = DateAndTime(data.last_modified)
        self._parent_ids.clear()
        self._parent_ids.extend(data.parent_ids)

    def refresh_pregnancy_data(self, on_create=None):
        if not self.is_pregnant:
            return
        if not (self._sim_info.is_male and is_available_pack(Pack.EP01)):
            self._create_and_name_offspring(on_create=on_create)
            self.clear_pregnancy()

class PregnancyElement(XevtTriggeredElement):
    __qualname__ = 'PregnancyElement'

    class _PregnancyParentParticipant(HasTunableSingletonFactory, AutoFactoryInit):
        __qualname__ = 'PregnancyElement._PregnancyParentParticipant'
        FACTORY_TUNABLES = {'subject': TunableEnumEntry(description='\n                The participant of the interaction that is to be the\n                impregnator.\n                ', tunable_type=ParticipantTypeSingle, default=ParticipantTypeSingle.TargetSim)}

        def get_parent(self, interaction, pregnancy_subject_sim_info):
            parent = interaction.get_participant(self.subject)
            return parent.sim_info

    class _PregnancyParentFilter(HasTunableSingletonFactory, AutoFactoryInit):
        __qualname__ = 'PregnancyElement._PregnancyParentFilter'
        FACTORY_TUNABLES = {'filter': TunableSimFilter.TunableReference(description='\n                The filter to use to find a parent.\n                ')}

        def get_parent(self, interaction, pregnancy_subject_sim_info):
            filter_results = services.sim_filter_service().submit_matching_filter(sim_filter=self.filter, allow_yielding=False, requesting_sim_info=pregnancy_subject_sim_info)
            if filter_results:
                parent = random.choice([filter_result.sim_info for filter_result in filter_results])
                return parent

    FACTORY_TUNABLES = {'pregnancy_subject': TunableEnumEntry(description='\n            The participant of this interaction that is to be impregnated. There\n            are no age or gender restrictions on this Sim, so ensure that you\n            are tuning the appropriate tests to avoid unwanted pregnancies.\n            ', tunable_type=ParticipantTypeSingle, default=ParticipantTypeSingle.Actor), 'pregnancy_parent': TunableVariant(description='\n            The participant of this interaction that is to be the impregnator.\n            ', from_participant=_PregnancyParentParticipant.TunableFactory(), from_filter=_PregnancyParentFilter.TunableFactory(), default='from_participant'), 'pregnancy_origin': TunableEnumEntry(description='\n            Define the origin of this pregnancy. This value is used to determine\n            some of the random elements at birth.\n            ', tunable_type=PregnancyOrigin, default=PregnancyOrigin.DEFAULT)}

    def _do_behavior(self, *args, **kwargs):
        subject_sim = self.interaction.get_participant(self.pregnancy_subject)
        if subject_sim is None or not subject_sim.household.free_slot_count:
            return
        death_interaction = get_death_interaction(subject_sim)
        if death_interaction is not None:
            return
        subject_sim_info = subject_sim.sim_info
        parent_sim_info = self.pregnancy_parent.get_parent(self.interaction, subject_sim_info)
        if parent_sim_info is None:
            return
        subject_sim_info.pregnancy_tracker.start_pregnancy(subject_sim_info, parent_sim_info, pregnancy_origin=self.pregnancy_origin)

from _weakrefset import WeakSet
from element_utils import build_critical_section_with_finally
from event_testing.results import TestResult
from interactions import ParticipantType
from sims4.tuning.tunable import Tunable, TunableFactory, TunableEnumFlags, TunableVariant, TunableReference, OptionalTunable, TunableSimMinute
import gsi_handlers
import services

class ReserveObjectHandler:
    __qualname__ = 'ReserveObjectHandler'
    LOCKOUT_TIME = TunableSimMinute(480, description='Number of sim minutes to lockout an in use object from autonomy.')

    def __init__(self, sim, target, reserver, all_parts=False, lock=False):
        self._sim = sim
        self._target = target.ref()
        self._reserver = reserver
        self._registered = False
        self._all_parts = all_parts
        self._lock = lock
        self._reserved_objects = WeakSet()

    @property
    def is_multi(self):
        return False

    def _is_valid_target(self, target):
        return True

    def get_targets(self):
        if self._target is not None:
            target = self._target()
            if not target.is_sim:
                if not self._all_parts:
                    return (target,)
                if target.is_part:
                    target = target.part_owner
                if target.parts:
                    return target.parts
                return (target,)
        return ()

    def _begin(self, element):
        if self.reserve():
            return True
        return False

    def reserve(self):
        if self._registered:
            return True
        if self.may_reserve(from_reservation_call=True):
            for target in self.get_targets():
                target.reserve(self._sim, self._reserver, multi=self.is_multi, lock=self._lock)
                self._reserved_objects.add(target)
            self._registered = True
            return True
        return False

    def end(self, *_, **__):
        if self._registered:
            for target in self._reserved_objects:
                target.release(self._sim, self._reserver, multi=self.is_multi, lock=self._lock)
            self._registered = False

    def may_reserve(self, *args, from_reservation_call=False, **kwargs):
        targets = self.get_targets()
        for target in targets:
            test_result = self._is_valid_target(target)
            if not test_result:
                return test_result
            reserve_result = target.may_reserve(self._sim, multi=self.is_multi, *args, **kwargs)
            if gsi_handlers.sim_handlers_log.sim_reservation_archiver.enabled:
                gsi_handlers.sim_handlers_log.archive_sim_reservation(self._sim, 'may_reserve' if not from_reservation_call else 'reserve', self._reserver, target, reserve_result, self.is_multi)
            while not reserve_result:
                return reserve_result
        return TestResult.TRUE

    def do_reserve(self, sequence=None):
        return build_critical_section_with_finally(self._begin, sequence, self.end)

class MultiReserveObjectHandler(ReserveObjectHandler):
    __qualname__ = 'MultiReserveObjectHandler'

    def __init__(self, *args, reservation_stat=None, **kwargs):
        super().__init__(*args, **kwargs)
        self._reservation_stat = reservation_stat

    @property
    def is_multi(self):
        return True

    @property
    def reservation_stat(self):
        return self._reservation_stat

    def _is_valid_target(self, target):
        if self._reservation_stat is not None:
            tracker = target.get_tracker(self._reservation_stat)
            users = target.get_users(sims_only=True)
            users.discard(self._sim)
            reservations = tracker.get_value(self._reservation_stat) - len(users)
            if reservations <= 0:
                return TestResult(False, '{} does not have enough left of {}'.format(target, self._reservation_stat))
        return TestResult.TRUE

class NestedReserveObjectHandler:
    __qualname__ = 'NestedReserveObjectHandler'

    def __init__(self):
        self._handlers = []
        self._registered = False

    @property
    def is_multi(self):
        return all(handler.is_multi for handler in self._handlers)

    def get_targets(self):
        all_targets = []
        for handler in self._handlers:
            all_targets.extend(handler.get_targets())
        return all_targets

    def add_handler(self, handler):
        self._handlers.append(handler)

    def may_reserve(self, *args, **kwargs):
        for handler in self._handlers:
            reserve_result = handler.may_reserve(*args, **kwargs)
            while not reserve_result:
                return reserve_result
        return True

    def reserve(self, *_, **__):
        if self._registered:
            return True
        if self.may_reserve():
            for handler in self._handlers:
                handler.reserve()
            self._registered = True
            return True
        return False

    def end(self, *_, **__):
        if self._registered:
            for handler in self._handlers:
                handler.end()
            self._registered = False

    def do_reserve(self, sequence=None):
        for handler in self._handlers:
            sequence = handler.do_reserve(sequence=sequence)
        return sequence

def create_reserver(sim, target, reserver=None, interaction=None, xevt=None, handler=ReserveObjectHandler, **kwargs):
    reserve_object_handler = handler(sim, target, reserver, **kwargs)
    if xevt is not None and interaction is not None:
        interaction.store_event_handler(reserve_object_handler.end, handler_id=xevt)
    return reserve_object_handler

class TunableBasicReserveObject(TunableFactory):
    __qualname__ = 'TunableBasicReserveObject'

    @staticmethod
    def _factory(sim, obj, xevt, **kwargs):
        return create_reserver(sim, obj, xevt=xevt, **kwargs)

    FACTORY_TYPE = _factory

class TunableBasicReserveObjectAndParts(TunableFactory):
    __qualname__ = 'TunableBasicReserveObjectAndParts'

    @staticmethod
    def _factory(sim, obj, xevt, **kwargs):
        return create_reserver(sim, obj, xevt=xevt, all_parts=True, **kwargs)

    FACTORY_TYPE = _factory

class TunableReserveAndLockAll(TunableFactory):
    __qualname__ = 'TunableReserveAndLockAll'

    @staticmethod
    def _factory(sim, obj, xevt, **kwargs):
        return create_reserver(sim, obj, xevt=xevt, all_parts=True, lock=True, **kwargs)

    FACTORY_TYPE = _factory

class TunableMultiReserveObject(TunableFactory):
    __qualname__ = 'TunableMultiReserveObject'

    @staticmethod
    def _factory(sim, obj, xevt, reservation_stat, **kwargs):
        return create_reserver(sim, obj, xevt=xevt, handler=MultiReserveObjectHandler, reservation_stat=reservation_stat, **kwargs)

    FACTORY_TYPE = _factory

    def __init__(self, **kwargs):
        super().__init__(reservation_stat=TunableReference(description='\n                The stat driving the available number of reservations.\n                ', manager=services.statistic_manager(), allow_none=True), **kwargs)

DEFAULT_RESERVATION_PARTICIPANT_TYPES = ParticipantType.Object | ParticipantType.CarriedObject | ParticipantType.CraftingObject

class TunableReserveObject(TunableFactory, is_fragment=True):
    __qualname__ = 'TunableReserveObject'
    DEFAULT_DESCRIPTION = "\n        Control which objects are marked as reserved for in use and how. When\n        setting this value, consider these rules:\n         \n        * Sims may never be marked as in use. Do not try to reserve TargetSim,\n        nor you should try to reserve Object if the interaction may run on\n        Sims.\n         \n        * You may not use a 'basic' reservation if you wish to reserve an entire\n        object and that object has parts. In that case, you must use an 'all'\n        reservation.\n        \n         e.g. Sit \n          Sims sit down, and may do so on individual parts of the sofa.\n          Because the interaction targets the part specifically, we use a 'basic'\n          reservation to reserve that and only that part.\n          \n         e.g. Possess\n          Ghosts may possess objects and have them shake. Since they might\n          possess the sofa (in its entirety), we want to use the 'all' reserve\n          type to mark the object, as well as each individual part as reserved\n          for use. This would prevent other Sims from sitting down while the\n          object is being possessed.\n          \n          Note: it is valid to use the 'all' reserve type on any object, not\n          just objects with parts!\n        "

    @staticmethod
    def _factory(sim, participant_resolver, *, reserve_type, subject, xevt, **kwargs):
        handler = NestedReserveObjectHandler()
        for obj in participant_resolver.get_participants(subject):
            while obj is not sim:
                handler.add_handler(reserve_type(sim, obj, xevt, reserver=participant_resolver, **kwargs))
        return handler

    FACTORY_TYPE = _factory

    def __init__(self, description=DEFAULT_DESCRIPTION, **kwargs):
        super().__init__(reserve_type=TunableVariant(basic=TunableBasicReserveObject(description='\n                    Reserve this object or part, preventing other Sims from\n                    also Reserving it. Sims may still be added to its use list.\n                    '), all=TunableBasicReserveObjectAndParts(description='\n                    Reserve this object and all its parts, preventing other\n                    Sims from also Reserving them. Sims may still be added to\n                    their use lists.\n                    '), multi=TunableMultiReserveObject(description="\n                    Add the actor Sim to this object's use list, where the\n                    number of Sims allowed to be simultaneously in the use list\n                    is gated by the statistic specified. If no statistic is set\n                    any number of Sims can use the object at the same time.\n                    "), reserve_and_lock_all_parts=TunableReserveAndLockAll(description='\n                    Reserve this object and all its parts and prevent any other\n                    Sims from reserving them or adding themselves to their use\n                    lists.\n                    '), default='basic'), subject=TunableEnumFlags(description='\n                Who or what to reserve.\n                ', enum_type=ParticipantType, default=DEFAULT_RESERVATION_PARTICIPANT_TYPES), xevt=OptionalTunable(description='\n                When specified, the end of the reservation is associated to\n                this xevent.\n                ', tunable=Tunable(int, None)), description=description, **kwargs)

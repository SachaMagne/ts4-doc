import collections
from clock import ClockSpeedMode
from date_and_time import create_time_span
from element_utils import build_critical_section_with_finally
from event_testing.test_variants import CraftTaggedItemFactory
from interactions import ParticipantType, ParticipantTypeSingle
from interactions.interaction_finisher import FinishingType
from interactions.liability import Liability
from interactions.priority import Priority
from interactions.utils.balloon import TunableBalloon
from interactions.utils.interaction_elements import XevtTriggeredElement
from interactions.utils.notification import NotificationElement
from objects import ALL_HIDDEN_REASONS
from retail.retail_utils import RetailUtils
from sims4 import commands
from sims4.localization import TunableLocalizedStringFactory
from sims4.tuning.tunable import TunableList, TunableReference, TunableFactory, Tunable, TunableEnumEntry, TunableMapping, TunableTuple, TunableVariant, HasTunableFactory, TunableSimMinute, OptionalTunable, AutoFactoryInit, TunableInterval
from snippets import TunableAffordanceListReference
from statistics.statistic import Statistic
from statistics.statistic_ops import TunableStatisticChange
from ui.ui_dialog_notification import TunableUiDialogNotificationSnippet
import alarms
import clock
import services
import sims4.log
import sims4.resources
logger = sims4.log.Logger('Super Interactions')

class TunableAffordanceLinkList(TunableList):
    __qualname__ = 'TunableAffordanceLinkList'

    def __init__(self, class_restrictions=(), **kwargs):
        super().__init__(TunableReference(services.get_instance_manager(sims4.resources.Types.INTERACTION), checks=[('check_posture_compatability', 'asm')], category='asm', description='Linked Affordance', class_restrictions=class_restrictions, pack_safe=True), **kwargs)

class ContentSet(HasTunableFactory):
    __qualname__ = 'ContentSet'
    FACTORY_TUNABLES = {'description': ' \n           This is where you tune any sub actions of this interaction.\n           \n           The interactions here can be tuned as reference to individual\n           affordances, lists of affordances, or phase affordances.\n           \n           Sub actions are affordances that can be run anytime this \n           interaction is active. Autonomy will choose which interaction\n           runs.\n           \n           Using phase affordances you can also tune Quick Time or \n           optional affordances that can appear.\n           ', 'affordance_links': TunableAffordanceLinkList(class_restrictions=('MixerInteraction',)), 'affordance_lists': TunableList(TunableAffordanceListReference(pack_safe=True)), 'phase_affordances': TunableMapping(description='\n            A mapping of phase names to affordance links and affordance lists. \n                      \n            This is also where you can specify an affordance is Quick Time (or\n            an optional affordance) and how many steps are required before an\n            option affordance is made available.\n            ', value_type=TunableList(TunableTuple(affordance_links=TunableAffordanceLinkList(class_restrictions=('MixerInteraction',)), affordance_lists=TunableList(TunableAffordanceListReference(pack_safe=True))))), 'phase_tuning': OptionalTunable(TunableTuple(description='\n            When enabled, statistic will be added to target and is used to\n            determine the phase index to determine which affordance group to use\n            in the phase affordance.\n            ', turn_statistic=Statistic.TunableReference(description='\n                The statistic used to track turns during interaction.\n                Value will be reset to 0 at the start of each phase.\n                '), target=TunableEnumEntry(description='\n                The participant the affordance will target.\n                ', tunable_type=ParticipantType, default=ParticipantType.Actor)))}
    EMPTY_LINKS = None

    def __init__(self, affordance_links, affordance_lists, phase_affordances, phase_tuning):
        self._affordance_links = affordance_links
        self._affordance_lists = affordance_lists
        self.phase_tuning = phase_tuning
        self._phase_affordance_links = []
        for key in sorted(phase_affordances.keys()):
            self._phase_affordance_links.append(phase_affordances[key])

    def _get_all_affordance_for_phase_gen(self, phase_affordances):
        for affordance in phase_affordances.affordance_links:
            yield affordance
        for affordance_list in phase_affordances.affordance_lists:
            for affordance in affordance_list:
                yield affordance

    def all_affordances_gen(self, phase_index=None):
        if phase_index is not None and self._phase_affordance_links:
            phase_index = min(phase_index, len(self._phase_affordance_links) - 1)
            phase = self._phase_affordance_links[phase_index]
            for phase_affordances in phase:
                for affordance in self._get_all_affordance_for_phase_gen(phase_affordances):
                    yield affordance
        else:
            for phase in self._phase_affordance_links:
                for phase_affordances in phase:
                    for affordance in self._get_all_affordance_for_phase_gen(phase_affordances):
                        yield affordance
            for link in self._affordance_links:
                yield link
            for l in self._affordance_lists:
                for link in l:
                    yield link

    @property
    def num_phases(self):
        return len(self._phase_affordance_links)

    def has_affordances(self):
        return bool(self._affordance_links) or (bool(self._affordance_lists) or bool(self._phase_affordance_links))

ContentSet.EMPTY_LINKS = ContentSet((), (), {}, None)

class ContentSetWithOverrides(ContentSet):
    __qualname__ = 'ContentSetWithOverrides'
    FACTORY_TUNABLES = {'balloon_overrides': OptionalTunable(TunableList(description='\n            Balloon Overrides lets you override the mixer balloons.\n            EX: Each of the comedy routine performances have a set of balloons.\n            However, the animation/mixer content is the same. We want to play\n            the same mixer content, but just have the balloons be different.\n            ', tunable=TunableBalloon())), 'additional_mixers_to_cache': TunableInterval(description='\n            Additional number of mixers to cache during a subaction request.\n            For mixer autonomy, we cache mixer for performance reasons.  The\n            current number mixers cached can be found in\n            AutonomyComponent.MIXERS_TO_CACHE.\n            \n            An example for reason to add more mixers to cache if there are\n            large number of mixers tuned in this content set such as socials,\n            you may need to increase this number.  \n            \n            Please talk to GPE if you are about to add additional mixers.\n            ', tunable_type=int, minimum=0, default_lower=0, default_upper=0)}

    def __init__(self, balloon_overrides, additional_mixers_to_cache, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.balloon_overrides = balloon_overrides
        self.additional_mixers_to_cache = additional_mixers_to_cache

class TunableStatisticAdvertisements(TunableList):
    __qualname__ = 'TunableStatisticAdvertisements'

    def __init__(self, **kwargs):
        super().__init__(TunableStatisticChange(locked_args={'subject': ParticipantType.Actor, 'advertise': True}), **kwargs)

class TunableContinuation(TunableList):
    __qualname__ = 'TunableContinuation'
    TAGGED_ITEM = 0
    ITEM_DEFINITION = 1
    ITEM_TUNING_ID = 2

    def __init__(self, target_default=ParticipantType.Object, locked_args={}, carry_target_default=ParticipantType.Object, **kwargs):
        super().__init__(TunableTuple(affordance=TunableReference(services.affordance_manager(), description='The affordance to push as a continuation on the actor for this SI.'), si_affordance_override=TunableReference(description="\n                When the tuned affordance is a mixer for a different SI, use\n                this to specify the mixer's appropriate SI. This is useful for\n                pushing socials.\n                ", manager=services.affordance_manager(), allow_none=True), actor=TunableEnumEntry(ParticipantType, ParticipantType.Actor, description='The Sim on which the affordance is pushed.'), target=TunableEnumEntry(ParticipantType, target_default, description='The participant the affordance will target.'), carry_target=OptionalTunable(TunableEnumEntry(ParticipantType, carry_target_default, description='The participant the affordance will set as a carry target.')), inventory_carry_target=TunableVariant(description='\n                Item in inventory (of continuations actor) to use as carry target for continuation if carry target is None\n                ', object_with_tag=CraftTaggedItemFactory(locked_args={'check_type': TunableContinuation.TAGGED_ITEM}), object_with_definition=TunableTuple(definition=TunableReference(description='\n                        The exact object definition to look for inside inventory.\n                        ', manager=services.definition_manager()), locked_args={'check_type': TunableContinuation.ITEM_DEFINITION}), object_with_base_definition=TunableTuple(definition=TunableReference(description='\n                        The base definition to look for inside inventory. Objects\n                        that redirect (like counters) will match if base definition\n                        is the same.\n                        ', manager=services.definition_manager()), locked_args={'check_type': TunableContinuation.ITEM_TUNING_ID}), locked_args={'None': None}, default='None'), locked_args=locked_args), **kwargs)

class TimeoutLiability(Liability, HasTunableFactory):
    __qualname__ = 'TimeoutLiability'
    LIABILITY_TOKEN = 'TimeoutLiability'
    FACTORY_TUNABLES = {'description': 'Establish a timeout for this affordance. If it has not run when the timeout hits, cancel and push timeout_affordance, if set.', 'timeout': TunableSimMinute(4, minimum=0, description='The time, in Sim minutes, after which the interaction is canceled and time_toute affordance is pushed, if set.'), 'timeout_affordance': TunableReference(services.affordance_manager(), allow_none=True, description='The affordance to push when the timeout expires. Can be unset, in which case the interaction will just be canceled.')}

    def __init__(self, interaction, *, timeout, timeout_affordance):

        def on_alarm(*_, **__):
            if interaction.running:
                return
            if interaction.transition is not None and interaction.transition.running:
                return
            if timeout_affordance is not None:
                context = interaction.context.clone_for_continuation(interaction)
                interaction.sim.push_super_affordance(timeout_affordance, interaction.target, context)
            interaction.cancel(FinishingType.LIABILITY, cancel_reason_msg='Timeout after {} sim minutes.'.format(timeout))

        time_span = clock.interval_in_sim_minutes(timeout)
        self._handle = alarms.add_alarm(self, time_span, on_alarm)

    def release(self):
        alarms.cancel_alarm(self._handle)

class GameSpeedLiability(Liability, HasTunableFactory):
    __qualname__ = 'GameSpeedLiability'
    LIABILITY_TOKEN = 'GameSpeedLiability'
    FACTORY_TUNABLES = {'game_speed': TunableEnumEntry(description='\n            The speed to set the game. If Super Speed 3 is chosen, it will only\n            take effect if every Sim in the active household has also requested\n            Super Speed 3. When the interaction ends, the game speed will be\n            set back to whatever it was before the interaction ran. However,\n            when Super Speed 3 ends, the game will always go to Normal speed.\n            ', tunable_type=ClockSpeedMode, default=ClockSpeedMode.NORMAL)}
    _ss3_requests = collections.defaultdict(set)

    def __init__(self, interaction, *, game_speed):
        self.speed_request = None
        self.new_game_speed = game_speed

    def on_add(self, interaction):
        self.interaction = interaction

    def on_run(self):
        if clock.GameClock.ignore_game_speed_requests:
            return
        sim = self.interaction.sim
        if self.new_game_speed == ClockSpeedMode.SUPER_SPEED3:
            self._ss3_requests[sim].add(self)

            def validity_check():
                retail_manager = RetailUtils.get_retail_manager_for_zone()
                if retail_manager is not None and retail_manager.is_open:
                    return False
                current_venue = services.get_current_venue()
                if current_venue is not None and not current_venue.allow_super_speed_three:
                    return False
                sims = services.active_household().instanced_sims_gen(allow_hidden_flags=ALL_HIDDEN_REASONS)
                return all(sim in self._ss3_requests for sim in sims)

            self.interaction.register_on_cancelled_callback(self._remove_liability)
        else:
            validity_check = None
        self.speed_request = services.game_clock_service().push_speed(self.new_game_speed, validity_check=validity_check, reason='SS3 interaction')

    def release(self):
        clock_service = services.game_clock_service()
        old_speed = clock_service.clock_speed
        sim = self.interaction.sim
        self._ss3_requests[sim].discard(self)
        if not self._ss3_requests[sim]:
            del self._ss3_requests[sim]
        if self.speed_request is not None:
            clock_service.remove_request(self.speed_request)
        if old_speed == ClockSpeedMode.SUPER_SPEED3 and clock_service.clock_speed != ClockSpeedMode.SUPER_SPEED3:
            clock_service.set_clock_speed(ClockSpeedMode.NORMAL, reason='Exited SS3 interaction')

    def merge(self, new_liability):
        if new_liability.interaction.sim is not self.interaction.sim:
            raise ValueError("Attempt to merge two different Sims' GameSpeedLiabilities.")
        if new_liability.__class__ != self.__class__:
            raise TypeError('Attempt to merge liabilities of different types.')
        self.new_game_speed = new_liability.new_game_speed
        return self

    def should_transfer(self, continuation):
        return False

    def _remove_liability(self, interaction):
        interaction.remove_liability(GameSpeedLiability.LIABILITY_TOKEN)

class CriticalPriorityLiability(Liability, HasTunableFactory, AutoFactoryInit):
    __qualname__ = 'CriticalPriorityLiability'
    LIABILITY_TOKEN = 'CriticalPriorityLiability'

    def __init__(self, interaction, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def on_add(self, interaction):
        interaction.priority = Priority.Critical

    def transfer(self, interaction):
        interaction.priority = Priority.Critical

class SaveLockLiability(Liability, HasTunableFactory):
    __qualname__ = 'SaveLockLiability'
    LIABILITY_TOKEN = 'SaveLockLiability'
    FACTORY_TUNABLES = {'description': '\n            Prevent the user from saving or traveling while this interaction is\n            in the queue or running.\n            ', 'save_lock_tooltip': TunableLocalizedStringFactory(description='\n                The tooltip/message to show when the player tries to save the\n                game or return to the neighborhood view while the interaction\n                is running or in the queue.\n                '), 'should_transfer': Tunable(description='\n                If this liability should transfer to continuations.\n                ', tunable_type=bool, default=True)}

    def __init__(self, interaction, *, save_lock_tooltip, should_transfer):
        self._save_lock_tooltip = save_lock_tooltip
        self._should_transfer = should_transfer
        self._interaction = interaction
        self._is_save_locked = False

    def on_add(self, interaction):
        self._interaction = interaction
        if not self._is_save_locked:
            services.get_persistence_service().lock_save(self)
            self._is_save_locked = True

    def should_transfer(self, continuation):
        return self._should_transfer

    def release(self):
        services.get_persistence_service().unlock_save(self)

    def get_lock_save_reason(self):
        return self._interaction.create_localized_string(self._save_lock_tooltip)

class PushAffordanceOnRouteFailLiability(Liability, HasTunableFactory, AutoFactoryInit):
    __qualname__ = 'PushAffordanceOnRouteFailLiability'
    LIABILITY_TOKEN = 'PushAffordanceOnRouteFailLiability'
    FACTORY_TUNABLES = {'actor': TunableEnumEntry(description='\n            The participant of this interaction that is going to have\n            the specified affordance pushed upon them.\n            ', tunable_type=ParticipantType, default=ParticipantType.Actor), 'target': OptionalTunable(description="\n            If enabled, specify a participant to be used as the\n            interaction's target.\n            ", tunable=TunableEnumEntry(description="\n                The participant to be used as the interaction's\n                target.\n                ", tunable_type=ParticipantType, default=ParticipantType.Object), enabled_by_default=True), 'carry_target': OptionalTunable(description="\n            If enabled, specify a participant to be used as the\n            interaction's carry target.\n            If disabled carry_target will be set to None.\n            ", tunable=TunableEnumEntry(description="\n                The participant to be used as the interaction's\n                carry target.\n                ", tunable_type=ParticipantType, default=ParticipantType.Object), disabled_name='No_carry_target'), 'affordance': TunableReference(description='\n            When this interaction is cancelled because of route fail, this\n            interaction will be pushed.\n            ', manager=services.get_instance_manager(sims4.resources.Types.INTERACTION), class_restrictions=('SuperInteraction',))}

    def __init__(self, interaction, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._interaction = interaction

    def on_add(self, interaction):
        self._interaction = interaction

    def transfer(self, interaction):
        self._interaction = interaction

    def release(self):
        if self._interaction.finishing_type == FinishingType.TRANSITION_FAILURE:
            self._push_affordance()
        super().release()

    def _push_affordance(self):
        affordance_target = self._interaction.get_participant(self.target) if self.target is not None else None
        for actor in self._interaction.get_participants(self.actor):
            if actor is self._interaction.sim:
                context = self._interaction.context.clone_for_concurrent_context()
            else:
                context = self._interaction.context.clone_for_sim(actor)
            if self.carry_target is not None:
                context.carry_target = self._interaction.get_participants(self.carry_target)
            else:
                context.carry_target = None
            for aop in self.affordance.potential_interactions(affordance_target, context):
                aop.test_and_execute(context)

def set_sim_sleeping(interaction, sequence=None):
    sim = interaction.sim

    def set_sleeping(_):
        sim.sleeping = True

    def set_awake(_):
        sim.sleeping = False

    return build_critical_section_with_finally(set_sleeping, sequence, set_awake)

class TunableSetSimSleeping(TunableFactory):
    __qualname__ = 'TunableSetSimSleeping'
    FACTORY_TYPE = staticmethod(set_sim_sleeping)

class TunableSetClockSpeed(XevtTriggeredElement):
    __qualname__ = 'TunableSetClockSpeed'
    FACTORY_TUNABLES = {'description': 'Change the game clock speed as part of an interaction.', 'game_speed': TunableVariant(description='\n            The speed to set for the game. If you want to super speed 3, use a\n            GameSpeedLiability so that super speed 3 will be tied to the\n            lifetime of an interaction.\n            ', locked_args={'PAUSED': ClockSpeedMode.PAUSED, 'NORMAL': ClockSpeedMode.NORMAL, 'SPEED2': ClockSpeedMode.SPEED2, 'SPEED3': ClockSpeedMode.SPEED3}, default='NORMAL')}

    def _do_behavior(self):
        if clock.GameClock.ignore_game_speed_requests:
            return
        services.game_clock_service().set_clock_speed(self.game_speed)

class ServiceNpcRequest(XevtTriggeredElement):
    __qualname__ = 'ServiceNpcRequest'
    MINUTES_ADD_TO_SERVICE_ARRIVAL = 5
    HIRE = 1
    CANCEL = 2
    FACTORY_TUNABLES = {'description': '\n        Request a service NPC as part of an interaction. Note for timing field:\n        Only beginning and end will work because xevents will trigger\n        immediately on the server for service requests\n        ', 'request_type': TunableVariant(description='\n                Specify the type of service NPC Request. You can hire, dismiss,\n                fire, or cancel a service npc.', hire=TunableTuple(description='\n                A reference to the tuned service npc instance that will be\n                requested at the specified time.', locked_args={'request_type': HIRE}, service=TunableReference(services.service_npc_manager())), cancel=TunableTuple(locked_args={'request_type': CANCEL}, service=TunableReference(services.service_npc_manager()), description='A reference to the tuned service that will be cancelled. This only really applies to recurring services where a cancelled service will never have any service npcs show up again until re-requested.'), default='hire'), 'notification': OptionalTunable(description='\n                When enabled, display a notification when the service npc is \n                successfully hired/cancelled.\n                If hired, last token is DateAndTime when service npc will\n                arrive. (usually this is 1)\n                ', tunable=NotificationElement.TunableFactory(locked_args={'timing': XevtTriggeredElement.LOCKED_AT_BEGINNING}))}

    def __init__(self, interaction, *args, request_type, notification, sequence=(), **kwargs):
        super().__init__(interaction, request_type=request_type, notification=notification, sequence=sequence, *args, **kwargs)
        self._request_type = request_type
        self.notification = notification
        self._household = interaction.sim.household
        self._service_npc_user_specified_data_id = None
        self._recurring = False
        self._read_interaction_parameters(**interaction.interaction_parameters)

    def _read_interaction_parameters(self, service_npc_user_specified_data_id=None, service_npc_recurring_request=False, **kwargs):
        self._service_npc_user_specified_data_id = service_npc_user_specified_data_id
        self._recurring = service_npc_recurring_request

    def _do_behavior(self):
        request_type = self._request_type.request_type
        service_npc = self._request_type.service
        if service_npc is None:
            return
        service_npc_service = services.current_zone().service_npc_service
        if request_type == self.HIRE:
            finishing_time = service_npc_service.request_service(self._household, service_npc, user_specified_data_id=self._service_npc_user_specified_data_id, is_recurring=self._recurring)
            if self.notification is not None and finishing_time is not None:
                finishing_time = finishing_time + create_time_span(minutes=self.MINUTES_ADD_TO_SERVICE_ARRIVAL)
                notification_element = self.notification(self.interaction)
                notification_element.show_notification(additional_tokens=(finishing_time,))
        elif request_type == self.CANCEL:
            service_npc_service.cancel_service(self._household, service_npc)
            if self.notification is not None:
                notification_element = self.notification(self.interaction)
                notification_element._do_behavior()

class DoCommand(XevtTriggeredElement, HasTunableFactory):
    __qualname__ = 'DoCommand'
    ARG_TYPE_PARTICIPANT = 0
    ARG_TYPE_STRING = 1
    ARG_TYPE_NUMBER = 2
    FACTORY_TUNABLES = {'command': Tunable(description='\n            The command to run.\n            ', tunable_type=str, default=None), 'arguments': TunableList(description="\n            The arguments for this command. Arguments will be added after the\n            command in the order they're listed here.\n            ", tunable=TunableVariant(description='\n                The argument to use. In most cases, the ID of the participant\n                will be used.\n                ', participant=TunableTuple(description='\n                    An argument that is a participant in the interaction. The\n                    ID will be used as the argument for the command.\n                    ', argument=TunableEnumEntry(description='\n                        The participant argument. The ID will be used in the\n                        command.\n                        ', tunable_type=ParticipantTypeSingle, default=ParticipantTypeSingle.Object), locked_args={'arg_type': ARG_TYPE_PARTICIPANT}), string=TunableTuple(description="\n                    An argument that's a string.\n                    ", argument=Tunable(description='\n                        The string argument.\n                        ', tunable_type=str, default=None), locked_args={'arg_type': ARG_TYPE_STRING}), number=TunableTuple(description='\n                    An argument that is a number. This can be a float or an int.\n                    ', argument=Tunable(description='\n                        The number argument.\n                        ', tunable_type=float, default=0), locked_args={'arg_type': ARG_TYPE_NUMBER})))}

    def _do_behavior(self):
        full_command = self.command
        for arg in self.arguments:
            if arg.arg_type == self.ARG_TYPE_PARTICIPANT:
                participant = self.interaction.get_participant(arg.argument)
                if participant is None:
                    logger.error('Trying to run the Do Command element with an invalid participant, {}.', arg.argument, owner='trevor')
                    return False
                full_command += ' {}'.format(participant.id)
            elif arg.arg_type == self.ARG_TYPE_STRING or arg.arg_type == self.ARG_TYPE_NUMBER:
                full_command += ' {}'.format(arg.argument)
            else:
                logger.error('Trying to run the Do Command element with an invalid arg type, {}.', arg.arg_type, owner='trevor')
                return False
        client_id = self.interaction.context.client.id if self.interaction.context.client is not None else None
        commands.execute(full_command, client_id)
        return True

class SetGoodbyeNotificationElement(XevtTriggeredElement):
    __qualname__ = 'SetGoodbyeNotificationElement'
    NEVER_USE_NOTIFICATION_NO_MATTER_WHAT = 'never_use_notification_no_matter_what'
    FACTORY_TUNABLES = {'description': 'Set the notification that a Sim will display when they leave.', 'participant': TunableEnumEntry(description='\n            The participant of the interaction who will have their "goodbye"\n            notification set.\n            ', tunable_type=ParticipantType, default=ParticipantType.Actor), 'goodbye_notification': TunableVariant(description='\n                The "goodbye" notification that will be set on this Sim. This\n                notification will be displayed when this Sim leaves the lot\n                (unless it gets overridden later).\n                ', notification=TunableUiDialogNotificationSnippet(), locked_args={'no_notification': None, 'never_use_notification_no_matter_what': NEVER_USE_NOTIFICATION_NO_MATTER_WHAT}, default='no_notification'), 'only_set_if_notification_already_set': Tunable(description="\n                If the Sim doesn't have a goodbye notification already set and\n                this checkbox is checked, leave the goodbye notification unset.\n                ", tunable_type=bool, default=True)}

    def _do_behavior(self):
        participants = self.interaction.get_participants(self.participant)
        for participant in participants:
            if participant.sim_info.goodbye_notification == self.NEVER_USE_NOTIFICATION_NO_MATTER_WHAT:
                pass
            if participant.sim_info.goodbye_notification is None and self.only_set_if_notification_already_set:
                pass
            participant.sim_info.goodbye_notification = self.goodbye_notification

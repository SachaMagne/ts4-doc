from protocolbuffers import SimObjectAttributes_pb2 as persistence_protocols
from objects import PaintingState
from objects.components import Component, componentmethod_with_fallback
from sims4.tuning.dynamic_enum import DynamicEnumFlags
from sims4.tuning.tunable import HasTunableFactory, TunableEnumFlags
import distributor.fields
import distributor.ops
import objects.components.types
import sims4

class CanvasType(DynamicEnumFlags):
    __qualname__ = 'CanvasType'
    NONE = 0

logger = sims4.log.Logger('Canvas')

class CanvasComponent(Component, HasTunableFactory, component_name=objects.components.types.CANVAS_COMPONENT, persistence_key=persistence_protocols.PersistenceMaster.PersistableData.CanvasComponent):
    __qualname__ = 'CanvasComponent'
    FACTORY_TUNABLES = {'canvas_types': TunableEnumFlags(CanvasType, CanvasType.NONE, description="\n            A painting texture must support at least one of these canvas types\n            to be applied to this object's painting area.\n            ")}

    def __init__(self, owner, *, canvas_types):
        super().__init__(owner)
        self.canvas_types = canvas_types
        self.time_stamp = None
        self._painting_state = None

    def save(self, persistence_master_message):
        if self.painting_state is not None:
            persistable_data = persistence_protocols.PersistenceMaster.PersistableData()
            persistable_data.type = persistence_protocols.PersistenceMaster.PersistableData.CanvasComponent
            canvas_data = persistable_data.Extensions[persistence_protocols.PersistableCanvasComponent.persistable_data]
            canvas_data.texture_id = self.painting_state.texture_id
            canvas_data.reveal_level = self.painting_state.reveal_level
            canvas_data.effect = self.painting_state.effect
            if self.time_stamp:
                canvas_data.time_stamp = self.time_stamp
            persistence_master_message.data.extend([persistable_data])
        else:
            logger.error('Object {} has no painting_state during save of the canvas component.', self.owner, owner='jwilkinson')

    def load(self, persistable_data):
        canvas_data = persistable_data.Extensions[persistence_protocols.PersistableCanvasComponent.persistable_data]
        if canvas_data.time_stamp:
            self.time_stamp = canvas_data.time_stamp
        self.painting_state = PaintingState(canvas_data.texture_id, canvas_data.reveal_level, False, canvas_data.effect)

    @distributor.fields.ComponentField(op=distributor.ops.SetPaintingState, default=None)
    def painting_state(self) -> PaintingState:
        return self._painting_state

    @painting_state.setter
    def painting_state(self, value):
        self._painting_state = value

    @property
    def painting_reveal_level(self) -> int:
        if self.painting_state is not None:
            return self.painting_state.reveal_level

    @painting_reveal_level.setter
    def painting_reveal_level(self, reveal_level):
        if self.painting_state is not None:
            self.painting_state = self.painting_state.get_at_level(reveal_level)

    @property
    def painting_effect(self) -> int:
        if self.painting_state is not None:
            return self.painting_state.effect

    @painting_effect.setter
    def painting_effect(self, effect):
        if self.painting_state is not None:
            self.painting_state = self.painting_state.get_with_effect(effect)

    def set_painting_texture_id(self, texture_id):
        if self.painting_state is not None:
            self.painting_state = self.painting_state.set_painting_texture_id(texture_id)
        else:
            logger.error('Object {} has no painting state and its trying to set a custom texture', self.owner, owner='camilogarcia')

    @componentmethod_with_fallback(lambda msg: None)
    def populate_icon_canvas_texture_info(self, msg):
        if self.painting_state is not None and msg is not None:
            msg.texture_id = self.painting_state.texture_id
            msg.texture_effect = self.painting_state.effect

    @componentmethod_with_fallback(lambda *_, **__: None)
    def get_canvas_texture_id(self):
        if self.painting_state is not None:
            return self.painting_state.texture_id

    @componentmethod_with_fallback(lambda *_, **__: None)
    def get_canvas_texture_effect(self):
        return self.painting_effect

from build_buy import ObjectOriginLocation
from event_testing.test_events import TestEvent
from event_testing.tests import TunableTestSet
from interactions import ParticipantType
from interactions.utils.interaction_elements import XevtTriggeredElement
from objects.components.inventory_enums import InventoryType
from sims4.tuning.tunable import HasTunableFactory, AutoFactoryInit, TunableVariant, TunableEnumEntry, TunableReference, TunableRange
import build_buy
import event_testing
import services
import sims4.log
logger = sims4.log.Logger('Inventory', default_owner='tingyul')

def transfer_entire_inventory(source, recipient):
    if source is None or recipient is None:
        raise ValueError('Attempt to transfer items from {} to {}.'.format(source, recipient))
    lot = services.active_lot()
    if isinstance(source, InventoryType):
        source_inventories = lot.get_object_inventories(source)
    else:
        source_inventories = (source.inventory_component,)
    if len(source_inventories) == 0 or None in source_inventories:
        raise ValueError('Failed to find inventory component for source of inventory transfer: {}'.format(source))
    recipient_is_inventory_type = isinstance(recipient, InventoryType)
    if recipient_is_inventory_type:
        recipient_inventories = lot.get_object_inventories(recipient)
        if len(recipient_inventories) > 1:
            raise ValueError('Attempt to transfer into inventory type with multiple nonshared inventories: {}'.format(recipient))
        recipient_inventory = recipient_inventories[0]
    else:
        recipient_inventory = recipient.inventory_component
    if recipient_inventory is None:
        raise ValueError('Attempt to transfer items to an object that has no inventory component: {}'.format(recipient))
    for source_inventory in source_inventories:
        for obj in list(source_inventory):
            if not source_inventory.try_remove_object_by_id(obj.id, count=obj.stack_count()):
                logger.warn('Failed to remove object {} from {} inventory', obj, source)
            if recipient_inventory.can_add(obj):
                if not recipient_is_inventory_type and recipient.is_sim:
                    obj.update_ownership(recipient)
                recipient_inventory.system_add_object(obj, None if recipient_is_inventory_type else recipient)
            else:
                obj.set_household_owner_id(services.active_household_id())
                build_buy.move_object_to_household_inventory(obj, object_location_type=ObjectOriginLocation.SIM_INVENTORY)

class InventoryTransfer(XevtTriggeredElement, HasTunableFactory, AutoFactoryInit):
    __qualname__ = 'InventoryTransfer'

    @staticmethod
    def _verify_tunable_callback(*args, **kwargs):
        if kwargs['source'] == InventoryType.UNDEFINED:
            logger.error('Inventory Transfer has an undefined inventory type as its source.', owner='bhill')
        if kwargs['recipient'] == InventoryType.UNDEFINED:
            logger.error('Inventory Transfer has an undefined inventory type as its recipient.', owner='bhill')

    FACTORY_TUNABLES = {'description': '\n            Transfer all objects with a specified inventory type from the\n            specified inventory to the inventory of a specified participant.\n            ', 'source': TunableVariant(description='\n            The source of the inventory objects being transferred.\n            ', lot_inventory_type=TunableEnumEntry(description='\n                The inventory from which the objects will be transferred.\n                ', tunable_type=InventoryType, default=InventoryType.UNDEFINED), participant=TunableEnumEntry(description='\n                The participant of the interaction whose inventory objects will\n                be transferred to the specified inventory.\n                ', tunable_type=ParticipantType, default=ParticipantType.Object)), 'recipient': TunableVariant(description='\n            The inventory that will receive the objects being transferred.\n            ', lot_inventory_type=TunableEnumEntry(description='\n                The inventory into which the objects will be transferred.\n                ', tunable_type=InventoryType, default=InventoryType.UNDEFINED), participant=TunableEnumEntry(description='\n                The participant of the interaction who will receive the objects \n                being transferred.\n                ', tunable_type=ParticipantType, default=ParticipantType.Object)), 'verify_tunable_callback': _verify_tunable_callback}

    def _do_behavior(self):
        if isinstance(self.source, ParticipantType):
            source = self.interaction.get_participant(self.source)
        else:
            source = self.source
        if isinstance(self.recipient, ParticipantType):
            recipient = self.interaction.get_participant(self.recipient)
        else:
            recipient = self.recipient
        transfer_entire_inventory(source, recipient)

class InventoryTransferFakePerform(HasTunableFactory, AutoFactoryInit):
    __qualname__ = 'InventoryTransferFakePerform'
    FACTORY_TUNABLES = {'description': '\n            Transfer all objects with a specified inventory type from the\n            specified inventory to the inventory of a specified participant.\n            ', 'source': TunableEnumEntry(description='\n            The inventory from which the objects will be transferred.\n            ', tunable_type=InventoryType, default=InventoryType.UNDEFINED), 'recipient': TunableEnumEntry(description='\n            The inventory into which the objects will be transferred.\n            ', tunable_type=InventoryType, default=InventoryType.UNDEFINED)}

    def _do_behavior(self):
        transfer_entire_inventory(self.source, self.recipient)

class PutObjectInMail(XevtTriggeredElement, HasTunableFactory, AutoFactoryInit):
    __qualname__ = 'PutObjectInMail'
    FACTORY_TUNABLES = {'description': '\n            Create an object of the specified type and place it in the hidden\n            inventory of the active lot so that it will be delivered along with\n            the mail.\n            ', 'object_to_be_mailed': TunableReference(description='\n            A reference to the type of object which will be sent to the hidden\n            inventory to be mailed.\n            ', manager=services.definition_manager())}

    def _do_behavior(self):
        lot = services.active_lot()
        if lot is None:
            return
        lot.create_object_in_hidden_inventory(self.object_to_be_mailed.id)

class DeliverBill(XevtTriggeredElement, HasTunableFactory, AutoFactoryInit):
    __qualname__ = 'DeliverBill'
    FACTORY_TUNABLES = {'description': '\n            Let the bills manager know that a bill has been delivered and\n            trigger appropriate bill-specific functionality.\n            '}

    def _do_behavior(self):
        household = services.owning_household_of_active_lot()
        if household is None:
            return
        if not household.bills_manager.can_deliver_bill:
            return
        household.bills_manager.trigger_bill_notifications_from_delivery()
        services.get_event_manager().process_events_for_household(TestEvent.BillsDelivered, household)

class DeliverBillFakePerform(HasTunableFactory, AutoFactoryInit):
    __qualname__ = 'DeliverBillFakePerform'
    FACTORY_TUNABLES = {'description': '\n            Let the bills manager know that a bill has been delivered and\n            trigger appropriate bill-specific functionality.\n            '}

    def _do_behavior(self):
        household = services.owning_household_of_active_lot()
        if household is None:
            return
        if not household.bills_manager.can_deliver_bill:
            return
        household.bills_manager.trigger_bill_notifications_from_delivery()
        services.get_event_manager().process_events_for_household(TestEvent.BillsDelivered, household)

class DestroySpecifiedObjectsFromTargetInventory(XevtTriggeredElement, HasTunableFactory, AutoFactoryInit):
    __qualname__ = 'DestroySpecifiedObjectsFromTargetInventory'
    ALL = 'ALL'
    FACTORY_TUNABLES = {'description': '\n            Destroy every object in the target inventory that passes the tuned\n            tests.\n            ', 'inventory_owner': TunableEnumEntry(description='\n            The participant of the interaction whose inventory will be checked\n            for objects to destroy.\n            ', tunable_type=ParticipantType, default=ParticipantType.Object), 'object_tests': TunableTestSet(description='\n            A list of tests to apply to all objects in the target inventory.\n            Every object that passes these tests will be destroyed.\n            '), 'count': TunableVariant(description="\n            The max number of objects to destroy. For example: A Sim has 2\n            red guitars and 1 blue guitar, and we're destroying guitars with\n            count = 2. Possible destroyed objects are: 2 red guitars, or 1 red\n            guitar and 1 blue guitar.\n            ", number=TunableRange(tunable_type=int, default=1, minimum=0), locked_args={'all': ALL}, default='all')}

    def _do_behavior(self):
        participant = self.interaction.get_participant(self.inventory_owner)
        inventory = participant.inventory_component
        if inventory is None:
            logger.error('Participant {} does not have an inventory to check for objects to destroy.', participant, owner='tastle')
            return
        objects_to_destroy = set()
        for obj in inventory:
            single_object_resolver = event_testing.resolver.SingleObjectResolver(obj)
            if not self.object_tests.run_tests(single_object_resolver):
                pass
            objects_to_destroy.add(obj)
        num_destroyed = 0
        for obj in objects_to_destroy:
            if self.count == self.ALL:
                count = obj.stack_count()
            else:
                count = min(self.count - num_destroyed, obj.stack_count())
            if not inventory.try_destroy_object(obj, count=count, source=inventory, cause='Destroying specified objects from target inventory extra.'):
                logger.error('Error trying to destroy object {}.', obj, owner='tastle')
            num_destroyed += count
            while self.count != self.ALL and num_destroyed >= self.count:
                break
        objects_to_destroy.clear()

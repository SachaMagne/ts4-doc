from sims4.tuning.dynamic_enum import DynamicEnum
from sims4.tuning.tunable import TunableMapping, TunableTuple, Tunable, TunableRange, OptionalTunable
from statistics.tunable import CommodityDecayModifierMapping
import enum
import sims4.log
logger = sims4.log.Logger('Inventory Enums')

class InventoryType(DynamicEnum):
    __qualname__ = 'InventoryType'
    UNDEFINED = 0
    SIM = 1
    HIDDEN = 2
    FISHBOWL = 3
    MAILBOX = 4

    def get_tuning(self):
        return InventoryTypeTuning.INVENTORY_TYPE_DATA.get(self)

    @property
    def is_shared_between_objects(self):
        tuning = self.get_tuning()
        return tuning is None or tuning.shared_between_objects

    @property
    def put_away_allowed(self):
        tuning = self.get_tuning()
        return tuning is None or tuning.put_away_allowed

    @property
    def max_inventory_size(self):
        tuning = self.get_tuning()
        if tuning is None:
            return sims4.math.MAX_UINT32
        return tuning.max_inventory_size

class StackScheme(enum.Int):
    __qualname__ = 'StackScheme'
    NONE = Ellipsis
    VARIANT_GROUP = Ellipsis
    DEFINITION = Ellipsis

class InventoryTypeTuning:
    __qualname__ = 'InventoryTypeTuning'
    INVENTORY_TYPE_DATA = TunableMapping(description='\n        A mapping of Inventory Type to any static information required by the\n        client to display inventory data as well information about allowances\n        for each InventoryType.\n        ', key_type=InventoryType, value_type=TunableTuple(description='\n            Any information required by the client to display inventory data.\n            ', skip_carry_pose_allowed=Tunable(description='\n                If checked, an object tuned to be put away in this inventory\n                type will be allowed to skip the carry pose.  If unchecked, it\n                will not be allowed to skip the carry pose.\n                ', tunable_type=bool, default=False), put_away_allowed=Tunable(description='\n                If checked, objects can be manually "put away" in this\n                inventory type. If unchecked, objects cannot be manually "put\n                away" in this inventory type.\n                ', tunable_type=bool, default=True), shared_between_objects=Tunable(description='\n                If checked, this inventory will be shared between all objects\n                that have it. For example, if you put an item in one fridge,\n                you would be able to remove it from a different fridge on the\n                lot.', tunable_type=bool, default=True), max_inventory_size=OptionalTunable(tunable=TunableRange(description='\n                    Max number of items inventory type can have\n                    ', tunable_type=int, default=sims4.math.MAX_INT32, minimum=1, maximum=sims4.math.MAX_INT32), disabled_name='default_size', disabled_value=sims4.math.MAX_UINT32, enabled_name='fixed_size')))
    GAMEPLAY_MODIFIERS = TunableMapping(description="\n        A mapping of Inventory Type to the gameplay effects they provide. If an\n        inventory does not affect contained objects, it is fine to leave that\n        inventory's type out of this mapping.\n        ", key_type=InventoryType, value_type=TunableTuple(description='\n            Gameplay modifiers.\n            ', decay_modifiers=CommodityDecayModifierMapping(description='\n                Multiply the decay rate of specific commodities by a tunable\n                integer in order to speed up or slow down decay while the\n                object is contained within this inventory. This modifier will\n                be multiplied with other modifiers on the object, if it has\n                any.\n                ')))

    @classmethod
    def _verify_tuning_callback(cls):
        for inventory_type in set(InventoryType) - set(cls.INVENTORY_TYPE_DATA.keys()):
            logger.error('Inventory type {} has no tuned inventory type data. This can be fixed in the tuning for objects.components.inventory_enum.tuning -> InventoryTypeTuning -> Inventory Type Data.', inventory_type.name, owner='bhill')

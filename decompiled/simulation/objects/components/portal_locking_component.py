from functools import total_ordering
from protocolbuffers import SimObjectAttributes_pb2
from event_testing.resolver import SingleSimResolver
from event_testing.test_variants import SituationJobTest, TunableSimInfoTest
from objects.components import Component, componentmethod, componentmethod_with_fallback
from objects.components.portal_locking_enums import LockPriority, LockSide, LockType
from objects.components.types import PORTAL_LOCKING_COMPONENT
from retail.retail_utils import RetailUtils
from sims4.tuning.tunable import HasTunableFactory, AutoFactoryInit, TunableList, TunableVariant, TunableEnumEntry, Tunable
import services
import sims4
logger = sims4.log.Logger('PortalLockComponent', default_owner='cjiang')

@total_ordering
class LockResult:
    __qualname__ = 'LockResult'
    UNLOCKED = None

    def __init__(self, is_locked, lock_type=None, lock_priority=None, lock_sides=LockSide.LOCK_BOTH, lock_reason=None):
        self.is_locked = is_locked
        self.lock_priority = lock_priority
        self.lock_type = lock_type
        self.lock_sides = lock_sides
        self.lock_reason = lock_reason

    def __bool__(self):
        return self.is_locked

    def __lt__(self, other):
        return other.is_locked and not self.is_locked

    def __eq__(self, other):
        return self.is_locked == other.is_locked and (self.lock_priority == other.lock_priority and self.lock_sides == other.lock_sides)

    def __repr__(self):
        return '{}, LockType:{}, LockSides:{}'.format(self.is_locked, self.lock_type, self.lock_sides)

    def is_locking_both(self):
        return self.lock_sides == LockSide.LOCK_BOTH

    def is_player_lock(self):
        return self.lock_priority == LockPriority.PLAYER_LOCK

LockResult.NONE = LockResult(False)

class LockData(HasTunableFactory, AutoFactoryInit):
    __qualname__ = 'LockData'
    FACTORY_TUNABLES = {'lock_priority': TunableEnumEntry(description='\n            The priority of this lock data. Used in comparison between multiple\n            lock datas on the lock component test.\n            ', tunable_type=LockPriority, default=LockPriority.SYSTEM_LOCK), 'lock_sides': TunableEnumEntry(description='\n            Which side or both this lock data will lock.\n            ', tunable_type=LockSide, default=LockSide.LOCK_BOTH)}

    def __init__(self, lock_type=None, **kwargs):
        super().__init__(**kwargs)
        self.lock_type = lock_type

    def test_lock(self, sim):
        raise NotImplementedError

    def setup_data_by_sim(self, sim):
        pass

    def update(self, other_data):
        if self.lock_type != other_data.lock_type:
            logger.error('Attempting to update mismatched lock types. Current: {}. Request: {}', self.lock_type, other_data.lock_type)
            return
        self.lock_priority = max(self.lock_priority, other_data.lock_priority)

    def get_exception_data(self):
        return ''

    def save(self, save_data):
        save_data.priority = self.lock_priority
        save_data.sides = self.lock_sides

    def load(self, load_data):
        pass

class LockSimInfoData(LockData):
    __qualname__ = 'LockSimInfoData'
    FACTORY_TUNABLES = {'siminfo_test': TunableSimInfoTest(description='\n            The test to determine whether this sim can pass or not.\n            ')}

    def __init__(self, **kwargs):
        super().__init__(lock_type=LockType.LOCK_SIMINFO, **kwargs)

    def test_lock(self, sim):
        single_sim_resolver = SingleSimResolver(sim.sim_info)
        if single_sim_resolver(self.siminfo_test):
            return LockResult(False, 'siminfo_lock', self.lock_priority, self.lock_sides)
        return LockResult(True, 'siminfo_lock', self.lock_priority, self.lock_sides)

class LockAllWithSimIdExceptionData(LockData):
    __qualname__ = 'LockAllWithSimIdExceptionData'
    FACTORY_TUNABLES = {'except_actor': Tunable(description='\n            If we want this lock data to have this actor as exception sim.\n            ', tunable_type=bool, default=False), 'except_household': Tunable(description="\n            If we want this lock data to have actor's household as exception sims.\n            ", tunable_type=bool, default=False)}

    def __init__(self, **kwargs):
        super().__init__(lock_type=LockType.LOCK_ALL_WITH_SIMID_EXCEPTION, **kwargs)
        self.except_sim_ids = set()

    def __repr__(self):
        return 'Except sims {}'.format(self.except_sim_ids)

    def setup_data_by_sim(self, sim):
        if self.except_actor and sim.id not in self.except_sim_ids:
            self.except_sim_ids.add(sim.id)

    def remove_except_sim(self, sim):
        if sim.id in self._except_sim_ids:
            self.except_sim_ids.remove(sim.id)

    def update(self, other_data):
        super().update(other_data)
        self.except_sim_ids.update(other_data.except_sim_ids)

    def test_lock(self, sim):
        if self.except_household and services.active_lot().get_household() is sim.household:
            return LockResult(False, 'all_lock', self.lock_priority, self.lock_sides)
        if self.except_sim_ids and sim.id in self.except_sim_ids:
            return LockResult(False, 'all_lock', self.lock_priority, self.lock_sides)
        return LockResult(True, 'all_lock', self.lock_priority, self.lock_sides)

    def get_exception_data(self):
        except_sim_names = []
        sim_info_mgr = services.sim_info_manager()
        for sim_id in self.except_sim_ids:
            sim_info = sim_info_mgr.get(sim_id)
            if sim_info is not None:
                except_sim_names.append(sim_info.full_name)
            else:
                except_sim_names.append(str(sim_id))
        return ','.join(except_sim_names)

    def save(self, save_data):
        super().save(save_data)
        save_data.except_actor = self.except_actor
        save_data.except_household = self.except_household
        if self.except_sim_ids:
            save_data.exception_sim_ids.extend(self.except_sim_ids)

    def load(self, load_data):
        super().load(load_data)
        for sim_id in load_data.exception_sim_ids:
            self.except_sim_ids.add(sim_id)

class LockAllWithSituationJobExceptionData(LockData):
    __qualname__ = 'LockAllWithSituationJobExceptionData'
    from event_testing.test_variants import TunableSituationJobTest
    FACTORY_TUNABLES = {'situation_job_test': TunableSituationJobTest(description='\n            The test to determine whether this sim can pass or not.\n            '), 'except_retail_employee': Tunable(description='\n            If true, the retail store employee will have exception to the door.\n            ', tunable_type=bool, default=False)}

    def __init__(self, **kwargs):
        super().__init__(lock_type=LockType.LOCK_ALL_WITH_SITUATION_JOB_EXCEPTION, **kwargs)

    def __repr__(self):
        return 'Except SituationJobs:{}, RoleTags:{}, Except retail employee: {}'.format(self.situation_job_test.situation_jobs, self.situation_job_test.role_tags, self.except_retail_employee)

    def test_lock(self, sim):
        single_sim_resolver = SingleSimResolver(sim.sim_info)
        if single_sim_resolver(self.situation_job_test):
            return LockResult(False, 'situation_job_lock', self.lock_priority, self.lock_sides)
        if self.except_retail_employee:
            retail_manager = RetailUtils.get_retail_manager_for_zone()
            if retail_manager is not None and retail_manager.is_employee(sim.sim_info):
                return LockResult(False, 'situation_job_lock', self.lock_priority, self.lock_sides)
        return LockResult(True, 'situation_job_lock', self.lock_priority, self.lock_sides)

    def get_exception_data(self):
        return 'SituationJobs:{}, RoleTags:{}, Except retail employee: {}'.format(self.situation_job_test.situation_jobs, self.situation_job_test.role_tags, self.except_retail_employee)

    def save(self, save_data):
        super().save(save_data)
        situation_job_test = self.situation_job_test
        save_data.participant_enum = situation_job_test.participant
        save_data.negate = situation_job_test.negate
        if situation_job_test.situation_jobs:
            job_list = [job.guid64 for job in situation_job_test.situation_jobs]
            save_data.situation_jobs.extend(job_list)
        if situation_job_test.role_tags:
            save_data.role_tags.extend(situation_job_test.role_tags)
        save_data.except_retail_employee = self.except_retail_employee

    def load(self, load_data):
        super().load(load_data)
        situation_jobs = set()
        situation_job_manager = services.situation_job_manager()
        if load_data.situation_jobs:
            for job_id in load_data.situation_jobs:
                job_type = situation_job_manager.get(job_id)
                while job_type is not None:
                    situation_jobs.add(job_type)
        role_tags = frozenset(load_data.role_tags)
        self.situation_job_test = SituationJobTest(participant=load_data.participant_enum, negate=load_data.negate, situation_jobs=frozenset(situation_jobs), role_tags=role_tags)

class PortalLockingComponent(Component, HasTunableFactory, AutoFactoryInit, component_name=PORTAL_LOCKING_COMPONENT, persistence_key=SimObjectAttributes_pb2.PersistenceMaster.PersistableData.PortalLockingComponent):
    __qualname__ = 'PortalLockingComponent'
    DEFAULT_LOCK = LockAllWithSimIdExceptionData(lock_priority=LockPriority.SYSTEM_LOCK, lock_sides=LockSide.LOCK_BOTH)
    FACTORY_TUNABLES = {'preset_lock_datas': TunableList(description='\n            The lock datas that preset on the component. Their default priority\n            should be set to SystemLock\n            ', tunable=TunableVariant(lock_siminfo=LockSimInfoData.TunableFactory(), default='lock_siminfo'))}

    def __init__(self, *args, preset_lock_datas, **kwargs):
        super().__init__(preset_lock_datas=preset_lock_datas, *args, **kwargs)
        self.lock_datas = {}
        if preset_lock_datas:
            instanced_data = [lock_data() for lock_data in preset_lock_datas]
            for data in instanced_data:
                self.lock_datas[data.lock_type] = data

    @componentmethod
    def add_lock_data(self, lock_data, replace_same_lock_type=True):
        existing_data = self.lock_datas[lock_data.lock_type] if lock_data.lock_type in self.lock_datas else None
        if not replace_same_lock_type and existing_data is not None:
            lock_data.update(existing_data)
        self.lock_datas[lock_data.lock_type] = lock_data
        self.refresh_locks()

    @componentmethod_with_fallback(lambda : None)
    def refresh_locks(self):
        self._clear_locks_on_all_sims()
        self._lock_every_sim()

    def _lock_every_sim(self):
        for target_sim in services.sim_info_manager().instanced_sims_gen():
            self.lock_sim(target_sim)

    @componentmethod
    def lock(self):
        self.add_lock_data(PortalLockingComponent.DEFAULT_LOCK)

    @componentmethod
    def unlock(self):
        if self.lock_datas:
            self._clear_locks_on_all_sims()
            self.lock_datas.clear()

    @componentmethod
    def has_lock_data(self, lock_priority=None, lock_types=None):
        for lock_data in self.lock_datas.values():
            while (lock_priority is None or lock_data.lock_priority == lock_priority) and (lock_types is None or lock_data.lock_type in lock_types):
                return True
        return False

    @componentmethod
    def remove_player_locks(self, unlock_type=None):
        if not self.lock_datas:
            return
        self._clear_locks_on_all_sims()
        for lock_data in list(self.lock_datas.values()):
            while lock_data.lock_priority == LockPriority.PLAYER_LOCK and (unlock_type is None or lock_data.lock_type == unlock_type):
                del self.lock_datas[lock_data.lock_type]
        self._lock_every_sim()

    def _clear_locks_on_all_sims(self):
        for target_sim in services.sim_info_manager().instanced_sims_gen():
            self.owner.remove_disallowed_sim(target_sim, self)

    @componentmethod
    def lock_sim(self, sim):
        lock_result = self.test_lock(sim)
        if lock_result:
            self.owner.add_disallowed_sim(sim, self, lock_both=lock_result.is_locking_both())

    @componentmethod
    def test_lock(self, sim):
        current_system_lock_result = LockResult.NONE
        current_player_lock_result = LockResult.NONE
        for lock_data in self.lock_datas.values():
            lock_result = lock_data.test_lock(sim)
            if lock_result.is_player_lock():
                if current_player_lock_result == LockResult.NONE:
                    current_player_lock_result = lock_result
                elif current_player_lock_result.is_locked and not lock_result.is_locked:
                    current_player_lock_result = lock_result
                elif current_player_lock_result.is_locked and lock_result.is_locked:
                    if lock_result.is_locking_both():
                        current_player_lock_result = lock_result
                    elif current_system_lock_result == LockResult.NONE:
                        current_system_lock_result = lock_result
                    elif not current_system_lock_result.is_locked and lock_result.is_locked:
                        current_system_lock_result = lock_result
                    else:
                        while current_system_lock_result.is_locked and lock_result.is_locked:
                            if lock_result.is_locking_both():
                                current_system_lock_result = lock_result
            elif current_system_lock_result == LockResult.NONE:
                current_system_lock_result = lock_result
            elif not current_system_lock_result.is_locked and lock_result.is_locked:
                current_system_lock_result = lock_result
            else:
                while current_system_lock_result.is_locked and lock_result.is_locked:
                    if lock_result.is_locking_both():
                        current_system_lock_result = lock_result
        if current_system_lock_result.is_locked:
            if current_player_lock_result.is_locked and current_player_lock_result.is_locking_both():
                return current_player_lock_result
            return current_system_lock_result
        elif current_player_lock_result.is_locked:
            return current_player_lock_result
        return LockResult.NONE

    def save(self, persistence_master_message):
        persistable_data = SimObjectAttributes_pb2.PersistenceMaster.PersistableData()
        persistable_data.type = SimObjectAttributes_pb2.PersistenceMaster.PersistableData.PortalLockingComponent
        portal_lock_data = persistable_data.Extensions[SimObjectAttributes_pb2.PersistablePortalLockingComponent.persistable_data]
        for lock_data in self.lock_datas.values():
            while lock_data.lock_priority == LockPriority.PLAYER_LOCK:
                persist_lock_data = portal_lock_data.lock_data.add()
                persist_lock_data.lock_type = lock_data.lock_type
                lock_data.save(persist_lock_data)
        persistence_master_message.data.extend([persistable_data])

    def load(self, persistence_master_message):
        portal_lock_data = persistence_master_message.Extensions[SimObjectAttributes_pb2.PersistablePortalLockingComponent.persistable_data]
        if portal_lock_data.lock_data:
            for persist_lock_data in portal_lock_data.lock_data:
                lock_data = None
                lock_priority = LockPriority(persist_lock_data.priority)
                lock_sides = LockSide(persist_lock_data.sides)
                if persist_lock_data.lock_type == LockType.LOCK_ALL_WITH_SIMID_EXCEPTION:
                    lock_data = LockAllWithSimIdExceptionData(lock_priority=lock_priority, lock_sides=lock_sides, except_actor=persist_lock_data.except_actor, except_household=persist_lock_data.except_household)
                    lock_data.load(persist_lock_data)
                elif persist_lock_data.lock_type == LockType.LOCK_ALL_WITH_SITUATION_JOB_EXCEPTION:
                    lock_data = LockAllWithSituationJobExceptionData(lock_priority=lock_priority, lock_sides=lock_sides, situation_job_test=None, except_retail_employee=persist_lock_data.except_retail_employee)
                    lock_data.load(persist_lock_data)
                while lock_data is not None:
                    self.add_lock_data(lock_data)

import enum

class LockPriority(enum.Int):
    __qualname__ = 'LockPriority'
    PLAYER_LOCK = 0
    SYSTEM_LOCK = 1

class LockSide(enum.IntFlags):
    __qualname__ = 'LockSide'
    LOCK_FRONT = 1
    LOCK_BACK = 2
    LOCK_BOTH = LOCK_FRONT | LOCK_BACK

class LockType(enum.Int):
    __qualname__ = 'LockType'
    LOCK_SIMINFO = 0
    LOCK_ALL_WITH_SIMID_EXCEPTION = 1
    LOCK_ALL_WITH_SITUATION_JOB_EXCEPTION = 2

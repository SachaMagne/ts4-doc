from distributor.ops import ShowLightColorUI
from distributor.system import Distributor
from event_testing.results import TestResult
from interactions.base.immediate_interaction import ImmediateSuperInteraction
from objects.components.lighting_component import LightingComponent
from objects.lighting.lighting_utils import LightingHelper
from sims4.localization import TunableLocalizedString
from sims4.math import almost_equal
from sims4.tuning.tunable import Tunable, TunableTuple, TunableList, TunableColor
from sims4.tuning.tunable_base import ExportModes
import sims4.log
logger = sims4.log.Logger('Lighting')

class LightColorTuning:
    __qualname__ = 'LightColorTuning'

    class TunableLightTuple(TunableTuple):
        __qualname__ = 'LightColorTuning.TunableLightTuple'

        def __init__(self, *args, **kwargs):
            super().__init__(color=TunableColor.TunableColorRGBA(description='\n                Tunable RGBA values used to set the color of a light. Tuning the\n                A value will not do anything as it is not used.\n                '), name=TunableLocalizedString(description=' \n                The name of the color that appears when you mouse over it.\n                '))

    LIGHT_COLOR_VARIATION_TUNING = TunableList(description='\n        A list of all of the different colors you can set the lights to be.\n        ', tunable=TunableLightTuple(), maxlength=18, export_modes=(ExportModes.ClientBinary,))

class ChangeLightColorIntensityImmediateInteraction(ImmediateSuperInteraction):
    __qualname__ = 'ChangeLightColorIntensityImmediateInteraction'
    INSTANCE_TUNABLES = {'_all_lights': Tunable(description='\n            Whether or not to apply the new color and intensity values to all\n            of the lights or not.\n            ', tunable_type=bool, default=False)}

    def _run_interaction_gen(self, timeline):
        target = self.target
        r = g = b = sims4.color.MAX_INT_COLOR_VALUE
        color = target.get_light_color()
        intensity = target.get_user_intensity_overrides()
        target.set_light_dimmer_value(intensity)
        if color is not None:
            (r, g, b, _) = sims4.color.to_rgba_as_int(color)
        op = ShowLightColorUI(r, g, b, intensity, target.id, self._all_lights)
        distributor = Distributor.instance()
        distributor.add_op_with_no_owner(op)

class SwitchLightImmediateInteraction(ImmediateSuperInteraction):
    __qualname__ = 'SwitchLightImmediateInteraction'
    INSTANCE_TUNABLES = {'lighting_settings': LightingHelper.TunableFactory(tuning_group='~~Lighting~~')}

    @classmethod
    def _test(cls, target, context, **kwargs):
        dimmer_value = target.get_light_dimmer_value()
        if cls.lighting_settings.dimmer_value == LightingComponent.LIGHT_AUTOMATION_DIMMER_VALUE and dimmer_value < 0:
            return TestResult(False, 'Light is already being automated')
        if cls.lighting_settings.light_target.is_multi_light or almost_equal(cls.lighting_settings.dimmer_value, dimmer_value, epsilon=0.0001):
            return TestResult(False, 'Light is already at the desired dimmer value.')
        return TestResult.TRUE

    def _run_interaction_gen(self, timeline):
        self.lighting_settings.execute_lighting_helper(self)

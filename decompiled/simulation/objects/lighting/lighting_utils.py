from build_buy import get_all_objects_with_flags_gen, BuyCategory, get_object_has_tag, get_object_all_tags
from objects.components.lighting_component import LightingComponent
from sims4.tuning.tunable import AutoFactoryInit, HasTunableSingletonFactory, TunableSet, TunableEnumEntry, TunableVariant, OptionalTunable, Tunable
from tag import Tag
import objects.components.types
import services
import sims4.log
logger = sims4.log.Logger('Lighting')

class LightingHelper(AutoFactoryInit, HasTunableSingletonFactory):
    __qualname__ = 'LightingHelper'

    class _LightTargetInteraction(HasTunableSingletonFactory):
        __qualname__ = 'LightingHelper._LightTargetInteraction'

        @property
        def is_multi_light(self):
            return False

        def get_light_target_gen(self, interaction):
            yield interaction.target

    class _LightTargetAll(HasTunableSingletonFactory):
        __qualname__ = 'LightingHelper._LightTargetAll'

        @property
        def is_multi_light(self):
            return True

        def get_light_target_gen(self, interaction):
            for obj in get_all_objects_with_flags_gen(services.object_manager().get_all(), BuyCategory.LIGHTING):
                if obj.lighting_component is None:
                    logger.error("\n                        {} is flagged as BuyCategory.Lighting but doesn't have a\n                        lighting component. Please give it a lighting component\n                        or sort it differently.\n                        ", obj)
                if get_object_has_tag(obj.definition.id, LightingComponent.MANUAL_LIGHT_TAG):
                    pass
                yield obj

    class _LightTargetFromTags(AutoFactoryInit, HasTunableSingletonFactory):
        __qualname__ = 'LightingHelper._LightTargetFromTags'
        FACTORY_TUNABLES = {'tags': TunableSet(description='\n                An object with any tag in this set is a potential target of this\n                interaction, provided it has a lighting component.\n                ', tunable=TunableEnumEntry(description='\n                    A tag.\n                    ', tunable_type=Tag, default=Tag.INVALID, pack_safe=True))}

        @property
        def is_multi_light(self):
            return True

        def get_light_target_gen(self, interaction):
            for obj in services.object_manager().get_all_objects_with_component_gen(objects.components.types.LIGHTING_COMPONENT):
                target_object_tags = set(get_object_all_tags(obj.definition.id))
                while self.tags & target_object_tags:
                    yield obj

    FACTORY_TUNABLES = {'light_target': TunableVariant(description='\n            Define the set of lights that this interaction is applied to.\n            ', this_light=_LightTargetInteraction.TunableFactory(), all_lights=_LightTargetAll.TunableFactory(), specific_lights=_LightTargetFromTags.TunableFactory(), default='this_light'), 'dimmer_value': OptionalTunable(description='\n            Specify the intensity to be applied to the light.\n            ', tunable=Tunable(description='\n                This value should be a float between 0.0 and 1.0. A value of\n                0.0 is off and a value of 1.0 is completely on.\n             \n                Any tuned values outside of the range will be clamped back\n                to within the range. For example a negative value cannot be\n                tuned here and will be clamped to 0.0 or off.\n                ', tunable_type=float, default=0), enabled_name='Specify_Dimmer_Value', disabled_name='Automated_By_Client', disabled_value=LightingComponent.LIGHT_AUTOMATION_DIMMER_VALUE)}

    def execute_lighting_helper(self, interaction):
        for light_obj in self.light_target.get_light_target_gen(interaction):
            light_obj.set_light_dimmer_value(self.dimmer_value)

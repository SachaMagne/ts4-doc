from _animation import get_joint_transform_from_rig
from _weakrefset import WeakSet
from collections import defaultdict
from weakref import WeakKeyDictionary
import itertools
import weakref
from animation.posture_manifest_constants import STAND_AT_NONE_CONSTRAINT, SWIM_AT_NONE_CONSTRAINT
from interactions.constraints import GLOBAL_STUB_ACTOR, Constraint
from interactions.utils.animation import ArbElement
from objects.game_object import GameObject
from objects.portal import Portal, PortalRequiredFlag
from routing import RAYCAST_HIT_TYPE_NONE
from sims4.math import Transform
from sims4.tuning.tunable import Tunable
from singletons import UNSET
import build_buy
import caches
import objects.components
import postures
import routing
import services
import sims4.geometry
import sims4.log
logger = sims4.log.Logger('Pools', default_owner='bhill')
pool_seat_quadtree = defaultdict(sims4.geometry.QuadTree)
POOL_DEF_ID = 65956
with sims4.reload.protected(globals()):
    cached_pool_objects = WeakSet()

def get_main_pool_objects_gen():
    yield cached_pool_objects

def get_pool_by_block_id(block_id):
    for pool in get_main_pool_objects_gen():
        while pool.block_id == block_id:
            return pool
    logger.error('No Pool Matching block Id: {}', block_id, owner='cgast')

class PoolPortal(Portal):
    __qualname__ = 'PoolPortal'
    INSTANCE_TUNABLES = {'portal_cost': Tunable(description='\n            The cost for traversing this portal in meters.\n            ', tunable_type=int, default=100)}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._locking_sims_dict = WeakKeyDictionary()

    @property
    def sim(self):
        if self._portal_sim_ref is not None:
            return self._portal_sim_ref()

    def on_add(self):
        super().on_add()
        self.register_on_use_list_changed(self._use_list_changed)

    def on_remove(self):
        super().on_remove()
        self.unregister_on_use_list_changed(self._use_list_changed)

    @property
    def current_portal_cost(self):
        return max(cost for cost in itertools.chain(self._locking_sims_dict.values(), (self.portal_cost,)))

    def set_portal_cost(self, sim, cost):
        self._locking_sims_dict[sim] = cost
        self._set_portal_cost(sim, cost)

    def clear_portal_cost(self, sim):
        if sim in self._locking_sims_dict:
            del self._locking_sims_dict[sim]
        self._set_portal_cost(sim, 0)

    def _set_portal_cost(self, sim, cost):
        portal_cost = self.current_portal_cost
        for portal_pair in self.portals:
            for portal in portal_pair:
                if cost != 0:
                    sim.routing_context.override_portal_cost(portal, self.portal_cost)
                routing.update_portal_cost(portal, portal_cost)
                while cost == 0:
                    sim.routing_context.clear_override_portal_cost(portal)

    def _use_list_changed(self, *args, **kwargs):
        if self.get_users():
            new_portal_cost = routing.PORTAL_LOCKED_COST
        else:
            new_portal_cost = self.portal_cost
        for portal_pair in self.portals:
            for portal in portal_pair:
                routing.update_portal_cost(portal, new_portal_cost)

    @property
    def block_id(self):
        return build_buy.get_block_id(self.zone_id, self._location.transform.translation + self.forward, self.portal_exit_routing_surface.secondary_id - 1)

    def destroy(self, *args, **kwargs):
        self.portal_cleanup()
        super().destroy(*args, **kwargs)

    def get_bounding_box(self):
        p = self.transform.translation
        p = sims4.math.Vector2(p.x, p.z)
        return sims4.geometry.QtRect(p + sims4.math.Vector2(-0.5, -0.5), p + sims4.math.Vector2(0.5, 0.5))

    def portal_setup(self):
        super().portal_setup()
        if services.current_zone().is_zone_shutting_down:
            return
        if self.posture_change is not None:
            end_body_posture = postures.create_posture(self.posture_change.end_posture, GLOBAL_STUB_ACTOR, self)
            constraint_intersection = end_body_posture.slot_constraint
            entry_handle = constraint_intersection.get_connectivity_handles(GLOBAL_STUB_ACTOR)[0]
            entry_goal = entry_handle.get_goals()[0]
            there_start = entry_goal
            for constraint in constraint_intersection:
                break
            there_end = constraint.containment_transform
            exit_handle = constraint_intersection.get_connectivity_handles(GLOBAL_STUB_ACTOR, entry=False)[0]
            exit_goal = exit_handle.get_goals()[0]
            for constraint in constraint_intersection:
                break
            back_start = constraint.containment_transform_exit
            back_end = exit_goal
        self.world_portal_exit_location = routing.Location(back_end.position, orientation=back_end.orientation, routing_surface=self.portal_entry_routing_surface)
        self.pool_portal_exit_location = routing.Location(there_end.translation, orientation=there_end.orientation, routing_surface=self.portal_exit_routing_surface)
        there = self.create_portal(routing.Location(there_start.position, orientation=there_start.orientation, routing_surface=self.portal_entry_routing_surface), self.pool_portal_exit_location, Portal.PortalType.PortalType_Animate, self.id, traversal_cost=self.portal_cost, usage_penalty=0.0, key_mask=int(PortalRequiredFlag.REQUIRE_NO_CARRY))
        back = self.create_portal(routing.Location(back_start.translation, orientation=back_start.orientation, routing_surface=self.portal_exit_routing_surface), self.world_portal_exit_location, Portal.PortalType.PortalType_Animate, self.id, traversal_cost=self.portal_cost, usage_penalty=0.0, key_mask=int(PortalRequiredFlag.REQUIRE_NO_CARRY))
        self.add_pair(there, back)

    def get_locations_for_posture(self, node):
        if node == postures.posture_graph.SWIM_AT_NONE:
            return (self.pool_portal_exit_location,)
        return (self.world_portal_exit_location,)

    def split_path_on_portal(self):
        return True

    def get_surface_override_for_posture(self, source_posture_name):
        if source_posture_name == 'swim':
            return self.portal_exit_routing_surface

class SwimmingPoolSeat(PoolPortal):
    __qualname__ = 'SwimmingPoolSeat'

    def __init__(self, *args, pool=None, **kwargs):
        self._data = None
        self._joint_transform = None
        self._children_cache = None
        self._adjacent_parts = UNSET
        self.overlapping_parts = []
        self.part_owner = pool
        super().__init__(*args, **kwargs)

    @property
    def adjacent_parts(self):
        return self._adjacent_parts

    @adjacent_parts.setter
    def adjacent_parts(self, new_value):
        if self._adjacent_parts != new_value:
            self._adjacent_parts = new_value
            if any(part._adjacent_parts is UNSET for part in self.part_owner.parts):
                return
            posture_graph_service = services.current_zone().posture_graph_service
            with posture_graph_service.object_moving(self.part_owner):
                pass

    @property
    def part_owner(self):
        return self._part_owner

    @part_owner.setter
    def part_owner(self, value):
        self._part_owner = value
        if value is None:
            self.part_group_index = 0
        else:
            value._parts.append(self)
            self.part_group_index = value._parts.index(self)

    @caches.cached(maxsize=20)
    def check_line_of_sight(self, *args, verbose=False, **kwargs):
        if verbose:
            return (RAYCAST_HIT_TYPE_NONE, [])
        return RAYCAST_HIT_TYPE_NONE

    def __repr__(self):
        return '<part {} on {}>'.format(self.part_group_index, self.part_owner)

    def __str__(self):
        return '{}[{}]'.format(self.part_owner, self.part_group_index)

    @property
    def is_part(self):
        return True

    @property
    def part_definition(self):
        pass

    @property
    def disable_sim_aop_forwarding(self):
        return True

    @property
    def disable_child_aop_forwarding(self):
        return True

    @property
    def forward_direction_for_picking(self):
        return sims4.math.Vector3.Z_AXIS()

    def adjacent_parts_gen(self):
        if self.adjacent_parts:
            yield self.adjacent_parts

    def has_adjacent_part(self, sim):
        if not self.adjacent_parts:
            return False
        return any(part.may_reserve(sim) for part in self.adjacent_parts)

    def get_overlapping_parts(self):
        return self.overlapping_parts[:]

    def is_mirrored(self, part=None):
        if part is None:
            return False
        offset = part.position - self.position
        return sims4.math.vector_cross_2d(self.forward, offset) < 0

    @property
    def is_base_part(self):
        return True

    @property
    def subroot_index(self):
        pass

    @property
    def part_suffix(self):
        pass

    def supports_posture_spec(self, posture_spec, interaction=None):
        if interaction is not None and interaction.is_super:
            affordance = interaction.affordance
            if affordance.requires_target_support and not self.supports_affordance(affordance):
                return False
        if not self.supported_posture_types:
            return True
        if posture_spec.body is None:
            return False
        for supported_posture_info in self.supported_posture_types:
            while posture_spec.body_posture is supported_posture_info.posture_type:
                return True
        return False

    @property
    def transform(self):
        if self._joint_transform is None:
            try:
                self._joint_transform = get_joint_transform_from_rig(self.rig, ArbElement._BASE_ROOT_STRING)
            except KeyError:
                raise KeyError('Unable to find joint {} on {}'.format(ArbElement._BASE_ROOT_STRING, self))
        return Transform.concatenate(self._joint_transform, self._location.world_transform)

    @transform.setter
    def transform(self, transform):
        self.move_to(transform=transform)

    @property
    def can_reset(self):
        return True

class SwimmingPool(GameObject):
    __qualname__ = 'SwimmingPool'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._parts = []
        self._old_footprint_component = self.remove_component(objects.components.types.FOOTPRINT_COMPONENT.instance_attr)
        self._bounding_polygon = None
        self._center_point = None
        self._provided_routing_surface = None
        self._world_routing_surface = None

    @classmethod
    def _verify_tuning_callback(cls):
        super()._verify_tuning_callback()
        if cls._has_reservation_tests:
            logger.error("Interactions with object reservation tests have been tuned on the swimming pool.\nThis is not okay because it will make performance horribly bad. (For every pool seat, we have to look at every other pool seat).\nPlease remove {} from the pool's super interaction list or remove their object reservation tests.", ', '.join(str(sa) for sa in cls._super_affordances if sa.object_reservation_tests), owner='bhill')

    def on_add(self):
        super().on_add()
        cached_pool_objects.add(self)

    def on_remove(self):
        super().on_remove()
        cached_pool_objects.discard(self)

    def _build_routing_surfaces(self):
        self._provided_routing_surface = routing.SurfaceIdentifier(self.zone_id, self._location.world_routing_surface.secondary_id, routing.SurfaceType.SURFACETYPE_POOL)
        self._world_routing_surface = routing.SurfaceIdentifier(self.zone_id, self._location.world_routing_surface.secondary_id, routing.SurfaceType.SURFACETYPE_WORLD)

    def on_location_changed(self, old_location):
        super().on_location_changed(old_location)
        if self._location.routing_surface.type == routing.SurfaceType.SURFACETYPE_POOL:
            self._build_routing_surfaces()
            self._create_bounding_polygon()

    @property
    def remove_children_from_posture_graph_on_delete(self):
        return False

    def get_users(self, *args, **kwargs):
        return set()

    def get_edges(self):
        pool_edges = build_buy.get_pool_edges(self.zone_id)
        return pool_edges[(self.block_id, self.routing_surface.secondary_id)]

    def get_edge_constraint(self, constraint_width=1.0, inward_dir=False, return_constraint_list=False):
        edges = self.get_edges()
        polygons = []
        for (start, stop) in edges:
            along = sims4.math.vector_normalize(stop - start)
            inward = sims4.math.vector3_rotate_axis_angle(along, sims4.math.PI/2, sims4.math.Vector3.Y_AXIS())
            if inward_dir:
                polygon = sims4.geometry.Polygon([start, start + constraint_width*inward, stop + constraint_width*inward, stop])
            else:
                polygon = sims4.geometry.Polygon([start, stop, stop - constraint_width*inward, start - constraint_width*inward])
            polygons.append(polygon)
        if inward_dir:
            constraint_spec = SWIM_AT_NONE_CONSTRAINT
            routing_surface = self.provided_routing_surface
        else:
            constraint_spec = STAND_AT_NONE_CONSTRAINT
            routing_surface = self.world_routing_surface
        if return_constraint_list:
            constraint_list = []
            for polygon in polygons:
                restricted_polygon = sims4.geometry.RestrictedPolygon(polygon, ())
                constraint = Constraint(routing_surface=routing_surface, geometry=restricted_polygon)
                constraint = constraint.intersect(constraint_spec)
                constraint_list.append(constraint)
            return constraint_list
        geometry = sims4.geometry.RestrictedPolygon(sims4.geometry.CompoundPolygon(polygons), ())
        constraint = Constraint(routing_surface=routing_surface, geometry=geometry)
        constraint = constraint.intersect(constraint_spec)
        return constraint

    def _get_bounds(self):
        edges = self.get_edges()
        return sims4.math.get_bounds_2D([edge_tuple[0] for edge_tuple in edges])

    def _create_bounding_polygon(self):
        (lower_bounds, upper_bounds) = self._get_bounds()
        ll = sims4.math.Vector3(lower_bounds[0], 0, lower_bounds[1])
        lr = sims4.math.Vector3(lower_bounds[0], 0, upper_bounds[1])
        ul = sims4.math.Vector3(upper_bounds[0], 0, lower_bounds[1])
        ur = sims4.math.Vector3(upper_bounds[0], 0, upper_bounds[1])
        self._bounding_polygon = sims4.geometry.Polygon((ul, ur, lr, ll))
        self._find_center()

    @property
    def bounding_polygon(self):
        return self._bounding_polygon

    def _find_center(self):
        bounding_points = list(self._bounding_polygon)
        upper_left = bounding_points[0]
        lower_left = bounding_points[3]
        lower_right = bounding_points[2]
        center_x = upper_left.x - (upper_left.x - lower_left.x)/2
        center_z = lower_left.z - (lower_left.z - lower_right.z)/2
        self._center_point = sims4.math.Vector2(center_x, center_z)

    @property
    def center_point(self):
        return self._center_point

    @property
    def provided_routing_surface(self):
        return self._provided_routing_surface

    @property
    def world_routing_surface(self):
        return self._world_routing_surface

    @property
    def block_id(self):
        return build_buy.get_block_id(self.zone_id, self._location.transform.translation, self.provided_routing_surface.secondary_id - 1)

    @caches.cached(maxsize=20)
    def check_line_of_sight(self, *args, verbose=False, **kwargs):
        if verbose:
            return (RAYCAST_HIT_TYPE_NONE, [])
        return RAYCAST_HIT_TYPE_NONE

from collections import namedtuple
import weakref
from interactions.utils.animation import TunableAnimationOverrides
from objects.components.portal_locking_component import PortalLockingComponent
from objects.game_object import GameObject
from postures.posture_specs import get_origin_spec
from sims.outfits.outfit_change import TunableOutfitChange
from sims4.tuning.tunable import TunableSet, TunableEnumWithFilter, OptionalTunable, TunableTuple, TunableReference, TunableEnumEntry
from sims4.tuning.tunable_base import GroupNames
import enum
import routing
import services
import sims4.math
import sims4.utils
import tag
logger = sims4.log.Logger('Portal')
_PortalPair = namedtuple('_PortalPair', ['there', 'back'])

class PortalRequiredFlag(enum.IntFlags, export=False):
    __qualname__ = 'PortalRequiredFlag'
    REQUIRE_NO_CARRY = 1

class Portal(GameObject):
    __qualname__ = 'Portal'
    INSTANCE_TUNABLES = {'portal_disallowance_tags': TunableSet(description="\n                A set of tags that define what the portal disallowance tags of\n                this portal are.  Sim's with role states that also include any\n                of these disallowance tags consider the portal to be locked\n                when routing.\n                ", tunable=TunableEnumWithFilter(description='\n                    A single portal disallowance tag.\n                    ', tunable_type=tag.Tag, default=tag.Tag.INVALID, filter_prefixes=tag.PORTAL_DISALLOWANCE_PREFIX)), 'outfit_change': OptionalTunable(TunableOutfitChange(description='\n            Define the outfit change that happens when you enter/exit this portal.\n            '), tuning_group=GroupNames.CLOTHING_CHANGE), 'portal_overrides': OptionalTunable(TunableAnimationOverrides(description='\n            When a Sim animates through this portal, these animation overrides are\n            applied to the transition state machine.\n            ')), 'posture_change': OptionalTunable(TunableTuple(description='\n            Define a posture change as you cross through this portal. e.g. For the pool,\n            the start posture is stand, and the end posture is swim.\n            ', start_posture=TunableReference(manager=services.posture_manager()), end_posture=TunableReference(manager=services.posture_manager()))), 'portal_entry_surface_type': TunableEnumEntry(routing.SurfaceType, description='\n            The surface type this portal starts on to. e.g. the Pool starts on the\n            world surface and ends on the pool surface.\n            ', default=routing.SurfaceType.SURFACETYPE_WORLD), 'portal_exit_surface_type': TunableEnumEntry(routing.SurfaceType, description='\n            The surface type this portal exits to. e.g. the Pool starts on the\n            world surface and ends on the pool surface.\n            ', default=routing.SurfaceType.SURFACETYPE_WORLD), 'locking_component': PortalLockingComponent.TunableFactory()}

    class PortalType(enum.Int, export=False):
        __qualname__ = 'Portal.PortalType'
        PortalType_Wormhole = 0
        PortalType_Walk = 1
        PortalType_Animate = 2

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.add_component(self.locking_component(self))
        self._portals = []
        self._disallowed_sims = weakref.WeakKeyDictionary()

    @property
    def portals(self):
        return self._portals

    def on_location_changed(self, old_location):
        super().on_location_changed(old_location)
        self.portal_cleanup()
        self.portal_setup()
        self.refresh_locks()

    def on_add(self):
        super().on_add()
        services.object_manager().add_portal_to_cache(self)

    def on_remove(self):
        super().on_remove()
        self.portal_cleanup()
        services.object_manager().remove_portal_from_cache(self)

    def add_disallowed_sim(self, sim, disallower, lock_both=False):
        if sim not in self._disallowed_sims:
            self._disallowed_sims[sim] = set()
        self._disallowed_sims[sim].add(disallower)
        for portal_pair in self._portals:
            sim.routing_context.lock_portal(portal_pair.there)
            while lock_both:
                sim.routing_context.lock_portal(portal_pair.back)

    def remove_disallowed_sim(self, sim, disallower):
        disallowing_objects = self._disallowed_sims.get(sim)
        if disallowing_objects is None:
            return
        if disallower not in disallowing_objects:
            return
        disallowing_objects.remove(disallower)
        if not disallowing_objects:
            for portal_pair in self._portals:
                sim.routing_context.unlock_portal(portal_pair.there)
                sim.routing_context.unlock_portal(portal_pair.back)
            del self._disallowed_sims[sim]

    def _portal_refresh(self):
        pass

    def on_buildbuy_exit(self):
        super().on_buildbuy_exit()
        self._portal_refresh()
        self.refresh_locks()

    def portal_setup(self):
        if self.portals:
            raise ValueError('Portal: Portals Already Exist.')

    def create_portal(self, start_loc, end_loc, portal_type, portal_id, traversal_cost=-1.0, key_mask=0, usage_penalty=-1.0):
        return routing.add_portal(start_loc, end_loc, portal_type, portal_id, traversal_cost, key_mask, usage_penalty)

    def add_pair(self, there, back):
        self._portals.append(_PortalPair(there, back))
        for sim in self._disallowed_sims.keys():
            sim.routing_context.lock_portal(there)

    def portal_cleanup(self):
        while self.portals:
            portal_pair = self.portals.pop()
            if portal_pair.there is not None:
                routing.remove_portal(portal_pair.there)
            while portal_pair.back is not None:
                routing.remove_portal(portal_pair.back)
                continue

    def add_portal_events(self, portal, actor, time, route_pb):
        sims4.log.info('Routing', 'Actor:{0} using portal {1} in object {2}', actor, portal, self)

    @sims4.utils.exception_protected(default_return=0)
    def c_api_get_portal_duration(self, portal_id, walkstyle, age, gender):
        return 0.0

    def add_portal_data(self, portal, actor, walkstyle):
        pass

    def split_path_on_portal(self):
        return False

    def swap_there_and_back(self):
        old_portals = tuple(self._portals)
        self._portals.clear()
        for portal_pair in old_portals:
            new_portal_pair = _PortalPair(portal_pair.back, portal_pair.there)
            self._portals.append(new_portal_pair)
            for sim in self._disallowed_sims.keys():
                sim.routing_context.lock_portal(new_portal_pair.there)
                sim.routing_context.unlock_portal(new_portal_pair.back)

    def _entering_portal(self, portal_id):
        for p in self.portals:
            while portal_id == p.there:
                return True
        return False

    def get_entry_clothing_change(self, interaction, portal_id, **kwargs):
        if self._entering_portal(portal_id) and self.outfit_change is not None:
            return self.outfit_change.get_on_entry_change(interaction, **kwargs)

    def get_exit_clothing_change(self, interaction, portal_id, **kwargs):
        if not self._entering_portal(portal_id) and self.outfit_change is not None:
            return self.outfit_change.get_on_exit_change(interaction, **kwargs)

    def get_on_entry_outfit(self, interaction, portal_id, **kwargs):
        if self._entering_portal(portal_id) and self.outfit_change is not None:
            return self.outfit_change.get_on_entry_outfit(interaction, **kwargs)

    def get_on_exit_outfit(self, interaction, portal_id, **kwargs):
        if not self._entering_portal(portal_id) and self.outfit_change is not None:
            return self.outfit_change.get_on_exit_outfit(interaction, **kwargs)

    def get_posture_change(self, portal_id, initial_posture):
        if self.posture_change is None:
            return (initial_posture,)
        start_posture = get_origin_spec(self.posture_change.start_posture)
        end_posture = get_origin_spec(self.posture_change.end_posture)
        if self._entering_portal(portal_id):
            return (start_posture, end_posture)
        return (end_posture, start_posture)

    @property
    def portal_entry_routing_surface(self):
        return routing.SurfaceIdentifier(self.zone_id, self._location.world_routing_surface.secondary_id, self.portal_entry_surface_type)

    @property
    def portal_exit_routing_surface(self):
        return routing.SurfaceIdentifier(self._location.world_routing_surface.primary_id, self._location.world_routing_surface.secondary_id, self.portal_exit_surface_type)

    def get_target_surface(self, sim):
        if sim.routing_surface == self.portal_entry_routing_surface:
            return self.portal_exit_routing_surface
        return self.portal_entry_routing_surface

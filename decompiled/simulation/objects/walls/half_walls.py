from objects.game_object import GameObject
import routing

class HalfWall(GameObject):
    __qualname__ = 'HalfWall'
    LOCATION_OFFSET = 0.25

    def get_locations_for_posture(self, node):
        return (routing.Location(self.position + self.forward*self.LOCATION_OFFSET, self.orientation, self.routing_surface), routing.Location(self.position - self.forward*self.LOCATION_OFFSET, self.orientation, self.routing_surface))

from relationships.relationship_bit import RelationshipBit
from relationships.relationship_track import RelationshipTrack
from sims4.tuning.dynamic_enum import DynamicEnum
from sims4.tuning.tunable import AutoFactoryInit, HasTunableSingletonFactory, Tunable, TunableSet, TunableMapping, TunableEnumEntry, TunableList, TunableTuple
from tunable_utils.tested_list import TunableTestedList

class DefaultGenealogyLink(DynamicEnum):
    __qualname__ = 'DefaultGenealogyLink'
    Roommate = 0
    FamilyMember = 1
    Spouse = 2

class DefaultRelationship(HasTunableSingletonFactory, AutoFactoryInit):
    __qualname__ = 'DefaultRelationship'
    FACTORY_TUNABLES = {'relationship_tracks': TunableList(description='\n            A list of relationship track and value pairs. E.g. a spouse has\n            Romantic relationship track value of 75.\n            ', tunable=TunableTuple(track=RelationshipTrack.TunableReference(description='\n                    The relationship track that is added to the relationship\n                    between the two sims.\n                    ', pack_safe=True), value=Tunable(description='\n                    The relationship track is set to this value.\n                    ', tunable_type=int, default=0))), 'relationship_bits': TunableSet(description='\n            A set of untracked relationship bits that are applied to the\n            relationship between the two sims. These are bits that are provided\n            outside of the relationship_track being set. E.g. everyone in the\n            household should have the Has Met bit and the spouse should have the\n            First Kiss bit.\n            ', tunable=RelationshipBit.TunableReference(pack_safe=True))}

    def apply(self, relationship_tracker, target_sim):
        sim_to_target_tracker = relationship_tracker.get_relationship_track_tracker(target_sim.sim_id, add=True)
        for data in self.relationship_tracks:
            track = data.track
            value = data.value
            relationship_track = sim_to_target_tracker.get_statistic(track, True)
            if relationship_track.get_value() < value:
                sim_to_target_tracker.set_value(track, value)
            relationship_track.update_instance_data()
        for bit in self.relationship_bits:
            sim_to_target_tracker.relationship.add_bit(bit)

class DefaultRelationshipInHousehold:
    __qualname__ = 'DefaultRelationshipInHousehold'
    RelationshipSetupMap = TunableMapping(description='\n        A mapping of the possible genealogy links in a family to the default \n        relationship values that we want to start our household members. \n        ', key_name='Family Link', key_type=TunableEnumEntry(description='\n            A genealogy link between the two Sims in the household.\n            ', tunable_type=DefaultGenealogyLink, default=DefaultGenealogyLink.Roommate), value_name='Default Relationship Setup', value_type=TunableTestedList(description='\n            A list of relationship actions to apply.\n            ', tunable_type=DefaultRelationship.TunableFactory()))

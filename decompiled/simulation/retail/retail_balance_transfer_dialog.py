from protocolbuffers import Dialog_pb2, DistributorOps_pb2
from distributor.ops import GenericProtocolBufferOp
from distributor.rollback import ProtocolBufferRollback
from distributor.system import Distributor
from sims4.localization import TunableLocalizedString, LocalizationHelperTuning
from sims4.tuning.tunable import TunableRange
import services
import sims4.log
logger = sims4.log.Logger('Retail', default_owner='trevor')

class RetailBalanceTransferDialog:
    __qualname__ = 'RetailBalanceTransferDialog'
    PLAYER_HOUSEHOLD_TITLE = TunableLocalizedString(description='\n        This is the text that will show for the players home lot. Typically,\n        the lot name would show but the home lot should say something along the\n        lines of "Player Household" to avoid confusion.\n        ')
    CURRENT_RETAIL_LOT_TITLE = TunableLocalizedString(description='\n        This is the text that will show for the current lot if it\'s a retail\n        lot. Typically, the lot name would show but if the active lot is a\n        retail lot, it makes more sense to say something along the lines of\n        "Current Retail Lot" instead of the name of the lot.\n        ')
    FIRST_TIME_BUYER_DEFAULT_TRANSFER_AMOUNT = TunableRange(description='\n        The amount of money to default in the transfer dialog when a player\n        first buys the retail lot. This should be a non-zero amount and is the\n        only time the transfer amount is defaulted to something other than 0.\n        ', tunable_type=int, default=500, minimum=1)

    @classmethod
    def show_dialog(cls, first_time_buyer=False):
        active_household = services.active_household()
        if active_household is None:
            logger.error('Trying to show the balance transfer dialog but failed to find an active household.')
            return False
        retail_tracker = active_household.retail_tracker
        if retail_tracker is None:
            logger.error("Trying to show the balance transfer dialog but this household doesn't own any retail lots.")
            return False
        retail_managers = retail_tracker.retail_managers
        if not retail_managers:
            logger.error("Trying to show the balance transfer dialog but this household doesn't own any retail lots.")
            return False
        current_zone_id = services.current_zone_id()
        balance_transfer_msg = Dialog_pb2.BalanceTransferDialog()
        balance_transfer_msg.transfer_amount = min(active_household.funds.money, cls.FIRST_TIME_BUYER_DEFAULT_TRANSFER_AMOUNT) if first_time_buyer else 0
        on_retail_lot = current_zone_id in retail_managers
        if first_time_buyer or not on_retail_lot:
            cls._add_household(balance_transfer_msg, active_household)
            cls._try_add_current_retail_lot(balance_transfer_msg, retail_managers, current_zone_id)
        else:
            cls._try_add_current_retail_lot(balance_transfer_msg, retail_managers, current_zone_id)
            cls._add_household(balance_transfer_msg, active_household)
        for (zone_id, retail_manager) in retail_managers.items():
            if zone_id == current_zone_id:
                pass
            zone_data = services.get_persistence_service().get_zone_proto_buff(zone_id)
            if zone_data is None:
                logger.error("Retail tracker thinks a zone exists that doesn't. Zone id:{}", zone_id)
            with ProtocolBufferRollback(balance_transfer_msg.lot_data) as lot_data:
                lot_data.lot_name = LocalizationHelperTuning.get_raw_text(zone_data.name)
                lot_data.zone_id = zone_id
                lot_data.balance = retail_manager.funds.money
        transfer_op = GenericProtocolBufferOp(DistributorOps_pb2.Operation.RETAIL_BALANCE_TRANSFER_DIALOG, balance_transfer_msg)
        Distributor.instance().add_op_with_no_owner(transfer_op)

    @classmethod
    def _add_household(cls, balance_transfer_msg, active_household):
        home_lot_data = balance_transfer_msg.lot_data.add()
        home_lot_data.lot_name = cls.PLAYER_HOUSEHOLD_TITLE
        home_lot_data.zone_id = active_household.home_zone_id
        home_lot_data.balance = active_household.funds.money

    @classmethod
    def _try_add_current_retail_lot(cls, balance_transfer_msg, retail_managers, current_zone_id):
        if current_zone_id in retail_managers:
            retail_data = balance_transfer_msg.lot_data.add()
            retail_data.lot_name = cls.CURRENT_RETAIL_LOT_TITLE
            retail_data.zone_id = current_zone_id
            retail_data.balance = retail_managers[current_zone_id].funds.money

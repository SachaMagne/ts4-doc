from protocolbuffers import Consts_pb2, Dialog_pb2, UI_pb2, InteractionOps_pb2
from distributor import shared_messages
from distributor.rollback import ProtocolBufferRollback
from distributor.system import Distributor
from event_testing.resolver import SingleSimResolver
from interactions.context import InteractionContext
from interactions.priority import Priority
from retail.retail_balance_transfer_dialog import RetailBalanceTransferDialog
from retail.retail_funds import RetailFunds
from retail.retail_manager import TELEMETRY_GROUP_RETAIL, TELEMETRY_HOOK_STORE_SOLD, TELEMETRY_HOOK_NEW_GAME_STORE_PURCHASED
from retail.retail_tuning import RetailTuning
from retail.retail_utils import RetailUtils
from server_commands.argument_helpers import OptionalSimInfoParam, get_optional_target, RequiredTargetParam
from sims4.commands import CommandType
from sims4.common import Pack
import distributor
import services
import sims4.commands
import telemetry_helper
logger = sims4.log.Logger('Retail', default_owner='trevor')
retail_telemetry_writer = sims4.telemetry.TelemetryWriter(TELEMETRY_GROUP_RETAIL)

@sims4.commands.Command('retail.get_retail_info', pack=Pack.EP01)
def get_retail_info(_connection=None):
    output = sims4.commands.Output(_connection)
    retail_manager = RetailUtils.get_retail_manager_for_zone()
    if retail_manager is None:
        output("This doesn't appear to be a retail lot.")
        return False
    is_open = retail_manager.is_open
    output('Funds: {}'.format(retail_manager.funds.money))
    output('Curb Appeal: {}'.format(retail_manager.get_curb_appeal()))
    output('Employee Count: {}'.format(retail_manager.employee_count))
    output('Markup Multiplier: {}X'.format(retail_manager.markup_multiplier))
    output('Median Item Price: {}'.format(retail_manager.get_median_item_value()))
    output('The store is {}.'.format('OPEN' if is_open else 'CLOSED'))
    if is_open:
        output('Items Sold: {}'.format(retail_manager.daily_items_sold))
        output('Gross Income: {}'.format(retail_manager.funds))
    format_msg = '{sim:>24} {career_level:>32} {salary:>12} {desired_salary:>12}'
    output(format_msg.format(sim='Sim', career_level='Career Level', salary='Current Salary', desired_salary='Desired Salary'))
    for employee_sim in retail_manager.get_employees_gen():
        career_level = retail_manager.get_employee_career_level(employee_sim)
        desired_career_level = retail_manager.RETAIL_CAREER.start_track.career_levels[retail_manager.get_employee_desired_career_level(employee_sim)]
        output(format_msg.format(sim=employee_sim.full_name, career_level=str(career_level.__name__), salary=career_level.simoleons_per_hour, desired_salary=desired_career_level.simoleons_per_hour))
    return True

@sims4.commands.Command('retail.set_price_markup_multiplier', command_type=CommandType.Live, pack=Pack.EP01)
def set_retail_price_markup_multiplier(markup_multiplier:float=0, _connection=None):
    output = sims4.commands.Output(_connection)
    retail_manager = RetailUtils.get_retail_manager_for_zone()
    if retail_manager is None:
        output("This doesn't appear to be a retail lot.")
        return False
    if markup_multiplier < 0:
        output('Invalid markup multiplier provided. Markup must be 0 or greater.')
        return False
    retail_manager.markup_multiplier = markup_multiplier
    return True

@sims4.commands.Command('retail.show_summary_dialog', command_type=CommandType.Live, pack=Pack.EP01)
def show_retail_summary_dialog(_connection=None):
    retail_manager = RetailUtils.get_retail_manager_for_zone()
    if retail_manager is None:
        return False
    retail_manager.show_summary_dialog()
    return True

@sims4.commands.Command('retail.show_retail_dialog', command_type=CommandType.Live, pack=Pack.EP01)
def show_retail_dialog(opt_sim:OptionalSimInfoParam=None, _connection=None):
    sim_info = get_optional_target(opt_sim, target_type=OptionalSimInfoParam, _connection=_connection)
    if sim_info is None:
        return False
    retail_manager = RetailUtils.get_retail_manager_for_zone()
    if retail_manager is None:
        return False
    msg = Dialog_pb2.RetailManageEmployeesDialog()
    msg.hiring_sim_id = sim_info.sim_id
    owner_household = retail_manager.owner_household
    if owner_household is not None:
        retail_tracker = owner_household.retail_tracker
        available_slots = retail_tracker.EMPLOYEE_SLOTS + retail_tracker.additional_employee_slots
        msg.open_slots = available_slots - retail_manager.get_employee_count()
        msg.locked_slots = retail_tracker.EMPLOYEE_SLOT_LIMIT - available_slots

    def populate_employee_msg(sim_info, employee_msg):
        employee_msg.sim_id = sim_info.sim_id
        for skill_type in retail_manager.EMPLOYEE_SKILLS:
            with ProtocolBufferRollback(employee_msg.skill_data) as employee_skill_msg:
                employee_skill_msg.skill_id = skill_type.guid64
                employee_skill_msg.curr_points = int(sim_info.get_stat_value(skill_type))
        if retail_manager.is_employee(sim_info):
            satisfaction_stat = sim_info.get_statistic(retail_manager.EMPLOYEE_SATISFACTION_COMMODITY)
            statisfaction_state_index = satisfaction_stat.get_state_index()
            if statisfaction_state_index is not None:
                employee_msg.satisfaction_string = satisfaction_stat.states[statisfaction_state_index].buff.buff_type.buff_name(sim_info)
            career_level = retail_manager.get_employee_career_level(sim_info)
            employee_msg.pay = career_level.simoleons_per_hour
            career = retail_manager.get_employee_career(sim_info)
            employee_msg.current_career_level = career.level
            employee_msg.max_career_level = len(career.current_track_tuning.career_levels) - 1
        else:
            desired_level = retail_manager.get_employee_desired_career_level(sim_info)
            career_level = retail_manager.RETAIL_CAREER.start_track.career_levels[desired_level]
            employee_msg.pay = career_level.simoleons_per_hour
            employee_msg.current_career_level = desired_level
            employee_msg.max_career_level = len(retail_manager.RETAIL_CAREER.start_track.career_levels) - 1

    for employee_sim_info in retail_manager.get_employees_gen():
        with ProtocolBufferRollback(msg.employees) as employee_msg:
            populate_employee_msg(employee_sim_info, employee_msg)
    results = services.sim_filter_service().submit_matching_filter(number_of_sims_to_find=retail_manager.EMPLOYEE_POOL_SIZE, sim_filter=retail_manager.EMPLOYEE_POOL_FILTER, requesting_sim_info=sim_info, allow_yielding=False)
    for result in results:
        with ProtocolBufferRollback(msg.available_sims) as employee_msg:
            populate_employee_msg(result.sim_info, employee_msg)
    op = shared_messages.create_message_op(msg, Consts_pb2.MSG_RETAIL_MANAGE_EMPLOYEES)
    Distributor.instance().add_op_with_no_owner(op)

@sims4.commands.Command('retail.employee_hire', command_type=CommandType.Live, pack=Pack.EP01)
def hire_retail_employee(sim, _connection=None):
    retail_manager = RetailUtils.get_retail_manager_for_zone()
    if retail_manager is None:
        return False
    target_sim = sim.get_target(manager=services.sim_info_manager())
    if target_sim is None:
        return False
    return retail_manager.run_employee_interaction(retail_manager.EMPLOYEE_INTERACTION_HIRE, target_sim)

@sims4.commands.Command('retail.employee_fire', command_type=CommandType.Live, pack=Pack.EP01)
def fire_retail_employee(sim, _connection=None):
    retail_manager = RetailUtils.get_retail_manager_for_zone()
    if retail_manager is None:
        return False
    target_sim = sim.get_target(manager=services.sim_info_manager())
    if target_sim is None:
        return False
    return retail_manager.run_employee_interaction(retail_manager.EMPLOYEE_INTERACTION_FIRE, target_sim)

@sims4.commands.Command('retail.employee_promote', command_type=CommandType.Live, pack=Pack.EP01)
def promote_retail_employee(sim, _connection=None):
    retail_manager = RetailUtils.get_retail_manager_for_zone()
    if retail_manager is None:
        return False
    target_sim = sim.get_target(manager=services.sim_info_manager())
    if target_sim is None:
        return False
    return retail_manager.run_employee_interaction(retail_manager.EMPLOYEE_INTERACTION_PROMOTE, target_sim)

@sims4.commands.Command('retail.employee_demote', command_type=CommandType.Live, pack=Pack.EP01)
def demote_retail_employee(sim, _connection=None):
    retail_manager = RetailUtils.get_retail_manager_for_zone()
    if retail_manager is None:
        return False
    target_sim = sim.get_target(manager=services.sim_info_manager())
    if target_sim is None:
        return False
    return retail_manager.run_employee_interaction(retail_manager.EMPLOYEE_INTERACTION_DEMOTE, target_sim)

@sims4.commands.Command('retail.add_funds', pack=Pack.EP01)
def add_retail_funds(amount:int=1000, _connection=None):
    output = sims4.commands.Output(_connection)
    retail_manager = RetailUtils.get_retail_manager_for_zone()
    if retail_manager is None:
        output("This doesn't appear to be a retail lot.")
        return False
    retail_manager.modify_funds(amount, from_item_sold=False)

@sims4.commands.Command('retail.sell_lot', command_type=CommandType.Live, pack=Pack.EP01)
def sell_retail_lot(_connection=None):
    output = sims4.commands.Output(_connection)
    retail_manager = RetailUtils.get_retail_manager_for_zone()
    if retail_manager is None:
        output("Trying to sell a lot that isn't a retail lot.")
        return False
    current_zone = services.current_zone()
    lot_value = current_zone.lot.furnished_lot_value
    sell_value = max(0.0, retail_manager._funds.money + lot_value)
    dialog = retail_manager.SELL_STORE_DIALOG(current_zone)
    dialog.show_dialog(on_response=sell_retail_lot_response, additional_tokens=(sell_value,))

def sell_retail_lot_response(dialog):
    if not dialog.accepted:
        return
    retail_manager = RetailUtils.get_retail_manager_for_zone()
    with telemetry_helper.begin_hook(retail_telemetry_writer, TELEMETRY_HOOK_STORE_SOLD, household=retail_manager.owner_household):
        pass
    current_zone = services.current_zone()
    lot = current_zone.lot
    lot_value = lot.furnished_lot_value
    retail_manager.modify_funds(lot_value)
    retail_manager.transfer_balance_to_household()
    services.get_zone_manager().clear_lot_ownership(current_zone.id)
    retail_manager.owner_household.retail_tracker.remove_owner(current_zone.id)
    msg = InteractionOps_pb2.SellRetailLot()
    msg.retail_zone_id = current_zone.id
    distributor.system.Distributor.instance().add_event(Consts_pb2.MSG_SELL_RETAIL_LOT, msg)

@sims4.commands.Command('retail.toggle_for_sale_vfx', command_type=CommandType.Live, pack=Pack.EP01)
def toggle_for_sale_vfx(_connection=None):
    retail_manager = RetailUtils.get_retail_manager_for_zone()
    if retail_manager is None:
        logger.error('Trying to toggle for sale VFX when not in a retail zone.', owner='tastle')
        return
    retail_manager.toggle_for_sale_vfx()

@sims4.commands.Command('retail.set_markup_multiplier', command_type=CommandType.Live, pack=Pack.EP01)
def set_markup_multiplier(markup_multiplier, _connection=None):
    retail_manager = RetailUtils.get_retail_manager_for_zone()
    if retail_manager is None:
        logger.error('Trying to set retail markup when not in a retail zone.', owner='tastle')
        return
    retail_manager.set_markup_multiplier(markup_multiplier)

@sims4.commands.Command('retail.set_open', command_type=CommandType.Live, pack=Pack.EP01)
def set_open(is_open, _connection=None):
    retail_manager = RetailUtils.get_retail_manager_for_zone()
    if retail_manager is None:
        logger.error('Trying to open or close a store when not in a retail zone.', owner='tastle')
        return
    if is_open:
        active_sim_info = services.active_sim_info()
        if retail_manager.has_any_object_for_sale():
            notification = retail_manager.OPEN_STORE_NOTIFICATION(active_sim_info, resolver=SingleSimResolver(active_sim_info))
        else:
            notification = retail_manager.NO_ITEM_FOR_SALE_WHEN_OPEN_NOTIFICATION(active_sim_info, resolver=SingleSimResolver(active_sim_info))
        notification.show_dialog()
    else:
        retail_manager.show_summary_dialog()
    retail_manager.set_open(is_open)

@sims4.commands.Command('retail.show_balance_transfer_dialog', command_type=CommandType.Live, pack=Pack.EP01)
def show_retail_balance_transfer_dialog(_connection=None):
    RetailBalanceTransferDialog.show_dialog()

@sims4.commands.Command('retail.transfer_funds', command_type=CommandType.Live, pack=Pack.EP01)
def transfer_retail_funds(amount, from_zone_id, to_zone_id, _connection=None):
    output = sims4.commands.Output(_connection)
    if amount < 1:
        output('You can only transfer positive, non-zero amounts.')
        return False
    from_retail_manager = RetailUtils.get_retail_manager_for_zone(from_zone_id)
    to_retail_manager = RetailUtils.get_retail_manager_for_zone(to_zone_id)
    if from_retail_manager is None and to_retail_manager is None:
        output('Invalid transfer request. Neither zone was a retail zone. At least one retail zone is required.')
        return False
    if from_retail_manager is None:
        household = to_retail_manager.owner_household
        RetailFunds.transfer_funds(amount, from_funds=household.funds, to_funds=to_retail_manager.funds)
    elif to_retail_manager is None:
        household = from_retail_manager.owner_household
        RetailFunds.transfer_funds(amount, from_funds=from_retail_manager.funds, to_funds=household.funds)
    else:
        RetailFunds.transfer_funds(amount, from_funds=from_retail_manager.funds, to_funds=to_retail_manager.funds)
    return True

@sims4.commands.Command('retail.get_owned_lot_count_message', command_type=CommandType.Live, pack=Pack.EP01)
def get_owned_retail_lot_count_message(_connection=None):
    lot_count = 0
    active_household = services.active_household()
    if active_household is not None:
        retail_tracker = active_household.retail_tracker
        if retail_tracker is not None:
            lot_count = len(retail_tracker.retail_managers)
    lot_count_msg = UI_pb2.OwnedRetailLotCountMessage()
    lot_count_msg.owned_lot_count = lot_count
    op = shared_messages.create_message_op(lot_count_msg, Consts_pb2.MSG_RETAIL_OWNED_LOT_COUNT)
    Distributor.instance().add_op_with_no_owner(op)

@sims4.commands.Command('retail.push_active_sim_to_buy_retail_lot', command_type=CommandType.Live, pack=Pack.EP01)
def push_active_sim_to_buy_retail_lot(_connection=None):
    output = sims4.commands.Output(_connection)
    active_sim = services.get_active_sim()
    if active_sim is None:
        output('There is no active sim.')
        return False
    context = InteractionContext(active_sim, InteractionContext.SOURCE_SCRIPT, Priority.High)
    result = active_sim.push_super_affordance(RetailTuning.BUY_RETAIL_LOT_AFFORDANCE, active_sim, context)
    if result:
        with telemetry_helper.begin_hook(retail_telemetry_writer, TELEMETRY_HOOK_NEW_GAME_STORE_PURCHASED, household=active_sim.household):
            pass
    return result

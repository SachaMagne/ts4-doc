import operator
import random
from buffs.tunable import TunableBuffReference
from date_and_time import create_time_span
from event_testing.test_events import TestEvent
from interactions import ParticipantType
from retail.retail_manager import RetailManager
from retail.retail_utils import RetailUtils
from role.role_state import RoleState
from sims4.tuning.tunable import TunableSimMinute, AutoFactoryInit, HasTunableFactory, TunableMapping, TunablePercent
from sims4.utils import classproperty
from situations.situation import Situation
from situations.situation_complex import SituationComplexCommon, TunableInteractionOfInterest, SituationState, SituationStateData
from situations.situation_job import SituationJob
import alarms
import services
import situations

class RetailEmployeeSituation(SituationComplexCommon):
    __qualname__ = 'RetailEmployeeSituation'

    class _EmployeeSituationState(HasTunableFactory, AutoFactoryInit, SituationState):
        __qualname__ = 'RetailEmployeeSituation._EmployeeSituationState'
        FACTORY_TUNABLES = {'role_state': RoleState.TunableReference(description='\n                The role state that is active on the employee for the duration\n                of this state.\n                '), 'timeout_min': TunableSimMinute(description='\n                The minimum amount of time, in Sim minutes, the employee will be\n                in this state before moving on to a new state.\n                ', default=10), 'timeout_max': TunableSimMinute(description='\n                The maximum amount of time, in Sim minutes, the employee will be\n                in this state before moving on to a new state.\n                ', default=30), 'push_interaction': TunableInteractionOfInterest(description='\n                If an interaction of this type is run by the employee, this\n                state will activate.\n                ')}

        def __init__(self, *args, state_name=None, **kwargs):
            super().__init__(*args, **kwargs)
            self.state_name = state_name

        def on_activate(self, reader=None):
            super().on_activate(reader)
            self.owner._set_job_role_state(self.owner.employee_job, self.role_state)
            timeout = random.randint(self.timeout_min, self.timeout_max)
            self._create_or_load_alarm(self.state_name, timeout, self._timeout_expired, reader=reader)

        def _timeout_expired(self, *_, **__):
            self._change_state(self.owner._choose_next_state())

    INSTANCE_TUNABLES = {'employee_job': SituationJob.TunableReference(description='\n            The situation job for the employee.\n            '), 'role_state_go_to_store': RoleState.TunableReference(description='\n            The role state for getting the employee inside the store. This is\n            the default role state and will be run first before any other role\n            state can start.\n            '), 'state_socialize': _EmployeeSituationState.TunableFactory(description='\n            The state during which employees socialize with customers.\n            ', locked_args={'state_name': 'socialize'}), 'state_restock': _EmployeeSituationState.TunableFactory(description='\n            The state during which employees restock items.\n            ', locked_args={'state_name': 'restock'}), 'state_clean': _EmployeeSituationState.TunableFactory(description='\n            The state during which employees clean the store.\n            ', locked_args={'state_name': 'clean'}), 'state_slack_off': _EmployeeSituationState.TunableFactory(description='\n            The state during which employees slack off.\n            ', locked_args={'state_name': 'slack_off'}), 'state_ring_up_customers': _EmployeeSituationState.TunableFactory(description='\n            The state during which employees will ring up customers.\n            ', locked_args={'state_name': 'ring_up_customers'}), 'go_to_store_interaction': TunableInteractionOfInterest(description='\n            The interaction that, when run by an employee, will switch the\n            situation state to start cleaning, upselling, restocking, etc.\n            '), 'go_home_interaction': TunableInteractionOfInterest(description='\n            The interaction that, when run on an employee, will have them end\n            this situation and go home.\n            '), 'total_work_time_min': TunableSimMinute(description='\n            The minimum amount of time, in sim minutes, an employee will work\n            before leaving for the day.\n            ', default=1), 'total_work_time_max': TunableSimMinute(description='\n            The maximum amount of time, in sim minutes, an employee will work\n            before leaving for the day.\n            ', default=2), 'total_work_time_buffs': TunableMapping(description="\n            A mapping from percentage duration worked to buffs applied to\n            employees working more than this amount. Buffs don't stack; the\n            higher percentage buff is the applied one.\n            ", key_type=TunablePercent(description='\n                The percentage duration worked.\n                ', default=50), value_type=TunableBuffReference(description='\n                The buff applied to employees working over the specified amount.\n                '))}
    REMOVE_INSTANCE_TUNABLES = Situation.NON_USER_FACING_REMOVE_INSTANCE_TUNABLES

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._employee_sim_info = None
        self._work_buff_handle = None
        self._work_alarm_handle = None
        reader = self._seed.custom_init_params_reader
        if reader is None:
            self._original_duration = random.randint(self.total_work_time_min, self.total_work_time_max)
        else:
            self._original_duration = reader.read_uint64('original_duration', self.duration)
        retail_manager = RetailUtils.get_retail_manager_for_zone()
        retail_manager.on_store_closed.register(self._on_go_home)
        services.get_event_manager().register_single_event(self, TestEvent.SkillLevelChange)

    @classmethod
    def _states(cls):
        return (SituationStateData(1, _GoToStoreState), SituationStateData(2, cls.state_socialize), SituationStateData(5, cls.state_restock), SituationStateData(6, cls.state_clean), SituationStateData(7, cls.state_slack_off), SituationStateData(8, cls.state_ring_up_customers))

    @classmethod
    def _state_to_uid(cls, state_to_find):
        state_type_to_find = type(state_to_find)
        if state_type_to_find is _GoToStoreState:
            return 1
        state_name = getattr(state_to_find, 'state_name', None)
        if state_name is None:
            return cls.INVALID_STATE_UID
        for state_data in cls._states():
            while getattr(state_data.state_type, 'state_name', None) == state_name:
                return state_data.uid
        return cls.INVALID_STATE_UID

    def _save_custom_situation(self, writer):
        super()._save_custom_situation(writer)
        writer.write_uint64('original_duration', self._original_duration)

    def handle_event(self, sim_info, event, resolver):
        if event == TestEvent.SkillLevelChange:
            if sim_info is self._employee_sim_info:
                retail_manager = RetailUtils.get_retail_manager_for_zone()
                if retail_manager is not None and retail_manager.is_owner_household_active:
                    skill = resolver.event_kwargs['skill']
                    if skill.stat_type in RetailManager.EMPLOYEE_SKILLS:
                        skill.force_show_level_notification(resolver.event_kwargs['new_level'])
        elif event == TestEvent.InteractionComplete:
            target_sim = resolver.interaction.get_participant(ParticipantType.TargetSim)
            if target_sim is None:
                target_sim = resolver.interaction.get_participant(ParticipantType.Actor)
            target_sim = getattr(target_sim, 'sim_info', target_sim)
            if target_sim is self._employee_sim_info:
                if resolver(self.state_socialize.push_interaction):
                    self._change_state(self.state_socialize())
                elif resolver(self.state_restock.push_interaction):
                    self._change_state(self.state_restock())
                elif resolver(self.state_clean.push_interaction):
                    self._change_state(self.state_clean())
                elif resolver(self.state_slack_off.push_interaction):
                    self._change_state(self.state_slack_off())
                elif resolver(self.state_ring_up_customers.push_interaction):
                    self._change_state(self.state_ring_up_customers())
                elif resolver(self.go_home_interaction):
                    self._on_go_home()
        super().handle_event(sim_info, event, resolver)

    def _destroy(self):
        if self._employee_sim_info is not None:
            self._clock_out()
        retail_manager = RetailUtils.get_retail_manager_for_zone()
        retail_manager.on_store_closed.unregister(self._on_go_home)
        services.get_event_manager().unregister_single_event(self, TestEvent.SkillLevelChange)
        super()._destroy()

    @classmethod
    def _get_tuned_job_and_default_role_state_tuples(cls):
        return [(cls.employee_job, cls.role_state_go_to_store)]

    @classmethod
    def default_job(cls):
        return cls.employee_job

    def start_situation(self):
        super().start_situation()
        self._change_state(_GoToStoreState())

    @classmethod
    def get_sims_expected_to_be_in_situation(cls):
        return 1

    @classproperty
    def situation_serialization_option(cls):
        return situations.situation_types.SituationSerializationOption.LOT

    def get_employee(self):
        if self._employee_sim_info is not None:
            return self._employee_sim_info
        return next(self._guest_list.invited_sim_infos_gen())

    def _on_set_sim_job(self, sim, job_type):
        super()._on_set_sim_job(sim, job_type)
        self._employee_sim_info = sim.sim_info
        self._update_work_buffs(from_load=True)

    def _on_go_home(self):
        self._self_destruct()
        if self._employee_sim_info is not None:
            employee_sim = self._employee_sim_info.get_sim_instance()
            if employee_sim is not None:
                services.get_zone_situation_manager().make_sim_leave_now_must_run(employee_sim)

    @property
    def _is_clocked_in(self):
        retail_manager = RetailUtils.get_retail_manager_for_zone()
        if retail_manager is None:
            return False
        return retail_manager.is_employee_clocked_in(self._employee_sim_info)

    def _clock_in(self):
        retail_manager = RetailUtils.get_retail_manager_for_zone()
        retail_manager.on_employee_clock_in(self._employee_sim_info)

    def _clock_out(self):
        if self._is_clocked_in:
            retail_manager = RetailUtils.get_retail_manager_for_zone()
            retail_manager.on_employee_clock_out(self._employee_sim_info)
        self._remove_work_buffs()

    def _start_work_duration(self):
        self._clock_in()
        self.change_duration(self._original_duration)
        self._update_work_buffs()

    def _choose_next_state(self):
        valid_states = [self.state_socialize, self.state_restock, self.state_clean, self.state_ring_up_customers]
        random_state = random.choice(valid_states)
        return random_state()

    def _update_work_buffs(self, *_, from_load=False, **__):
        self._remove_work_buffs()
        if not self._is_clocked_in:
            return
        previous_duration = None
        elapsed_duration = self._original_duration - self._get_remaining_time_in_minutes()
        for (duration_percent, buff) in sorted(self.total_work_time_buffs.items(), key=operator.itemgetter(0), reverse=True):
            required_duration = duration_percent*self._original_duration
            if elapsed_duration >= required_duration:
                self._work_buff_handle = self._employee_sim_info.add_buff(buff.buff_type, buff_reason=buff.buff_reason, from_load=from_load)
                break
            previous_duration = required_duration - elapsed_duration
        if previous_duration is not None:
            alarm_duration = create_time_span(minutes=previous_duration)
            self._work_alarm_handle = alarms.add_alarm(self, alarm_duration, self._update_work_buffs)

    def _remove_work_buffs(self):
        if self._work_alarm_handle is not None:
            alarms.cancel_alarm(self._work_alarm_handle)
        if self._work_buff_handle is not None:
            self._employee_sim_info.remove_buff(self._work_buff_handle)

class _GoToStoreState(SituationState):
    __qualname__ = '_GoToStoreState'

    def on_activate(self, reader=None):
        super().on_activate(reader)
        self.owner._set_job_role_state(self.owner.employee_job, self.owner.role_state_go_to_store)
        self._test_event_register(TestEvent.InteractionComplete)

    def on_deactivate(self):
        employee_sim_info = self.owner._employee_sim_info
        if employee_sim_info is not None:
            sim_instance = employee_sim_info.get_sim_instance()
            if sim_instance is not None and not sim_instance.is_being_destroyed and not self.owner._is_clocked_in:
                self.owner._start_work_duration()
        return super().on_deactivate()

    def handle_event(self, sim_info, event, resolver):
        if self.owner is None:
            return
        if event == TestEvent.InteractionComplete and self.owner._employee_sim_info is sim_info and resolver(self.owner.go_to_store_interaction):
            self.owner._start_work_duration()
            self._change_state(self.owner._choose_next_state())

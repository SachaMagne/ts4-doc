from protocolbuffers import Consts_pb2
from retail.retail_utils import RetailUtils
from sims.funds import _Funds
from sims4.tuning.dynamic_enum import DynamicEnum
import services
import sims4.log
logger = sims4.log.Logger('Retail Funds', default_owner='trevor')

class RetailFundsCategory(DynamicEnum):
    __qualname__ = 'RetailFundsCategory'
    NONE = 0

class RetailFunds(_Funds):
    __qualname__ = 'RetailFunds'

    def __init__(self, retail_manager, starting_funds):
        super().__init__(retail_manager.owner_household, starting_funds)
        self._retail_manager = retail_manager

    def _send_money_update_internal(self, household, vfx_amount, sim=None, reason=0):
        if RetailUtils.get_retail_manager_for_zone() is self._retail_manager:
            self._retail_manager.send_retail_funds_update()

    def send_money_update(self, vfx_amount, sim=None, reason=0):
        self._send_money_update_internal(self._retail_manager.owner_household, vfx_amount, sim, reason)

    @property
    def funds_transfer_gain_reason(self):
        return Consts_pb2.FUNDS_RETAIL_TRANSFER_GAIN

    @property
    def funds_transfer_loss_reason(self):
        return Consts_pb2.FUNDS_RETAIL_TRANSFER_LOSS

    @property
    def allow_negative_funds(self):
        return True

    def add(self, amount, reason, tags=None):
        amount = round(amount)
        if amount < 0:
            logger.error('Attempt to add negative amount of money to retail funds.')
            return
        self._update_money(amount, reason, tags=tags, count_as_earnings=False)
        if reason != self.funds_transfer_gain_reason:
            pass

    def try_remove(self, amount, *args, funds_category=RetailFundsCategory.NONE, **kwargs):
        reserved_funds = super().try_remove(amount, *args, **kwargs)
        if reserved_funds is not None:
            original_apply_function = reserved_funds.get_apply_function()

            def apply_removal(*args, **kwargs):
                if original_apply_function is not None:
                    original_apply_function(*args, **kwargs)
                self._retail_manager.add_to_funds_category(funds_category, amount)

            reserved_funds.set_apply_function(apply_removal)
        return reserved_funds

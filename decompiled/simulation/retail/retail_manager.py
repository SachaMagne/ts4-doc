from _weakrefset import WeakSet
from collections import Counter
from math import floor
from weakref import WeakKeyDictionary
import itertools
from protocolbuffers import Consts_pb2, UI_pb2
from audio.primitive import TunablePlayAudioAllPacks, PlaySound
from bucks.bucks_enums import BucksType
from date_and_time import DateAndTime
from distributor.ops import SetRetailStoreOpen
from distributor.rollback import ProtocolBufferRollback
from distributor.system import Distributor
from interactions.context import InteractionContext
from interactions.priority import Priority
from objects.components.inventory_enums import InventoryType
from objects.lighting.lighting_utils import LightingHelper
from retail.retail_balance_transfer_dialog import RetailBalanceTransferDialog
from retail.retail_funds import RetailFunds, RetailFundsCategory
from retail.retail_summary_dialog import RetailSummaryDialog
from retail.retail_utils import RetailUtils
from sims.outfits.outfit_enums import OutfitCategory
from sims.sim_info_types import Gender
from sims.sim_info_utils import sim_info_auto_finder
from sims4 import math
from sims4.callback_utils import CallableList
from sims4.localization import TunableLocalizedString
from sims4.math import clamp
from sims4.tuning.tunable import TunablePackSafeReference, TunableRange, TunablePackSafeResourceKey, TunableMapping, TunableReference, TunablePercent, TunableEnumEntry, TunableList, TunableTuple, OptionalTunable
from singletons import DEFAULT
from statistics.commodity import Commodity
from ui.ui_dialog import UiDialogOkCancel
from ui.ui_dialog_notification import TunableUiDialogNotificationSnippet
from vfx import PlayEffect
from world.premade_lot_status import PremadeLotStatus
import distributor
import services
import sims4.log
import tag
import telemetry_helper
TELEMETRY_GROUP_RETAIL = 'RETL'
TELEMETRY_HOOK_STORE_PURCHASED = 'RESP'
TELEMETRY_HOOK_NEW_GAME_STORE_PURCHASED = 'RENG'
TELEMETRY_HOOK_STORE_SOLD = 'RESS'
TELEMETRY_HOOK_NPC_STORE_VISITED = 'RENS'
TELEMETRY_HOOK_STORE_CLOSED = 'RESC'
TELEMETRY_HOOK_LENGTH_STORE_OPENED = 'lsto'
TELEMETRY_HOOK_NUM_WORKERS = 'numw'
TELEMETRY_HOOK_AMOUNT_PROFIT = 'prof'
retail_telemetry_writer = sims4.telemetry.TelemetryWriter(TELEMETRY_GROUP_RETAIL)
logger = sims4.log.Logger('Retail', default_owner='trevor')

class RetailManager:
    __qualname__ = 'RetailManager'
    RETAIL_VENUE = TunablePackSafeReference(description='\n        The venue type for retail lots.\n        ', manager=services.get_instance_manager(sims4.resources.Types.VENUE))
    RETAIL_CAREER = TunablePackSafeReference(description='\n        The career that retail employees are hired into.\n        ', manager=services.get_instance_manager(sims4.resources.Types.CAREER))
    EMPLOYEE_SKILLS = TunableMapping(description='\n        A mapping of employee skills to their weight, i.e. how much they\n        contribute, relative to each other, to the initial and desired career\n        level for a specific employee.\n        ', key_type=TunableReference(description='\n            The employee skill.\n            ', manager=services.get_instance_manager(sims4.resources.Types.STATISTIC), class_restrictions=('Skill',), pack_safe=True), value_type=TunablePercent(description="\n            The weight of this skill's completion level. This is used to compute\n            a weighted average of skill completion.\n            \n            e.g. If all skills are maxed out, and their weights are all 1, the\n            average completion is 100%.\n            \n            If all skills are maxed out, and there are two skills, one of which\n            has a 50% weight, the average completion is 75%.\n            ", default=100))
    EMPLOYEE_SKILL_TO_CAREER_LEVEL_RATIO = TunableRange(description='\n        The ratio between the computed weighted average of the employee skills\n        and the desired career level within the retail career.\n        \n        e.g. All skills are equally weighted, and the average skill level is 50%\n        completion.\n        \n        If this value is 100%, then the employee will start in (and desire to\n        be), at 50% progression within the career.\n        \n        If this value is 50%, then the employee will start in (and desire to\n        be), at 25% progression within the career.\n        ', tunable_type=float, default=1, minimum=0)
    EMPLOYEE_CAREER_LEVEL_BUFFS = TunableMapping(description="\n        A dictionary mapping the difference in career level and desired career\n        level to a buff that is applied when such condition exists for a given\n        employee.\n        \n        e.g. A Sim is hired at their desired career level. They would be awarded\n        the buff corresponding to entry '0'.\n        \n        e.g.\n        A Sim is hired as a level 2 employee but desires to be a level 5\n        employee. The would be awarded the buff corresponding to entry '-3'.\n        ", key_type=int, value_type=TunableReference(description='\n            The buff to be awarded when the specified condition exists.\n            ', manager=services.get_instance_manager(sims4.resources.Types.BUFF), pack_safe=True))
    EMPLOYEE_SATISFACTION_COMMODITY = TunablePackSafeReference(description='\n        The commodity representing employee satisfaction. Its states are used to\n        populate the Retail UI with textual cues.\n        ', manager=services.get_instance_manager(sims4.resources.Types.STATISTIC), class_restrictions=('Commodity',))
    EMPLOYEE_POOL_FILTER = TunablePackSafeReference(description='\n        The filter to use to find Sims for hire.\n        ', manager=services.get_instance_manager(sims4.resources.Types.SIM_FILTER))
    EMPLOYEE_POOL_SIZE = TunableRange(description='\n        How many employees should be available for hire. Existing Sims will be\n        available, but if no existing Sim is available, Sims are created to\n        match the specified filter.\n        ', tunable_type=int, default=6, minimum=1)
    EMPLOYEE_UNIFORM_POSE = TunablePackSafeReference(description='\n        The pose that mannequins in CAS are in when designing employee uniforms.\n        ', manager=services.get_instance_manager(sims4.resources.Types.ANIMATION), class_restrictions=('ObjectPose',))
    EMPLOYEE_UNIFORM_MALE = TunablePackSafeResourceKey(description='\n        The SimInfo file to use to edit male employee uniforms.\n        ', default=None, resource_types=(sims4.resources.Types.SIMINFO,))
    EMPLOYEE_UNIFORM_FEMALE = TunablePackSafeResourceKey(description='\n        The SimInfo file to use to edit female employee uniforms.\n        ', default=None, resource_types=(sims4.resources.Types.SIMINFO,))
    EMPLOYEE_INTERACTION_HIRE = TunablePackSafeReference(description='\n        The interaction to run when hiring employees via UI.\n        ', manager=services.get_instance_manager(sims4.resources.Types.INTERACTION))
    EMPLOYEE_INTERACTION_FIRE = TunablePackSafeReference(description='\n        The interaction to run when firing employees via UI.\n        ', manager=services.get_instance_manager(sims4.resources.Types.INTERACTION))
    EMPLOYEE_INTERACTION_PROMOTE = TunablePackSafeReference(description='\n        The interaction to run when promoting employees via UI.\n        ', manager=services.get_instance_manager(sims4.resources.Types.INTERACTION))
    EMPLOYEE_INTERACTION_DEMOTE = TunablePackSafeReference(description='\n        The interaction to run when demoting employees via UI.\n        ', manager=services.get_instance_manager(sims4.resources.Types.INTERACTION))
    DEFAULT_MARKUP_MULTIPLIER = TunableRange(description="\n        The default markup multipler for a brand new retail store. Note: Tuning\n        this to 0 means everything in the store is free. Don't tune 0. This\n        also MUST match a multiplier that is in the Markup Multipliers field.\n        ", tunable_type=float, default=1.2, minimum=math.EPSILON)
    MARKUP_MULTIPLIER_DATA = TunableList(description='\n        A list of markup multiplier display names and the actual multiplier\n        associated with that name. This is used strictly for sending the markup\n        information to the UI. Ideally, these would also be used to generate\n        the pie menu but because of time limitations these need to match all of\n        the markup interactions.\n        ', tunable=TunableTuple(description='\n            A tuple of the markup multiplier display name and the actual\n            multiplier associated with that display name.\n            ', name=TunableLocalizedString(description='\n                The display name for this markup multiplier.\n                '), markup_multiplier=TunableRange(description='\n                The multiplier associated with this display name.\n                ', tunable_type=float, default=1, minimum=math.EPSILON)))
    NPC_STARTING_FUNDS = TunableRange(description='\n        The amount of funds an NPC retail lot starts with.\n        ', tunable_type=int, default=1000, minimum=0)
    NPC_STORE_FOR_SALE_TAG = TunableEnumEntry(description='\n        Objects with this tag will be set For Sale when an NPC store is\n        visited.\n        ', tunable_type=tag.Tag, default=tag.Tag.INVALID, pack_safe=True)
    NPC_STORE_MANNEQUIN_TAG = TunableEnumEntry(description='\n        Objects with this tag will have their mannequin component outfits\n        restocked any time a premade NPC store is visited.\n        ', tunable_type=tag.Tag, default=tag.Tag.INVALID, pack_safe=True)
    RETAIL_BUCKS_REFERENCE = TunableEnumEntry(description='\n        A reference to the BucksType enum entry used in this system to unlock\n        Retail Perks.\n        ', tunable_type=BucksType, default=BucksType.INVALID, pack_safe=True)
    FOR_SALE_VFX = PlayEffect.TunableFactory(description='\n        An effect that can be toggled on/off for all objects marked for sale.\n        ')
    MANAGE_OUTFIT_AFFORDANCES = TunableList(description='\n        A list of affordances that are shown when the player clicks on the\n        Manage Outfits button.\n        ', tunable=TunableReference(description='\n            An affordance shown when the player clicks on the manage\n            outfits button.\n            ', manager=services.affordance_manager(), pack_safe=True))
    ADVERTISING_COMMODITIES = TunableList(description='\n        The list of commodities that can be added to the retail lot for\n        advertising.\n        ', tunable=Commodity.TunableReference(pack_safe=True))
    LIGHTING_HELPER_OPEN = LightingHelper.TunableFactory(description='\n        The lighting helper to execute when the store is opened.\n        \n        e.g. Turn on all neon signs.\n        ')
    LIGHTING_HELPER_CLOSE = LightingHelper.TunableFactory(description='\n        The lighting helper to execute when the store is closed.\n        \n        e.g. Turn off all neon signs.\n        ')
    AUDIO_STING_OPEN = TunablePlayAudioAllPacks(description='\n        The audio sting to play when the store opens.\n        ')
    AUDIO_STING_CLOSE = TunablePlayAudioAllPacks(description='\n        The audio sting to play when the store closes.\n        ')
    RETAIL_FUNDS_CATEGORY_DATA = TunableMapping(description='\n        Define data associated with specific retail funds categories.\n        ', key_type=TunableEnumEntry(description='\n            The retail funds category.\n            ', tunable_type=RetailFundsCategory, default=RetailFundsCategory.NONE), value_type=TunableTuple(description='\n            The data associated with this retail funds category.\n            ', summary_dialog_entry=OptionalTunable(description='\n                If enabled, an entry for this category is displayed in the\n                summary dialog.\n                ', tunable=TunableLocalizedString(description='\n                    The dialog entry for this retail funds category. This string\n                    takes no tokens.\n                    '))))
    SELL_STORE_DIALOG = UiDialogOkCancel.TunableFactory(description='\n         This dialog is to confirm the sale of the store\n         ')
    ITEMS_SENT_TO_HH_INVENTORY_NOTIFICATION = TunableUiDialogNotificationSnippet(description="\n            The notification that shows up when items are sent to the household's\n            inventory because the item that these things are slotted to are\n            sold.\n            ")
    RETAIL_INVENTORY_TYPES = TunableList(description='\n        A reference to the inventory types of retail inventories. If a retail\n        item gets put into any inventory type in this list, it will\n        automatically be marked for sale.\n        ', tunable=TunableEnumEntry(description='\n            The inventory type.\n            ', tunable_type=InventoryType, default=InventoryType.UNDEFINED, pack_safe=True))
    OPEN_STORE_NOTIFICATION = TunableUiDialogNotificationSnippet(description='\n        The notification that shows up when the player opens the store. We\n        need to trigger this from code because we need the notification to \n        show up when we open the store through the UI and through the \n        openstore interaction. \n        ')
    NO_ITEM_FOR_SALE_WHEN_OPEN_NOTIFICATION = TunableUiDialogNotificationSnippet(description='\n        The notification that shows up when the player opens an empty store. It\n        will replace the OPEN_STORE_NOTIFICATION if there are not items for\n        sale in the store while opening.\n        ')
    NPC_STORE_ITEM_COMMODITIES_TO_MAX_ON_OPEN = TunableList(description='\n        A list of commodities that should get maxed out on retail objects\n        when an NPC store opens.\n        ', tunable=Commodity.TunableReference(description='\n            The commodity to max out.\n            ', pack_safe=True))

    def __init__(self, owner_household, zone_id):
        self._owner_household = owner_household
        self._zone_id = zone_id
        self.on_store_closed = CallableList()
        self._employee_sim_ids = None
        self._employees = WeakSet()
        self._employee_uniform_data_male = None
        self._employee_uniform_data_female = None
        starting_funds = self.NPC_STARTING_FUNDS if self._owner_household is None else 0
        self._funds = RetailFunds(self, starting_funds)
        self._funds_category_tracker = Counter()
        self._clear_employee_payroll()
        self._employee_career_level_buffs = WeakKeyDictionary()
        self._grand_opening = self._owner_household is not None
        self._is_open = False
        self._open_time = None
        self._daily_items_sold = 0
        self._daily_revenue = 0
        self._daily_employee_wages = 0
        self._daily_funds = 0
        self._markup_multiplier = self.DEFAULT_MARKUP_MULTIPLIER
        self._objs_with_for_sale_vfx = {}
        self._for_sale_vfx_toggle_value = False

    def _clear_employee_payroll(self):
        self._employee_payroll = dict()

    def save_data(self, retail_data):
        retail_data.is_open = self._is_open
        if self._is_open:
            retail_data.open_time = self._open_time.absolute_ticks()
        retail_data.markup = self._markup_multiplier
        retail_data.funds = self._funds.money
        if self._employee_sim_ids:
            retail_data.employee_ids.extend(self._employee_sim_ids)
        else:
            retail_data.employee_ids.extend([s.id for s in self._employees])
        retail_data.grand_opening = self._grand_opening
        retail_data.daily_revenue = self._daily_revenue
        retail_data.daily_items_sold = self._daily_items_sold
        retail_data.daily_employee_wages = self._daily_employee_wages
        if self._employee_uniform_data_male is not None:
            retail_data.employee_uniform_data_male.mannequin_id = self._employee_uniform_data_male.sim_id
            self._employee_uniform_data_male.save_sim_info(retail_data.employee_uniform_data_male)
        if self._employee_uniform_data_female is not None:
            retail_data.employee_uniform_data_female.mannequin_id = self._employee_uniform_data_female.sim_id
            self._employee_uniform_data_female.save_sim_info(retail_data.employee_uniform_data_female)
        for (sim_id, (clock_in_time, payroll_data)) in self._employee_payroll.items():
            with ProtocolBufferRollback(retail_data.employee_payroll) as payroll_msg:
                payroll_msg.sim_id = sim_id
                payroll_msg.clock_in_time = clock_in_time.absolute_ticks() if clock_in_time is not None else 0
                for (career_level, hours_worked) in payroll_data.items():
                    with ProtocolBufferRollback(payroll_msg.payroll_data) as payroll_entry_msg:
                        payroll_entry_msg.career_level_guid = career_level.guid64
                        payroll_entry_msg.hours_worked = hours_worked
        for (funds_category, amount) in self._funds_category_tracker.items():
            with ProtocolBufferRollback(retail_data.funds_category_tracker_data) as funds_category_msg:
                funds_category_msg.funds_category = funds_category
                funds_category_msg.amount = amount

    def load_data(self, retail_data):
        self._is_open = retail_data.is_open
        if self._is_open:
            self._open_time = DateAndTime(retail_data.open_time)
            self._distribute_store_open_status(self._is_open, self._open_time.absolute_ticks())
        else:
            self._distribute_store_open_status(self._is_open)
        self._markup_multiplier = retail_data.markup
        self._funds = RetailFunds(self, starting_funds=retail_data.funds)
        self._employee_sim_ids = list(retail_data.employee_ids)
        self._grand_opening = retail_data.grand_opening
        self._daily_revenue = retail_data.daily_revenue
        self._daily_items_sold = retail_data.daily_items_sold
        self._daily_employee_wages = retail_data.daily_employee_wages
        persistence_service = services.get_persistence_service()
        if retail_data.HasField('employee_uniform_data_male'):
            employee_uniform_data_male = retail_data.employee_uniform_data_male
            self._employee_uniform_data_male = self.get_employee_uniform_data(Gender.MALE, sim_id=employee_uniform_data_male.mannequin_id)
            if persistence_service is not None:
                persisted_data = persistence_service.get_mannequin_proto_buff(employee_uniform_data_male.mannequin_id)
                if persisted_data is not None:
                    employee_uniform_data_male = persisted_data
            self._employee_uniform_data_male.load_sim_info(employee_uniform_data_male)
            self._send_employee_uniform_data(self._employee_uniform_data_male)
        if retail_data.HasField('employee_uniform_data_female'):
            employee_uniform_data_female = retail_data.employee_uniform_data_female
            self._employee_uniform_data_female = self.get_employee_uniform_data(Gender.FEMALE, sim_id=employee_uniform_data_female.mannequin_id)
            if persistence_service is not None:
                persisted_data = persistence_service.get_mannequin_proto_buff(employee_uniform_data_female.mannequin_id)
                if persisted_data is not None:
                    employee_uniform_data_female = persisted_data
            self._employee_uniform_data_female.load_sim_info(employee_uniform_data_female)
            self._send_employee_uniform_data(self._employee_uniform_data_female)
        career_level_manager = services.get_instance_manager(sims4.resources.Types.CAREER_LEVEL)
        for payroll_msg in retail_data.employee_payroll:
            payroll_data = Counter()
            for payroll_entry_msg in payroll_msg.payroll_data:
                career_level = career_level_manager.get(payroll_entry_msg.career_level_guid)
                if career_level is None:
                    pass
                payroll_data[career_level] = payroll_entry_msg.hours_worked
            if not payroll_data:
                pass
            clock_in_time = DateAndTime(payroll_msg.clock_in_time) if payroll_msg.clock_in_time else None
            self._employee_payroll[payroll_msg.sim_id] = (clock_in_time, payroll_data)
        for funds_category_msg in retail_data.funds_category_tracker_data:
            self._funds_category_tracker[funds_category_msg.funds_category] = funds_category_msg.amount

    def on_loading_screen_animation_finished(self):
        if self._grand_opening:
            self._grand_opening = False
            RetailBalanceTransferDialog.show_dialog(first_time_buyer=True)

    def on_all_households_and_sim_infos_loaded(self):
        sim_info_manager = services.sim_info_manager()
        for employee_id in self._employee_sim_ids:
            sim_info = sim_info_manager.get(employee_id)
            while sim_info is not None:
                self._employees.add(sim_info)
        self._employee_sim_ids = None
        self.update_employees()
        if self.should_close_after_load():
            self._close_store(play_sound=False)
        if not self.is_active_household_and_zone:
            return
        if self.is_open:
            for sim_info in self.get_employees_on_payroll_gen():
                if self.get_employee_career(sim_info) is None:
                    self.on_employee_clock_out(sim_info)
                (clock_in_time, _) = self._employee_payroll[sim_info.sim_id]
                while clock_in_time is not None:
                    self._register_employee_callbacks(sim_info)
                    self._add_employee_career_buff(sim_info)
        for obj in RetailUtils.get_all_retail_objects():
            self._fixup_placard_if_necessary(obj)
        self._funds._send_money_update_internal(self.owner_household, 0)
        self.send_daily_profit_and_cost_update()
        self.send_daily_items_sold_update()

    def should_close_after_load(self):
        game_clock_service = services.game_clock_service()
        if game_clock_service.time_has_passed_in_world_since_zone_save():
            return True
        current_zone = services.current_zone()
        if current_zone.active_household_changed_between_save_and_load() and services.active_household() is self._owner_household:
            return True
        return False

    def on_client_disconnect(self):
        self.remove_for_sale_vfx_from_all_objects()

    def on_build_buy_enter(self):
        self.send_retail_funds_update()

    def on_build_buy_exit(self):
        if self._owner_household is not None:
            self._owner_household.funds.send_money_update(vfx_amount=0)

    @property
    def is_open(self):
        return self._is_open

    @property
    def minutes_open(self):
        time_span = self._get_timespan_since_open()
        if time_span is None:
            return 0
        return time_span.in_minutes()

    @property
    def hours_open(self):
        time_span = self._get_timespan_since_open()
        if time_span is None:
            return 0
        return time_span.in_hours()

    @property
    def daily_items_sold(self):
        return self._daily_items_sold

    @daily_items_sold.setter
    def daily_items_sold(self, value):
        self._daily_items_sold = value
        self.send_daily_items_sold_update()

    @property
    def daily_revenue(self):
        return self._daily_revenue

    @daily_revenue.setter
    def daily_revenue(self, value):
        self._daily_revenue = value
        if self._owner_household is not None:
            self.send_daily_profit_and_cost_update()

    @property
    def daily_funds(self):
        return self._daily_funds

    @daily_funds.setter
    def daily_funds(self, value):
        self._daily_funds = value

    @property
    def funds(self):
        return self._funds

    def add_to_funds_category(self, funds_category, amount):
        if not self.is_open:
            return
        self._funds_category_tracker[funds_category] += amount
        self.send_daily_profit_and_cost_update()

    def get_funds_category_entries_gen(self):
        for (funds_category, amount) in self._funds_category_tracker.items():
            funds_category_data = self.RETAIL_FUNDS_CATEGORY_DATA.get(funds_category)
            while funds_category_data is not None and funds_category_data.summary_dialog_entry is not None:
                yield (funds_category_data.summary_dialog_entry, amount)

    def get_curb_appeal(self):
        total_curb_appeal = sum(obj.retail_component.get_current_curb_appeal() for obj in RetailUtils.get_all_retail_objects())
        lot = services.active_lot()
        for ad_commodity in self.ADVERTISING_COMMODITIES:
            lot_commodity = lot.commodity_tracker.get_statistic(ad_commodity)
            while lot_commodity is not None:
                total_curb_appeal += lot_commodity.get_value()
        return total_curb_appeal

    @property
    def employee_count(self):
        return len(self._employees)

    @property
    def markup_multiplier(self):
        return self._markup_multiplier

    @property
    def owner_household(self):
        return self._owner_household

    @property
    def owner_household_id(self):
        return self._owner_household.id

    @property
    def is_owner_household_active(self):
        if self._owner_household is None:
            return False
        return self._owner_household.id == services.active_household_id()

    @property
    def is_active_household_and_zone(self):
        return self.is_owner_household_active and (self._zone_id is not None and self._zone_id == services.current_zone_id())

    def is_household_owner(self, household_id):
        if self._owner_household is None:
            return False
        return self._owner_household.id == household_id

    def send_retail_funds_update(self):
        if self.is_active_household_and_zone:
            op = distributor.ops.SetRetailFunds(self.funds.money)
            distributor.ops.record(self._owner_household, op)

    def send_daily_profit_and_cost_update(self):
        if self.is_active_household_and_zone:
            op = distributor.ops.SetRetailDailyNetProfit(self.get_daily_net_profit())
            distributor.ops.record(self._owner_household, op)
            op = distributor.ops.SetRetailDailyOutgoingCosts(self.get_daily_outgoing_costs())
            distributor.ops.record(self._owner_household, op)

    def set_markup_multiplier(self, multiplier):
        valid_multipliers = [entry.markup_multiplier for entry in self.MARKUP_MULTIPLIER_DATA]
        if multiplier in valid_multipliers:
            self._markup_multiplier = multiplier
            self.send_markup_multiplier_message()
        else:
            logger.error('Tried setting the markup multiplier to an invalid multiplier. Invalid multiplier is {}. Valid multipliers are {}.', multiplier, valid_multipliers)

    def send_markup_multiplier_message(self):
        if not self.is_active_household_and_zone:
            return
        markup_msg = UI_pb2.RetailMarkupMultiplierMessage()
        for markup_multiplier in self.MARKUP_MULTIPLIER_DATA:
            with ProtocolBufferRollback(markup_msg.markup_multipliers) as multiplier_entry:
                multiplier_entry.name = markup_multiplier.name
                multiplier_entry.multiplier = markup_multiplier.markup_multiplier
                multiplier_entry.is_selected = markup_multiplier.markup_multiplier == self._markup_multiplier
        op = distributor.shared_messages.create_message_op(markup_msg, Consts_pb2.MSG_RETAIL_MARKUP_MULTIPLIER)
        Distributor.instance().add_op_with_no_owner(op)

    def send_daily_items_sold_update(self):
        if self.is_active_household_and_zone:
            op = distributor.ops.SetRetailDailyItemsSold(self.daily_items_sold)
            distributor.ops.record(self._owner_household, op)

    def get_daily_outgoing_costs(self):
        return self._daily_employee_wages + sum(self._funds_category_tracker.values())

    def get_daily_net_profit(self):
        return self.daily_revenue - self.get_daily_outgoing_costs()

    @markup_multiplier.setter
    def markup_multiplier(self, val):
        self._markup_multiplier = max(0, val)

    def _get_timespan_since_open(self):
        if not self.is_open:
            return
        return services.game_clock_service().now() - self._open_time

    def _distribute_store_open_status(self, is_open, open_time=0):
        if self.is_active_household_and_zone:
            op = SetRetailStoreOpen(is_open, open_time)
            Distributor.instance().add_op_with_no_owner(op)

    def try_open_npc_store(self):
        if self.owner_household is None:
            self._open_pure_npc_store(services.active_lot().get_premade_status() == PremadeLotStatus.IS_PREMADE)
        elif not self.is_owner_household_active:
            self._open_household_owned_npc_store()

    def has_any_object_for_sale(self):
        for retail_obj in RetailUtils.all_retail_objects_gen(allow_not_for_sale=True):
            if retail_obj.is_in_inventory():
                return True
            while retail_obj.retail_component.is_for_sale:
                return True
        return False

    def _open_pure_npc_store(self, is_premade):
        has_retail_obj = False
        for retail_obj in RetailUtils.all_retail_objects_gen(allow_not_for_sale=True):
            self._adjust_commodities_if_necessary(retail_obj)
            obj_retail_component = retail_obj.retail_component
            if obj_retail_component.is_for_sale or retail_obj.is_in_inventory():
                has_retail_obj = True
            if is_premade:
                set_for_sale = retail_obj.has_tag(self.NPC_STORE_FOR_SALE_TAG)
            else:
                set_for_sale = obj_retail_component.is_sold
            if set_for_sale:
                obj_retail_component.set_for_sale()
                has_retail_obj = True
            while retail_obj.has_tag(self.NPC_STORE_MANNEQUIN_TAG):
                self._set_up_mannequin_during_open(retail_obj)
                has_retail_obj = True
        self.set_open(has_retail_obj)

    @classmethod
    def _set_up_mannequin_during_open(self, mannequin):
        (current_outfit_type, _) = mannequin.get_current_outfit()
        if current_outfit_type == OutfitCategory.BATHING:
            mannequin.set_current_outfit(mannequin.get_previous_outfit())

    def _open_household_owned_npc_store(self):
        should_open = False
        for retail_obj in RetailUtils.all_retail_objects_gen(allow_not_for_sale=True):
            self._adjust_commodities_if_necessary(retail_obj)
            while not should_open and not retail_obj.retail_component.is_not_for_sale:
                should_open = True
        self.set_open(should_open)

    @classmethod
    def _adjust_commodities_if_necessary(cls, obj):
        for obj_commodity in cls.NPC_STORE_ITEM_COMMODITIES_TO_MAX_ON_OPEN:
            tracker = obj.get_tracker(obj_commodity)
            while tracker is not None and tracker.has_statistic(obj_commodity):
                tracker.set_max(obj_commodity)

    def set_open(self, is_open):
        if self._is_open == is_open:
            return
        if is_open:
            self._open_store()
        else:
            self._close_store()
        owned_by_active_household = self.is_active_household_and_zone
        for obj in RetailUtils.all_retail_objects_gen(allow_not_for_sale=True):
            obj.update_component_commodity_flags()
            while not owned_by_active_household:
                self._fixup_placard_if_necessary(obj)

    def _fixup_placard_if_necessary(self, obj):
        obj_retail_component = obj.retail_component
        if obj_retail_component is not None and obj_retail_component.is_sold:
            obj_retail_component._change_to_placard(play_vfx=False)

    def _open_store(self):
        self._is_open = True
        self._clear_variables()
        self._open_time = services.time_service().sim_now
        if self._owner_household is not None:
            zone_director = services.venue_service().get_zone_director()
            zone_director.start_employee_situations(self._employees, owned_by_npc=not self.is_owner_household_active)
            self._owner_household.bucks_tracker.activate_stored_temporary_perk_timers_of_type(self.RETAIL_BUCKS_REFERENCE)
        self.send_daily_profit_and_cost_update()
        self.daily_funds = 0
        self._distribute_store_open_status(True, self._open_time.absolute_ticks())
        self.LIGHTING_HELPER_OPEN.execute_lighting_helper(self)
        sound = PlaySound(services.get_active_sim(), self.AUDIO_STING_OPEN.instance)
        sound.start()
        with telemetry_helper.begin_hook(retail_telemetry_writer, TELEMETRY_HOOK_NPC_STORE_VISITED, household=self.owner_household):
            pass

    def _close_store(self, play_sound=True):
        for sim_info in itertools.chain(self._employees, self.get_employees_on_payroll_gen()):
            self.on_employee_clock_out(sim_info)
        self._daily_employee_wages = self.get_total_employee_wages()
        self.send_daily_profit_and_cost_update()
        self._send_store_closed_telemetry()
        if self._owner_household is not None:
            self._owner_household.bucks_tracker.deactivate_all_temporary_perk_timers_of_type(self.RETAIL_BUCKS_REFERENCE)
            self._funds.remove(self.get_total_employee_wages(), Consts_pb2.FUNDS_RETAIL_PROFITS)
        self._clear_employee_payroll()
        self._is_open = False
        self.on_store_closed()
        self._distribute_store_open_status(False)
        self.LIGHTING_HELPER_CLOSE.execute_lighting_helper(self)
        if play_sound:
            sound = PlaySound(services.get_active_sim(), self.AUDIO_STING_CLOSE.instance)
            sound.start()

    def _clear_variables(self):
        self._open_time = None
        self._funds_category_tracker.clear()
        self._daily_employee_wages = 0
        self.daily_items_sold = 0
        self.daily_funds = 0
        self.daily_revenue = 0

    def refresh_for_sale_vfx_for_object(self, obj):
        if obj.retail_component is None:
            logger.error('Attempting to toggle for sale vfx on an object {} with no retail component.', obj, owner='tastle')
            return
        show_vfx = self._for_sale_vfx_toggle_value if obj.retail_component.is_for_sale else False
        self._update_for_sale_vfx_for_object(obj, show_vfx)

    def _update_for_sale_vfx_for_object(self, obj, toggle_value):
        if obj not in self._objs_with_for_sale_vfx and toggle_value:
            self._objs_with_for_sale_vfx[obj] = self.FOR_SALE_VFX(obj)
            self._objs_with_for_sale_vfx[obj].start()
        elif obj in self._objs_with_for_sale_vfx and not toggle_value:
            obj_vfx = self._objs_with_for_sale_vfx.pop(obj)
            obj_vfx.stop()

    def remove_for_sale_vfx_from_all_objects(self):
        self._for_sale_vfx_toggle_value = False
        for obj_vfx in self._objs_with_for_sale_vfx.values():
            obj_vfx.stop()
        self._objs_with_for_sale_vfx.clear()

    def toggle_for_sale_vfx(self):
        self._for_sale_vfx_toggle_value = not self._for_sale_vfx_toggle_value
        for item in RetailUtils.all_retail_objects_gen(allow_sold=False, include_inventories=False):
            self._update_for_sale_vfx_for_object(item, self._for_sale_vfx_toggle_value)

    def set_price_type(self, price_type):
        self._price_type = price_type

    def get_value_with_markup(self, value):
        return value*self.markup_multiplier

    def add_employee(self, sim_info):
        career = self.RETAIL_CAREER(sim_info)
        career_location = career.get_career_location()
        career_location.set_zone_id(services.current_zone_id())
        career_level = self.get_employee_desired_career_level(sim_info) + 1
        sim_info.career_tracker.add_career(career, user_level_override=career_level)
        sim_info.add_statistic(self.EMPLOYEE_SATISFACTION_COMMODITY, self.EMPLOYEE_SATISFACTION_COMMODITY.initial_value)
        self._employees.add(sim_info)

    def remove_employee(self, sim_info):
        zone_director = services.venue_service().get_zone_director()
        if zone_director is not None:
            zone_director.on_remove_employee(sim_info)
        if self.is_open:
            self.on_employee_clock_out(sim_info)
        sim_info.career_tracker.remove_career(self.RETAIL_CAREER.guid64)
        sim_info.remove_statistic(self.EMPLOYEE_SATISFACTION_COMMODITY)
        self._employees.discard(sim_info)

    def update_employees(self):
        for employee_sim_info in tuple(self.get_employees_gen()):
            while self.get_employee_career(employee_sim_info) is None:
                self.remove_employee(employee_sim_info)

    def get_employee_count(self):
        return len(self._employees)

    def get_employees_gen(self):
        yield self._employees

    @sim_info_auto_finder
    def get_employees_on_payroll_gen(self):
        yield self._employee_payroll

    def get_employee_career(self, sim_info):
        return sim_info.career_tracker.get_career_by_uid(self.RETAIL_CAREER.guid64)

    def get_employee_career_level(self, sim_info):
        career = self.get_employee_career(sim_info)
        if career is not None:
            return career.current_level_tuning

    def get_employee_desired_career_level(self, sim_info):
        skill_completion = sum(weight*sim_info.get_stat_value(skill)/skill.get_max_skill_value() for (skill, weight) in self.EMPLOYEE_SKILLS.items())/len(self.EMPLOYEE_SKILLS)
        max_career_level = len(self.RETAIL_CAREER.start_track.career_levels) - 1
        career_level = clamp(0, floor(self.EMPLOYEE_SKILL_TO_CAREER_LEVEL_RATIO*skill_completion*max_career_level), max_career_level)
        return career_level

    def potential_manage_outfit_interactions_gen(self, context, **kwargs):
        for affordance in RetailManager.MANAGE_OUTFIT_AFFORDANCES:
            for aop in affordance.potential_interactions(context.sim, context, **kwargs):
                yield aop

    def get_total_employee_wages(self):
        return sum(self.get_employee_wages(sim_info) for sim_info in self.get_employees_on_payroll_gen())

    def get_employee_wages(self, sim_info):
        if not self.is_open:
            return 0
        if sim_info.sim_id not in self._employee_payroll:
            return 0
        (clock_in_time, payroll_data) = self._employee_payroll[sim_info.sim_id]
        total_wages = sum(career_level.simoleons_per_hour*round(hours_worked) for (career_level, hours_worked) in payroll_data.items())
        if clock_in_time is not None:
            hours_worked = (services.time_service().sim_now - clock_in_time).in_hours()
            total_wages += self.get_employee_career_level(sim_info).simoleons_per_hour*round(hours_worked)
        return math.ceil(total_wages)

    def get_employee_wages_breakdown_gen(self, sim_info):
        if sim_info.sim_id not in self._employee_payroll:
            return
        (clock_in_time, payroll_data) = self._employee_payroll[sim_info.sim_id]
        for (career_level, hours_worked) in payroll_data.items():
            if clock_in_time is not None and career_level is self.get_employee_career_level(sim_info):
                hours_worked += (services.time_service().sim_now - clock_in_time).in_hours()
            yield (career_level, round(hours_worked))

    def is_employee(self, sim_info):
        if self._employee_sim_ids is not None:
            return sim_info.sim_id in self._employee_sim_ids
        return sim_info in self._employees

    def is_employee_clocked_in(self, sim_info):
        (clock_in_time, _) = self._employee_payroll.get(sim_info.sim_id, (None, None))
        return clock_in_time is not None

    def on_employee_clock_in(self, sim_info):
        self._register_employee_callbacks(sim_info)
        clock_in_time = services.time_service().sim_now
        if sim_info.sim_id not in self._employee_payroll:
            self._employee_payroll[sim_info.sim_id] = (clock_in_time, Counter())
        (_, payroll_data) = self._employee_payroll[sim_info.sim_id]
        payroll_data[self.get_employee_career_level(sim_info)] += 0
        self._employee_payroll[sim_info.sim_id] = (clock_in_time, payroll_data)
        self._add_employee_career_buff(sim_info)

    def on_employee_clock_out(self, sim_info, career_level=DEFAULT):
        career = self.get_employee_career(sim_info)
        if self.on_employee_promotion in career.on_promoted:
            career.on_promoted.unregister(self.on_employee_promotion)
        if self.on_employee_demotion in career.on_demoted:
            career.on_demoted.unregister(self.on_employee_demotion)
        if career is not None and self.on_career_removed in career.on_career_removed:
            career.on_career_removed.unregister(self.on_career_removed)
        if sim_info.sim_id not in self._employee_payroll:
            return
        clock_out_time = services.time_service().sim_now
        (clock_in_time, payroll_data) = self._employee_payroll[sim_info.sim_id]
        career_level = self.get_employee_career_level(sim_info) if career_level is DEFAULT else career_level
        if clock_in_time is not None and career_level is not None:
            payroll_data[career_level] += (clock_out_time - clock_in_time).in_hours()
        self._employee_payroll[sim_info.sim_id] = (None, payroll_data)
        self._remove_employee_career_buff(sim_info)

    def _register_employee_callbacks(self, sim_info):
        career = self.get_employee_career(sim_info)
        if self.on_employee_promotion not in career.on_promoted:
            career.on_promoted.register(self.on_employee_promotion)
        if self.on_employee_demotion not in career.on_demoted:
            career.on_demoted.register(self.on_employee_demotion)
        if self.on_career_removed not in career.on_career_removed:
            career.on_career_removed.register(self.on_career_removed)

    def on_employee_promotion(self, sim_info):
        if not self.is_open:
            return
        if sim_info.sim_id not in self._employee_payroll:
            return
        career = self.get_employee_career(sim_info)
        self.on_employee_clock_out(sim_info, career_level=career.previous_level_tuning)
        self.on_employee_clock_in(sim_info)

    def on_employee_demotion(self, sim_info):
        if not self.is_open:
            return
        if sim_info.sim_id not in self._employee_payroll:
            return
        career = self.get_employee_career(sim_info)
        self.on_employee_clock_out(sim_info, career_level=career.next_level_tuning)
        self.on_employee_clock_in(sim_info)

    def on_career_removed(self, sim_info):
        if self.is_employee(sim_info):
            self.remove_employee(sim_info)

    def _add_employee_career_buff(self, sim_info):
        self._remove_employee_career_buff(sim_info)
        career_level = self.get_employee_career(sim_info).level
        desired_career_level = self.get_employee_desired_career_level(sim_info)
        career_level_delta = career_level - desired_career_level
        career_level_buff = self.EMPLOYEE_CAREER_LEVEL_BUFFS.get(career_level_delta)
        if career_level_buff is not None:
            self._employee_career_level_buffs[sim_info] = sim_info.add_buff(career_level_buff)

    def _remove_employee_career_buff(self, sim_info):
        buff_handle = self._employee_career_level_buffs.get(sim_info)
        if buff_handle is not None:
            sim_info.remove_buff(buff_handle)

    def get_employee_uniform_data(self, gender, sim_id=0):
        from sims.sim_info_base_wrapper import SimInfoBaseWrapper
        if gender == Gender.MALE:
            if self._employee_uniform_data_male is None:
                self._employee_uniform_data_male = SimInfoBaseWrapper(sim_id=sim_id)
                self._employee_uniform_data_male.load_from_resource(self.EMPLOYEE_UNIFORM_MALE)
                self._employee_uniform_data_male.set_current_outfit((OutfitCategory.CAREER, 0))
                if not sim_id:
                    self._send_employee_uniform_data(self._employee_uniform_data_male)
            return self._employee_uniform_data_male
        if gender == Gender.FEMALE:
            if self._employee_uniform_data_female is None:
                self._employee_uniform_data_female = SimInfoBaseWrapper(sim_id=sim_id)
                self._employee_uniform_data_female.load_from_resource(self.EMPLOYEE_UNIFORM_FEMALE)
                self._employee_uniform_data_female.set_current_outfit((OutfitCategory.CAREER, 0))
                if not sim_id:
                    self._send_employee_uniform_data(self._employee_uniform_data_female)
            return self._employee_uniform_data_female

    def _send_employee_uniform_data(self, employee_uniform_data):
        employee_uniform_data.manager = services.sim_info_manager()
        Distributor.instance().add_object(employee_uniform_data)

    def run_employee_interaction(self, affordance, target_sim):
        active_sim = services.get_active_sim()
        if active_sim is None:
            return False
        context = InteractionContext(active_sim, InteractionContext.SOURCE_PIE_MENU, Priority.High)
        return active_sim.push_super_affordance(affordance, None, context, picked_item_ids=(target_sim.sim_id,))

    def show_summary_dialog(self):
        RetailSummaryDialog.show_dialog(self)

    def get_median_item_value(self):
        values = [obj.retail_component.get_retail_value() for obj in RetailUtils.all_retail_objects_gen()]
        if not values:
            return 0
        values.sort()
        count = len(values)
        midpoint = count//2
        if count % 2:
            return values[midpoint]
        return (values[midpoint] + values[midpoint - 1])/2

    def modify_funds(self, amount, from_item_sold=True):
        if amount == 0:
            return
        if amount < 0:
            if from_item_sold:
                logger.warn('Trying to deduct funds from the retail manager but claiming an item was sold. This makes no sense and is being ignored.')
                return
            self._funds.remove(-amount, Consts_pb2.FUNDS_RETAIL_PROFITS)
        else:
            self._funds.add(amount, Consts_pb2.FUNDS_RETAIL_PROFITS)
        if from_item_sold:
            pass

    def transfer_balance_to_household(self):
        sim = next(self._owner_household.instanced_sims_gen())
        retail_funds = self._funds.money
        if retail_funds > 0:
            self.owner_household._funds.add(retail_funds, Consts_pb2.FUNDS_RETAIL_PROFITS, sim)
        self._funds.remove(retail_funds, Consts_pb2.FUNDS_RETAIL_PROFITS, sim)

    def _send_store_closed_telemetry(self):
        with telemetry_helper.begin_hook(retail_telemetry_writer, TELEMETRY_HOOK_STORE_CLOSED, household=self.owner_household) as hook:
            hook.write_int(TELEMETRY_HOOK_LENGTH_STORE_OPENED, self.minutes_open)
            hook.write_int(TELEMETRY_HOOK_NUM_WORKERS, self.employee_count)
            hook.write_int(TELEMETRY_HOOK_AMOUNT_PROFIT, self.get_daily_net_profit())

import operator
from protocolbuffers import DistributorOps_pb2
import protocolbuffers
from distributor.ops import GenericProtocolBufferOp
from distributor.rollback import ProtocolBufferRollback, ProtocolBufferRollbackExpected
from distributor.shared_messages import create_icon_info_msg, IconInfoData
from distributor.system import Distributor
from interactions.utils.tunable_icon import TunableIcon
from sims4.localization import TunableLocalizedString, TunableLocalizedStringFactory, LocalizationHelperTuning
import services

class RetailSummaryDialog:
    __qualname__ = 'RetailSummaryDialog'
    DIALOG_ICON = TunableIcon(description='\n        The Icon to show in the header of the dialog.\n        ')
    DIALOG_SUBTITLE = TunableLocalizedString(description="\n        The subtitle for the dialog. The main title will be the store's name.\n        ")
    LINE_ITEM_ITEMS_SOLD_HEADER = TunableLocalizedString(description="\n        The header for the 'Items Sold' line item. By design, this should say\n        'Items Sold:'\n        ")
    LINE_ITEM_ITEMS_SOLD_TEXT = TunableLocalizedStringFactory(description="\n        The text in the 'Items Sold' line item. By design, this should say\n        the number of items sold.\n        {0.Number} = number of items sold since the store was open\n        i.e. {0.Number}\n        ")
    LINE_ITEM_WAGES_OWED_HEADER = TunableLocalizedString(description="\n        The header text for the 'Wages Owned' line item. By design, this\n        should say 'Wages Owed:'\n        ")
    LINE_ITEM_WAGES_OWED_TEXT = TunableLocalizedStringFactory(description="\n        The text in the 'Wages Owed' line item. By design, this should say the\n        number of hours worked and the price per hour.\n        {0.Number} = number of hours worked by all employees\n        {1.Money} = amount employees get paid per hour\n        i.e. {0.Number} hours worked x {1.Money}/hr\n        ")
    LINE_ITEM_PAYROLL_HEADER = TunableLocalizedStringFactory(description='\n        The header text for each unique Sim on payroll. This is provided one\n        token, the Sim.\n        ')
    LINE_ITEM_PAYROLL_TEXT = TunableLocalizedStringFactory(description='\n        The text for each job that the Sim on payroll has held today. This is\n        provided three tokens: the career level name, the career level salary,\n        and the total hours worked.\n        \n        e.g.\n         {0.String} ({1.Money}/hr) * {2.Number} {S2.hour}{P2.hours}\n        ')

    @classmethod
    def show_dialog(cls, retail_manager):
        report_msg = protocolbuffers.Dialog_pb2.RetailSummaryDialog()
        report_msg.name = LocalizationHelperTuning.get_raw_text(services.current_zone().lot.get_lot_name())
        report_msg.subtitle = cls.DIALOG_SUBTITLE
        report_msg.icon = create_icon_info_msg(IconInfoData(cls.DIALOG_ICON))
        report_msg.hours_open = round(retail_manager.hours_open)
        report_msg.total_amount = retail_manager.get_daily_net_profit()
        if retail_manager.is_open:
            pass
        items_sold_line_item = report_msg.line_items.add()
        items_sold_line_item.name = cls.LINE_ITEM_ITEMS_SOLD_HEADER
        items_sold_line_item.item_type = cls.LINE_ITEM_ITEMS_SOLD_TEXT(retail_manager.daily_items_sold)
        items_sold_line_item.value = retail_manager.daily_revenue
        for sim_info in sorted(retail_manager.get_employees_on_payroll_gen(), key=operator.attrgetter('last_name')):
            current_career_level = retail_manager.get_employee_career_level(sim_info)
            with ProtocolBufferRollback(report_msg.line_items) as line_item_msg:
                payroll_entries = []
                for (career_level, hours_worked) in sorted(retail_manager.get_employee_wages_breakdown_gen(sim_info), key=lambda wage: -wage[0].simoleons_per_hour):
                    if not hours_worked and career_level is not current_career_level:
                        pass
                    payroll_entries.append(cls.LINE_ITEM_PAYROLL_TEXT(career_level.title(sim_info), career_level.simoleons_per_hour, hours_worked))
                if not payroll_entries:
                    raise ProtocolBufferRollbackExpected
                line_item_msg.name = cls.LINE_ITEM_PAYROLL_HEADER(sim_info)
                line_item_msg.item_type = LocalizationHelperTuning.get_new_line_separated_strings(*payroll_entries)
                line_item_msg.value = -retail_manager.get_employee_wages(sim_info)
        wages_owed_line_item = report_msg.line_items.add()
        wages_owed_line_item.name = cls.LINE_ITEM_WAGES_OWED_HEADER
        wages_owed_line_item.item_type = cls.LINE_ITEM_WAGES_OWED_TEXT(retail_manager.get_total_employee_wages())
        wages_owed_line_item.value = -retail_manager.get_total_employee_wages()
        for (entry_name, entry_value) in retail_manager.get_funds_category_entries_gen():
            with ProtocolBufferRollback(report_msg.line_items) as line_item_msg:
                line_item_msg.name = LocalizationHelperTuning.get_raw_text('')
                line_item_msg.item_type = entry_name
                line_item_msg.value = -entry_value
        report_op = GenericProtocolBufferOp(DistributorOps_pb2.Operation.RETAIL_SUMMARY_DIALOG, report_msg)
        Distributor.instance().add_op_with_no_owner(report_op)

from _collections import defaultdict
from distributor.rollback import ProtocolBufferRollback
from retail import retail_manager
from retail.retail_manager import RetailManager
import services
import sims4.log
logger = sims4.log.Logger('Retail', default_owner='trevor')

class RetailTracker:
    __qualname__ = 'RetailTracker'
    EMPLOYEE_SLOTS = 1
    EMPLOYEE_SLOT_LIMIT = 3

    def __init__(self, owner_household):
        self._owner_household = owner_household
        self._retail_managers = defaultdict(set)
        self._additional_employee_slots = 0
        self._owner_household.bucks_tracker.add_perk_unlocked_callback(RetailManager.RETAIL_BUCKS_REFERENCE, self._retail_perk_unlocked_callback)

    @property
    def retail_managers(self):
        return self._retail_managers

    @property
    def additional_employee_slots(self):
        return self._additional_employee_slots

    def save_data(self, household_msg):
        household_msg.gameplay_data.additional_employee_slots = self._additional_employee_slots
        for (zone_id, retail_manager) in self._retail_managers.items():
            with ProtocolBufferRollback(household_msg.gameplay_data.retail_data) as retail_data:
                retail_data.retail_zone_id = zone_id
                retail_manager.save_data(retail_data)

    def load_data(self, household_msg):
        self._retail_managers.clear()
        self._additional_employee_slots = household_msg.gameplay_data.additional_employee_slots
        for retail_data in household_msg.gameplay_data.retail_data:
            retail_manager = RetailManager(self._owner_household, retail_data.retail_zone_id)
            retail_manager.load_data(retail_data)
            self._retail_managers[retail_data.retail_zone_id] = retail_manager

    def on_all_households_and_sim_infos_loaded(self):
        if not self.is_on_open_retail_lot:
            self._owner_household.bucks_tracker.deactivate_all_temporary_perk_timers_of_type(RetailManager.RETAIL_BUCKS_REFERENCE)
        for retail_manager in self._retail_managers.values():
            retail_manager.on_all_households_and_sim_infos_loaded()

    def on_client_disconnect(self):
        self._owner_household.bucks_tracker.remove_perk_unlocked_callback(RetailManager.RETAIL_BUCKS_REFERENCE, self._retail_perk_unlocked_callback)
        retail_manager = self._get_retail_manager_for_current_zone()
        if retail_manager is not None:
            retail_manager.on_client_disconnect()

    def _get_retail_manager_for_current_zone(self):
        return self.get_retail_manager_for_zone(services.current_zone_id())

    def get_retail_manager_for_zone(self, zone_id):
        return self._retail_managers.get(zone_id, None)

    @property
    def is_on_open_retail_lot(self):
        retail_manager = self._get_retail_manager_for_current_zone()
        return retail_manager is not None and retail_manager.is_open

    def make_owner(self, retail_zone_id):
        self._retail_managers[retail_zone_id] = RetailManager(self._owner_household, retail_zone_id)

    def remove_owner(self, retail_zone_id):
        del self._retail_managers[retail_zone_id]

    def _retail_perk_unlocked_callback(self, perk):
        if perk.temporary_perk_information is None:
            return
        if self.is_on_open_retail_lot:
            return
        self._owner_household.bucks_tracker.deactivate_temporary_perk_timer(perk)

    def increment_additional_employee_slots(self):
        if self._additional_employee_slots >= self.EMPLOYEE_SLOT_LIMIT - self.EMPLOYEE_SLOTS:
            logger.error('Attempting to add additional slot beyond the max limit.')
            return

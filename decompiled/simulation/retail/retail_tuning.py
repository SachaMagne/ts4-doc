from sims4.resources import Types
from sims4.tuning.tunable import TunablePackSafeReference
import services

class RetailTuning:
    __qualname__ = 'RetailTuning'
    BUY_RETAIL_LOT_AFFORDANCE = TunablePackSafeReference(description='\n        The afforance to buy a retail lot.\n        ', manager=services.get_instance_manager(Types.INTERACTION))

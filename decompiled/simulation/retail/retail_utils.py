from sims4.tuning.tunable import TunableEnumWithFilter
import objects.components.types
import services
import tag

class RetailUtils:
    __qualname__ = 'RetailUtils'
    RETAIL_CUSTOMER_SITUATION_TAG = TunableEnumWithFilter(description='\n        The tag associated with customer situations.\n        ', tunable_type=tag.Tag, default=tag.Tag.INVALID, filter_prefixes=['situation'], pack_safe=True)
    RETAIL_EMPLOYEE_SITUATION_TAG = TunableEnumWithFilter(description='\n        The tag associated with employee situations.\n        ', tunable_type=tag.Tag, default=tag.Tag.INVALID, filter_prefixes=['situation'], pack_safe=True)

    @classmethod
    def get_retail_manager_for_zone(cls, zone_id=None):
        zone_id = zone_id or services.current_zone_id()
        zone_data = services.get_persistence_service().get_zone_proto_buff(zone_id)
        if zone_data is None:
            return
        owner_household_id = zone_data.household_id
        owner_household = services.household_manager().get(owner_household_id)
        if owner_household is not None:
            retail_tracker = owner_household.retail_tracker
            return retail_tracker.get_retail_manager_for_zone(zone_id)
        zone_director = services.venue_service().get_zone_director()
        return getattr(zone_director, 'retail_manager', None)

    @classmethod
    def all_retail_objects_gen(cls, allow_for_sale=True, allow_sold=True, allow_not_for_sale=False, include_inventories=True):
        retail_manager = cls.get_retail_manager_for_zone()
        if retail_manager is None:
            return
        lot_owner_household_id = cls._get_lot_owner_household_id()
        accessed_shared_inventories = []
        for obj in services.object_manager().valid_objects():
            if not obj.is_on_active_lot():
                pass
            if not cls._is_obj_owned_by_lot_owner(obj, lot_owner_household_id):
                pass
            if obj.has_component(objects.components.types.RETAIL_COMPONENT) and (allow_for_sale and obj.retail_component.is_for_sale or allow_sold and obj.retail_component.is_sold or allow_not_for_sale and obj.retail_component.is_not_for_sale):
                yield obj
            while allow_for_sale and include_inventories and obj.has_component(objects.components.types.INVENTORY_COMPONENT):
                inventory_type = obj.inventory_component.inventory_type
                if inventory_type in retail_manager.RETAIL_INVENTORY_TYPES:
                    if inventory_type.is_shared_between_objects:
                        if inventory_type in accessed_shared_inventories:
                            pass
                        else:
                            accessed_shared_inventories.append(inventory_type)
                    yield obj.inventory_component

    @classmethod
    def get_all_retail_objects(cls):
        retail_manager = cls.get_retail_manager_for_zone()
        if retail_manager is None:
            return set()
        lot_owner_household_id = cls._get_lot_owner_household_id()
        output_set = set()
        for obj in services.object_manager().valid_objects():
            if not obj.is_on_active_lot():
                pass
            if not cls._is_obj_owned_by_lot_owner(obj, lot_owner_household_id):
                pass
            if obj.has_component(objects.components.types.RETAIL_COMPONENT):
                output_set.add(obj)
            while obj.has_component(objects.components.types.INVENTORY_COMPONENT) and obj.inventory_component.inventory_type in retail_manager.RETAIL_INVENTORY_TYPES:
                output_set |= set(obj.inventory_component)
        return output_set

    @classmethod
    def get_retail_customer_situation_from_sim(cls, sim):
        situation_manager = services.get_zone_situation_manager()
        for situation in situation_manager.get_situations_sim_is_in_by_tag(sim, cls.RETAIL_CUSTOMER_SITUATION_TAG):
            pass

    @classmethod
    def get_retail_employee_situation_from_sim(cls, sim):
        situation_manager = services.get_zone_situation_manager()
        for situation in situation_manager.get_situations_sim_is_in_by_tag(sim, cls.RETAIL_EMPLOYEE_SITUATION_TAG):
            pass

    @classmethod
    def _get_lot_owner_household_id(cls):
        zone_data = services.get_persistence_service().get_zone_proto_buff(services.current_zone_id())
        if zone_data is not None:
            return zone_data.household_id
        return 0

    @classmethod
    def _is_obj_owned_by_lot_owner(cls, obj, lot_owner_household_id):
        obj_household_owner_id = obj.get_household_owner_id() or 0
        return lot_owner_household_id == obj_household_owner_id

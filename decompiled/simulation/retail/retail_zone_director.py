import random
from date_and_time import create_time_span
from event_testing.resolver import SingleSimResolver
from retail.retail_customer_situation import RetailCustomerSituation
from retail.retail_employee_situation import RetailEmployeeSituation
from retail.retail_manager import RetailManager
from retail.retail_utils import RetailUtils
from sims4.math import MAX_FLOAT, almost_equal
from sims4.random import weighted_random_item
from sims4.tuning.geometric import TunableCurve
from sims4.tuning.tunable import TunableMapping, TunableTuple, TunableInterval, TunableSimMinute, TunableList, TunableReference, Tunable
from situations.situation_guest_list import SituationGuestList, SituationGuestInfo, SituationInvitationPurpose
from zone_director import ZoneDirectorBase
import alarms
import services
import sims4.resources
logger = sims4.log.Logger('Retail', default_owner='trevor')

class RetailZoneDirector(ZoneDirectorBase):
    __qualname__ = 'RetailZoneDirector'
    CUSTOMER_SITUATION_INTERVAL = TunableSimMinute(description='\n        The amount of time, in Sim minutes, between attempts to create new\n        customer situations.\n        ', default=10)
    CUSTOMER_COUNT_CURB_APPEAL_CURVE = TunableCurve(description='\n        The number of customers we want on the lot based on the curb appeal of\n        the lot. This only determines how many customers we want on the lot.\n        The type of customer is driven by the Customer Data Map and the average\n        value of sellable items on the lot.\n        ', x_axis_name='Curb Appeal', y_axis_name='Customer Count')
    NPC_EMPLOYEE_SITUATION_COUNT = Tunable(description='\n        The number of employee situations to create at an NPC retail lot.\n        ', tunable_type=int, default=3)
    INSTANCE_TUNABLES = {'customer_situations': TunableMapping(description='\n            A mapping that defines which customer situations are spawned based\n            on certain properties of the retail lot.\n            ', key_name='Markup Multiplier', key_type=Tunable(description="\n                The store's price multiplier.\n                ", tunable_type=float, default=1), value_type=TunableList(description='\n                A list of tuple defining the customer data for this multiplier.\n                ', tunable=TunableTuple(required_median_value=TunableInterval(description='\n                        The median value of all items in the store must fall within\n                        this interval, which is inclusive.\n                        ', tunable_type=float, default_lower=0, default_upper=MAX_FLOAT, minimum=0), weighted_situations=TunableList(description='\n                        A list of situations that are available in the specified\n                        markup and price range combination. The situations are\n                        weighted relative to one another within this list.\n                        ', tunable=TunableTuple(situation=RetailCustomerSituation.TunableReference(description="\n                                The situation defining the customer's behavior.\n                                "), weight=Tunable(description="\n                                This situation's weight, relative to other\n                                situations in this list.\n                                ", tunable_type=float, default=1)))))), 'employee_situations': TunableList(description='\n            The list of possible employee situations. Right now, one will be\n            assigned at random when the employee comes to work.\n            ', tunable=RetailEmployeeSituation.TunableReference()), 'employee_job': TunableReference(description='\n            A reference to the employee job for employee situations.\n            ', manager=services.get_instance_manager(sims4.resources.Types.SITUATION_JOB)), 'npc_employee_situation': RetailEmployeeSituation.TunableReference(description='\n            The situation NPC employees will run.\n            ')}
    CUSTOMER_SITUATION_LIST_GUID = 258695776
    EMPLOYEE_SITUATION_LIST_GUID = 2967593715

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._customer_situation_alarm_handle = None
        self._customer_situation_ids = []
        self._employee_situation_ids = []
        self._retail_manager = None
        self._is_npc_store = False

    @property
    def retail_manager(self):
        return self._retail_manager

    def on_startup(self):
        super().on_startup()
        self._retail_manager = RetailUtils.get_retail_manager_for_zone()
        if self._retail_manager is None:
            self._retail_manager = RetailManager(None, services.current_zone_id())
        self._customer_situation_alarm_handle = alarms.add_alarm(self, create_time_span(minutes=self.CUSTOMER_SITUATION_INTERVAL), self._on_customer_situation_request, repeating=True)
        self._retail_manager.send_markup_multiplier_message()
        self._retail_manager.update_employees()
        self._retail_manager.try_open_npc_store()

    def on_shutdown(self):
        if self._customer_situation_alarm_handle is not None:
            alarms.cancel_alarm(self._customer_situation_alarm_handle)
            self._customer_situation_alarm_handle = None
        if self._retail_manager is not None:
            self._retail_manager.set_open(False)
        super().on_shutdown()

    def _load_custom_zone_director(self, zone_director_proto, reader):
        for situation_data_proto in zone_director_proto.situations:
            if situation_data_proto.situation_list_guid == self.CUSTOMER_SITUATION_LIST_GUID:
                self._customer_situation_ids.extend(situation_data_proto.situation_ids)
            else:
                while situation_data_proto.situation_list_guid == self.EMPLOYEE_SITUATION_LIST_GUID:
                    self._employee_situation_ids.extend(situation_data_proto.situation_ids)
        super()._load_custom_zone_director(zone_director_proto, reader)

    def _save_custom_zone_director(self, zone_director_proto, writer):
        situation_data_proto = zone_director_proto.situations.add()
        situation_data_proto.situation_list_guid = self.CUSTOMER_SITUATION_LIST_GUID
        situation_data_proto.situation_ids.extend(self._customer_situation_ids)
        situation_data_proto = zone_director_proto.situations.add()
        situation_data_proto.situation_list_guid = self.EMPLOYEE_SITUATION_LIST_GUID
        situation_data_proto.situation_ids.extend(self._employee_situation_ids)
        super()._save_custom_zone_director(zone_director_proto, writer)

    def _did_sim_overstay(self, sim_info):
        if self._retail_manager.is_household_owner(sim_info.household_id):
            return False
        if self._retail_manager.should_close_after_load():
            return True
        return super()._did_sim_overstay(sim_info)

    def start_employee_situations(self, employees, owned_by_npc=False):
        if not owned_by_npc and not employees:
            return
        situation_manager = services.get_zone_situation_manager()
        for employee in employees:
            guest_list = SituationGuestList(invite_only=True)
            guest_info = SituationGuestInfo.construct_from_purpose(employee.sim_id, self.employee_job, SituationInvitationPurpose.CAREER)
            guest_info.expectation_preference = True
            guest_list.add_guest_info(guest_info)
            situation_id = situation_manager.create_situation(random.choice(self.employee_situations), guest_list=guest_list, user_facing=False)
            self._employee_situation_ids.append(situation_id)
        if owned_by_npc:
            self.start_npc_employee_situations(self.NPC_EMPLOYEE_SITUATION_COUNT - len(employees))

    def start_npc_employee_situations(self, num_to_create):
        if num_to_create < 1:
            return
        situation_manager = services.get_zone_situation_manager()
        for _ in range(num_to_create):
            situation_id = situation_manager.create_situation(self.npc_employee_situation, guest_list=SituationGuestList(invite_only=True), spawn_sims_during_zone_spin_up=True, user_facing=False)
            self._employee_situation_ids.append(situation_id)

    def on_remove_employee(self, sim_info):
        situation_manager = services.get_zone_situation_manager()
        for situation_id in self._employee_situation_ids:
            situation = situation_manager.get(situation_id)
            while situation is not None:
                if sim_info is situation.get_employee():
                    situation_manager.destroy_situation_by_id(situation_id)
                    self._employee_situation_ids.remove(situation_id)

    def _get_valid_situations(self, retail_manager):
        median_item_value = retail_manager.get_median_item_value()
        markup_multiplier = retail_manager.markup_multiplier
        for (customer_situation_markup_multiplier, customer_situation_datas) in self.customer_situations.items():
            while almost_equal(markup_multiplier, customer_situation_markup_multiplier):
                break
        return ()
        valid_situations = []
        resolver = SingleSimResolver(services.active_sim_info())
        for customer_situation_data in customer_situation_datas:
            while median_item_value in customer_situation_data.required_median_value:
                valid_situations.extend((pair.weight, pair.situation) for pair in customer_situation_data.weighted_situations if pair.situation.can_start_situation(resolver))
        return valid_situations

    def create_situations_during_zone_spin_up(self):
        super().create_situations_during_zone_spin_up()
        if self._retail_manager.is_open and self._retail_manager.owner_household is None:
            self.start_npc_employee_situations(self.NPC_EMPLOYEE_SITUATION_COUNT)

    def _on_customer_situation_request(self, *_, **__):
        if not self._retail_manager.is_open:
            return
        situation_manager = services.get_zone_situation_manager()
        self._customer_situation_ids = [situation_id for situation_id in self._customer_situation_ids if situation_manager.get(situation_id) is not None]
        desired_situation_count = self.CUSTOMER_COUNT_CURB_APPEAL_CURVE.get(self._retail_manager.get_curb_appeal())
        valid_weighted_situations = self._get_valid_situations(self._retail_manager)
        if not valid_weighted_situations:
            logger.warn('Tried finding a valid starting situation for customer but no situations matches were found.')
            return
        while desired_situation_count > len(self._customer_situation_ids):
            situation_to_start = weighted_random_item(valid_weighted_situations)
            if situation_to_start is None:
                break
            situation_id = situation_manager.create_situation(situation_to_start, guest_list=SituationGuestList(invite_only=True), spawn_sims_during_zone_spin_up=True, user_facing=False)
            self._customer_situation_ids.append(situation_id)

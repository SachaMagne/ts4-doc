from sims4.commands import CommandType
import paths
import services
import sims4.commands

def is_command_available(command_type):
    if command_type >= CommandType.Live:
        return True
    cheats_enabled = False
    active_household = services.active_household()
    if active_household is not None and active_household.cheats_enabled:
        cheats_enabled = True
    if command_type >= CommandType.Cheat and cheats_enabled:
        return True
    if paths.AUTOMATION_MODE:
        return True
    if command_type >= CommandType.Automation and cheats_enabled:
        return True
    return False

sims4.commands.is_command_available = is_command_available
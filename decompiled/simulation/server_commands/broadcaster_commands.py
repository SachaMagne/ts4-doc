from server_commands.argument_helpers import OptionalTargetParam, TunableInstanceParam, get_optional_target
from sims4.commands import CommandType
import services
import sims4.commands
import sims4.resources

@sims4.commands.Command('broadcasters.add')
def broadcasters_add(broadcaster_type, broadcasting_object:OptionalTargetParam=None, _connection=None):
    broadcasting_object = get_optional_target(broadcasting_object, _connection)
    if broadcasting_object is None:
        return False
    broadcaster = broadcaster_type(broadcasting_object=broadcasting_object)
    services.current_zone().broadcaster_service.add_broadcaster(broadcaster)
    return True

@sims4.commands.Command('broadcasters.info')
def broadcasters_info(_connection=None, command_type=CommandType.Automation):
    broadcaster_service = services.current_zone().broadcaster_service
    if broadcaster_service is None:
        return False
    output = sims4.commands.Output(_connection)
    output('Broadcaster Service info:')
    (object_cache, object_cache_tags) = broadcaster_service.get_object_cache_info()
    if object_cache is None:
        output('    There is no cache.')
        return True
    if object_cache_tags is None:
        output('    Considering all objects.')
    else:
        output('    Considering objects with tags:')
        for tag in object_cache_tags:
            output('        {}'.format(tag))
    output('    Cached objects:')
    for obj in object_cache:
        output('        {}'.format(obj))
    return True

from retail.retail_utils import RetailUtils
from server_commands.argument_helpers import OptionalTargetParam, get_optional_target, RequiredTargetParam
from sims.occult_tracker import OccultType
from sims.sim_info_types import Gender
from sims4.common import Pack
from sims4.resources import get_protobuff_for_key
import services
import sims4.commands

@sims4.commands.Command('sims.modify_in_cas', command_type=sims4.commands.CommandType.Live)
def modify_in_cas(opt_sim:OptionalTargetParam=None, _connection=None):
    sim = get_optional_target(opt_sim, _connection)
    if sim is None:
        sims4.commands.output('No valid target for sims.modify_in_cas.', _connection)
        return False
    sims4.commands.client_cheat('sims.exit2cas {} {}'.format(sim.id, sim.household_id), _connection)
    return True

@sims4.commands.Command('sims.modify_in_cas_with_householdId', command_type=sims4.commands.CommandType.Live)
def modify_in_cas_with_household_id(opt_sim:OptionalTargetParam=None, _connection=None):
    sim = get_optional_target(opt_sim, _connection)
    if sim is None:
        sims4.commands.output('No valid target for sims.modify_in_cas_with_householdId.', _connection)
        return False
    sims4.commands.client_cheat('sims.exit2caswithhouseholdid {} {}'.format(sim.id, sim.household_id), _connection)
    return True

@sims4.commands.Command('sims.modify_career_outfit_in_cas', command_type=sims4.commands.CommandType.Live)
def modify_career_outfit_in_cas(opt_sim:OptionalTargetParam=None, _connection=None):
    sim = get_optional_target(opt_sim, _connection)
    if sim is None:
        sims4.commands.output('No valid target specified.', _connection)
        return False
    sims4.commands.client_cheat('sims.exit2caswithhouseholdid {} {} career'.format(sim.id, sim.household_id), _connection)
    return True

@sims4.commands.Command('sims.modify_disguise_in_cas', command_type=sims4.commands.CommandType.Live)
def modify_disguise_in_cas(opt_sim:OptionalTargetParam=None, _connection=None):
    sim = get_optional_target(opt_sim, _connection)
    if sim is None:
        sims4.commands.output('No valid target specified.', _connection)
        return False
    occult_tracker = sim.sim_info.occult_tracker
    occult_tracker.set_pending_occult_type(sim.sim_info.current_occult_types)
    occult_tracker.switch_to_occult_type(OccultType.HUMAN)
    sims4.commands.client_cheat('sims.exit2caswithhouseholdid {} {} disguise'.format(sim.id, sim.household_id), _connection)
    return True

@sims4.commands.Command('cas.modify_mannequin', command_type=sims4.commands.CommandType.Live)
def modify_mannequin_in_cas(obj_id:RequiredTargetParam=None, apply_outfit=False, _connection=None):
    mannequin = obj_id.get_target()
    if mannequin is None:
        sims4.commands.output('No valid target with the specified ID found.', _connection)
        return False
    mannequin_component = mannequin.mannequin_component
    if mannequin_component is None:
        sims4.commands.output('The specified target does not have a Mannequin component.', _connection)
        return False
    persistence_service = services.get_persistence_service()
    if persistence_service is not None:
        persistence_service.del_mannequin_proto_buff(mannequin.id)
        sim_info_data_proto = persistence_service.add_mannequin_proto_buff()
        mannequin_component.populate_sim_info_data_proto(sim_info_data_proto)
        current_zone_id = services.current_zone_id()
        sim_info_data_proto.zone_id = current_zone_id
        sim_info_data_proto.world_id = persistence_service.get_world_id_from_zone(current_zone_id)
        sims4.commands.client_cheat('sims.exit2caswithmannequinid {} {}'.format(mannequin.id, 'apply_outfit' if apply_outfit else ''), _connection)
    return True

@sims4.commands.Command('cas.modify_retail_uniform', command_type=sims4.commands.CommandType.Live, pack=Pack.EP01)
def modify_retail_uniform_in_cas(gender, _connection=None):
    retail_manager = RetailUtils.get_retail_manager_for_zone()
    if retail_manager is None:
        return False
    employee_uniform_data = retail_manager.get_employee_uniform_data(gender)
    if employee_uniform_data is None:
        return False
    persistence_service = services.get_persistence_service()
    if persistence_service is not None:
        persistence_service.del_mannequin_proto_buff(employee_uniform_data.sim_id)
        sim_info_data_proto = persistence_service.add_mannequin_proto_buff()
        sim_info_data_proto.mannequin_id = employee_uniform_data.sim_id
        employee_uniform_data.save_sim_info(sim_info_data_proto)
        if retail_manager.EMPLOYEE_UNIFORM_POSE is not None:
            sim_info_data_proto.animation_pose.asm = get_protobuff_for_key(retail_manager.EMPLOYEE_UNIFORM_POSE.asm)
            sim_info_data_proto.animation_pose.state_name = retail_manager.EMPLOYEE_UNIFORM_POSE.state_name
        current_zone_id = services.current_zone_id()
        sim_info_data_proto.zone_id = current_zone_id
        sim_info_data_proto.world_id = persistence_service.get_world_id_from_zone(current_zone_id)
        sims4.commands.client_cheat('sims.exit2caswithmannequinid {} career'.format(employee_uniform_data.sim_id), _connection)
    return True

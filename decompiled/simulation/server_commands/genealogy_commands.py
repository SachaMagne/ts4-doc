from protocolbuffers import DistributorOps_pb2
from distributor.ops import GenericProtocolBufferOp
from distributor.rollback import ProtocolBufferRollback
from distributor.system import Distributor
from server_commands.argument_helpers import OptionalTargetParam, get_optional_target, RequiredTargetParam
from sims.genealogy_tracker import FamilyRelationshipIndex, genealogy_caching, FamilyRelationshipTuning
from sims.sim_info_types import Age, Gender
from sims.sim_spawner import SimCreator, SimSpawner
import services
import sims4.commands

@sims4.commands.Command('genealogy.print')
def genalogy_print(sim_id:OptionalTargetParam=None, _connection=None):
    sim = get_optional_target(sim_id, _connection)
    if sim is None:
        return False
    genealogy = sim.sim_info.genealogy
    genealogy.log_contents()
    return True

@sims4.commands.Command('genealogy.generate_dynasty')
def genealogy_random_generate(sim_id:OptionalTargetParam=None, generations:int=4, _connection=None):
    sim = get_optional_target(sim_id, _connection)
    if sim is None:
        return False

    def add_parents(child, generation=0):
        if generation >= generations:
            return
        sim_creators = (SimCreator(gender=Gender.MALE, age=Age.ADULT, last_name=child.last_name), SimCreator(gender=Gender.FEMALE, age=Age.ADULT))
        (sim_info_list, _) = SimSpawner.create_sim_infos(sim_creators, household=sim.household, account=sim.account, zone_id=sim.zone_id, creation_source='cheat: genealogy.generate_dynasty')
        sim_info_list[0].death_tracker.set_death_type(1)
        sim_info_list[1].death_tracker.set_death_type(1)
        child.set_and_propagate_family_relation(FamilyRelationshipIndex.FATHER, sim_info_list[0])
        child.set_and_propagate_family_relation(FamilyRelationshipIndex.MOTHER, sim_info_list[1])
        add_parents(sim_info_list[0], generation=generation + 1)
        add_parents(sim_info_list[1], generation=generation + 1)

    add_parents(sim.sim_info)
    sims4.commands.output('Dynasty created for {}'.format(sim), _connection)
    return True

@sims4.commands.Command('genealogy.prune')
def genealogy_prune(sim_id:OptionalTargetParam=None, _connection=None):
    sim = get_optional_target(sim_id, _connection)
    if sim is None:
        return False
    household = sim.household
    if household is None:
        return False
    prune_set = set()
    safe_set = set()
    household.build_set_of_distant_relatives_eligible_for_pruning(prune_set, safe_set)
    sim_info_manager = services.sim_info_manager()
    if prune_set:
        prune_set -= safe_set
        for sim_info in prune_set:
            sim_info.household.remove_sim_info(sim_info)
            sim_info_manager.remove_permanently(sim_info)
            services.get_persistence_service().del_sim_proto_buff(sim_info.id)
    return True

@sims4.commands.Command('genealogy.find_relation')
def genalogy_relation(x_sim, y_sim, _connection=None):
    output = sims4.commands.Output(_connection)
    sim_x = x_sim.get_target()
    bit = None
    if sim_x is not None:
        bit = sim_x.sim_info.genealogy.get_family_relationship_bit(y_sim.target_id, output)
    return bit is not None

@sims4.commands.Command('genealogy.show_family_tree', command_type=sims4.commands.CommandType.Live)
def genealogy_show_family_tree(sim_info_id, antecedent_depth:int=8, descendant_depth:int=2, _connection=None):
    sim_info_manager = services.sim_info_manager()
    closed_set = set()

    def populate_family_tree_node(family_tree_node, sim_info_id, antecedent_depth, descendant_depth, extended_info_depth=0, include_spouse=False, step_depth=0, incoming_sim_id=0):
        family_tree_node.sim_id = sim_info_id
        sim_info = sim_info_manager.get(sim_info_id)
        if sim_info is None:
            return
        relationship_key = (incoming_sim_id, sim_info_id) if incoming_sim_id < sim_info_id else (sim_info_id, incoming_sim_id)
        if relationship_key in closed_set:
            return
        closed_set.add(relationship_key)
        if include_spouse:
            spouse_id = sim_info.spouse_sim_id
            if spouse_id:
                populate_family_tree_node(family_tree_node.spouse, spouse_id, 0, 1 if step_depth else 0, incoming_sim_id=sim_info_id)
        if antecedent_depth:
            for parent_id in sim_info.genealogy.get_parent_sim_ids_gen():
                with ProtocolBufferRollback(family_tree_node.parents) as parent_family_tree_node:
                    populate_family_tree_node(parent_family_tree_node, parent_id, antecedent_depth - 1, 1 if extended_info_depth else 0, extended_info_depth=extended_info_depth - 1 if extended_info_depth else 0, step_depth=step_depth - 1 if step_depth else 0, include_spouse=True, incoming_sim_id=sim_info_id)
        if descendant_depth:
            for child_id in sim_info.genealogy.get_children_sim_ids_gen():
                with ProtocolBufferRollback(family_tree_node.children) as child_family_tree_node:
                    populate_family_tree_node(child_family_tree_node, child_id, 1 if extended_info_depth else 0, descendant_depth - 1, include_spouse=True, incoming_sim_id=sim_info_id)
        if extended_info_depth or include_spouse:
            for relationship in sim_info.relationship_tracker:
                if extended_info_depth:
                    if relationship.has_bit(FamilyRelationshipTuning.SIBLING_RELATIONSHIP_BIT):
                        with ProtocolBufferRollback(family_tree_node.siblings) as sibling_family_tree_node:
                            populate_family_tree_node(sibling_family_tree_node, relationship.target_sim_id, 1, 0, incoming_sim_id=sim_info_id)
                    elif not incoming_sim_id and relationship.has_bit(FamilyRelationshipTuning.GRANDPARENT_RELATIONSHIP_BIT):
                        with ProtocolBufferRollback(family_tree_node.grandparents) as grandparent_family_tree_node:
                            populate_family_tree_node(grandparent_family_tree_node, relationship.target_sim_id, 0, 1, include_spouse=True, incoming_sim_id=sim_info_id)
                    elif not incoming_sim_id and relationship.has_bit(FamilyRelationshipTuning.GRANDCHILD_RELATIONSHIP_BIT):
                        with ProtocolBufferRollback(family_tree_node.grandchildren) as grandchild_family_tree_node:
                            populate_family_tree_node(grandchild_family_tree_node, relationship.target_sim_id, 1, 0, incoming_sim_id=sim_info_id)
                while include_spouse:
                    if relationship.has_bit(FamilyRelationshipTuning.DIVORCED_SPOUSE_RELATIONSHIP_BIT):
                        with ProtocolBufferRollback(family_tree_node.divorced_spouses) as divorced_spouse_family_tree_node:
                            populate_family_tree_node(divorced_spouse_family_tree_node, relationship.target_sim_id, 0, 0, incoming_sim_id=sim_info_id)
                    if relationship.has_bit(FamilyRelationshipTuning.DEAD_SPOUSE_RELATIONSHIP_BIT):
                        with ProtocolBufferRollback(family_tree_node.divorced_spouses) as dead_spouse_family_tree_node:
                            populate_family_tree_node(dead_spouse_family_tree_node, relationship.target_sim_id, 0, 0, incoming_sim_id=sim_info_id)
        return family_tree_node

    family_tree_msg = DistributorOps_pb2.ShowFamilyTree()
    with genealogy_caching():
        populate_family_tree_node(family_tree_msg.root, sim_info_id, antecedent_depth, descendant_depth, extended_info_depth=2, include_spouse=True, step_depth=2)
    distributor = Distributor.instance()
    family_tree_op = GenericProtocolBufferOp(DistributorOps_pb2.Operation.SHOW_FAMILY_TREE, family_tree_msg)
    distributor.add_op_with_no_owner(family_tree_op)

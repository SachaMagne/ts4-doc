from event_testing.resolver import SingleSimResolver
from server_commands.argument_helpers import TunableInstanceParam
import services
import sims4.commands

@sims4.commands.Command('loot.apply_to_sim', command_type=sims4.commands.CommandType.DebugOnly)
def loot_apply_to_sim(loot_type, opt_sim_id:int=None, _connection=None):
    if opt_sim_id is None:
        client = services.client_manager().get(_connection)
        sim_info = client.active_sim_info if client is not None else None
    else:
        sim_info = services.sim_info_manager().get(opt_sim_id)
    if sim_info is None:
        sims4.commands.output('No sim_info specified', _connection)
        return
    resolver = SingleSimResolver(sim_info)
    loot_type.apply_to_resolver(resolver)

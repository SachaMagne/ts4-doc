from objects.components.mannequin_component import MannequinGroupSharingMode, set_mannequin_group_sharing_mode
from server_commands.argument_helpers import OptionalTargetParam, get_optional_target
from sims.outfits.outfit_enums import OutfitCategory
import sims4.commands

@sims4.commands.Command('outfits.generate_outfit')
def generate_outfit(outfit_category, outfit_index:int=0, obj_id:OptionalTargetParam=None, _connection=None):
    obj = get_optional_target(obj_id, _connection)
    if obj is None:
        return False
    outfits = obj.get_outfits()
    if outfits is None:
        return False
    sim_info = outfits.get_sim_info()
    sim_info.generate_outfit(outfit_category=outfit_category, outfit_index=outfit_index)
    output = sims4.commands.Output(_connection)
    output('Generated {} outfit {}.'.format(outfit_category, outfit_index))
    return True

@sims4.commands.Command('outfits.switch_outfit')
def switch_outfit(outfit_category:OutfitCategory=0, outfit_index:int=0, obj_id:OptionalTargetParam=None, _connection=None):
    obj = get_optional_target(obj_id, _connection)
    if obj is None:
        return False
    outfits = obj.get_outfits()
    if outfits is None:
        return False
    sim_info = outfits.get_sim_info()
    sim_info.set_current_outfit((outfit_category, outfit_index))
    return True

@sims4.commands.Command('outfits.info')
def show_outfit_info(obj_id:OptionalTargetParam=None, _connection=None):
    obj = get_optional_target(obj_id, _connection)
    if obj is None:
        return False
    outfits = obj.get_outfits()
    if outfits is None:
        return False
    sim_info = outfits.get_sim_info()
    output = sims4.commands.Output(_connection)
    output('Current outfit: {}'.format(sim_info.get_current_outfit()))
    output('Previous outfit: {}'.format(sim_info.get_previous_outfit()))
    for (outfit_category, outfit_list) in outfits.get_all_outfits():
        output('\t{}'.format(OutfitCategory(outfit_category)))
        for (outfit_index, outfit_data) in enumerate(outfit_list):
            output('\t\t{}: {}'.format(outfit_index, ', '.join(str(part) for part in outfit_data['parts'])))
    output('')
    return True

@sims4.commands.Command('outfits.set_sharing_mode')
def set_outfit_sharing_mode(outfit_sharing_mode):
    set_mannequin_group_sharing_mode(outfit_sharing_mode)
    return True

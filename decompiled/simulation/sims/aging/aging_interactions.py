from interactions.base.super_interaction import SuperInteraction
from sims.aging.aging_liability import AgingLiability

class AgeUpSuperInteraction(SuperInteraction):
    __qualname__ = 'AgeUpSuperInteraction'

    def _setup_gen(self, timeline):
        result = yield super()._setup_gen(timeline)
        if result:
            self.store_event_handler(self._age_up_event_handler, handler_id=100)
            return True
        return False

    def _pre_perform(self, *args, **kwargs):
        self.add_liability(AgingLiability.LIABILITY_TOKEN, AgingLiability(self.sim.sim_info, self.sim.sim_info.age))
        return super()._pre_perform(*args, **kwargs)

    def _age_up_event_handler(self, *args, **kwargs):
        self.sim.sim_info.advance_age()

from event_testing.resolver import SingleSimResolver
from interactions.liability import Liability
from sims.aging.aging_tuning import AgeTransitions
from sims4.localization import TunableLocalizedString
import services

class AgingLiability(Liability):
    __qualname__ = 'AgingLiability'
    LIABILITY_TOKEN = 'AgingLiability'
    AGING_SAVELOCK_TOOLTIP = TunableLocalizedString(description='\n        The tooltip that is used as the reason why the game is save locked while\n        the age up dialog is visible.\n        ')

    def __init__(self, sim_info, starting_age):
        self._sim_info = sim_info
        self._starting_age = starting_age

    def get_lock_save_reason(self):
        return AgingLiability.AGING_SAVELOCK_TOOLTIP

    def release(self):
        if self._sim_info.age != self._starting_age and not self._sim_info.is_npc:
            services.get_persistence_service().lock_save(self)
            dialog = AgeTransitions.AGE_TRANSITION_INFO[self._sim_info.age].age_transition_dialog(self._sim_info, assignment_sim_info=self._sim_info, resolver=SingleSimResolver(self._sim_info))
            dialog.show_dialog(on_response=lambda _: services.get_persistence_service().unlock_save(self))

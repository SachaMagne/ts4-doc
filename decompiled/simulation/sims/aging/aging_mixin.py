import random
from clock import interval_in_sim_days
from date_and_time import TimeSpan, create_time_span
from element_utils import build_element
from event_testing.resolver import SingleSimResolver
from event_testing.test_events import TestEvent
from interactions.context import InteractionContext
from interactions.priority import Priority
from objects import ALL_HIDDEN_REASONS
from relationships.relationship import Relationship
from sims.aging.aging_statistic import AgeProgressContinuousStatistic
from sims.aging.aging_tuning import AgeSpeeds, AgeTransitions, AgingTuning
from sims.baby.baby_aging import baby_age_up
from sims.sim_info_types import Age
import alarms
import distributor.fields
import distributor.ops
import services
import sims4.log
import sims4.telemetry
import telemetry_helper
logger = sims4.log.Logger('Aging')
TELEMETRY_CHANGE_AGE = 'AGES'
writer_age = sims4.telemetry.TelemetryWriter(TELEMETRY_CHANGE_AGE)

class AgingMixin:
    __qualname__ = 'AgingMixin'
    AGE_PROGRESS_BAR_FACTOR = 100

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._age_progress = AgeProgressContinuousStatistic(None, 0.0)
        self._age_progress.decay_enabled = True
        self._auto_aging_enabled = True
        self._age_speed_setting = AgeSpeeds.NORMAL
        self._almost_can_age_handle = None
        self._can_age_handle = None
        self._auto_age_handle = None
        self._walk_on_lot_handle = None
        self._age_time = 1
        self._time_alive = TimeSpan.ZERO
        self._last_time_time_alive_updated = None
        self._age_suppression_alarm_handle = None

    @property
    def is_child(self):
        return self._base.age == Age.CHILD

    @property
    def is_teen(self):
        return self._base.age == Age.TEEN

    @property
    def is_teen_or_older(self):
        return self._base.age >= Age.TEEN

    @property
    def is_teen_or_younger(self):
        return self._base.age <= Age.TEEN

    @property
    def is_young_adult(self):
        return self._base.age == Age.YOUNGADULT

    @property
    def is_young_adult_or_older(self):
        return self._base.age >= Age.YOUNGADULT

    @property
    def is_adult(self):
        return self._base.age == Age.ADULT

    @property
    def is_elder(self):
        return self._base.age == Age.ELDER

    @property
    def age_progress(self):
        return self._age_progress.get_value()

    @age_progress.setter
    def age_progress(self, value):
        self._age_progress.set_value(value)

    @distributor.fields.Field(op=distributor.ops.SetAgeProgress)
    def age_progress_in_days(self):
        return int(self.age_progress/self._age_time*self.AGE_PROGRESS_BAR_FACTOR)

    resend_age_progress = age_progress_in_days.get_resend()

    def _create_fake_total_time_alive(self):
        age = Age.BABY
        time_alive = TimeSpan.ZERO
        while age != self.age:
            age_time = interval_in_sim_days(AgeTransitions.get_duration(Age(self._base.age)))
            time_alive += age_time
            age = Age.next_age(age)
        setting_multiplier = AgeTransitions.get_speed_multiple(self._age_speed_setting)
        time_alive /= setting_multiplier
        return time_alive

    def load_time_alive(self, loaded_time):
        if loaded_time is None:
            loaded_time = self._create_fake_total_time_alive()
        self._time_alive = loaded_time
        self._last_time_time_alive_updated = services.time_service().sim_now

    def update_time_alive(self):
        if self._last_time_time_alive_updated is None:
            logger.error('Trying to update time live before initial value has been set.', owner='jjacobson')
            return
        current_time = services.time_service().sim_now
        time_since_last_update = current_time - self._last_time_time_alive_updated
        self._last_time_time_alive_updated = current_time

    def advance_age_phase(self):
        if self._base.age == Age.ELDER:
            bonus_days = self._get_bonus_days()
        else:
            bonus_days = 0
        age_time = AgeTransitions.get_duration(Age(self._base.age))
        warn_time = age_time - AgeTransitions.get_warning(Age(self._base.age))
        auto_age_time = age_time + AgeTransitions.get_delay(Age(self._base.age)) + bonus_days
        age_progress = self._age_progress.get_value()
        if age_progress <= warn_time:
            age_progress = warn_time
        elif age_progress <= age_time:
            age_progress = age_time
        else:
            age_progress = auto_age_time
        self._age_progress.set_value(age_progress - 0.0001)
        self.update_age_callbacks()

    def reset_age_progress(self):
        self._age_progress.set_value(self._age_progress.min_value)
        self.send_age_progress_bar_update()
        self.resend_age()
        self.update_age_callbacks()

    def _days_until_ready_to_age(self):
        setting_multiplier = AgeTransitions.get_speed_multiple(self._age_speed_setting)
        return (self._age_time - self._age_progress.get_value())/setting_multiplier

    def update_age_callbacks(self):
        self._update_age_trait(self._base.age)
        self._age_time = AgeTransitions.get_duration(Age(self._base.age))
        if self._is_aging_disabled():
            self._age_progress.decay_enabled = False
            if self._almost_can_age_handle is not None:
                alarms.cancel_alarm(self._almost_can_age_handle)
                self._almost_can_age_handle = None
            if self._can_age_handle is not None:
                alarms.cancel_alarm(self._can_age_handle)
                self._can_age_handle = None
            if self._auto_age_handle is not None:
                alarms.cancel_alarm(self._auto_age_handle)
                self._auto_age_handle = None
            if self._walk_on_lot_handle is not None:
                alarms.cancel_alarm(self._walk_on_lot_handle)
                self._walk_on_lot_handle = None
            return
        self._age_progress.decay_enabled = True
        if self.is_elder:
            bonus_days = self._get_bonus_days()
        else:
            bonus_days = 0
        setting_multiplier = AgeTransitions.get_speed_multiple(self._age_speed_setting)
        self._age_progress.set_modifier(setting_multiplier)
        age_time = self._days_until_ready_to_age()
        warn_time = age_time - AgeTransitions.get_warning(Age(self._base.age))/setting_multiplier
        auto_age_time = age_time + (AgeTransitions.get_delay(Age(self._base.age)) + bonus_days)/setting_multiplier
        if self._almost_can_age_handle is not None:
            alarms.cancel_alarm(self._almost_can_age_handle)
        if warn_time >= 0:
            self._almost_can_age_handle = alarms.add_alarm(self, create_time_span(days=warn_time), self.callback_almost_ready_to_age, False)
        if self._can_age_handle is not None:
            alarms.cancel_alarm(self._can_age_handle)
        if age_time >= 0:
            self._can_age_handle = alarms.add_alarm(self, create_time_span(days=age_time), self.callback_ready_to_age, False)
        self._create_auto_age_callback(delay=max(0, auto_age_time))
        self.send_age_progress()

    def send_age_progress(self):
        if self.is_selectable:
            self.send_age_progress_bar_update()

    def _create_auto_age_callback(self, delay=1):
        if self._auto_age_handle is not None:
            alarms.cancel_alarm(self._auto_age_handle)
        time_span_until_age_up = create_time_span(days=delay)
        if time_span_until_age_up.in_ticks() <= 0:
            time_span_until_age_up = create_time_span(minutes=1)
        self._auto_age_handle = alarms.add_alarm(self, time_span_until_age_up, self.callback_auto_age, False)

    def add_bonus_days(self, number_of_days):
        pass

    def _get_bonus_days(self):
        bonus_days = 0
        for trait_inst_id in self._trait_tracker.trait_ids:
            bonus_days_percent = AgeTransitions.get_bonus_for_trait(trait_inst_id)
            bonus_days += bonus_days_percent*AgeTransitions.get_total_lifetime()
        bonus_days += AgeTransitions.get_bonus_for_sim_statistic_objectives(self.aspiration_tracker)
        bonus_days += self._additional_bonus_days
        return bonus_days

    def _apply_auto_aging_buff(self):
        buff = AgeTransitions.get_auto_aging_buff(self)
        if buff is not None:
            self.add_buff_from_op(buff, buff.buff_name(self))

    def _update_age_trait(self, next_age, current_age=None):
        trait_tracker = self.trait_tracker
        if current_age is not None:
            trait_to_remove = AgeTransitions.get_age_trait(current_age)
            if trait_tracker.has_trait(trait_to_remove):
                self.remove_trait(trait_to_remove)
        trait_to_add = AgeTransitions.get_age_trait(next_age)
        if trait_to_add is None:
            return
        if not trait_tracker.has_trait(trait_to_add):
            self.add_trait(trait_to_add)

    def _show_age_notification(self, notification_name):
        if not self.is_npc and not self._is_aging_disabled():
            notification = getattr(AgeTransitions.AGE_TRANSITION_INFO[self.age], notification_name)
            dialog = notification(self, SingleSimResolver(self))
            dialog.show_dialog(additional_tokens=(self,))

    def callback_ready_to_age(self, *_, **__):
        logger.info('READY TO AGE: {}', self.full_name)
        self._show_age_notification('age_up_available_notification')
        services.get_event_manager().process_event(TestEvent.ReadyToAge, sim_info=self)

    def callback_almost_ready_to_age(self, *_, **__):
        logger.info('ALMOST READY TO AGE: {}', self.full_name)
        self._show_age_notification('age_up_warning_notification')

    def callback_auto_age(self, *_, **__):
        if self._is_aging_disabled():
            self._create_auto_age_callback()
        else:
            walk_on_lot_delay = create_time_span(0.001*random.randrange(1, 10, 1))
            self._walk_on_lot_handle = alarms.add_alarm(self, walk_on_lot_delay, self.age_moment, False)

    def age_moment(self, walk_on_lot_handle):
        logger.info('AGE UP COMPLETE BY AUTOAGING: {}', self.full_name)
        if self.is_baby:
            self._age_up_baby()
        elif self.is_elder:
            self._age_up_elder()
        else:
            self._age_up_pctya()
        alarms.cancel_alarm(walk_on_lot_handle)
        self._walk_on_lot_handle = None

    def _age_up_baby(self):
        baby = services.object_manager().get(self.sim_id)
        if baby is not None:
            client = services.client_manager().get_client_by_account(self._account.id)
            self.advance_age()
            baby_age_up(self, client)
        elif self.is_npc:
            self.advance_age()

    def _age_up_pctya(self):
        sim_instance = self.get_sim_instance(allow_hidden_flags=ALL_HIDDEN_REASONS)
        if sim_instance is not None:
            client = services.client_manager().get_client_by_account(self._account.id)
            context = InteractionContext(sim_instance, InteractionContext.SOURCE_SCRIPT, Priority.Critical, client=client, pick=None)
            result = sim_instance.push_super_affordance(AgingTuning.AGE_UP_MOMENT, None, context)
            if not result:
                self._create_auto_age_callback()
                return
        elif self.is_npc:
            self.advance_age()
        self._apply_auto_aging_buff()

    def _age_up_elder(self):
        sim_instance = self.get_sim_instance(allow_hidden_flags=ALL_HIDDEN_REASONS)
        if sim_instance is not None:
            client = services.client_manager().get_client_by_account(self._account.id)
            context = InteractionContext(sim_instance, InteractionContext.SOURCE_SCRIPT, Priority.Critical, client=client, pick=None)
            if not sim_instance.push_super_affordance(AgingTuning.OLD_AGE_DEATH, None, context):
                self._create_auto_age_callback()
        elif self.is_npc:
            household = self.household
            if self.spouse_sim_id is not None:
                spouse_sim_id = self.spouse_sim_id
                self.relationship_tracker.remove_relationship_bit(spouse_sim_id, Relationship.MARRIAGE_RELATIONSHIP_BIT)
                spouse_sim_info = services.sim_info_manager().get(spouse_sim_id)
                if spouse_sim_info is not None:
                    spouse_sim_info.relationship_tracker.remove_relationship_bit(self.id, Relationship.MARRIAGE_RELATIONSHIP_BIT)
            self.death_tracker.set_death_type(AgingTuning.OLD_AGE_DEATH.death_type)
            self.death_tracker.handle_adultless_household(household)

    def set_aging_speed(self, speed):
        if speed is None or speed < 0 or speed > 2:
            logger.warn('Trying to set aging speed on a sim with an invalid speed: {}. Speed can only be 0, 1, or 2.', speed)
        self._age_speed_setting = AgeSpeeds(speed)
        self.update_age_callbacks()

    def set_aging_enabled(self, enabled):
        self._auto_aging_enabled = enabled
        self.update_age_callbacks()

    def advance_age_progress(self, days) -> None:
        self._age_progress.set_value(self._age_progress.get_value() + days)

    def _is_aging_disabled_common(self):
        if any(not trait.can_age_up for trait in self.trait_tracker):
            return True
        if self._age_suppression_alarm_handle is not None:
            return True
        return False

    def _is_aging_disabled(self):
        if not self._auto_aging_enabled:
            return True
        if self._is_aging_disabled_common():
            return True
        if self.is_elder and self.is_death_disabled():
            return True
        return False

    def is_death_disabled(self):
        return any(not trait.can_die for trait in self.trait_tracker)

    def can_age_up(self) -> bool:
        if self.is_elder:
            return False
        if self._is_aging_disabled_common():
            return False
        return True

    @property
    def time_until_age_up(self):
        return self._age_time - self._age_progress.get_value()

    def advance_age(self, force_age=None) -> None:
        current_age = Age(self._base.age)
        next_age = Age(force_age) if force_age is not None else Age.next_age(current_age)
        self._relationship_tracker.update_bits_on_age_up(current_age)
        self.age_progress = 0
        self.apply_age(next_age)
        self.age = next_age
        self.appearance_tracker.evaluate_appearance_modifiers()
        self.resend_physical_attributes()
        self._update_age_trait(next_age, current_age)
        self.init_child_skills()
        if self.is_teen:
            self.remove_child_only_features()
        self.career_tracker.remove_invalid_careers()
        sim_instance = self.get_sim_instance(allow_hidden_flags=ALL_HIDDEN_REASONS)
        if sim_instance is not None:
            with telemetry_helper.begin_hook(writer_age, TELEMETRY_CHANGE_AGE, sim=sim_instance) as hook:
                hook.write_enum('agef', current_age)
                hook.write_enum('aget', next_age)
                sim_instance.schedule_element(services.time_service().sim_timeline, build_element(sim_instance._update_face_and_posture_gen))
                sim_instance._update_multi_motive_buff_trackers()
            if current_age != Age.BABY:
                self.verify_school(from_age_up=True)
        self.reset_age_progress()
        if self.is_npc:
            if self.is_child or self.is_teen:
                available_aspirations = []
                aspiration_track_manager = services.get_instance_manager(sims4.resources.Types.ASPIRATION_TRACK)
                for aspiration_track in aspiration_track_manager.types.values():
                    if aspiration_track.is_child_aspiration_track:
                        if self.is_child:
                            available_aspirations.append(aspiration_track)
                            while self.is_teen:
                                available_aspirations.append(aspiration_track)
                    else:
                        while self.is_teen:
                            available_aspirations.append(aspiration_track)
                self.primary_aspiration = random.choice(available_aspirations)
            trait_tracker = self.trait_tracker
            empty_trait_slots = trait_tracker.empty_slot_number
            available_traits = [trait for trait in services.trait_manager().types.values() if trait.is_personality_trait]
            while True:
                while empty_trait_slots > 0 and available_traits:
                    trait = random.choice(available_traits)
                    available_traits.remove(trait)
                    if not trait_tracker.can_add_trait(trait):
                        continue
                    #ERROR: Unexpected statement:   699 POP_BLOCK  |   700 JUMP_ABSOLUTE 719

                    if self.add_trait(trait):
                        empty_trait_slots -= 1
                        continue
                        continue
                    continue
        else:
            self.whim_tracker.validate_goals()
        client = services.client_manager().get_client_by_household_id(self._household_id)
        if client is None:
            return
        client.selectable_sims.notify_dirty()

    def _suppress_aging_callback(self, _):
        self._age_suppression_alarm_handle = None

    def suppress_aging(self):
        if self._age_suppression_alarm_handle is not None:
            logger.warn("Trying to suppress aging when aging is already suppressed. You probably don't want to do be doing this.", owner='jjacobson')
        self._age_suppression_alarm_handle = alarms.add_alarm(self, create_time_span(minutes=AgingTuning.AGE_SUPPRESSION_ALARM_TIME), self._suppress_aging_callback)

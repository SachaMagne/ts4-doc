from event_testing.resolver import SingleSimResolver
from event_testing.tests import TunableTestSet
from sims.sim_dialogs import SimPersonalityAssignmentDialog
from sims.sim_info_types import Age
from sims4.tuning.tunable import TunableFactory, TunableReference, Tunable, TunableMapping, TunableTuple, TunableList, TunableEnumEntry, TunableSimMinute, TunableThreshold
from ui.ui_dialog import PhoneRingType
from ui.ui_dialog_notification import UiDialogNotification
import enum
import services
import sims4.resources

class AgeSpeeds(enum.Int):
    __qualname__ = 'AgeSpeeds'
    FAST = 0
    NORMAL = 1
    SLOW = 2

class TraitDaysFactory(TunableFactory):
    __qualname__ = 'TraitDaysFactory'

    @staticmethod
    def factory(tuned_trait, percent_days_award, trait_in_question):
        if trait_in_question == tuned_trait.guid64:
            return percent_days_award
        return 0

    FACTORY_TYPE = factory

    def __init__(self, **kwargs):
        super().__init__(tuned_trait=TunableReference(description='\n                The trait we want to test ownership of.\n                ', manager=services.get_instance_manager(sims4.resources.Types.TRAIT)), percent_days_award=Tunable(description='\n                Percent lifespan is extended as bonus life span for possessing\n                this trait. (This is the bonus time, so .1 is 10% more.)\n                ', tunable_type=float, default=0))

class TunableSimBonusDaysFactory(TunableFactory):
    __qualname__ = 'TunableSimBonusDaysFactory'

    @staticmethod
    def factory(tracker, sim_stat_objective, threshold, days_to_award):
        current_count = tracker.get_objective_count(sim_stat_objective)
        if threshold.compare(current_count):
            return days_to_award
        return 0

    FACTORY_TYPE = factory

    def __init__(self, **kwargs):
        super().__init__(sim_stat_objective=TunableReference(description='\n                A reference to an objective to get the mood count from.  Sim\n                Info Statistic object completion type is currently only\n                supported type of objective.\n                ', manager=services.get_instance_manager(sims4.resources.Types.OBJECTIVE)), threshold=TunableThreshold(description='\n                Threshold that must be satisified for bonus days to be awarded.\n                ', tunable_type=float, default=1), days_to_award=Tunable(description='\n                 Number of Sim days awarded as bonus life span when objective\n                 count satisfies threshold.\n                 ', tunable_type=float, default=1))

class AgeTransitionInfo(TunableTuple):
    __qualname__ = 'AgeTransitionInfo'

    def __init__(self, **kwargs):
        super().__init__(age_up_warning_notification=UiDialogNotification.TunableFactory(description='\n                Message sent to client to warn of impending age up.\n                '), age_up_available_notification=UiDialogNotification.TunableFactory(description='\n                Message sent to client to alert age up is ready.\n                '), age_transition_threshold=Tunable(description='\n                Number of Sim days required to be eligible to transition from\n                the mapping key age to the next one."\n                ', tunable_type=float, default=1), age_transition_warning=Tunable(description='\n                Number of Sim days prior to the transition a Sim will get a\n                warning of impending new age.\n                ', tunable_type=float, default=1), age_transition_delay=Tunable(description='\n                Number of Sim days after transition time elapsed before auto-\n                aging occurs.\n                ', tunable_type=float, default=1), auto_aging_actions=TunableTuple(description='\n                Tuning related to actions that will be applied on auto-aging\n                rather than aging up normally through the birthday cake.\n                ', buff=TunableReference(description='\n                    Buff that will be applied to the Sim when aging up to the\n                    current age.  This buff will be applied if the Sim auto-ages\n                    rather than if they age up with the birthday cake.\n                    ', manager=services.buff_manager()), buff_tests=TunableTestSet(description='\n                    Tests that will be run to determine if to apply the buff on\n                    the Sim.  This should be used to not apply the auto-aging\n                    under certain circumstances, example auto-aging while a\n                    birthday party is being thrown for the sim.\n                    ')), age_trait=TunableReference(description="\n                The age trait that corresponds to this Sim's age\n                ", manager=services.trait_manager()), age_transition_dialog=SimPersonalityAssignmentDialog.TunableFactory(description='\n                Dialog displayed to the player when their sim ages up.\n                ', locked_args={'phone_ring_type': PhoneRingType.NO_RING}))

class AgeTransitions:
    __qualname__ = 'AgeTransitions'
    AGE_TRANSITION_INFO = TunableMapping(description='\n        A mapping between age and the tuning that details the transition into\n        and out of that age.\n        ', key_type=TunableEnumEntry(description='\n            The age that this set of age transition info will correspond to.\n            ', tunable_type=Age, default=Age.ADULT), value_type=AgeTransitionInfo())
    AGE_SPEED_SETTING_MULTIPLIER = TunableMapping(description='\n        A mapping between age speeds and the multiplier that those speeds\n        correspond to.\n        ', key_type=TunableEnumEntry(description='\n            The age speed that will be mapped to its multiplier.\n            ', tunable_type=AgeSpeeds, default=AgeSpeeds.NORMAL), value_type=Tunable(description="\n            The multiplier by which to adjust the lifespan based on user age\n            speed settings. Setting this to 2 for 'Slow' speed will double the\n            Sim's age play time in that setting.\n            ", tunable_type=float, default=1))
    BONUS_TRAIT_DAYS = TunableList(description='\n        Extra days of life to be awarded to the Sim for possessing this trait.\n        ', tunable=TraitDaysFactory())
    SIM_BONUS_DAYS = TunableList(description='\n        Extra days of life to be awarded to the sim for statistic objective passing a threshold.\n        ', tunable=TunableSimBonusDaysFactory())

    @classmethod
    def get_duration(cls, age) -> float:
        if age in cls.AGE_TRANSITION_INFO.keys():
            return cls.AGE_TRANSITION_INFO[age].age_transition_threshold
        return 0

    @classmethod
    def get_total_lifetime(cls) -> float:
        total_lifetime = sum(age_data.age_transition_threshold for age_data in cls.AGE_TRANSITION_INFO.values())
        age_service = services.get_age_service()
        total_lifetime /= cls.get_speed_multiple(age_service.aging_speed)
        return total_lifetime

    @classmethod
    def get_warning(cls, age) -> float:
        if age in cls.AGE_TRANSITION_INFO.keys():
            return cls.AGE_TRANSITION_INFO[age].age_transition_warning
        return 0

    @classmethod
    def get_delay(cls, age) -> float:
        if age in cls.AGE_TRANSITION_INFO.keys():
            return cls.AGE_TRANSITION_INFO[age].age_transition_delay
        return 0

    @classmethod
    def get_speed_multiple(cls, ageSpeed) -> float:
        if ageSpeed in cls.AGE_SPEED_SETTING_MULTIPLIER.keys():
            return cls.AGE_SPEED_SETTING_MULTIPLIER[ageSpeed]
        return 0

    @classmethod
    def get_auto_aging_buff(cls, sim_info):
        age = sim_info.age
        if age not in cls.AGE_TRANSITION_INFO.keys():
            return
        auto_aging_actions = cls.AGE_TRANSITION_INFO[age].auto_aging_actions
        if auto_aging_actions.buff is None:
            return
        resolver = SingleSimResolver(sim_info)
        if not auto_aging_actions.buff_tests.run_tests(resolver):
            return
        return auto_aging_actions.buff

    @classmethod
    def get_bonus_for_trait(cls, trait_inst_id) -> float:
        return sum(trait_to_check(trait_in_question=trait_inst_id) for trait_to_check in cls.BONUS_TRAIT_DAYS)

    @classmethod
    def get_bonus_for_sim_statistic_objectives(cls, tracker) -> float:
        return sum(bonus_data(tracker) for bonus_data in cls.SIM_BONUS_DAYS)

    @classmethod
    def get_age_trait(cls, age):
        age_data = cls.AGE_TRANSITION_INFO.get(age)
        if age_data is not None:
            return age_data.age_trait

class AgingTuning:
    __qualname__ = 'AgingTuning'
    AGE_PROGRESS_UPDATE_TIME = Tunable(description='\n        The update rate, in Sim Days, of age progression in the UI.\n        ', tunable_type=float, default=0.2)
    AGE_UP_MOMENT = TunableReference(description='\n        Interaction to age up a Sim\n        ', manager=services.get_instance_manager(sims4.resources.Types.INTERACTION), class_restrictions='AgeUpSuperInteraction')
    OLD_AGE_DEATH = TunableReference(description="\n        Interaction of a Sim's demise for old age\n        ", manager=services.get_instance_manager(sims4.resources.Types.INTERACTION), class_restrictions='DeathSuperInteraction')
    AGE_SUPPRESSION_ALARM_TIME = TunableSimMinute(description='\n        Amount of time in sim seconds to suppress aging.\n        ', default=5, minimum=1)

from interactions.aop import AffordanceObjectPair
from interactions.context import InteractionContext, InteractionSource, QueueInsertStrategy
from interactions.priority import Priority
from objects import VisibilityState
from objects.system import create_object
from sims.aging.aging_liability import AgingLiability
from sims.baby.baby_tuning import BabyTuning
from sims.sim_info_types import Age
from sims.sim_spawner import SimSpawner
from singletons import DEFAULT
import services
import sims4.log
logger = sims4.log.Logger('Baby')

def baby_age_up(sim_info, client=DEFAULT):
    bassinet = services.object_manager().get(sim_info.id)
    if bassinet is None:
        return
    middle_bassinet = bassinet.replace_for_age_up()
    if middle_bassinet is not None:
        try:

            def run_age_up(kid):

                def age_up_exit_behavior():
                    new_bassinet = create_object(BabyTuning.get_corresponding_definition(middle_bassinet.definition))
                    new_bassinet.location = middle_bassinet.location
                    middle_bassinet.make_transient()

                kid.fade_opacity(1, 0)
                kid.visibility = VisibilityState(False)
                affordance = bassinet.get_age_up_addordance()
                aop = AffordanceObjectPair(affordance, middle_bassinet, affordance, None, exit_functions=(age_up_exit_behavior,))
                context = InteractionContext(kid, InteractionSource.SCRIPT, Priority.Critical, insert_strategy=QueueInsertStrategy.NEXT)
                result = aop.test_and_execute(context)
                if result:
                    result.interaction.add_liability(AgingLiability.LIABILITY_TOKEN, AgingLiability(sim_info, Age.BABY))
                else:
                    logger.error('Failed to run baby age up interaction.', owner='jjacobson')
                return True

            if not SimSpawner.spawn_sim(sim_info, middle_bassinet.position, spawn_action=run_age_up):
                logger.error('Failed to spawn sim in process of baby age up.  We are in an unrecoverable situation if this occurs.', owner='jjacobson')
            if client is DEFAULT:
                client = services.client_manager().get_client_by_household_id(sim_info.household_id)
            while client is not None:
                if sim_info not in client.selectable_sims:
                    client.selectable_sims.add_selectable_sim_info(sim_info)
                else:
                    client.on_sim_added_to_skewer(sim_info)
                    client.selectable_sims.notify_dirty()
        except Exception as e:
            logger.exception('{} fail to age up with sim_info {}', middle_bassinet, sim_info, exc=e)

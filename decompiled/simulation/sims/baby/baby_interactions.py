from event_testing.results import TestResult
from interactions.base.interaction import RESERVATION_LIABILITY
from interactions.base.super_interaction import SuperInteraction
from sims.baby.baby_aging import baby_age_up

class BabyManualAgeUpInteraction(SuperInteraction):
    __qualname__ = 'BabyManualAgeUpInteraction'

    @classmethod
    def _test(cls, target, context, **interaction_parameters):
        result = super()._test(target, context, **interaction_parameters)
        if not result:
            return result
        if target.sim_info is None:
            return TestResult(False, 'Bassinet({}) does not have sim info.', target)
        if not target.sim_info.can_age_up():
            return TestResult(False, 'Baby {} cannot age up now.', target.sim_info)
        return TestResult.TRUE

    def _run_interaction_gen(self, timeline):

        def age_up_baby():
            sim_info = self.target.sim_info
            self.set_target(None)
            self.remove_liability(RESERVATION_LIABILITY)
            sim_info.advance_age()
            baby_age_up(sim_info)

        self.add_exit_function(age_up_baby)

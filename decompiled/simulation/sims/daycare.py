from _weakrefset import WeakSet
from event_testing.resolver import SingleSimResolver
from interactions.liability import Liability
from sims4.service_manager import Service
from sims4.tuning.tunable import HasTunableFactory, TunableReference
from ui.ui_dialog_notification import TunableUiDialogNotificationSnippet
import itertools
import services
import sims4.log
logger = sims4.log.Logger('Daycare', default_owner='epanero')

class DaycareTuning:
    __qualname__ = 'DaycareTuning'
    DAYCARE_TRAIT_ON_KIDS = TunableReference(description='\n        The trait that indicates a baby is at daycare.\n        ', manager=services.trait_manager())
    NANNY_TRAIT_ON_KIDS = TunableReference(description='\n        The trait that children and babies that are with the nanny have.\n        ', manager=services.trait_manager())
    SEND_BABY_TO_DAYCARE_NOTIFICATION_SINGLE_BABY = TunableUiDialogNotificationSnippet(description='\n        The message appearing when a single baby is sent to daycare. You can\n        reference this single baby by name.\n        ')
    SEND_BABY_TO_DAYCARE_NOTIFICATION_MULTIPLE_BABIES = TunableUiDialogNotificationSnippet(description='\n        The message appearing when multiple babies are sent to daycare. You can\n        not reference any of these babies by name.\n        ')
    BRING_BABY_BACK_FROM_DAYCARE_NOTIFICATION_SINGLE_BABY = TunableUiDialogNotificationSnippet(description='\n        The message appearing when a single baby is brought back from daycare.\n        You can reference this single baby by name.\n        ')
    BRING_BABY_BACK_FROM_DAYCARE_NOTIFICATION_MULTIPLE_BABIES = TunableUiDialogNotificationSnippet(description='\n        The message appearing when multiple babies are brought back from\n        daycare. You can not reference any of these babies by name.\n        ')
    SEND_CHILD_TO_NANNY_NOTIFICATION_SINGLE = TunableUiDialogNotificationSnippet(description='\n        The message appears when a single child is sent to the nanny. You can\n        reference this single nanny by name.\n        ')
    SEND_CHILD_TO_NANNY_NOTIFICATION_MULTIPLE = TunableUiDialogNotificationSnippet(description='\n        The message appearing when multiple children are sent to the nanny. You\n        can not reference any of these children by name.\n        ')
    BRING_CHILD_BACK_FROM_NANNY_NOTIFICATION_SINGLE = TunableUiDialogNotificationSnippet(description='\n        The message appearing when a single child is brought back from the\n        nanny. You can reference this single child by name.\n        ')
    BRING_CHILD_BACK_FROM_NANNY_NOTIFICATION_MULTIPLE = TunableUiDialogNotificationSnippet(description='\n        The message appearing when multiple children are brought back from\n        the nanny. You can not reference any of these by name.\n        ')

class DaycareLiability(Liability, HasTunableFactory):
    __qualname__ = 'DaycareLiability'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._sim_info = None

    def on_add(self, interaction):
        super().on_add(interaction)
        if interaction.sim is None:
            logger.error('Daycare liability is unavailable on Simless interaction {}', interaction)
            return
        self._sim_info = interaction.sim.sim_info

    def on_run(self):
        super().on_run()
        daycare_service = services.daycare_service()
        if daycare_service is not None:
            daycare_service.set_sim_unavailable(self._sim_info)

    def release(self):
        super().release()
        daycare_service = services.daycare_service()
        if daycare_service is not None:
            daycare_service.set_sim_available(self._sim_info)

class DaycareService(Service):
    __qualname__ = 'DaycareService'

    def __init__(self):
        self._unavailable_sims = WeakSet()

    def _enable_daycare_state(self, baby):
        baby.ignore_daycare = False
        baby.empty_baby_state()
        return True

    def _disable_daycare_state(self, baby):
        if baby.ignore_daycare:
            baby.ignore_daycare = False
            return False
        baby.enable_baby_state()
        return True

    def _get_all_babies_gen(self, household):
        object_manager = services.object_manager(services.current_zone_id())
        for baby_info in household.baby_info_gen():
            baby = object_manager.get(baby_info.sim_id)
            while baby is not None:
                yield baby

    def _is_sim_available(self, sim_info, household, current_zone_id):
        if sim_info.zone_id != current_zone_id:
            if sim_info.career_tracker.currently_at_work:
                return False
            return True
        if sim_info.zone_id == household.home_zone_id and sim_info not in self._unavailable_sims:
            return True
        return False

    def _is_any_sim_available(self, household):
        current_zone_id = services.current_zone_id()
        return any(self._is_sim_available(sim_info, household, current_zone_id) for sim_info in household.teen_or_older_info_gen())

    def _is_everyone_on_vacation(self, household):
        return all(sim_info.is_in_travel_group() for sim_info in household.teen_or_older_info_gen())

    def _enable_daycare_or_nanny_if_necessary(self, household):
        nanny_sim_infos = self.get_sim_infos_for_nanny(household)
        sent_sim_infos = []
        for sim_info in nanny_sim_infos:
            while not sim_info.trait_tracker.has_trait(DaycareTuning.NANNY_TRAIT_ON_KIDS):
                sent_sim_infos.append(sim_info)
                sim_info.add_trait(DaycareTuning.NANNY_TRAIT_ON_KIDS)
                if sim_info.trait_tracker.has_trait(DaycareTuning.DAYCARE_TRAIT_ON_KIDS):
                    sim_info.remove_trait(DaycareTuning.DAYCARE_TRAIT_ON_KIDS)
        self._show_nanny_notification(household, sent_sim_infos, is_enable=True)
        if nanny_sim_infos:
            return
        if not self._is_any_sim_available(household):
            if household.home_zone_id == services.current_zone_id():
                daycare_sim_infos = [baby.sim_info for baby in self._get_all_babies_gen(household) if self._enable_daycare_state(baby)]
            else:
                daycare_sim_infos = list(household.baby_info_gen())
            sent_sim_infos = []
            for sim_info in daycare_sim_infos:
                while not sim_info.trait_tracker.has_trait(DaycareTuning.DAYCARE_TRAIT_ON_KIDS):
                    sent_sim_infos.append(sim_info)
                    sim_info.add_trait(DaycareTuning.DAYCARE_TRAIT_ON_KIDS)
            self._show_daycare_notification(household, sent_sim_infos, is_enable=True)

    def _disable_daycare_or_nanny_if_necessary(self, household):
        returned_children = []
        if household.number_of_babies_and_children and not self._is_everyone_on_vacation(household):
            sim_infos_for_nanny = list(household.baby_info_gen())
            for child_info in household.child_info_gen():
                while not child_info.is_in_travel_group():
                    sim_infos_for_nanny.append(child_info)
            for sim_info in sim_infos_for_nanny:
                while sim_info.trait_tracker.has_trait(DaycareTuning.NANNY_TRAIT_ON_KIDS):
                    sim_info.remove_trait(DaycareTuning.NANNY_TRAIT_ON_KIDS)
                    returned_children.append(sim_info)
            self._show_nanny_notification(household, returned_children, is_enable=False)
        if household.number_of_babies and self._is_any_sim_available(household):
            returned_babies = [baby.sim_info for baby in self._get_all_babies_gen(household) if self._disable_daycare_state(baby)]
            for baby in returned_babies:
                while baby.trait_tracker.has_trait(DaycareTuning.DAYCARE_TRAIT_ON_KIDS):
                    baby.remove_trait(DaycareTuning.DAYCARE_TRAIT_ON_KIDS)
            if not returned_children:
                self._show_daycare_notification(household, returned_babies, is_enable=False)

    def is_daycare_enabled(self, household):
        return not self._is_any_sim_available(household)

    def get_sim_infos_for_nanny(self, household):
        if not self._is_everyone_on_vacation(household):
            return []
        sim_infos_for_nanny = list(household.baby_info_gen())
        for child_info in household.child_info_gen():
            while not child_info.is_in_travel_group():
                sim_infos_for_nanny.append(child_info)
        return sim_infos_for_nanny

    def on_sim_spawn(self, sim_info):
        current_zone = services.current_zone()
        if not current_zone.is_zone_running:
            return
        household = sim_info.household
        if household is not None:
            if household.home_zone_id == current_zone.id:
                self._unavailable_sims.add(sim_info)
                self.set_sim_available(sim_info)
            else:
                self.set_sim_unavailable(sim_info)

    def on_loading_screen_animation_finished(self):
        household = services.active_household()
        if household is None:
            return
        if household.home_zone_id == services.current_zone_id():
            returning_sim_infos = [sim_info for sim_info in services.sim_info_manager().get_traveled_to_zone_sim_infos() if sim_info not in self._unavailable_sims]
            for sim_info in returning_sim_infos:
                self._unavailable_sims.add(sim_info)
            for sim_info in returning_sim_infos:
                self.set_sim_available(sim_info)
        else:
            self._enable_daycare_or_nanny_if_necessary(household)

    def refresh_household_daycare_nanny_status(self, household):
        if self.is_anyone_with_nanny(household) or self.is_anyone_at_daycare(household):
            self._disable_daycare_or_nanny_if_necessary(household)
        else:
            self._enable_daycare_or_nanny_if_necessary(household)

    def refresh_daycare_status(self, baby):
        household = baby.household
        if household is not None:
            if self.is_daycare_enabled(household):
                self._enable_daycare_state(baby)
            else:
                self._disable_daycare_state(baby)

    def is_anyone_at_daycare(self, household):
        return any(sim_info.trait_tracker.has_trait(DaycareTuning.DAYCARE_TRAIT_ON_KIDS) for sim_info in household.baby_info_gen())

    def is_anyone_with_nanny(self, household):
        return any(sim_info.trait_tracker.has_trait(DaycareTuning.NANNY_TRAIT_ON_KIDS) for sim_info in itertools.chain(household.baby_info_gen(), household.child_info_gen()))

    def set_sim_available(self, sim_info):
        household = sim_info.household
        daycare_previously_enabled = self.is_anyone_at_daycare(household)
        nanny_previously_enabled = False if daycare_previously_enabled else self.is_anyone_with_nanny(household)
        self._unavailable_sims.discard(sim_info)
        if daycare_previously_enabled or nanny_previously_enabled:
            self._disable_daycare_or_nanny_if_necessary(household)

    def set_sim_unavailable(self, sim_info):
        household = sim_info.household
        nanny_previously_disabled = not self.is_anyone_with_nanny(household)
        daycare_previously_disabled = True if not nanny_previously_disabled else not self.is_anyone_at_daycare(household)
        self._unavailable_sims.add(sim_info)
        if nanny_previously_disabled or daycare_previously_disabled:
            self._enable_daycare_or_nanny_if_necessary(household)

    def _show_nanny_notification(self, household, sim_infos, is_enable=False):
        if sim_infos:
            if is_enable:
                if len(sim_infos) == 1:
                    self._show_notification(DaycareTuning.SEND_CHILD_TO_NANNY_NOTIFICATION_SINGLE, household, resolver=SingleSimResolver(sim_infos[0]))
                else:
                    self._show_notification(DaycareTuning.SEND_CHILD_TO_NANNY_NOTIFICATION_MULTIPLE, household)
                    if len(sim_infos) == 1:
                        self._show_notification(DaycareTuning.BRING_CHILD_BACK_FROM_NANNY_NOTIFICATION_SINGLE, household, resolver=SingleSimResolver(sim_infos[0]))
                    else:
                        self._show_notification(DaycareTuning.BRING_CHILD_BACK_FROM_NANNY_NOTIFICATION_MULTIPLE, household)
            elif len(sim_infos) == 1:
                self._show_notification(DaycareTuning.BRING_CHILD_BACK_FROM_NANNY_NOTIFICATION_SINGLE, household, resolver=SingleSimResolver(sim_infos[0]))
            else:
                self._show_notification(DaycareTuning.BRING_CHILD_BACK_FROM_NANNY_NOTIFICATION_MULTIPLE, household)

    def _show_daycare_notification(self, household, sim_infos, is_enable=False):
        if sim_infos:
            if is_enable:
                if len(sim_infos) == 1:
                    self._show_notification(DaycareTuning.SEND_BABY_TO_DAYCARE_NOTIFICATION_SINGLE_BABY, household, resolver=SingleSimResolver(sim_infos[0]))
                else:
                    self._show_notification(DaycareTuning.SEND_BABY_TO_DAYCARE_NOTIFICATION_MULTIPLE_BABIES, household)
                    if len(sim_infos) == 1:
                        self._show_notification(DaycareTuning.BRING_BABY_BACK_FROM_DAYCARE_NOTIFICATION_SINGLE_BABY, household, resolver=SingleSimResolver(sim_infos[0]))
                    else:
                        self._show_notification(DaycareTuning.BRING_BABY_BACK_FROM_DAYCARE_NOTIFICATION_MULTIPLE_BABIES, household)
            elif len(sim_infos) == 1:
                self._show_notification(DaycareTuning.BRING_BABY_BACK_FROM_DAYCARE_NOTIFICATION_SINGLE_BABY, household, resolver=SingleSimResolver(sim_infos[0]))
            else:
                self._show_notification(DaycareTuning.BRING_BABY_BACK_FROM_DAYCARE_NOTIFICATION_MULTIPLE_BABIES, household)

    def _show_notification(self, notification, household, resolver=None):
        if not services.current_zone().is_zone_running:
            return
        if household.is_npc_household:
            return
        active_sim = services.get_active_sim()
        if active_sim is None:
            return
        resolver = SingleSimResolver(active_sim) if resolver is None else resolver
        dialog = notification(active_sim, resolver)
        dialog.show_dialog()

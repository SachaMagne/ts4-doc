from protocolbuffers import Consts_pb2
from event_testing import test_events
from event_testing.event_data_const import SimoleonData
from retail.retail_utils import RetailUtils
from sims4.localization import TunableLocalizedStringFactory
from sims4.tuning.tunable import TunableRange, TunableMapping, TunableEnumEntry
import distributor.ops
import enum
import services
import sims4.log
import sims4.telemetry
import telemetry_helper
logger = sims4.log.Logger('Family Funds', default_owner='trevor')
TELEMETRY_GROUP_FUNDS = 'FUND'
TELEMETRY_HOOK_POCKET = 'POKT'
TELEMETRY_HOOK_FUNDS_CHANGE = 'FMOD'
TELEMETRY_AMOUNT = 'amnt'
TELEMETRY_REASON = 'resn'
TELEMETRY_FUND_AMOUNT = 'fund'
writer = sims4.telemetry.TelemetryWriter(TELEMETRY_GROUP_FUNDS)

class FundsSource(enum.Int):
    __qualname__ = 'FundsSource'
    HOUSEHOLD = 0
    RETAIL = 1

class FundsTuning:
    __qualname__ = 'FundsTuning'
    UNAFFORDABLE_TOOLTIPS = TunableMapping(description='\n        A mapping of tooltips for each of the funds sources when an interaction\n        cannot be performed due to lack of such funds. Each tooltip takes the\n        same tokens as the interaction.\n        ', key_type=TunableEnumEntry(description='\n            The funds source.\n            ', tunable_type=FundsSource, default=FundsSource.HOUSEHOLD), value_type=TunableLocalizedStringFactory(description='\n            The tooltip to display when a Sim cannot run the specified\n            interaction due to lack of funds.\n            '))

def get_funds_for_source(funds_source, *, sim):
    if funds_source == FundsSource.HOUSEHOLD:
        return sim.family_funds
    if funds_source == FundsSource.RETAIL:
        retail_manager = RetailUtils.get_retail_manager_for_zone()
        if retail_manager is None:
            return
        return retail_manager.funds
    raise NotImplementedError

class ReservedFunds:
    __qualname__ = 'ReservedFunds'
    __slots__ = ('_apply_function', '_cancel_function', '_amount')

    def __init__(self, apply_function, cancel_function, amount):
        self.set_apply_function(apply_function)
        self._cancel_function = cancel_function
        self._amount = amount

    def get_apply_function(self):
        return self._apply_function

    def set_apply_function(self, apply_function):
        self._apply_function = apply_function

    def apply(self):
        if self._apply_function:
            self._apply_function()
            self._cancel_function = None
            self._apply_function = None

    def cancel(self, from_gc=False):
        if self._cancel_function:
            if from_gc:
                logger.warn('Refund being issued by garbage collection of a ReservedFunds object.')
            self._cancel_function()
            self._cancel_function = None
            self._apply_function = None

    def __del__(self):
        self.cancel(True)

    @property
    def amount(self):
        return self._amount

class _Funds:
    __qualname__ = '_Funds'
    __slots__ = ('_household', '_funds', '_reserved')
    MAX_FUNDS = TunableRange(int, 99999999, 0, sims4.math.MAX_INT32, description='Max Funds a household can have.')

    def __init__(self, household, starting_amount):
        self._household = household
        self._funds = starting_amount
        self._reserved = 0

    @property
    def money(self):
        return self._funds - self._reserved

    @property
    def funds_transfer_gain_reason(self):
        raise NotImplementedError

    @property
    def funds_transfer_loss_reason(self):
        raise NotImplementedError

    @property
    def allow_negative_funds(self):
        return False

    def send_money_update(self, vfx_amount, sim=None, reason=0):
        raise NotImplementedError

    def _send_money_update_internal(self, household, vfx_amount, sim=None, reason=0):
        op = distributor.ops.SetMoney(self.money, vfx_amount, sim, reason)
        distributor.ops.record(household, op)

    def try_remove(self, amount, reason, sim):
        amount = round(amount)
        if sim is None:
            raise ValueError('None Sim.')
        is_npc = sim.is_npc
        if self.money < amount and not is_npc:
            return
        if amount == 0 or is_npc:
            apply_removal = None
            cancel_removal = None
        elif amount < 0:

            def apply_removal():
                self.add(amount, reason, sim)

            cancel_removal = None
        else:
            self.send_money_update(vfx_amount=-amount, reason=reason)

            def apply_removal():
                self._update_money(-amount, reason, sim, show_fx=False)

            def cancel_removal():
                self.send_money_update(vfx_amount=amount, reason=reason)

        return ReservedFunds(apply_removal, cancel_removal, amount)

    def remove(self, amount, reason, sim=None):
        amount = round(amount)
        if amount < 0:
            logger.error('Attempt to remove negative amount of money from Family Funds.')
            return
        sim_account_id = self._get_sim_account_id(sim)
        if sim_account_id is None:
            logger.info('Attempt to remove funds with no client connected')
            return
        self._update_money(-amount, reason, sim)

    @staticmethod
    def transfer_funds(amount, from_funds, to_funds):
        if from_funds.money < amount:
            return False
        from_funds.remove(amount, reason=from_funds.funds_transfer_loss_reason)
        to_funds.add(amount, reason=to_funds.funds_transfer_gain_reason)
        return True

    def _get_sim_account_id(self, sim):
        if sim is None:
            client = services.client_manager().get_client_by_household_id(self._household.id)
            if client is not None:
                return client.account.id
            return
        return sim.account_id

    def _update_money(self, amount, reason, sim=None, tags=None, count_as_earnings=True, show_fx=True):
        if amount == 0:
            return
        self._funds = min(self._funds + amount, self.MAX_FUNDS)
        if self._funds < 0 and not self.allow_negative_funds:
            logger.error('Negative funds amount ({}) not supported', self._funds)
            self._funds = 0
        vfx_amount = amount if show_fx else 0
        self.send_money_update(vfx_amount=vfx_amount, sim=sim, reason=reason)
        with telemetry_helper.begin_hook(writer, TELEMETRY_HOOK_FUNDS_CHANGE, sim=sim) as hook:
            hook.write_int(TELEMETRY_AMOUNT, amount)
            hook.write_int(TELEMETRY_REASON, reason)
            hook.write_int(TELEMETRY_FUND_AMOUNT, self._funds)
        if count_as_earnings and amount > 0:
            if sim is None:
                services.get_event_manager().process_events_for_household(test_events.TestEvent.SimoleonsEarned, self._household, simoleon_data_type=SimoleonData.TotalMoneyEarned, amount=amount, skill_used=None, tags=tags)
            else:
                services.get_event_manager().process_event(test_events.TestEvent.SimoleonsEarned, sim_info=sim.sim_info, simoleon_data_type=SimoleonData.TotalMoneyEarned, amount=amount, skill_used=None, tags=tags)
                services.get_event_manager().process_events_for_household(test_events.TestEvent.SimoleonsEarned, self._household, simoleon_data_type=SimoleonData.TotalMoneyEarned, amount=0, skill_used=None, tags=frozenset(), exclude_sim=sim.sim_info)

class FamilyFunds(_Funds):
    __qualname__ = 'FamilyFunds'
    __slots__ = set()

    @property
    def funds_transfer_gain_reason(self):
        return Consts_pb2.FUNDS_HOUSEHOLD_TRANSFER_GAIN

    @property
    def funds_transfer_loss_reason(self):
        return Consts_pb2.FUNDS_HOUSEHOLD_TRANSFER_LOSS

    def send_money_update(self, vfx_amount, sim=None, reason=0):
        self._send_money_update_internal(self._household, vfx_amount, sim, reason)

    def add(self, amount, reason, sim=None, tags=None, count_as_earnings=True):
        amount = round(amount)
        if amount < 0:
            logger.error('Attempt to add negative amount of money to Family Funds.')
            return
        if sim is None:
            client = services.client_manager().get_client_by_household_id(self._household.id)
            if client is not None:
                self._update_money(amount, reason, tags=tags, count_as_earnings=count_as_earnings)
                return
            logger.callstack('Attempt to raise household funds on a house with no client connected.', level=sims4.log.LEVEL_INFO)
        elif sim.household is not self._household:
            logger.error('Attempt to add funds to the wrong household.')
            return
        self._update_money(amount, reason, sim, tags=tags, count_as_earnings=count_as_earnings)

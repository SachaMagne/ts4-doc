import services
import sims4.tuning.tunable
import tag
import traits.traits

class Ghost:
    __qualname__ = 'Ghost'
    URNSTONE_TAG_FILTER_PREFIXES = ('func',)
    URNSTONE_TAG = sims4.tuning.tunable.TunableEnumWithFilter(description='\n        The tag associated with urns and tombstone. They all need this tag if\n        they want to be considered for an NPC ghost to spawn.\n        ', tunable_type=tag.Tag, filter_prefixes=URNSTONE_TAG_FILTER_PREFIXES, default=tag.Tag.INVALID)
    NETHERWORLD_TRAIT = traits.traits.Trait.TunableReference(description='\n        A trait that describes a Sim that has been sent to the Netherworld. Sims\n        with this trait are not available for any filter and cannot be\n        interacted with. They will never instantiate and are effectively "gone."\n        ')
    WALKBY_COOLDOWN = sims4.tuning.tunable.TunableSimMinute(description='\n        The amount of time the ghost must wait before performing another\n        walkby. The cooldown time starts when the ghost is uninstantiated.\n        ', default=48)

    @classmethod
    def _check_urnstone_validity(cls, urnstone, require_uninstantiated=True):
        stored_sim_info = urnstone.get_stored_sim_info()
        if stored_sim_info is None:
            return False
        if not require_uninstantiated:
            return True
        if stored_sim_info.is_instanced():
            return False
        if cls._is_ghost_on_ambient_cooldown(stored_sim_info):
            return False
        return True

    @classmethod
    def _is_ghost_on_ambient_cooldown(cls, sim_info):
        saved_time = sim_info.time_sim_was_saved
        if saved_time is None:
            return True
        time_delta = services.time_service().sim_now - saved_time
        return time_delta.in_minutes() <= cls.WALKBY_COOLDOWN

    @classmethod
    def get_urnstones_gen(cls):
        yield services.object_manager().get_objects_with_tag_gen(cls.URNSTONE_TAG)

    @classmethod
    def get_valid_urnstones(cls, require_uninstantiated=True):
        return list(u for u in cls.get_urnstones_gen() if cls._check_urnstone_validity(u, require_uninstantiated=require_uninstantiated))

    @classmethod
    def get_urnstone_for_sim_id(cls, sim_id):
        for urnstone in cls.get_urnstones_gen():
            while urnstone.get_stored_sim_id() == sim_id:
                return urnstone

    @classmethod
    def remove_ghost_from_sim(cls, sim_info):
        sim_info.trait_tracker.remove_traits_of_type(traits.traits.TraitType.GHOST)
        sim_info.death_tracker.clear_death_type()
        sim = sim_info.get_sim_instance()
        if sim is not None:
            sim.routing_context.ghost_route = False
        sim_info.update_age_callbacks()

    @classmethod
    def make_ghost_if_needed(cls, sim_info):
        trait = sim_info.death_tracker.get_ghost_trait()
        if trait is not None:
            sim_info.add_trait(trait)
            sim = sim_info.get_sim_instance()
            if sim is not None:
                sim.routing_context.ghost_route = True

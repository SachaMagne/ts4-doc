import random
from protocolbuffers import SimObjectAttributes_pb2 as protocols
from algos import count_bits
from cas.cas import generate_occult_siminfo
from distributor.rollback import ProtocolBufferRollback
from sims.outfits.outfit_enums import REGULAR_OUTFIT_CATEGORIES, HIDDEN_OUTFIT_CATEGORIES, BodyTypeFlag, OutfitCategory
from sims.sim_info_base_wrapper import SimInfoBaseWrapper
from sims4.tuning.tunable import TunableMapping, TunableEnumEntry, TunableTuple
from sims4.tuning.tunable_base import ExportModes
from traits.trait_tracker import HasTraitTrackerMixin
from traits.traits import Trait
import distributor.fields
import distributor.ops
import enum

class OccultType(enum.IntFlags):
    __qualname__ = 'OccultType'
    HUMAN = 1
    ALIEN = 2

class SimInfoWithOccultTracker(HasTraitTrackerMixin, SimInfoBaseWrapper):
    __qualname__ = 'SimInfoWithOccultTracker'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._occult_tracker = OccultTracker(self)
        self._base.occult_types = OccultType.HUMAN
        self._base.current_occult_types = OccultType.HUMAN

    @property
    def occult_tracker(self):
        return self._occult_tracker

    @distributor.fields.Field(op=distributor.ops.SetOccultTypes)
    def occult_types(self):
        return OccultType(self._base.occult_types)

    @occult_types.setter
    def occult_types(self, value):
        if self._base.occult_types != value:
            self._base.occult_types = value

    @distributor.fields.Field(op=distributor.ops.SetCurrentOccultTypes)
    def current_occult_types(self):
        return OccultType(self._base.current_occult_types)

    @current_occult_types.setter
    def current_occult_types(self, value):
        if self._base.current_occult_types != value:
            self._base.current_occult_types = value

    def apply_age(self, age):
        return self._occult_tracker.apply_occult_age(age)

    def apply_genetics(self, parent_a, parent_b, seed, **kwargs):
        return self._occult_tracker.apply_occult_genetics(parent_a, parent_b, seed=seed, **kwargs)

    def add_trait(self, trait, **kwargs):
        success = super().add_trait(trait, **kwargs)
        if success:
            for (occult_type, trait_data) in OccultTracker.OCCULT_DATA.items():
                if trait_data.occult_trait is trait:
                    self._occult_tracker.add_occult_type(occult_type)
                    if self.is_baby:
                        self.add_trait(trait_data.current_occult_trait)
                while trait_data.current_occult_trait is trait:
                    self._occult_tracker.switch_to_occult_type(occult_type)
        self.trait_tracker.update_voice_effect()
        return success

    def remove_trait(self, trait, **kwargs):
        success = super().remove_trait(trait, **kwargs)
        if success:
            for (occult_type, trait_data) in OccultTracker.OCCULT_DATA.items():
                if trait_data.occult_trait is trait:
                    self._occult_tracker.remove_occult_type(occult_type)
                while trait_data.current_occult_trait is trait:
                    self._occult_tracker.switch_to_occult_type(OccultType.HUMAN)
        self.trait_tracker.update_voice_effect()
        return success

class OccultTracker:
    __qualname__ = 'OccultTracker'
    OCCULT_DATA = TunableMapping(description="\n        A mapping of occult types to data that affect a Sim's behavior.\n        ", key_type=TunableEnumEntry(description='\n            The occult type that this entry applies to.\n            ', tunable_type=OccultType, default=OccultType.HUMAN), value_type=TunableTuple(description='\n            Occult data specific to this occult type.\n            ', occult_trait=Trait.TunableReference(description='\n                The trait that all Sims that have this occult are equipped with.\n                ', pack_safe=True), current_occult_trait=Trait.TunableReference(description='\n                That trait that all Sims currently in this occult are equipped\n                with.\n                ', pack_safe=True), part_occult_trait=Trait.TunableReference(description='\n                The trait that identifies a Sim that is partly occult. For any\n                part occult trait, we will apply genetics that are half occult\n                and half non-occult.\n                ', pack_safe=True)), export_modes=(ExportModes.ServerXML, ExportModes.ClientBinary))

    def __init__(self, sim_info):
        self._sim_info = sim_info
        self._sim_info_map = dict()
        self._pending_occult_type = None

    def __repr__(self):
        return '<OccultTracker: {} ({}) @{}>'.format(str(self._sim_info), self._sim_info.occult_types, self._sim_info.current_occult_types)

    @property
    def sim_info(self):
        return self._sim_info

    def add_occult_type(self, occult_type):
        if not self.has_occult_type(occult_type):
            self._update_occult_traits()

    def remove_occult_type(self, occult_type):
        if occult_type == self._sim_info.current_occult_types:
            self.switch_to_occult_type(OccultType.HUMAN)
        if self.has_occult_type(occult_type):
            self._update_occult_traits()
        if occult_type in self._sim_info_map:
            del self._sim_info_map[occult_type]

    def switch_to_occult_type(self, occult_type):
        if occult_type not in self._sim_info_map:
            self.add_occult_type(occult_type)
            self._generate_sim_info(occult_type)
        if self._sim_info.current_occult_types != occult_type:
            self._switch_to_occult_type_internal(occult_type)
            self._update_occult_traits()

    def set_pending_occult_type(self, occult_type):
        self._pending_occult_type = occult_type

    def _switch_to_occult_type_internal(self, occult_type):
        current_outfit = self._sim_info.get_current_outfit()
        current_sim_info = self._sim_info_map[self._sim_info.current_occult_types]
        current_sim_info.sim_outfits.load_sim_outfits_from_persistence_proto(current_sim_info.sim_id, self._sim_info.sim_outfits.save_sim_outfits())
        current_sim_info.serialize_and_set_base_outfits()
        occult_sim_info = self._sim_info_map[occult_type]
        self._copy_shared_attributes(occult_sim_info, self._sim_info)
        self._copy_physical_attributes(self._sim_info._base, occult_sim_info)
        sim_outfits = self._sim_info.sim_outfits
        sim_outfits.load_sim_outfits_from_persistence_proto(self._sim_info.sim_id, occult_sim_info.sim_outfits.save_sim_outfits())
        self._sim_info.serialize_and_set_base_outfits()
        if sim_outfits.has_outfit(current_outfit):
            self._sim_info.set_current_outfit(current_outfit)
        else:
            (outfit_category, outfit_index) = current_outfit
            if not sim_outfits.has_outfit((outfit_category, outfit_index)):
                outfit_category = OutfitCategory.EVERYDAY
            self._sim_info.set_current_outfit((outfit_category, outfit_index))
        self._sim_info.current_occult_types = occult_type
        self._sim_info.appearance_tracker.evaluate_appearance_modifiers()
        sim_instance = self._sim_info.get_sim_instance()
        if sim_instance is not None:
            sim_instance.on_outfit_changed(self._sim_info.get_current_outfit())
        self._sim_info.resend_physical_attributes()
        self._sim_info.resend_current_outfit()

    def has_occult_type(self, occult_type):
        if self._sim_info.occult_types & occult_type:
            return True
        return False

    def get_occult_sim_info(self, occult_type):
        return self._sim_info_map.get(occult_type)

    def _generate_sim_info(self, occult_type, generate_new=True):
        if not self._sim_info_map and occult_type != OccultType.HUMAN:
            self._generate_sim_info(OccultType.HUMAN)
        sim_info = SimInfoBaseWrapper(gender=self._sim_info.gender, age=self._sim_info.age, first_name=self._sim_info.first_name, last_name=self._sim_info.last_name, full_name_key=self._sim_info.full_name_key, physique=self._sim_info.physique, skin_tone=self._sim_info.skin_tone)
        self._copy_physical_attributes(sim_info._base, self._sim_info)
        if generate_new:
            generate_occult_siminfo(sim_info._base, sim_info._base, occult_type)
            for outfit_category in REGULAR_OUTFIT_CATEGORIES:
                sim_info.generate_outfit(outfit_category=outfit_category)
            self._copy_shared_attributes(sim_info, self._sim_info)
        sim_info._base.current_occult_types = occult_type
        self._sim_info_map[occult_type] = sim_info
        return sim_info

    @staticmethod
    def _copy_physical_attributes(sim_info_a, sim_info_b):
        sim_info_a.physique = sim_info_b.physique
        sim_info_a.facial_attributes = sim_info_b.facial_attributes
        sim_info_a.voice_pitch = sim_info_b.voice_pitch
        sim_info_a.voice_actor = sim_info_b.voice_actor
        sim_info_a.voice_effect = sim_info_b.voice_effect
        sim_info_a.skin_tone = sim_info_b.skin_tone
        OccultTracker._copy_genetic_data(sim_info_a, sim_info_b)

    @staticmethod
    def _copy_genetic_data(sim_info_a, sim_info_b):
        genetic_data_b = sim_info_b.genetic_data
        if hasattr(genetic_data_b, 'SerializeToString'):
            genetic_data_b = genetic_data_b.SerializeToString()
        if hasattr(sim_info_a.genetic_data, 'MergeFromString'):
            sim_info_a.genetic_data.MergeFromString(genetic_data_b)
        else:
            sim_info_a.genetic_data = genetic_data_b

    @staticmethod
    def _copy_shared_attributes(sim_info_a, sim_info_b):
        sim_info_a.physique = sim_info_b.physique
        for outfit_category in HIDDEN_OUTFIT_CATEGORIES:
            sim_info_a.generate_merged_outfits_for_category(sim_info_b, outfit_category, outfit_flags=BodyTypeFlag.CLOTHING_ALL)

    def _update_occult_traits(self):
        for (occult_type, trait_data) in self.OCCULT_DATA.items():
            if self.has_occult_type(occult_type):
                self._sim_info.add_trait(trait_data.occult_trait)
                if self._sim_info.current_occult_types == occult_type:
                    self._sim_info.add_trait(trait_data.current_occult_trait)
                else:
                    self._sim_info.remove_trait(trait_data.current_occult_trait)
                    self._sim_info.remove_trait(trait_data.current_occult_trait)
                    self._sim_info.remove_trait(trait_data.occult_trait)
            else:
                self._sim_info.remove_trait(trait_data.current_occult_trait)
                self._sim_info.remove_trait(trait_data.occult_trait)

    def apply_occult_age(self, age):
        if not self._sim_info_map:
            return SimInfoBaseWrapper.apply_age(self.sim_info, age)
        for (occult_type, sim_info) in self._sim_info_map.items():
            if occult_type == self._sim_info.current_occult_types:
                SimInfoBaseWrapper.apply_age(self.sim_info, age)
                sim_info.age = age
                self._copy_physical_attributes(sim_info, self._sim_info)
            else:
                SimInfoBaseWrapper.apply_age(sim_info, age)

    def apply_occult_genetics(self, parent_a, parent_b, seed, **kwargs):
        r = random.Random()
        r.seed(seed)
        if r.random() < 0.5:
            (occult_tracker_a, occult_tracker_b) = (parent_a.occult_tracker, parent_b.occult_tracker)
        else:
            (occult_tracker_a, occult_tracker_b) = (parent_b.occult_tracker, parent_a.occult_tracker)
        parent_a_normal = occult_tracker_a.get_occult_sim_info(OccultType.HUMAN) or occult_tracker_a.sim_info
        parent_b_normal = occult_tracker_b.get_occult_sim_info(OccultType.HUMAN) or occult_tracker_b.sim_info
        normal_sim_info = self.get_occult_sim_info(OccultType.HUMAN) or self._sim_info
        SimInfoBaseWrapper.apply_genetics(normal_sim_info, parent_a_normal, parent_b_normal, seed=seed, **kwargs)
        for (occult_type, trait_data) in self.OCCULT_DATA.items():
            if self.has_occult_type(occult_type):
                parent_info_a = occult_tracker_a.get_occult_sim_info(occult_type) or occult_tracker_a.sim_info
                parent_info_b = occult_tracker_b.get_occult_sim_info(occult_type) or occult_tracker_b.sim_info
                offspring_info = self.get_occult_sim_info(occult_type) or self._generate_sim_info(occult_type)
                if occult_type == self._sim_info.current_occult_types:
                    SimInfoBaseWrapper.apply_genetics(self._sim_info, parent_info_a, parent_info_b, seed=seed, **kwargs)
                    self._copy_physical_attributes(offspring_info, self._sim_info)
                else:
                    SimInfoBaseWrapper.apply_genetics(offspring_info, parent_info_a, parent_info_b, seed=seed, **kwargs)
            while self._sim_info.has_trait(trait_data.part_occult_trait):
                if occult_tracker_a.has_occult_type(occult_type):
                    parent_info_a = occult_tracker_a.get_occult_sim_info(occult_type) or parent_a_normal
                    parent_info_b = parent_b_normal
                else:
                    parent_info_a = parent_a_normal
                    parent_info_b = occult_tracker_b.get_occult_sim_info(occult_type) or parent_b_normal
                SimInfoBaseWrapper.apply_genetics(normal_sim_info, parent_info_a, parent_info_b, seed=seed, **kwargs)
        if not self._sim_info.current_occult_types:
            self._copy_physical_attributes(normal_sim_info, self._sim_info)

    def save(self):
        data = protocols.PersistableOccultTracker()
        data.occult_types = self._sim_info.occult_types
        data.current_occult_types = self._sim_info.current_occult_types
        if self._pending_occult_type is not None:
            data.pending_occult_type = self._pending_occult_type
        for (occult_type, sim_info) in self._sim_info_map.items():
            with ProtocolBufferRollback(data.occult_sim_infos) as sim_info_data:
                self._copy_shared_attributes(sim_info, self._sim_info)
                sim_info_data.occult_type = occult_type
                sim_info_data.outfits = sim_info.sim_outfits.save_sim_outfits()
                self._copy_physical_attributes(sim_info_data, sim_info)
        return data

    def load(self, data):
        self._sim_info.occult_types = data.occult_types or OccultType.HUMAN
        self._sim_info.current_occult_types = data.current_occult_types or OccultType.HUMAN
        self._pending_occult_type = data.pending_occult_type
        occult_data_map = {}
        for sim_info_data in data.occult_sim_infos:
            occult_data_map[sim_info_data.occult_type] = sim_info_data
        for occult_type in OccultType:
            if occult_type != OccultType.HUMAN and occult_type not in self.OCCULT_DATA:
                if self._sim_info.current_occult_types == occult_type:
                    self._sim_info.current_occult_types = OccultType.HUMAN
                if self._pending_occult_type == occult_type:
                    self._pending_occult_type = None
                    if occult_type in occult_data_map:
                        sim_info_data = occult_data_map[occult_type]
                        sim_info = self._generate_sim_info(sim_info_data.occult_type, generate_new=False)
                        if occult_type == self._sim_info.current_occult_types:
                            self._copy_physical_attributes(sim_info_data, self._sim_info._base)
                        else:
                            sim_info.sim_outfits.load_sim_outfits_from_persistence_proto(sim_info.sim_id, sim_info_data.outfits)
                            sim_info.serialize_and_set_base_outfits()
                            self._copy_physical_attributes(sim_info._base, sim_info_data)
                            while occult_type != OccultType.HUMAN and self.has_occult_type(occult_type):
                                if occult_type == self._sim_info.current_occult_types:
                                    self._generate_sim_info(occult_type, generate_new=False)
                    else:
                        while occult_type != OccultType.HUMAN and self.has_occult_type(occult_type):
                            if occult_type == self._sim_info.current_occult_types:
                                self._generate_sim_info(occult_type, generate_new=False)
            elif occult_type in occult_data_map:
                sim_info_data = occult_data_map[occult_type]
                sim_info = self._generate_sim_info(sim_info_data.occult_type, generate_new=False)
                if occult_type == self._sim_info.current_occult_types:
                    self._copy_physical_attributes(sim_info_data, self._sim_info._base)
                else:
                    sim_info.sim_outfits.load_sim_outfits_from_persistence_proto(sim_info.sim_id, sim_info_data.outfits)
                    sim_info.serialize_and_set_base_outfits()
                    self._copy_physical_attributes(sim_info._base, sim_info_data)
                    while occult_type != OccultType.HUMAN and self.has_occult_type(occult_type):
                        if occult_type == self._sim_info.current_occult_types:
                            self._generate_sim_info(occult_type, generate_new=False)
            else:
                while occult_type != OccultType.HUMAN and self.has_occult_type(occult_type):
                    if occult_type == self._sim_info.current_occult_types:
                        self._generate_sim_info(occult_type, generate_new=False)
        if self._sim_info_map:
            self._update_occult_traits()
            self._switch_to_occult_type_internal(self._sim_info.current_occult_types)
        if self._pending_occult_type:
            self.switch_to_occult_type(self._pending_occult_type)
            self._pending_occult_type = None

from weakref import WeakKeyDictionary
import random
from element_utils import build_critical_section
from event_testing.resolver import SingleSimResolver
from event_testing.tests import TunableGlobalTestSet
from interactions import ParticipantType, ParticipantTypeSingle
from interactions.utils.animation import flush_all_animations
from sims.outfits.outfit_enums import OutfitCategory, FilterFlag, SpecialOutfitIndex
from sims.outfits.outfit_tuning import get_maximum_outfits_for_category
from sims.sim_outfits import OutfitChangeReason
from sims4.tuning.tunable import AutoFactoryInit, HasTunableSingletonFactory, TunableVariant, OptionalTunable, TunableEnumEntry, TunableSet, HasTunableFactory, TunableTuple, Tunable, TunableList
from singletons import DEFAULT
from tag import Tag
import sims4.log
logger = sims4.log.Logger('SimOutfits', default_owner='rfleig')

class OutfitChangeBase(HasTunableSingletonFactory, AutoFactoryInit):
    __qualname__ = 'OutfitChangeBase'

    def __bool__(self):
        return True

    def get_on_entry_change(self, interaction, **kwargs):
        raise NotImplementedError

    def get_on_exit_change(self, interaction, **kwargs):
        raise NotImplementedError

    def get_on_entry_outfit(self, interaction, **kwargs):
        raise NotImplementedError

    def get_on_exit_outfit(self, interaction, **kwargs):
        raise NotImplementedError

class TunableOutfitChange(TunableVariant):
    __qualname__ = 'TunableOutfitChange'

    class _OutfitChangeNone(OutfitChangeBase):
        __qualname__ = 'TunableOutfitChange._OutfitChangeNone'

        def __bool__(self):
            return False

        def get_on_entry_change(self, interaction, **kwargs):
            pass

        def get_on_exit_change(self, interaction, **kwargs):
            pass

        def get_on_entry_outfit(self, interaction, **kwargs):
            pass

        def get_on_exit_outfit(self, interaction, **kwargs):
            pass

    class _OutfitChangeForReason(OutfitChangeBase):
        __qualname__ = 'TunableOutfitChange._OutfitChangeForReason'
        FACTORY_TUNABLES = {'on_entry': OptionalTunable(description='\n                When enabled, define the change reason to apply on posture\n                entry.\n                ', tunable=TunableEnumEntry(tunable_type=OutfitChangeReason, default=OutfitChangeReason.Invalid)), 'on_exit': OptionalTunable(description='\n                When enabled, define the change reason to apply on posture\n                exit.\n                ', tunable=TunableEnumEntry(tunable_type=OutfitChangeReason, default=OutfitChangeReason.Invalid))}

        def get_on_entry_change(self, interaction, sim_info=DEFAULT, **kwargs):
            sim_info = interaction.sim.sim_info if sim_info is DEFAULT else sim_info
            return sim_info.sim_outfits.get_change(interaction, self.on_entry, **kwargs)

        def get_on_exit_change(self, interaction, sim_info=DEFAULT, **kwargs):
            sim_info = interaction.sim.sim_info if sim_info is DEFAULT else sim_info
            return sim_info.sim_outfits.get_change(interaction, self.on_exit, **kwargs)

        def get_on_entry_outfit(self, interaction, sim_info=DEFAULT):
            if self.on_entry is not None:
                if sim_info is DEFAULT:
                    sim_info = interaction.sim.sim_info
                    resolver = None
                else:
                    resolver = SingleSimResolver(sim_info)
                return sim_info.sim_outfits.get_outfit_for_clothing_change(interaction, self.on_entry, resolver=resolver)

        def get_on_exit_outfit(self, interaction, sim_info=DEFAULT):
            if self.on_exit is not None:
                if sim_info is DEFAULT:
                    sim_info = interaction.sim.sim_info
                    resolver = None
                else:
                    resolver = SingleSimResolver(sim_info)
                return sim_info.sim_outfits.get_outfit_for_clothing_change(interaction, self.on_exit, resolver=resolver)

    class _OutfitChangeForTags(OutfitChangeBase):
        __qualname__ = 'TunableOutfitChange._OutfitChangeForTags'

        @staticmethod
        def _verify_tunable_callback(instance_class, tunable_name, source, value, **kwargs):
            if value.on_entry and value.on_entry.auto_undo_on_exit and value.on_exit is not None:
                logger.error('{} has tuned both on_entry.auto_undo_on_exit and on_exit in a For Tags outfit change. These two things conflict.', instance_class, owner='rfleig')

        FACTORY_TUNABLES = {'on_entry': OptionalTunable(description='\n                The tuning for how to handle the outfit change on entry of\n                the new context.\n                ', tunable=TunableTuple(description='\n                    Contains the tags used to tune the outfit and also\n                    a preference for whether or not to automatically switch out\n                    of the tags outfit when on exit.\n                    ', tag_set=TunableSet(description='\n                        Tags that determine which outfit parts are valid\n                        ', tunable=TunableEnumEntry(tunable_type=Tag, default=Tag.INVALID)), auto_undo_on_exit=Tunable(description="\n                        If True then the Sim will switch out of the entry tag\n                        outfit on exit. \n                        If False then the Sim will stay in the tag outfit.\n                        \n                        NOTE: This tuning conflicts with the On Exit tuning. If\n                        this is set to true and On Exit is enabled then an \n                        error should occur on load because you can't both switch\n                        out of the tag outfit and switch into a different tag\n                        outfit.\n                        ", tunable_type=bool, default=True)), enabled_by_default=True), 'on_exit': OptionalTunable(description='\n                The clothing change that happens on exit of the current context.\n                ', tunable=TunableList(description='\n                    A list of (tests, clothing change) tuples. The first entry\n                    that passes all of its tests will be used while the other\n                    entries after that one will be ignored. So the order of the \n                    list is essentially priority.\n                    ', tunable=TunableTuple(description='\n                        A tuple of clothing changes and tests for whether they\n                        should happen or not.\n                        ', tag_set=TunableSet(description='\n                            Tags that determine which outfit parts are valid\n                            ', tunable=TunableEnumEntry(tunable_type=Tag, default=Tag.INVALID)), tests=TunableGlobalTestSet(description='\n                            Tests to run when deciding which clothing change\n                            entry to use. All of the tests must pass in order \n                            for the item to pass.\n                            ')))), 'special_outfit_index': TunableEnumEntry(description='\n                The Special outfit index to use when creating the outfit using\n                the provided flags. There are multiple Special outfits that \n                are indexed by the entries in the SpecialOutfitIndex enum.\n                \n                GPE NOTE:\n                If you want to add a new index you will need to add a value\n                to SpecialOutfitIndex as well as change the values in \n                outfit_tuning.py and OutfitTypes.h to allow for more special\n                outfits.\n                ', tunable_type=SpecialOutfitIndex, default=SpecialOutfitIndex.DEFAULT), 'verify_tunable_callback': _verify_tunable_callback}

        def get_on_entry_change(self, interaction, sim_info=DEFAULT, **kwargs):
            if not self.on_entry:
                return
            sim_info = interaction.sim.sim_info if sim_info is DEFAULT else sim_info
            for trait in sim_info.get_traits():
                outfit_change_reason = trait.get_outfit_change_reason(None)
                while outfit_change_reason is not None:
                    return sim_info.sim_outfits.get_change(interaction, outfit_change_reason, **kwargs)
            self._generate_outfits(sim_info, self.special_outfit_index, self.on_entry.tag_set)
            return build_critical_section(sim_info.sim_outfits.get_change_outfit_element((OutfitCategory.SPECIAL, self.special_outfit_index), **kwargs), flush_all_animations)

        def get_on_exit_change(self, interaction, sim_info=DEFAULT, **kwargs):
            sim_info = interaction.sim.sim_info if sim_info is DEFAULT else sim_info
            if not self.on_exit and self.on_entry is not None and self.on_entry.auto_undo_on_exit:
                return sim_info.sim_outfits.get_change(interaction, OutfitChangeReason.PreviousClothing, **kwargs)
            if self.on_exit:
                choice = self.choose_on_exit_clothing_change(sim_info)
                if choice is None:
                    return
                self._generate_outfits(sim_info, self.special_outfit_index, choice.tag_set)
                return build_critical_section(sim_info.sim_outfits.get_change_outfit_element((OutfitCategory.SPECIAL, self.special_outfit_index), **kwargs), flush_all_animations)

        def choose_on_exit_clothing_change(self, sim_info):
            resolver = SingleSimResolver(sim_info)
            for outfit_change in self.on_exit:
                result = outfit_change.tests.run_tests(resolver)
                while result:
                    return outfit_change

        def get_on_entry_outfit(self, interaction, sim_info=DEFAULT):
            if self.on_entry is not None:
                return (OutfitCategory.SPECIAL, self.special_outfit_index)

        def get_on_exit_outfit(self, interaction, sim_info=DEFAULT):
            if sim_info is DEFAULT:
                sim_info = interaction.sim.sim_info
                resolver = None
            else:
                resolver = SingleSimResolver(sim_info)
            if not self.on_exit and self.on_entry is not None and self.on_entry.auto_undo_on_exit:
                return sim_info.sim_outfits.get_outfit_for_clothing_change(interaction, OutfitChangeReason.PreviousClothing, resolver=resolver)
            if self.on_exit:
                choice = self.choose_on_exit_clothing_change(sim_info)
                if choice is None:
                    return
                self._generate_outfits(sim_info, self.special_outfit_index, choice.tag_set)
                return (OutfitCategory.SPECIAL, self.special_outfit_index)

        def _generate_outfits(self, sim_info, special_outfit_index, tag_set):
            if special_outfit_index > 0:
                for i in range(0, special_outfit_index):
                    while not sim_info.sim_outfits.has_outfit((OutfitCategory.SPECIAL, i)):
                        sim_info.generate_outfit(OutfitCategory.SPECIAL, i)
            sim_info.generate_outfit(OutfitCategory.SPECIAL, special_outfit_index, tag_list=tag_set)

    class _OutfitChangeFromPickedItemId(OutfitChangeBase):
        __qualname__ = 'TunableOutfitChange._OutfitChangeFromPickedItemId'

        def _get_outfit(self, interaction):
            outfits = interaction.get_participants(ParticipantType.PickedItemId)
            if outfits is None:
                return
            outfit = next(iter(outfits))
            return outfit

        def get_on_entry_change(self, interaction, sim_info=DEFAULT, **kwargs):
            outfit = self._get_outfit(interaction)
            if outfit is not None:
                sim_info = interaction.sim.sim_info if sim_info is DEFAULT else sim_info
                return build_critical_section(sim_info.sim_outfits.get_change_outfit_element(outfit, **kwargs), flush_all_animations)

        def get_on_exit_change(self, interaction, sim_info=DEFAULT, **kwargs):
            pass

        def get_on_entry_outfit(self, interaction, sim_info=DEFAULT):
            return self._get_outfit(interaction)

        def get_on_exit_outfit(self, interaction, sim_info=DEFAULT):
            pass

    class _OutfitChangeWithState(OutfitChangeBase):
        __qualname__ = 'TunableOutfitChange._OutfitChangeWithState'

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self._outfit_change_map = WeakKeyDictionary()

        def _get_outfit_change_internal(self, interaction, sim_info):
            sim_map = self._outfit_change_map.get(interaction)
            if sim_map is None:
                sim_map = WeakKeyDictionary()
                self._outfit_change_map[interaction] = sim_map
            change = sim_map.get(sim_info)
            if change is None:
                change = self._create_outfit_change_internal(interaction, sim_info)
                sim_map[sim_info] = change
            return change

        def _create_outfit_change_internal(self, interaction, sim_info):
            raise NotImplementedError

        def get_on_entry_change(self, interaction, sim_info=DEFAULT, **kwargs):
            sim_info = interaction.sim.sim_info if sim_info is DEFAULT else sim_info
            for trait in sim_info.get_traits():
                outfit_change_reason = trait.get_outfit_change_reason(None)
                while outfit_change_reason is not None:
                    return sim_info.sim_outfits.get_change(interaction, outfit_change_reason, **kwargs)
            outfit_change = self._get_outfit_change_internal(interaction, sim_info)
            if outfit_change is not None:
                return build_critical_section(sim_info.sim_outfits.get_change_outfit_element(outfit_change.entry_outfit, **kwargs), flush_all_animations)

        def get_on_exit_change(self, interaction, sim_info=DEFAULT, **kwargs):
            sim_info = interaction.sim.sim_info if sim_info is DEFAULT else sim_info
            outfit_change = self._get_outfit_change_internal(interaction, sim_info)
            if outfit_change is not None:
                return build_critical_section(sim_info.sim_outfits.get_change_outfit_element(outfit_change.exit_outfit, **kwargs), flush_all_animations)

        def get_on_entry_outfit(self, interaction, sim_info=DEFAULT):
            sim_info = interaction.sim.sim_info if sim_info is DEFAULT else sim_info
            outfit_change = self._get_outfit_change_internal(interaction, sim_info)
            if outfit_change is not None:
                return outfit_change.entry_outfit

        def get_on_exit_outfit(self, interaction, sim_info=DEFAULT):
            sim_info = interaction.sim.sim_info if sim_info is DEFAULT else sim_info
            outfit_change = self._get_outfit_change_internal(interaction, sim_info)
            if outfit_change is not None:
                return outfit_change.exit_outfit

    class _OutfitChangeFromParticipant(_OutfitChangeWithState):
        __qualname__ = 'TunableOutfitChange._OutfitChangeFromParticipant'

        class _OutfitChangeTemporary(HasTunableFactory, AutoFactoryInit):
            __qualname__ = 'TunableOutfitChange._OutfitChangeFromParticipant._OutfitChangeTemporary'
            SPECIAL_OUTFIT_KEY = (OutfitCategory.SPECIAL, 0)

            def __init__(self, sim_info, outfit_source, *args, **kwargs):
                super().__init__(*args, **kwargs)
                outfits = outfit_source.get_outfits()
                source_outfit = outfit_source.get_current_outfit()
                sim_info.generate_merged_outfit(outfits.get_sim_info(), self.SPECIAL_OUTFIT_KEY, sim_info.get_current_outfit(), source_outfit)
                self.entry_outfit = self.SPECIAL_OUTFIT_KEY
                self.exit_outfit = sim_info.get_current_outfit()

        class _OutfitChangeAddition(HasTunableFactory, AutoFactoryInit):
            __qualname__ = 'TunableOutfitChange._OutfitChangeFromParticipant._OutfitChangeAddition'

            def __init__(self, sim_info, outfit_source, *args, **kwargs):
                super().__init__(*args, **kwargs)
                source_outfit = outfit_source.get_current_outfit()
                source_category = source_outfit[0]
                source_outfits = outfit_source.get_outfits()
                target_outfits = sim_info.sim_outfits
                outfits_in_category = target_outfits.get_outfits_in_category(source_category)
                outfits_in_category = len(outfits_in_category) if outfits_in_category is not None else 0
                current_outfit = sim_info.get_current_outfit()
                if outfits_in_category >= get_maximum_outfits_for_category(source_category):
                    available_outfits = [(source_category, index) for index in range(1, outfits_in_category) if (source_category, index) != current_outfit]
                    destination_outfit = random.choice(available_outfits)
                else:
                    destination_outfit = target_outfits.add_outfit(source_category, None)
                sim_info.generate_merged_outfit(source_outfits.get_sim_info(), destination_outfit, current_outfit, source_outfit)
                self.entry_outfit = destination_outfit
                self.exit_outfit = None

        FACTORY_TUNABLES = {'outfit_participant': TunableEnumEntry(description='\n                The Sim or object whose current outfit is going to be\n                temporarily applied to the Sim being affected by this change.\n                ', tunable_type=ParticipantTypeSingle, default=ParticipantTypeSingle.Object), 'outfit_change_behavior': TunableVariant(description='\n                Define how this outfit is to be applied to the Sim.\n                ', temporary=_OutfitChangeTemporary.TunableFactory(), addition=_OutfitChangeAddition.TunableFactory(), default='temporary')}

        def _create_outfit_change_internal(self, interaction, sim_info):
            outfit_participant = interaction.get_participant(self.outfit_participant)
            if outfit_participant is None:
                return
            return self.outfit_change_behavior(sim_info, outfit_participant)

    class _OutfitChangeForNew(_OutfitChangeWithState):
        __qualname__ = 'TunableOutfitChange._OutfitChangeForNew'

        class _OutfitChangeGeneration(HasTunableFactory, AutoFactoryInit):
            __qualname__ = 'TunableOutfitChange._OutfitChangeForNew._OutfitChangeGeneration'
            FACTORY_TUNABLES = {'outfit_category': TunableEnumEntry(description="\n                    The outfit category for which we're creating a new outfit.\n                    ", tunable_type=OutfitCategory, default=OutfitCategory.EVERYDAY)}

            def __init__(self, interaction, sim_info, *args, **kwargs):
                super().__init__(*args, **kwargs)
                outfits = sim_info.get_outfits()
                current_outfit = sim_info.get_current_outfit()
                outfits_in_category = outfits.get_outfits_in_category(self.outfit_category)
                outfits_in_category = len(outfits_in_category) if outfits_in_category is not None else 0
                if outfits_in_category >= get_maximum_outfits_for_category(self.outfit_category):
                    available_outfits = [(self.outfit_category, index) for index in range(1, outfits_in_category) if (self.outfit_category, index) != current_outfit]
                    (_, outfit_index) = random.choice(available_outfits)
                else:
                    (_, outfit_index) = outfits.add_outfit(self.outfit_category, None)
                sim_info.generate_outfit(outfit_category=self.outfit_category, outfit_index=outfit_index, filter_flag=FilterFlag.NONE)
                self.entry_outfit = (self.outfit_category, outfit_index)
                self.exit_outfit = None

        FACTORY_TUNABLES = {'outfit_change_behavior': _OutfitChangeGeneration.TunableFactory()}

        def _create_outfit_change_internal(self, interaction, sim_info):
            return self.outfit_change_behavior(interaction, sim_info)

    def __init__(self, allow_outfit_change=True, **kwargs):
        options = {'no_change': TunableOutfitChange._OutfitChangeNone.TunableFactory()}
        if allow_outfit_change:
            options['for_reason'] = TunableOutfitChange._OutfitChangeForReason.TunableFactory()
            options['for_tags'] = TunableOutfitChange._OutfitChangeForTags.TunableFactory()
            options['for_new'] = TunableOutfitChange._OutfitChangeForNew.TunableFactory()
            options['from_participant'] = TunableOutfitChange._OutfitChangeFromParticipant.TunableFactory()
            options['from_picker'] = TunableOutfitChange._OutfitChangeFromPickedItemId.TunableFactory()
        kwargs.update(options)
        super().__init__(default='no_change', **kwargs)

from element_utils import build_critical_section, build_critical_section_with_finally
from elements import ParentElement
from interactions import ParticipantType
from sims.outfits.outfit_change import TunableOutfitChange
from sims4.tuning.tunable import HasTunableFactory, AutoFactoryInit, TunableEnumFlags
import sims4.log
logger = sims4.log.Logger('SimOutfits')

class ChangeOutfitElement(ParentElement, HasTunableFactory, AutoFactoryInit):
    __qualname__ = 'ChangeOutfitElement'
    FACTORY_TUNABLES = {'subject': TunableEnumFlags(description='\n            The participant of who will change their outfit.\n            ', enum_type=ParticipantType, default=ParticipantType.Actor), 'outfit_change': TunableOutfitChange(description='\n            The change that you want to occur.\n            ')}

    def __init__(self, interaction, *args, sequence=(), **kwargs):
        super().__init__(*args, **kwargs)
        self.interaction = interaction
        self.sequence = sequence
        subject = self.interaction.get_participant(self.subject)
        self.outfits = subject.get_outfits()
        self.sim_info = self.outfits.get_sim_info()
        self.entry_outfit = self.outfit_change.get_on_entry_outfit(interaction, sim_info=self.sim_info)
        if self.entry_outfit is not None:
            self.sim_info.add_preload_outfit(self.entry_outfit)
        self.exit_outfit = self.outfit_change.get_on_exit_outfit(interaction, sim_info=self.sim_info)
        if self.exit_outfit is not None:
            self.sim_info.set_previous_outfit()
            self.sim_info.add_preload_outfit(self.exit_outfit)

    def _run(self, timeline):
        sequence = self.sequence
        if self.entry_outfit is not None:
            sequence = build_critical_section(self.outfit_change.get_on_entry_change(self.interaction, sim_info=self.sim_info), sequence)
        if self.exit_outfit is not None:
            sequence = build_critical_section(sequence, self.outfit_change.get_on_exit_change(self.interaction, sim_info=self.sim_info))
            sequence = build_critical_section_with_finally(sequence, lambda _: self.outfits.exit_change_verification(self.exit_outfit))
        return timeline.run_child(sequence)

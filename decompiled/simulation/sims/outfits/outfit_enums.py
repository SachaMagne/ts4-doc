import enum

class OutfitCategory(enum.Int):
    __qualname__ = 'OutfitCategory'
    CURRENT_OUTFIT = -1
    EVERYDAY = 0
    FORMAL = 1
    ATHLETIC = 2
    SLEEP = 3
    PARTY = 4
    BATHING = 5
    CAREER = 6
    SITUATION = 7
    SPECIAL = 8
    SWIMWEAR = 9

class SpecialOutfitIndex(enum.Int):
    __qualname__ = 'SpecialOutfitIndex'
    DEFAULT = 0
    TOWEL = 1

REGULAR_OUTFIT_CATEGORIES = (OutfitCategory.EVERYDAY, OutfitCategory.FORMAL, OutfitCategory.ATHLETIC, OutfitCategory.SLEEP, OutfitCategory.PARTY, OutfitCategory.SWIMWEAR)
HIDDEN_OUTFIT_CATEGORIES = (OutfitCategory.CAREER, OutfitCategory.SITUATION, OutfitCategory.SPECIAL)

class BodyType(enum.Int):
    __qualname__ = 'BodyType'
    NONE = 0
    HAT = 1
    HAIR = 2
    HEAD = 3
    FACE = 4
    FULL_BODY = 5
    UPPER_BODY = 6
    LOWER_BODY = 7
    SHOES = 8
    ACCESSORIES = 9
    EARRINGS = 10
    GLASSES = 11
    NECKLACE = 12
    GLOVES = 13
    WRIST_LEFT = 14
    WRIST_RIGHT = 15
    LIP_RING_LEFT = 16
    LIP_RING_RIGHT = 17
    NOSE_RING_LEFT = 18
    NOSE_RING_RIGHT = 19
    BROW_RING_LEFT = 20
    BROW_RING_RIGHT = 21
    INDEX_FINGER_LEFT = 22
    INDEX_FINGER_RIGHT = 23
    RING_FINGER_LEFT = 24
    RING_FINGER_RIGHT = 25
    MIDDLE_FINGER_LEFT = 26
    MIDDLE_FINGER_RIGHT = 27
    FACIAL_HAIR = 28
    LIPS_TICK = 29
    EYE_SHADOW = 30
    EYE_LINER = 31
    BLUSH = 32
    FACEPAINT = 33
    EYEBROWS = 34
    EYECOLOR = 35
    SOCKS = 36
    MASCARA = 37
    SKINDETAIL_CREASE_FOREHEAD = 38
    SKINDETAIL_FRECKLES = 39
    SKINDETAIL_DIMPLE_LEFT = 40
    SKINDETAIL_DIMPLE_RIGHT = 41
    TIGHTS = 42
    SKINDETAIL_MOLE_LIP_LEFT = 43
    SKINDETAIL_MOLE_LIP_RIGHT = 44
    TATTOO_ARM_LOWER_LEFT = 45
    TATTOO_ARM_UPPER_LEFT = 46
    TATTOO_ARM_LOWER_RIGHT = 47
    TATTOO_ARM_UPPER_RIGHT = 48
    TATTOO_LEG_LEFT = 49
    TATTOO_LEG_RIGHT = 50
    TATTOO_TORSO_BACK_LOWER = 51
    TATTOO_TORSO_BACK_UPPER = 52
    TATTOO_TORSO_FRONT_LOWER = 53
    TATTOO_TORSO_FRONT_UPPER = 54
    SKINDETAIL_MOLE_CHEEK_LEFT = 55
    SKINDETAIL_MOLE_CHEEK_RIGHT = 56
    SKINDETAIL_CREASE_MOUTH = 57
    SKIN_OVERLAY = 58

class BodyTypeFlag:
    __qualname__ = 'BodyTypeFlag'

    def _make_body_type_flag(*body_types):
        flags = 0
        for body_type in body_types:
            flags |= 1 << body_type
        return flags

    BRACELETS = _make_body_type_flag(BodyType.WRIST_LEFT, BodyType.WRIST_RIGHT)
    PIERCINGS = _make_body_type_flag(BodyType.LIP_RING_LEFT, BodyType.LIP_RING_RIGHT, BodyType.NOSE_RING_LEFT, BodyType.NOSE_RING_RIGHT, BodyType.BROW_RING_LEFT, BodyType.BROW_RING_RIGHT)
    RINGS = _make_body_type_flag(BodyType.INDEX_FINGER_LEFT, BodyType.INDEX_FINGER_RIGHT, BodyType.RING_FINGER_LEFT, BodyType.RING_FINGER_RIGHT, BodyType.MIDDLE_FINGER_LEFT, BodyType.MIDDLE_FINGER_RIGHT)
    ACCESSORY_ALL = _make_body_type_flag(BodyType.EARRINGS, BodyType.GLASSES, BodyType.NECKLACE, BodyType.GLOVES, BodyType.SOCKS, BodyType.TIGHTS) | BRACELETS | PIERCINGS | RINGS
    CLOTHING_ALL = _make_body_type_flag(BodyType.HAT, BodyType.FULL_BODY, BodyType.UPPER_BODY, BodyType.LOWER_BODY, BodyType.SHOES) | ACCESSORY_ALL

class FilterFlag(enum.IntFlags, export=False):
    __qualname__ = 'FilterFlag'
    NONE = 0
    USE_EXISTING_IF_APPROPRIATE = 1
    IGNORE_IF_NO_MATCH = 2
    OR_SAME_CATEGORY = 4
    EXCLUDE_FULLBODY = 8

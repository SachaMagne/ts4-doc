import operator
from filters.tunable import TunableSimFilter
from interactions import ParticipantType
from interactions.base.picker_interaction import PickerSuperInteraction
from interactions.context import QueueInsertStrategy
from interactions.utils.tunable import TunableContinuation
from retail.retail_utils import RetailUtils
from sims.outfits.outfit_enums import OutfitCategory, REGULAR_OUTFIT_CATEGORIES
from sims.sim_info_types import Gender
from sims4.localization import TunableLocalizedStringFactory
from sims4.tuning.tunable import TunableEnumEntry, TunableVariant, HasTunableSingletonFactory, AutoFactoryInit, OptionalTunable, TunableEnumSet, TunableList
from sims4.tuning.tunable_base import GroupNames
from sims4.utils import flexmethod
from ui.ui_dialog_picker import ObjectPickerTuningFlags, TunablePickerDialogVariant, OutfitPickerRow, UiSimPicker, SimPickerRow
import services

class OutfitPickerSuperInteraction(PickerSuperInteraction):
    __qualname__ = 'OutfitPickerSuperInteraction'

    class _OutfitPickerActionPushInteraction(HasTunableSingletonFactory, AutoFactoryInit):
        __qualname__ = 'OutfitPickerSuperInteraction._OutfitPickerActionPushInteraction'
        FACTORY_TUNABLES = {'continuation': TunableContinuation(description='\n                The continuation to push. The selected outfits are the picked\n                item of the pushed interaction.\n                ')}

        def get_disabled_tooltip(self):
            pass

        def on_choice_selected(self, interaction, picked_items, **kwargs):
            interaction.push_tunable_continuation(self.continuation, insert_strategy=QueueInsertStrategy.LAST, picked_item_ids=picked_items)

    class _OutfitActionDeleteOutfit(HasTunableSingletonFactory, AutoFactoryInit):
        __qualname__ = 'OutfitPickerSuperInteraction._OutfitActionDeleteOutfit'

        def get_disabled_tooltip(self):
            pass

        def on_choice_selected(self, interaction, picked_items, **kwargs):
            outfit_participant = interaction.outfit_sim_info.get_outfit_sim_info(interaction)
            outfits = outfit_participant.get_outfits()
            current_outfit = outfit_participant.get_current_outfit()
            for outfit in sorted(picked_items, key=operator.itemgetter(1), reverse=True):
                if current_outfit[0] == outfit[0] and current_outfit[1] >= outfit[1]:
                    current_outfit = (current_outfit[0], current_outfit[1] - 1)
                outfits.remove_outfit(*outfit)
            sim_info = outfits.get_sim_info()
            sim_info.serialize_and_set_base_outfits()
            sim_info.resend_outfits()
            sim_info.set_current_outfit(current_outfit)

    class _OutfitActionApplyCareerOutfit(HasTunableSingletonFactory, AutoFactoryInit):
        __qualname__ = 'OutfitPickerSuperInteraction._OutfitActionApplyCareerOutfit'
        FACTORY_TUNABLES = {'picker_dialog': UiSimPicker.TunableFactory(description='\n                The picker dialog to show when selecting Sims to apply this\n                outfit on.\n                '), 'sim_filter': TunableSimFilter.TunableReference(description='\n                The set of available Sims to show in the Sim picker.\n                '), 'pie_menu_test_tooltip': OptionalTunable(description='\n                If enabled, then a greyed-out tooltip will be displayed if there\n                are no valid choices.\n                ', tunable=TunableLocalizedStringFactory(description='\n                    The tooltip text to show in the greyed-out tooltip when no\n                    valid choices exist.\n                    '))}

        def get_disabled_tooltip(self):
            if self.pie_menu_test_tooltip is None:
                return
            filter_results = self._get_filter_results()
            if not filter_results:
                return self.pie_menu_test_tooltip

        def _get_filter_results(self):
            return services.sim_filter_service().submit_filter(self.sim_filter, None, allow_yielding=False)

        def _get_on_sim_choice_selected(self, interaction, picked_items):

            def _on_sim_choice_selected(dialog):
                if dialog.accepted:
                    outfit_source = interaction.outfit_sim_info.get_outfit_sim_info(interaction)
                    for sim_info in dialog.get_result_tags():
                        sim_info.generate_merged_outfit(outfit_source, (OutfitCategory.CAREER, 0), sim_info.get_current_outfit(), picked_items[0])
                        sim_info.resend_current_outfit()

            return _on_sim_choice_selected

        def on_choice_selected(self, interaction, picked_items, **kwargs):
            dialog = self.picker_dialog(interaction.sim, title=lambda *_, **__: interaction.get_name(apply_name_modifiers=False), resolver=interaction.get_resolver())
            for filter_result in self._get_filter_results():
                dialog.add_row(SimPickerRow(filter_result.sim_info.sim_id, tag=filter_result.sim_info))
            dialog.add_listener(self._get_on_sim_choice_selected(interaction, picked_items))
            dialog.show_dialog()

    class _OutfitSimInfoSelectorParticipant(HasTunableSingletonFactory, AutoFactoryInit):
        __qualname__ = 'OutfitPickerSuperInteraction._OutfitSimInfoSelectorParticipant'
        FACTORY_TUNABLES = {'participant': TunableEnumEntry(description='\n                The participant that has outfits that we want to display. This\n                must be either a Sim or an object with a component that supports\n                outfits, such as the Mannequin component.\n                ', tunable_type=ParticipantType, default=ParticipantType.Actor)}

        def get_outfit_sim_info(self, interaction, **kwargs):
            return interaction.get_participant(self.participant, **kwargs)

    class _OutfitSimInfoSelectorRetail(HasTunableSingletonFactory, AutoFactoryInit):
        __qualname__ = 'OutfitPickerSuperInteraction._OutfitSimInfoSelectorRetail'
        FACTORY_TUNABLES = {'gender': TunableEnumEntry(description="\n                The gender of the retail store's employee uniform mannequin from\n                which we want to select outfits.\n                ", tunable_type=Gender, default=Gender.MALE)}

        def get_outfit_sim_info(self, interaction, **_):
            retail_manager = RetailUtils.get_retail_manager_for_zone()
            if retail_manager is None:
                return
            return retail_manager.get_employee_uniform_data(self.gender)

    INSTANCE_TUNABLES = {'picker_dialog': TunablePickerDialogVariant(description='\n            The picker dialog to show.\n            ', available_picker_flags=ObjectPickerTuningFlags.OUTFIT, tuning_group=GroupNames.PICKERTUNING), 'outfit_sim_info': TunableVariant(description='\n            Define the Sim or object whose outfits are to be displayed.\n            ', from_participant=_OutfitSimInfoSelectorParticipant.TunableFactory(), from_retail=_OutfitSimInfoSelectorRetail.TunableFactory(), default='from_participant', tuning_group=GroupNames.PICKERTUNING), 'outfit_categories': TunableEnumSet(description='\n            The categories to display.\n            ', enum_type=OutfitCategory, default_enum_list=REGULAR_OUTFIT_CATEGORIES, tuning_group=GroupNames.PICKERTUNING), 'allow_current_outfit': OptionalTunable(description='\n            Whether or not the current outfit is a selectable entry.\n            ', tunable=TunableLocalizedStringFactory(description='\n                The tooltip to show on the current outfit.\n                '), enabled_name='Disallow', disabled_name='Allow', tuning_group=GroupNames.PICKERTUNING), 'outfit_actions': TunableList(description='\n            All the actions to undertake once a selection has been made.\n            ', tunable=TunableVariant(description='\n                The action to undertake once a selection has been made.\n                ', push_affordance=_OutfitPickerActionPushInteraction.TunableFactory(), delete_outfit=_OutfitActionDeleteOutfit.TunableFactory(), apply_career_outfit=_OutfitActionApplyCareerOutfit.TunableFactory(), default='push_affordance'), tuning_group=GroupNames.PICKERTUNING)}

    @classmethod
    def has_valid_choice(cls, target, context, **kwargs):
        if not cls._has_valid_outfit_choice(target, context, **kwargs):
            return False
        if not cls._has_valid_outfit_action():
            return False
        return True

    @classmethod
    def _has_valid_outfit_choice(cls, target, context, **kwargs):
        outfit_participant = cls.outfit_sim_info.get_outfit_sim_info(cls, sim=context.sim, target=target)
        if outfit_participant is None:
            return False
        outfits = outfit_participant.get_outfits()
        if not any(outfit_category in cls.outfit_categories and outfit_list for (outfit_category, outfit_list) in outfits.get_all_outfits()):
            return False
        return True

    @classmethod
    def _has_valid_outfit_action(cls):
        return cls._get_invalid_outfit_action() is None

    @classmethod
    def _get_invalid_outfit_action(cls):
        for outfit_action in cls.outfit_actions:
            disabled_tooltip = outfit_action.get_disabled_tooltip()
            while disabled_tooltip is not None:
                return disabled_tooltip

    @classmethod
    def get_disabled_tooltip(cls, *args, **kwargs):
        if not cls._has_valid_outfit_choice(*args, **kwargs):
            return cls.pie_menu_test_tooltip
        return cls._get_invalid_outfit_action()

    def _run_interaction_gen(self, timeline):
        self._show_picker_dialog(self.sim, target_sim=self.sim, target=self.target)
        return True

    @flexmethod
    def picker_rows_gen(cls, inst, target, context, **kwargs):
        inst_or_cls = inst if inst is not None else cls
        if inst is not None:
            outfit_participant = inst_or_cls.outfit_sim_info.get_outfit_sim_info(inst)
        else:
            outfit_participant = inst_or_cls.outfit_sim_info.get_outfit_sim_info(inst_or_cls, sim=context.sim, target=target)
        current_outfit = outfit_participant.get_current_outfit()
        if outfit_participant is not None:
            outfits = outfit_participant.get_outfits()
            outfit_sim_info = outfits.get_sim_info()
            for (outfit_category, outfit_list) in outfits.get_all_outfits():
                if outfit_category not in inst_or_cls.outfit_categories:
                    pass
                for (outfit_index, _) in enumerate(outfit_list):
                    outfit_key = (outfit_category, outfit_index)
                    if inst_or_cls.allow_current_outfit is not None and current_outfit == outfit_key:
                        is_enable = False
                        row_tooltip = lambda *_, **__: inst_or_cls.create_localized_string(inst_or_cls.allow_current_outfit)
                    else:
                        is_enable = True
                        row_tooltip = None
                    yield OutfitPickerRow(outfit_sim_id=outfit_sim_info.sim_id, outfit_category=outfit_category, outfit_index=outfit_index, is_enable=is_enable, row_tooltip=row_tooltip, tag=outfit_key)

    def _on_picker_selected(self, dialog):
        if dialog.accepted:
            picked_items = dialog.get_result_tags()
            for outfit_action in self.outfit_actions:
                outfit_action.on_choice_selected(self, picked_items)

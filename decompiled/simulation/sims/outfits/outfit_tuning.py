from sims.outfits.outfit_enums import OutfitCategory

def get_maximum_outfits_for_category(outfit_category):
    if outfit_category == OutfitCategory.BATHING or outfit_category == OutfitCategory.SITUATION:
        return 1
    if outfit_category == OutfitCategory.SPECIAL:
        return 2
    if outfit_category == OutfitCategory.CAREER:
        return 3
    return 5

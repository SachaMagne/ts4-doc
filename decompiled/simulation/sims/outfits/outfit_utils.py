from protocolbuffers import S4Common_pb2
import services

def populate_outfit_proto(outfit_msg, outfit_category, outfit_index, outfit_data):
    outfit_msg.outfit_id = outfit_data['outfit_id']
    outfit_msg.category = outfit_category
    outfit_msg.outfit_index = outfit_index
    outfit_msg.created = services.time_service().sim_now.absolute_ticks()
    outfit_msg.parts = S4Common_pb2.IdList()
    for part in outfit_data['parts']:
        outfit_msg.parts.ids.append(part)
    for body_type in outfit_data['body_types']:
        outfit_msg.body_types_list.body_types.append(body_type)
    outfit_msg.match_hair_style = outfit_data['match_hair_style']
    outfit_msg.outfit_flags = outfit_data['outfit_flags']

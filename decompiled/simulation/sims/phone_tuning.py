from event_testing.tests import TunableTestVariant
from sims4.localization import TunableLocalizedStringFactory
from sims4.tuning.tunable import TunableTuple, TunableList

class PhoneTuning:
    __qualname__ = 'PhoneTuning'
    DISABLE_PHONE_TESTS = TunableList(description='\n        List of tests and tooltip that when passed will disable opening the\n        phone.\n        ', tunable=TunableTuple(description='\n            Test that should pass for the phone to be disabled and its tooltip\n            to display to the player when he clicks on the phone.\n            ', test=TunableTestVariant(), tooltip=TunableLocalizedStringFactory()))

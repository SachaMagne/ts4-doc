import functools
import itertools
import random
from animation.arb_accumulator import with_skippable_animation_time
from animation.posture_manifest import Hand
from autonomy import autonomy_modes
from broadcasters.environment_score.environment_score_mixin import EnvironmentScoreMixin
from buffs.tunable import TunableBuffReference
from carry import get_carried_objects_gen
from date_and_time import DateAndTime
from distributor.ops import SetRelativeLotLocation
from distributor.system import Distributor
from element_utils import build_critical_section_with_finally, build_element, build_critical_section
from event_testing import test_events
from event_testing.test_variants import SocialContextTest
from interactions import priority, constraints
from interactions.aop import AffordanceObjectPair
from interactions.base.interaction import FITNESS_LIABILITY, FitnessLiability, Interaction, STAND_SLOT_LIABILITY, StandSlotReservationLiability
from interactions.context import InteractionContext, QueueInsertStrategy, InteractionSource
from interactions.interaction_finisher import FinishingType
from interactions.priority import Priority
from interactions.si_state import SIState
from interactions.utils.animation import AnimationOverrides, flush_all_animations, AsmAutoExitInfo, ArbElement
from interactions.utils.animation_reference import TunableAnimationReference
from interactions.utils.death import DeathTracker
from interactions.utils.routing import WalkStyleRequest, FollowPath, WalkStylePriority
from objects import HiddenReasonFlag, VisibilityState
from objects.base_interactions import JoinInteraction, AskToJoinInteraction
from objects.components.consumable_component import ConsumableComponent
from objects.game_object import GameObject
from objects.helpers.user_footprint_helper import UserFootprintHelper
from objects.mixins import LockoutMixin
from objects.object_enums import ResetReason
from objects.part import Part
from objects.portal import PortalRequiredFlag
from postures import ALL_POSTURES, posture_graph
from postures.posture_specs import PostureSpecVariable, get_origin_spec
from postures.posture_state import PostureState
from postures.transition_sequence import DerailReason
from services.reset_and_delete_service import ResetRecord
from sims.master_controller import WorkRequest
from sims.outfits.outfit_enums import OutfitCategory
from sims.self_interactions import AnimationInteraction
from sims.sim_info_mixin import HasSimInfoMixin
from sims.sim_outfits import ForcedOutfitChanges, OutfitChangeReason
from sims4.callback_utils import CallableList, consume_exceptions, RemovableCallableList
from sims4.geometry import test_point_in_polygon
from sims4.localization import TunableLocalizedString
from sims4.math import Transform
from sims4.tuning.instances import lock_instance_tunables
from sims4.tuning.tunable import Tunable, TunableRange, TunableList, TunableReference, TunableMapping, TunableThreshold, TunableEnumEntry
from sims4.utils import classproperty, flexmethod
from singletons import DEFAULT
from world import region
from zone import Zone
import animation.arb
import autonomy.autonomy_request
import buffs.buff
import build_buy
import caches
import cas.cas
import clock
import date_and_time
import distributor.fields
import distributor.ops
import element_utils
import elements
import enum
import event_testing.resolver
import gsi_handlers.sim_timeline_handlers
import interactions.interaction_queue
import objects.components.topic_component
import objects.components.types
import placement
import reset
import routing
import services
import sims.multi_motive_buff_tracker
import sims.ui_manager
import sims4.log
import statistics.commodity
TELEMETRY_QUICKTIME_INTERACTION = 'QUIC'
writer_2 = sims4.telemetry.TelemetryWriter(TELEMETRY_QUICKTIME_INTERACTION)
try:
    import _zone
except ImportError:

    class _zone:
        __qualname__ = '_zone'

        @staticmethod
        def add_sim(_):
            pass

        @staticmethod
        def remove_sim(_):
            pass

logger = sims4.log.Logger('Sim')

def __reload__(old_module_vars):
    global GLOBAL_AUTONOMY
    GLOBAL_AUTONOMY = old_module_vars['GLOBAL_AUTONOMY']

class SimulationState(enum.Int, export=False):
    __qualname__ = 'SimulationState'
    INITIALIZING = 1
    RESETTING = 2
    SIMULATING = 3
    BEING_DESTROYED = 4

class LOSAndSocialConstraintTuning:
    __qualname__ = 'LOSAndSocialConstraintTuning'
    constraint_expansion_amount = Tunable(description="\n        The amount, in meters, to expand the Sim's current constraint by when\n        calculating fallback social constraints. This number should be equal to\n        the tuned radius for the standard social group constraint minus a\n        nominal amount, such as 1 meter to prevent extremely small intersections\n        from being considered valid.\n        ", tunable_type=float, default=5)
    num_sides_for_circle_expansion_of_point_constraint = Tunable(description='\n        The number of sides to use when creating a circle for expanding point\n        constraints for the fallback social constraint.\n        ', tunable_type=int, default=8)
    incompatible_target_sim_maximum_time_to_wait = Tunable(description='\n        The number of sim minutes to wait for the target Sim of a social\n        interaction if they are in an incompatible state (such as sleeping)\n        before giving up and canceling the social.\n        ', tunable_type=float, default=20)
    incompatible_target_sim_route_nearby_frequency = Tunable(description='\n        The number of sim minutes to delay in between routing nearby the target\n        Sim of a social interaction if they are in an incompatible state (such\n        as sleeping).\n        ', tunable_type=float, default=5)
    maximum_intended_distance_to_route_nearby = Tunable(description="\n        The maximum distance in meters from the target Sim's current position to\n        their intended position where a Sim will stop the target Sim instead of\n        routing to their intended position. Note: this only applies to Sims who\n        are trying to socialize with a target Sim at higher-priority than the\n        interaction that Sim is running.\n        ", tunable_type=float, default=20)
    minimum_delay_between_route_nearby_attempts = Tunable(description="\n        The minimum delay, in Sim minutes, between route nearby attempts when a\n        social is in the head of a Sim's queue. NOTE: This is performance-\n        critical so please don't change this unless you know what you are doing.\n        ", tunable_type=float, default=5)

class Sim(HasSimInfoMixin, GameObject, LockoutMixin, EnvironmentScoreMixin, reset.ResettableObjectMixin):
    __qualname__ = 'Sim'
    INSTANCE_TUNABLES = {'max_interactions': TunableRange(description='\n            Max interactions in queue, including running interaction. If this\n            value is greater than 10, the interaction queue .swf must be\n            updated.\n            ', tunable_type=int, default=8, minimum=0, maximum=10), 'initial_buff': TunableBuffReference(description='\n            A buff that will be permanently added to the Sim on creation. Used\n            to affect the neutral state of a Sim.\n            '), '_quadtree_radius': Tunable(description="\n            Size of the Sim's quadtree footprint used for spatial queries.\n            ", tunable_type=float, default=0.123), '_phone_affordances': TunableList(description="\n            A list of affordances generated when the player wants to use the\n            Sim's cell phone.\n            ", tunable=TunableReference(description='\n                An affordance that can be run as a solo interaction.\n                ', manager=services.affordance_manager(), pack_safe=True)), '_relation_panel_affordances': TunableList(description='\n            A list of affordances that are shown when the player clicks on a Sim\n            in the relationship panel. These affordances must be able to run as\n            solo interactions, meaning they cannot have a target object or Sim.\n            \n            When the selected interaction runs, the Subject type \n            "PickedItemId" will be set to the clicked Sim\'s id. For example,\n            a relationship change loot op with Subject as Actor and Target\n            Subject as PickedItemId will change the relationship between the\n            Active Sim and the Sim selected in the Relationship Panel.\n            ', tunable=TunableReference(description='\n                An affordance shown when the player clicks on a relation in the\n                relationship panel.\n                ', manager=services.affordance_manager())), 'handedness_trait_map': TunableMapping(description="\n            Associate the Sim's handedness to specific traits.\n            ", key_type=TunableEnumEntry(description="\n                The Sim's handedness.\n                ", tunable_type=Hand, default=Hand.RIGHT), value_type=TunableReference(description='\n                The trait associated with this specific handedness.\n                ', manager=services.get_instance_manager(sims4.resources.Types.TRAIT)))}
    REMOVE_INSTANCE_TUNABLES = ('_should_search_forwarded_sim_aop', '_should_search_forwarded_child_aop')
    _pathplan_context = None
    _reaction_triggers = {}
    FACIAL_OVERLAY_ANIMATION = TunableAnimationReference(description='\n        Facial Overlay Animation for Mood.\n        ')
    FOREIGN_ZONE_BUFF = buffs.buff.Buff.TunableReference(description='\n        This buff is applied to any sim that is not in their home zone.  It is\n        used by autonomy for NPCs to score the GoHome interaction.\n        ')
    BUFF_CLOTHING_REASON = TunableLocalizedString(description='\n        The localized string used to give reason why clothing buff was added.\n        Does not support any tokens.\n        ')
    MULTI_MOTIVE_BUFF_MOTIVES = TunableMapping(description='\n        Buffs, Motives and the threshold needed for that motive to count towards\n        the multi motive buff\n        ', key_type=buffs.buff.Buff.TunableReference(description='\n            Buff that is added when all the motives are above their threshold\n            '), value_type=TunableMapping(description='\n            Motives and the threshold needed for that motive to count towards\n            the multi motive buff\n            ', key_type=statistics.commodity.Commodity.TunableReference(description='\n                Motive needed above threshold to get the buff\n                '), value_type=TunableThreshold(description='\n                Threshold at which this motive counts for the buff\n                ')))

    def __init__(self, *args, **kwargs):
        self._sim_info = None
        self._simulation_state = SimulationState.INITIALIZING
        GameObject.__init__(self, *args, **kwargs)
        LockoutMixin.__init__(self)
        self.remove_component(objects.components.types.FOOTPRINT_COMPONENT.instance_attr)
        self.add_component(objects.components.topic_component.TopicComponent(self))
        self.add_component(objects.components.inventory.SimInventoryComponent(self))
        self.queue = None
        self._is_removed = False
        self._scheduled_elements = set()
        self._starting_up = False
        self._persistence_group = objects.persistence_groups.PersistenceGroups.SIM
        self._route_fail_disable_count = 0
        self.waiting_dialog_response = None
        self._posture_state = None
        self.target_posture = None
        self._si_state = SIState(self)
        self._obj_manager = None
        self._pathplan_context = routing.PathPlanContext()
        self._pathplan_context.footprint_key = self.definition.get_footprint(0)
        self._pathplan_context.agent_id = self.id
        self._pathplan_context.agent_radius = routing.get_default_agent_radius()
        self._pathplan_context.set_key_mask(routing.FOOTPRINT_KEY_ON_LOT | routing.FOOTPRINT_KEY_OFF_LOT)
        self._lot_routing_restriction_ref_count = 0
        goal_finder_result_strategy = placement.FGLResultStrategyDefault()
        goal_finder_result_strategy.max_results = 20
        goal_finder_result_strategy.done_on_max_results = True
        self._goal_finder_search_strategy = placement.FGLSearchStrategyRouting(routing_context=self._pathplan_context)
        self._goal_finder_search_strategy.use_sim_footprint = True
        self._goal_finder = placement.FGLSearch(self._goal_finder_search_strategy, goal_finder_result_strategy)
        self.on_social_group_changed = CallableList()
        self._social_groups = []
        self.on_social_geometry_changed = CallableList()
        self.on_posture_event = CallableList()
        self.on_follow_path = CallableList()
        self.on_plan_path = CallableList()
        self.on_intended_location_changed = CallableList()
        self.on_intended_location_changed.append(self.refresh_los_constraint)
        self.on_intended_location_changed.append(self._update_social_geometry_on_location_changed)
        self.on_intended_location_changed.append(lambda *_, **__: self.two_person_social_transforms.clear())
        self.on_intended_location_changed.append(self.update_intended_position_on_active_lot)
        self._ui_manager = sims.ui_manager.UIManager(self)
        self._posture_compatibility_filter = []
        self._mixers_locked_out = {}
        self._mixer_front_page_cooldown = {}
        self.current_path = None
        self.needs_fitness_update = False
        self.asm_auto_exit = AsmAutoExitInfo()
        self._update_facial_overlay_interaction = None
        self.animation_interaction = None
        self.on_slot = None
        self.last_affordance = None
        self.last_animation_factory = None
        self._sleeping = False
        self._buff_handles = []
        self.interaction_logging = False
        self.transition_path_logging = False
        self._multi_motive_buff_trackers = []
        self._los_constraint = None
        self._social_group_constraint = None
        self.on_start_up = RemovableCallableList()
        self.object_ids_to_ignore = set()
        self._posture_target_refs = []
        self.next_passive_balloon_unlock_time = DateAndTime(0)
        self.two_person_social_transforms = {}
        self._intended_position_on_active_lot = False
        self.active_transition = None
        self._allow_route_instantly_when_hitting_marks = False
        self.current_object_set_as_head = None
        self._handedness = None
        self.stand_slot_reservation_removed_callbacks = CallableList()
        self._socials_locked = False

    def __repr__(self):
        if self.sim_info is None:
            if self._simulation_state == SimulationState.INITIALIZING:
                return "sim 'Creating Sim - Unknown Name' {0:#x}".format(self.id)
            return "sim 'Destroyed Sim - Unknown Name' {0:#x}".format(self.id)
        return "<sim '{0} {1} {2}' {3:#x}>".format(self.first_name, self.last_name, self.persona, self.id)

    def __str__(self):
        if self.sim_info is None:
            if self._simulation_state == SimulationState.INITIALIZING:
                return "sim 'Creating Sim - Unknown Name' {0:#x}".format(self.id)
            return 'Destroyed Sim - Unknown Name ID: {0:#x}'.format(self.id)
        return self.full_name

    @classproperty
    def reaction_triggers(cls):
        return cls._reaction_triggers

    @classproperty
    def is_sim(cls):
        return True

    @property
    def _anim_overrides_internal(self):
        return AnimationOverrides(overrides=super()._anim_overrides_internal, params={'sex': self.gender.name.lower(), 'age': self.age.animation_age_param, 'mood': self.get_mood_animation_param_name()})

    @property
    def sim_info(self):
        return self._sim_info

    @sim_info.setter
    def sim_info(self, value):
        self._sim_info = value
        if self._sim_info is not None:
            self._pathplan_context.agent_id = self._sim_info.sim_id

    @distributor.fields.Field(op=distributor.ops.SetThumbnail)
    def thumbnail(self):
        return self.sim_info.thumbnail

    @thumbnail.setter
    def thumbnail(self, value):
        self.sim_info.thumbnail = value

    @property
    def socials_locked(self):
        return self._socials_locked

    @socials_locked.setter
    def socials_locked(self, value):
        self._socials_locked = value

    @property
    def zone_id(self):
        if self.sim_info is not None:
            return self.sim_info.zone_id

    @property
    def block_id(self):
        if self.zone_id is not None and self.zone_id != 0:
            return build_buy.get_block_id(self.zone_id, self.location.transform.translation, self.level)
        return 0

    @property
    def in_pool(self):
        current_zone_id = services.current_zone_id()
        return build_buy.is_location_pool(current_zone_id, self.location.transform.translation, self.location.routing_surface.secondary_id)

    @property
    def level(self):
        if self.in_pool:
            return self.location.routing_surface.secondary_id - 1
        return self.location.routing_surface.secondary_id

    @property
    def is_dying(self):
        return self.has_buff(DeathTracker.IS_DYING_BUFF)

    @property
    def is_selected(self):
        client = services.client_manager().get_client_by_household(self.household)
        if client is not None:
            return self is client.active_sim
        return False

    @property
    def transition_controller(self):
        return self.queue.transition_controller

    def _create_routing_context(self):
        pass

    def get_or_create_routing_context(self):
        return self._pathplan_context

    @property
    def routing_context(self):
        return self._pathplan_context

    @property
    def object_radius(self):
        return self._pathplan_context.agent_radius

    @object_radius.setter
    def object_radius(self, value):
        self._pathplan_context.agent_radius = value

    @property
    def goal_finder_search_strategy(self):
        return self._goal_finder_search_strategy

    @property
    def goal_finder(self):
        return self._goal_finder

    @property
    def client(self):
        if self.account is not None:
            return self.account.get_client(self.zone_id)

    @property
    def si_state(self):
        return self._si_state

    @property
    def is_valid_posture_graph_object(self):
        return False

    @property
    def should_route_fail(self):
        return self._route_fail_disable_count == 0

    @property
    def should_route_instantly(self):
        zone = services.current_zone()
        if Zone.force_route_instantly:
            return True
        if not zone.are_sims_hitting_their_marks or not self._allow_route_instantly_when_hitting_marks:
            return False
        return not services.sim_spawner_service().sim_is_leaving(self)

    def set_allow_route_instantly_when_hitting_marks(self, allow):
        self._allow_route_instantly_when_hitting_marks = allow

    @property
    def is_simulating(self):
        return self._simulation_state == SimulationState.SIMULATING

    @property
    def is_being_destroyed(self):
        return self._simulation_state == SimulationState.RESETTING and self.reset_reason() == ResetReason.BEING_DESTROYED

    @property
    def handedness(self):
        if self._handedness is None:
            for (handedness, handedness_trait) in self.handedness_trait_map.items():
                while self.sim_info.has_trait(handedness_trait):
                    self._handedness = handedness
                    break
            self.handedness = Hand.RIGHT if self.sim_id % 4 else Hand.LEFT
        return self._handedness

    @handedness.setter
    def handedness(self, value):
        if self._handedness is not None:
            handedness_trait = self.handedness_trait_map.get(self._handedness)
            if handedness_trait is not None:
                self.sim_info.remove_trait(handedness_trait)
        handedness_trait = self.handedness_trait_map.get(value)
        if handedness_trait is not None:
            self.sim_info.add_trait(handedness_trait)
        self._handedness = value

    @property
    def on_home_lot(self):
        current_zone = services.current_zone()
        if self.household.home_zone_id == current_zone.id:
            active_lot = current_zone.lot
            if active_lot.is_position_on_lot(self.position):
                return True
        return False

    def set_location_without_distribution(self, value):
        if self._location.transform.translation != sims4.math.Vector3.ZERO() and value.parent is None and value.transform.translation == sims4.math.Vector3.ZERO():
            logger.callstack('Attempting to move an unparented object {} to position Zero'.format(self), level=sims4.log.LEVEL_ERROR)
        super().set_location_without_distribution(value)

    def update_intended_position_on_active_lot(self, *_, update_ui=False, **__):
        arrival_spawn_point = services.current_zone().active_lot_arrival_spawn_point
        if services.active_lot().is_position_on_lot(self.intended_position) or arrival_spawn_point is not None and test_point_in_polygon(self.intended_position, arrival_spawn_point.get_footprint_polygon()):
            new_intended_position_on_active_lot = True
        else:
            new_intended_position_on_active_lot = False
        on_off_lot_update = self._intended_position_on_active_lot != new_intended_position_on_active_lot
        if on_off_lot_update or update_ui:
            self._intended_position_on_active_lot = new_intended_position_on_active_lot
            msg = SetRelativeLotLocation(self.id, self.intended_position_on_active_lot, self.sim_info.lives_here, self.sim_info.is_in_travel_group())
            distributor = Distributor.instance()
            distributor.add_op(self, msg)
            services.get_event_manager().process_event(test_events.TestEvent.SimActiveLotStatusChanged, sim_info=self.sim_info, on_active_lot=new_intended_position_on_active_lot)

    def preload_inappropriate_streetwear_change(self, final_si, preload_outfit_set):
        if self.sim_info._current_outfit[0] in ForcedOutfitChanges.INAPPROPRIATE_STREETWEAR:
            if self.transition_controller is None:
                return
            remaining_transitions = self.transition_controller.get_transition_specs(self)
            for transition_spec in remaining_transitions:
                while transition_spec.portal is not None:
                    outfit_category_and_index = transition_spec.portal.get_on_entry_outfit(final_si, transition_spec.portal_id, sim_info=self.sim_info)
                    if outfit_category_and_index is None:
                        pass
                    if outfit_category_and_index[0] in ForcedOutfitChanges.INAPPROPRIATE_STREETWEAR:
                        outfit_category_and_index = None
                    break
            outfit_category_and_index = None
            if outfit_category_and_index is None:
                outfit_category_and_index = self.sim_info.sim_outfits.get_outfit_for_clothing_change(final_si, OutfitChangeReason.DefaultOutfit)
            self.transition_controller.inappropriate_streetwear_change = outfit_category_and_index
            preload_outfit_set.add(outfit_category_and_index)

    @distributor.fields.Field(op=distributor.ops.SetSimSleepState)
    def sleeping(self):
        return self._sleeping

    @sleeping.setter
    def sleeping(self, value):
        self._sleeping = value

    def save_object(self, object_list, item_location, container_id):
        pass

    def get_create_after_objs(self):
        super_objs = super().get_create_after_objs()
        return (self.sim_info,) + super_objs

    def set_build_buy_lockout_state(self, lockout_state, lockout_timer=None):
        raise AssertionError('Trying to illegally set a Sim as locked out: {}'.format(self))

    def without_route_failure(self, sequence=None):

        def disable_route_fail(_):
            pass

        def enable_route_fail(_):
            pass

        return build_critical_section_with_finally(disable_route_fail, sequence, enable_route_fail)

    @property
    def rig(self):
        return self._rig

    def inc_lot_routing_restriction_ref_count(self):
        if not self.is_npc or self.sim_info.lives_here:
            return
        if services.current_zone().lot.is_position_on_lot(self.position):
            return
        if self._pathplan_context.get_key_mask() & routing.FOOTPRINT_KEY_ON_LOT:
            self._pathplan_context.set_key_mask(self._pathplan_context.get_key_mask() & ~routing.FOOTPRINT_KEY_ON_LOT)

    def dec_lot_routing_restriction_ref_count(self):
        if not self.is_npc or self.sim_info.lives_here:
            return
        if self._lot_routing_restriction_ref_count > 0:
            if self._lot_routing_restriction_ref_count == 0:
                self._pathplan_context.set_key_mask(self._pathplan_context.get_key_mask() | routing.FOOTPRINT_KEY_ON_LOT)

    def clear_lot_routing_restrictions_ref_count(self):
        self._lot_routing_restriction_ref_count = 0
        self._pathplan_context.set_key_mask(self._pathplan_context.get_key_mask() | routing.FOOTPRINT_KEY_ON_LOT)

    def set_portal_mask_flag(self, flag):
        self._pathplan_context.set_portal_key_mask(self._pathplan_context.get_portal_key_mask() | flag)

    def clear_portal_mask_flag(self, flag):
        self._pathplan_context.set_portal_key_mask(self._pathplan_context.get_portal_key_mask() & ~flag)

    def execute_adjustment_interaction(self, affordance, constraint, int_priority, group_id=None, **kwargs):
        aop = AffordanceObjectPair(affordance, None, affordance, None, constraint_to_satisfy=constraint, route_fail_on_transition_fail=False, is_adjustment_interaction=True, **kwargs)
        context = InteractionContext(self, InteractionContext.SOURCE_SOCIAL_ADJUSTMENT, int_priority, insert_strategy=QueueInsertStrategy.NEXT, group_id=group_id, must_run_next=True, cancel_if_incompatible_in_queue=True)
        return aop.test_and_execute(context)

    @property
    def ui_manager(self):
        return self._ui_manager

    def _update_social_geometry_on_location_changed(self, *args, **kwargs):
        social_group = self.get_main_group()
        if social_group is not None:
            social_group.refresh_social_geometry(sim=self)

    def notify_social_group_changed(self, group):
        if self in group:
            if group not in self._social_groups:
                self._social_groups.append(group)
        elif group in self._social_groups:
            self._social_groups.remove(group)
        self.on_social_group_changed(self, group)

    def in_non_adjustable_posture(self):
        for aspect in self._posture_state.aspects:
            while not aspect.allow_social_adjustment:
                return True
        return False

    def filter_supported_postures(self, supported_postures):
        filtered_postures = supported_postures
        if filtered_postures is ALL_POSTURES:
            return ALL_POSTURES
        for filter_func in self._posture_compatibility_filter:
            filtered_postures = filter_func(filtered_postures)
        return filtered_postures

    def may_reserve(self, *args, **kwargs):
        return False

    def reserve(self, *args, **kwargs):
        logger.error('Attempting to reserve Sim {}. Violation of human rights.', self, owner='tastle')

    def schedule_element(self, timeline, element):
        resettable_element = reset.ResettableElement(element, self)
        resettable_element.on_scheduled(timeline)
        timeline.schedule(resettable_element)
        return resettable_element

    def register_reset_element(self, element):
        self._scheduled_elements.add(element)

    def unregister_reset_element(self, element):
        self._scheduled_elements.discard(element)

    def on_reset_element_hard_stop(self):
        self.reset(reset_reason=ResetReason.RESET_EXPECTED)

    def on_reset_notification(self, reset_reason):
        super().on_reset_notification(reset_reason)
        self._simulation_state = SimulationState.RESETTING
        self.queue.lock()

    def on_reset_get_elements_to_hard_stop(self, reset_reason):
        elements_to_reset = super().on_reset_get_elements_to_hard_stop(reset_reason)
        scheduled_elements = list(self._scheduled_elements)
        self._scheduled_elements.clear()
        for element in scheduled_elements:
            elements_to_reset.append(element)
            element.unregister()
        return elements_to_reset

    def on_reset_get_interdependent_reset_records(self, reset_reason, reset_records):
        super().on_reset_get_interdependent_reset_records(reset_reason, reset_records)
        master_controller = services.get_master_controller()
        master_controller.add_interdependent_reset_records(self, reset_records)
        for other_sim in master_controller.added_sims():
            while other_sim is not self:
                if other_sim.has_sim_in_any_queued_interactions_required_sim_cache(self):
                    reset_records.append(ResetRecord(other_sim, ResetReason.RESET_EXPECTED, self, 'In required sims of queued interaction.'))
        for social_group in self.get_groups_for_sim_gen():
            for other_sim in social_group:
                while other_sim is not self:
                    reset_records.append(ResetRecord(other_sim, ResetReason.RESET_EXPECTED, self, 'In social group'))
        for interaction in self.get_all_running_and_queued_interactions():
            while interaction.prepared:
                while True:
                    for other_sim in interaction.required_sims():
                        while other_sim is not self:
                            reset_records.append(ResetRecord(other_sim, ResetReason.RESET_EXPECTED, self, 'required sim in {}'.format(interaction)))
        if self.posture_state is not None:
            for aspect in self.posture_state.aspects:
                target = aspect.target
                while target is not None:
                    if target.is_part:
                        target = target.part_owner
                    reset_records.append(ResetRecord(target, ResetReason.RESET_EXPECTED, self, 'Posture state aspect:{} target:{}'.format(aspect, target)))

    def on_reset_restart(self):
        self._start_animation_interaction()
        return False

    def on_state_changed(self, state, old_value, new_value):
        if not self.is_simulating:
            return
        affordances = self.sim_info.PHYSIQUE_CHANGE_AFFORDANCES
        reaction_affordance = None
        if old_value != new_value and (state == ConsumableComponent.FAT_STATE or state == ConsumableComponent.FIT_STATE):
            self.needs_fitness_update = True
            if state == ConsumableComponent.FAT_STATE:
                reaction_affordance = affordances.FAT_CHANGE_NEUTRAL_AFFORDANCE
                fat_commodity = ConsumableComponent.FAT_COMMODITY
                old_fat = self.sim_info.fat
                new_fat = self.commodity_tracker.get_value(fat_commodity)
                midrange_fat = (fat_commodity.max_value + fat_commodity.min_value)/2
                self.sim_info.fat = new_fat
                if new_fat > midrange_fat:
                    if old_fat < new_fat:
                        if new_fat == fat_commodity.max_value:
                            reaction_affordance = affordances.FAT_CHANGE_MAX_NEGATIVE_AFFORDANCE
                        else:
                            reaction_affordance = affordances.FAT_CHANGE_NEGATIVE_AFFORDANCE
                            if old_fat > new_fat:
                                reaction_affordance = affordances.FAT_CHANGE_POSITIVE_AFFORDANCE
                                reaction_affordance = affordances.FAT_CHANGE_MAX_POSITIVE_AFFORDANCE
                    elif old_fat > new_fat:
                        reaction_affordance = affordances.FAT_CHANGE_POSITIVE_AFFORDANCE
                        reaction_affordance = affordances.FAT_CHANGE_MAX_POSITIVE_AFFORDANCE
                else:
                    reaction_affordance = affordances.FAT_CHANGE_MAX_POSITIVE_AFFORDANCE
            else:
                reaction_affordance = affordances.FIT_CHANGE_NEUTRAL_AFFORDANCE
                old_fit = self.sim_info.fit
                new_fit = self.commodity_tracker.get_value(ConsumableComponent.FIT_COMMODITY)
                self.sim_info.fit = new_fit
                if old_fit < new_fit:
                    reaction_affordance = affordances.FIT_CHANGE_POSITIVE_AFFORDANCE
                else:
                    reaction_affordance = affordances.FIT_CHANGE_NEGATIVE_AFFORDANCE
            if reaction_affordance is not None:
                context = InteractionContext(self, InteractionContext.SOURCE_SCRIPT, Priority.Low, client=None, pick=None)
                result = self.push_super_affordance(reaction_affordance, None, context)
                if result:
                    result.interaction.add_liability(FITNESS_LIABILITY, FitnessLiability(self))
                    return
            self.sim_info.update_fitness_state()

    def _on_navmesh_updated(self):
        self.validate_current_location_or_fgl()
        if self.transition_controller is not None:
            if self.current_path is not None and self.current_path.nodes.needs_replan():
                if self.transition_controller.succeeded:
                    self.reset(ResetReason.RESET_EXPECTED, None, 'Traversing a path that needs replanning but is not controlled by the transition sequence.')
                elif self.current_path.final_location.transform != self.current_path.intended_location.transform and not self.validate_location(self.current_path.intended_location):
                    self.reset(ResetReason.RESET_EXPECTED, None, "Traversing an path that's been canceled or derailed and the position we've chosen to cancel/derail to is not valid.")
                else:
                    zone = services.current_zone()
                    if zone.is_in_build_buy:
                        self.transition_controller.derail(DerailReason.NAVMESH_UPDATED_BY_BUILD, self)
                    else:
                        self.transition_controller.derail(DerailReason.NAVMESH_UPDATED, self)
                        if self.transition_controller.sim_is_traversing_invalid_portal(self):
                            self.reset(ResetReason.RESET_EXPECTED, None, 'Transitioning through a portal that was deleted.')
            elif self.transition_controller.sim_is_traversing_invalid_portal(self):
                self.reset(ResetReason.RESET_EXPECTED, None, 'Transitioning through a portal that was deleted.')
        self.two_person_social_transforms.clear()

    def validate_location(self, location):
        routing_location = routing.Location(location.transform.translation, location.transform.orientation, location.routing_surface)
        contexts = set()
        for interaction in itertools.chain((self.queue.running,), self.si_state):
            while not interaction is None:
                if interaction.target is None:
                    pass
                contexts.add(interaction.target.raycast_context())
                while interaction.target.parent is not None:
                    contexts.add(interaction.target.parent.raycast_context())
        if not contexts:
            contexts.add(self.routing_context)
        test_portal_clearance = self.posture.unconstrained and not self.in_pool
        for context in contexts:
            while placement.validate_sim_location(routing_location, routing.get_default_agent_radius(), context, test_portal_clearance):
                return True
        return False

    def validate_current_location_or_fgl(self, from_reset=False, derail_reason=None):
        zone = services.current_zone()
        if (derail_reason is None or derail_reason != DerailReason.NAVMESH_UPDATED_BY_BUILD) and not zone.is_in_build_buy and not from_reset:
            return
        if from_reset and zone.is_in_build_buy:
            services.get_event_manager().process_event(test_events.TestEvent.OnBuildBuyReset, sim_info=self.sim_info)
        if self.current_path is not None:
            if from_reset:
                return
            if any(sim_primitive.is_traversing_invalid_portal() for sim_primitive in self.primitives if isinstance(sim_primitive, FollowPath)):
                self.reset(ResetReason.RESET_EXPECTED, self, 'Traversing invalid portal.')
            return
        (location, on_surface) = self.get_location_on_nearest_surface_below()
        if self.validate_location(location):
            if not on_surface:
                if not from_reset:
                    self.reset(ResetReason.RESET_EXPECTED, self, 'Failed to validate location.')
                self.location = location
            return
        ignored_object_ids = {self.sim_id}
        ignored_object_ids.update(child.id for child in self.children_recursive_gen())
        parent_object = self.parent_object()
        while parent_object is not None:
            ignored_object_ids.add(parent_object.id)
            parent_object = self.parent_object()
        search_flags = placement.FGLSearchFlagsDefault | placement.FGLSearchFlag.USE_SIM_FOOTPRINT | placement.FGLSearchFlag.STAY_IN_CURRENT_BLOCK
        starting_location = placement.create_starting_location(location=location)
        fgl_context = placement.FindGoodLocationContext(starting_location, ignored_object_ids=ignored_object_ids, additional_avoid_sim_radius=routing.get_sim_extra_clearance_distance(), search_flags=search_flags, routing_context=self.routing_context)
        (trans, orient) = placement.find_good_location(fgl_context)
        if trans is None or orient is None:
            if not from_reset:
                self.reset(ResetReason.RESET_EXPECTED, self, 'Failed to find location.')
                return
            self.fgl_reset_to_landing_strip()
            return
        if not from_reset:
            self.reset(ResetReason.RESET_EXPECTED, self, 'Failed to find location.')
        new_transform = sims4.math.Transform(trans, orient)
        self.location = location.clone(transform=new_transform)

    def fgl_reset_to_landing_strip(self):
        self.reset(ResetReason.RESET_EXPECTED, self, 'Reset to landing strip.')
        zone = services.current_zone()
        spawn_point = zone.active_lot_arrival_spawn_point
        if spawn_point is None:
            self.move_to_landing_strip()
            return
        (spawn_trans, _) = spawn_point.next_spawn_spot()
        location = routing.Location(spawn_trans, routing_surface=spawn_point.routing_surface)
        success = False
        if self._pathplan_context.get_key_mask() & routing.FOOTPRINT_KEY_ON_LOT:
            self._pathplan_context.set_key_mask(self._pathplan_context.get_key_mask() & ~routing.FOOTPRINT_KEY_ON_LOT)
            should_have_permission = True
        else:
            should_have_permission = False
        try:
            starting_location = placement.create_starting_location(location=location)
            fgl_context = placement.create_fgl_context_for_sim(starting_location, self, additional_avoid_sim_radius=routing.get_default_agent_radius(), routing_context=self.routing_context)
            (trans, orient) = placement.find_good_location(fgl_context)
            while trans is not None and orient is not None:
                transform = Transform(trans, orient)
                if spawn_point is not None:
                    self.location = self.location.clone(routing_surface=spawn_point.routing_surface, transform=transform)
                else:
                    self.location = self.location.clone(transform=transform)
                success = True
        finally:
            if should_have_permission:
                self._pathplan_context.set_key_mask(self._pathplan_context.get_key_mask() | routing.FOOTPRINT_KEY_ON_LOT)
        return success

    def _get_best_valid_level(self):
        position = sims4.math.Vector3(self.position.x, self.position.y, self.position.z)
        for i in range(self.routing_surface.secondary_id, build_buy.get_lowest_level_allowed() - 1, -1):
            zone = services.current_zone()
            while build_buy.has_floor_at_location(zone.id, position, i):
                return i
        if self.routing_surface.secondary_id < 0:
            for i in range(self.routing_surface.secondary_id, 0, 1):
                zone = services.current_zone()
                while build_buy.has_floor_at_location(zone.id, position, i):
                    return i
        return 0

    def get_location_on_nearest_surface_below(self):
        if self.posture_state.valid and (not self.posture.unconstrained or self.active_transition is not None and not self.active_transition.source.unconstrained):
            return (self.location, True)
        location = self.location
        level = self._get_best_valid_level()
        if self.location.routing_surface.type == routing.SurfaceType.SURFACETYPE_POOL and not self.in_pool:
            surface_type = routing.SurfaceType.SURFACETYPE_WORLD
        else:
            surface_type = None
        if level != location.routing_surface.secondary_id or surface_type is not None:
            routing_surface = routing.SurfaceIdentifier(location.routing_surface.primary_id, level, surface_type or location.routing_surface.type)
            location = location.clone(routing_surface=routing_surface)
        on_surface = False
        snapped_y = services.terrain_service.terrain_object().get_routing_surface_height_at(location.transform.translation.x, location.transform.translation.z, location.routing_surface)
        LEVEL_SNAP_TOLERANCE = 0.001
        if location.routing_surface == self.routing_surface and sims4.math.almost_equal(snapped_y, location.transform.translation.y, epsilon=LEVEL_SNAP_TOLERANCE):
            on_surface = True
        translation = sims4.math.Vector3(location.transform.translation.x, snapped_y, location.transform.translation.z)
        location = location.clone(translation=translation)
        return (location, on_surface)

    def move_to_landing_strip(self):
        zone = services.current_zone()
        spawn_point = zone.get_spawn_point()
        if spawn_point is not None:
            (trans, _) = spawn_point.next_spawn_spot()
            self.location = self.location.clone(translation=trans, routing_surface=spawn_point.routing_surface)
            self.fade_in()
            return
        logger.warn('No landing strip exists in zone {}', zone)

    def _start_animation_interaction(self):
        animation_aop = AffordanceObjectPair(AnimationInteraction, None, AnimationInteraction, None, hide_unrelated_held_props=False)
        facial_overlay_interaction_context = InteractionContext(self, InteractionContext.SOURCE_SCRIPT, priority.Priority.High)
        self._update_facial_overlay_interaction = animation_aop.interaction_factory(facial_overlay_interaction_context).interaction
        animation_interaction_context = InteractionContext(self, InteractionContext.SOURCE_SCRIPT, priority.Priority.High)
        animation_aop = AffordanceObjectPair(AnimationInteraction, None, AnimationInteraction, None)
        self.animation_interaction = animation_aop.interaction_factory(animation_interaction_context).interaction

    def _stop_animation_interaction(self):
        if self._update_facial_overlay_interaction is not None:
            self._update_facial_overlay_interaction.cancel(FinishingType.RESET, 'Sim is being reset.')
            self._update_facial_overlay_interaction.on_removed_from_queue()
            self._update_facial_overlay_interaction = None
        if self.animation_interaction is not None:
            self.animation_interaction.cancel(FinishingType.RESET, 'Sim is being reset.')
            self.animation_interaction.on_removed_from_queue()
            self.animation_interaction = None

    def on_reset_internal_state(self, reset_reason):
        being_destroyed = reset_reason == ResetReason.BEING_DESTROYED
        try:
            if not being_destroyed:
                self.set_last_user_directed_action_time()
            services.get_master_controller().on_reset_sim(self, reset_reason)
            self.hide(HiddenReasonFlag.NOT_INITIALIZED)
            self.queue.on_reset()
            self.si_state.on_reset()
            self.socials_locked = False
            if self.posture_state is not None:
                self.posture_state.on_reset(ResetReason.RESET_EXPECTED)
            if not being_destroyed:
                if self.sim_info._current_outfit[0] == OutfitCategory.BATHING and not self.in_pool:
                    self.sim_info.set_current_outfit((OutfitCategory.EVERYDAY, 0))
                self.sim_info.resend_current_outfit()
            self._posture_target_refs.clear()
            self._stop_environment_score()
            if self._update_facial_overlay in self.Buffs.on_mood_changed:
                self.Buffs.on_mood_changed.remove(self._update_facial_overlay)
            self._stop_animation_interaction()
            self.ui_manager.remove_all_interactions()
            self.on_sim_reset(being_destroyed)
            self.clear_all_autonomy_skip_sis()
            if being_destroyed:
                self._remove_multi_motive_buff_trackers()
            self.asm_auto_exit.clear()
            self.last_affordance = None
            self.last_animation_factory = None
            if not being_destroyed and not self._is_removed:
                try:
                    self.validate_current_location_or_fgl(from_reset=True)
                    self.refresh_los_constraint()
                    self.visibility = VisibilityState()
                    self.opacity = 1
                except Exception:
                    logger.exception('Exception thrown while finding good location for Sim on reset:')
            services.get_event_manager().process_event(test_events.TestEvent.OnSimReset, sim_info=self.sim_info)
            self.two_person_social_transforms.clear()
        except:
            logger.exception('TODO: Exception thrown during Sim reset, possibly we should be kicking the Sim out of the game.')
            raise
        finally:
            super().on_reset_internal_state(reset_reason)

    def _reset_reference_arb(self):
        self._reference_arb = None

    def _create_motives(self):
        if self.initial_buff.buff_type is not None:
            self.add_buff(self.initial_buff.buff_type, self.initial_buff.buff_reason)

    def running_interactions_gen(self, affordance):
        if self.si_state is not None:
            interaction_type = affordance.get_interaction_type()
            for si in self.si_state.sis_actor_gen():
                if issubclass(si.get_interaction_type(), interaction_type):
                    yield si
                else:
                    linked_interaction_type = si.get_linked_interaction_type()
                    while linked_interaction_type is not None:
                        if issubclass(linked_interaction_type, interaction_type):
                            yield si

    def get_all_running_and_queued_interactions(self):
        interactions = [si for si in self.si_state.sis_actor_gen()]
        for si in self.queue:
            interactions.append(si)
        return interactions

    def get_running_and_queued_interactions_by_tag(self, tags):
        interaction_set = set()
        for si in self.si_state.sis_actor_gen():
            while tags & si.affordance.interaction_category_tags:
                interaction_set.add(si)
        for si in self.queue:
            while tags & si.affordance.interaction_category_tags:
                interaction_set.add(si)
        return interaction_set

    def has_sim_in_any_queued_interactions_required_sim_cache(self, sim_in_question):
        return any(interaction.has_sim_in_required_sim_cache(sim_in_question) for interaction in self.queue)

    def get_running_interactions_by_tags(self, tags):
        interaction_set = set()
        for si in self.si_state.sis_actor_gen():
            while tags & si.affordance.interaction_category_tags:
                interaction_set.add(si)
        return interaction_set

    @caches.cached
    def _all_affordance_targets(self):
        results = []
        for si in self.si_state.sis_actor_gen():
            if si.is_finishing:
                pass
            affordance = si.get_interaction_type()
            results.append((affordance, si.target))
            linked_affordance = si.get_linked_interaction_type()
            if linked_affordance is not None:
                results.append((linked_affordance, si.target))
            for other_target in si.get_potential_mixer_targets():
                results.append((affordance, other_target))
                while linked_affordance is not None:
                    results.append((linked_affordance, other_target))
        return frozenset(results)

    @caches.cached
    def _shared_affordance_targets(self, sim):
        affordance_targets_a = self._all_affordance_targets()
        affordance_targets_b = sim._all_affordance_targets()
        both = affordance_targets_a & affordance_targets_b
        if both:
            result = frozenset(affordance for (affordance, _) in both)
            return result
        return ()

    def is_running_interaction(self, affordance, target):
        affordance_targets = self._all_affordance_targets()
        return (affordance, target) in affordance_targets

    def are_running_equivalent_interactions(self, sim, affordance):
        shared = self._shared_affordance_targets(sim)
        return affordance in shared

    def _provided_interactions_gen(self, context, **kwargs):
        _generated_affordance = set()
        for interaction in self.si_state:
            if interaction.is_finishing:
                pass
            for affordance_data in interaction.affordance.provided_affordances:
                affordance = affordance_data.affordance
                if affordance in _generated_affordance:
                    pass
                if context.sim.is_running_interaction(affordance, self):
                    pass
                if context.sim is not None and self.are_running_equivalent_interactions(context.sim, affordance):
                    pass
                aop = AffordanceObjectPair(affordance, self, affordance, None, depended_on_si=interaction if affordance_data.is_linked else None, **kwargs)
                while aop.test(context):
                    _generated_affordance.add(affordance)
                    yield aop
        if context.sim is not None:
            for (_, _, carried_object) in get_carried_objects_gen(context.sim):
                for affordance in carried_object.get_provided_affordances_gen():
                    aop = AffordanceObjectPair(affordance, self, affordance, None, **kwargs)
                    while aop.test(context):
                        yield aop

    def _potential_joinable_interactions_gen(self, context, **kwargs):

        def get_target(interaction, join_participant):
            join_target = interaction.get_participant(join_participant)
            if join_target and isinstance(join_target, Part):
                join_target = join_target.part_owner
            return join_target

        def get_join_affordance(default, join_info, joining_sim, target):
            if join_info.join_affordance.is_affordance:
                join_affordance = join_info.join_affordance.value
                if join_affordance is None:
                    join_affordance = default
                if target is not None:
                    for interaction in joining_sim.si_state:
                        while interaction.get_interaction_type() is join_affordance:
                            interaction_join_target = get_target(interaction, join_info.join_target)
                            if interaction_join_target is target:
                                return (None, target)
                return (join_affordance, target)
            if context.source == InteractionSource.AUTONOMY:
                return (None, target)
            commodity_search = join_info.join_affordance.value
            for interaction in joining_sim.si_state:
                while commodity_search.commodity in interaction.commodity_flags:
                    return (None, target)
            join_context = InteractionContext(joining_sim, InteractionContext.SOURCE_AUTONOMY, Priority.High, client=None, pick=None, always_check_in_use=True)
            constraint = constraints.Circle(target.position, commodity_search.radius, target.routing_surface)
            autonomy_request = autonomy.autonomy_request.AutonomyRequest(joining_sim, autonomy_modes.FullAutonomy, static_commodity_list=(commodity_search.commodity,), context=join_context, constraint=constraint, limited_autonomy_allowed=True, consider_scores_of_zero=True, allow_forwarding=False, autonomy_mode_label_override='Joinable')
            best_action = services.autonomy_service().find_best_action(autonomy_request)
            if best_action:
                return (best_action, best_action.target)
            return (None, target)

        def get_join_aops_gen(interaction, join_sim, joining_sim, join_factory):
            interaction_type = interaction.get_interaction_type()
            join_target_ref = join_sim.ref()
            for joinable_info in interaction.joinable:
                if join_sim is self and not joinable_info.join_available:
                    pass
                if join_sim is context.sim and not joinable_info.invite_available:
                    pass
                join_target = get_target(interaction, joinable_info.join_target)
                if join_target is None and interaction.sim is not self:
                    pass
                (joinable_interaction, join_target) = get_join_affordance(interaction_type, joinable_info, joining_sim, join_target)
                if joinable_interaction is None:
                    pass
                join_interaction = join_factory(joinable_interaction.affordance, joining_sim, interaction, joinable_info)
                for aop in join_interaction.potential_interactions(join_target, context, join_target_ref=join_target_ref, **kwargs):
                    result = aop.test(context)
                    while result or result.tooltip:
                        yield aop

        def create_join_si(affordance, joining_sim, join_interaction, joinable_info):
            return JoinInteraction.generate(affordance, join_interaction, joinable_info)

        def create_invite_to_join_si(affordance, joining_sim, join_interaction, joinable_info):
            return AskToJoinInteraction.generate(affordance, joining_sim, join_interaction, joinable_info)

        for interaction in self.si_state.sis_actor_gen():
            while interaction.joinable and not interaction.is_finishing:
                while True:
                    for aop in get_join_aops_gen(interaction, self, context.sim, create_join_si):
                        yield aop
        if context.sim is not None:
            for interaction in context.sim.si_state.sis_actor_gen():
                while interaction.joinable and not interaction.is_finishing:
                    while True:
                        for aop in get_join_aops_gen(interaction, context.sim, self, create_invite_to_join_si):
                            yield aop

    def _potential_role_state_affordances_gen(self, context, **kwargs):

        def _can_show_affordance(shift_held, affordance):
            if shift_held:
                if affordance.cheat:
                    return True
                if affordance.debug and __debug__:
                    return True
            elif not affordance.debug and not affordance.cheat:
                return True
            return False

        shift_held = False
        if context is not None:
            shift_held = context.shift_held
        if self.active_roles() is not None:
            for active_role in self.active_roles():
                for affordance in active_role.role_affordances:
                    while _can_show_affordance(shift_held, affordance):
                        yield affordance

    @caches.cached_generator
    def potential_interactions(self, context, get_interaction_parameters=None, **kwargs):
        for affordance in self.super_affordances(context):
            if context.sim.is_running_interaction(affordance, self):
                pass
            if context.sim is not None and self.are_running_equivalent_interactions(context.sim, affordance):
                pass
            if get_interaction_parameters is not None:
                interaction_parameters = get_interaction_parameters(affordance, kwargs)
            else:
                interaction_parameters = kwargs
            yield affordance.potential_interactions(self, context, **interaction_parameters)
        if context.sim is not self:
            yield self._provided_interactions_gen(context, **kwargs)
        if context.sim is not self:
            yield self._potential_joinable_interactions_gen(context, **kwargs)
        else:
            for si in self.si_state.sis_actor_gen():
                for affordance in si.all_affordances_gen():
                    for aop in affordance.potential_interactions(si.target, si.affordance, si, **kwargs):
                        while aop.affordance.allow_forward:
                            yield aop

    def potential_phone_interactions(self, context, **kwargs):
        for affordance in self._phone_affordances:
            for aop in affordance.potential_interactions(self, context, **kwargs):
                yield aop

    def potential_relation_panel_interactions(self, context, **kwargs):
        for affordance in self._relation_panel_affordances:
            for aop in affordance.potential_interactions(self, context, **kwargs):
                yield aop

    def locked_from_obj_by_privacy(self, obj):
        for privacy in services.privacy_service().privacy_instances:
            if self in privacy.allowed_sims:
                pass
            if self not in privacy.disallowed_sims and privacy.evaluate_sim(self):
                pass
            while privacy.intersects_with_object(obj):
                return True
        return False

    @flexmethod
    def super_affordances(cls, inst, context=None):
        inst_or_cls = inst if inst is not None else cls
        for affordance in super(GameObject, inst_or_cls).super_affordances(context):
            yield affordance
        if inst is not None:
            for affordance in inst._potential_role_state_affordances_gen(context):
                yield affordance

    @property
    def commodity_flags(self):
        dynamic_commodity_flags = set()
        return super().commodity_flags | dynamic_commodity_flags

    @staticmethod
    def _get_mixer_key(target, affordance, sim_specific):
        if sim_specific and target is not None and target.is_sim:
            return (affordance, target.id)
        return affordance

    def set_sub_action_lockout(self, mixer_interaction, target=None, lock_other_affordance=False, initial_lockout=False):
        now = services.time_service().sim_now
        if initial_lockout:
            lockout_time = mixer_interaction.lock_out_time_initial.random_float()
            sim_specific = False
        else:
            lockout_time = mixer_interaction.lock_out_time.interval.random_float()
            sim_specific = mixer_interaction.lock_out_time.target_based_lock_out
        lockout_time_span = clock.interval_in_sim_minutes(lockout_time)
        lock_out_time = now + lockout_time_span
        mixer_lockout_key = self._get_mixer_key(mixer_interaction.target, mixer_interaction.affordance, sim_specific)
        self._mixers_locked_out[mixer_lockout_key] = lock_out_time
        if not initial_lockout and lock_other_affordance and mixer_interaction.lock_out_affordances is not None:
            while True:
                for affordance in mixer_interaction.lock_out_affordances:
                    sim_specific = affordance.lock_out_time.target_based_lock_out if affordance.lock_out_time is not None else False
                    mixer_lockout_key = self._get_mixer_key(mixer_interaction.target, affordance, sim_specific)
                    self._mixers_locked_out[mixer_lockout_key] = lock_out_time

    def update_last_used_mixer(self, mixer_interaction):
        if mixer_interaction.lock_out_time is not None:
            self.set_sub_action_lockout(mixer_interaction, lock_other_affordance=True)
        if mixer_interaction.front_page_cooldown is not None:
            cooldown_time = mixer_interaction.front_page_cooldown.interval.random_float()
            now = services.time_service().sim_now
            cooldown_time_span = clock.interval_in_sim_minutes(cooldown_time)
            cooldown_finish_time = now + cooldown_time_span
            affordance = mixer_interaction.affordance
            cur_penalty = self.get_front_page_penalty(affordance)
            penalty = mixer_interaction.front_page_cooldown.penalty + cur_penalty
            self._mixer_front_page_cooldown[affordance] = (cooldown_finish_time, penalty)

    def get_front_page_penalty(self, affordance):
        if affordance in self._mixer_front_page_cooldown:
            (cooldown_finish_time, penalty) = self._mixer_front_page_cooldown[affordance]
            now = services.time_service().sim_now
            if now >= cooldown_finish_time:
                del self._mixer_front_page_cooldown[affordance]
            else:
                return penalty
        return 0

    def is_sub_action_locked_out(self, affordance, target=None):
        if affordance is None:
            return False
        targeted_lockout_key = self._get_mixer_key(target, affordance, True)
        global_lockout_key = self._get_mixer_key(target, affordance, False)
        targeted_unlock_time = self._mixers_locked_out.get(targeted_lockout_key, None)
        global_unlock_time = self._mixers_locked_out.get(global_lockout_key, None)
        if targeted_unlock_time is None and global_unlock_time is None:
            return False
        now = services.time_service().sim_now
        locked_out = False
        if targeted_unlock_time is not None:
            if now >= targeted_unlock_time:
                del self._mixers_locked_out[targeted_lockout_key]
            else:
                locked_out = True
        if global_unlock_time is not None:
            if now >= global_unlock_time:
                del self._mixers_locked_out[global_lockout_key]
            else:
                locked_out = True
        return locked_out

    def create_default_si(self):
        context = InteractionContext(self, InteractionContext.SOURCE_SCRIPT, priority.Priority.Low)
        zone_id = services.current_zone_id()
        if build_buy.is_location_pool(zone_id, self.position, self.location.level):
            aop = posture_graph.SIM_SWIM_AOP
        else:
            aop = posture_graph.SIM_DEFAULT_AOP
        result = aop.interaction_factory(context)
        if not result:
            logger.error('Error creating default si: {}', result.reason)
        return result.interaction

    def pre_add(self, manager, *args, **kwargs):
        super().pre_add(manager, *args, **kwargs)
        self.queue = interactions.interaction_queue.InteractionQueue(self)
        self._obj_manager = manager
        self.hide(HiddenReasonFlag.NOT_INITIALIZED)

    @property
    def persistence_group(self):
        return self._persistence_group

    @persistence_group.setter
    def persistence_group(self, value):
        logger.callstack('Trying to override the persistence group of sim: {}.', self, owner='msantander')

    def on_add(self):
        super().on_add()
        zone_id = services.current_zone_id()
        _zone.add_sim(self.sim_id, zone_id)
        self._update_quadtree_location()
        with consume_exceptions('SimInfo', 'Error during motive creation'):
            self._create_motives()
        with consume_exceptions('SimInfo', 'Error during buff addition'):
            while self.sim_info.should_add_foreign_zone_buff(zone_id):
                self.add_buff(self.FOREIGN_ZONE_BUFF)
        with consume_exceptions('SimInfo', 'Error during inventory load'):
            self.inventory_component.load_items(self.sim_info.inventory_data)
        with consume_exceptions('SimInfo', 'Error during aspiration initialization'):
            self.aspiration_tracker.load(self.sim_info.aspirations_blob)
        with consume_exceptions('SimInfo', 'Error during pregnancy initialization'):
            self.sim_info.pregnancy_tracker.enable_pregnancy()
        with consume_exceptions('SimInfo', 'Error during spawn condition trigger'):
            self.manager.trigger_sim_spawn_condition(self.sim_id)
        with consume_exceptions('SimInfo', 'Error during Daycare refresh'):
            services.daycare_service().on_sim_spawn(self.sim_info)
        services.get_master_controller().add_sim(self)

    def _portal_added_callback(self, portal):
        portal.lock_sim(self)

    def _update_face_and_posture_gen(self, timeline):
        zone_id = services.current_zone_id()
        if build_buy.is_location_pool(zone_id, self.position, self.location.level):
            posture_type = posture_graph.SIM_SWIM_POSTURE_TYPE
        else:
            posture_type = posture_graph.SIM_DEFAULT_POSTURE_TYPE
        origin_posture_spec = get_origin_spec(posture_type)
        self.posture_state = PostureState(self, None, origin_posture_spec, {PostureSpecVariable.HAND: (Hand.LEFT,)})
        yield self.posture_state.kickstart_gen(timeline, self.routing_surface)
        self._start_animation_interaction()
        if self._update_facial_overlay not in self.Buffs.on_mood_changed:
            self.Buffs.on_mood_changed.append(self._update_facial_overlay)
        self._update_facial_overlay()

    def _update_multi_motive_buff_trackers(self):
        for multi_motive_buff_tracker in self._multi_motive_buff_trackers:
            multi_motive_buff_tracker.setup_callbacks()

    def _remove_multi_motive_buff_trackers(self):
        for multi_motive_buff_tracker in self._multi_motive_buff_trackers:
            multi_motive_buff_tracker.cleanup_callbacks()
        self._multi_motive_buff_trackers.clear()

    def add_callbacks(self):
        with consume_exceptions('SimInfo', 'Error during routing initialization'):
            self.register_on_location_changed(self._update_quadtree_location)
            self.register_on_location_changed(self._check_violations)
            self.on_plan_path.append(self._on_update_goals)
        with consume_exceptions('SimInfo', 'Error during navmesh initialization'):
            zone = services.get_zone(self.zone_id)
            while zone is not None:
                zone.navmesh_change_callbacks.append(self._on_navmesh_updated)
                zone.wall_contour_update_callbacks.append(self._on_navmesh_updated)
                zone.foundation_and_level_height_update_callbacks.append(self.validate_current_location_or_fgl)
        with consume_exceptions('SimInfo', 'Error during outfit initialization'):
            self.sim_info.on_outfit_changed.append(self.on_outfit_changed)

    def remove_callbacks(self):
        zone = services.current_zone()
        if self._on_update_goals in self.on_plan_path:
            self.on_plan_path.remove(self._on_update_goals)
        if self._on_location_changed_callbacks is not None and self._check_violations in self._on_location_changed_callbacks:
            self.unregister_on_location_changed(self._check_violations)
        if self._on_location_changed_callbacks is not None and self._update_quadtree_location in self._on_location_changed_callbacks:
            self.unregister_on_location_changed(self._update_quadtree_location)
        if self._on_navmesh_updated in zone.navmesh_change_callbacks:
            zone.navmesh_change_callbacks.remove(self._on_navmesh_updated)
        if self._on_navmesh_updated in zone.wall_contour_update_callbacks:
            zone.wall_contour_update_callbacks.remove(self._on_navmesh_updated)
        if self.validate_current_location_or_fgl in zone.foundation_and_level_height_update_callbacks:
            zone.foundation_and_level_height_update_callbacks.remove(self.validate_current_location_or_fgl)
        if self.on_outfit_changed in self.sim_info.on_outfit_changed:
            self.sim_info.on_outfit_changed.remove(self.on_outfit_changed)
        self.manager.unregister_portal_added_callback(self._portal_added_callback)

    def _startup_sim_gen(self, timeline):
        if self._starting_up:
            logger.error('Attempting to run _startup_sim while it is already running on another thread.')
            return
        previous_simulation_state = self._simulation_state
        self._starting_up = True
        try:
            yield self._update_face_and_posture_gen(timeline)
            self.queue.unlock()
            self.show(HiddenReasonFlag.NOT_INITIALIZED)
            if self._simulation_state == SimulationState.INITIALIZING:
                self.sim_info.verify_school(from_age_up=False)
                for commodity in tuple(self.commodity_tracker):
                    while not commodity.is_skill:
                        commodity.fixup_on_sim_instantiated()
                owning_household_of_active_lot = services.owning_household_of_active_lot()
                if owning_household_of_active_lot is not None:
                    for target_sim_info in owning_household_of_active_lot:
                        self.relationship_tracker.add_relationship_appropriateness_buffs(target_sim_info.id)
                self.autonomy_component.start_autonomy_alarm()
                situation_manager = services.get_zone_situation_manager()
                situation_manager.on_begin_sim_creation_notification(self)
                services.sim_spawner_service().on_sim_creation(self)
                situation_manager.on_end_sim_creation_notification(self)
                self.commodity_tracker.start_regular_simulation()
                for (buff, multi_motive_buff_motives) in self.MULTI_MOTIVE_BUFF_MOTIVES.items():
                    self._multi_motive_buff_trackers.append(sims.multi_motive_buff_tracker.MultiMotiveBuffTracker(self, multi_motive_buff_motives, buff))
                self.sim_info.buffs_component.on_sim_ready_to_simulate()
                self.sim_info.career_tracker.on_sim_startup()
                self.sim_info.whim_tracker.load_whims_info_from_proto()
                self.sim_info.whim_tracker.start_whims_tracker()
                self.update_sleep_schedule()
                sims.ghost.Ghost.make_ghost_if_needed(self.sim_info)
                if self.sim_info.is_in_travel_group():
                    travel_group = self.travel_group
                    current_region = services.current_region()
                    travel_group_region = region.get_region_instance_from_zone_id(travel_group.zone_id)
                    if not current_region.is_region_compatible(travel_group_region):
                        travel_group.remove_sim_info(self.sim_info)
                if services.current_zone().is_zone_running:
                    self.sim_info.away_action_tracker.refresh()
                    if self.is_selectable:
                        self.sim_info.aspiration_tracker.initialize_aspiration()
                        self.sim_info.aspiration_tracker.set_update_alarm()
                        self.sim_info.career_tracker.activate_career_aspirations()
                if self.is_selected:
                    self.client.notify_active_sim_changed(None, self)
                self.manager.add_portal_lock(self, self._portal_added_callback)
            elif self._simulation_state == SimulationState.RESETTING:
                self.remove_callbacks()
            self.on_outfit_changed(self._sim_info.get_current_outfit())
            self.refresh_los_constraint()
            self._simulation_state = SimulationState.SIMULATING
            self.add_callbacks()
            self.on_start_up(self)
            self._start_environment_score()
            self.update_intended_position_on_active_lot(update_ui=True)
            self._update_walkstyle()
        finally:
            self._starting_up = False
            if previous_simulation_state == SimulationState.RESETTING:
                services.current_zone().service_manager.on_sim_reset(self)

    def on_remove(self):
        self.sim_info.buffs_component.on_sim_removed()
        self._stop_environment_score()
        self.commodity_tracker.remove_non_persisted_commodities()
        self.commodity_tracker.stop_regular_simulation()
        self.sim_info.time_sim_was_saved = services.time_service().sim_now
        if self.is_selectable:
            self.commodity_tracker.start_low_level_simulation()
        self.asm_auto_exit.clear()
        zone = services.current_zone()
        if zone.master_controller is not None:
            zone.master_controller.remove_sim(self)
        self.on_posture_event.clear()
        self.on_slot = None
        _zone.remove_sim(self.sim_id, zone.id)
        self._is_removed = True
        super().on_remove()
        self._posture_state = None
        self.on_start_up.clear()
        self.remove_callbacks()
        self.on_intended_location_changed.clear()
        zone.sim_quadtree.remove(self.sim_id, placement.ItemType.SIM_POSITION, 0)
        zone.sim_quadtree.remove(self.sim_id, placement.ItemType.SIM_INTENDED_POSITION, 0)
        if self.refresh_los_constraint in zone.wall_contour_update_callbacks:
            zone.wall_contour_update_callbacks.remove(self.refresh_los_constraint)
        self._remove_multi_motive_buff_trackers()
        self.object_ids_to_ignore.clear()
        self._si_state = None
        self._mixers_locked_out.clear()
        self._mixer_front_page_cooldown.clear()

    def post_remove(self):
        super().post_remove()
        self._clear_clothing_buffs()
        self.queue = None

    @property
    def allow_running_for_long_distance_routes(self):
        for buff in self.Buffs:
            while not buff.allow_running_for_long_distance_routes:
                return False
        return True

    def _update_facial_overlay(self, *_, **__):

        def restart_overlay_asm(asm):
            asm.set_current_state('entry')
            return True

        if self._update_facial_overlay_interaction is None:
            return
        overlay_animation = self.FACIAL_OVERLAY_ANIMATION(self._update_facial_overlay_interaction, setup_asm_additional=restart_overlay_asm, enable_auto_exit=False)
        asm = overlay_animation.get_asm()
        if asm is None:
            logger.warn('Sim: {} - overlay_animation.get_asm() returned None instead of a valid ASM in sim._update_facial_overlay()', self)
            return
        arb = animation.arb.Arb()
        overlay_animation.append_to_arb(asm, arb)
        arb_element = ArbElement(arb)
        arb_element.distribute()

    def _update_quadtree_location(self, *_, **__):
        pos = self.position
        pos = sims4.math.Vector2(pos.x, pos.z)
        geo = sims4.geometry.QtCircle(pos, self._quadtree_radius)
        services.sim_quadtree().insert(self, self.sim_id, placement.ItemType.SIM_POSITION, geo, self.routing_surface.secondary_id, False, 0)

    def add_stand_slot_reservation(self, interaction, position, routing_surface, excluded_sims):
        interaction.add_liability(STAND_SLOT_LIABILITY, StandSlotReservationLiability(self, interaction))
        excluded_sims.add(self)
        self._stand_slot_reservation = position
        pos_2d = sims4.math.Vector2(position.x, position.z)
        geo = sims4.geometry.QtCircle(pos_2d, self._quadtree_radius)
        services.sim_quadtree().insert(self, self.sim_id, placement.ItemType.ROUTE_GOAL_SUPPRESSOR, geo, routing_surface.secondary_id, False, 0)
        reservation_radius = self._quadtree_radius*2
        polygon = sims4.geometry.generate_circle_constraint(6, position, reservation_radius)
        self.on_slot = (position, polygon, routing_surface)
        UserFootprintHelper.force_move_sims_in_polygon(polygon, routing_surface, exclude=excluded_sims)

    def remove_stand_slot_reservation(self, interaction):
        services.sim_quadtree().remove(self.sim_id, placement.ItemType.ROUTE_GOAL_SUPPRESSOR, 0)
        self.on_slot = None
        self.stand_slot_reservation_removed_callbacks(sim=self)

    def get_stand_slot_reservation_violators(self, excluded_sims=()):
        if not self.on_slot:
            return
        (_, polygon, routing_surface) = self.on_slot
        violators = []
        excluded_sims = {sim for sim in itertools.chain((self,), excluded_sims)}
        for sim_nearby in placement.get_nearby_sims(polygon.centroid(), routing_surface.secondary_id, radius=polygon.radius(), exclude=excluded_sims):
            while sims4.geometry.test_point_in_polygon(sim_nearby.position, polygon):
                violators.append(sim_nearby)
        return violators

    def _check_violations(self, *_, **__):
        if services.privacy_service().check_for_late_violators(self):
            return
        for reaction_trigger in self.reaction_triggers.values():
            reaction_trigger.intersect_and_execute(self)

    @property
    def quadtree_radius(self):
        return self._quadtree_radius

    @property
    def intended_location(self):
        if self.queue is not None and self.queue.transition_controller is not None:
            return self.queue.transition_controller.intended_location(self)
        return self.location

    @property
    def intended_transform(self):
        return self.intended_location.transform

    @property
    def intended_routing_surface(self):
        return self.intended_location.routing_surface

    @property
    def intended_position_on_active_lot(self):
        return self._intended_position_on_active_lot

    def get_intended_location_excluding_transition(self, exclude_transition):
        if self.queue.transition_controller is None or self.queue.transition_controller is exclude_transition:
            return self.location
        return self.intended_location

    @property
    def is_moving(self):
        return not sims4.math.transform_almost_equal_2d(self.intended_transform, self.transform) or self.intended_routing_surface != self.routing_surface

    def _on_update_goals(self, goal_list, starting):
        NUM_GOALS_TO_RESERVE = 2
        if starting:
            for (index, goal) in enumerate(goal_list):
                if index >= NUM_GOALS_TO_RESERVE:
                    break
                pos = sims4.math.Vector2(goal.position.x, goal.position.z)
                geo = sims4.geometry.QtCircle(pos, self._quadtree_radius)
                services.sim_quadtree().insert(self, self.sim_id, placement.ItemType.SIM_INTENDED_POSITION, geo, goal.routing_surface_id.secondary_id, False, index + 1)
        else:
            for (index, goal) in enumerate(goal_list):
                if index >= NUM_GOALS_TO_RESERVE:
                    break
                services.sim_quadtree().remove(self.sim_id, placement.ItemType.SIM_INTENDED_POSITION, index + 1)

    def _should_invalidate_location(self):
        return False

    def commodities_gen(self):
        for stat in self.commodity_tracker:
            yield stat

    def static_commodities_gen(self):
        for stat in self.static_commodity_tracker:
            yield stat

    def statistics_gen(self):
        for stat in self.statistic_tracker:
            yield stat

    def object_tags_override_off_lot_autonomy_ref_count(self, object_tag_list):
        return self.sim_info.object_tags_override_off_lot_autonomy_ref_count(object_tag_list)

    def skills_gen(self):
        for stat in self.commodities_gen():
            while stat.is_skill:
                yield stat

    def all_skills(self):
        return self.sim_info.all_skills()

    def scored_stats_gen(self):
        for stat in self.statistics_gen():
            while stat.is_scored and self.is_scorable(stat.stat_type):
                yield stat
        for commodity in self.commodities_gen():
            while commodity.is_scored and self.is_scorable(commodity.stat_type):
                yield commodity
        for static_commodity in self.static_commodities_gen():
            while static_commodity.is_scored and self.is_scorable(static_commodity.stat_type):
                yield static_commodity

    @property
    def walkstyle(self):
        for walkstyle in self.sim_info._walkstyle_requests:
            while walkstyle.priority != WalkStylePriority.COMBO:
                return walkstyle.walkstyle
        return self.default_walkstyle

    @property
    def walkstyle_list(self):
        return self.sim_info.walkstyle_list

    @property
    def default_walkstyle(self):
        return self.sim_info._walkstyle_requests[-1].walkstyle

    @default_walkstyle.setter
    def default_walkstyle(self, walkstyle):
        self.sim_info._walkstyle_requests[-1] = WalkStyleRequest(-1, walkstyle)
        self._update_walkstyle()

    def request_walkstyle(self, walkstyle_request, uid):
        self.sim_info.request_walkstyle(walkstyle_request, uid)

    def remove_walkstyle(self, uid):
        self.sim_info.remove_walkstyle(uid)

    def _update_walkstyle(self):
        for primitive in self.primitives:
            try:
                primitive.request_walkstyle_update()
            except AttributeError:
                pass

    def route_finished(self, path_id):
        for primitive in self.primitives:
            while hasattr(primitive, 'route_finished'):
                primitive.route_finished(path_id)

    def route_time_update(self, path_id, current_time):
        for primitive in self.primitives:
            while hasattr(primitive, 'route_time_update'):
                primitive.route_time_update(path_id, current_time)

    def force_update_routing_location(self):
        for primitive in self.primitives:
            while hasattr(primitive, 'update_routing_location'):
                primitive.update_routing_location()

    def populate_localization_token(self, *args, **kwargs):
        self.sim_info.populate_localization_token(*args, **kwargs)

    def create_posture_interaction_context(self):
        return InteractionContext(self, InteractionContext.SOURCE_POSTURE_GRAPH, Priority.High)

    @property
    def posture(self):
        return self.posture_state.body

    @property
    def posture_state(self):
        return self._posture_state

    @posture_state.setter
    def posture_state(self, value):
        if self._posture_state.carry_targets != value.carry_targets:
            for interaction in self.queue:
                while interaction.transition is not None and not interaction.transition.running and (interaction.carry_track is not None or interaction.should_carry_create_target()):
                    interaction.transition.reset_sim_progress(self)
        if self._posture_state is not None and value is not None and self._posture_state.body != value.body:
            self._update_facial_overlay_interaction.clear_animation_liability_cache()
            self.animation_interaction.clear_animation_liability_cache()
        self._posture_state = value
        if any(value.carry_targets):
            self.clear_portal_mask_flag(PortalRequiredFlag.REQUIRE_NO_CARRY)
        else:
            self.set_portal_mask_flag(PortalRequiredFlag.REQUIRE_NO_CARRY)
        self._posture_target_refs.clear()
        for aspect in self._posture_state.aspects:
            while aspect.target is not None:
                self._posture_target_refs.append(aspect.target.ref(lambda _: self.reset(ResetReason.RESET_EXPECTED, self, 'Posture target went away.')))
        if self.posture_state is not None:
            connectivity_handles = self.posture_state.connectivity_handles
            if connectivity_handles is not None:
                self._pathplan_context.connectivity_handles = connectivity_handles

    @property
    def connectivity_handles(self):
        return self._pathplan_context.connectivity_handles

    def is_surface(self, *args, **kwargs):
        return False

    @caches.cached
    def ignore_group_socials(self, excluded_group=None):
        for si in self.si_state:
            if excluded_group is not None and si.social_group is excluded_group:
                pass
            while si.ignore_group_socials:
                return True
        next_interaction = self.queue.peek_head()
        if next_interaction is not None and (next_interaction.is_super and next_interaction.ignore_group_socials) and (excluded_group is None or next_interaction.social_group is not excluded_group):
            return True
        return False

    @property
    def disallow_as_mixer_target(self):
        return any(si.disallow_as_mixer_target for si in self.si_state)

    def get_groups_for_sim_gen(self):
        for group in self._social_groups:
            while self in group:
                yield group

    def get_main_group(self):
        for group in self.get_groups_for_sim_gen():
            while not group.is_side_group:
                return group

    def get_visible_group(self):
        visible_group = None
        for group in self.get_groups_for_sim_gen():
            while group.is_visible:
                if not group.is_side_group:
                    return group
                visible_group = group
        return visible_group

    def is_in_side_group(self):
        return any(self in g and g.is_side_group for g in self.get_groups_for_sim_gen())

    def is_in_group_with(self, target_sim):
        return any(target_sim in group for group in self.get_groups_for_sim_gen())

    @caches.cached
    def get_social_context(self):
        sims = set(itertools.chain(*self.get_groups_for_sim_gen()))
        social_context_bit = SocialContextTest.get_overall_short_term_context_bit(*sims)
        if social_context_bit is not None:
            size_limit = social_context_bit.size_limit
            if size_limit is not None:
                if len(sims) > size_limit.size:
                    social_context_bit = size_limit.transformation
        return social_context_bit

    def on_social_context_changed(self):
        SocialContextTest.get_overall_short_term_context_bit.cache.clear()
        self.get_social_context.cache.clear()
        for group in self.get_groups_for_sim_gen():
            group.on_social_context_changed()

    def without_social_focus(self, sequence):
        new_sequence = sequence
        for group in self.get_groups_for_sim_gen():
            new_sequence = group.without_social_focus(self, self, new_sequence)
        return new_sequence

    def set_mood_asm_parameter(self, asm, actor_name):
        mood_asm_name = self.get_mood_animation_param_name()
        if mood_asm_name is not None:
            asm.set_actor_parameter(actor_name, self, 'mood', mood_asm_name.lower())

    def set_trait_asm_parameters(self, asm, actor_name):
        sim_traits = self.sim_info.trait_tracker.equipped_traits
        asm_param_dict = {}
        for trait in sim_traits:
            while trait.asm_param_name is not None:
                asm_param_dict[(trait.asm_param_name, actor_name)] = True
        asm.update_locked_params(asm_param_dict)

    def evaluate_si_state_and_cancel_incompatible(self, finishing_type, cancel_reason_msg):
        sim_transform_constraint = interactions.constraints.Transform(self.transform, routing_surface=self.routing_surface)
        sim_posture_constraint = self.posture_state.posture_constraint_strict
        sim_constraint = sim_transform_constraint.intersect(sim_posture_constraint)
        (_, included_sis) = self.si_state.get_combined_constraint(sim_constraint, None, None, None, True, True)
        for si in self.si_state:
            while si not in included_sis and si.basic_content is not None and si.basic_content.staging:
                si.cancel(finishing_type, cancel_reason_msg)

    def refresh_los_constraint(self, *args, target_position=DEFAULT, **kwargs):
        if target_position is DEFAULT:
            target_position = self.intended_position
            target_forward = self.intended_forward
            target_routing_surface = self.intended_routing_surface
        else:
            target_forward = self.forward
            target_routing_surface = self.routing_surface
        if target_routing_surface == self.lineofsight_component.routing_surface and sims4.math.vector3_almost_equal_2d(target_position, self.lineofsight_component.position):
            return
        target_position = target_position + target_forward*self.lineofsight_component.facing_offset
        self.lineofsight_component.generate(position=target_position, routing_surface=target_routing_surface, lock=True, build_convex=True)
        self._los_constraint = self.lineofsight_component.constraint
        zone = services.current_zone()
        if self.refresh_los_constraint not in zone.wall_contour_update_callbacks:
            zone.wall_contour_update_callbacks.append(self.refresh_los_constraint)
        self._social_group_constraint = None

    @property
    def los_constraint(self):
        return self._los_constraint

    def can_see(self, obj):
        if obj.intended_position is not None:
            obj_position = obj.intended_position
        else:
            obj_position = obj.position
        return self.los_constraint.geometry.contains_point(obj_position)

    def get_social_group_constraint(self, si):
        if self._social_group_constraint is None:
            si_constraint = self.si_state.get_total_constraint(priority=si.priority if si is not None else None, include_inertial_sis=True, to_exclude=si)
            for base_constraint in si_constraint:
                while base_constraint.geometry is not None:
                    break
            if self.queue.running is not None and self.queue.running.is_super:
                base_constraint = interactions.constraints.Transform(self.transform, routing_surface=self.routing_surface)
            if base_constraint.geometry is not None and base_constraint.geometry.polygon:
                los_constraint = self.los_constraint
                base_geometry = base_constraint.geometry
                expanded_polygons = []
                for sub_polygon in base_geometry.polygon:
                    if len(sub_polygon) == 1:
                        new_polygon = sims4.geometry.generate_circle_constraint(LOSAndSocialConstraintTuning.num_sides_for_circle_expansion_of_point_constraint, sub_polygon[0], LOSAndSocialConstraintTuning.constraint_expansion_amount)
                    else:
                        while len(sub_polygon) > 1:
                            center = sum(sub_polygon, sims4.math.Vector3.ZERO())/len(sub_polygon)
                            new_polygon = sims4.geometry.inflate_polygon(sub_polygon, LOSAndSocialConstraintTuning.constraint_expansion_amount, centroid=center)
                            expanded_polygons.append(new_polygon)
                    expanded_polygons.append(new_polygon)
                new_compound_polygon = sims4.geometry.CompoundPolygon(expanded_polygons)
                new_restricted_polygon = sims4.geometry.RestrictedPolygon(new_compound_polygon, [])
                base_constraint = interactions.constraints.Constraint(geometry=new_restricted_polygon, routing_surface=los_constraint.routing_surface)
                intersection = base_constraint.intersect(los_constraint)
                self._social_group_constraint = intersection
            else:
                self._social_group_constraint = interactions.constraints.Anywhere()
        return self._social_group_constraint

    def get_next_work_priority(self):
        if not self.is_simulating:
            return Priority.Critical
        next_interaction = self.queue.get_head()
        if next_interaction is not None:
            return next_interaction.priority
        return Priority.Low

    def get_next_work(self):
        if self.is_being_destroyed:
            logger.error('sim.get_next_work() called for Sim {} when they were in the process of being destroyed.', self, owner='tastle/sscholl')
            return WorkRequest()
        if not self.is_simulating:
            if self._starting_up:
                return WorkRequest()
            return WorkRequest(work_element=elements.GeneratorElement(self._startup_sim_gen), required_sims=(self,))
        _ = self.queue._get_head()
        next_interaction = self.queue.get_head()
        if next_interaction is None and services.current_zone().is_zone_running:
            if any(not i.is_super for i in self.queue._autonomy):
                for i in tuple(self.queue._autonomy):
                    i.cancel(FinishingType.INTERACTION_QUEUE, 'Blocked interaction in autonomy bucket, canceling all interactions in the autonomy bucket to fix.')
            else:
                self.run_subaction_autonomy()
                next_interaction = self.queue.get_head()
        if next_interaction is not None:
            next_interaction.refresh_and_lock_required_sims()
            required_sims = next_interaction.required_sims(for_threading=True)
            element = elements.GeneratorElement(functools.partial(self._process_interaction_gen, interaction=next_interaction))
            return WorkRequest(work_element=element, required_sims=required_sims, additional_resources=next_interaction.required_resources(), set_work_timestamp=next_interaction.set_work_timestamp, debug_name=str(next_interaction))
        return WorkRequest()

    def get_idle_element(self, duration=10):
        if self.is_being_destroyed:
            logger.error('sim.get_idle_element() called for Sim {} when they were in the process of being destroyed.', self, owner='tastle/sscholl')
        if not self.is_simulating:
            return (None, None)
        possible_idle_behaviors = []
        for si in self.si_state:
            idle_behavior = si.get_idle_behavior()
            while idle_behavior is not None:
                possible_idle_behaviors.append((si, idle_behavior))
        if possible_idle_behaviors:
            (_, idle_behavior) = random.choice(possible_idle_behaviors)
        else:
            idle_behavior = self.posture.get_idle_behavior()
        sleep_behavior = build_element((elements.SoftSleepElement(date_and_time.create_time_span(minutes=duration)), self.si_state.process_gen))
        idle_sequence = build_element([build_critical_section(idle_behavior, flush_all_animations), sleep_behavior])
        idle_sequence = with_skippable_animation_time((self,), sequence=idle_sequence)
        for group in self.get_groups_for_sim_gen():
            idle_sequence = group.with_listener_focus(self, self, idle_sequence)

        def do_idle_behavior(timeline):
            nonlocal idle_sequence
            with gsi_handlers.sim_timeline_handlers.archive_sim_timeline_context_manager(self, 'Sim', 'Process Idle Interaction'):
                try:
                    self.queue._apply_next_pressure()
                    result = yield element_utils.run_child(timeline, idle_sequence)
                    return result
                finally:
                    idle_sequence = None

        def cancel_idle_behavior():
            nonlocal idle_sequence
            if idle_sequence is not None:
                idle_sequence.trigger_soft_stop()
                idle_sequence = None

        return (elements.GeneratorElement(do_idle_behavior), cancel_idle_behavior)

    def _process_interaction_gen(self, timeline, interaction=None):
        with gsi_handlers.sim_timeline_handlers.archive_sim_timeline_context_manager(self, 'Sim', 'Process Interaction', interaction):
            try:
                if self.queue.get_head() is not interaction:
                    logger.info('Interaction has changed from {} to {} after work was scheduled. Bailing.', interaction, self.queue.get_head())
                    return
                yield self.queue.process_one_interaction_gen(timeline)
            finally:
                interaction.unlock_required_sims()

    def get_resolver(self):
        return event_testing.resolver.SingleSimResolver(self.sim_info)

    def push_super_affordance(self, super_affordance, target, context, **kwargs):
        if isinstance(super_affordance, str):
            super_affordance = services.get_instance_manager(sims4.resources.Types.INTERACTION).get(super_affordance)
            if not super_affordance:
                raise ValueError('{0} is not a super affordance'.format(super_affordance))
        aop = interactions.aop.AffordanceObjectPair(super_affordance, target, super_affordance, None, **kwargs)
        res = aop.test_and_execute(context)
        return res

    def test_super_affordance(self, super_affordance, target, context, **kwargs):
        if isinstance(super_affordance, str):
            super_affordance = services.get_instance_manager(sims4.resources.Types.INTERACTION).get(super_affordance)
            if not super_affordance:
                raise ValueError('{0} is not a super affordance'.format(super_affordance))
        aop = interactions.aop.AffordanceObjectPair(super_affordance, target, super_affordance, None, **kwargs)
        res = aop.test(context)
        return res

    def find_interaction_by_id(self, id_to_find):
        id_to_find = self.ui_manager.get_routing_owner_id(id_to_find)
        interaction = None
        if self.queue is not None:
            interaction = self.queue.find_interaction_by_id(id_to_find)
            if interaction is None:
                transition_controller = self.queue.transition_controller
                if transition_controller is not None:
                    (target_si, _) = transition_controller.interaction.get_target_si()
                    if target_si is not None and target_si.id == id_to_find:
                        return target_si
        if interaction is None and self.si_state is not None:
            interaction = self.si_state.find_interaction_by_id(id_to_find)
        return interaction

    def find_continuation_by_id(self, source_id):
        interaction = None
        if self.queue is not None:
            interaction = self.queue.find_continuation_by_id(source_id)
        if interaction is None and self.si_state is not None:
            interaction = self.si_state.find_continuation_by_id(source_id)
        return interaction

    def find_sub_interaction_by_aop_id(self, super_id, aop_id):
        interaction = None
        if self.queue is not None:
            interaction = self.queue.find_sub_interaction(super_id, aop_id)
        return interaction

    def set_autonomy_preference(self, preference, obj):
        if preference.is_scoring:
            self.sim_info.autonomy_scoring_preferences[preference.tag] = obj.id
        else:
            self.sim_info.autonomy_use_preferences[preference.tag] = obj.id

    def is_object_scoring_preferred(self, preference_tag, obj):
        return self._check_preference(preference_tag, obj, self.sim_info.autonomy_scoring_preferences)

    def is_object_use_preferred(self, preference_tag, obj):
        return self._check_preference(preference_tag, obj, self.sim_info.autonomy_use_preferences)

    @property
    def autonomy_settings(self):
        return self.get_autonomy_settings()

    def _check_preference(self, preference_tag, obj, preference_map):
        obj_id = preference_map.get(preference_tag, None)
        return obj.id == obj_id

    def _clear_clothing_buffs(self):
        for (buff_type, buff_handle) in self._buff_handles:
            if buff_handle is not None:
                self.remove_buff(buff_handle)
            stat = self.get_stat_instance(buff_type.commodity)
            while stat is not None:
                stat.decay_enabled = True
        self._buff_handles.clear()

    def on_outfit_changed(self, category_and_index):
        self._clear_clothing_buffs()
        part_ids = self.sim_info.get_part_ids_for_current_outfit()
        if part_ids:
            buff_guids = cas.cas.get_buff_from_part_ids(part_ids)
            for buff_guid in buff_guids:
                buff_type = services.get_instance_manager(sims4.resources.Types.BUFF).get(buff_guid)
                if buff_type is None:
                    logger.error('Error one of the parts in current outfit does not have a valid buff')
                buff_handle = None
                if buff_type.can_add(self):
                    buff_handle = self.add_buff(buff_type, buff_reason=self.BUFF_CLOTHING_REASON)
                else:
                    if buff_type.commodity is None:
                        pass
                    stat = self.get_stat_instance(buff_type.commodity)
                    if stat is not None:
                        stat.decay_enabled = True
                self._buff_handles.append((buff_type, buff_handle))

    def load_staged_interactions(self):
        return self.si_state.load_staged_interactions(self.sim_info.si_state)

    def load_transitioning_interaction(self):
        return self.si_state.load_transitioning_interaction(self.sim_info.si_state)

    def load_queued_interactions(self):
        self.si_state.load_queued_interactions(self.sim_info.si_state)

    def update_related_objects(self, triggering_sim, forced_interaction=None):
        if not triggering_sim.valid_for_distribution:
            return
        PARTICIPANT_TYPE_MASK = interactions.ParticipantType.Actor | interactions.ParticipantType.Object | interactions.ParticipantType.Listeners | interactions.ParticipantType.CarriedObject | interactions.ParticipantType.CraftingObject | interactions.ParticipantType.ActorSurface
        relevant_obj_ids = set()
        relevant_obj_ids.add(self.id)
        if forced_interaction is not None:
            objs = forced_interaction.get_participants(PARTICIPANT_TYPE_MASK)
            for obj in objs:
                relevant_obj_ids.add(obj.id)
        for i in self.running_interactions_gen(Interaction):
            objs = i.get_participants(PARTICIPANT_TYPE_MASK)
            for obj in objs:
                relevant_obj_ids.add(obj.id)
        if self.queue.running is not None:
            objs = self.queue.running.get_participants(PARTICIPANT_TYPE_MASK)
            for obj in objs:
                relevant_obj_ids.add(obj.id)
        op = distributor.ops.SetRelatedObjects(relevant_obj_ids, self.id)
        dist = Distributor.instance()
        dist.add_op(triggering_sim, op)

    @caches.cached(maxsize=10)
    def is_on_active_lot(self, tolerance=0):
        lot = services.current_zone().lot
        position = self.position
        if not lot.is_position_on_lot(position, tolerance):
            return False
        intended_position = self.intended_position
        if intended_position != position and not lot.is_position_on_lot(intended_position, tolerance):
            return False
        return True

    def log_sim_info(self, *args, **kwargs):
        self.sim_info.log_sim_info(*args, **kwargs)

    def should_suppress_social_front_page_when_targeted(self):
        if any(buff.suppress_social_front_page_when_targeted for buff in self.Buffs):
            return True
        return False

    def discourage_route_to_join_social_group(self):
        return self.sim_info.discourage_route_to_join_social_group()

lock_instance_tunables(Sim, _persists=False, _world_file_object_persists=False)
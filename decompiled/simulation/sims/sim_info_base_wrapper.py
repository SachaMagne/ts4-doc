from contextlib import contextmanager
from protocolbuffers import DistributorOps_pb2
from protocolbuffers.Localization_pb2 import LocalizedStringToken
from buffs.appearance_modifier.appearance_tracker import AppearanceTracker
from cas.cas import BaseSimInfo, generate_offspring, generate_merged_outfit, is_duplicate_merged_outfit
from sims.baby.baby_tuning import BabyTuning
from sims.outfits.outfit_enums import OutfitCategory, FilterFlag
from sims.sim_info_types import Gender, Age
from sims.sim_outfits import SimOutfits
from sims4.callback_utils import CallableList
from singletons import DEFAULT
import distributor.fields
import distributor.ops
import id_generator

class SimInfoBaseWrapper:
    __qualname__ = 'SimInfoBaseWrapper'

    def __init__(self, *args, sim_id:int=0, gender:Gender=Gender.MALE, age:Age=Age.ADULT, first_name:str='', last_name:str='', full_name_key=0, physique:str='', skin_tone=1, **kwargs):
        super().__init__(*args, **kwargs)
        self.sim_id = sim_id or id_generator.generate_object_id()
        self.primitives = distributor.ops.DistributionSet(self)
        self.manager = None
        self._base = BaseSimInfo(sim_id, first_name, last_name, full_name_key, age, gender, skin_tone, physique)
        self._base.voice_pitch = 0.0
        self._base.voice_actor = 0
        self._base.voice_effect = 0
        self._base.facial_attributes = ''
        self.on_outfit_changed = CallableList()
        self.on_outfit_generated = CallableList()
        self._outfits = SimOutfits(self)
        self._set_current_outfit_without_distribution((OutfitCategory.EVERYDAY, 0))
        self._previous_outfit = (OutfitCategory.EVERYDAY, 0)
        self._preload_outfit_list = []
        self.on_preload_outfits_changed = CallableList()
        self.appearance_tracker = AppearanceTracker(self)

    @property
    def id(self):
        return self.sim_id

    @id.setter
    def id(self, value):
        pass

    def get_create_op(self, *args, **kwargs):
        additional_ops = list(self.get_additional_create_ops_gen())
        return distributor.ops.SimInfoCreate(self, additional_ops=additional_ops, *args, **kwargs)

    def get_additional_create_ops_gen(self):
        pass

    def get_delete_op(self):
        return distributor.ops.SimInfoDelete()

    def get_create_after_objs(self):
        return ()

    @property
    def valid_for_distribution(self):
        return self.manager is not None and self.id is not None

    def populate_localization_token(self, token):
        token.type = LocalizedStringToken.SIM
        token.first_name = self.first_name
        token.last_name = self.last_name
        token.full_name_key = self.full_name_key
        token.is_female = self.is_female

    def save_sim_info(self, sim_info_msg):
        sim_info_msg.gender = self.gender
        sim_info_msg.age = self.age
        sim_info_msg.physique = self.physique
        sim_info_msg.facial_attributes = self.facial_attributes
        sim_info_msg.skin_tone = self.skin_tone
        sim_info_msg.outfits = self.sim_outfits.save_sim_outfits()
        (current_outfit_type, current_outfit_index) = self.get_current_outfit()
        sim_info_msg.current_outfit_type = current_outfit_type
        sim_info_msg.current_outfit_index = current_outfit_index
        (previous_outfit_type, previous_outfit_index) = self.get_previous_outfit()
        sim_info_msg.previous_outfit_type = previous_outfit_type
        sim_info_msg.previous_outfit_index = previous_outfit_index

    def load_sim_info(self, sim_info_msg):
        self.gender = sim_info_msg.gender
        self.age = sim_info_msg.age
        self.physique = sim_info_msg.physique
        self.facial_attributes = sim_info_msg.facial_attributes
        self.skin_tone = sim_info_msg.skin_tone
        self.sim_outfits.load_sim_outfits_from_persistence_proto(self.sim_id, sim_info_msg.outfits)
        self.serialize_and_set_base_outfits()
        self.set_current_outfit((sim_info_msg.current_outfit_type, sim_info_msg.current_outfit_index))
        self._previous_outfit = (sim_info_msg.previous_outfit_type, sim_info_msg.previous_outfit_index)

    def get_traits(self):
        return ()

    def get_sim_instance(self, *args, **kwargs):
        pass

    def resend_physical_attributes(self):
        self.resend_skin_tone()
        self.resend_facial_attributes()
        self.resend_physique()
        self.resend_outfits()
        self.resend_voice_pitch()
        self.resend_voice_actor()
        self.resend_voice_effect()

    def load_from_resource(self, resource_key):
        if not self._base.load_from_resource(resource_key):
            return
        self.load_outfits_from_cas_proto_and_send_to_client()
        self.resend_physical_attributes()

    def apply_genetics(self, parent_a, parent_b, **kwargs):
        generate_offspring(parent_a._base, parent_b._base, self._base, **kwargs)

    def generate_career_outfit(self, tag_list=[]):
        self._base.generate_career_outfit(tag_list)
        self.load_outfits_from_cas_proto_and_send_to_client()
        self.on_outfit_generated(OutfitCategory.CAREER, 0)

    def generate_outfit(self, outfit_category=OutfitCategory.EVERYDAY, outfit_index=0, tag_list=[], filter_flag=DEFAULT, send_outfits_to_client=True):
        filter_flag = FilterFlag.USE_EXISTING_IF_APPROPRIATE if filter_flag is DEFAULT else filter_flag
        self._base.generate_outfit(outfit_category, outfit_index, tag_list, filter_flag=filter_flag)
        if send_outfits_to_client:
            self.load_outfits_from_cas_proto_and_send_to_client()
        self.on_outfit_generated(outfit_category, outfit_index)
        if outfit_category == OutfitCategory.SITUATION:
            self._outfits.situation_outfit_dirty = True

    def generate_merged_outfit(self, source_sim_info, destination_outfit, template_outfit, source_outfit, preserve_outfit_flags=False):
        generate_merged_outfit(self._base, source_sim_info._base, destination_outfit[0], destination_outfit[1], template_outfit[0], template_outfit[1], source_outfit[0], source_outfit[1])
        self.load_outfits_from_cas_proto_and_send_to_client()
        if preserve_outfit_flags:
            source_outfit_data = source_sim_info.sim_outfits.get_outfit(*source_outfit)
            destination_outfit_data = self.sim_outfits.get_outfit(*destination_outfit)
            destination_outfit_data['outfit_flags'] = source_outfit_data['outfit_flags']
            self.serialize_and_set_base_outfits()

    @contextmanager
    def set_temporary_outfit_flags_on_all_outfits(self, outfit_flags, source_sim_info):
        previous_outfit_flags = []
        for outfit_data in source_sim_info.sim_outfits:
            while outfit_flags != DEFAULT:
                previous_outfit_flags.append(outfit_data['outfit_flags'])
                outfit_data['outfit_flags'] = outfit_flags
        source_sim_info.serialize_and_set_base_outfits()
        try:
            yield None
        finally:
            for (index, outfit_data) in enumerate(source_sim_info.sim_outfits):
                while outfit_flags != DEFAULT:
                    outfit_data['outfit_flags'] = previous_outfit_flags[index]
            source_sim_info.serialize_and_set_base_outfits()

    @contextmanager
    def set_temporary_outfit_flags(self, outfit_data, outfit_flags, source_sim_info):
        if outfit_flags != DEFAULT:
            previous_outfit_flags = outfit_data['outfit_flags']
            outfit_data['outfit_flags'] = outfit_flags
            source_sim_info.serialize_and_set_base_outfits()
        try:
            yield None
        finally:
            if outfit_flags != DEFAULT:
                outfit_data['outfit_flags'] = previous_outfit_flags
                source_sim_info.serialize_and_set_base_outfits()

    def generate_merged_outfits_for_category(self, source_sim_info, outfit_category, outfit_flags=DEFAULT, **kwargs):
        current_outfit = self.get_current_outfit()
        for (outfit_index, outfit_data) in enumerate(source_sim_info.sim_outfits.get_outfits_in_category(outfit_category)):
            merged_outfit = (outfit_category, outfit_index)
            if self.sim_outfits.has_outfit(merged_outfit):
                template_outfit = merged_outfit
            elif self.sim_outfits.has_outfit((outfit_category, 0)):
                template_outfit = (outfit_category, 0)
            else:
                template_outfit = self.get_current_outfit()
            with self.set_temporary_outfit_flags(outfit_data, outfit_flags, source_sim_info):
                self.generate_merged_outfit(source_sim_info, merged_outfit, template_outfit, merged_outfit, **kwargs)
        while len(self.sim_outfits.get_outfits_in_category(outfit_category)) > len(source_sim_info.sim_outfits.get_outfits_in_category(outfit_category)):
            self.sim_outfits.remove_outfit(outfit_category)
        self.serialize_and_set_base_outfits()
        if self.sim_outfits.has_outfit(current_outfit):
            self.set_current_outfit(current_outfit)
        else:
            if self.sim_outfits.has_outfit((outfit_category, 0)):
                self.set_current_outfit((outfit_category, 0))
            elif self.sim_outfits.has_outfit((OutfitCategory.EVERYDAY, 0)):
                self.set_current_outfit((OutfitCategory.EVERYDAY, 0))
            else:
                self.set_current_outfit((OutfitCategory.BATHING, 0))
            self.set_previous_outfit()

    def is_generated_outfit_duplicate_in_category(self, source_sim_info, source_outfit):
        return any(is_duplicate_merged_outfit(self._base, source_sim_info._base, source_outfit[0], outfit_index, source_outfit[0], source_outfit[1]) for outfit_index in range(len(self.sim_outfits.get_outfits_in_category(source_outfit[0]))))

    def load_outfits_from_cas_proto_and_send_to_client(self):
        outfits_proto = DistributorOps_pb2.SetSimOutfits()
        outfits_proto.ParseFromString(self._base.outfits)
        self._outfits.load_sim_outfits_from_cas_proto(outfits_proto)
        self.resend_outfits()

    def serialize_and_set_base_outfits(self):
        outfit_proto = DistributorOps_pb2.SetSimOutfits()
        for outfits in self._outfits:
            outfit_message = outfit_proto.outfits.add()
            outfit_message.outfit_id = outfits['outfit_id']
            outfit_message.sim_id = self.sim_id
            outfit_message.version = 0
            outfit_message.type = outfits['type']
            outfit_message.part_ids.extend(outfits['parts'])
            outfit_message.body_types.extend(outfits['body_types'])
            outfit_message.outfit_flags = outfits['outfit_flags']
            outfit_message.match_hair_style = outfits['match_hair_style']
        self._base.outfits = outfit_proto.SerializeToString()

    def get_outfits(self):
        return self.sim_outfits

    @property
    def sim_outfits(self):
        return self._outfits

    @distributor.fields.Field(op=distributor.ops.SetSimOutfits)
    def _client_outfits(self):
        override = self.appearance_tracker.appearance_override_sim_info
        if override is not None:
            return override._outfits
        return self._outfits

    resend_outfits = _client_outfits.get_resend()

    @distributor.fields.Field(op=distributor.ops.PreloadSimOutfit)
    def preload_outfit_list(self):
        return tuple(self._preload_outfit_list)

    resend_preload_outfit_list = preload_outfit_list.get_resend()

    def add_preload_outfit(self, outfit):
        self._preload_outfit_list.append(outfit)
        self.resend_preload_outfit_list()
        self.on_preload_outfits_changed()

    def set_preload_outfits(self, outfit_list):
        self._preload_outfit_list = list(outfit_list)
        self.resend_preload_outfit_list()
        self.on_preload_outfits_changed()

    @distributor.fields.Field(op=distributor.ops.ChangeSimOutfit, priority=distributor.fields.Field.Priority.LOW)
    def _current_outfit(self):
        return self._base.outfit_type_and_index

    resend_current_outfit = _current_outfit.get_resend()

    @_current_outfit.setter
    def _current_outfit(self, value):
        self._set_current_outfit_without_distribution(value)

    def _set_current_outfit_without_distribution(self, value):
        self._base.outfit_type_and_index = value
        self.on_outfit_changed(value)

    def can_switch_to_outfit(self, outfit_category_and_index) -> bool:
        if outfit_category_and_index is None:
            return False
        if self._outfits.situation_outfit_dirty and outfit_category_and_index[0] == OutfitCategory.SITUATION:
            return True
        if self._current_outfit == outfit_category_and_index:
            return False
        return True

    def get_current_outfit(self):
        return self._current_outfit

    def get_previous_outfit(self):
        return self._previous_outfit

    def get_part_ids_for_current_outfit(self):
        current_outfit = self.get_current_outfit()
        return self.get_part_ids_for_outfit(*current_outfit)

    def get_part_ids_for_outfit(self, outfit_category, outfit_index):
        return self._outfits.get_parts_ids_for_categry_and_index(outfit_category, outfit_index)

    def set_current_outfit(self, outfit_category_and_index) -> bool:
        if self._outfits.situation_outfit_dirty and self._current_outfit == outfit_category_and_index and outfit_category_and_index[0] != OutfitCategory.SITUATION:
            return False
        self.set_previous_outfit()
        self._current_outfit = outfit_category_and_index
        if outfit_category_and_index[0] == OutfitCategory.SITUATION:
            self._outfits.situation_outfit_dirty = False
        return True

    def set_previous_outfit(self, outfit=DEFAULT):
        previous_outfit = self._current_outfit if outfit is DEFAULT else outfit
        self._previous_outfit = previous_outfit

    @property
    def is_male(self):
        return Gender(self._base.gender) == Gender.MALE

    @property
    def is_female(self):
        return Gender(self._base.gender) == Gender.FEMALE

    @distributor.fields.Field(op=distributor.ops.SetFirstName)
    def first_name(self):
        return self._base.first_name

    @first_name.setter
    def first_name(self, value):
        if self._base.first_name != value:
            self._base.first_name = value

    @distributor.fields.Field(op=distributor.ops.SetLastName)
    def last_name(self):
        return self._base.last_name

    @last_name.setter
    def last_name(self, value):
        if self._base.last_name != value:
            self._base.last_name = value

    @property
    def first_name_key(self):
        return self._base.first_name_key

    @first_name_key.setter
    def first_name_key(self, value):
        if self._base.first_name_key != value:
            self._base.first_name_key = value

    @property
    def last_name_key(self):
        return self._base.last_name_key

    @last_name_key.setter
    def last_name_key(self, value):
        if self._base.last_name_key != value:
            self._base.last_name_key = value

    @distributor.fields.Field(op=distributor.ops.SetFullNameKey)
    def full_name_key(self):
        return self._base.full_name_key

    @full_name_key.setter
    def full_name_key(self, value):
        if self._base.full_name_key != value:
            self._base.full_name_key = value

    @property
    def full_name(self):
        return ''

    @distributor.fields.Field(op=distributor.ops.SetGender)
    def gender(self):
        return Gender(self._base.gender)

    @gender.setter
    def gender(self, value):
        self._base.gender = Gender(value)

    @distributor.fields.Field(op=distributor.ops.SetAge)
    def age(self):
        return Age(self._base.age)

    @age.setter
    def age(self, value):
        self._base.age = value

    resend_age = age.get_resend()

    def apply_age(self, age):
        self._base.update_for_age(age)
        self.load_outfits_from_cas_proto_and_send_to_client()

    @property
    def is_baby(self):
        return self._base.age == Age.BABY

    @distributor.fields.Field(op=distributor.ops.SetSkinTone, priority=distributor.fields.Field.Priority.HIGH)
    def skin_tone(self):
        override = self.appearance_tracker.appearance_override_sim_info
        if override is not None:
            return override._base.skin_tone
        return self._base.skin_tone

    _resend_skin_tone = skin_tone.get_resend()

    def resend_skin_tone(self, *args, **kwargs):
        self._resend_skin_tone(*args, **kwargs)
        if self.is_baby:
            self.resend_baby_skin_tone(*args, **kwargs)

    @skin_tone.setter
    def skin_tone(self, value):
        self._base.skin_tone = value
        if self.is_baby:
            self.resend_baby_skin_tone()

    @distributor.fields.Field(op=distributor.ops.SetBabySkinTone)
    def baby_skin_tone(self):
        return BabyTuning.get_baby_skin_tone_enum(self)

    resend_baby_skin_tone = baby_skin_tone.get_resend()

    @distributor.fields.Field(op=distributor.ops.SetVoicePitch)
    def voice_pitch(self):
        return self._base.voice_pitch

    resend_voice_pitch = voice_pitch.get_resend()

    @voice_pitch.setter
    def voice_pitch(self, value):
        self._base.voice_pitch = value

    @distributor.fields.Field(op=distributor.ops.SetVoiceActor)
    def voice_actor(self):
        return self._base.voice_actor

    resend_voice_actor = voice_actor.get_resend()

    @voice_actor.setter
    def voice_actor(self, value):
        self._base.voice_actor = value

    @distributor.fields.Field(op=distributor.ops.SetVoiceEffect)
    def voice_effect(self):
        return self._base.voice_effect

    @voice_effect.setter
    def voice_effect(self, value):
        if value is None:
            value = 0
        self._base.voice_effect = value

    resend_voice_effect = voice_effect.get_resend()

    @distributor.fields.Field(op=distributor.ops.SetPhysique)
    def physique(self):
        return self._base.physique

    resend_physique = physique.get_resend()

    @physique.setter
    def physique(self, value):
        self._base.physique = value

    @distributor.fields.Field(op=distributor.ops.SetFacialAttributes)
    def facial_attributes(self):
        return self._base.facial_attributes

    @facial_attributes.setter
    def facial_attributes(self, value):
        self._base.facial_attributes = value

    resend_facial_attributes = facial_attributes.get_resend()

    @distributor.fields.Field(op=distributor.ops.SetGeneticData)
    def genetic_data(self):
        return self._base.genetic_data

    @genetic_data.setter
    def genetic_data(self, value):
        self._base.genetic_data = value

    resend_genetic_data = genetic_data.get_resend()

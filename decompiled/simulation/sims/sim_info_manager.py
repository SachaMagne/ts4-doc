import itertools
from audio.primitive import play_tunable_audio
from date_and_time import create_time_span
from interactions.si_restore import SuperInteractionRestorer
from objects import ALL_HIDDEN_REASONS, HiddenReasonFlag
from objects.object_manager import DistributableObjectManager
from sims.genealogy_tracker import genealogy_caching
from sims.outfits.outfit_enums import OutfitCategory
from sims.sim_info_types import SimZoneSpinUpAction
from sims4.callback_utils import CallableList
from story_progression import StoryProgressionFlags
from story_progression.actions import StoryProgressionActionMaxPopulation
from world.travel_tuning import TravelTuning
import alarms
import caches
import interactions.utils.routing
import services
import sims.sim_info
import sims.sim_info_types
import sims4.log
import world
logger = sims4.log.Logger('SimInfoManager', default_owner='manus')
relationship_setup_logger = sims4.log.Logger('DefaultRelSetup', default_owner='manus')
FIREMETER_FREQUENCY = 30
FIREMEMTER_SIM_INFO_CAP = 250

class SimInfoManager(DistributableObjectManager):
    __qualname__ = 'SimInfoManager'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._sim_infos_saved_in_zone = []
        self._sim_infos_saved_in_open_street = []
        self._sims_traveled_to_zone = []
        self._sim_infos_injected_into_zone = []
        self._sim_info_to_spin_up_action = {}
        self._startup_time = None
        self._sim_ids_at_work = set()
        self.on_sim_info_removed = CallableList()
        self._firemeter = None

    def flush_to_client_on_teardown(self):
        for sim_info in self.objects:
            sim_info.flush_to_client_on_teardown()
        if self._firemeter is not None:
            alarms.cancel_alarm(self._firemeter)
            self._firemeter = None

    def add_sim_info_if_not_in_manager(self, sim_info):
        if sim_info.id in self._objects:
            pass
        else:
            self.add(sim_info)

    def save(self, zone_data=None, open_street_data=None, **kwargs):
        owning_household = services.current_zone().get_active_lot_owner_household()
        situation_manager = services.get_zone_situation_manager()
        current_zone_id = services.current_zone_id()
        for sim_info in self.get_all():
            while sim_info.account_id is not None:
                sim = sim_info.get_sim_instance(allow_hidden_flags=ALL_HIDDEN_REASONS)
                if sim is not None:
                    if sim.is_selectable or owning_household is not None and sim_info in owning_household or sim_info.is_renting_zone(current_zone_id):
                        if sim.is_on_active_lot() or sim.has_hidden_flags(HiddenReasonFlag.RABBIT_HOLE):
                            sim_info._serialization_option = sims.sim_info_types.SimSerializationOption.LOT
                        else:
                            sim_info._serialization_option = sims.sim_info_types.SimSerializationOption.OPEN_STREETS
                            sim_info._serialization_option = situation_manager.get_sim_serialization_option(sim)
                    else:
                        sim_info._serialization_option = situation_manager.get_sim_serialization_option(sim)
                sim_info.save_sim()

    def on_all_households_and_sim_infos_loaded(self, client):
        for sim_info in tuple(self.values()):
            sim_info.on_all_households_and_sim_infos_loaded()
            zone = services.current_zone()
            if sim_info.sim_id not in self._sims_traveled_to_zone:
                if sim_info.zone_id == zone.id:
                    if sim_info.serialization_option == sims.sim_info_types.SimSerializationOption.LOT:
                        self._sim_infos_saved_in_zone.append(sim_info)
                    elif sim_info.serialization_option == sims.sim_info_types.SimSerializationOption.UNDECLARED:
                        if not sim_info.is_baby or sim_info.lives_here:
                            self._sim_infos_injected_into_zone.append(sim_info)
                self._sim_infos_saved_in_open_street.append(sim_info)
            else:
                sim_info._serialization_option = sims.sim_info_types.SimSerializationOption.UNDECLARED

    def add_sims_to_zone(self, sim_list):
        self._sims_traveled_to_zone.extend(sim_list)

    def on_spawn_sims_for_zone_spin_up(self, client):
        traveled_sim_infos = []
        for sim_id in tuple(self._sims_traveled_to_zone):
            if sim_id == 0:
                self._sims_traveled_to_zone.remove(sim_id)
            sim_info = self.get(sim_id)
            if sim_info is None:
                logger.error('sim id {} for traveling did not spawn because sim info does not exist.', sim_id, owner='msantander')
            traveled_sim_infos.append(sim_info)
        lot_tuning = world.lot_tuning.LotTuningMaps.get_lot_tuning()
        if lot_tuning is not None and lot_tuning.audio_sting is not None:
            sting = lot_tuning.audio_sting(None)
            sting.start()
        elif traveled_sim_infos:
            play_tunable_audio(TravelTuning.TRAVEL_SUCCESS_AUDIO_STING)
        else:
            play_tunable_audio(TravelTuning.NEW_GAME_AUDIO_STING)
        services.current_zone().venue_service.process_traveled_and_persisted_and_resident_sims_during_zone_spin_up(traveled_sim_infos, self._sim_infos_saved_in_zone, self._sim_infos_saved_in_open_street, self._sim_infos_injected_into_zone)

    def on_spawn_sim_for_zone_spin_up_completed(self, client):
        for sim_info in self.values():
            if sim_info.is_instanced(allow_hidden_flags=ALL_HIDDEN_REASONS) or sim_info.is_selectable:
                sim_info.commodity_tracker.start_low_level_simulation()
            while sim_info.is_instanced(allow_hidden_flags=ALL_HIDDEN_REASONS) and not sim_info.is_npc:
                sim_info.aspiration_tracker.refresh_progress(sim_info)
        client.refresh_achievement_data()
        services.get_event_manager().unregister_unused_handlers()
        for sim_info in client.selectable_sims:
            while sim_info.is_instanced(allow_hidden_flags=ALL_HIDDEN_REASONS):
                sim_info.aspiration_tracker.initialize_aspiration()
                sim_info.aspiration_tracker.set_update_alarm()
                sim_info.career_tracker.activate_career_aspirations()
        self.set_default_genealogy()
        for sim_info in self.values():
            sim_info.relationship_tracker.send_relationship_info()
        for sim_info in itertools.chain(self._sim_infos_saved_in_zone, self._sim_infos_saved_in_open_street, self._sim_infos_injected_into_zone):
            while not sim_info.is_baby and sim_info.get_sim_instance(allow_hidden_flags=ALL_HIDDEN_REASONS) is None:
                if not sim_info.lives_here:
                    sim_info.inject_into_inactive_zone(sim_info.vacation_or_home_zone_id)
                else:
                    sim_info.inject_into_inactive_zone(0)

    def set_default_genealogy(self):

        def get_spouse(sim_info):
            spouse = None
            spouse_id = sim_info.spouse_sim_id
            if spouse_id is not None:
                spouse = self.get(spouse_id)
            return spouse

        depth = 3
        with genealogy_caching():
            for sim_info in self.values():
                extended_family = set()
                candidates = set([sim_info])
                spouse = get_spouse(sim_info)
                if spouse is not None:
                    candidates.add(spouse)
                    extended_family.add(spouse)
                for _ in range(depth):
                    new_candidates = set()
                    for _id in itertools.chain.from_iterable(x.genealogy.get_immediate_family_sim_ids_gen() for x in candidates):
                        family_member = self.get(_id)
                        while family_member is not None and family_member not in extended_family:
                            new_candidates.add(family_member)
                            spouse = get_spouse(family_member)
                            if spouse is not None and family_member not in extended_family:
                                new_candidates.add(spouse)
                    candidates = new_candidates
                    extended_family.update(candidates)
                extended_family -= set([sim_info])
                for family_member in extended_family:
                    sim_info.add_family_link(family_member)
                    family_member.add_family_link(sim_info)
        relationship_setup_logger.info('set_default_genealogy updated genealogy links for {} sim_infos.', len(self.values()))

    def get_traveled_to_zone_sim_infos(self):
        return [self.get(sim_id) for sim_id in self._sims_traveled_to_zone]

    def get_sim_infos_saved_in_zone(self):
        return list(self._sim_infos_saved_in_zone)

    def get_sim_infos_saved_in_open_streets(self):
        return list(self._sim_infos_saved_in_open_street)

    def instanced_sims_gen(self, allow_hidden_flags=0):
        for info in self.get_all():
            sim = info.get_sim_instance(allow_hidden_flags=allow_hidden_flags)
            while sim is not None:
                yield sim

    def instanced_sims_on_active_lot_gen(self, allow_hidden_flags=0):
        for sim in self.instanced_sims_gen(allow_hidden_flags=allow_hidden_flags):
            while sim.is_on_active_lot():
                yield sim

    def instanced_sim_info_including_baby_gen(self, allow_hidden_flags=0):
        object_manager = services.object_manager()
        for sim_info in self.get_all():
            if sim_info.is_baby:
                sim_or_baby = object_manager.get(sim_info.id)
            else:
                sim_or_baby = sim_info.get_sim_instance(allow_hidden_flags=allow_hidden_flags)
            while sim_or_baby is not None:
                yield sim_info

    def get_player_npc_sim_count(self):
        npc = 0
        player = 0
        for sim in self.instanced_sims_gen(allow_hidden_flags=ALL_HIDDEN_REASONS):
            if sim.is_selectable:
                player += 1
            else:
                while sim.sim_info.is_npc:
                    npc += 1
        return (player, npc)

    def are_npc_sims_in_open_streets(self):
        return any(s.is_npc and not s.is_on_active_lot() for s in self.instanced_sims_gen(allow_hidden_flags=ALL_HIDDEN_REASONS))

    def get_sim_info_by_name(self, first_name, last_name):
        first_name = first_name.lower()
        last_name = last_name.lower()
        for info in self.get_all():
            while info.first_name.lower() == first_name and info.last_name.lower() == last_name:
                return info

    def auto_satisfy_sim_motives(self):
        for sim in self.instanced_sims_gen():
            statistics = list(sim.commodities_gen())
            for statistic in statistics:
                if statistic.is_skill:
                    pass
                statistic.set_to_auto_satisfy_value()

    def handle_event(self, sim_info, event, resolver):
        self._sim_started_startup_interaction(sim_info, event, resolver)

    def _run_preroll_autonomy(self):
        used_target_list = []
        for sim_info in self.get_sims_for_spin_up_action(SimZoneSpinUpAction.PREROLL):
            sim = sim_info.get_sim_instance()
            if sim is None:
                pass
            caches.clear_all_caches()
            sim.set_allow_route_instantly_when_hitting_marks(True)
            (interaction_started, interaction_target) = sim.run_preroll_autonomy(used_target_list)
            if interaction_started:
                logger.debug('sim: {} started interaction:{} as part of preroll autonomy.', sim, interaction_started)
                used_target_list.append(interaction_target)
            else:
                logger.debug('sim: {} failed to choose interaction as part of preroll autonomy.', sim)

    def _run_startup_interactions(self, create_startup_interactions_function):
        try:
            create_startup_interactions_function()
        except Exception as e:
            logger.exception('Exception raised while trying to startup interactions.', exc=e)

    def schedule_sim_spin_up_action(self, sim_info, action):
        if self._sim_info_to_spin_up_action is None:
            return
        if sim_info in self._sim_info_to_spin_up_action:
            logger.error('Setting spin up action twice for Sim:{} first:{} second:{}', sim_info, self._sim_info_to_spin_up_action[sim_info], action)
        self._sim_info_to_spin_up_action[sim_info] = action

    def get_sims_for_spin_up_action(self, action):
        if self._sim_info_to_spin_up_action is None:
            return
        results = []
        for (sim_info, scheduled_action) in self._sim_info_to_spin_up_action.items():
            while scheduled_action == action:
                results.append(sim_info)
        return results

    def restore_sim_si_state(self):
        super_interaction_restorer = SuperInteractionRestorer()
        super_interaction_restorer.restore_sim_si_state()

    def verify_travel_sims_outfits(self):
        for traveled_sim_id in self._sims_traveled_to_zone:
            sim_info = self.get(traveled_sim_id)
            while sim_info is not None:
                if sim_info.get_current_outfit()[0] == OutfitCategory.BATHING:
                    sim_info.set_current_outfit((OutfitCategory.EVERYDAY, 0))

    def run_preroll_autonomy(self):
        self._run_startup_interactions(self._run_preroll_autonomy)
        self.verify_travel_sims_outfits()

    def push_sims_to_go_home(self):
        go_home_affordance = sims.sim_info.SimInfo.GO_HOME_FROM_OPEN_STREET
        if go_home_affordance is None:
            return
        for sim_info in self.get_sims_for_spin_up_action(SimZoneSpinUpAction.PUSH_GO_HOME):
            sim = sim_info.get_sim_instance()
            while sim is not None:
                tolerance = sim.get_off_lot_autonomy_rule().tolerance
                if not sim.is_on_active_lot(tolerance=tolerance):
                    context = interactions.context.InteractionContext(sim, interactions.context.InteractionContext.SOURCE_SCRIPT, interactions.priority.Priority.High)
                    if sim.push_super_affordance(go_home_affordance, None, context):
                        logger.debug('sim: {} pushed to go home.', sim, owner='sscholl')
                    else:
                        logger.warn('Failed to push sim to go home from open street: {}', sim_info, owner='msantander')

    def _firemeter_callback(self, _):
        sim_info_count = len(self)
        if sim_info_count < FIREMEMTER_SIM_INFO_CAP:
            return
        logger.error('FireMeter: We have {} sim_infos in the save file! Purge begins.', sim_info_count)
        self.trigger_firemeter()

    def trigger_firemeter(self):
        action = StoryProgressionActionMaxPopulation.TunableFactory().default
        action.process_action(StoryProgressionFlags.SIM_INFO_FIREMETER)

    def set_aging_enabled_on_all_sims(self, is_aging_enabled_for_sim_info_fn):
        for sim_info in self.objects:
            sim_info.set_aging_enabled(is_aging_enabled_for_sim_info_fn(sim_info))

    def set_aging_speed_on_all_sims(self, speed):
        for sim_info in self.objects:
            sim_info.set_aging_speed(speed)

    def set_sim_at_work(self, sim_info):
        self._sim_ids_at_work.add(sim_info.id)

    def on_loading_screen_animation_finished(self):
        for sim_info in self.objects:
            sim_info.on_loading_screen_animation_finished()
        daycare_service = services.daycare_service()
        if daycare_service is not None:
            daycare_service.on_loading_screen_animation_finished()
        self._sims_traveled_to_zone.clear()
        self._sim_infos_saved_in_open_street.clear()
        self._sim_infos_saved_in_zone.clear()
        self._sim_ids_at_work.clear()
        self._sim_infos_injected_into_zone.clear()
        self._sim_info_to_spin_up_action = None
        self._firemeter = alarms.add_alarm(self, create_time_span(minutes=FIREMETER_FREQUENCY), self._firemeter_callback, repeating=True, use_sleep_time=False)

    def remove_permanently(self, sim_info):
        sim_info.relationship_tracker.destroy_all_relationships()
        self.remove(sim_info)
        self.on_sim_info_removed(sim_info)

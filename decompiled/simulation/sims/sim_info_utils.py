import functools
import inspect
import services

def sim_info_auto_finder(fn):
    is_generator = inspect.isgeneratorfunction(fn)
    if is_generator:

        @functools.wraps(fn)
        def wrapped(*args, **kwargs):
            sim_info_manager = services.sim_info_manager()
            for sim_id in fn(*args, **kwargs):
                sim_info = sim_info_manager.get(sim_id)
                while sim_info is not None:
                    yield sim_info

    else:

        @functools.wraps(fn)
        def wrapped(*args, **kwargs):
            sim_info_manager = services.sim_info_manager()
            sim_infos = []
            for sim_id in fn(*args, **kwargs):
                sim_info = sim_info_manager.get(sim_id)
                while sim_info is not None:
                    sim_infos.append(sim_info)
            return tuple(sim_infos)

    return wrapped

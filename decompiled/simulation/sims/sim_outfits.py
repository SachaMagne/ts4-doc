import collections
import random
import weakref
from protocolbuffers import Outfits_pb2, S4Common_pb2
from distributor.rollback import ProtocolBufferRollback
from element_utils import build_critical_section
from event_testing.tests import TunableTestSet
from interactions.utils.animation import flush_all_animations, create_run_animation
from objects import ALL_HIDDEN_REASONS
from sims.outfits.outfit_enums import OutfitCategory
from sims.outfits.outfit_utils import populate_outfit_proto
from sims4.localization import TunableLocalizedStringFactory
from sims4.tuning.dynamic_enum import DynamicEnum
from sims4.tuning.tunable import Tunable, TunableResourceKey, TunableMapping, TunableTuple, TunableList, TunableEnumEntry, OptionalTunable
from singletons import DEFAULT
import animation
import animation.arb
import animation.asm
import element_utils
import services
import sims4.log
logger = sims4.log.Logger('SimOutfits')
OUTFITS_TO_POPULATE_ON_LOAD = (OutfitCategory.SWIMWEAR,)

class OutfitChangeReason(DynamicEnum):
    __qualname__ = 'OutfitChangeReason'
    Invalid = -1
    PreviousClothing = 0
    DefaultOutfit = 1
    RandomOutfit = 2
    ExitBedNPC = 3

class DefaultOutfitPriority(DynamicEnum):
    __qualname__ = 'DefaultOutfitPriority'
    NoPriority = 0

class ForcedOutfitChanges:
    __qualname__ = 'ForcedOutfitChanges'
    INAPPROPRIATE_STREETWEAR = TunableList(description='\n        A list of outfit categories inappropriate for wearing on open streets.\n        If the Sim is in one of these categories when they first decided to go\n        off-lot, they will switch out of it beforehand.\n        ', tunable=TunableEnumEntry(tunable_type=OutfitCategory, default=OutfitCategory.EVERYDAY))

class ClothingChangeTunables:
    __qualname__ = 'ClothingChangeTunables'
    DEFAULT_ACTOR = 'x'
    clothing_change_state = Tunable(str, 'ClothesChange', description='State in the clothing_change_asm that runs clothing_change animation.')
    clothing_change_asm = TunableResourceKey(None, resource_types=[sims4.resources.Types.STATEMACHINE], description='ASM used for the clothing change.')
    clothing_reasons_to_outfits = TunableMapping(key_type=OutfitChangeReason, value_type=TunableList(TunableTuple(tests=TunableTestSet(), outfit_category=TunableEnumEntry(OutfitCategory, OutfitCategory.EVERYDAY, description='On Reason, Change Directly to Outfit')), description='List of tunable mappings of change reason to outfit change.'), key_name='OutfitChangeReason', value_name='TunableMappings')

OutfitPriority = collections.namedtuple('OutfitPriority', ['change_reason', 'priority', 'interaction_ref'])

class SimOutfits:
    __qualname__ = 'SimOutfits'
    OUTFIT_CATEGORY_TUNING = TunableMapping(key_type=TunableEnumEntry(OutfitCategory, OutfitCategory.EVERYDAY, description='The outfit category enum'), value_type=TunableTuple(localized_category=TunableLocalizedStringFactory(description='Localized name of the outfit category.', allow_none=True), save_outfit_category=OptionalTunable(description="\n                                                                        If set to 'save_this_category', a Sim saved while wearing this \n                                                                        outfit category will change back into this outfit category on load.\n                                                                        \n                                                                        EX: you're tuning Everyday outfit, which is set as save_this_category,\n                                                                        meaning a sim wearing everyday will still be wearing everyday on load.\n                                                                        \n                                                                        Otherwise, you can set to save_as_different_category, which allows you\n                                                                        to specific another outfit category for the sim to be saved in\n                                                                        instead of this category.\n                                                                        \n                                                                        EX: if tuning Bathing category, if the sim is in the bathing category, set this to Everday so that when\n                                                                        the sim loads back up, the sim will be in Everyday wear instead of naked.\n                                                                        ", tunable=TunableEnumEntry(OutfitCategory, OutfitCategory.EVERYDAY, description='The outfit category to save as instead of this category.'), disabled_name='save_this_category', enabled_name='save_as_different_category')))
    CATEGORIES_EXEMPT_FROM_RANDOMIZATION = (OutfitCategory.BATHING,)

    def __init__(self, sim_info):
        self._sim_info = sim_info
        self._initialize_outfits()
        self._default_outfit_priorities = []
        self._randomize_daily = {}
        self._last_randomize = {}
        self._daily_defaults = {}
        self.outfit_censor_handle = None
        self.situation_outfit_dirty = True
        for category in OutfitCategory:
            self._randomize_daily[category] = True
            self._last_randomize[category] = None

    def get_sim_info(self):
        return self._sim_info

    def get_sim_instance(self):
        return self._sim_info.get_sim_instance(allow_hidden_flags=ALL_HIDDEN_REASONS)

    def get_change(self, interaction, change_reason, **kwargs):
        if change_reason is not None:
            return build_critical_section(self.get_clothing_change(interaction, change_reason, **kwargs), flush_all_animations)

    def _generate_daily_outfit(self, category):
        current_time = services.time_service().sim_now
        existing_default = category in self._daily_defaults
        last_randomize_time = self._last_randomize[category]
        if not existing_default or current_time.absolute_days() - last_randomize_time.absolute_days() >= 1 or current_time.day() != last_randomize_time.day():
            index = 0
            number_of_outfits = len(self._outfits[category])
            if number_of_outfits > 1:
                if existing_default:
                    index = random.randrange(number_of_outfits - 1)
                    exclusion = self._daily_defaults[category]
                    index += 1
                else:
                    index = random.randrange(number_of_outfits)
            self._daily_defaults[category] = index
            self._last_randomize[category] = current_time
        return (category, self._daily_defaults[category])

    def __iter__(self):
        for outfit_category in self._outfits.values():
            for outfit_data in outfit_category:
                yield outfit_data

    def _initialize_outfits(self):
        self._outfits = {}
        for category in OutfitCategory:
            self._outfits[category] = []

    def get_clothing_change(self, interaction, outfit_change_reason, do_spin=True):
        outfit_category_and_index = self.get_outfit_for_clothing_change(interaction, outfit_change_reason)
        return self.get_change_outfit_element(outfit_category_and_index, do_spin=do_spin)

    def get_change_outfit_element(self, outfit_category_and_index, do_spin=True):

        def change_outfit(timeline):
            arb = animation.arb.Arb()
            self._try_set_current_outfit(outfit_category_and_index, do_spin=do_spin, arb=arb)
            if not arb.empty:
                clothing_element = create_run_animation(arb)
                yield element_utils.run_child(timeline, clothing_element)

        return change_outfit

    def _try_set_current_outfit(self, outfit_category_and_index, do_spin=False, arb=None):
        sim = self.get_sim_instance()
        if sim is None:
            do_spin = False
        if self._sim_info.can_switch_to_outfit(outfit_category_and_index):
            if do_spin:
                did_change = False

                def set_ending(*_, **__):
                    nonlocal did_change
                    if not did_change:
                        self._sim_info.set_current_outfit(outfit_category_and_index)
                        did_change = True

                arb.register_event_handler(set_ending, handler_id=100)
                clothing_context = animation.get_throwaway_animation_context()
                clothing_change_asm = animation.asm.create_asm(ClothingChangeTunables.clothing_change_asm, context=clothing_context)
                result = sim.posture.setup_asm_interaction(clothing_change_asm, sim, None, ClothingChangeTunables.DEFAULT_ACTOR, None)
                if not result:
                    logger.error('Could not setup asm for Clothing Change. {}', result)
                clothing_change_asm.request(ClothingChangeTunables.clothing_change_state, arb)
            else:
                self._sim_info.set_current_outfit(outfit_category_and_index)

    def exit_change_verification(self, outfit_category_and_index):
        if self._sim_info._current_outfit is not outfit_category_and_index:
            self._sim_info.set_current_outfit(outfit_category_and_index)

    def get_outfit_for_clothing_change(self, interaction, reason, resolver=None):
        for trait in self._sim_info.get_traits():
            reason = trait.get_outfit_change_reason(reason)
        if reason == OutfitChangeReason.Invalid:
            return self._sim_info._current_outfit
        if reason == OutfitChangeReason.DefaultOutfit:
            return self.get_default_outfit(interaction=interaction, resolver=resolver)
        if reason == OutfitChangeReason.PreviousClothing:
            return self._sim_info._previous_outfit
        if reason == OutfitChangeReason.RandomOutfit:
            return self.get_random_outfit(self._outfits.keys())
        if reason == OutfitChangeReason.ExitBedNPC:
            if self._sim_info.is_npc:
                return self._sim_info._previous_outfit
            return
        elif reason in ClothingChangeTunables.clothing_reasons_to_outfits:
            test_group_and_outfit_list = ClothingChangeTunables.clothing_reasons_to_outfits[reason]
            for test_group_and_outfit in test_group_and_outfit_list:
                category = test_group_and_outfit.outfit_category
                if category == OutfitCategory.BATHING and not self._outfits[OutfitCategory.BATHING]:
                    self._sim_info.generate_outfit(OutfitCategory.BATHING)
                if not test_group_and_outfit.tests:
                    if test_group_and_outfit.outfit_category == OutfitCategory.CURRENT_OUTFIT:
                        return self._sim_info._current_outfit
                    if self._randomize_daily[category]:
                        return self._generate_daily_outfit(category)
                    return (test_group_and_outfit.outfit_category, 0)
                if resolver is None:
                    resolver = interaction.get_resolver()
                while test_group_and_outfit.tests.run_tests(resolver):
                    if test_group_and_outfit.outfit_category == OutfitCategory.CURRENT_OUTFIT:
                        return self._sim_info._current_outfit
                    if self._randomize_daily[category]:
                        return self._generate_daily_outfit(category)
                    return (test_group_and_outfit.outfit_category, 0)
        return (OutfitCategory.EVERYDAY, 0)

    def get_default_outfit(self, interaction=None, resolver=None):
        default_outfit = OutfitPriority(None, 0, None)
        for outfit_priority in self._default_outfit_priorities:
            while outfit_priority.priority > default_outfit.priority:
                default_outfit = outfit_priority
        if interaction is not None or resolver is not None:
            return self.get_outfit_for_clothing_change(interaction, default_outfit.change_reason, resolver=resolver)
        if default_outfit.interaction_ref() is not None:
            return self.get_outfit_for_clothing_change(default_outfit.interaction_ref(), default_outfit.change_reason)
        return self._sim_info._current_outfit

    def get_random_outfit(self, categories, exclusion=None):
        outfits_list = []
        for category in categories:
            if category in self.CATEGORIES_EXEMPT_FROM_RANDOMIZATION:
                pass
            for index in range(len(self._outfits[category])):
                category_and_index = (category, index)
                while category_and_index != exclusion:
                    outfits_list.append(category_and_index)
        if not outfits_list:
            return (OutfitCategory.EVERYDAY, 0)
        return random.choice(outfits_list)

    def add_default_outfit_priority(self, interaction, outfit_change_reason, priority):
        outfit_priority = OutfitPriority(outfit_change_reason, priority, weakref.ref(interaction) if interaction is not None else None)
        self._default_outfit_priorities.append(outfit_priority)
        return id(outfit_priority)

    def remove_default_outfit_priority(self, outfit_priority_id):
        for (index, value) in enumerate(self._default_outfit_priorities):
            while id(value) == outfit_priority_id:
                self._default_outfit_priorities.pop(index)
                break

    def add_outfit(self, outfit_category, outfit_data):
        self._outfits[outfit_category].append(outfit_data)
        return (outfit_category, len(self._outfits[outfit_category]) - 1)

    def get_outfit(self, outfit_category, outfit_index):
        outfits_for_category = self._outfits.get(outfit_category)
        if outfits_for_category is not None and len(outfits_for_category) > outfit_index:
            return outfits_for_category[outfit_index]

    def get_all_outfits(self):
        return self._outfits.items()

    def get_outfits_in_category(self, outfit_category):
        return self._outfits.get(outfit_category)

    def has_outfit(self, category_and_index):
        (category, index) = category_and_index
        outfits_in_category = self._outfits.get(category)
        if outfits_in_category is None:
            return False
        if index >= len(outfits_in_category):
            return False
        return True

    def remove_outfit(self, outfit_category, outfit_index=DEFAULT):
        outfits_for_category = self._outfits.get(outfit_category)
        if outfits_for_category is not None:
            outfit_index = len(outfits_for_category) - 1 if outfit_index is DEFAULT else outfit_index
            del outfits_for_category[outfit_index]

    def remove_outfits_in_category(self, outfit_category):
        self._outfits[outfit_category] = []

    def set_outfit_flags(self, outfit_category, outfit_index, outfit_flags):
        outfit = self.get_outfit(outfit_category, outfit_index)
        if outfit is not None:
            outfit['outfit_flags'] = outfit_flags
            self._sim_info.serialize_and_set_base_outfits()

    def save_sim_outfits(self):
        outfit_list_msg = Outfits_pb2.OutfitList()
        for outfit_category in OutfitCategory:
            outfits_in_category = self.get_outfits_in_category(outfit_category)
            while outfits_in_category is not None:
                while True:
                    for (outfit_index, outfit_data) in enumerate(outfits_in_category):
                        with ProtocolBufferRollback(outfit_list_msg.outfits) as outfit_msg:
                            populate_outfit_proto(outfit_msg, outfit_category, outfit_index, outfit_data)
        return outfit_list_msg

    def load_sim_outfits_from_persistence_proto(self, sim_id, outfit_list):
        self._initialize_outfits()
        if outfit_list is not None:
            for outfit_data in outfit_list.outfits:
                new_outfit = {}
                new_outfit['sim_id'] = sim_id
                new_outfit['outfit_id'] = outfit_data.outfit_id
                new_outfit['type'] = outfit_data.category
                part_list = S4Common_pb2.IdList()
                part_list.MergeFrom(outfit_data.parts)
                new_outfit['parts'] = part_list.ids
                body_types_list = Outfits_pb2.BodyTypesList()
                body_types_list.MergeFrom(outfit_data.body_types_list)
                new_outfit['body_types'] = body_types_list.body_types
                new_outfit['outfit_flags'] = outfit_data.outfit_flags
                new_outfit['match_hair_style'] = outfit_data.match_hair_style
                outfit_category = OutfitCategory(outfit_data.category)
                self.add_outfit(outfit_category, new_outfit)

    def generate_unpopulated_outfits(self, outfit_list):
        if outfit_list is not None:
            loaded_categories = {OutfitCategory(outfit_data.category) for outfit_data in outfit_list.outfits}
        else:
            loaded_categories = set()
        outfits_generated = False
        for category in OUTFITS_TO_POPULATE_ON_LOAD:
            while category not in loaded_categories:
                self._sim_info.generate_outfit(outfit_category=category, send_outfits_to_client=False)
                outfits_generated = True
        return outfits_generated

    def load_sim_outfits_from_cas_proto(self, outfit_data):
        self._initialize_outfits()
        if outfit_data is not None:
            for outfit in outfit_data.outfits:
                new_outfit = {}
                new_outfit['sim_id'] = self._sim_info.sim_id
                new_outfit['outfit_id'] = outfit.outfit_id
                new_outfit['version'] = outfit.version
                new_outfit['type'] = outfit.type
                new_outfit['parts'] = outfit.part_ids
                new_outfit['body_types'] = outfit.body_types
                new_outfit['outfit_flags'] = outfit.outfit_flags
                new_outfit['match_hair_style'] = outfit.match_hair_style
                self.add_outfit(OutfitCategory(outfit.type), new_outfit)

    def is_wearing_outfit(self, category_and_index):
        if self.situation_outfit_dirty and category_and_index[0] == OutfitCategory.SITUATION:
            return False
        return self._sim_info._current_outfit == category_and_index

    def get_parts_ids_for_categry_and_index(self, category, index):
        outfits_in_category = self.get_outfits_in_category(category)
        if outfits_in_category is not None and index < len(outfits_in_category):
            outfit = outfits_in_category[index]
            return list(outfit['parts'])

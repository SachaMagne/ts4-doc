from sims4.tuning.instances import HashedTunedInstanceMetaclass
from sims4.tuning.tunable import HasTunableReference
from situations.situation_curve import SituationCurve
import services
import sims4.log
logger = sims4.log.Logger('WalkbyTuning')

class WalkbyTuning(HasTunableReference, metaclass=HashedTunedInstanceMetaclass, manager=services.walk_by_manager()):
    __qualname__ = 'WalkbyTuning'
    INSTANCE_TUNABLES = {'walkby_desire_by_day_of_week': SituationCurve.TunableFactory(description='\n            The desire that walk-by Sims are spawned at specific times of the\n            day.\n            ')}

    @classmethod
    def get_desired_sim_count_tunable(cls):
        return cls.walkby_desire_by_day_of_week.get_desired_sim_count_tunable()

    @classmethod
    def get_ambient_walkby_situation(cls, sim_slots_available):
        lot_id = services.active_lot_id()

        def can_start_situation(situation):
            return situation.can_start_walkby(lot_id, sim_slots_available)

        return cls.walkby_desire_by_day_of_week.get_situation(predicate=can_start_situation)

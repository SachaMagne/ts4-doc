import random
from clock import interval_in_sim_minutes, ClockSpeedMode
from event_testing.resolver import DoubleSimResolver
from scheduler import WeeklySchedule, ScheduleEntry
from sims4.localization import TunableLocalizedStringFactory
from sims4.service_manager import Service
from sims4.tuning.tunable import TunableTuple, Tunable, TunableList, TunableReference, TunableSimMinute, TunableRange, TunableFactory, TunableSingletonFactory, OptionalTunable
from situations.situation_guest_list import SituationGuestList, SituationGuestInfo, SituationInvitationPurpose
from tunable_time import TunableTimeOfDay
from ui.ui_dialog import UiDialogOkCancel, UiDialogResponse, ButtonType
from ui.ui_dialog_picker import UiSimPicker, SimPickerRow
import alarms
import services
import sims4.random
import sims4.resources
import telemetry_helper
logger = sims4.log.Logger('NPCHostedSituations', default_owner='jjacobson')
TELEMETRY_GROUP_SITUATIONS = 'SITU'
TELEMETRY_HOOK_SITUATION_INVITED = 'INVI'
TELEMETRY_HOOK_SITUATION_ACCEPTED = 'ACCE'
TELEMETRY_HOOK_SITUATION_REJECTED = 'REJE'
TELEMETRY_SITUATION_TYPE_ID = 'type'
TELEMETRY_GUEST_COUNT = 'gcou'
TELEMETRY_CHOSEN_ZONE = 'czon'
writer = sims4.telemetry.TelemetryWriter(TELEMETRY_GROUP_SITUATIONS)

class NPCHostedSituationDialog(UiDialogOkCancel):
    __qualname__ = 'NPCHostedSituationDialog'
    FACTORY_TUNABLES = {'bring_other_sims': OptionalTunable(description='\n            If enabled then the tuned dialog will appear \n            ', tunable=TunableTuple(picker_dialog=UiSimPicker.TunableFactory(description='\n                    The picker dialog to show when selecting Sims to invite to\n                    the event.\n                    '), travel_with_filter=TunableReference(description='\n                    The sim filter that will be used to select sims to bring to\n                    the situation as well.\n                    ', manager=services.get_instance_manager(sims4.resources.Types.SIM_FILTER)), text=TunableLocalizedStringFactory(description='\n                    The "Yes, with friends" text.\n                    '), situation_job=TunableReference(description='\n                    The situation job that sims who are picked from this picker\n                    will be put into.\n                    ', manager=services.get_instance_manager(sims4.resources.Types.SITUATION_JOB))))}
    BRING_OTHER_SIMS_RESPONSE_ID = 1

    @property
    def responses(self):
        if self.bring_other_sims is not None:
            return (UiDialogResponse(dialog_response_id=ButtonType.DIALOG_RESPONSE_OK, text=self.text_ok, ui_request=UiDialogResponse.UiDialogUiRequest.NO_REQUEST), UiDialogResponse(dialog_response_id=NPCHostedSituationDialog.BRING_OTHER_SIMS_RESPONSE_ID, text=self.bring_other_sims.text, ui_request=UiDialogResponse.UiDialogUiRequest.NO_REQUEST), UiDialogResponse(dialog_response_id=ButtonType.DIALOG_RESPONSE_CANCEL, text=self.text_cancel, ui_request=UiDialogResponse.UiDialogUiRequest.NO_REQUEST))
        return super().responses

class NPCHostedSituationEntry(ScheduleEntry):
    __qualname__ = 'NPCHostedSituationEntry'
    FACTORY_TUNABLES = {'description': '\n            A representation of a single possible moment in which an NPC\n            hosted event could start.\n            ', 'possible_situations': TunableList(description='\n                A list of possible situations that can be chosen as an NPC\n                hosted situation.\n                ', tunable=TunableTuple(situation=TunableReference(description='\n                        The situation that will attempted to be run by the NPC.\n                        ', manager=services.get_instance_manager(sims4.resources.Types.SITUATION), pack_safe=True), weight=Tunable(description='\n                        The weight that this situation will be chosen.\n                        ', tunable_type=float, default=1.0))), 'duration': Tunable(description='\n                If Random Start is checked then this then the actual time\n                that this entry can start will be between the Start Time and\n                Start Time + Randomization Interval.\n                ', display_name='Randomization Interval', tunable_type=float, default=1.0)}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def select_situation(self):
        return sims4.random.weighted_random_item([(possible_situation.weight, possible_situation.situation) for possible_situation in self.possible_situations])

TunableNPCHostedSituationEntry = TunableSingletonFactory.create_auto_factory(NPCHostedSituationEntry)

class NPCHostedSituationSchedule(WeeklySchedule):
    __qualname__ = 'NPCHostedSituationSchedule'
    FACTORY_TUNABLES = {'schedule_entries': TunableList(description='\n                A list of event schedules. Each event is a mapping of days of\n                the week to a start_time and duration.\n                ', tunable=TunableNPCHostedSituationEntry()), 'cooldown': TunableSimMinute(description='\n                The cooldown of the scheduler.  If a situation is started\n                through it then another situation will not start for that\n                amount of time.\n                ', default=10, minimum=1), 'creation_chance': TunableRange(description='\n                Chance that a situation will start at each entry point.\n                ', tunable_type=float, default=0.5, minimum=0.0, maximum=1.0)}

    def __init__(self, schedule_entries, cooldown, creation_chance):
        super().__init__(schedule_entries, start_callback=self._select_and_create_NPC_hosted_situation, schedule_immediate=False, min_alarm_time_span=interval_in_sim_minutes(30))
        self._cooldown = interval_in_sim_minutes(cooldown)
        self._creation_chance = creation_chance

    def get_all_npc_hosted_situations(self):
        possible_situations = set()
        for schedule_entry in self._schedule_entry_tuning:
            for possible_situation_entry in schedule_entry.possible_situations:
                possible_situations.add(possible_situation_entry.situation)
        return possible_situations

    def _select_and_create_NPC_hosted_situation(self, scheduler, alarm_data, callback_data):
        if random.random() > self._creation_chance:
            logger.info('Did not start NPC Hosted Situation because random chance was not met.')
            return
        if services.get_zone_situation_manager().is_user_facing_situation_running():
            logger.info('Did not start NPC Hosted Situation because user facing situation was running.')
            return
        chosen_situation = alarm_data.entry.select_situation()
        if chosen_situation is None:
            logger.info('Did not start NPC Hosted Situation because there was no situation to choose.')
            return
        reason = self.try_and_create_NPC_hosted_situation(chosen_situation)
        if reason is not None:
            logger.info(reason)

    def try_and_create_NPC_hosted_situation(self, chosen_situation):
        if services.get_persistence_service().is_save_locked():
            return 'Could not start situation since the game is save locked.'
        sim_results = chosen_situation.get_npc_hosted_sims()
        if sim_results.player_sim_info is None:
            return sim_results.failure_reason
        chosen_zone_id = None
        if chosen_situation.host_event_at_NPCs_residence:
            chosen_zone_id = sim_results.host_sim_info.vacation_or_home_zone_id
        if not chosen_zone_id and chosen_situation.has_venue_location():
            chosen_zone_id = chosen_situation.get_venue_location()
        if not chosen_zone_id:
            return 'Could not find valid zone id for situation {}'.format(chosen_situation)

        def _create_NPC_hosted_situation(dialog):
            if dialog.response == ButtonType.DIALOG_RESPONSE_CANCEL or dialog.response == ButtonType.DIALOG_RESPONSE_NO_RESPONSE or dialog.response == ButtonType.DIALOG_RESPONSE_CLOSED:
                with telemetry_helper.begin_hook(writer, TELEMETRY_HOOK_SITUATION_REJECTED, sim_info=sim_results.player_sim_info) as hook:
                    hook.write_guid(TELEMETRY_SITUATION_TYPE_ID, chosen_situation.guid64)
                return
            self.add_cooldown(self._cooldown)
            guest_list = SituationGuestList(host_sim_id=sim_results.host_sim_info.id)
            if chosen_situation.NPC_hosted_situation_use_player_sim_as_filter_requester:
                guest_list.filter_requesting_sim_id = sim_results.player_sim_info.id
            guest_list.add_guest_info(SituationGuestInfo.construct_from_purpose(sim_results.player_sim_info.id, chosen_situation.NPC_hosted_situation_player_job, SituationInvitationPurpose.INVITED))
            if chosen_situation.NPC_host_job is not None:
                guest_list.add_guest_info(SituationGuestInfo.construct_from_purpose(sim_results.host_sim_info.id, chosen_situation.NPC_host_job, SituationInvitationPurpose.INVITED))
            if dialog.response == ButtonType.DIALOG_RESPONSE_OK:
                services.get_zone_situation_manager().create_situation(chosen_situation, guest_list=guest_list, zone_id=chosen_zone_id, scoring_enabled=chosen_situation.NPC_hosted_situation_scoring_enabled)
                with telemetry_helper.begin_hook(writer, TELEMETRY_HOOK_SITUATION_ACCEPTED, sim_info=sim_results.player_sim_info) as hook:
                    hook.write_guid(TELEMETRY_SITUATION_TYPE_ID, chosen_situation.guid64)
                return
            bring_other_sims_data = chosen_situation.NPC_hosted_situation_start_message.bring_other_sims
            picker_dialog = bring_other_sims_data.picker_dialog(sim_results.player_sim_info, resolver=DoubleSimResolver(sim_results.player_sim_info, sim_results.host_sim_info))
            blacklist = {sim_info.id for sim_info in sim_results.host_sim_info.household.sim_info_gen()}
            blacklist.add(sim_results.player_sim_info.id)
            results = services.sim_filter_service().submit_filter(bring_other_sims_data.travel_with_filter, callback=None, requesting_sim_info=sim_results.player_sim_info, blacklist_sim_ids=blacklist, allow_yielding=False)
            for result in results:
                picker_dialog.add_row(SimPickerRow(result.sim_info.sim_id, tag=result.sim_info))

            def _picker_dialog_callback(picker_dialog):
                if not picker_dialog.accepted:
                    with telemetry_helper.begin_hook(writer, TELEMETRY_HOOK_SITUATION_REJECTED, sim_info=sim_results.player_sim_info) as hook:
                        hook.write_guid('type', chosen_situation.guid64)
                    return
                picked_sims = picker_dialog.get_result_tags()
                for sim_info in picked_sims:
                    guest_list.add_guest_info(SituationGuestInfo.construct_from_purpose(sim_info.id, bring_other_sims_data.situation_job, SituationInvitationPurpose.INVITED))
                services.get_zone_situation_manager().create_situation(chosen_situation, guest_list=guest_list, zone_id=chosen_zone_id, scoring_enabled=chosen_situation.NPC_hosted_situation_scoring_enabled)
                with telemetry_helper.begin_hook(writer, TELEMETRY_HOOK_SITUATION_ACCEPTED, sim_info=sim_results.player_sim_info) as hook:
                    hook.write_guid(TELEMETRY_SITUATION_TYPE_ID, chosen_situation.guid64)
                    hook.write_int(TELEMETRY_GUEST_COUNT, len(picked_sims))

            picker_dialog.show_dialog(on_response=_picker_dialog_callback)

        with telemetry_helper.begin_hook(writer, TELEMETRY_HOOK_SITUATION_INVITED, sim_info=sim_results.player_sim_info) as hook:
            hook.write_guid(TELEMETRY_SITUATION_TYPE_ID, chosen_situation.guid64)
            hook.write_int(TELEMETRY_CHOSEN_ZONE, chosen_zone_id)
        dialog = chosen_situation.NPC_hosted_situation_start_message(sim_results.player_sim_info, target_sim_id=sim_results.host_sim_info.id, resolver=DoubleSimResolver(sim_results.player_sim_info, sim_results.host_sim_info))
        dialog.show_dialog(on_response=_create_NPC_hosted_situation)

TunableNPCHostedSituationSchedule = TunableFactory.create_auto_factory(NPCHostedSituationSchedule)

class NPCHostedSituationService(Service):
    __qualname__ = 'NPCHostedSituationService'
    NPC_HOSTED_EVENT_SCHEDULER = TunableNPCHostedSituationSchedule()
    WELCOME_WAGON_TUNING = TunableTuple(description='\n        Tuning dedicated to started the welcome wagon.\n        ', situation=TunableReference(description='\n            The welcome wagon situation.\n            ', manager=services.get_instance_manager(sims4.resources.Types.SITUATION)), minimum_time_to_start_situation=TunableSimMinute(description='\n            The minimum amount of time since the service started that the\n            welcome wagon will begin.\n            ', default=60, minimum=0), available_time_of_day=TunableTuple(description='\n            The start and end times that determine the time that the welcome\n            wagon can begin.  This has nothing to do with the end time of the\n            situation.  The duration of the situation can last beyond the times\n            tuned here.\n            ', start_time=TunableTimeOfDay(description='\n                The start time that the welcome wagon can begin.\n                '), end_time=TunableTimeOfDay(description='\n                The end time that the welcome wagon can begin.\n                ')))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._npc_hosted_situation_scheduler = None
        self._welcome_wagon_alarm = None

    def get_all_npc_hosted_events(self):
        if self._npc_hosted_situation_scheduler is None:
            logger.error('Trying to access NPC Hosted Scheduler before it has been setup.')
            return set()
        return self._npc_hosted_situation_scheduler.get_all_npc_hosted_situations()

    def _start_welcome_wagon(self, _):
        situation_manager = services.get_zone_situation_manager()
        if situation_manager.is_user_facing_situation_running():
            self._schedule_welcome_wagon()
            return
        active_household = services.active_household()
        active_household.needs_welcome_wagon = False
        welcome_wagon_situation = NPCHostedSituationService.WELCOME_WAGON_TUNING.situation
        guest_list = welcome_wagon_situation.get_predefined_guest_list()
        if guest_list is None:
            return
        game_clock_services = services.game_clock_service()
        if game_clock_services.clock_speed == ClockSpeedMode.SUPER_SPEED3:
            game_clock_services.set_clock_speed(ClockSpeedMode.NORMAL)
        situation_manager.create_situation(welcome_wagon_situation, guest_list=guest_list, user_facing=True, scoring_enabled=False)

    def on_all_households_and_sim_infos_loaded(self, client):
        self._npc_hosted_situation_scheduler = NPCHostedSituationService.NPC_HOSTED_EVENT_SCHEDULER()
        self._schedule_welcome_wagon()

    def _schedule_welcome_wagon(self):
        active_household = services.active_household()
        if not active_household.needs_welcome_wagon:
            return
        if services.current_zone_id() != active_household.home_zone_id:
            return
        minimum_time = interval_in_sim_minutes(NPCHostedSituationService.WELCOME_WAGON_TUNING.minimum_time_to_start_situation)
        now = services.time_service().sim_now
        possible_time = now + minimum_time
        if possible_time.time_between_day_times(NPCHostedSituationService.WELCOME_WAGON_TUNING.available_time_of_day.start_time, NPCHostedSituationService.WELCOME_WAGON_TUNING.available_time_of_day.end_time):
            time_till_welcome_wagon = minimum_time
        else:
            time_till_welcome_wagon = now.time_till_next_day_time(NPCHostedSituationService.WELCOME_WAGON_TUNING.available_time_of_day.start_time)
        self._welcome_wagon_alarm = alarms.add_alarm(self, time_till_welcome_wagon, self._start_welcome_wagon)

    def stop(self):
        super().stop()
        if self._welcome_wagon_alarm is not None:
            alarms.cancel_alarm(self._welcome_wagon_alarm)

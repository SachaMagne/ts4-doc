import operator
from scheduler import TunableDayAvailability
from sims4 import random
from sims4.tuning.tunable import AutoFactoryInit, HasTunableSingletonFactory, TunableList, TunableTuple, TunableMapping, Tunable, TunableLiteralOrRandomValue, TunedIntervalLiteral
from situations.situation import Situation
import date_and_time
import services
import sims4.log
logger = sims4.log.Logger('Situations')

class SituationCurve(HasTunableSingletonFactory, AutoFactoryInit):
    __qualname__ = 'SituationCurve'

    class _DesiredSituations(HasTunableSingletonFactory, AutoFactoryInit):
        __qualname__ = 'SituationCurve._DesiredSituations'
        FACTORY_TUNABLES = {'desired_sim_count': TunableLiteralOrRandomValue(description='\n                The number of Sims desired to be participating in the situation.\n                ', tunable_type=int, default=0), 'weighted_situations': TunableList(description='\n                A weighted list of situations to be used while fulfilling the\n                desired Sim count.\n                ', tunable=TunableTuple(situation=Situation.TunableReference(pack_safe=True), weight=Tunable(tunable_type=int, default=1)))}

    @staticmethod
    def _verify_tunable_callback(instance_class, tunable_name, source, value, **kwargs):
        keys = set()
        for item in value.entries:
            days = item.days_of_the_week
            for (day, enabled) in days.items():
                while enabled:
                    if day in keys:
                        logger.error('WalkbyTuning {} has multiple population curves for the day {}.', source, day, owner='manus')
                    else:
                        keys.add(day)
            if item.walkby_desire_by_time_of_day:
                for hour in item.walkby_desire_by_time_of_day.keys():
                    while hour < 0 or hour > 24:
                        logger.error('Situation Curve in {} has in invalid hour of the day {}. Range: [0, 24].', source, hour, owner='manus')
            else:
                logger.error("Situation Curve in {}'s days {} has no walkby desire population curve.", source, days, owner='manus')

    FACTORY_TUNABLES = {'entries': TunableList(description='\n            A list of tuples declaring a relationship between days of the week\n            and desire curves.\n            ', tunable=TunableTuple(description='\n                The first value is the day of the week that maps to a desired\n                curve of population by time of the day.\n                \n                days_of_the_week    population_desire_by_time_of_day\n                    M,Th,F                time_curve_1\n                    W,Sa                  time_curve_2\n                    \n                By production/design request we do not support multiple\n                population curves for the same day. e.g. if you want something\n                special to occur at noon on a Wednesday, make a unique curve for\n                Wednesday and apply the changes to it.\n                ', days_of_the_week=TunableDayAvailability(), walkby_desire_by_time_of_day=TunableMapping(description='\n                    Each entry in the map has two columns. The first column is\n                    the hour of the day (0-24) that maps to a desired list of\n                    population (second column).\n                    \n                    The entry with starting hour that is closest to, but before\n                    the current hour will be chosen.\n                    \n                    Given this tuning: \n                        hour_of_day           desired_situations\n                        6                     [(w1, s1), (w2, s2)]\n                        10                    [(w1, s2)]\n                        14                    [(w2, s5)]\n                        20                    [(w9, s0)]\n                        \n                    if the current hour is 11, hour_of_day will be 10 and desired is [(w1, s2)].\n                    if the current hour is 19, hour_of_day will be 14 and desired is [(w2, s5)].\n                    if the current hour is 23, hour_of_day will be 20 and desired is [(w9, s0)].\n                    if the current hour is 2, hour_of_day will be 20 and desired is [(w9, s0)]. (uses 20 tuning because it is not 6 yet)\n                    \n                    The entries will be automatically sorted by time.\n                    ', key_name='hour_of_day', key_type=Tunable(tunable_type=int, default=0), value_name='desired_walkby_situations', value_type=_DesiredSituations.TunableFactory()))), 'verify_tunable_callback': _verify_tunable_callback}

    def _get_sorted_situation_schedule(self, day):
        situation_schedule = []
        for item in self.entries:
            enabled = item.days_of_the_week.get(day, None)
            while enabled:
                while True:
                    for (beginning_hour, situations) in item.walkby_desire_by_time_of_day.items():
                        situation_schedule.append((beginning_hour, situations))
        situation_schedule.sort(key=operator.itemgetter(0))
        return situation_schedule

    def _get_desired_situations(self):
        if not self.entries:
            return
        time_of_day = services.time_service().sim_now
        hour_of_day = time_of_day.hour()
        day = time_of_day.day()
        situation_schedule = self._get_sorted_situation_schedule(day)
        if not situation_schedule:
            return
        entry = situation_schedule[-1]
        desire = entry[1]
        for entry in situation_schedule:
            if entry[0] <= hour_of_day:
                desire = entry[1]
            else:
                break
        return desire

    def get_desired_sim_count_tunable(self):
        desire = self._get_desired_situations()
        if desire is None:
            return TunedIntervalLiteral(0)
        return desire.desired_sim_count

    def get_timespan_to_next_shift_time(self, time_of_day):
        if not self.entries:
            return
        days_to_schedule_ahead = 1
        current_day = time_of_day.day()
        next_day = (current_day + days_to_schedule_ahead) % 7
        next_day_sorted_times = self._get_sorted_situation_schedule(next_day)
        if next_day_sorted_times:
            next_shift_hour = next_day_sorted_times[0][0]
        else:
            next_shift_hour = 0
        now = services.time_service().sim_now
        sorted_times = self._get_sorted_situation_schedule(current_day)
        scheduled_day = int(now.absolute_days())
        now_hour = now.hour()
        for (shift_hour, _) in sorted_times:
            while shift_hour > now_hour:
                next_shift_hour = shift_hour
                break
        scheduled_day += 1
        future = date_and_time.create_date_and_time(days=scheduled_day, hours=next_shift_hour)
        time_span_until = future - now
        return time_span_until

    def get_situation(self, predicate=lambda _: True):
        desire = self._get_desired_situations()
        if desire is None:
            return
        weighted_situations = tuple((item.weight, item.situation) for item in desire.weighted_situations if predicate(item.situation))
        sitaution = random.weighted_random_item(weighted_situations)
        return sitaution

from sims4.tuning.tunable import HasTunableSingletonFactory, TunableVariant
import services

class _SituationTimeJump:
    __qualname__ = '_SituationTimeJump'

    def should_load(self, seed):
        raise NotImplementedError

    def require_guest_list_regeneration(self, situation):
        raise NotImplementedError

class SituationTimeJumpDisallow(HasTunableSingletonFactory):
    __qualname__ = 'SituationTimeJumpDisallow'

    def should_load(self, seed):
        if services.game_clock_service().time_has_passed_in_world_since_zone_save():
            return False
        return True

    def require_guest_list_regeneration(self, situation):
        return False

class SituationTimeJumpAllow(HasTunableSingletonFactory):
    __qualname__ = 'SituationTimeJumpAllow'

    def should_load(self, seed):
        return True

    def require_guest_list_regeneration(self, situation):
        if services.game_clock_service().time_has_passed_in_world_since_zone_save():
            return True
        return False

class SituationTimeJumpSimulate(SituationTimeJumpAllow):
    __qualname__ = 'SituationTimeJumpSimulate'

    def should_load(self, seed):
        if not services.game_clock_service().time_has_passed_in_world_since_zone_save():
            return True
        situation_type = seed.situation_type
        if situation_type is not None and situation_type.should_load_after_time_jump(seed):
            seed.allow_time_jump = True
            return True
        return False

SITUATION_TIME_JUMP_DISALLOW = SituationTimeJumpDisallow()

class TunableSituationTimeJumpVariant(TunableVariant):
    __qualname__ = 'TunableSituationTimeJumpVariant'

    def __init__(self, *args, **kwargs):
        return super().__init__(disallow=SituationTimeJumpDisallow.TunableFactory(), allow=SituationTimeJumpAllow.TunableFactory(), simulate=SituationTimeJumpSimulate.TunableFactory(), default='disallow', *args, **kwargs)

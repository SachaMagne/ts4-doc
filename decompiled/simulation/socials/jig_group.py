from interactions import ParticipantType
from interactions.constraints import Constraint, Anywhere, create_constraint_set, Nowhere
from interactions.interaction_finisher import FinishingType
from interactions.utils.routing import get_two_person_transforms_for_jig, fgl_and_get_two_person_transforms_for_jig
from sims4.tuning.instances import lock_instance_tunables
from sims4.tuning.tunable import TunableReference, TunableMapping, TunableEnumEntry, Tunable, TunableSimMinute
from socials.side_group import SideGroup
import interactions.constraints
import placement
import services
import sims4.log
import socials.group
from objects.pools.pool import get_pool_by_block_id
logger = sims4.log.Logger('Social Group')

class JigGroup(SideGroup):
    __qualname__ = 'JigGroup'
    INSTANCE_TUNABLES = {'jig': TunableReference(description='\n            The jig to use for finding a place to do the social.', manager=services.definition_manager()), 'participant_slot_map': TunableMapping(description='\n            The slot index mapping on the jig keyed by participant type', key_type=TunableEnumEntry(ParticipantType, ParticipantType.Actor), value_type=Tunable(description='The slot index for the participant type', tunable_type=int, default=0)), 'cancel_delay': TunableSimMinute(description='\n        Amount of time a jig group must be inactive before it will shut down.\n        ', default=15), 'stay_outside': Tunable(description='\n        Whether the FGL should require the jig to be outside.\n        ', tunable_type=bool, default=False)}
    DEFAULT_SLOT_INDEX_ACTOR = 1
    DEFAULT_SLOT_INDEX_TARGET = 0

    @classmethod
    def _get_jig_transforms(cls, initiating_sim, target_sim, picked_object=None, participant_slot_overrides=None):
        slot_map = cls.participant_slot_map if participant_slot_overrides is None else participant_slot_overrides
        actor_slot_index = slot_map.get(ParticipantType.Actor, cls.DEFAULT_SLOT_INDEX_ACTOR)
        target_slot_index = slot_map.get(ParticipantType.TargetSim, cls.DEFAULT_SLOT_INDEX_TARGET)
        if picked_object is not None and picked_object.carryable_component is None and not picked_object.is_sim:
            try:
                return get_two_person_transforms_for_jig(picked_object.definition, picked_object.transform, picked_object.routing_surface, actor_slot_index, target_slot_index)
            except RuntimeError:
                pass
        fallback_routing_surface = None
        if target_sim is not None and target_sim.in_pool:
            sim_pool = get_pool_by_block_id(target_sim.block_id)
            fallback_routing_surface = sim_pool.world_routing_surface
        return fgl_and_get_two_person_transforms_for_jig(cls.jig, initiating_sim, actor_slot_index, target_sim, target_slot_index, cls.stay_outside, fallback_routing_surface=fallback_routing_surface)

    def __init__(self, *args, si=None, target_sim=None, participant_slot_overrides=None, **kwargs):
        super().__init__(si=si, target_sim=target_sim, *args, **kwargs)
        initiating_sim = si.sim
        if initiating_sim is None or target_sim is None:
            logger.error('JigGroup {} cannot init with initial sim {()} or target sim {()}', self.__name__, initiating_sim, target_sim)
            return
        self._initating_sim_ref = initiating_sim.ref() if initiating_sim is not None else None
        self._target_sim_ref = target_sim.ref() if target_sim is not None else None
        self._picked_object_ref = si.picked_object.ref() if si.picked_object is not None else None
        self.participant_slot_overrides = participant_slot_overrides
        self._create_social_geometry()

    @property
    def initiating_sim(self):
        if self._initating_sim_ref is not None:
            return self._initating_sim_ref()

    @property
    def target_sim(self):
        if self._target_sim_ref is not None:
            return self._target_sim_ref()

    @property
    def picked_object(self):
        if self._picked_object_ref is not None:
            return self._picked_object_ref()

    def _create_social_geometry(self, *args, **kwargs):
        self._sim_transform_map = {}
        self.geometry = None
        (sim_transform, target_transform, routing_surface) = self._get_jig_transforms(self.initiating_sim, self.target_sim, picked_object=self.picked_object, participant_slot_overrides=self.participant_slot_overrides)
        self._jig_transform = target_transform
        if target_transform is not None:
            self._jig_polygon = placement.get_placement_footprint_polygon(target_transform.translation, target_transform.orientation, routing_surface, self.jig.get_footprint(0))
        else:
            self._jig_polygon = None
        self._sim_transform_map[self.initiating_sim] = sim_transform
        self._sim_transform_map[self.target_sim] = target_transform
        if target_transform is None:
            self._constraint = Nowhere('JigGroup, failed to FGL and place the jig. Sim: {}, Target Sim: {}, Picked Object: {}', self.initiating_sim, self.target_sim, self.picked_object)
            return
        target_forward = target_transform.transform_vector(sims4.math.FORWARD_AXIS)
        self._set_focus(target_transform.translation, target_forward, routing_surface, refresh_geometry=False)
        self._initialize_constraint(notify=True)

    @classmethod
    def _verify_tuning_callback(cls):
        if cls.jig is None:
            logger.error('JigGroup {} must have a jig tuned.', cls.__name__)

    @classmethod
    def make_constraint_default(cls, actor, target_sim, position, routing_surface, participant_type=ParticipantType.Actor, picked_object=None, participant_slot_overrides=None):
        (actor_transform, target_transform, routing_surface) = cls._get_jig_transforms(actor, target_sim, picked_object=picked_object, participant_slot_overrides=participant_slot_overrides)
        if actor_transform is None or target_transform is None:
            return Nowhere('JigGroup could not find a location to place the social. Sim: {}, Target Sim: {}', actor, target_sim)
        if participant_type == ParticipantType.Actor:
            constraint_transform = actor_transform
        elif participant_type == ParticipantType.TargetSim:
            constraint_transform = target_transform
        else:
            return Anywhere()
        return interactions.constraints.Transform(constraint_transform, routing_surface=routing_surface, debug_name='JigGroupConstraint')

    def _relocate_group_around_focus(self, *args, **kwargs):
        return False

    @property
    def group_radius(self):
        if self._jig_polygon is not None:
            return self._jig_polygon.radius()
        return 0

    @property
    def jig_polygon(self):
        return self._jig_polygon

    @property
    def jig_transform(self):
        return self._jig_transform

    def get_constraint(self, sim):
        transform = self._sim_transform_map.get(sim, None)
        if transform is not None:
            return interactions.constraints.Transform(transform, routing_surface=self.routing_surface)
        if sim in self._sim_transform_map:
            return Nowhere("JigGroup, Sim is expected to have a transform but we didn't find a good spot for them. Sim: {}", sim)
        return Anywhere()

    def _make_constraint(self, *args, **kwargs):
        if self._constraint is None:
            constraints = [interactions.constraints.Transform(t, routing_surface=self.routing_surface) for t in self._sim_transform_map.values()]
            self._constraint = create_constraint_set(constraints) if constraints else Anywhere()
        return self._constraint

    _create_adjustment_alarm = socials.group.SocialGroup._create_adjustment_alarm

    def _consider_adjusting_sim(self, sim=None, initial=False):
        if not initial:
            for sim in self:
                sis = self._si_registry.get(sim)
                if sis is not None and any(not si.staging for si in sis):
                    return
                for _ in self.queued_mixers_gen(sim):
                    pass
            if self.time_since_interaction().in_minutes() < self.cancel_delay:
                return
            self.shutdown(FinishingType.NATURAL)

lock_instance_tunables(JigGroup, social_anchor_object=None)
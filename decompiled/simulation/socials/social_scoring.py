import weakref
from interactions.constraints import CostFunctionBase
import sims4.math
import socials.geometry

class SocialGroupCostFunction(CostFunctionBase):
    __qualname__ = 'SocialGroupCostFunction'

    def __init__(self, group, sim):
        self._group_ref = weakref.ref(group)
        self._sim = sim

    def constraint_cost(self, position, orientation, routing_surface):
        group = self._group_ref()
        if group is None:
            return 0.0
        geometry = group.geometry
        if not geometry or len(geometry) == 1 and self._sim in geometry:
            ideal_position = group.position
            effective_distance = (position - ideal_position).magnitude_2d()*2.0
            score = socials.geometry.SocialGeometry.GROUP_DISTANCE_CURVE.get(effective_distance)
            return -score
        (base_focus, base_field) = socials.geometry._get_social_geometry_for_sim(self._sim)
        transform = sims4.math.Transform(position, orientation)
        multiplier = socials.geometry.score_transform(transform, self._sim, geometry, group.group_radius, base_focus, base_field)
        offset = multiplier*socials.geometry.SocialGeometry.SCORE_STRENGTH_MULTIPLIER
        if self._sim in geometry and sims4.math.vector3_almost_equal_2d(position, self._sim.position, epsilon=0.01):
            offset += socials.geometry.SocialGeometry.SCORE_OFFSET_FOR_CURRENT_POSITION
        return -offset

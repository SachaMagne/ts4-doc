from singletons import DEFAULT
from statistics.base_statistic import GalleryLoadBehavior
from statistics.base_statistic_listener import BaseStatisticCallbackListener
import services
import sims4.callback_utils
import sims4.log
import uid
logger = sims4.log.Logger('Statistic')
with sims4.reload.protected(globals()):
    _handle_id_gen = uid.UniqueIdGenerator(1)

class BaseStatisticTracker:
    __qualname__ = 'BaseStatisticTracker'

    def __init__(self, owner=None):
        self._statistics = None
        self._owner = owner
        self._watchers = {}
        self._on_remove_callbacks = None
        self.suppress_callback_setup_during_load = False
        self.statistics_to_skip_load = None

    def __iter__(self):
        if self._statistics is not None:
            return self._statistics.values().__iter__()
        return iter([])

    def __len__(self):
        if self._statistics is not None:
            return len(self._statistics)
        return 0

    @property
    def owner(self):
        return self._owner

    def _statistics_values_gen(self):
        if self._statistics:
            for stat in self._statistics.values():
                yield stat

    def destroy(self):
        for stat in list(self):
            stat.on_remove(on_destroy=True)
        self._watchers.clear()
        self._on_remove_callbacks = None

    def on_initial_startup(self):
        pass

    def create_and_add_listener(self, stat_type, threshold, callback, on_callback_alarm_reset=None) -> BaseStatisticCallbackListener:
        stat = self.get_statistic(stat_type, add=stat_type.add_if_not_in_tracker)
        if stat is not None:
            callback_listener = stat.create_and_add_callback_listener(threshold, callback, on_callback_alarm_reset=on_callback_alarm_reset)
            return callback_listener

    def remove_listener(self, listener):
        stat = self.get_statistic(listener.statistic_type)
        if stat is not None:
            stat.remove_callback_listener(listener)

    def add_watcher(self, callback):
        handle_id = _handle_id_gen()
        self._watchers[handle_id] = callback
        return handle_id

    def has_watcher(self, handle):
        return handle in self._watchers

    def remove_watcher(self, handle):
        del self._watchers[handle]

    def notify_watchers(self, stat_type, old_value, new_value):
        for watcher in list(self._watchers.values()):
            watcher(stat_type, old_value, new_value)

    def add_on_remove_callback(self, callback):
        if self._on_remove_callbacks is None:
            self._on_remove_callbacks = sims4.callback_utils.CallableList()
        self._on_remove_callbacks.append(callback)

    def remove_on_remove_callback(self, callback):
        if callback in self._on_remove_callbacks:
            self._on_remove_callbacks.remove(callback)
        if not (self._on_remove_callbacks is not None and self._on_remove_callbacks):
            self._on_remove_callbacks = None

    def add_statistic(self, stat_type, owner=None, **kwargs):
        if self._statistics:
            stat = self._statistics.get(stat_type)
        else:
            stat = None
        if owner is None:
            owner = self._owner
        if stat is None and stat_type.can_add(owner, **kwargs):
            stat = stat_type(self)
            if self._statistics is None:
                self._statistics = {}
            self._statistics[stat_type] = stat
            stat.on_add()
            value = stat.get_value()
            self.notify_watchers(stat_type, value, value)
        return stat

    def remove_statistic(self, stat_type, on_destroy=False):
        if self.has_statistic(stat_type):
            stat = self._statistics[stat_type]
            del self._statistics[stat_type]
            if self._on_remove_callbacks:
                self._on_remove_callbacks(stat)
            stat.on_remove(on_destroy=on_destroy)

    def get_statistic(self, stat_type, add=False):
        if self._statistics:
            stat = self._statistics.get(stat_type)
        else:
            stat = None
        if stat is None and add:
            stat = self.add_statistic(stat_type)
        return stat

    def has_statistic(self, stat_type):
        if self._statistics is None:
            return False
        return stat_type in self._statistics

    def get_communicable_statistic_set(self):
        if self._statistics is None:
            return set()
        return {stat_type for stat_type in self._statistics if stat_type.communicable_by_interaction_tag is not None}

    def get_value(self, stat_type, add=False):
        stat = self.get_statistic(stat_type, add=add)
        if stat is not None:
            return stat.get_value()
        return stat_type.default_value

    def get_int_value(self, stat_type, scale:int=None):
        value = self.get_value(stat_type)
        if scale is not None:
            value = scale*value/stat_type.max_value
        return int(sims4.math.floor(value))

    def get_user_value(self, stat_type):
        stat = self.get_statistic(stat_type)
        if stat is not None:
            return stat.get_user_value()
        return stat_type.default_user_value

    def set_value(self, stat_type, value, add=DEFAULT, from_load=False, **kwargs):
        if add is DEFAULT:
            add = stat_type.add_if_not_in_tracker or from_load
        stat = self.get_statistic(stat_type, add=add)
        if stat is not None:
            stat.set_value(value, from_load=from_load)

    def set_user_value(self, stat_type, user_value):
        stat = self.get_statistic(stat_type, add=True)
        stat.set_user_value(user_value)

    def add_value(self, stat_type, amount, **kwargs):
        if amount == 0:
            logger.warn('Attempting to add 0 to stat {}', stat_type)
            return
        stat = self.get_statistic(stat_type, add=stat_type.add_if_not_in_tracker)
        if stat is not None:
            stat.add_value(amount, **kwargs)

    def set_max(self, stat_type):
        stat = self.get_statistic(stat_type, add=stat_type.add_if_not_in_tracker)
        if stat is not None:
            self.set_value(stat_type, stat.max_value)

    def set_min(self, stat_type):
        stat = self.get_statistic(stat_type, add=stat_type.add_if_not_in_tracker)
        if stat is not None:
            self.set_value(stat_type, stat.min_value)

    def get_decay_time(self, stat_type, threshold):
        pass

    def set_convergence(self, stat_type, convergence):
        raise TypeError("This stat type doesn't have a convergence value.")

    def reset_convergence(self, stat_type):
        raise TypeError("This stat type doesn't have a convergence value.")

    def set_all_commodities_to_max(self, visible_only=False, core_only=False):
        for stat_type in list(self._statistics):
            stat = self.get_statistic(stat_type)
            while stat is not None:
                if not visible_only and not core_only or visible_only and stat.is_visible or core_only and stat.core:
                    self.set_value(stat_type, stat_type.max_value)

    def save(self):
        save_list = []
        if self._statistics:
            for stat in self._statistics.values():
                while stat.persisted:
                    value = stat.get_saved_value()
                    save_data = (type(stat).__name__, value)
                    save_list.append(save_data)
        return save_list

    def _should_add_commodity_from_gallery(self, statistic_type, skip_load):
        if statistic_type.gallery_load_behavior == GalleryLoadBehavior.LOAD_FOR_ALL:
            return True
        if self.owner.is_sim:
            if skip_load and statistic_type.gallery_load_behavior != GalleryLoadBehavior.LOAD_ONLY_FOR_SIM:
                return False
        elif self.owner.is_downloaded and statistic_type.gallery_load_behavior != GalleryLoadBehavior.LOAD_ONLY_FOR_OBJECT:
            return False
        return True

    def load(self, load_list):
        try:
            for (stat_type_name, value) in load_list:
                stat_cls = services.get_instance_manager(sims4.resources.Types.STATISTIC).get(stat_type_name)
                while stat_cls is not None:
                    if stat_cls.persisted:
                        self.set_value(stat_cls, value)
                    else:
                        logger.info('Trying to load unavailable STATISTIC resource: {}', stat_type_name)
        except ValueError:
            logger.error('Attempting to load old data in BaseStatisticTracker.load()')

    def debug_output_all(self, _connection):
        if self._statistics:
            for stat in self._statistics.values():
                sims4.commands.output('{:<24} Value: {:-6.2f}'.format(stat.__class__.__name__, stat.get_value()), _connection)

    def debug_set_all_to_max_except(self, stat_to_exclude, core=True):
        for stat_type in list(self._statistics):
            while not core or self.get_statistic(stat_type).core:
                if stat_type != stat_to_exclude:
                    self.set_value(stat_type, stat_type.max_value)

    def debug_set_all_to_min(self, core=True):
        for stat_type in list(self._statistics):
            while not core or self.get_statistic(stat_type).core:
                self.set_value(stat_type, stat_type.min_value)

    def debug_set_all_to_default(self, core=True):
        for stat_type in list(self._statistics):
            while not core or self.get_statistic(stat_type).core:
                self.set_value(stat_type, stat_type.initial_value)

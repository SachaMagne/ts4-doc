from sims4.localization import TunableLocalizedString
from sims4.tuning.instances import HashedTunedInstanceMetaclass
from sims4.tuning.tunable import HasTunableReference, Tunable, TunableResourceKey, TunableEnumEntry
from sims4.tuning.tunable_base import ExportModes
from sims4.utils import classproperty
from statistics.base_statistic import BaseStatistic, GalleryLoadBehavior
from statistics.tunable import TunableStatAsmParam
import services
import sims4.resources

class Statistic(HasTunableReference, BaseStatistic, metaclass=HashedTunedInstanceMetaclass, manager=services.statistic_manager()):
    __qualname__ = 'Statistic'
    INSTANCE_TUNABLES = {'stat_asm_param': TunableStatAsmParam.TunableFactory(locked_args={'use_effective_skill_level': True}), 'initial_value': Tunable(description='\n            The initial value of this statistic.\n            ', tunable_type=int, default=0), 'min_value_tuning': Tunable(description='\n            The minimum value that this statistic can reach.\n            ', tunable_type=int, default=0, export_modes=ExportModes.All), 'max_value_tuning': Tunable(description='\n            The minimum value that this statistic can reach.\n            ', tunable_type=int, default=100, export_modes=ExportModes.All), 'stat_name': TunableLocalizedString(description='\n            Localized name of this statistic.\n            ', allow_none=True, export_modes=ExportModes.All), 'icon': TunableResourceKey(description='\n            Icon to be displayed for the Statistic.\n            ', allow_none=True, resource_types=sims4.resources.CompoundTypes.IMAGE), 'persisted_tuning': Tunable(description="\n            Whether this statistic will persist when saving a Sim or an object.\n            For example, a Sims's SI score statistic should never persist.\n            ", tunable_type=bool, default=True), 'gallery_load_behavior': TunableEnumEntry(description="\n            When owner of commodity is loaded from the gallery, tune this to\n            determine if commodity should be loaded or not.\n            \n            DONT_LOAD = Don't load statistic when owner is coming from gallery\n            \n            LOAD_ONLY_FOR_OBJECT = Load only if statistic is being added to an\n            object.  If this statistic is tuned as a linked stat to a state, make\n            sure the state is also marked as gallery persisted. i.e. Statistics\n            like fish_freshness or gardening_groth. Switching on this bit has\n            performance implications when downloading a lot from the gallery.\n            Please discuss with a GPE when setting this tunable.\n    \n            LOAD_ONLY_FOR_SIM = Load only if statistic is being added to a sim.\n            LOAD_FOR_ALL = Always load commodity.  This has the same ramifications\n            as LOAD_ONLY_FOR_OBJECT if owner is an object.\n            ", tunable_type=GalleryLoadBehavior, default=GalleryLoadBehavior.LOAD_ONLY_FOR_SIM)}

    def __init__(self, tracker):
        super().__init__(tracker, self.initial_value)

    @classproperty
    def name(cls):
        return cls.__name__

    @classproperty
    def max_value(cls):
        return cls.max_value_tuning

    @classproperty
    def min_value(cls):
        return cls.min_value_tuning

    @classproperty
    def persisted(cls):
        return cls.persisted_tuning

    @classproperty
    def persists_across_gallery_for_state(cls):
        if cls.gallery_load_behavior == GalleryLoadBehavior.LOAD_FOR_ALL or cls.gallery_load_behavior == GalleryLoadBehavior.LOAD_ONLY_FOR_OBJECT:
            return True
        return False

    @classproperty
    def default_value(cls):
        return cls.initial_value

    def get_asm_param(self):
        return self.stat_asm_param.get_asm_param(self)

    @classproperty
    def valid_for_stat_testing(cls):
        return True

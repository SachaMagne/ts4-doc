from protocolbuffers import SimObjectAttributes_pb2 as protocols
import services
import sims4.log
import statistics.base_statistic_tracker
logger = sims4.log.Logger('Statistic')

class StatisticTracker(statistics.base_statistic_tracker.BaseStatisticTracker):
    __qualname__ = 'StatisticTracker'

    def save(self):
        save_list = []
        for stat in self._statistics_values_gen():
            while stat.persisted:
                try:
                    statistic_data = protocols.Statistic()
                    statistic_data.name_hash = stat.guid64
                    statistic_data.value = stat.get_saved_value()
                    save_list.append(statistic_data)
                except Exception:
                    logger.exception('Exception thrown while trying to save stat {}', stat, owner='rez')
        return save_list

    def load(self, statistics, skip_load=False):
        try:
            statistics_manager = services.get_instance_manager(sims4.resources.Types.STATISTIC)
            for statistics_data in statistics:
                stat_cls = statistics_manager.get(statistics_data.name_hash)
                if stat_cls is not None:
                    if not self._should_add_commodity_from_gallery(stat_cls, skip_load):
                        pass
                    if not stat_cls.persisted:
                        pass
                    if self.statistics_to_skip_load is not None and stat_cls in self.statistics_to_skip_load:
                        pass
                    self.set_value(stat_cls, statistics_data.value, from_load=True)
                else:
                    logger.info('Trying to load unavailable STATISTIC resource: {}', statistics_data.name_hash)
        finally:
            self.statistics_to_skip_load = None

import services
import sims4.commands

@sims4.commands.Command('story_progression.process_index', command_type=sims4.commands.CommandType.Cheat)
def process_index(index:int=0, _connection=None):
    current_zone = services.current_zone()
    if current_zone is None:
        return False
    story_progression_service = current_zone.story_progression_service
    if story_progression_service is None:
        return False
    story_progression_service.process_action_index(index)

@sims4.commands.Command('story_progression.process_all')
def process_all_story_progression(times_to_process:int=1, _connection=None):
    current_zone = services.current_zone()
    if current_zone is None:
        return False
    story_progression_service = current_zone.story_progression_service
    if story_progression_service is None:
        return False
    while times_to_process > 0:
        story_progression_service.process_all_actions()
        times_to_process -= 1
    sims4.commands.output('All story progression processed.', _connection)

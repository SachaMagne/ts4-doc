import operator
from protocolbuffers import SimObjectAttributes_pb2 as protocols
from event_testing import test_events
from interactions.base.picker_interaction import PickerSuperInteraction
from interactions.utils.death import DeathType
from sims.ghost import Ghost
from sims.sim_info_types import Gender
from sims4.tuning.tunable import Tunable, TunableMapping, TunableEnumEntry, TunableList, TunableTuple
from sims4.utils import flexmethod
from traits.traits import logger, Trait
from ui.ui_dialog_picker import ObjectPickerRow
import distributor.fields
import distributor.ops
import services
import sims.ghost
import sims4.telemetry
import telemetry_helper
TELEMETRY_GROUP_TRAITS = 'TRAT'
TELEMETRY_HOOK_ADD_TRAIT = 'TADD'
TELEMETRY_HOOK_REMOVE_TRAIT = 'TRMV'
TELEMETRY_FIELD_TRAIT_ID = 'idtr'
writer = sims4.telemetry.TelemetryWriter(TELEMETRY_GROUP_TRAITS)

class HasTraitTrackerMixin:
    __qualname__ = 'HasTraitTrackerMixin'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._trait_tracker = TraitTracker(self)

    @property
    def trait_tracker(self):
        return self._trait_tracker

    def add_trait(self, *args, **kwargs):
        return self._trait_tracker._add_trait(*args, **kwargs)

    def get_traits(self):
        return self._trait_tracker.equipped_traits

    def has_trait(self, *args, **kwargs):
        return self._trait_tracker.has_trait(*args, **kwargs)

    def remove_trait(self, *args, **kwargs):
        return self._trait_tracker._remove_trait(*args, **kwargs)

    @distributor.fields.Field(op=distributor.ops.SetTraits)
    def trait_ids(self):
        return self._trait_tracker.trait_ids

    resend_trait_ids = trait_ids.get_resend()

class TraitTracker:
    __qualname__ = 'TraitTracker'
    GENDER_TRAITS = TunableMapping(description='\n        A mapping from gender to trait. Any Sim with the specified gender will\n        have the corresponding gender trait.\n        ', key_type=TunableEnumEntry(description="\n            The Sim's gender.\n            ", tunable_type=Gender, default=Gender.MALE), value_type=Trait.TunableReference(description='\n            The trait associated with the specified gender.\n            '))
    TRAIT_INHERITANCE = TunableList(description='\n        Define how specific traits are transferred to offspring. Define keys of\n        sets of traits resulting in the assignment of another trait, weighted\n        against other likely outcomes.\n        ', tunable=TunableTuple(description='\n            A set of trait requirements and outcomes. Please note that inverted\n            requirements are not necessary. The game will automatically swap\n            parents A and B to try to fulfill the constraints.\n            \n            e.g. Alien Inheritance\n                Alien inheritance follows a simple set of rules:\n                 Alien+Alien always generates aliens\n                 Alien+None always generates part aliens\n                 Alien+PartAlien generates either aliens or part aliens\n                 PartAlien+PartAlien generates either aliens, part aliens, or regular Sims\n                 PartAlien+None generates either part aliens or regular Sims\n                 \n                Given the specifications involving "None", we need to probably\n                blacklist the two traits to detect a case where only one of the\n                two parents has a meaningful trait:\n                \n                a_whitelist = Alien\n                b_whitelist = Alien\n                outcome = Alien\n                \n                a_whitelist = Alien\n                b_blacklist = Alien,PartAlien\n                outcome = PartAlien\n                \n                etc...\n            ', parent_a_whitelist=TunableList(description='\n                Traits that parent A must have in order to generate this\n                outcome.\n                ', tunable=Trait.TunableReference(pack_safe=True)), parent_a_blacklist=TunableList(description='\n                Traits that parent A must not have in order to generate this\n                outcome.\n                ', tunable=Trait.TunableReference(pack_safe=True)), parent_b_whitelist=TunableList(description='\n                Traits that parent B must have in order to generate this\n                outcome.\n                ', tunable=Trait.TunableReference(pack_safe=True)), parent_b_blacklist=TunableList(description='\n                Traits that parent B must not have in order to generate this\n                outcome.\n                ', tunable=Trait.TunableReference(pack_safe=True)), outcomes=TunableList(description='\n                A weighted list of potential outcomes given that the\n                requirements have been satisfied.\n                ', tunable=TunableTuple(description='\n                    A weighted outcome. The weight is relative to other entries\n                    within this outcome set.\n                    ', weight=Tunable(description='\n                        The relative weight of this outcome versus other\n                        outcomes in this same set.\n                        ', tunable_type=float, default=1), trait=Trait.TunableReference(description='\n                        The potential inherited trait.\n                        ', allow_none=True, pack_safe=True)))))

    def __init__(self, sim_info):
        self._sim_info = sim_info
        self._equipped_traits = set()
        self._unlocked_equip_slot = 0
        self._buff_handles = {}

    def __iter__(self):
        return self._equipped_traits.__iter__()

    def __len__(self):
        return len(self._equipped_traits)

    def can_add_trait(self, trait):
        if self.has_trait(trait):
            logger.info('Trying to equip an existing trait {} for Sim {}', trait, self._sim_info)
            return False
        if self.empty_slot_number == 0 and trait.is_personality_trait:
            logger.info('Reach max equipment slot number {} for Sim {}', self.equip_slot_number, self._sim_info)
            return False
        if not trait.test_sim_info(self._sim_info):
            logger.info("Trying to equip a trait {} that conflicts with Sim {}'s age {} or gender {}", trait, self._sim_info, self._sim_info.age, self._sim_info.gender)
            return False
        if self.is_conflicting(trait):
            logger.info('Trying to equip a conflicting trait {} for Sim {}', trait, self._sim_info)
            return False
        return True

    def add_gender_trait(self):
        gender_trait = self.GENDER_TRAITS.get(self._sim_info.gender)
        if not (gender_trait is not None and self.has_trait(gender_trait)):
            self._add_trait(gender_trait)

    def _add_trait(self, trait, from_load=False):
        if not self.can_add_trait(trait):
            return False
        self._equipped_traits.add(trait)
        self._add_buffs(trait)
        self.update_voice_effect()
        if not from_load:
            if trait.is_personality_trait:
                for household_sim in self._sim_info.household:
                    if household_sim is self._sim_info:
                        pass
                    household_sim.relationship_tracker.add_known_trait(trait, self._sim_info.sim_id)
            self._sim_info.resend_trait_ids()
            sim = self._sim_info.get_sim_instance()
            if sim is not None:
                with telemetry_helper.begin_hook(writer, TELEMETRY_HOOK_ADD_TRAIT, sim=sim) as hook:
                    hook.write_int(TELEMETRY_FIELD_TRAIT_ID, trait.guid64)
                services.get_event_manager().process_event(test_events.TestEvent.TraitAddEvent, sim_info=self._sim_info)
            elif trait is Ghost.NETHERWORLD_TRAIT:
                self._sim_info.death_tracker.set_death_type(DeathType.NETHERWORLD)
        return True

    def _remove_trait(self, trait):
        if not self.has_trait(trait):
            logger.warn('Try to remove a non-equipped trait {}', trait)
            return False
        self._equipped_traits.remove(trait)
        self._remove_buffs(trait)
        self.update_voice_effect()
        self._sim_info.resend_trait_ids()
        if not any(t.is_ghost_trait for t in self._equipped_traits):
            sims.ghost.Ghost.remove_ghost_from_sim(self._sim_info)
        sim = self._sim_info.get_sim_instance()
        if sim is not None:
            with telemetry_helper.begin_hook(writer, TELEMETRY_HOOK_REMOVE_TRAIT, sim=sim) as hook:
                hook.write_int(TELEMETRY_FIELD_TRAIT_ID, trait.guid64)
        return True

    def remove_traits_of_type(self, trait_type):
        for trait in list(self._equipped_traits):
            while trait.trait_type == trait_type:
                self._remove_trait(trait)

    def clear_traits(self):
        for trait in list(self._equipped_traits):
            self._remove_trait(trait)

    def has_trait(self, trait):
        return trait in self._equipped_traits

    def has_any_trait(self, traits):
        return any(t in traits for t in self._equipped_traits)

    def is_conflicting(self, trait):
        return any(t.is_conflicting(trait) for t in self._equipped_traits)

    @staticmethod
    def _get_inherited_traits_internal(traits_a, traits_b, trait_entry):
        if trait_entry.parent_a_whitelist and not any(t in traits_a for t in trait_entry.parent_a_whitelist):
            return False
        if any(t in traits_a for t in trait_entry.parent_a_blacklist):
            return False
        if trait_entry.parent_b_whitelist and not any(t in traits_b for t in trait_entry.parent_b_whitelist):
            return False
        if any(t in traits_b for t in trait_entry.parent_b_blacklist):
            return False
        return True

    def get_inherited_traits(self, other_sim):
        traits_a = list(self)
        traits_b = list(other_sim.trait_tracker)
        inherited_entries = []
        for trait_entry in TraitTracker.TRAIT_INHERITANCE:
            while self._get_inherited_traits_internal(traits_a, traits_b, trait_entry) or self._get_inherited_traits_internal(traits_b, traits_a, trait_entry):
                inherited_entries.append(tuple((outcome.weight, outcome.trait) for outcome in trait_entry.outcomes))
        return inherited_entries

    @property
    def personality_traits(self):
        return tuple(trait for trait in self if trait.is_personality_trait)

    @property
    def aspiration_traits(self):
        return tuple(trait for trait in self if trait.is_aspiration_trait)

    @property
    def trait_ids(self):
        return list(t.guid64 for t in self._equipped_traits)

    @property
    def equipped_traits(self):
        return self._equipped_traits

    @property
    def equip_slot_number(self):
        equip_slot_number_map = Trait.EQUIP_SLOT_NUMBER_MAP
        age = self._sim_info.age
        slot_number = self._unlocked_equip_slot
        if age in equip_slot_number_map:
            slot_number += equip_slot_number_map[age]
        else:
            logger.warn('Trait.EQUIP_SLOT_NUMBER_MAP missing tuning for age {}', age)
        return slot_number

    @property
    def empty_slot_number(self):
        equipped_personality_traits = [trait for trait in self if trait.is_personality_trait]
        empty_slot_number = self.equip_slot_number - len(equipped_personality_traits)
        return max(empty_slot_number, 0)

    def _add_buffs(self, trait):
        buffs = trait.buffs
        buff_handles = []
        for buff in buffs:
            buff_handle = self._sim_info.add_buff(buff.buff_type, buff_reason=buff.buff_reason)
            buff_handles.append(buff_handle)
        if buff_handles:
            self._buff_handles[trait.guid64] = buff_handles

    def _remove_buffs(self, trait):
        buff_handles = self._buff_handles.get(trait.guid64, None)
        if buff_handles is not None:
            for buff_handle in buff_handles:
                self._sim_info.remove_buff(buff_handle)
            del self._buff_handles[trait.guid64]

    def update_voice_effect(self):
        try:
            voice_effect_request = max((trait.voice_effect for trait in self if trait.voice_effect is not None), key=operator.attrgetter('priority'))
            self._sim_info.voice_effect = voice_effect_request.voice_effect
        except ValueError:
            self._sim_info.voice_effect = None

    def save(self):
        data = protocols.PersistableTraitTracker()
        trait_ids = [trait.guid64 for trait in self._equipped_traits if trait.persistable]
        data.trait_ids.extend(trait_ids)
        return data

    def load(self, data):
        trait_manager = services.get_instance_manager(sims4.resources.Types.TRAIT)
        for trait_instance_id in data.trait_ids:
            trait = trait_manager.get(trait_instance_id)
            while trait is not None:
                self._add_trait(trait, from_load=True)

class TraitPickerSuperInteraction(PickerSuperInteraction):
    __qualname__ = 'TraitPickerSuperInteraction'
    INSTANCE_TUNABLES = {'is_add': Tunable(description='\n                If this interaction is trying to add a trait to the sim or to\n                remove a trait from the sim.', tunable_type=bool, default=True)}

    def _run_interaction_gen(self, timeline):
        self._show_picker_dialog(self.sim, target_sim=self.sim)
        return True

    @classmethod
    def _trait_selection_gen(cls, target):
        trait_manager = services.get_instance_manager(sims4.resources.Types.TRAIT)
        trait_tracker = target.sim_info.trait_tracker
        if cls.is_add:
            for trait in trait_manager.types.values():
                while not trait_tracker.has_trait(trait):
                    yield trait
        else:
            for trait in trait_tracker.equipped_traits:
                yield trait

    @flexmethod
    def picker_rows_gen(cls, inst, target, context, **kwargs):
        trait_tracker = target.sim_info.trait_tracker
        for trait in cls._trait_selection_gen(target):
            row = ObjectPickerRow(is_enable=not trait_tracker.is_conflicting(trait), name=trait.display_name(target), icon=trait.icon, row_description=trait.trait_description, tag=trait)
            yield row

    def on_choice_selected(self, choice_tag, **kwargs):
        trait = choice_tag
        if trait is not None:
            if self.is_add:
                self.target.sim_info.add_trait(trait)
            else:
                self.target.sim_info.remove_trait(trait)

from sims4.tuning.dynamic_enum import DynamicEnum
from sims4.tuning.tunable import AutoFactoryInit, HasTunableSingletonFactory, TunableStringHash64, TunableEnumEntry

class VoiceEffectRequestPriority(DynamicEnum):
    __qualname__ = 'VoiceEffectRequestPriority'
    INVALID = 0

class VoiceEffectRequest(HasTunableSingletonFactory, AutoFactoryInit):
    __qualname__ = 'VoiceEffectRequest'
    FACTORY_TUNABLES = {'voice_effect': TunableStringHash64(description='\n            When set, this voice effect will be applied to the Sim when the\n            trait is added and removed when the trait is removed.\n            '), 'priority': TunableEnumEntry(description='\n            The requests priority.\n            ', tunable_type=VoiceEffectRequestPriority, default=VoiceEffectRequestPriority.INVALID)}

from buffs.tunable import TunableBuffReference
from interactions.utils.tunable import ContentSet
from sims import sim_info_types
from sims.sim_outfits import OutfitChangeReason
from sims4.localization import TunableLocalizedString, TunableLocalizedStringFactory
from sims4.resources import CompoundTypes
from sims4.tuning.dynamic_enum import DynamicEnum
from sims4.tuning.instances import HashedTunedInstanceMetaclass
from sims4.tuning.tunable import TunableResourceKey, OptionalTunable, TunableReference, TunableList, TunableEnumEntry, TunableSet, TunableMapping, Tunable, HasTunableReference, TunableTuple
from sims4.tuning.tunable_base import ExportModes, SourceQueries, GroupNames
from sims4.utils import classproperty
from traits.trait_voice_effect import VoiceEffectRequest
import enum
import services
import sims4.log
import tag
logger = sims4.log.Logger('Trait', default_owner='cjiang')

def are_traits_conflicting(trait_a, trait_b):
    if trait_a is None or trait_b is None:
        return False
    return trait_a.is_conflicting(trait_b)

def get_possible_traits(age, gender):
    return [trait for trait in services.trait_manager().types.values() if trait.is_valid_trait(age, gender)]

class TraitType(enum.Int):
    __qualname__ = 'TraitType'
    PERSONALITY = 0
    GAMEPLAY = 1
    WALKSTYLE = 2
    HIDDEN = 4
    GHOST = 5
    ASPIRATION = 6

class TraitBuffReplacementPriority(DynamicEnum):
    __qualname__ = 'TraitBuffReplacementPriority'
    NORMAL = 0

class Trait(HasTunableReference, metaclass=HashedTunedInstanceMetaclass, manager=services.trait_manager()):
    __qualname__ = 'Trait'
    EQUIP_SLOT_NUMBER_MAP = TunableMapping(description='\n        The number of personality traits available to Sims of specific ages.\n        ', key_type=TunableEnumEntry(description="\n            The Sim's age.\n            ", tunable_type=sim_info_types.Age, default=sim_info_types.Age.YOUNGADULT), value_type=Tunable(description='\n            The number of personality traits available to a Sim of the specified\n            age.\n            ', tunable_type=int, default=3), key_name='Age', value_name='Slot Number')
    PERSONALITY_TRAIT_TAG = TunableEnumEntry(description='\n        The tag that marks a trait as a personality trait.\n        ', tunable_type=tag.Tag, default=tag.Tag.INVALID)
    INSTANCE_TUNABLES = {'trait_type': TunableEnumEntry(description='\n            The type of the trait.\n            ', tunable_type=TraitType, default=TraitType.PERSONALITY, export_modes=ExportModes.All, tuning_group=GroupNames.APPEARANCE), 'display_name': TunableLocalizedStringFactory(description="\n            The trait's display name. This string is provided with the owning\n            Sim as its only token.\n            ", allow_none=True, export_modes=ExportModes.All, tuning_group=GroupNames.APPEARANCE), 'trait_description': TunableLocalizedString(description="\n            The trait's description.\n            ", allow_none=True, export_modes=ExportModes.All, tuning_group=GroupNames.APPEARANCE), 'trait_origin_description': TunableLocalizedString(description='\n            A description of how the Sim obtained this trait.\n            ', allow_none=True, export_modes=ExportModes.All, tuning_group=GroupNames.APPEARANCE), 'icon': TunableResourceKey(description="\n            The trait's icon.\n            ", allow_none=True, resource_types=CompoundTypes.IMAGE, export_modes=ExportModes.All, tuning_group=GroupNames.APPEARANCE), 'pie_menu_icon': TunableResourceKey(description="\n            The trait's pie menu icon.\n            ", resource_types=CompoundTypes.IMAGE, default=None, allow_none=True, tuning_group=GroupNames.APPEARANCE), 'trait_asm_param': Tunable(description="\n            The ASM parameter for this trait. If unset, it will be auto-\n            generated depending on the instance name (e.g. 'trait_Clumsy').\n            ", tunable_type=str, default=None, tuning_group=GroupNames.ANIMATION), 'ages': TunableSet(description='\n            The allowed ages for this trait. If no ages are specified, then all\n            ages are considered valid.\n            ', tunable=TunableEnumEntry(tunable_type=sim_info_types.Age, default=None, export_modes=ExportModes.All), tuning_group=GroupNames.AVAILABILITY), 'genders': TunableSet(description='\n            The allowed genders for this trait. If no genders are specified,\n            then all genders are considered valid.\n            ', tunable=TunableEnumEntry(tunable_type=sim_info_types.Gender, default=None, export_modes=ExportModes.All), tuning_group=GroupNames.AVAILABILITY), 'conflicting_traits': TunableList(description='\n            Conflicting traits for this trait. If the Sim has any of the\n            specified traits, then they are not allowed to be equipped with this\n            one.\n            \n            e.g.\n             Family Oriented conflicts with Hates Children, and vice-versa.\n            ', tunable=TunableReference(manager=services.trait_manager(), pack_safe=True), export_modes=ExportModes.All, tuning_group=GroupNames.AVAILABILITY), 'is_npc_only': Tunable(description='\n            If checked, this trait will get removed from Sims that have a home\n            when the zone is loaded.\n            ', tunable_type=bool, default=False, tuning_group=GroupNames.AVAILABILITY), 'cas_selected_icon': TunableResourceKey(description='\n            Icon to be displayed in CAS when this trait has already been applied\n            to a Sim.\n            ', resource_types=CompoundTypes.IMAGE, default=None, allow_none=True, export_modes=(ExportModes.ClientBinary,), tuning_group=GroupNames.CAS), 'cas_idle_asm_key': TunableResourceKey(description='\n            The ASM to use for the CAS idle.\n            ', resource_types=(sims4.resources.Types.STATEMACHINE,), default=None, allow_none=True, category='asm', export_modes=ExportModes.All, tuning_group=GroupNames.CAS), 'cas_idle_asm_state': Tunable(description='\n            The state to play for the CAS idle.\n            ', tunable_type=str, default=None, source_location='cas_idle_asm_key', source_query=SourceQueries.ASMState, export_modes=ExportModes.All, tuning_group=GroupNames.CAS), 'cas_trait_asm_param': Tunable(description='\n            The ASM parameter for this trait for use with CAS ASM state machine,\n            driven by selection of this Trait, i.e. when a player selects the a\n            romantic trait, the Flirty ASM is given to the state machine to\n            play. The name tuned here must match the animation state name\n            parameter expected in Swing.\n            ', tunable_type=str, default=None, export_modes=ExportModes.All, tuning_group=GroupNames.CAS), 'tags': TunableList(description="\n            The associated categories of the trait. Need to distinguish among\n            'Personality Traits', 'Achievement Traits' and 'Walkstyle\n            Traits'.\n            ", tunable=TunableEnumEntry(tunable_type=tag.Tag, default=tag.Tag.INVALID), export_modes=ExportModes.All, tuning_group=GroupNames.CAS), 'interactions': OptionalTunable(description='\n            Mixer interactions that are available to Sims equipped with this\n            trait.\n            ', tunable=ContentSet.TunableFactory(locked_args={'phase_affordances': {}, 'phase_tuning': None})), 'buffs': TunableList(description='\n            Buffs that should be added to the Sim whenever this trait is\n            equipped.\n            ', tunable=TunableBuffReference()), 'buffs_proximity': TunableList(description='\n            Proximity buffs that are active when this trait is equipped.\n            ', tunable=TunableReference(manager=services.buff_manager())), 'buff_replacements': TunableMapping(description='\n            A mapping of buff replacement. If Sim has this trait on, whenever he\n            get the buff tuned in the key of the mapping, it will get replaced\n            by the value of the mapping.\n            ', key_type=TunableReference(description='\n                Buff that will get replaced to apply on Sim by this trait.\n                ', manager=services.buff_manager(), reload_dependent=True, pack_safe=True), value_type=TunableTuple(description='\n                Data specific to this buff replacement.\n                ', buff_type=TunableReference(description='\n                    Buff used to replace the buff tuned as key.\n                    ', manager=services.buff_manager(), reload_dependent=True, pack_safe=True), buff_reason=OptionalTunable(description='\n                    If enabled, override the buff reason.\n                    ', tunable=TunableLocalizedString(description='\n                        The overridden buff reason.\n                        ')), buff_replacement_priority=TunableEnumEntry(description="\n                    The priority of this buff replacement, relative to other\n                    replacements. Tune this to be a higher value if you want\n                    this replacement to take precedence.\n                    \n                    e.g.\n                     (NORMAL) trait_HatesChildren (buff_FirstTrimester -> \n                                                   buff_FirstTrimester_HatesChildren)\n                     (HIGH)   trait_Male (buff_FirstTrimester -> \n                                          buff_FirstTrimester_Male)\n                                          \n                     In this case, both traits have overrides on the pregnancy\n                     buffs. However, we don't want males impregnated by aliens\n                     that happen to hate children to lose their alien-specific\n                     buffs. Therefore we tune the male replacement at a higher\n                     priority.\n                    ", tunable_type=TraitBuffReplacementPriority, default=TraitBuffReplacementPriority.NORMAL))), 'excluded_mood_types': TunableList(TunableReference(description='\n            List of moods that are prevented by having this trait.\n            ', manager=services.mood_manager())), 'relbit_replacements': TunableMapping(description='\n            A mapping of bit replacement. If Sim has this trait on, whenever he\n            get the relationship bit tuned in the key of the mapping, it will be\n            replaced by the value of the mapping.\n            ', key_type=TunableReference(description='\n                Relationship bit that will get replaced to apply on Sim by this\n                trait.\n                ', manager=services.relationship_bit_manager(), reload_dependent=True), value_type=TunableReference(description='\n                Relationship bit used to replace bit tuned as key.\n                ', manager=services.relationship_bit_manager(), reload_dependent=True)), 'outfit_replacements': TunableMapping(description="\n            A mapping of outfit replacements. If the Sim has this trait, outfit\n            change requests are intercepted to produce the tuned result. If\n            multiple traits with outfit replacements exist, the behavior is\n            undefined.\n            \n            Tuning 'Invalid' as a key acts as a fallback and applies to all\n            reasons.\n            \n            Tuning 'Invalid' as a value keeps a Sim in their current outfit.\n            ", key_type=TunableEnumEntry(tunable_type=OutfitChangeReason, default=OutfitChangeReason.Invalid), value_type=TunableEnumEntry(tunable_type=OutfitChangeReason, default=OutfitChangeReason.Invalid)), 'can_age_up': Tunable(description='\n            When set, Sims with this trait are allowed to age up. When unset,\n            Sims are prevented from aging up.\n            ', tunable_type=bool, default=True, tuning_group=GroupNames.SPECIAL_CASES), 'can_die': Tunable(description='\n            When set, Sims with this trait are allowed to die. When unset, Sims\n            are prevented from dying.\n            ', tunable_type=bool, default=True, tuning_group=GroupNames.SPECIAL_CASES), 'immune_to_culling': Tunable(description='\n            When set, Sims with this trait are immune from being culled via the\n            story progression action that culls our sim population to stay\n            under cap. \n            \n            E.g. Grim Reaper, Alien Pollinator. These are the only sims in the\n            game that explicitly do not age up or die.\n            ', tunable_type=bool, default=False, tuning_group=GroupNames.SPECIAL_CASES), 'voice_effect': OptionalTunable(description='\n            The voice effect of a Sim with this trait. This is prioritized\n            against other traits with voice effects.\n            \n            The Sim may only have one voice effect at a time.\n            ', tunable=VoiceEffectRequest.TunableFactory()), 'persistable': Tunable(description='\n            If checked then this trait will be saved onto the sim.  If\n            unchecked then the trait will not be saved.\n            Example unchecking:\n            Traits that are applied for the sim being in the region.\n            ', tunable_type=bool, default=True)}
    _asm_param_name = None

    def __repr__(self):
        return '<Trait:({})>'.format(self.__name__)

    def __str__(self):
        return '{}'.format(self.__name__)

    @classmethod
    def _tuning_loaded_callback(cls):
        cls._asm_param_name = cls.trait_asm_param
        if cls._asm_param_name is None:
            cls._asm_param_name = cls.__name__
        for (buff, replacement_buff) in cls.buff_replacements.items():
            if buff.trait_replacement_buffs is None:
                buff.trait_replacement_buffs = {}
            buff.trait_replacement_buffs[cls] = replacement_buff
        for (bit, replacement_bit) in cls.relbit_replacements.items():
            if bit.trait_replacement_bits is None:
                bit.trait_replacement_bits = {}
            bit.trait_replacement_bits[cls] = replacement_bit
        for mood in cls.excluded_mood_types:
            if mood.excluding_traits is None:
                mood.excluding_traits = []
            mood.excluding_traits.append(cls)

    @classproperty
    def asm_param_name(cls):
        return cls._asm_param_name

    @classproperty
    def is_personality_trait(cls):
        return cls.trait_type == TraitType.PERSONALITY

    @classproperty
    def is_aspiration_trait(cls):
        return cls.trait_type == TraitType.ASPIRATION

    @classproperty
    def is_ghost_trait(cls):
        return cls.trait_type == TraitType.GHOST

    @classmethod
    def is_valid_trait(cls, age, gender):
        if cls.ages and age not in cls.ages:
            return False
        if cls.genders and gender not in cls.genders:
            return False
        return True

    @classmethod
    def test_sim_info(cls, sim_info):
        return cls.is_valid_trait(sim_info.age, sim_info.gender)

    @classmethod
    def is_conflicting(cls, trait):
        if trait is None:
            return False
        if cls.conflicting_traits and trait in cls.conflicting_traits:
            return True
        if trait.conflicting_traits and cls in trait.conflicting_traits:
            return True
        return False

    @classmethod
    def get_outfit_change_reason(cls, outfit_change_reason):
        replaced_reason = cls.outfit_replacements.get(outfit_change_reason if outfit_change_reason is not None else OutfitChangeReason.Invalid)
        if replaced_reason is not None:
            return replaced_reason
        if outfit_change_reason is not None:
            replaced_reason = cls.outfit_replacements.get(OutfitChangeReason.Invalid)
            if replaced_reason is not None:
                return replaced_reason
        return outfit_change_reason

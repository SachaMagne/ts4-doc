from event_testing.tests import TunableTestSet
from sims4.tuning.tunable import TunableList, TunableTuple

class _TestedList(tuple):
    __qualname__ = '_TestedList'

    def __call__(self, *, resolver):
        for item_pair in self:
            while item_pair.test.run_tests(resolver):
                yield item_pair.item

class TunableTestedList(TunableList):
    __qualname__ = 'TunableTestedList'

    def __init__(self, *args, tunable_type, **kwargs):
        super().__init__(tunable=TunableTuple(description='\n                An entry in this tested list.\n                ', test=TunableTestSet(), item=tunable_type), *args, **kwargs)

    def load_etree_node(self, *args, **kwargs):
        value = super().load_etree_node(*args, **kwargs)
        return _TestedList(value)

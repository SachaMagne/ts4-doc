from sims4.tuning.tunable import OptionalTunable
from singletons import DEFAULT
from situations.complex.yoga_class import YogaClassScheduleMixin
from situations.situation import Situation
from situations.situation_guest_list import SituationGuestList, SituationGuestInfo, SituationInvitationPurpose
from venues.scheduling_zone_director import SchedulingZoneDirector
import services
import sims4
logger = sims4.log.Logger('RelaxationCenterZoneDirector')

class VisitorSituationOnArrivalZoneDirectorMixin:
    __qualname__ = 'VisitorSituationOnArrivalZoneDirectorMixin'
    INSTANCE_TUNABLES = {'user_sim_arrival_situation': Situation.TunableReference(description='\n            The situation to place all of the Sims from the users household\n            in when they arrive to the Relaxation Center Venue.\n            '), 'travel_companion_arrival_situation': OptionalTunable(description="\n            If enabled then Sims that aren't controllable that travel with the\n            users Sims will be placed in the tuned situation on arrival.\n            ", tunable=Situation.TunableReference(description="\n                If the user invites NPC's to travel with them to this lot then\n                this is the situation that they will be added to.\n                "))}

    def add_sim_info_into_arrival_situation(self, sim_info, situation_type=DEFAULT, during_spin_up=False):
        if situation_type is DEFAULT:
            situation_type = self.user_sim_arrival_situation
        situation_manager = services.get_zone_situation_manager()
        if during_spin_up:
            seeds = situation_manager.get_zone_persisted_seeds_during_zone_spin_up()
            for seed in seeds:
                while sim_info in seed.invited_sim_infos_gen() and seed.situation_type is self.user_sim_arrival_situation and (not services.game_clock_service().time_has_passed_in_world_since_zone_save() or seed.allow_time_jump):
                    return
        guest_list = SituationGuestList(invite_only=True)
        guest_info = SituationGuestInfo.construct_from_purpose(sim_info.id, situation_type.default_job(), SituationInvitationPurpose.INVITED)
        guest_list.add_guest_info(guest_info)
        situation_manager.create_situation(situation_type, guest_list=guest_list, user_facing=False)

    def create_situations_during_zone_spin_up(self):
        super().create_situations_during_zone_spin_up()
        for sim_info in self.get_user_controlled_sim_infos():
            self.add_sim_info_into_arrival_situation(sim_info, during_spin_up=True)
        if self.travel_companion_arrival_situation:
            for sim_info in self._traveled_sim_infos:
                while not sim_info.is_selectable:
                    self.add_sim_info_into_arrival_situation(sim_info, situation_type=self.travel_companion_arrival_situation, during_spin_up=True)

    def handle_sim_summon_request(self, sim_info):
        self.add_sim_info_into_arrival_situation(sim_info)

class RelaxationCenterZoneDirector(YogaClassScheduleMixin, VisitorSituationOnArrivalZoneDirectorMixin, SchedulingZoneDirector):
    __qualname__ = 'RelaxationCenterZoneDirector'

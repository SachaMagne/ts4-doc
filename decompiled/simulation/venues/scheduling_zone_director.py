from sims4.tuning.tunable import TunableSimMinute, TunableList, TunableTuple, TunableEnumEntry
from situations.situation_curve import SituationCurve
from situations.situation_guest_list import SituationGuestList
from zone_director import ZoneDirectorBase
import alarms
import clock
import enum
import services
import sims4.log
logger = sims4.log.Logger('ZoneDirectorScheduling')

class SituationChurnOperation(enum.Int, export=False):
    __qualname__ = 'SituationChurnOperation'
    DO_NOTHING = 0
    START_SITUATION = 1
    REMOVE_SITUATION = 2

class SituationShiftStrictness(enum.Int):
    __qualname__ = 'SituationShiftStrictness'
    DESTROY = 0
    OVERLAP = 1

class SchedulingZoneDirector(ZoneDirectorBase):
    __qualname__ = 'SchedulingZoneDirector'
    INSTANCE_SUBCLASSES_ONLY = True
    INSTANCE_TUNABLES = {'situation_shifts': TunableList(description='\n            New objects will be spawned within this distance of objects that\n            pass the tuned tests for this crime scene change. \n            ', tunable=TunableTuple(shift_curve=SituationCurve.TunableFactory(), shift_strictness=TunableEnumEntry(description='\n                    Determine how situations on shift will be handled on shift\n                    change.\n                    \n                    Example: I want 3 customers between 10-12.  then after 12 I\n                    want only 1.  Having this checked will allow 3 customers to\n                    stay and will let situation duration kick the sim out when\n                    appropriate.  This will not create new situations if over\n                    the cap.\n                    ', tunable_type=SituationShiftStrictness, default=SituationShiftStrictness.DESTROY))), 'churn_alarm_interval': TunableSimMinute(description='\n            Number sim minutes to check to make sure shifts and churn are being accurate.\n            ', default=10)}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._shift_alarm_handles = {}
        self._situation_shifts_to_situation_ids = {}

    def on_startup(self):
        super().on_startup()
        time_of_day = services.time_service().sim_now
        churn_interval = clock.interval_in_sim_minutes(self.churn_alarm_interval)
        for situation_shift in self.situation_shifts:
            alarm_handle = alarms.add_alarm(self, churn_interval, self._shift_churn_alarm_callback, repeating=True)
            self._shift_alarm_handles[alarm_handle] = situation_shift
            time_span = situation_shift.shift_curve.get_timespan_to_next_shift_time(time_of_day)
            alarm_handle = alarms.add_alarm(self, time_span, self._shift_change_alarm_callback)
            self._shift_alarm_handles[alarm_handle] = situation_shift

    def on_shutdown(self):
        for alarm_handle in self._shift_alarm_handles:
            alarms.cancel_alarm(alarm_handle)
        self._shift_alarm_handles.clear()
        situation_manager = services.get_zone_situation_manager()
        for situation_ids in self._situation_shifts_to_situation_ids.values():
            for situation_id in situation_ids:
                situation_manager.destroy_situation_by_id(situation_id)
        self._situation_shifts_to_situation_ids.clear()
        super().on_shutdown()

    def create_situations_during_zone_spin_up(self):
        super().create_situations_during_zone_spin_up()
        for situation_shift in self.situation_shifts:
            self._handle_situation_shift_churn(situation_shift)

    def _shift_change_alarm_callback(self, alarm_handle=None):
        situation_shift = self._shift_alarm_handles.pop(alarm_handle, None)
        if situation_shift is None:
            return
        situation_manager = services.get_zone_situation_manager()
        situation_ids = self._situation_shifts_to_situation_ids.get(situation_shift, [])
        situation_ids = self._prune_stale_situations(situation_ids)
        number_sims_desired_for_shift = situation_shift.shift_curve.get_desired_sim_count_tunable().random_int()
        if situation_shift.shift_strictness == SituationShiftStrictness.DESTROY:
            for situation_id in situation_ids:
                situation_manager.destroy_situation_by_id(situation_id)
            situation_ids.clear()
        elif situation_shift.shift_strictness == SituationShiftStrictness.OVERLAP:
            number_sims_desired_for_shift -= len(situation_ids)
        logger.debug('Situation Shift Change: Adding {}', number_sims_desired_for_shift)
        if number_sims_desired_for_shift > 0:
            for _ in range(number_sims_desired_for_shift):
                situation_to_start = situation_shift.shift_curve.get_situation()
                while situation_to_start is not None and situation_to_start.situation_meets_starting_requirements():
                    logger.debug('Situation Shift Change: Adding situation: {}', situation_to_start)
                    situation_id = situation_manager.create_situation(situation_to_start, guest_list=SituationGuestList(invite_only=True), spawn_sims_during_zone_spin_up=True, user_facing=False)
                    if situation_id is not None:
                        situation_ids.append(situation_id)
        self._situation_shifts_to_situation_ids[situation_shift] = situation_ids
        if alarm_handle is not None:
            time_of_day = services.time_service().sim_now
            time_span = situation_shift.shift_curve.get_timespan_to_next_shift_time(time_of_day)
            alarm_handle = alarms.add_alarm(self, time_span, self._shift_change_alarm_callback)
            self._shift_alarm_handles[alarm_handle] = situation_shift

    def _shift_churn_alarm_callback(self, alarm_handle):
        situation_shift = self._shift_alarm_handles.get(alarm_handle, None)
        if situation_shift is None:
            return
        self._handle_situation_shift_churn(situation_shift)

    def _handle_situation_shift_churn(self, situation_shift):
        number_situations_desired = situation_shift.shift_curve.get_desired_sim_count_tunable()
        situation_ids = self._situation_shifts_to_situation_ids.get(situation_shift, [])
        situation_ids = self._prune_stale_situations(situation_ids)
        op = SituationChurnOperation.DO_NOTHING
        situation_to_start = None
        if number_situations_desired.lower_bound > len(situation_ids):
            situation_to_start = situation_shift.shift_curve.get_situation()
            if situation_to_start is not None:
                if situation_to_start.situation_meets_starting_requirements():
                    op = SituationChurnOperation.START_SITUATION
        elif number_situations_desired.upper_bound < len(situation_ids):
            op = SituationChurnOperation.REMOVE_SITUATION
        situation_manager = services.get_zone_situation_manager()
        if op == SituationChurnOperation.START_SITUATION:
            if situation_to_start is not None:
                situation_id = situation_manager.create_situation(situation_to_start, guest_list=SituationGuestList(invite_only=True), spawn_sims_during_zone_spin_up=True, user_facing=False)
                if situation_id is not None:
                    situation_ids.append(situation_id)
            self._situation_shifts_to_situation_ids[situation_shift] = situation_ids
        elif op == SituationChurnOperation.REMOVE_SITUATION:
            time_of_day = services.time_service().sim_now
            situations = [situation_manager.get(situation_id) for situation_id in situation_ids]
            situations.sort(key=lambda x: time_of_day - x.situation_start_time, reverse=True)
            situation_to_remove = next(iter(situations))
            if situation_to_remove is not None:
                logger.debug('Situation Churn Alarm Callback: Removing Situation: {}', situation_to_remove)
                situation_manager.destroy_situation_by_id(situation_to_remove.id)

    def _save_custom_zone_director(self, zone_director_proto, writer):
        for (index, situation_shift) in enumerate(self.situation_shifts):
            situation_ids = self._situation_shifts_to_situation_ids.get(situation_shift, [])
            situation_ids = self._prune_stale_situations(situation_ids)
            while situation_ids:
                situation_data_proto = zone_director_proto.situations.add()
                situation_data_proto.situation_list_guid = index
                situation_data_proto.situation_ids.extend(situation_ids)
        super()._save_custom_zone_director(zone_director_proto, writer)

    def _load_custom_zone_director(self, zone_director_proto, reader):
        for situation_data_proto in zone_director_proto.situations:
            situation_shift = self.situation_shifts[situation_data_proto.situation_list_guid]
            situation_ids = []
            situation_ids.extend(situation_data_proto.situation_ids)
            self._situation_shifts_to_situation_ids[situation_shift] = situation_ids
        super()._load_custom_zone_director(zone_director_proto, reader)

from sims4.tuning.dynamic_enum import DynamicEnum
import enum

class ZoneDirectorRequestType(enum.Int, export=False):
    __qualname__ = 'ZoneDirectorRequestType'
    CAREER_EVENT = Ellipsis
    AMBIENT_VENUE = Ellipsis

class NPCSummoningPurpose(DynamicEnum):
    __qualname__ = 'NPCSummoningPurpose'
    DEFAULT = 0
    PLAYER_BECOMES_GREETED = 1
    BRING_PLAYER_SIM_TO_LOT = 2

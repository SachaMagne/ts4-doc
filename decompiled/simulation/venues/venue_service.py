import random
from protocolbuffers import GameplaySaveData_pb2 as gameplay_serialization
from build_buy import get_current_venue
from sims4.callback_utils import CallableList
from sims4.service_manager import Service
from sims4.tuning.tunable import TunableSimMinute
from situations.service_npcs.modify_lot_items_tuning import ModifyAllLotItems
from venues.venue_constants import ZoneDirectorRequestType
import alarms
import build_buy
import clock
import services
import sims4.log
import sims4.resources
import zone_director
try:
    import _zone
except ImportError:

    class _zone:
        __qualname__ = '_zone'

logger = sims4.log.Logger('Venue', default_owner='manus')

class VenueService(Service):
    __qualname__ = 'VenueService'
    SPECIAL_EVENT_SCHEDULE_DELAY = TunableSimMinute(description='\n        Number of real time seconds to wait after the loading screen before scheduling\n        special events.\n        ', default=10.0)
    VENUE_CLEANUP_ACTIONS = ModifyAllLotItems.TunableFactory()
    ELAPSED_TIME_SINCE_LAST_VISIT_FOR_CLEANUP = TunableSimMinute(description='\n        If more than this amount of sim minutes has elapsed since the lot was\n        last visited, the auto cleanup will happen.\n        ', default=720, minimum=0)

    def __init__(self):
        self._persisted_background_event_id = None
        self._persisted_special_event_id = None
        self._special_event_start_alarm = None
        self._venue = None
        self._zone_director = None
        self._requested_zone_directors = []
        self._prior_zone_director_proto = None
        self.build_buy_edit_mode = False
        self.on_venue_type_changed = CallableList()

    @property
    def venue(self):
        return self._venue

    def _set_venue(self, venue_type):
        if venue_type is None:
            logger.error('Zone {} has invalid venue type.', services.current_zone().id)
            return False
        if type(self._venue) is venue_type:
            return False
        if self._venue is not None:
            self._venue.shut_down()
            if self._special_event_start_alarm is not None:
                alarms.cancel_alarm(self._special_event_start_alarm)
                self._special_event_start_alarm = None
        new_venue = venue_type()
        self._venue = new_venue
        return True

    def _get_venue_tuning(self, zone):
        venue_tuning = None
        venue_type = get_current_venue(zone.id)
        if venue_type is not None:
            venue_tuning = services.venue_manager().get(venue_type)
        return venue_tuning

    def change_venue_type_at_runtime(self, venue_type):
        if self.build_buy_edit_mode:
            return
        type_changed = self._set_venue(venue_type)
        if type_changed and self._venue is not None:
            zone_director = self._venue.create_zone_director_instance()
            self.change_zone_director(zone_director, run_cleanup=True)
            if self._zone_director.should_create_venue_background_situation:
                self._venue.schedule_background_events(schedule_immediate=True)
                self._venue.schedule_special_events(schedule_immediate=False)
            self.on_venue_type_changed()

    def make_venue_type_zone_director_request(self):
        if self._venue is None:
            raise RuntimeError('Venue type must be determined before requesting a zone director.')
        zone_director = self._venue.create_zone_director_instance()
        self.request_zone_director(zone_director, ZoneDirectorRequestType.AMBIENT_VENUE)

    def setup_lot_premade_status(self):
        services.active_lot().flag_as_premade(True)

    def _select_zone_director(self):
        if self._requested_zone_directors is None:
            raise RuntimeError('Cannot select a zone director twice')
        if not self._requested_zone_directors:
            raise RuntimeError('At least one zone director must be requested')
        requested_zone_directors = self._requested_zone_directors
        self._requested_zone_directors = None
        requested_zone_directors.sort()
        (_, zone_director, preserve_state) = requested_zone_directors[0]
        self._set_zone_director(zone_director, True)
        if self._prior_zone_director_proto:
            self._zone_director.load(self._prior_zone_director_proto, preserve_state=preserve_state)
            self._prior_zone_director_proto = None

    @property
    def has_zone_director(self):
        return self._zone_director is not None

    def get_zone_director(self):
        return self._zone_director

    def request_zone_director(self, zone_director, request_type, preserve_state=True):
        if self._requested_zone_directors is None:
            raise RuntimeError('Cannot request a new zone director after one has been selected.')
        if zone_director is None:
            raise ValueError('Cannot request a None zone director.')
        for (prior_request_type, prior_zone_director, _) in self._requested_zone_directors:
            while prior_request_type == request_type:
                raise ValueError('Multiple requests for zone directors with the same request type {}.  Original: {} New: {}'.format(request_type, prior_zone_director, zone_director))
        self._requested_zone_directors.append((request_type, zone_director, preserve_state))

    def change_zone_director(self, zone_director, run_cleanup):
        if self._zone_director is None:
            raise RuntimeError('Cannot request a new zone director before one has been selected.')
        if self._zone_director is zone_director:
            raise ValueError('Attempting to change zone director to the same instance')
        self._set_zone_director(zone_director, run_cleanup)

    def _set_zone_director(self, zone_director, run_cleanup):
        if self._zone_director is not None:
            if run_cleanup:
                self._zone_director.process_cleanup_actions()
            else:
                for cleanup_action in self._zone_director._cleanup_actions:
                    zone_director.add_cleanup_action(cleanup_action)
            self._zone_director.on_shutdown()
        self._zone_director = zone_director
        if self._zone_director is not None:
            self._zone_director.on_startup()

    def determine_which_situations_to_load(self):
        self._zone_director.determine_which_situations_to_load()

    def on_client_connect(self, client):
        zone = services.current_zone()
        venue_type = get_current_venue(zone.id)
        logger.assert_raise(venue_type is not None, 'Venue Type is None in on_client_connect for zone:{}', zone, owner='sscholl')
        venue_tuning = self._get_venue_tuning(zone)
        if venue_tuning is not None:
            self._set_venue(venue_tuning)

    def on_cleanup_zone_objects(self, client):
        zone = services.current_zone()
        if client.household_id != zone.lot.owner_household_id:
            time_elapsed = services.game_clock_service().time_elapsed_since_last_save()
            if time_elapsed.in_minutes() > self.ELAPSED_TIME_SINCE_LAST_VISIT_FOR_CLEANUP:
                cleanup = VenueService.VENUE_CLEANUP_ACTIONS()
                cleanup.modify_objects_on_active_lot()

    def stop(self):
        if self.build_buy_edit_mode:
            return
        self._set_zone_director(None, True)

    def create_situations_during_zone_spin_up(self):
        self._zone_director.create_situations_during_zone_spin_up()
        self.initialize_venue_background_schedule()

    def handle_active_lot_changing_edge_cases(self):
        self._zone_director.handle_active_lot_changing_edge_cases()

    def initialize_venue_background_schedule(self):
        if not self._zone_director.should_create_venue_background_situation:
            return
        if self._venue is not None:
            self._venue.set_active_event_ids(self._persisted_background_event_id, self._persisted_special_event_id)
            situation_manager = services.current_zone().situation_manager
            schedule_immediate = self._persisted_background_event_id is None or self._persisted_background_event_id not in situation_manager
            self._venue.schedule_background_events(schedule_immediate=schedule_immediate)

    def process_traveled_and_persisted_and_resident_sims_during_zone_spin_up(self, traveled_sim_infos, zone_saved_sim_infos, open_street_saved_sim_infos, injected_into_zone_sim_infos):
        self._zone_director.process_traveled_and_persisted_and_resident_sims(traveled_sim_infos, zone_saved_sim_infos, open_street_saved_sim_infos, injected_into_zone_sim_infos)

    def setup_special_event_alarm(self):
        special_event_time_span = clock.interval_in_sim_minutes(self.SPECIAL_EVENT_SCHEDULE_DELAY)
        self._special_event_start_alarm = alarms.add_alarm(self, special_event_time_span, self._schedule_venue_special_events, repeating=False)

    def _schedule_venue_special_events(self, alarm_handle):
        if self._venue is not None:
            self._venue.schedule_special_events(schedule_immediate=True)

    def has_zone_for_venue_type(self, venue_types):
        venue_manager = services.get_instance_manager(sims4.resources.Types.VENUE)
        for neighborhood_proto in services.get_persistence_service().get_neighborhoods_proto_buf_gen():
            for lot_owner_info in neighborhood_proto.lots:
                zone_id = lot_owner_info.zone_instance_id
                while zone_id is not None:
                    venue_type_id = build_buy.get_current_venue(zone_id)
                    venue_type = venue_manager.get(venue_type_id)
                    if venue_type and venue_type in venue_types:
                        return True
        return False

    def get_zones_for_venue_type(self, venue_type):
        possible_zones = []
        venue_manager = services.get_instance_manager(sims4.resources.Types.VENUE)
        for neighborhood_proto in services.get_persistence_service().get_neighborhoods_proto_buf_gen():
            for lot_owner_info in neighborhood_proto.lots:
                zone_id = lot_owner_info.zone_instance_id
                while zone_id is not None:
                    venue_type_id = build_buy.get_current_venue(zone_id)
                    if venue_manager.get(venue_type_id) is venue_type:
                        possible_zones.append(lot_owner_info.zone_instance_id)
        return possible_zones

    def get_zone_and_venue_type_for_venue_types(self, venue_types):
        possible_zones = []
        for venue_type in venue_types:
            venue_zones = self.get_zones_for_venue_type(venue_type)
            for zone in venue_zones:
                possible_zones.append((zone, venue_type))
        if possible_zones:
            return random.choice(possible_zones)
        if possible_zones:
            (selected_zone, venue_type) = random.choice(possible_zones)
            current_zone = services.current_zone()
            zone_id = _zone.create_venue(current_zone.id, selected_zone.household_description, venue_type.guid64, current_zone.neighborhood_id, 'maxis_lot')
            return (zone_id, venue_type)
        return (None, None)

    def save(self, zone_data=None, **kwargs):
        if zone_data is not None and self._venue is not None:
            venue_data = zone_data.gameplay_zone_data.venue_data
            if self._venue.active_background_event_id is not None:
                venue_data.background_situation_id = self._venue.active_background_event_id
            if self._venue.active_special_event_id is not None:
                venue_data.special_event_id = self._venue.active_special_event_id
            if self._zone_director is not None:
                zone_director_data = gameplay_serialization.ZoneDirectorData()
                self._zone_director.save(zone_director_data)
                venue_data.zone_director = zone_director_data

    def load(self, zone_data=None, **kwargs):
        if zone_data is not None and zone_data.HasField('gameplay_zone_data') and zone_data.gameplay_zone_data.HasField('venue_data'):
            venue_data = zone_data.gameplay_zone_data.venue_data
            if venue_data.HasField('background_situation_id'):
                self._persisted_background_event_id = venue_data.background_situation_id
            if venue_data.HasField('special_event_id'):
                self._persisted_special_event_id = venue_data.special_event_id
            if hasattr(gameplay_serialization, 'ZoneDirectorData'):
                if venue_data.HasField('zone_director'):
                    self._prior_zone_director_proto = gameplay_serialization.ZoneDirectorData()
                    self._prior_zone_director_proto.CopyFrom(venue_data.zone_director)

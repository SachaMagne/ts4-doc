from aspirations.aspiration_tuning import AspirationBasic
from aspirations.aspiration_types import AspriationType
from event_testing import objective_tuning
from event_testing.resolver import DoubleSimResolver
from interactions import ParticipantType
from sims import genealogy_tracker
from sims4.tuning.instances import lock_instance_tunables
from sims4.tuning.tunable import TunableEnumEntry, TunableReference
from situations.situation_goal import TunableWeightedSituationGoalReference
from statistics.commodity import RuntimeCommodity, CommodityTimePassageFixupType
import event_testing
import services
import sims4.tuning.tunable
logger = sims4.log.Logger('Whimset', default_owner='jjacobson')

class GeneTargetFactory(sims4.tuning.tunable.TunableFactory):
    __qualname__ = 'GeneTargetFactory'

    @staticmethod
    def factory(sim_info, relationship):
        family_member_sim_id = sim_info.get_relation(relationship)
        if family_member_sim_id is None:
            return
        family_member_sim_info = services.sim_info_manager().get(family_member_sim_id)
        if family_member_sim_info is not None and (family_member_sim_info.is_baby or family_member_sim_info.is_instanced()):
            return family_member_sim_info

    FACTORY_TYPE = factory

    def __init__(self, **kwargs):
        super().__init__(description='\n            This option tests for completion of a tuned Achievement.\n            ', relationship=TunableEnumEntry(genealogy_tracker.FamilyRelationshipIndex, genealogy_tracker.FamilyRelationshipIndex.FATHER), **kwargs)

class RelationTargetFactory(sims4.tuning.tunable.TunableFactory):
    __qualname__ = 'RelationTargetFactory'

    @staticmethod
    def factory(sim_info, relationship_test):
        relationship_match = None
        for relation in sim_info.relationship_tracker:
            relation_sim_info = services.sim_info_manager().get(relation.relationship_id)
            while relation_sim_info is not None and (relation_sim_info.is_baby or relation_sim_info.is_instanced()):
                resolver = DoubleSimResolver(sim_info, relation_sim_info)
                relationship_match = resolver(relationship_test)
                if relationship_match:
                    return relation_sim_info

    FACTORY_TYPE = factory

    def __init__(self, **kwargs):
        super().__init__(description='\n            This option tests for completion of a tuned Achievement.\n            ', relationship_test=event_testing.test_variants.TunableRelationshipTest(description='\n                The relationship state that this goal will complete when\n                obtained.\n                ', locked_args={'subject': ParticipantType.Actor, 'tooltip': None, 'target_sim': ParticipantType.TargetSim, 'num_relations': 0}), **kwargs)

class AspirationWhimSet(AspirationBasic):
    __qualname__ = 'AspirationWhimSet'
    INSTANCE_TUNABLES = {'objectives': sims4.tuning.tunable.TunableList(description='\n            A Set of objectives for completing an aspiration.', tunable=sims4.tuning.tunable.TunableReference(description='\n                One objective for an aspiration', manager=services.get_instance_manager(sims4.resources.Types.OBJECTIVE))), 'force_target': sims4.tuning.tunable.OptionalTunable(description='\n            Upon WhimSet activation, use this option to seek out and set a specific target for this set.\n            If the desired target does not exist or is not instanced on the lot, WhimSet will not activate.\n            ', tunable=sims4.tuning.tunable.TunableVariant(genealogy_target=GeneTargetFactory(), relationship_target=RelationTargetFactory(), default='genealogy_target')), 'whims': sims4.tuning.tunable.TunableList(description='\n            List of weighted goals.', tunable=TunableWeightedSituationGoalReference(pack_safe=True)), 'connected_whims': sims4.tuning.tunable.TunableMapping(description='\n            A tunable list of whims that upon a goal from this list succeeding will activate.', key_type=TunableReference(services.get_instance_manager(sims4.resources.Types.SITUATION_GOAL), description='The goal to map.'), value_type=sims4.tuning.tunable.TunableList(description='\n                A tunable list of whim sets that upon this whim goal completing will activate', tunable=sims4.tuning.tunable.TunableReference(description='\n                    These Aspiration Whim Sets become active automatically upon completion of this whim.', manager=services.get_instance_manager(sims4.resources.Types.ASPIRATION), class_restrictions='AspirationWhimSet'))), 'connected_whim_sets': sims4.tuning.tunable.TunableList(description='\n            A tunable list of whim sets that upon a goal from this list succeeding will activate', tunable=sims4.tuning.tunable.TunableReference(description='\n                These Aspiration Whim Sets become active automatically upon completion of a whim from this set.', manager=services.get_instance_manager(sims4.resources.Types.ASPIRATION), class_restrictions='AspirationWhimSet')), 'cooldown_timer': sims4.tuning.tunable.TunableRange(description='\n            Number of Sim minutes this set of Whims is de-prioritized after de-activation.', tunable_type=float, minimum=0, maximum=3600, default=60), 'disabled': sims4.tuning.tunable.Tunable(description='\n            Checking this box will remove this Aspiration from the event system and the UI, but preserve the tuning.', tunable_type=bool, default=False), 'activated_priority': sims4.tuning.tunable.TunableRange(description='\n            Priority for this set to be chosen if triggered by contextual events.', tunable_type=int, minimum=0, maximum=10, default=6), 'chained_priority': sims4.tuning.tunable.TunableRange(description='\n            Priority for this set to be chosen if triggered by a previous whim set.', tunable_type=int, minimum=0, maximum=15, default=11), 'priority_decay_rate': sims4.tuning.tunable.TunableRange(description="\n            The decay rate of a whimset's priority.  A whimset's priority will\n            only decay when a whim of that whimset is active.  A whimset's\n            priority will converge to the whimset's base priority.\n            ", tunable_type=float, default=0.01, minimum=0.0), 'timeout_retest': sims4.tuning.tunable.TunableReference(description='\n            Tuning an objective here will re-test the WhimSet for contextual\n            relevance upon active timer timeout; If the objective test passes,\n            the active timer will be refreshed. Note you can only use tests\n            without data passed in, other types will result in an assert on\n            load.\n            ', manager=services.get_instance_manager(sims4.resources.Types.OBJECTIVE), allow_none=True), 'whimset_emotion': sims4.tuning.tunable.OptionalTunable(description='\n            Setting this field sets whims in this whimset as emotional, indicating unique \n            emotional behavior and UI treatment in the whim system.', tunable=sims4.tuning.tunable.TunableReference(description='\n                The emotion associated with this whim set.', manager=services.get_instance_manager(sims4.resources.Types.MOOD))), 'whim_reason': sims4.localization.TunableLocalizedStringFactory(description='\n            The reason that shows in the whim tooltip for the reason that this\n            whim was chosen for the sim.\n            ')}
    priority_commodity = None

    @classmethod
    def _tuning_loaded_callback(cls):
        commodity = RuntimeCommodity.generate(cls.__name__)
        commodity.decay_rate = cls.priority_decay_rate
        commodity.convergence_value = 0
        commodity.remove_on_convergence = False
        commodity.visible = False
        if cls.activated_priority > cls.chained_priority:
            commodity.max_value_tuning = cls.activated_priority
        else:
            commodity.max_value_tuning = cls.chained_priority
        commodity.min_value_tuning = 0
        commodity.initial_value = 0
        commodity._time_passage_fixup_type = CommodityTimePassageFixupType.DO_NOT_FIXUP
        cls.priority_commodity = commodity

    @classmethod
    def aspiration_type(cls):
        return AspriationType.WHIM_SET

    @classmethod
    def _verify_tuning_callback(cls):
        if cls.activated_priority == 0 and cls.chained_priority == 0:
            logger.error('No priority tuned for value greater than 0 in {}', cls)
        if cls.objectives:
            logger.error('Emotional Whimset {} has tuned objectives.  This will cause incorrect behavior with the emotional whim not appearing in the emotional whim slot.', cls)
        if cls.whimset_emotion is not None and cls.timeout_retest is not None:
            logger.error('Emotional Whimset {} has timeout retest which can cause the set to try and become actually active and cause emotional whims to appear outside of the emotional whim slot.', cls)
        for objective in cls.objectives:
            pass
        for whim in cls.whims:
            pass
        if cls.timeout_retest is not None:
            pass

lock_instance_tunables(AspirationWhimSet, complete_only_in_sequence=False)
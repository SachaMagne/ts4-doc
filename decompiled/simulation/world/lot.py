from collections import defaultdict
import weakref
from protocolbuffers import Consts_pb2, UI_pb2
from distributor.system import Distributor
from objects.components import ComponentContainer
from objects.components.inventory import SharedInventoryContainer
from objects.components.inventory_enums import InventoryType
from objects.components.inventory_item import ItemLocation
from objects.components.statistic_component import HasStatisticComponent
from objects.system import create_object
from sims4.math import vector_normalize
from world.lot_tuning import GlobalLotTuningAndCleanup, LotTuningMaps
from world.premade_lot_status import PremadeLotStatus
import distributor
import services
import sims4.log
try:
    import _lot
except ImportError:

    class _lot:
        __qualname__ = '_lot'

        @staticmethod
        def get_lot_id_from_instance_id(*_, **__):
            return 0

        class Lot:
            __qualname__ = '_lot.Lot'

get_lot_id_from_instance_id = _lot.get_lot_id_from_instance_id
logger = sims4.log.Logger('Lot')

class Lot(ComponentContainer, HasStatisticComponent, _lot.Lot):
    __qualname__ = 'Lot'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.inventory_owners = defaultdict(weakref.WeakSet)
        self._front_door_id = None
        self._hidden_inventory = None

    @property
    def is_sim(self):
        return False

    @property
    def is_downloaded(self):
        return False

    @property
    def center(self):
        return self.position

    @property
    def front_door_id(self):
        return self._front_door_id

    @front_door_id.setter
    def front_door_id(self, value):
        self._front_door_id = value

    def get_front_door(self):
        if self._front_door_id:
            return services.object_manager().get(self._front_door_id)

    def get_default_position(self, position=None):
        front_door = self.get_front_door()
        if front_door is not None:
            default_position = front_door.position
        elif position is not None:
            default_position = min(self.corners, key=lambda p: (p - position).magnitude_squared())
        else:
            default_position = self.corners[0]
        return default_position + vector_normalize(self.position - default_position)

    def get_hidden_inventory(self):
        if self._hidden_inventory is None:
            self._hidden_inventory = self.get_object_inventories(InventoryType.HIDDEN)[0]
        return self._hidden_inventory

    def create_object_in_hidden_inventory(self, definition_id):
        inventory = self.get_hidden_inventory()
        obj = create_object(definition_id, loc_type=ItemLocation.OBJECT_INVENTORY)
        try:
            inventory.system_add_object(obj, None)
            return obj
        except:
            obj.destroy(source=self, cause='Failed to place object in hidden inventory.')

    def get_mailbox_inventory(self):
        return self.get_object_inventories(InventoryType.MAILBOX)[0]

    def create_object_in_mailbox(self, definition_id):
        inventory = self.get_mailbox_inventory()
        if inventory is None:
            return
        obj = create_object(definition_id, loc_type=ItemLocation.OBJECT_INVENTORY)
        try:
            inventory.system_add_object(obj, None)
            return obj
        except:
            obj.destroy(source=self, cause='Failed to place object in mailbox.')

    def get_object_inventories(self, inv_type):
        inventory_owners = self.inventory_owners[inv_type]
        if not inventory_owners and inv_type.is_shared_between_objects:
            owner = SharedInventoryContainer(inv_type)
            inventory_owners.add(owner)
        return [inventory_owner.inventory_component for inventory_owner in inventory_owners]

    def get_all_object_inventories_gen(self, shared_only=False):
        for (inventory_type, inventory_owners) in self.inventory_owners.items():
            if shared_only and not inventory_type.is_shared_between_objects:
                pass
            for inventory_owner in inventory_owners:
                yield (inventory_type, inventory_owner.inventory_component)

    def publish_inventory_items(self):
        distributor = Distributor.instance()
        for (inventory_type, inventory_owners) in self.inventory_owners.items():
            if inventory_type == InventoryType.HIDDEN:
                pass
            for inventory_owner in inventory_owners:
                for (obj, message_op) in inventory_owner.inventory_component.get_item_update_ops_gen():
                    distributor.add_op(obj, message_op)
                inventory_owner.update_inventory_count()

    def get_lot_name(self):
        persistence = services.get_persistence_service()
        lot_owner_data = persistence.get_lot_proto_buff(self.lot_id)
        lot_name = None
        if lot_owner_data is not None:
            zone_data = persistence.get_zone_proto_buff(lot_owner_data.zone_instance_id)
            if zone_data is not None:
                lot_name = zone_data.name
        return lot_name

    def send_lot_display_info(self):
        lot_name = self.get_lot_name()
        household = self.get_household()
        if household is not None:
            owner_household_name = household.name
        else:
            owner_household_name = None
        msg = UI_pb2.LotDisplayInfo()
        if lot_name is not None:
            msg.lot_name = lot_name
        if owner_household_name is not None:
            msg.household_name = owner_household_name
        op = distributor.shared_messages.create_message_op(msg, Consts_pb2.MSG_UI_LOT_DISPLAY_INFO)
        Distributor.instance().add_op_with_no_owner(op)

    def get_household(self):
        return services.household_manager().get(self.owner_household_id)

    def _should_track_premade_status(self):
        lot_tuning = LotTuningMaps.get_lot_tuning()
        if lot_tuning is None:
            return False
        return lot_tuning.track_premade_status

    def get_premade_status(self):
        if self._should_track_premade_status():
            save_game_data = services.get_persistence_service().get_save_game_data_proto()
            premade_lot_status = save_game_data.gameplay_data.premade_lot_status
            for lot_data in premade_lot_status:
                while lot_data.lot_id == self.lot_id:
                    if lot_data.is_premade:
                        return PremadeLotStatus.IS_PREMADE
                    return PremadeLotStatus.NOT_PREMADE
        return PremadeLotStatus.NOT_TRACKED

    def flag_as_premade(self, is_premade):
        if not self._should_track_premade_status():
            return
        save_game_data = services.get_persistence_service().get_save_game_data_proto()
        premade_lot_status = save_game_data.gameplay_data.premade_lot_status
        for lot_data in premade_lot_status:
            while lot_data.lot_id == self.lot_id:
                if lot_data.is_premade == False:
                    return
                lot_data.is_premade = is_premade
                return
        lot_data = premade_lot_status.add()
        lot_data.lot_id = self.lot_id
        lot_data.is_premade = is_premade

    def save(self, gameplay_zone_data, is_instantiated=True):
        gameplay_zone_data.ClearField('commodity_tracker')
        gameplay_zone_data.ClearField('statistics_tracker')
        gameplay_zone_data.ClearField('skill_tracker')
        if is_instantiated:
            GlobalLotTuningAndCleanup.calculate_object_quantity_statistic_values(self)
        self.update_all_commodities()
        (commodites, skill_statistics) = self.commodity_tracker.save()
        gameplay_zone_data.commodity_tracker.commodities.extend(commodites)
        regular_statistics = self.statistic_tracker.save()
        gameplay_zone_data.statistics_tracker.statistics.extend(regular_statistics)
        gameplay_zone_data.skill_tracker.skills.extend(skill_statistics)

    def load(self, gameplay_zone_data):
        self.commodity_tracker.load(gameplay_zone_data.commodity_tracker.commodities)
        self.statistic_tracker.load(gameplay_zone_data.statistics_tracker.statistics)
        self.commodity_tracker.load(gameplay_zone_data.skill_tracker.skills)

import enum

class PremadeLotStatus(enum.Int):
    __qualname__ = 'PremadeLotStatus'
    NOT_TRACKED = 0
    IS_PREMADE = 1
    NOT_PREMADE = 2
    export = False

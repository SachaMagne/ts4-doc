from sims4.localization import TunableLocalizedString
from sims4.tuning.instances import HashedTunedInstanceMetaclass
from sims4.tuning.tunable import HasTunableReference, TunableMapping, TunableRegionDescription, TunableReference, TunableList, Tunable, TunableEnumEntry
from sims4.tuning.tunable_base import ExportModes
import services
import sims4.log
import tag
logger = sims4.log.Logger('Region')

class Region(HasTunableReference, metaclass=HashedTunedInstanceMetaclass, manager=services.region_manager()):
    __qualname__ = 'Region'
    REGION_DESCRIPTION_TUNING_MAP = TunableMapping(description='\n        A mapping between Catalog region description and tuning instance. This\n        way we can find out what region description the current zone belongs to\n        at runtime then grab its tuning instance.\n        ', key_type=TunableRegionDescription(description='\n            Catalog-side Region Description.\n            ', pack_safe=True, export_modes=ExportModes.All), value_type=TunableReference(description="\n            Region Tuning instance. This is retrieved at runtime based on what\n            the active zone's region description is.\n            ", pack_safe=True, manager=services.region_manager(), export_modes=ExportModes.All), key_name='RegionDescription', value_name='Region', tuple_name='RegionDescriptionMappingTuple', export_modes=ExportModes.All)
    INSTANCE_TUNABLES = {'gallery_download_venue_map': TunableMapping(description='\n            A map from gallery venue to instanced venue. We need to be able to\n            convert gallery venues into other venues that are only compatible\n            with that region.\n            ', key_type=TunableReference(description='\n                A venue type that exists in the gallery.\n                ', manager=services.venue_manager(), export_modes=ExportModes.All), value_type=TunableReference(description='\n                The venue type that the gallery venue will become when it is\n                downloaded into this region.\n                ', manager=services.venue_manager(), export_modes=ExportModes.All, pack_safe=True), key_name='gallery_venue_type', value_name='region_venue_type', export_modes=ExportModes.All), 'compatible_venues': TunableList(description='\n            A list of venues that are allowed to be set by the player in this\n            region.\n            ', tunable=TunableReference(description='\n                A venue that the player can set in this region.\n                ', manager=services.venue_manager(), export_modes=ExportModes.All, pack_safe=True), export_modes=ExportModes.All), 'tags': TunableList(description='\n            Tags that are used to group regions. Destination Regions will\n            likely have individual tags, but Home/Residential Regions will\n            share a tag.\n            ', tunable=TunableEnumEntry(description='\n                A Tag used to group this region. Destination Regions will\n                likely have individual tags, but Home/Residential Regions will\n                share a tag.\n                ', tunable_type=tag.Tag, default=tag.Tag.INVALID, pack_safe=True)), 'region_buffs': TunableList(description='\n            A list of buffs that are added on Sims while they are instanced in\n            this region.\n            ', tunable=TunableReference(description='\n                A buff that exists on Sims while they are instanced in this\n                region.\n                ', manager=services.buff_manager(), pack_safe=True)), 'store_travel_group_placed_objects': Tunable(description='\n            If checked, any placed objects while in a travel group will be returned to household inventory once\n            travel group is disbanded.\n            ', tunable_type=bool, default=False), 'travel_group_build_disabled_tooltip': TunableLocalizedString(description='\n            The string that will appear in the tooltip of the grayed out build\n            mode button if build is being disabled because of a travel group in\n            this region.\n            ', allow_none=True, export_modes=ExportModes.All)}

    @classmethod
    def _cls_repr(cls):
        return "Region: <class '{}.{}'>".format(cls.__module__, cls.__name__)

    @classmethod
    def is_region_compatible(cls, region_instance):
        if region_instance is cls or region_instance is None:
            return True
        for tag in cls.tags:
            while tag in region_instance.tags:
                return True
        return False

    @classmethod
    def is_sim_info_compatible(cls, sim_info):
        other_region = get_region_instance_from_zone_id(sim_info.zone_id)
        if cls.is_region_compatible(other_region):
            return True
        travel_group_id = sim_info.travel_group_id
        if travel_group_id:
            travel_group = services.travel_group_manager().get(travel_group_id)
            if travel_group is not None and not travel_group.played:
                return True
        return False

def get_region_instance_from_zone_id(zone_id):
    neighborhood_proto = services.get_persistence_service().get_neighborhood_proto_buf_from_zone_id(zone_id)
    if neighborhood_proto is None:
        return
    region_description_id = neighborhood_proto.region_id
    region_instance = Region.REGION_DESCRIPTION_TUNING_MAP.get(region_description_id)
    return region_instance

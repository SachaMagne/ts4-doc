from interactions.context import InteractionContext, InteractionSource
from interactions.priority import Priority
from sims4.tuning.tunable import AutoFactoryInit, HasTunableSingletonFactory, TunableVariant, TunableReference
import services
import sims4.log
logger = sims4.log.Logger('Sim Spawner')

class SpawnActionFadeIn:
    __qualname__ = 'SpawnActionFadeIn'

    def __call__(self, sim):
        sim.fade_in()
        return True

class SpawnActionAffordance(HasTunableSingletonFactory, AutoFactoryInit):
    __qualname__ = 'SpawnActionAffordance'
    FACTORY_TUNABLES = {'spawn_affordance': TunableReference(description='\n            The affordance that is pushed on the Sim as soon as they are spawned\n            on the lot.\n            ', manager=services.affordance_manager(), class_restrictions=('SuperInteraction',))}

    def __call__(self, sim):

        def verify_opacity():
            if not sim.opacity:
                logger.error('{} failed to make {} visible. Fading them in.', self.spawn_affordance, sim)
                sim.fade_in()

        context = InteractionContext(sim, InteractionSource.SCRIPT, Priority.Critical)
        result = sim.push_super_affordance(self.spawn_affordance, None, context, exit_functions=(verify_opacity,))
        if not result:
            logger.error('{} failed to run, with result {}. Fading {} in.', self.spawn_affordance, result, sim)
            sim.fade_in()
        return True

class TunableSpawnActionVariant(TunableVariant):
    __qualname__ = 'TunableSpawnActionVariant'

    def __init__(self, **kwargs):
        super().__init__(affordance=SpawnActionAffordance.TunableFactory(), locked_args={'fade_in': SpawnActionFadeIn()}, default='fade_in', **kwargs)

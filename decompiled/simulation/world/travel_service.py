from protocolbuffers import Consts_pb2, InteractionOps_pb2
from clock import ClockSpeedMode
from interactions.aop import AffordanceObjectPair
from interactions.context import InteractionContext
from interactions.priority import Priority
from sims4.service_manager import Service
from sims4.tuning.tunable import TunableReference
import distributor
import services
import sims4.log
logger = sims4.log.Logger('Travel')

class TravelService(Service):
    __qualname__ = 'TravelService'
    TRAVEL_AFFORDANCE = TunableReference(manager=services.get_instance_manager(sims4.resources.Types.INTERACTION), description='The affordance used to make a Sim travel.')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.pending_travel_request = []

    def add_pending_travel(self, sim):
        self.pending_travel_request.append(sim.account)

    def has_pending_travel(self, account):
        return account in self.pending_travel_request

    def remove_pending_travel(self, sim):
        self.pending_travel_request.remove(sim.account)

def on_travel_interaction_succeeded(sim_info, from_zone_id, to_zone_id, callback, context):
    callback(from_zone_id, sim_info.sim_id, 1, context)
    services.travel_service().remove_pending_travel(sim_info)

def travel_sim_to_zone(sim_id, zone_id):
    travel_sims_to_zone((sim_id,), zone_id)

def travel_sims_to_zone(sim_ids, zone_id):
    travel_info = InteractionOps_pb2.TravelSimsToZone()
    travel_info.zone_id = zone_id
    travel_info.sim_ids.extend(sim_ids)
    distributor.system.Distributor.instance().add_event(Consts_pb2.MSG_TRAVEL_SIMS_TO_ZONE, travel_info)
    services.game_clock_service().set_clock_speed(ClockSpeedMode.PAUSED)

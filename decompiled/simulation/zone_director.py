from _hashutil import hash32
from _math import Vector2, Quaternion
import itertools
import math
import random
from build_buy import register_build_buy_exit_callback, unregister_build_buy_exit_callback
from date_and_time import DateAndTime
from default_property_stream_reader import DefaultPropertyStreamReader
from event_testing.resolver import SingleObjectResolver
from objects.components.state import TunableStateValueReference
from objects.definition_manager import TunableDefinitionList
from objects.system import create_object
from placement import FGLSearchFlag
from postures.posture_graph import PostureGraphService
from protocolbuffers import Routing_pb2
from services.fire_service import FireService
from sims.baby.baby_utils import run_baby_spawn_behavior
from sims.outfits.outfit_enums import OutfitCategory
from sims.sim_info_types import SimZoneSpinUpAction
from sims.sim_spawner import SimSpawner
from sims4 import geometry
from sims4.math import UP_AXIS
from sims4.resources import Types
from sims4.tuning.geometric import TunableVector2
from sims4.tuning.instances import HashedTunedInstanceMetaclass
from sims4.tuning.tunable import HasTunableReference, Tunable, OptionalTunable, TunableList, TunableTuple, TunableVariant, TunableInterval, AutoFactoryInit, HasTunableSingletonFactory, TunableReference, TunableAngle
from singletons import DEFAULT
from situations.service_npcs.modify_lot_items_tuning import TunableObjectModifyTestSet
from world.spawn_point import SpawnPointOption, SpawnPoint
import build_buy
import date_and_time
import distributor.rollback
import enum
import placement
import routing
import services
import sims
import sims4.log
import sims4.resources
import sims4.utils
import terrain
logger = sims4.log.Logger('ZoneDirector')

class _ZoneSavedSimOp(enum.Int, export=False):
    __qualname__ = '_ZoneSavedSimOp'
    MAINTAIN = Ellipsis
    REINITIATE = Ellipsis
    CLEAR = Ellipsis

class _OpenStreetSavedSimOp(enum.Int, export=False):
    __qualname__ = '_OpenStreetSavedSimOp'
    MAINTAIN = Ellipsis
    CLEAR = Ellipsis

def _get_connectivity_source_handle():
    current_zone = services.current_zone()
    spawn_point = current_zone.get_spawn_point()
    if spawn_point is not None:
        return routing.connectivity.Handle(spawn_point.center, spawn_point.routing_surface)
    corner = current_zone.lot.corners[0]
    routing_surface = routing.SurfaceIdentifier(current_zone.id, 0, routing.SurfaceType.SURFACETYPE_WORLD)
    return routing.connectivity.Handle(corner, routing_surface)

class CreateRandomObjectSetup(HasTunableSingletonFactory, AutoFactoryInit):
    __qualname__ = 'CreateRandomObjectSetup'
    FACTORY_TUNABLES = {'objects_to_choose_from': TunableDefinitionList(description='\n            An object will be randomly chosen from this list and placed near\n            whichever object passed the tuned tests for this crime scene\n            change.\n            '), 'maximum_distance': Tunable(description='\n            New objects will be spawned within this distance of objects that\n            pass the tuned tests for this crime scene change.\n            ', tunable_type=float, default=3.0)}

    def apply(self, obj):
        object_definition = random.choice(self.objects_to_choose_from)
        created_obj = create_object(object_definition)
        if created_obj is None or obj.routing_surface is None:
            return
        search_flags = placement.FGLSearchFlagsDefault | FGLSearchFlag.SHOULD_TEST_BUILDBUY | FGLSearchFlag.STAY_IN_CURRENT_BLOCK
        starting_location = placement.create_starting_location(location=obj.location)
        fgl_context = placement.create_fgl_context_for_object(starting_location, created_obj, search_flags=search_flags, max_distance=self.maximum_distance, random_range_weighting=sims4.math.MAX_INT32)
        (position, orientation) = placement.find_good_location(fgl_context)
        if position is None:
            created_obj.destroy()
            return
        source_handles = [_get_connectivity_source_handle()]
        dest_handles = [routing.connectivity.Handle(position, obj.routing_surface)]
        connectivity = routing.test_connectivity_batch(source_handles, dest_handles, allow_permissive_connections=True)
        if connectivity is None:
            created_obj.destroy()
            return
        created_obj.location = sims4.math.Location(sims4.math.Transform(position, orientation), obj.routing_surface)
        return DestroyObjects(objects_to_destroy=(created_obj,))

    def fill_quota(self, count):
        zone = services.current_zone()
        lot = zone.lot
        terrain = services.terrain_service.terrain_object()
        min_level = lot.min_level
        max_level = lot.max_level
        source_handles = [_get_connectivity_source_handle()]
        dest_handles = []
        num_points = count*2*(1 + max_level - min_level)
        positions = geometry.random_uniform_points_in_polygon(lot.corners, num=num_points)
        for position in positions:
            level = random.randint(min_level, max_level)
            surface = routing.SurfaceIdentifier(zone.id, level, routing.SurfaceType.SURFACETYPE_WORLD)
            handle = routing.connectivity.Handle(position, surface)
            dest_handles.append(handle)
        connectivity = routing.test_connectivity_batch(source_handles, dest_handles, allow_permissive_connections=True)
        if connectivity is None:
            return ()
        random.shuffle(connectivity)
        objects = []
        for (_, handle, _) in connectivity:
            orientation = sims4.math.angle_to_yaw_quaternion(random.uniform(0, 2*sims4.math.PI))
            (x, _, z) = handle.polygons[0][0]
            y = terrain.get_routing_surface_height_at(x, z, handle.routing_surface_id)
            location = sims4.math.Location(sims4.math.Transform(sims4.math.Vector3(x, y, z), orientation), handle.routing_surface_id)
            object_definition = random.choice(self.objects_to_choose_from)
            (result, _) = build_buy.test_location_for_object(None, object_definition.id, location=location)
            while result:
                obj = create_object(object_definition)
                obj.location = location
                objects.append(obj)
                if len(objects) == count:
                    break
        if not objects:
            return ()
        return (DestroyObjects(objects_to_destroy=objects),)

class SetStateSetup(HasTunableSingletonFactory, AutoFactoryInit):
    __qualname__ = 'SetStateSetup'
    FACTORY_TUNABLES = {'new_state': TunableStateValueReference(description='\n            The new state and value to set on the object.\n            '), 'undo_state': TunableStateValueReference(description='\n            The state to set the object back to during the crime scene cleanup.\n            ')}

    def apply(self, obj):
        new_state_value = self.new_state
        if not obj.state_component or not obj.has_state(new_state_value.state):
            return
        routing_context = routing.PathPlanContext()
        routing_context.set_key_mask(routing.FOOTPRINT_KEY_ON_LOT | routing.FOOTPRINT_KEY_OFF_LOT)
        for parent in obj.parenting_hierarchy_gen():
            while parent.routing_context and parent.routing_context.object_footprint_id:
                routing_context.ignore_footprint_contour(parent.routing_context.object_footprint_id)
        source_handles = [_get_connectivity_source_handle()]
        top_level_parent = obj
        while top_level_parent.parent is not None:
            top_level_parent = top_level_parent.parent
        reference_pt = obj.position
        if top_level_parent.wall_or_fence_placement:
            reference_pt += top_level_parent.forward*PostureGraphService.WALL_OBJECT_FORWARD_MOD
        dest_handles = [routing.connectivity.Handle(reference_pt, obj.routing_surface)]
        connectivity = routing.test_connectivity_batch(source_handles, dest_handles, routing_context=routing_context, allow_permissive_connections=True)
        if connectivity is None:
            return
        obj.set_state(new_state_value.state, new_state_value, immediate=True)
        return SetStateCleanup(((obj, new_state_value.state, self.undo_state),))

    def fill_quota(self, count):
        return ()

class CreateScorchSetup(HasTunableSingletonFactory, AutoFactoryInit):
    __qualname__ = 'CreateScorchSetup'
    FACTORY_TUNABLES = {'max_distance': Tunable(description='\n            The maximum distance, in meters, that floor scorch marks will be\n            spawned from the target object.\n            ', tunable_type=float, default=3.0)}

    def apply(self, obj):
        random_angle = random.uniform(0, 2*math.pi)
        random_distance = random.triangular(0, self.max_distance, self.max_distance)
        offset = sims4.math.Vector3(random_distance*math.cos(random_angle), 0, random_distance*math.sin(random_angle))
        position = obj.position + offset
        level = obj.location.level
        if not FireService.add_scorch_mark(position, level):
            return
        return CleanScorchMark(position, level)

    def fill_quota(self, count):
        lot = services.current_zone().lot
        min_level = lot.min_level
        max_level = lot.max_level
        cleanup_actions = []
        for _ in range(2*count):
            level = random.randint(min_level, max_level)
            x = lot.center.x + random.uniform(-lot.size_x/2, lot.size_x/2)
            z = lot.center.z + random.uniform(-lot.size_z/2, lot.size_z/2)
            position = sims4.math.Vector3(x, 0, z)
            if not FireService.add_scorch_mark(position, level):
                pass
            cleanup_actions.append(CleanScorchMark(position, level))
            while len(cleanup_actions) >= count:
                break
        return cleanup_actions

class ZoneDirectorBase(HasTunableReference, metaclass=HashedTunedInstanceMetaclass, manager=services.get_instance_manager(sims4.resources.Types.ZONE_DIRECTOR)):
    __qualname__ = 'ZoneDirectorBase'
    INSTANCE_TUNABLES = {'allow_venue_background_situations': Tunable(description='\n            If checked, venues will be allowed to run their background situations\n            (e.g. museum patrons will still show up and look at the art). If\n            unchecked, no venue-specific background situations will be created\n            (but walkbys may still occur).\n            ', tunable_type=bool, default=True), 'lot_preparations': TunableList(description='\n            A list of changes to objects when preparing the lot.\n            ', tunable=TunableTuple(tests=TunableObjectModifyTestSet(description='\n                    Tests to specify what objects to apply actions to.\n                    Every test in at least one of the sublists must pass\n                    for the action associated with this tuning to be run.\n                    '), action=TunableVariant(description='\n                    The action that will be applied to objects when preparing\n                    the lot.\n                    ', set_state=SetStateSetup.TunableFactory(), create_random_object_nearby=CreateRandomObjectSetup.TunableFactory(), create_scorch_mark=CreateScorchSetup.TunableFactory()), number_of_changes=TunableInterval(description='\n                    The minimum and maximum number of allowed changes of this\n                    type for the lot preparation. The exact number will be\n                    chosen randomly and according to how many objects meet the\n                    criteria for state changes.\n                    ', tunable_type=int, minimum=0, default_lower=1, default_upper=5))), 'objects_to_spawn': TunableList(description='\n            A list of objects to spawn on the lot. Objects will be cleaned up\n            when this zone director stops.\n            ', tunable=TunableTuple(description='\n                Object and position.\n                ', definition=TunableReference(description='\n                    Definition of the object to spawn.\n                    ', manager=services.definition_manager()), position=TunableVector2(description='\n                    Position on lot to spawn object at. Height is automatically\n                    set as the height of the terrain.\n                    ', default=Vector2.ZERO()), angle=TunableAngle(description='\n                    Orientation of the object.\n                    ', default=0), init_state_values=TunableList(description='\n                    List of states the created object will be pushed to.\n                    ', tunable=TunableStateValueReference()), children=TunableList(description='\n                    Children to slot into object.\n                    ', tunable=TunableTuple(definition=TunableReference(description='\n                            The child object to create.  It will appear in the\n                            first available slot in which it fits, subject to\n                            additional restrictions specified in the other\n                            values of this tuning.\n                            ', manager=services.definition_manager()), part_index=OptionalTunable(description='\n                            If specified, restrict slot selection to the given\n                            part index.\n                            ', tunable=Tunable(tunable_type=int, default=0)), parent_slot=TunableVariant(description='\n                            Restrict slot selection to the given slot.\n                            ', bone_name=Tunable(description='\n                                Restrict slot by bone name.\n                                ', tunable_type=str, default='_ctnm_chr_'), slot_reference=TunableReference(description='\n                                Restrict slot by slot reference.\n                                ', manager=services.get_instance_manager(Types.SLOT_TYPE))), init_state_values=TunableList(description='\n                            List of states the children object will be set to.\n                            ', tunable=TunableStateValueReference())))))}
    resource_key = None

    def __init__(self):
        self._cleanup_actions = []
        self._traveled_sim_infos = set()
        self._zone_saved_sim_infos = set()
        self._open_street_saved_sim_infos = set()
        self._resident_sim_infos = set()
        self._injected_into_zone_sim_infos = set()
        self._zone_saved_sim_op = _ZoneSavedSimOp.MAINTAIN
        self._open_street_saved_sim_op = _OpenStreetSavedSimOp.MAINTAIN
        self.was_loaded = False

    def on_startup(self):
        self.send_startup_telemetry_event()
        self._create_automatic_objects()
        register_build_buy_exit_callback(self._create_automatic_objects)

    def send_startup_telemetry_event(self):
        pass

    def prepare_lot(self):
        if self.was_loaded:
            logger.error('Zone director {} attempting to prepare lot on save/load. This should only happen the first time the lot is loaded into with the zone director.', self, owner='bhill')
            return
        cleanup_actions = []
        for lot_preparation in self.lot_preparations:
            num_changes = 0
            target_num_changes = lot_preparation.number_of_changes.random_int()
            if target_num_changes <= 0:
                pass
            all_objects = list(services.object_manager().values())
            random.shuffle(all_objects)
            for obj in all_objects:
                while not obj.is_sim:
                    if not obj.is_on_active_lot():
                        pass
                    resolver = SingleObjectResolver(obj)
                    if not lot_preparation.tests.run_tests(resolver):
                        pass
                    cleanup_action = lot_preparation.action.apply(obj)
                    while cleanup_action is not None:
                        cleanup_actions.append(cleanup_action)
                        num_changes += 1
                        if num_changes == target_num_changes:
                            break
            while target_num_changes != num_changes:
                new_actions = lot_preparation.action.fill_quota(target_num_changes - num_changes)
                cleanup_actions.extend(new_actions)
        spawn_cleanup_action = self._spawn_objects()
        if spawn_cleanup_action is not None:
            cleanup_actions.append(spawn_cleanup_action)
        for cleanup_action in reversed(cleanup_actions):
            self.add_cleanup_action(cleanup_action)

    def on_shutdown(self):
        self.send_shutdown_telemetry_event()
        unregister_build_buy_exit_callback(self._create_automatic_objects)

    def send_shutdown_telemetry_event(self):
        pass

    def load(self, zone_director_proto, preserve_state):
        self._cleanup_actions = _load_cleanup_actions(zone_director_proto)
        if zone_director_proto.HasField('resource_key'):
            previous_resource_key = sims4.resources.get_key_from_protobuff(zone_director_proto.resource_key)
        else:
            previous_resource_key = None
        if preserve_state and previous_resource_key == self.resource_key:
            self.was_loaded = True
            if zone_director_proto.HasField('custom_data'):
                reader = DefaultPropertyStreamReader(zone_director_proto.custom_data)
            else:
                reader = None
            self._load_custom_zone_director(zone_director_proto, reader)
        else:
            self.process_cleanup_actions()

    def _load_custom_zone_director(self, zone_director_proto, reader):
        pass

    def save(self, zone_director_proto):
        if self.resource_key is not None:
            zone_director_proto.resource_key = sims4.resources.get_protobuff_for_key(self.resource_key)
        _save_cleanup_actions(zone_director_proto, self._cleanup_actions)
        writer = sims4.PropertyStreamWriter()
        self._save_custom_zone_director(zone_director_proto, writer)
        data = writer.close()
        if writer.count > 0:
            zone_director_proto.custom_data = data

    def _save_custom_zone_director(self, zone_director_proto, writer):
        pass

    def add_cleanup_action(self, action):
        self._cleanup_actions.append(action)

    def process_cleanup_actions(self):
        old_cleanup_actions = self._cleanup_actions
        self._cleanup_actions = []
        for cleanup_action in old_cleanup_actions:
            try:
                cleanup_action.process_cleanup_action()
            except:
                logger.exception('Error running cleanup action {}', cleanup_action)

    def _create_automatic_objects(self):
        venue_tuning = services.venue_service().venue
        if venue_tuning is None:
            return
        starting_position = services.active_lot().get_default_position()
        object_manager = services.object_manager()
        for tag_pair in venue_tuning.automatic_objects:
            obj = None
            try:
                existing_objects = set(object_manager.get_objects_with_tag_gen(tag_pair.tag))
                while not existing_objects:
                    obj = self._create_object(tag_pair.default_value, starting_position)
            except:
                logger.error('Automatic object {} could not be created in venue {} (zone: {}).', tag_pair.default_value, venue_tuning, services.current_zone_id())
                if obj is not None:
                    obj.destroy(cause='Failed to place automatic object required by venue.')

    def _create_object(self, definition, position, orientation=None, state_values=()):
        obj = create_object(definition)
        if obj is None:
            return
        starting_location = placement.create_starting_location(position=position, orientation=orientation)
        fgl_context = placement.create_fgl_context_for_object(starting_location, obj, ignored_object_ids=(obj.id,))
        (position, orientation) = placement.find_good_location(fgl_context)
        if position is not None:
            obj.location = sims4.math.Location(sims4.math.Transform(position, orientation), routing.SurfaceIdentifier(services.current_zone_id(), 0, routing.SurfaceType.SURFACETYPE_WORLD))
        else:
            obj.destroy()
            return
        for state in state_values:
            obj.set_state(state.state, state)
        return obj

    def _create_child_object(self, definition, parent, slot_types=None, bone_name_hash=None, state_values=()):
        for runtime_slot in parent.get_runtime_slots_gen(slot_types=slot_types, bone_name_hash=bone_name_hash):
            while runtime_slot.is_valid_for_placement(definition=definition):
                break
        return
        child = create_object(definition)
        runtime_slot.add_child(child)
        for state_value in state_values:
            child.set_state(state_value.state, state_value)
        return child

    def _spawn_objects(self):
        spawned_objects = []
        lot = services.current_zone().lot
        for info in self.objects_to_spawn:
            position = sims4.math.Vector3(float(info.position.x), terrain.get_terrain_height(info.position.x, info.position.y), float(info.position.y))
            orientation = Quaternion.from_axis_angle(info.angle, UP_AXIS)
            lot_transform = sims4.math.Transform(position, orientation)
            world_transform = lot.convert_to_world_coordinates(lot_transform)
            obj = self._create_object(info.definition, world_transform.translation, orientation=world_transform.orientation, state_values=info.init_state_values)
            if obj is None:
                pass
            spawned_objects.append(obj)
            for child_info in info.children:
                slot_owner = obj
                if child_info.part_index is not None:
                    for obj_part in obj.parts:
                        while obj_part.subroot_index == child_info.part_index:
                            slot_owner = obj_part
                            break
                if isinstance(child_info.parent_slot, str):
                    slot_types = None
                    bone_name_hash = hash32(child_info.parent_slot)
                else:
                    slot_types = {child_info.parent_slot}
                    bone_name_hash = None
                child = self._create_child_object(child_info.definition, slot_owner, slot_types=slot_types, bone_name_hash=bone_name_hash, state_values=child_info.init_state_values)
                while child is not None:
                    spawned_objects.append(child)
        if spawned_objects:
            return DestroyObjects(objects_to_destroy=reversed(spawned_objects))

    def process_traveled_and_persisted_and_resident_sims(self, traveled_sim_infos, zone_saved_sim_infos, open_street_saved_sim_infos, injected_into_zone_sim_infos):
        self._zone_saved_sim_op = self._determine_zone_saved_sim_op()
        self._open_street_saved_sim_op = self._determine_open_street_saved_sim_op()
        self._traveled_sim_infos = set(traveled_sim_infos)
        self._zone_saved_sim_infos = set(zone_saved_sim_infos)
        self._open_street_saved_sim_infos = set(open_street_saved_sim_infos)
        self._resident_sim_infos = self._get_resident_sims()
        self._injected_into_zone_sim_infos = set(injected_into_zone_sim_infos)
        self._zone_saved_sim_infos.difference_update(self._traveled_sim_infos)
        self._open_street_saved_sim_infos.difference_update(self._traveled_sim_infos)
        self._injected_into_zone_sim_infos.difference_update(self._traveled_sim_infos)
        self._send_home_sims_who_overstayed()
        for sim_info in tuple(self._traveled_sim_infos):
            self._process_traveled_sim(sim_info)
        for sim_info in tuple(self._resident_sim_infos):
            self._process_resident_sim(sim_info)
        for sim_info in tuple(self._injected_into_zone_sim_infos):
            self._process_injected_sim(sim_info)
        for sim_info in tuple(self._zone_saved_sim_infos):
            self._process_zone_saved_sim(sim_info)
        for sim_info in tuple(self._open_street_saved_sim_infos):
            self._process_open_street_saved_sim(sim_info)

    def _determine_zone_saved_sim_op(self):
        current_zone = services.current_zone()
        if current_zone.lot_owner_household_changed_between_save_and_load() or current_zone.venue_type_changed_between_save_and_load():
            return _ZoneSavedSimOp.CLEAR
        if current_zone.active_household_changed_between_save_and_load() or current_zone.game_clock.time_has_passed_in_world_since_zone_save():
            return _ZoneSavedSimOp.REINITIATE
        return _ZoneSavedSimOp.MAINTAIN

    def _determine_open_street_saved_sim_op(self):
        current_zone = services.current_zone()
        if current_zone.time_has_passed_in_world_since_open_street_save():
            return _OpenStreetSavedSimOp.CLEAR
        return _OpenStreetSavedSimOp.MAINTAIN

    def _request_spawning_of_sim_at_location(self, sim_info, sim_spawn_reason, startup_location=DEFAULT, spin_up_action=SimZoneSpinUpAction.NONE):
        if sim_info.is_baby:
            run_baby_spawn_behavior(sim_info)
        else:
            if startup_location is DEFAULT:
                startup_location = sim_info.startup_sim_location
            sim_spawn_request = sims.sim_spawner_service.SimSpawnRequest(sim_info, sim_spawn_reason, sims.sim_spawner_service.SimSpawnLocationStrategy(startup_location), from_load=True, spin_up_action=spin_up_action)
            services.sim_spawner_service().submit_request(sim_spawn_request)

    def _request_spawning_of_sim_at_spawn_point(self, sim_info, sim_spawn_reason, spawner_tags=(SpawnPoint.ARRIVAL_SPAWN_POINT_TAG,), spawn_point_option=SpawnPointOption.SPAWN_SAME_POINT, spawn_action=None, spin_up_action=SimZoneSpinUpAction.NONE):
        if sim_info.is_baby:
            run_baby_spawn_behavior(sim_info)
        else:
            sim_spawn_request = sims.sim_spawner_service.SimSpawnRequest(sim_info, sim_spawn_reason, sims.sim_spawner_service.SimSpawnPointStrategy(spawner_tags, spawn_point_option, spawn_action), from_load=True, spin_up_action=spin_up_action)
            services.sim_spawner_service().submit_request(sim_spawn_request)

    def _send_home_sims_who_overstayed(self):
        for sim_info in tuple(itertools.chain(self._zone_saved_sim_infos, self._open_street_saved_sim_infos)):
            send_home = self._did_sim_overstay(sim_info)
            while send_home:
                self._send_sim_home(sim_info)

    def _did_sim_overstay(self, sim_info):
        if sim_info.is_selectable:
            return False
        if sim_info.lives_here:
            return False
        if sim_info.game_time_bring_home is None:
            return False
        time_to_expire = DateAndTime(sim_info.game_time_bring_home)
        if services.time_service().sim_now < time_to_expire:
            return False
        return True

    def _send_sim_home(self, sim_info):
        if not sim_info.lives_here:
            logger.debug('Sending home:{}', sim_info)
            self._zone_saved_sim_infos.discard(sim_info)
            self._open_street_saved_sim_infos.discard(sim_info)
            sim_info.inject_into_inactive_zone(sim_info.vacation_or_home_zone_id)

    def _process_traveled_sim(self, sim_info):
        self._request_spawning_of_sim_at_spawn_point(sim_info, sims.sim_spawner_service.SimSpawnReason.TRAVELING)
        if sim_info.get_current_outfit()[0] == OutfitCategory.SLEEP:
            random_everyday_outfit = sim_info.sim_outfits.get_random_outfit([OutfitCategory.EVERYDAY])
            sim_info.set_current_outfit(random_everyday_outfit)

    def _get_resident_sims(self):
        return set()

    def _process_resident_sim(self, sim_info):
        pass

    def _process_injected_sim(self, sim_info):
        self._request_spawning_of_sim_at_spawn_point(sim_info, sims.sim_spawner_service.SimSpawnReason.DEFAULT, spin_up_action=sims.sim_info_types.SimZoneSpinUpAction.PREROLL)

    def _process_zone_saved_sim(self, sim_info):
        if self._zone_saved_sim_op == _ZoneSavedSimOp.CLEAR:
            self._on_clear_zone_saved_sim(sim_info)
        elif self._zone_saved_sim_op == _ZoneSavedSimOp.REINITIATE:
            self._on_reinitiate_zone_saved_sim(sim_info)
        else:
            self._on_maintain_zone_saved_sim(sim_info)

    def _on_clear_zone_saved_sim(self, sim_info):
        if sim_info.is_selectable:
            self._request_spawning_of_sim_at_location(sim_info, sims.sim_spawner_service.SimSpawnReason.SAVED_ON_ZONE, spin_up_action=SimZoneSpinUpAction.PREROLL)
            return
        self._send_sim_home(sim_info)

    def _on_reinitiate_zone_saved_sim(self, sim_info):
        self._request_spawning_of_sim_at_location(sim_info, sims.sim_spawner_service.SimSpawnReason.SAVED_ON_ZONE, spin_up_action=SimZoneSpinUpAction.PREROLL)

    def _on_maintain_zone_saved_sim(self, sim_info):
        self._request_spawning_of_sim_at_location(sim_info, sims.sim_spawner_service.SimSpawnReason.SAVED_ON_ZONE, spin_up_action=SimZoneSpinUpAction.RESTORE_SI)

    def _process_open_street_saved_sim(self, sim_info):
        if self._open_street_saved_sim_op == _OpenStreetSavedSimOp.CLEAR:
            self._on_clear_open_street_saved_sim(sim_info)
        else:
            self._on_maintain_open_street_saved_sim(sim_info)

    def _on_clear_open_street_saved_sim(self, sim_info):
        if sim_info.is_selectable:
            self._request_spawning_of_sim_at_location(sim_info, sims.sim_spawner_service.SimSpawnReason.SAVED_ON_OPEN_STREETS)
            return
        self._send_sim_home(sim_info)

    def _on_maintain_open_street_saved_sim(self, sim_info):
        spin_up_action = SimZoneSpinUpAction.RESTORE_SI
        if self._traveled_sim_infos and sim_info.is_selectable and sim_info not in self._traveled_sim_infos:
            spin_up_action = SimZoneSpinUpAction.PUSH_GO_HOME
        self._request_spawning_of_sim_at_location(sim_info, sims.sim_spawner_service.SimSpawnReason.SAVED_ON_OPEN_STREETS, spin_up_action=spin_up_action)

    def determine_which_situations_to_load(self):
        situation_manager = services.get_zone_situation_manager()
        arriving_seed = situation_manager.get_arriving_seed_during_zone_spin()
        if arriving_seed is not None:
            arriving_seed.allow_creation = self._decide_whether_to_load_arriving_situation_seed(arriving_seed)
        zone_seeds = situation_manager.get_zone_persisted_seeds_during_zone_spin_up()
        for seed in zone_seeds:
            seed.allow_creation = self._decide_whether_to_load_zone_situation_seed(seed)
        open_street_seeds = situation_manager.get_open_street_persisted_seeds_during_zone_spin_up()
        for seed in open_street_seeds:
            seed.allow_creation = self._decide_whether_to_load_open_street_situation_seed(seed)

    def get_user_controlled_sim_infos(self):
        return [sim_info for sim_info in itertools.chain(self._traveled_sim_infos, self._injected_into_zone_sim_infos, self._zone_saved_sim_infos, self._open_street_saved_sim_infos, self._resident_sim_infos) if not sim_info.is_npc]

    def _decide_whether_to_load_arriving_situation_seed(self, seed):
        if services.current_zone().id != seed.zone_id:
            logger.debug('Travel situation :{} not loaded. Expected zone :{} but is on zone:{}', seed.situation_type, seed.zone_id, services.current_zone().id)
            return False
        if seed.travel_time is None:
            logger.debug("Not loading traveled situation {} because it's missing travel_time")
            return False
        time_since_travel_seed_created = services.time_service().sim_now - seed.travel_time
        if time_since_travel_seed_created > date_and_time.TimeSpan.ZERO:
            logger.debug('Not loading traveled situation :{} because time has passed {}', seed.situation_type, time_since_travel_seed_created)
            return False
        return True

    def _decide_whether_to_load_zone_situation_seed(self, seed):
        return seed.situation_type.should_seed_be_loaded(seed)

    def _decide_whether_to_load_open_street_situation_seed(self, seed):
        return seed.situation_type.should_seed_be_loaded(seed)

    def create_situations_during_zone_spin_up(self):
        pass

    @property
    def should_create_venue_background_situation(self):
        return self.allow_venue_background_situations

    def handle_active_lot_changing_edge_cases(self):
        situation_manager = services.get_zone_situation_manager()
        if services.game_clock_service().time_has_passed_in_world_since_zone_save() or services.current_zone().active_household_changed_between_save_and_load():
            sim_infos_to_fix_up = []
            for sim_info in self._zone_saved_sim_infos:
                sim = sim_info.get_sim_instance()
                while sim is not None and (sim_info.is_npc and not sim_info.lives_here) and not situation_manager.get_situations_sim_is_in(sim):
                    sim_infos_to_fix_up.append(sim_info)
            if sim_infos_to_fix_up:
                logger.debug('Fixing up npcs {} during zone fixup', sim_infos_to_fix_up, owner='sscholl')
                services.current_zone().venue_service.venue.zone_fixup(sim_infos_to_fix_up)

    def _prune_stale_situations(self, situation_ids):
        situation_manager = services.get_zone_situation_manager()
        return [situation_id for situation_id in situation_ids if situation_id in situation_manager]

    def handle_sim_summon_request(self, sim_info):
        SimSpawner.spawn_sim(sim_info)

class CleanupAction:
    __qualname__ = 'CleanupAction'

    @classmethod
    def get_guid(cls):
        if not hasattr(cls, 'GUID'):
            raise NotImplementedError('Each cleanup action must define a 32-bit GUID class attribute')
        return cls.GUID

    def process_cleanup_action(self):
        pass

    def save(self, cleanup_action_proto):
        cleanup_action_proto.guid = self.get_guid()

    def load(self, cleanup_action_proto):
        logger.assert_raise(cleanup_action_proto.guid == self.get_guid(), 'Incorrect GUID {} for cleanup action {} (guid {})', cleanup_action_proto.guid, self, self.get_guid())

class DestroyObjects(CleanupAction):
    __qualname__ = 'DestroyObjects'
    GUID = 150212831

    def __init__(self, objects_to_destroy=()):
        self.object_ids_to_destroy = [obj.id for obj in objects_to_destroy]

    def process_cleanup_action(self):
        zone = services.current_zone()
        object_manager = zone.object_manager
        inventory_manager = zone.inventory_manager
        objects_to_destroy = []
        for object_id in self.object_ids_to_destroy:
            obj = object_manager.get(object_id) or inventory_manager.get(object_id)
            while obj is not None:
                objects_to_destroy.append(obj)
        services.get_reset_and_delete_service().trigger_batch_destroy(objects_to_destroy)

    def save(self, cleanup_action_proto):
        super().save(cleanup_action_proto)
        cleanup_action_proto.object_ids.extend(self.object_ids_to_destroy)

    def load(self, cleanup_action_proto):
        super().load(cleanup_action_proto)
        self.object_ids_to_destroy = list(cleanup_action_proto.object_ids)

class DestroyObjectsOfType(CleanupAction):
    __qualname__ = 'DestroyObjectsOfType'
    GUID = 1460378648

    def __init__(self):
        self.object_definitions_to_destroy = []

    def process_cleanup_action(self):
        zone = services.current_zone()
        object_manager = zone.object_manager
        inventory_manager = zone.inventory_manager
        definitions_to_destroy = set(self.object_definitions_to_destroy)
        objects_to_destroy = []
        for obj in itertools.chain(object_manager.values(), inventory_manager.values()):
            while obj.definition in definitions_to_destroy:
                objects_to_destroy.append(obj)
        services.get_reset_and_delete_service().trigger_batch_destroy(objects_to_destroy)

    def save(self, cleanup_action_proto):
        super().save(cleanup_action_proto)
        for object_definition in self.object_definitions_to_destroy:
            resource_key = sims4.resources.Key(sims4.resources.Types.OBJECTDEFINITION, object_definition.id, 0)
            key_proto = sims4.resources.get_protobuff_for_key(resource_key)
            cleanup_action_proto.resource_keys.append(key_proto)

    def load(self, cleanup_action_proto):
        super().load(cleanup_action_proto)
        object_definitions_to_destroy = []
        for key_proto in cleanup_action_proto.resource_keys:
            resource_key = sims4.resources.get_key_from_protobuff(key_proto)
            object_definition = services.definition_manager().get(resource_key.instance)
            while object_definition is not None:
                object_definitions_to_destroy.append(object_definition)
        self.object_definitions_to_destroy = object_definitions_to_destroy

class SetStateCleanup(CleanupAction):
    __qualname__ = 'SetStateCleanup'
    GUID = 3024953117

    def __init__(self, state_changes=()):
        self.object_and_state_to_value = {}
        for (obj, state, state_value) in state_changes:
            self.add_state_change(obj, state, state_value)

    def add_state_change(self, obj, state, state_value):
        self.object_and_state_to_value[(obj.id, state.guid64)] = state_value.guid64

    def process_cleanup_action(self):
        zone = services.current_zone()
        object_manager = zone.object_manager
        inventory_manager = zone.inventory_manager
        state_manager = services.get_instance_manager(sims4.resources.Types.OBJECT_STATE)
        for ((object_id, state_guid64), state_value_guid64) in self.object_and_state_to_value.items():
            obj = object_manager.get(object_id) or inventory_manager.get(object_id)
            if obj is None:
                pass
            state = state_manager.get(state_guid64)
            if state is None:
                pass
            state_value = state_manager.get(state_value_guid64)
            if state_value is None:
                pass
            obj.set_state(state, state_value, immediate=True)

    def save(self, cleanup_action_proto):
        super().save(cleanup_action_proto)
        for ((object_id, state_guid64), state_value_guid64) in self.object_and_state_to_value.items():
            cleanup_action_proto.object_ids.append(object_id)
            with distributor.rollback.ProtocolBufferRollback(cleanup_action_proto.states) as safe_proto:
                safe_proto.state_name_hash = state_guid64
                safe_proto.value_name_hash = state_value_guid64

    def load(self, cleanup_action_proto):
        super().load(cleanup_action_proto)
        self.object_and_state_to_value = {}
        for (object_id, state_info) in zip(cleanup_action_proto.object_ids, cleanup_action_proto.states):
            self.object_and_state_to_value[(object_id, state_info.state_name_hash)] = state_info.value_name_hash

class CleanScorchMark(CleanupAction):
    __qualname__ = 'CleanScorchMark'
    GUID = 150212820

    def __init__(self, position=None, level=None):
        self.position = position
        self.level = level

    def save(self, proto):
        super().save(proto)
        proto.location = Routing_pb2.Location()
        proto.location.transform.translation.x = self.position.x
        proto.location.transform.translation.y = self.position.y
        proto.location.transform.translation.z = self.position.z
        proto.location.surface_id.secondary_id = self.level

    def load(self, proto):
        super().load(proto)
        self.position = sims4.math.Vector3(proto.location.transform.translation.x, proto.location.transform.translation.y, proto.location.transform.translation.z)
        self.level = proto.location.surface_id.secondary_id

    def process_cleanup_action(self):
        FireService.remove_scorch_mark(self.position, self.level)

def _load_cleanup_actions(zone_director_proto):
    cleanup_actions = []
    subclasses = sims4.utils.all_subclasses(CleanupAction)
    guid_to_action = {subclass.get_guid(): subclass for subclass in subclasses}
    for cleanup_action_proto in zone_director_proto.cleanup_actions:
        action_guid = 0
        try:
            action_guid = cleanup_action_proto.guid
            action_class = guid_to_action.get(action_guid)
            while action_class is not None:
                cleanup_action = action_class()
                cleanup_action.load(cleanup_action_proto)
                cleanup_actions.append(cleanup_action)
        except:
            logger.exception('Error restoring cleanup action with GUID {}', action_guid)
    return cleanup_actions

def _save_cleanup_actions(zone_director_proto, cleanup_actions):
    for cleanup_action in cleanup_actions:
        with distributor.rollback.ProtocolBufferRollback(zone_director_proto.cleanup_actions) as cleanup_action_proto:
            cleanup_action.save(cleanup_action_proto)

## @package pyexample
# documentation for this module
# 
# and more detail

## documentation for funcA
# with more detail
def funcA():
	pass
	
## handles ponies 
# @pre need to feed them first
# @warn might bite you
# @param[in] a wandering little pony
# @return captured true or false
# @see {funcA}
def funcB(pony):
	return True

## class A 
class ClassA:
	## constructor doing that
	def __init__(self):
		self._memVar = 0;
	
	## Documentation for a method.
   	#  @param self The object pointer.	
	def PyMethod(self):
		pass
	
	## A class variable.
	classVar = 0;
	
	 ## @var _memVar
	 # A member		 
	 self.m=1
   
